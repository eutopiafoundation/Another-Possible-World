---
title: Посвящения
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Для идеала в каждом из нас, кто страдает от мира, который не является таковым.

За страдания тех, кого мы любим, за наши собственные страдания, за страдания тех, кого мы не знаем, и за всех тех, кто еще придет...

Эта работа должна быть создана всем человечеством и обновляться на протяжении всей его эволюции, им самим и для него.
