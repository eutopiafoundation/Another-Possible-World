## Présentation

Bienvenue sur le récit collaboratif de la fondation Eutopia.

Il est conseillé dans un premier temps de lire l'ensemble du récit avant de soumettre des propositions. Cela vous permettra d'avoir une vue globale du récit et d'éviter de soumettre des propositions pour lesquels vous trouveriez des réponses dans la suite de votre lecture.

Pour se faire, rendez-vous sur https://wiki.fondation-eutopia.org

Vous pouvez ensuite passer au fichier suivant "2-HowContribute.md" qui vous expliquera comment contribuer au récit.

Merci d'ores et déjà pour votre participation.
