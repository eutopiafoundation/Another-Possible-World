---
title: Bibliographie
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

**Liste non exhaustive**

## Introduction

[1]: Yuval Noah Harari, Sapiens : Une brève histoire de l'humanité. 2011
[2]: Sébastien Bohler, Où est le sens ?/ 2020
[3]: [Étymologie du mot utopie](https://fr.wiktionary.org/wiki/utopie)
[4]: Serge-Christophe Kolm, La bonne économie : La réciprocité générale. 1984
[5]: [Concentration maximale de CO2 dans l'atmosphère](https://www.ipcc.ch/report/ar6/wg1/)
[6]: [Limite de perturbation des cycles biochimiques du phosphore et de l'azote](https://www.nature.com/articles/s41467-023-40569-3)
[7]: [Érosion maximale de la biodiversité](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[8]: [Usage intensif des sols](https://www.nature.com/articles/s41467-023-40569-3)
[9]: [Utilisation abusive d'eau douce](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[10]: [Acidification des océans](https://www.ipcc.ch/report/ar6/wg1/)
[11]: Comment tout peut s'effondrer, Pablo Servigne & Raphael Stevens
[12]: [Étymologie du mot eutopie](https://fr.wiktionary.org/wiki/eutopie)
[13]: Martin J. S. Rudwick, The Earth's History
[14]: The Future of Life on Earth" par Peter D. Ward et Donald Brownlee
[15]: Sébastien Bohler, Le bug humain. 2019
[16]: Jared Diamond, Effondrement. 2004
[17]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes. 2021
[18]: Serge Christhonne, Les élections sont-elles la démocratie ?. 1977
[19]: Ben Goldacre, Bad Science. 2008
[20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[21]: [The Replication Crisis](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[22]: ARTE, Documentaire : La fabrique de l'ignorance. 2020
[23]: Jaegwon Kim, Fondements de la métaphysique
[24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[25]: Aristote, Éthique à Eudème

## Partie 1 : Chapitre 1 : Imaginer un autre monde

[1]: Serge-Christophe Kolm, La bonne économie. 1984
[2]: Serge-Christophe Kolm, Les éléctions sont-elles la démocratie ?/ 1977
[3]: [Perspectives de la population mondiale 2022" publiées par les Nations Unies](https://desapublications.un.org/file/989/download)
[4]: [Rapports du GIEC 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[5]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes. 2021

## Partie 1 : Chapitre 2 : La planète Terre

[1]: [Wikipédia : Projection de Peters](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[2]: Isabelle Delannoy, L'économie symbiotique, Actes sud | Colibri, 2017
[3]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[4]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)

## Partie 1 : Chapitre 3 : L'espèce Homo Sapiens

[1]: [Rapport du GIEC](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[2]: [Wikipédia : Pyramide des besoins](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectives on Pshychological Science
[4]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[5]: [Chiffres et analyses sur le mal-logement en France, y compris le nombre de personnes sans-abris et les conditions de vie précaires.](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[6]: [Le renoncement aux soins pour raisons financières dans l’agglomération parisienne :](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[7]: [La Fondation de France publie la : 13e édition de son étude sur les solitudes](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[8]: [Handicap international : Les personnes handicapées restent parmi les plus exclues au monde](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[9]: Abraham Maslow, Motivation and Personality, 1970 (deuxième édition)
[10]: Arendt Hannah, La condition de l'homme moderne, 1961
[11]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes
[12]: Emmanuel Todd, L'origine des systèmes familliaux, 2011 - Où en sommes-nous ? Une esquisse de l'histoire humaine, 2017 - La lutte des classes en France au XXI siècle, 2020.
[13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997
[14]: Jean Baudrillard, La société de consommation, 1970
[15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994
[16]: Charles Darwin, L'Origine des espèces : Chapitre IV / III, 1859 & La Descendance de l'homme et la sélection sexuelle : Chapitre VII, 1871
[17]: Hannah Arendt, Les origines du totalitarisme, 1951
[18]: Albert Camus, La peste, 1947
[19]: Jean-Paul Sartre, L'existentialisme est un humanisme
[20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, L'homo Sociabilis : La réciprocité
[21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[22]: Sébastien Bohler, Le bug humain
[23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[25]: David Eagleman, The Primal Brain
[26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior
[27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[28]: Matthieu Ricard, L'altruisme : une énigme ?
[29]: Matthieu Ricard, "L'altruisme : Le gène ou l'éducation ?"
[30]: Kathryn Spink, "Mère Teresa : Une vie au service des autres"
[31]: Wolfram Schultz, The Role of Dopamine in Learning and Memory, 2007
[32]: Read, D., & Northoff, G, Neural correlates of impulsive and reflective decisions. Nature Neuroscience, 2018
[33]: Jean-Didier Vincent, Le Cerveau et le Plaisir
[34]: David Eagleman, Le Cerveau inconscient
[35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 and Gloria Y. Yeh3, A Systematic Review and Meta-analysis of the Effects of Meditation on Empathy, Compassion, and Prosocial Behaviors, 2018
[36]: Sébastien Bohler, Où est le sens ?, 2020
[37]: [Roy F. Baumeister, The Need for Meaning: A Psychological Perspective](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[38]: Eddy Fougier, Eco-anxiété : analyse d’une angoisse contemporaine
[39]: Albert Camus, Le Mythe de Sisyphe, 1942
[40]: [Enquête "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[42]: Jonathan Haidt, L'homme irrationnel 2001
[43]: Elliot Aronson, La théorie de la dissonance cognitive
[44]: Elizabeth Kolbert, La 6e Extinction, 2015
[45]: Robert Axelrod, Le dilemme du prisonnier
[46]: Nancy Huston, L'epèce fabulatrice
[47]: Serge-Chritophe Kolm, Le bonheur liberté
[48]: David M. Eagleman, Incognito, 2015
[49]: William James, The Principles of Psychology, 1980
[50]: [Wikipedia : Biais de sélection](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[51]: Julia Shaw, L'illusion De La Mémoire
[52]: David Dunning et Justin Kruger, Unskilled and unaware of it: How difficulties in recognizing one's own incompetence lead to inflated self-assessments, 1997
[53]: [Festinger et Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[56]: [Edward E. Jones et Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[57]: Kahneman PhD, Daniel, Thinking, Fast and Slow, 2011
[58]: [Asch, S. E., Forming impressions of personality. , 1946](https://psycnet.apa.org/record/1946-04654-001)
[59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[62]: Thorndike, E.L. A constant error in psychological ratings, 1920

## Partie 1 : Chapitre 4 : L'ère de la Noosphère

[1]: Serge-Christophe Kolm, La bonne économie, 1984
[2]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes
[3]: Braudel, F. Baiille, S., Philiphe, R. 1963. Le monde actuel, histoire et civilisation, manuel d'histoire de terminale.
[4]: Teilhard de Chardin, Le phénomène humain. 1955
[5]: Lao Tseu, Livre de la Voie et de la Vertu
[6]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p44-46
[7]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p239
[8]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p211
[9]: Harari, Yuval Noah, Sapiens: Une brève histoire de l'humanité, 2014
[10]: Gottner-Abendroth, H. 2019, Les sociétés matriartcales. Recherches sur les cultures autochtones à travers le monde.
[11]: Carl Gustav Jung, Psychologie inconsciente, 1916
[12]: Demoule, J.-P. 2008. La révolution néolithique.
[13]: [Flore Joachim, Expositio in Apocalypsim, 1184-1202](https://gallica.bnf.fr/ark:/12148/btv1b90778502/f2.item)
[14]: Diamond J. De l'inégalité parmis les sociétés, essai sur l'homme et l'environnement dans l'histoire, 2000
[15]: Mendras, H. La fin des paysans, 1967
[16]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p26-29
[17]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p29-44
[18]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p46-51
[19]: Todd, E. Où en sommes-nous ? Une esquisse de l'histoire humaine. 2017
[20]: Descartes, R. Le discours de la méthode pour bien conduire la raison et chercher la vérité dans les sciences, 1637
[21]: Chollet, Sorcières : la puissance invaincue des femmes 2018
[22]: Foucault, M. Histoire de la folie à l'âge classique. Thèse d'Etat de philosophie,1972
[23]: David Monnier, D. Psychanalyse du capitalisme. 2019.
[24]: Rey, O. Une question de taille, 2014
[25]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes, p52-57
[26]: Rifkin, J. La troisième révolution industrielle. 2012.
[27]: Keynes. J. Lettre à nos petits-enfants. Essai sur l'avenir du capitalisme. 1930.
[28]: Piketty, Le capital au XXI siècle. 2013.
[29]: Saez, E., Zucman, G. Le triomphe de l'injustice. Richesse évasion fiscale et démocratie. 2020
[30]: Boltansky, L., Chiapello, E. 1999. Le nouvel eprit du capitalisme. 1999
[31]: Pierre Bitoun et Yves Dupont. Le sacrfice des paysans, une catasrophe sociale et antrhopologique. 2016
[32]: Courcelle, Fijalkow & Taulelle. Services publics et terrotpores. 2017
[33]: Morin, E. Le temps est venu de changer de civilization. 2017
[34]: Luyckx-Ghisi, Surgissement d'un nouveau monde: valeur, visisons, économie, politique, tout change. 2012
[35]: Mattéi, J-F. Le sens de la démsure. 2009. M'homme dévasté, essai sur la déconstruction de la culture. 2015
[36]: Abdennour Bidar. Libérons-nous ! Des chaînes du travail et de la consommation. 2018
[37]: Patel, R., Moore, J. W. Comment nous monde est devenu cheap 2018
[38]: [Lily La Fronde, La Poste facture 19,90 euros les 5 minutes de conversation avec votre grand-mère](https://www.streetpress.com/sujet/1498489306-poste-facture-lien-social)
[39]: Jean Ziegler. Le capitaliisme expliqué à ma petite fille (en espérant qu'elle en vera la fin). 2018
[40]: Paul Jorion. Se débaraser du capitalisme, une question de survie. 2017
[41]: Hervé Kempf. Que crève le capitalisme, ce sera lui ou nous. 2020
[43]: Rey, O. Une question de taille. 2014
[44]: Pierre Dardot, Christian Laval. Ce cauchemar qui n'en finit pas : Comment le néolibéralisme défait la démocratie. 2016.
[45]: Cederstrom, C., Spicer, A. Le syndrome du bien-être. 2016
[46]: Cabanas, E. et Illouz, E. Happycratie. 2018
[48]: [Analyse critique par David Affagard (2019) dans le club Médiapart sur les termes utilisé par Laurent Alexandre (2019) lors d'une conférence à l'Ecole polytechnique du 14 janvier 2019, par la semaine du Plateau de Saclay sur le transhumanisme, Quel Homme pour 2050 ?](https://blogs.mediapart.fr/david-affagard/blog/060319/la-republique-en-marche-cautionne-l-eugenisme-numerique-en-conference-polytechnique)
[49]: Bidar, A. Libérons-nous ! Des chaines du travail et de la consommation .2018
[50]: Hannah Arendt. La condition de l'homme moderne.1961[1958]
[51]: Idriss Aberkane. Economie de la connaisance. Paris. Fondation pour l'innovation politique (2015)
[52]: Monnier, D. Psychanalyse du capitalisme. 2019
[53]: Snyder, G. Le sens des lieux, éthique, esthétique et basins-versants. 2018.
[54]: Jablonka, I. Des hommes justes, du patriarcat aux nouvelles masculinités. 2019.
[55]: [Wilczek, F. Krauss, L. The materiality of a vacuum, conférence du 31 janvier 2017](https://www.youtube.com/watch?v=TD9PofbrrLc)
[56]: Haramein, N. L'univers décodé ou la théorie de l'unification. 2012.
[57]: Charbonier, J-J. La conscience intuitive extraneuronale. 2017.
[58]: Graeber. Bullshit Jobs. 2018
[59]: Ludwig Wittgenstein. Tractatus logico-philosophicus. 1921.

## Partie 1 : Chapitre 5 : Collaborer tous ensemble

[1]: Earth Overshoot day 2017 fell on August 2, Earth Over shoot Day. https://overshootday.org
[2]: [Wikipédia : Economie](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[3]: Serge-Christophe Kolm. La bonne économie. 1984
[4]: La Fin de la croissance. Richard Heinberg. 2013
[5]: Sébastien Bohler. Le Bug humain. 2020
[6]: Karl Polanyi."The great transformation" et "The economy as Instituted Process " dans Trade and Market in the Early Empires : Economies in History and Theory.
[7]: Marshall Sahlins - Stone-age Economics
[8]: Marcel Mauss- Essai sur le don
[9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[10]: [Organisation sociale des Incas](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[11]: Sébastien Bohler. Où est le sens ? 2020
[12]: David Graeber. Dette: 5000 ans d'histoire. 2011
[13]: Lee Alan Dugatkin. The Altruism Equation: Seven Scientists Search for the Origins of Goodness. 2006
[14]: [Nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[15]: Yuval Noah Harari. Sapiens: Une brève histoire de l'humanité
[16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[17]: Daniel Kahneman. Thinking, Fast and Slow
[18]: [Ernst Fehr, The nature of human altruism](https://www.nature.com/articles/nature02043)
[19]: [The Need for Social Approval](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[20]: [Religion and Morality](https://plato.stanford.edu/entries/religion-morality/)
[21]: Wright, B., Altruism in children and the perceived conduct of others in Journal of Abnormal and social Psychology, 1942, 37, p. 2018-233
[22]: Gabriel Tarde. Les lois de l’imitation, 1890
[23]: Charles Duhigg. The Power of Habit. 2014
[24]: Émile Durkheim. Éducation et sociologie; 1922
[25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORY OF NORMATIVE CONDUCT: A THEORETICAL REFINEMENT AND REEVALUATION OF THE ROLE OF NORMS IN HUMAN BEHAVIOR
[26]: [L’étymologie du mot travail est-elle tripalium ?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[27]: Karl Marx, Critique du programme de Gotha (1875). 1891
[28]: Karl Marx. Le capital. 1867
[29]: [Conférence: Communisme & Capitalisme : l'histoire derrière ces idéologies](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[30]: Nino Fournier. L'ordre de l'argent - Critique de l'économie. 2019
[31]: [Documentaire Arte 2014 - Ilan Ziv - Capitalisme](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)

## Partie 1 : Chapitre 6 : Décider tous ensemble

[1]: [Wikipedia : Démocratie ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[2]: Serge-Christophe Kolm. Les éléctions sont-elles la démocratie ? 1977
[3]: David Shearman. The Climate Change Challenge and the Failure of Democracy (Politics and the Environment). 2007.
[4]: [Datageule. Démocratie(s) ?](https://www.youtube.com/watch?v=RAvW7LIML60)
[5]: [Science4All : Le principe fondamental de la politique](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory. 1656
[8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[9]: [Science4All.Favoriser l'honnêteté | Démocratie 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[10]: Raymond J. La Raja et Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail. 2015
[11]: [Science4All. Nos démocraties divisent | Démocratie 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[12]: [Science4All. Le scrutin de Condorcet randomisé | Démocratie 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[13]: [Science4All. Êtes-vous un hooligan politique ? Démocratie 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[15]: [Science4All. Rationnellement irrationnels | Démocratie 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies. 2008
[17]: [La Tronche en Biais #5. Les Biais de Confirmation](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[18]: [ScienceEtonnante - L'effet de Halo — Crétin de cerveau ! #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[19]: [La Tronche en Biais #3- La dissonance cognitive](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[20]: [Franklin Templeton Academy – Biais de disponibilité](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[21]: [Science4All. Petit communautarisme deviendra grand | Démocratie 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[22]: [TED Conférence. Eli Pariser nous met en garde contre "les bulles de filtres" en ligne.](https://www.youtube.com/watch?v=B8ofWFx525s)
[23]: [Science4All. Chère conviction, mute-toi en infection VIRALE !!! Démocratie 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[24]: Michel Diard. Concentrations des médias : les milliardaires vous informent. 2016

## Partie 1 : Chapitre 7 : Constitution d'Eutopia

[1]: [Déclaration universelle des droits de l'homme. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[2]: [Déclaration Universelle des Droits de la Terre Mère. 2012 ](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)

## Partie 2 : Indicatif d'une bonne économie

[1]: Tim Jackson. Prospérité sans croissance. 2009
[2]: [Comprendre l'effet rebond](https://theothereconomy.com/fr/fiches/comprendre-leffet-rebond/)
[3]: [Bonheur national brut](https://fr.wikipedia.org/wiki/Bonheur_national_brut)
[4]: [La théorie du Donut : une nouvelle économie est possible](https://www.oxfamfrance.org/actualite/la-theorie-du-donut-une-nouvelle-economie-est-possible/)

## Partie 2 : Digital

[1]: Extrait de : Isavelle Delannnoy. L'Economie symbiotique : Régener la planète, l'économie et la société. 2017
[2]: Licklider. La symbiose homme-ordinateur. 1960 L'ordinateur comme outil de communication 1968.
[3]: Prônée par Leonard Kleinrock, la section en paquets a été développée également et de facon parallèle au Royaume-Uni par Donald Davies et Roger Scantlebury.
[4]: Lawrence Lessig. The Future of Ideas. The Fate of the Commons in a Connected World. 2001
[5]: Giles Slade. Made to Break: Technology and Obsolescence in America. 2006
[6]: [Obsolescence programmée : un fléau difficile à éradiquer](https://lebondigital.com/obsolescence-programmee-un-fleau-difficile-a-eradiquer/)

## Partie 2 : Temps de travail

[1]: Serge-Christophe Kolms. La bonne économie. 1984
[2]: Adret. Travailler deux heures par jour. 1977
[3]: [Insee. Croissance – Productivité](https://www.insee.fr/fr/statistiques/4277770?sommaire=4318291)
[4]: Bullshit Job. David Graeber. 2018
[5]: [Etude Mercer Marsh Benefits - Baromètre de l’absentéisme](https://www.mercer.com/fr-fr/insights/total-rewards/employee-benefits-strategy/barometre-de-labsenteisme-2023/)
[6]: [OMMUNICATION DE LA COMMISSION AU PARLEMENT EUROPÉEN, AU CONSEIL, AU COMITÉ ÉCONOMIQUE ET SOCIAL EUROPÉEN ET AU COMITÉ DES RÉGIONS sur une approche globale en matière de santé mentale](https://health.ec.europa.eu/system/files/2023-06/com_2023_298_1_act_fr.pdf)
[7]: [ STATBEL. Emploi et chômage ](https://statbel.fgov.be/fr/themes/emploi-formation/marche-du-travail/emploi-et-chomage)
[8]: Keynes. J. Lettre à nos petits-enfants. Essai sur l'avenir du capitalisme. 1930.

## Partie 2 : Organisation du travail

[1]: Isabelle Delannoy. L'économie symbiotique : Rénégéner la planète, l'économie et la société.

## Partie 2 : Mode de production

[1]: Extrait d'Isbaelle Delannoy dans "L'économie symbiotique : Régénérer la planète, l'économie et la société. 2017
[2]: Union internationale des communications. ICT Facts Figures 2016.
[3]: Sven E. Jorgensenet al., "Recent Progress in Systems Ecology", Ecol. Model., 2015
[4]: Jean-Francois Vincent, Olga A. Bogatyreva et Nikolay R. Bogatyrev, "Biomimetics : its Practice and Theory", J. R. Soc. Interface, 3, 2006, p. 471-482
[5]: Lynn Margulis, Origin of Eukaryotic Cells, Yale University Press, New Haven, 1970 ; Lynn Margulis et René Fester, Symbiosis as a Source of Evolutionary Innovation : Speciation and morphogenesis, The MITPress, Cambridge, 1991

## Partie 2 : Transport

[1]: Isabelle Delannoy. L'économie symbiotique : Régénérer la planète, l'économie et la société. 2016

## Partie 2 : Construction

[1]: Isabelle Delannoy. L'économie symbiotique : Régénerer la planète, l'économie et la société. 2017
[2]: Valérie Jousseaume. Ploucpride : Un nouveau réci pour les campagnes p263. 2021
[3]: Frédéric Lenoir. La guérison du monde. 2012
[4]: Alain de Botton. L'architecture du bonheur. 2009
[5]: Guégen, N., Meineri, S. Pourquoi la nature nous fait du bien. 2012

## Partie 2 : Energie

[1]: Agence internationale de l'énergie, Key World Energy Statistics. 2016
[2]: Isabelle Delannoy. L'économie symbiotique : Régénérer la planète, l'économie et la société. 2016
[3]: Jonathan G. Koomey, Growth in Data Center Electricity Use 2005 to 2010, Analytics Press, Oakland, 2011. L'énergie déoensée par les centres de stockage des données informatiques équivaudrait à 1.1 à 1.5% de la consommation électrique mondiale.
[4]: Un réacteur nucléaire a une puissance moyenne de 1 gigawatt. Une consommation de 200 000 à 300 000 milliards de kilowattheures correspond à la production de 23 à 31 réacteurs nucléaires (consommation/(365\*24))
[5]: En 2005 comme en 2010. Jonathan G. Koomey, "Worldwide Electricity Used in Data Centers Environnemental Research Letter vol. 3, n°3, N, IOP Publishing Ltd, 23 septembre 2008 ; Jonathan G. Koomey, Growth in Data Center Electricity Use 2005 to 2010, op, cit.
[6]: The Climate Group for the Global e-Sustainability Intiative, "Smart 2020 : Enabling the Low Corbon Economy in the Information Age" (PDF), cité dans l'article anglais de Wikipédia sur les data centers.

## Partie 2 : Nourriture et agriculture

[1]: Isabelle Delannoy. L'économie symbiotique. Régénéerer la planète, l'économie et la société. 2017.

## Partie 2 : Religion

[1]: Sébastien Bohler. Ou est le sens ?. 2020

## Partie 2 : Liste des professions

[1]: [ Les 10 chiffres clés du secteur de la banque et de l'assurance ](https://www.hellowork.com/fr-fr/medias/chiffres-banque-assurance.html)
[2]: []()
[3]:
[4]: [Commercial à bord des trains](https://www.emploi.sncf.com/nos-metiers/commercial-a-bord-des-trains/)
[5]: [Forces armées françaises](https://fr.wikipedia.org/wiki/Forces_arm%C3%A9es_fran%C3%A7aises)
[6]: [](https://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-economique-et-commerce-exterieur/soutenir-les-entreprises-francaises-a-l-etranger/les-secteurs-economiques-de-pointe-un-atout-pour-la-france-soutien-aux-secteurs/article/industries-et-technologies-de-defense)
[7]: [Shortage of drivers in the EU](https://truckmobility-info.com/shortage-of-drivers-in-the-eu/)
[8]: [La constante évolution des VTC et des taxis ](https://www.journaldunet.com/mobilites/1494709-la-constante-evolution-des-vtc-et-des-taxis/)

## Autres :

- L'Écologie ou la mort. Socialter
- Ecolère. Thibaut Mareschal
- [La faute à notre cerveau, vraiment ? Les erreurs du Bug humain de S. Bohler](https://bonpote.com/la-faute-a-notre-cerveau-vraiment-les-erreurs-du-bug-humain-de-s-bohler/)
- [The Century of the Self / Le siècle du moi (épisode 1 - VOSTFR)](https://www.youtube.com/watch?v=8Tt9hRY7Uk8)
- [Economie du donut](https://www.oxfamfrance.org/actualite/la-theorie-du-donut-une-nouvelle-economie-est-possible/)
- Podcast : #50 - Démocratie & écologie : redonner le pouvoir aux citoyens ? Mathilde Imer. Greenletter Club, l'écologie décortiquée
- Podcast : #79 - Décroissance : retour à la lampe à l'huile ? Yhimothée Parrique. Greenletter Club, l'écologie décortiquée
- Podcast : #78 - Croissance verte : un mythe dangereux ? Timothée Parrique. Greenletter Club, l'écologie décortiquée
