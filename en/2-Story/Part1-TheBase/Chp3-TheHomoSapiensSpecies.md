---
title: The Homo Sapiens species
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## Homo Sapiens

The human species may be endowed with a formidable intelligence, a conscience and an ever-growing technological knowledge, but we can't help noticing that when confronted with the present and future consequences of climate disruption, with figures to back them up, we just can't seem to change our course.[^1] [^2

Hence the need to understand humans and how they function, because only in this way can we ensure that the environment created in our Eutopia is possible, and that we can predict our behavior within it.

Understanding human "weaknesses" will enable us to adapt and develop tools accordingly, in order to exploit them to create a strength.

We're going to understand the mechanisms at the individual level, its various physical and psychological aspects... But this approach would be insufficient, and is accompanied by a whole range of global and sociological considerations.

<!-- ## Brief history of Homo sapiens -->

## The needs of Homo Sapiens

Let's return to our research question:

"How can we enable all humans to have a fulfilling life on Earth for the next 80 million generations?"

We started from the premise that a fulfilled life for every human is a fulfilling life, one that meets their needs and aspirations. A need is a felt necessity, whether physical, social or mental.

### Classification of needs

To help us in our research, we'll start with Abraham Maslow's pyramid, formulated in 1943. This classifies human needs into 5 stages, and is an interesting basis for an overview of the major human needs[^2].

Note that this model is being called into question today, because according to Maslow, we only feel a higher need when the previous one has been reached. But in reality, these needs are by no means pyramidal: today, numerous advances have demonstrated that social needs are just as important as physiological needs, which is not to say that they should be at the base of the pyramid, but rather that they are omnipresent at every level, and it's the same for the others: everyone feels them all concomitantly[^3].

But in any case, we don't need to understand exactly which need is more important than another, and under what circumstances, because the aim here is to fulfill them all without exception.

Below, we'll complete the pyramid by updating the theory in the light of developments in psychological and neuroscientific knowledge.

Maslow's Pyramid](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

On the first level are physiological needs, the basic needs necessary to maintain life and the proper functioning of the organism, such as eating, drinking, sexuality, sleeping, breathing...

It's worth noting here the total failure of our current system to meet the basic needs of the entire population. Today, 25,0000 people have died of hunger, including 15,000 children under the age of 5[^4]. Similarly, sexuality is often taboo, stigmatized, discriminated against and uneducated, leading to numerous disorders, transmission of disease, acts of non-consent, trauma, mental and physical health problems, lack or addiction to pleasures, lack of a sense of freedom and confidence, all of which create a general lack of well-being with one's sexuality for a very large part of the population.

On the second floor, we have security needs. The security of shelter, such as a place to live, the security of access to desired resources, such as materials, tools, games... Physical safety from violence and aggression... Moral and psychological security, emotional security and stability, medical/social security and health security.

Here, too, we note the various shortcomings of our society at this level, even in Western countries: housing security is a real challenge for many people, particularly the homeless, who are often faced with precarious and inhumane conditions[^5]. Access to resources may also be limited for some people, depending on their socio-economic status, which can affect their well-being and quality of life. Regarding medical and psychological safety, there is a lack of mental health professionals, which makes access to care difficult. That even with social security in countries like France, the cost of care can be prohibitive for some people, preventing them from receiving the care they need[^6].

The 3rd tier are the needs to belong, the need to love and be loved, to have intimate relationships, to have friends, to be part of a cohesive group, to feel accepted, not alone or rejected.

We live in a society where individualism is very present and/or human relationships are often neglected in favor of personal success. A large proportion of the population is feeling increasingly lonely, and a loss of social ties. Some are rejected and isolated, leading to mental health problems such as depression and anxiety. [^7]Our social model favors generalized competition and self-interested or commercial relationships as the only relational model, promoting a veritable culture of egoism.

The fourth stage represents esteem needs: human beings need to feel considered, to feel appreciated and respected by others, to feel recognized for their achievements and to feel capable of succeeding. These needs are fulfilled by doing things that are valued by society, by setting goals and working towards them, or by engaging in activities that enable personal development.

The fifth stage is the need for self-fulfillment. This is the need to realize oneself, to exploit and enhance one's personal potential. It manifests itself in the desire to develop one's talents and skills, to engage in activities that bring meaning and satisfaction, and to surpass oneself.

In today's society, it is difficult to satisfy this need for self-fulfillment, as the social pressures and high expectations imposed on most individuals can make it difficult to pursue one's passions and personal interests. Our highly competitive society and its alienation of the individual, leaves little time to explore and develop one's talents and skills. Many people are also excluded from the system, especially those with disabilities who are unable to fit into it due to its lack of adaptation. [^9]

Nevertheless, this need for self-fulfillment was somewhat challenged in 2010 by the work of 4 psychologists, who proposed an update with regard to evolutionary theory. [^3] This removes the need for self-fulfillment to make way for three needs linked to the reproductive drive: encounter, continuity and posterity.

|     |     |
| --- | --- |

| Need for posterity: Postpones inevitable death.
| Need for continuity: Keeping partner, status, etc. | Repels loss |
| Need for encounter : To acquire a partner | Repels competitors and social isolation |
| Need for esteem and status: To be recognized as a unique being.
| Need for belonging or affiliation: To be loved and accepted for who you are.
| Need for protection: Shelter, security +physical, family, psychological, financial, etc. | Repel violence |
| Physiological needs: Breathing, drinking, eating, relieving oneself, sleeping, keeping warm | Pushing back hunger, thirst, cold, ashpyxia, fatigue and illness | Pushing back violence, violence, violence, violence, violence

\*List of seven unconscious and concomitant basic human needs from D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

These needs are unconscious evolutionary needs, expressed in a thousand different ways in each person's life. For example, artistic creativity can be based just as much on the need to acquire status and be socially recognized as a unique being as on the need to seduce partners, or to endure in time by leaving a work to posterity.

We've just reviewed the 5/7 types of need, but it's worth noting that towards the end of his life, Abarham Maslow added a 6th need, "self-transcendence". This is at the top of the pyramid, and he would say that :

> "The fully developed human being working under the best conditions tends to be motivated by values that transcend his 'self'"[^9].

It's interesting to realize that a fulfilled human being who goes beyond self-fulfillment would be fully open to going beyond his or her individuality to commit to the service of others.

To conclude, these needs can also be complemented by the reflections of Hannah Arendt.

In our human condition, we are driven towards a quest for eternity: the vita contemplativa. This quest is increasingly difficult to satisfy in our modern societies, which no longer believe in God. We are also driven towards a quest for immortality, the vita activa. [^10]

The quest for immortality is what drives a mortal human being to utter words and perform creative acts that go beyond his own vital necessity, his own very existence, and project him into the future beyond his own death.

Originally, the age of modernity socially recognized the need for immortality, for example through the heroism of soldiers, through titles and mottos, or through the courage and authenticity of uncompromising speech: "One only thinks, one only speaks forcefully from the depths of one's grave: it is there that one must place oneself, it is from there that one must address human beings" (Diderot, 1782). Today, all these titles, all these words, all these actions, no longer resonate in our society. Hannah Arendt argues that the social repression of the quest for eternity out of the public domain, coupled with the social repression of the quest for immortality, explains how "the modern age - which began with an explosion of human activity so new, so rich in promise - ends in the most inert, sterile passivity that history has ever known. [^10][^11]

Emmanuel Todd makes a similar point. He asserts that the transition from "Man who believes in Man" to "Man who believes in God" has generated significant collective energy. Moving away from the quest for eternity has reinforced energies towards the quest for immortality. When the church as a social framework collapsed, a new secular humanist ideal developed. This was the immense hope in the New Man that was carried by the revolutionary movements of the 19th century. By contrast, the collapse of the communist social ideal and the revolutionary republican ideal marks the social end of the quest for immortality. "The Man who believes in 'Man, disappears in his turn and becomes the "Man who no longer believes in anything at all", a nihilistic, apathetic human being, without hope or purpose, filling his existential void with consumerism[^12].

What ensures the satisfaction of our need for posterity? According to Hannah Harendt, it's the work. A work is that which is capable of leaving a posterity, i.e. a child. It also means planting trees under which the passage of time will not allow us to come and take shade ourselves. It means passing on something to children. It means writing poetry or carving pieces of wood. It means heroically sacrificing one's life. It's doing work that has meaning. It offers inner security and self-control. Through his or her actions, the individual manifests his or her unique work to the world, and offers a glimpse of immortality. Each person's work, created in private and offered in public, is what makes the human world a world. [^10]

Subsistence work is mere survival. According to the philosopher, work is a vital necessity, sustaining our physiological existence. Work is a narrow dimension of human life over which we have no real power. Hannah Arendt deplores the stunting of the collectively-proposed project of life by the incredible place taken, in modern societies, by subsistence work. In her view, work should be relegated to the private sphere and made secondary. Work should be at the center of public social life.

Hannah Arendt's philosophical reflection on the excessive importance of the work of vital necessity and the disappearance of the work of posterity provides many explanatory links to the contemporary situation. It allows us to understand the disappearance of citizens, replaced by consumers. It allows us to understand the absolute existential disarray that unemployment can produce in a society that has forgotten the work and is publicly organized solely around subsistence work. It allows us to understand the attitude of the younger generations to subsistence work and their quest for meaning in a trade. Last but not least, it allows the pyramid of needs to be broken down into human facets.

From a bodily point of view, to survive means to reproduce, and the different ones have children. From a spiritual point of view, it means being eternal. From a social point of view, immortality means leaving a trace of one's passage on earth. The quest for immortality, as Hannah Arendt understands it, is the social expression of the need for posterity, which is present at every level of our physical, psychological, social and spiritual lives. Here, descent meets transcendence. It's worth remembering that, for Hannah Arendt as for Kenrick Douglas and his colleagues, the need for posterity combines two necessities: the intimate production of a totally unique and personal work, combined with a relationship to the world that can manifest it.

This multidimensional need for posterity is completely overshadowed by our functionalist, utilitarian society. And yet it is here, in these acts that have no apparent economic or rational significance for everyday life or the development of immediate benefits, that meaning and grandeur are to be found. Today's modern society, by proposing a life limited to vital survival and material development, has gradually cut off access to the satisfaction of the need for posterity. The result is a nagging feeling of dissatisfaction and unfulfillment, in a society where, paradoxically, abundance and pleasure reign supreme. This is where the quest for meaning comes in. This quest for meaning seems unable to find recognition in hyper-modern society. [^11]

Here, then, are the fundamental human needs, complemented by Hannah Arendt's reflections. The arrows represent the different dimensions of the human body according to Eastern medicine. This representation was chosen earlier than the classical distinctions of modern Western medicine, because it broadens the spectrum to include Hannah Arendt's vita comtemplativa. The arrows are dotted to indicate that they are outside the scope of current science.

[![Basic human needs supplemented by Hannah Arendt's reflections](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)]

### Intrinsic and extrinsic needs or desires

We need to distinguish between intrinsic and extrinsic needs or desires.

Intrinsic needs are internal, personal motivations that come from within the individual.

Extrinsic needs are external motivations that come from the environment and the individual. They refer to the image that the individual wants to project to others, and even to him/herself. Consumption can satisfy needs of both types at the same time, and most consumption that satisfies intrinsic needs also has an extrinsic dimension.

Extrinsic needs are closely linked to various social norms, status, ethical behavior and the whole collective imaginary. Sometimes the satisfaction of extrinsic needs has a drawback on intrinsic needs[^13].

Social distinction and imitation create extrinsic desires.

Every society will create extrinsic needs and desires.

In our society, these desires are constantly and powerfully manipulated: education and the social environment inculcate lifestyle models, or very often the feeling that more is better, and advertising creates desire after desire[^14].

Dissatisfaction stems from the gap between desires and their satisfaction, or in other words between aspirations and realizations.

To limit this dissatisfaction, we can a priori play on two terms: reducing desire or satisfying it. Emphasis is often placed on the second, satisfying the desire, but the existence of the first is almost entirely forgotten or neglected.

So what we need to do first is understand how our desires are formed and how we can control them. And that's what we're going to do.

## Human nature

First and foremost, however, it's essential to take stock of human nature, as this is a point that comes up regularly in debate. The social sciences and neurosciences have long been trying to distinguish between what is innate and what is acquired, but there is no consensus on human nature.

While the natural sciences seemed to support utilitarianism for over half a century, it is now possible to contradict this hypothesis: man is not only selfish, but also not perfectly rational. This non-utilitarianism of the human being applies to the most primitive forms of life: cooperation is inscribed from the very beginning and would be, along with survival of the fittest, one of the essential motors of evolution[^15].

Believing that our evolution is based exclusively on inter-individual competition brings a biased view of evolution, whereas the role of cooperation was already present in Darwinian evolutionary theory. [^16]

But whatever human nature may be, history's countless examples prove that it can become anything: the worst as well as the best.[^17][^18] Human nature need not justify anything about what we wish to become. We can certainly decide what we wish to become in our society, because we simply have the possibility of doing so.[^19]

What is certain, however, is that humans are not hermits; they live in society and are sociable. Humans are first and foremost homo sociabilis, with a propensity to get close to their fellow creatures and interact with them. They need to talk, exchange information, share knowledge and emotions, but they also fundamentally need to exist in the eyes of others. He seeks human interaction for the sociability it provides and for the exchange of positive, gratifying images on which he feeds. [^20][^21]

## Shaping our desires

We said earlier that we must first understand how our desires are formed and how we can control them.

For over a billion and a half years, we've had a small organ in our skull called the striatum.

The striatum releases dopamine, a substance that gives us a sense of pleasure and will reinforce behavior identified as a success. [^22]

It's part of a whole mechanism of cooperation and dialogue between different structures and takes part in decision-making, receives information and compares different options, potential gains, effort required, immediacy of reward.

This system is often called the reward circuit, but dopamine is merely a hormone that predicts the outcome of an action. [^23]

These dopamine discharges give rise to incentives that drive us to action, and will reinforce that behavior or activity in the future.

These reinforcers, also known as deep brain motivators, are : [^22]

- Food
- sex
- Social position
- Need for information
- Least effort
- Intellectual stimulation

These motivations were essential to our survival and the development of our species in primitive times, and are still highly sensitive today. Neurons in the striatum and throughout the reward circuit attract us to what satisfies our reinforcers, i.e. anything that provides food, sex, status, ease and information.
They are not intrinsically harmful, but as we can see at present, they can be exploited to develop productivist, consumerist, individualistic and environmentally destructive economies.

What's special about this mechanism, which conditions our brains not only to seek food, money, sex and social status, is that it seeks to obtain ever more money, ever more food, ever more sex or ever more status.

When a rat is placed near a corridor with a piece of cheese at the end, its neurons release dopamine as soon as it enters the corridor. The first time he finds a piece of cheese at the end of the corridor, his neurons discharge dopamine; the second time, if he finds the cheese, his dopamine neurons do nothing. But if he finds two pieces of cheese instead of one, his dopamine neurons will discharge again. [^22]

The striatum does all this only to the extent that it can do more and more.

So we only succeed in stimulating our pleasure circuits by increasing the doses.

Another of our mechanisms is that the pleasure and ease we can afford now has a hundred times more weight in our decisions than consideration of the distant future. The more distant in time a benefit is, the less value it has for our brain. This is known as temporal devaluation.

We prefer immediate pleasure to a tenfold pleasure in the future, or to making a choice that benefits us now, but will have harmful consequences in the long term.

The longer the delay, the weaker the anticipatory response[^24].

There are certainly genetic differences between people, and some are more sensitive to their striatum, but it is above all elements linked to education and the socio-cultural environment that prove to be decisive. [^25][^26]

These neurons are actually at the root of our learning processes

They condition us: once we've learned that food follows a gesture, the gesture itself becomes attractive. This is the principle of social learning: we are then conditioned to like this or that behavior. The principle of operant conditioning reigns supreme in a place of learning par excellence, the school.

Selfishness and altruism are equally motivated by pleasure.

When we give a gift to someone we love, it doesn't consciously occur to us to do it to feel good, and yet we do feel good.

So we might ask ourselves, if altruism makes us feel good personally, and on top of that, we're conscious of it, is it still altruism, or is it more a kind of well-placed self-interest?

There's a fine line between altruism and egoism, a line that doesn't even exist on a biological level: there isn't an altruism hormone or an egoism hormone. There's an act for which our brain calculates the probability that it will benefit us, and then we derive pleasure from it. The areas involved in the brain are common, but what we notice is that there is greater activation of the dorsolateral prefrontal cortex in egoism, due to the more pronounced evaluation of cost-benefit calculations. [^27] But overall, it's we who classify acts as altruistic or egoistic, and this definition can therefore vary according to our way of thinking.

We would say that in the egoistic act, the sole aim is to derive personal benefit.

In the altruistic act, the intention is to give benefit to another, while receiving an unconsciously sought-after benefit. But there can also be the intention to give benefit to another, while receiving a consciously sought benefit. [^28]

The altruistic act is therefore a mutual exchange that goes both ways, whereas the selfish act only goes one way.

Both egoism and altruism can therefore lead to individual pleasure, and we'll discover which of these 2 motivations is the most effective in achieving our common goal.

But if some people are more generous, it's probably because their brains have been configured that way from an early age. Like Mother Teresa, renowned for her unwavering dedication to helping others, her compassion and unconditional love for all those she met, she devoted her life to doing good for others, without concern for her own personal safety or comfort, and became one of the world's most important symbols of charity and altruism. When she was just 6 years old, her mother would take her to visit the poorest of the poor, alcoholics and orphans. And she would give her an invariable, stubborn recommendation: "My daughter never accepts a mouthful that is not shared with others". Sharing, taught in the cradle on a regular basis and with a constant concern for application and exemplarity, becomes here such a powerful conditioning that Mother Teresa's striatum will never waver in eighty years of vocation. Her environment and upbringing thus contributed to developing in her values and motivations such as generosity, compassion and the desire to serve others[^29][^30] Of course, religion, in advocating these moral values of generosity and altruism, plays an important part in the configuration of Mother Teresa's brain, and this stems from our search for meaning, which we'll discuss below.

Studies also reveal the existence of a bonus for mental agility and problem-solving ability, which leads to the activation of memory zones and better memorization of information communicated by the teacher. In this way, the dopamine received by a pupil when he receives a good point will modulate synaptic plasticity and consolidate memories. [^31]

Of course, the striatum is not the only part of our brain involved in decision-making. The frontal cortex is the foremost part of the human brain, and has become increasingly thicker over the course of evolution, reflecting our increasing cognitive abilities. It is the seat of willpower and planning.

For example, when participants were asked to choose between twenty euros right away or 30euro in 2 weeks' time, those who took the money right away saw the striatum light up, otherwise it was the frontal cortex[^32].

So, if we want to form a human community capable of grasping the challenges of the future, these are the connections we need to develop through high standards, awareness and perseverance.

## Controlling our desires

To limit the harmful effects of our striatum, there are 3 possible solutions,

- Suppressing the striatum: Throughout history, striatum activity has been blocked by moral commandments and the effort of the will against temptation. But this is no solution: if we repress our striatum, we can affect our ability to feel motivation or pleasure, which can lead to problems such as depression and various disorders. Repression has negative consequences for individual well-being and functioning, and is therefore neither an effective nor a sustainable solution. [^33]

- Taking the striatum at its own game, we can indeed play on primary reinforcers to achieve an ideal society, for example making it the social norm to have a healthy and sustainable lifestyle. Push the development of reinforcers essential to a good society.

- But a more effective and sustainable solution is to call on the unique capacity of the human being, consciousness. For the striatum's strength comes from the fact that its commands are non-conscious.

Once the striatum's neurons have become accustomed to a certain social level, they become blunted and we no longer feel anything, so it becomes imperative to move up another notch to stimulate them. This incremental process doesn't produce lasting satisfaction; it can't bring happiness.

Most of our actions are undertaken with a very low level of consciousness. More often than not, we act mechanically. Even when we engage in intellectual activities, it would be wrong to claim that we do so consciously. [^34]

We are endowed with a cerebral cortex of enormous computing power, which we use primarily for utilitarian, performance and technical purposes. For millennia, our power of abstraction, conceptualization and planning has been used primarily to design tools that satisfy our striatum.

We are beings with a high level of intelligence, but a low level of consciousness. [^22]

Intelligence works out solutions, generates calculations, implements goals and programs. But it can do all this without consciousness. The most obvious example is artificial intelligence, which can perform extremely complex tasks without any consciousness.

The great artificial intelligence systems you know today also operate with a notion of probability, and by a reinforcement system, which involves rewarding or punishing one's actions in order to encourage the learning of desirable behaviors. In other words, these systems work in a similar way to dopamine in humans.

And so, if we want to create consciousness in AI, all we need to do is understand how our own is created. This mystery
is not yet fully understood by neuroscience, and we'll come back to this later. But if it is, and we manage to create this consciousness, the difference between us and a machine will become very fine. This potential breakthrough raises a host of ethical questions, and leads us to reflect on how we perceive our own consciousness.

In any case, part of the solution to the challenges facing our species lies in adding more consciousness to our everyday actions.

Consciousness is also a sounding board for our perceptions.

By developing our sensory sounding board, we can trick our striatum into thinking it's getting more pleasure, when we're giving it quantitatively less.

A very practical tool for developing our awareness is mediation. [^35]

Bringing our degree of consciousness up to a level comparable with our level of intelligence will undoubtedly be a major challenge in building a good society and ensuring the future of our species. The evolution towards a society of consciousness and an economy of mental growth.

## The need for meaning and certainty

Once the individual has the autonomy of choice and belief, i.e. the right to give his or her life the direction he or she wishes, defining that direction has become an arduous task. With direction no longer fixed by religion or a totalitarian political regime, each of us must create our own meaning. But when we realize that our existence is brief and doomed to nothingness, we are faced with finding a justification for our existence and our actions, and this realization can become unbearable. [^36]

Because meaning is more important to life than anything else, it has a survival value for the human being. So to make up for our lack of it, we plunge into the search for money, status and so on...

The pleasure signal generated upstream of what the animal is looking for is a prediction made by our brain about what's going to happen. This production takes place because it's an evolutionary advantage, a mechanism that increases the animal's chances of survival.

By being able to produce what's going to happen based on what it observes around it, it increases its power of control and decision-making tenfold. It can seek out advantageous situations and flee potentially dangerous ones. He's one step ahead of reality.

The brains of vertebrate animals have invented a way of making predictions and staying one step ahead of reality. This ability to establish links between the state of the environment at time T and its future state is the basis of what we call, in a highly celebrated species like Homo Sapiens, sense.

We have created a sense of human society, knowing that we can be accepted and find our way in it, provided we respect the established rules and codes. This society is not left to chaos, it has an order, and this intelligible order is fundamentally reassuring to us.

Our tendency to detect links of meaning within our environment is so developed and irrepressible that it sometimes leads us to discern links where none necessarily exist.

For example, when a warrior goes out to hunt prey, puts on a necklace and hunts prey with it, the next time he puts on the same necklace, the warrior will receive a small jolt of dopamine about the possibility of capturing another prey. The warrior will feel more confident about his future success.

This system of anticipation reduces feelings of uncertainty about the future. Observing, predicting and anticipating future events, and reducing anxiety, are all part of the notion of meaning. This advantage is so decisive that there is every reason to believe it has been selected by evolution.

Detecting meaning around us is so crucial to our survival that situations in which this meaning eludes us provoke the onset of acute physiological anxiety. This reaction is triggered by our organism's survival instinct[^37].

With the help of brain imaging tools, we can find out what's going on in the brain. A small strip of cerebral cortex, located a few centimetres above the striatum, comes into action. This is a fold of cerebral cortex located at the interface between two cerebral hemispheres, themselves connected to the striatum, and called the anterior cingulate cortex. This inner cingulate cortex lights up as soon as the predictions made are no longer confirmed by what actually happens.

This alarm signal signifies a violation of the prediction. If too many predictions are invalidated, it becomes difficult to organize oneself and one has the impression of falling into chaos. Overloaded, this error signal becomes harmful to the individual's physical and mental health. It triggers a powerful stress reaction in the body, with the cingulate cortex activating a multi-link nervous circuit that leads down to a brain center involved in fear and anxiety, the amygdala, then to the adrenocortical glands on the kidneys, and to neuronal nuclei in the brain stem, which release hormones such as cortisol and noradrenaline, putting the body in a flight or even paralysis posture, and provoking anguish that can become existential.

Our cingulate cortex thus plays the role of an alarm signal, warning us when our world no longer makes any detectable sense. The consequences of this reaction range from sleep disorders to depression, anxiety, memory decline, cardiovascular disease and diabetes.

If the level of order and organization in our environment begins to decline, this central part of our brain activates and alerts us to the presence of a potential danger to our survival. If society is relatively stable, where the structures of work, family and human relationships don't change too unpredictably and arbitrarily, the cingulate cortex is a factor of adaptation and adjustment, but if the reference points change too quickly, and constantly without giving the individual any respite, it can be extremely dangerous for himself and for others.

But when faced with a void of meaning, how does the human mind react? It builds systems of representation steeped in meaning, order and coherence. And so, for as long as man has existed, all he has done is breathe meaning into reality.

The first attempts took the form of mythical accounts of creation and nature. The need for meaning stems from the need for control, and is an offshoot of our desire to survive. And representing the world as a place inhabited by meaning calms the internal warning system in the event of a punctual prediction error in the concrete world.

As a result, the alert reaction triggered by the cingulate cortex is naturally attenuated in believers. But today, for most people in the Western world, we no longer believe, and have lost this sense.

The great systems of religious, ideological, democratic or philosophical meaning are, so to speak, no more than devalued referents, weakened by scientific knowledge and the coexistence of multiple spiritual or ideological messages that we strive to tolerate, but whose sheer multiplicity is enough to reduce to nothing the hope that any one of them alone can hold an absolute truth.

Throughout history, rituals have systematically appeared before moral systems. This synchronization and mimicry at work in collective rituals makes humans more sensitive to the feelings, desires and emotions of their fellow human beings. This ability to put ourselves in someone else's shoes is called cognitive empathy, and enables us to "put ourselves in the other person's shoes", to feel what they feel and think what they think. And this ability is enhanced tenfold by imitation, as synchronized partners feel compassion for each other.

Rituals soothe our cingulate cortex by enabling us to predict more reliably what our fellow human beings are likely to do or not do, simply because we can already predict their movements during the ritual.

But it's more difficult to predict people's thoughts than their movements. To achieve this, we need to share not just gestures, but mental representations. In other words, values and worldviews.

There can be different worldviews, some of which are normative, in the sense that they prescribe certain behaviors, while others are merely positive (or factual), and conversely do not prescribe behaviors. When this vision prescribes what is good to do or not, and people adhere to it and see the world in the same way as we do, it considerably reduces uncertainty and de facto the activity of the cingulate cortex.

But their violation, on the other hand, activates it (and other brain structures). For our anterior cingulate cortex, non-compliance with moral standards is a fundamental predictive error. This, in turn, is a powerful calming factor for the cingulate cortex as it regulates the behavior of others and seeks to find its place among its fellow human beings.

As soon as you are certain that others believe in the same sacred moral values as you do, you can begin to predict with greater reliability what they are likely to do or not do.

As the Latin verb religere means "to bind", we understand that religion's main function is to bind people together.

In the course of discovery, mankind has lost its social and moral focus in favor of prediction about the material world. As the famous philosopher Friedrich Nietzsche put it: "God is dead, everything is permitted".

For our cingulate cortex, this is the beginning of anguish. Because the meaning provided by science only offers a positive (or factual) vision, it doesn't have quite the same reassuring power as that provided by religion and morality.

Science introduces a factual meaning, but not, at least initially, a moral one.

Of course, the factual meaning obtained by science is colossal. Today, human beings know to a certain extent how the earth was formed and how they arrived on it, and can retrace fairly precisely the sequence of events that led to the evolution of species from bacteria to the first vertebrates, mammals, apes and Homo Sapiens. The question of the cause of its existence, its nature and its physical and mental characteristics is answered. Likewise, knowledge of the laws of physics and of living organisms enables him to predict a staggering number of concrete facts, such as the weather, eclipses of the moon and sun, the power of a combustion engine according to the degree of refinement of the petrol used and its cubic capacity, and so on.

But we no longer know what makes sense in human and existential terms. For if we have killed meaning, we have not killed the need for meaning. The part of our brain that took hundreds of thousands of years to conform to find meaning in the world and in social structures is still very much there. [^36]

In our society, we are living out the myth of Sisyphus, a Greek mythological character condemned to eternally roll a boulder up a hill, only to roll it back down again each time before reaching the top. [^39] In our society, the way we produce no longer makes sense. We are now in a disposable world. And when nothing lasts, life becomes a waste. We're all more or less confronted with this problem.

What's more, with today's climate change, nothing is stable anymore: neither the cycle of the seasons, nor sea levels, nor the occurrence of natural disasters. The world is now on the move, and this movement is going to exceed our debt capacities.

The word is harsh, but uncertainty kills, it undermines our brains, it destroys the fundamental human aspiration to meaning.

## Our reactions to uncertainty

The feeling of indeterminacy activates the cingulate cortex, which painfully picks up on the impossibility of making predictions about the future, of defining oneself and glimpsing a clear life trajectory.

Eco-anxiety, for example, is a new form of anxiety resulting from the paralyzing awareness of our planet's destruction. [^38]

In a world with a maximum level of uncertainty, the cingulate cortex will manage to restore certainty on smaller scales, since it cannot do so on the more global scale of its existence. This is what we call micro-certainty.

Drugs that affect our neurons can be a substitute for meaning. Cocaine, for example, destroys uncertainty.

Self-esteem is the self-centered invention of an individualistic world, and a way of compensating for the loss of meaning.

Money is the absolute passport to freedom from the anguish of uncertainty.

But in the game of competition, inequalities are growing. The losers in this race, dehumanized by the breaking of the cord, will then in the worst case turn to a degraded form of humanity. This is the kind of humanity that denies the humanity of others to ensure its own exclusivity. We're talking here about identity.

Because this tension automatically gives rise to an urgent need: to know once and for all who you are. And the simple way to clarify your identity is to define yourself by reference to a group to which you belong. Defining oneself through one's relationship to a group fulfils a fundamental need: to find a way of conforming to the rules in force, which will limit untimely reactions from the cingulate cortex.

Indeed, one of the main sources of meaning regularly mentioned in surveys is the feeling of belonging to a social group[^40].

Inequalities precipitate societies towards identity-based withdrawal. Where the only thing that counts is the ability to use the production machine for one's own ends. The neo-liberal economic model, based on competition, personal mobility, accelerated work schedules and reduced public spending on the protection and equitable distribution of wealth, provokes an authoritarian and identity-based impulse designed to soothe individual reactions to uncertainty and a lack of social belonging.

Anomie refers to the feeling of a loss of order and logic in society.

The people who most often have nostalgic thoughts are also those who have the clearest impression that existence has meaning[^41].

Our brains are extremely good at denial.

When our actions are not in line with our mental representations, this creates cognitive dissonance. Contradiction also activates cognitive dissonance.

This is because the human brain seeks coherence and reason in all circumstances. It will therefore strive to resolve the dissonance that represents uncertainty, either by modifying its actions to bring them into line with its thinking, or by modifying its thinking to bring it into line with its actions.

Research on cognitive dissonance shows that most of the time it's the thoughts that adapt to the actions, and not the other way round. [^42][^43]

We lose touch with reality, because we no longer know how to interpret it. Sense protects us because it suggests ways of interpreting the world and acting more effectively to master it and protect us from its threats.

As the spectre of major disasters looms ever closer, community retreats will only become more pronounced, as will the compensatory behaviours of hyper-consumption, ego-inflation and denial.

The lack of systems of meaning drives us to consume material goods, especially in situations of high uncertainty, acceleration and competition. Hypermaterialism is the palliative for uncertainty and competition[^36].

## Solutions to the lack of meaning

The brain seeks to predict the world in order to better control it, and it achieves this either through meaning or through technology. When technology advances, it no longer needs meaning. And when technology fails, it needs meaning.

Today we have technology, but it has failed us, because it is signing our extinction. [^44]

In view of the disaster now unfolding and yet to come, we must stop the consumption machine that humanity has become as quickly as possible, while actively seeking ways to repair the earth.

If the more sense decreases, the more consumption increases, then conversely, the more sense increases, the more consumption decreases.

We have 8 billion cingulate cortexes busy deceiving their fear of death and the unpredictable, now we need to keep them busy deceiving this fear by uniting them around a shared meaning[^36].

This meaning must bring together billions of people of different cultural origins, scattered civilizational heritages and heterogeneous intellectual backgrounds, to make them forget their obsession with possessing and exploiting.

There are 2 meanings: the cosmic and the social.

The first is based on our understanding of the laws of nature, matter and the universe. The second involves our ability to act, in a social context, in accordance with what we believe to be right and wrong.

They create a moral system, defining a right and a wrong that regulate social and individual action.

The sacred is the pivot from which meaning is constructed in human assemblies. Today, modern humanity has lost the sacred by deconstructing reality and discovering that everything, from the movement of atoms in a molten star to the flow of ions in the membrane of your neurons as you read these lines, obeys mechanistic laws in which good and evil have no place.

The consequence of this demoralization of the world is that every individual on the face of the Earth can decide what he or she finds good or bad.

Yet no collective sense can exist without the notion of the sacred. The solution is to recreate the sacred, and the sacred can be our earth.

The sacred can only be manifested through ritual, and humanity will have to invent new rituals to break the deadlock it has reached. And, as we've seen, synchronous practices can calm the cingulate cortex. Such rituals will have to make the preservation of the planet sacred.

By doing so, humans can provide themselves with an effective means of reducing the mental load of anguish associated with individualism. Knowing that the citizens of my country, of the entire continent, if possible of the entire planet, hold sacred what I myself hold as an unsurpassable value, creates the basis for a new pact of belonging and trust.

Man is a conditional cooperator, capable of immense sacrifice provided he has the assurance that the other members of his community will do the same. [^45]

By making carbon purity and the preservation of biological and geological equilibrium sacred values, the billions of human beings can bring the activity of their cingulate cortexes back to acceptable levels and cease to be dependent on material goods, drugs, money and ego substitutes to support their own existence.

Ecology can propose a worldview common to all human beings.

With knowledge, the list of acts compatible with sacred value is a matter of science, measurement and calculation by the human collective.

At stake is the creation of a sense of global identity that gives everyone the feeling of being accepted into a group, without the need to prove their worth and eligibility through the accumulation of material goods or ego supports.

Today, we have to accept that science can say not only what is true and false, but also what is right and wrong.

The third meaning to come will therefore be ecological.

To develop it, we need to develop knowledge through education, to pass on knowledge of living systems, biodiversity, climatology, ecology and the evolution of species.

This will create a sense of wonder - for wonder is a powerful antidote to the existential anguish that grips us - through the discovery of the infinite richness of living things, the beauty of landscapes, flora and fauna, as a source of meaning.

As we all inhabit the same Earth, act upon it and suffer the consequences of its degradation, we are led to recognize the same sacred value.

In this way, citizens can engage in actions that are consistent with their values and convictions, and experience a sense of meaning.

Becoming a player in this change is in itself already a source of meaning, and of alignment between our opinions and our actions.

We need to review the very foundations of our civilization. The logic of just-in-time production and consumption that underpins our economies is unsustainable. In the following chapter, you'll discover our proposal in terms of economy'.

Deep down, we all have an immense aspiration, we're in search of meaning, but we don't know how to satisfy it. The aim of this work is to satisfy that longing, and to do so without even waiting for it to become a reality - because it's already there, at the very beginning.

## Need for fiction - The storytelling brain

The human brain, that extraordinary fabulation machine, is at the heart of our ability to create narratives, for better or for worse. It forges the stories that enable us to justify our actions and give them meaning. This narrative faculty is no accident but the product of a complex evolution, revealing the depth of our need for meaning and order in a chaotic world. [^46]

So it's not just that our species has been waging war since the dawn of time - chimpanzees and ants do it just as much - but that we've been making a whole story of it... and millions of stories.

Other animals don't do evil for evil's sake, nor do they do anything else for good's sake, acting mainly on the impulse of immediate needs or instincts. Human beings, on the other hand, seek meaning in their actions and those of others, often through the prism of morality, right and wrong. This quest for meaning is therefore a manifestation of our higher brain functions, which drive us to seek explanations and justify our behavior in a wider context.

What we think of as the "self" is not an isolated or autonomous entity, but rather a mosaic of traits, beliefs and values borrowed, shared and sometimes contested from those around us. Every interaction, every cultural or social exchange contributes to shaping our identity, making it dynamic and evolving. Our self is woven from others. We define ourselves in the mirror of our relationships, each person being a reflection that helps us form our own image[^49].

This process of narrative construction resonates with Buddhist teachings on the illusion of the self. According to Buddhism, the "self" we perceive as a stable, continuous entity is an illusion, a construct of the mind. [^47] We'll come back to this point in greater depth in the Religion section of Part 2. In the same way, neuroscience reveals that our identity is a story we tell ourselves, a coherent sequence fabricated by our brain to maintain an illusion of continuity[^48].

Identifying with our stories and our constructed "self" can lock us into limiting thought patterns. Recognizing this narrative construction as an illusion can be liberating. It invites us to question traditional narratives and open up to new possibilities for perceiving and experiencing the world.

The interplay between "real life" and fiction takes on a new dimension. Rather than seeing these spheres as distinct, we can understand that they are intrinsically linked, mutually nourishing each other. Our personal and collective narratives are not mere distractions or escapes, but powerful tools for transformation and understanding.

Recognizing the narrative nature of our brains and acknowledging the illusion of self doesn't mean rejecting our identity or our stories, but seeing them for what they are: flexible, modifiable and profoundly human constructs. It encourages us to stop following traditional narratives and say to ourselves "This is how the world has always been and always will be", but to understand and say to ourselves "We only see what we see".

It offers us a path to a richer, more nuanced understanding of ourselves and the world, no longer clinging to our identity as an irremovable reality and identifying with it exclusively, but allowing us to glimpse in our lives the possibility of reimagining the world and ourselves.

## Effects and cognitive bias

Here, we'll take a look at the different effects and biases of our brains, as they can affect our perception, memory, judgments, decisions and behavior in a variety of ways, influencing the way we process and interact with information. This understanding of ourselves will enable us to develop effective tools for our society, in particular for shaping our decision-making process, and to avoid the consequences of these biases preventing us from achieving our common goal.

A detailed reading of cognitive effects and biases, although recommended, is by no means necessary and can be skipped for your reading comfort, as they have been summarized in the following section.

### Summary of cognitive effects and bias

Here is a non-exhaustive list of cognitive biases:

- Selection bias: We are more inclined to select information that fits our preconceived beliefs and opinions, rather than considering a wider range of information. [^50]

- Memory illusion: We tend to overestimate the reliability of our memories and color them with our current perception[^51].

- Rationalization of beliefs: We tend to justify our decisions and behaviors, even if they are wrong or harmful, by interpreting them in a way that is favorable to ourselves.[^53]

- Overestimation bias: We tend to overestimate our ability to predict future events and control situations[^52].

- Optimism bias: We tend to overestimate the probability of future benefits and underestimate potential risks. [^54]

- Complexity reduction bias: We tend to simplify the complexity of situations to make them more manageable[^55].

- Causal inference bias: We tend to attribute a cause to an event even when this is not necessarily justified or reasonable.[^56]

- Reduction effect bias: We tend to overestimate the positive effects of an action and underestimate the negative effects.[^57]

- Primacy effect bias: We are influenced by the first information we receive and tend to weight it more heavily than later information.[^58]

- Similarity effect bias: We are more inclined to like and trust people who are similar to us[^59].
- Availability heuristic: We often use a quick, intuitive estimate to assess the probability or frequency of an event, based on the information most readily available in our memory.[^60]

- Peer pressure: We are influenced by the perception of what others think of us and are pressured to conform to their opinions and behaviors.[^61]

- Halo bias: We tend to generalize a positive or negative opinion of a person based on a single characteristic.[^62]

### In-depth explanation of effects and cognitive bias

- Confirmation bias

Confirmation bias is a cognitive bias. It's when we favor information that supports our opinions and beliefs. As we search for meaning, we select the information that goes our way. The solution to this is to be our own contradicter, to put our thoughts to the test and try to prove them wrong.

In this way, the contradictions in our judgments should not hurt us, but awaken us and put us into action. We don't like the correction of our opinions, but on the contrary, we should lend ourselves to it and offer ourselves to it, and let it come in the form of a conversation and not a lecture.

We shouldn't, rightly or wrongly, look for ways to get rid of each opposition, but rather look deeply into whether it can be right.

- Availability bias

Availability bias is another cognitive bias that can affect decision-making. It occurs when we estimate the probability of an event or the frequency of its occurrence on the basis of examples that come quickly to mind. This bias can lead us to underestimate the probability of rare events and overestimate that of common or recent ones. We overestimate our reasoning by favouring information directly accessible to our memory.

To avoid this bias, it is important to take into account a representative sample of information and try to be objective when assessing probabilities. It is also important not to rely solely on information that is immediately available, but to seek out additional information that can give us a more complete and balanced view.

- Halo effect

The halo effect is the mechanism that makes us attribute qualities to someone based on their physical appearance.

For example, a dating site called cupidexe has carried out an experiment, rating people's profiles on personality and looks. Those with a low physical score have a low personality score, while those with a high physical score have a high personality score. You'd think they'd have good looks because they seem to have a good personality, but in the top 1%, some personality profiles are empty...
They redid an experiment to check, here users came across profiles with text and sometimes not, and whether you had text or not you were given the same score.

Other studies and statistics confirm this effect. For example, all American presidents are tall, and the taller you are, the more money you make.

- Barnum effect

The Barnum effect refers to the tendency of people to believe that vague or general comments are accurate descriptions of their personality or life. This phenomenon is named after the famous showman P.T. Barnum, who used vague personality readings to attract people to his shows.

For example, a personality test that states "you are a caring person who loves your family and friends" is vague enough to be considered true for most people. However, it doesn't give specific details about the person, which can lead to a feeling of identification with the general description, even if it's not very personal.

The Barnum effect can be reinforced by people's need for recognition and thirst for self-determination. People like to feel understood and valued, which can encourage them to accept general descriptions as true for them.

It's important to take the Barnum effect into account when analyzing personality tests or comments about your life or personality. It's always best to look for more specific and detailed information to gain a more accurate understanding of yourself.

- Anchoring effect

The anchoring effect refers to people's tendency to rely on an initial piece of information, called an "anchor", when making estimates or decisions. This first piece of information can have a major impact on subsequent estimates, even if it is irrelevant or even erroneous.

For example, if you're asked to guess the number of people living in your town, and you're given a high figure as your first piece of information, you'll tend to make a higher estimate than if you'd been given a lower figure. Similarly, if you're negotiating the price of an object, the first proposal will often be considered the anchor and will influence subsequent proposals.

The anchoring effect can be used consciously or unconsciously in negotiation, sales or persuasion situations, leading to less-than-rational and biased decisions.

- Peer pressure

Group pressure is a social phenomenon that describes the tendency of individuals to conform to the opinions, attitudes and behaviors of a group to which they belong or with which they feel affiliated. This phenomenon can occur when individuals seek to conform to social norms and avoid disapproval or isolation from the group.

For example, when a person finds themselves in a group of people with a particular opinion or attitude, they may be influenced by these opinions and attitudes, even if they do not correspond to their own. This can be particularly true in situations where the person wants to be accepted by the group and avoid disapproval or exclusion.

Group pressure can also manifest itself when a person feels obliged to conform to the behaviors of a group to avoid feeling different or uncomfortable. For example, someone in a group that drinks alcohol may be influenced to drink alcohol themselves, even if they don't feel like it.

It's important to be aware of group pressure, and not to give in to a group's opinions or behavior simply to avoid disapproval or to conform to social norms. It's always better to follow your own opinions and convictions than to give in to group pressure.

- Availability heuristic

The availability heuristic is a mental shortcut we often use to assess the frequency or probability of an event based on the most recent and readily available examples in our memory. This can lead to errors in our judgment, as there may be biases in the examples we have in mind.

For example, if a person has just heard about a plane crash on TV, they may feel more concerned about their safety on their next flight, as this event is fresh in their memory and readily available. However, this person may underestimate the safety of aviation in general, as air accidents are actually very rare.

It's important to remember that the availability heuristic can lead to errors in our judgment, and to look for more complete and reliable information to assess the frequency or probability of an event. It's also worth remembering that the media can influence our perception of events by highlighting certain stories over others. This can lead to an over-representation of rare or dramatic events, and give a false impression of their true frequency. So it's important to remember not to let ourselves be influenced solely by what we see or hear in the media.

In conclusion, the availability heuristic can be useful for making quick decisions in everyday situations, but it's important to view it with caution when it comes to making more important judgments or decisions with long-term consequences. It's better to look for complete and reliable information when assessing probabilities and risks, rather than relying solely on what's readily available in our memory.

## Conclusion

Through this section on Homo Sapiens, we can already see a number of avenues opening up. We will develop some of them in this conclusion, but this list is not exhaustive. We'll be developing many more in subsequent chapters, building on the foundation we've laid here.

We now know which human needs we need to satisfy in order to achieve total fulfillment. We have seen that it is possible to transform our extrinsic needs, and that it will be necessary to do so to make them compatible with a common project that will enable us all to fulfill our need for meaning.

This collective meaning must exist through the notion of the sacred. And there's nothing more sacred than what we all have in common, and whose survival is directly linked to us, i.e. our earth and its ecosystems. So science and ecology are no longer just capable of telling us what is true or false, but are also capable of dictating our values, what is right or wrong.

We have seen that there is no human nature to prevent us from succeeding in this project, not even an immovable self, and in this task the development of our consciousness will be our best ally. Equally important will be the education of new generations in the altruism and wonder of nature, in order to protect it.

Through the study of various cognitive effects and biases, we have gathered ever more information needed to understand ourselves and therefore also to develop tools that will take these parameters into account and make them effective in helping us in our common project.

## Sources

[^1]: [IPCC Report](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia: Pyramid of needs](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectives on Pshychological Science
[^4]: [Food and Agriculture Organization of the United Nations (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Figures and analysis on poor housing in France, including the number of homeless people and precarious living conditions](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Renunciation of healthcare for financial reasons in the Paris region:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [The Fondation de France publishes the : 13th edition of its study on loneliness](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap international: Disabled people remain among the most excluded in the world](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivation and Personality, 1970 (second edition)
[^10]: Arendt Hannah, The Condition of Modern Man, 1961
[^11]: Valérie Jousseaume, Plouc Pride: A new narrative for the countryside
[^12]: Emmanuel Todd, The Origin of Family Systems, 2011 - Where do we stand? A sketch of human history, 2017 - Class struggle in France in the 21st century, 2020.
[^13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997
[^14]: Jean Baudrillard, Consumer Society, 1970
[^15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994
[^16]: Charles Darwin, The Origin of Species: Chapter IV / III, 1859 & The Descent of Man and Sexual Selection: Chapter VII, 1871
[^17]: Hannah Arendt, The Origins of Totalitarianism, 1951
[^18]: Albert Camus, The Plague, 1947
[^19]: Jean-Paul Sartre, Existentialism is a Humanism
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, L'homo Sociabilis : La réciprocité
[^21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, The human bug
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, The Primal Brain
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, L'altruisme : une énigme ?
[^29]: Matthieu Ricard, "Altruism: Gene or Education?"
[^30]: Kathryn Spink, "Mother Teresa: A life in the service of others"
[^31]: Wolfram Schultz, The Role of Dopamine in Learning and Memory, 2007
[^32]: Read, D., & Northoff, G, Neural correlates of impulsive and reflective decisions. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, The Brain and Pleasure
[^34]: David Eagleman, The Unconscious Brain
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 and Gloria Y. Yeh,3 A Systematic Review and Meta-analysis of the Effects of Meditation on Empathy, Compassion, and Prosocial Behaviors, 2018
[^36]: Sébastien Bohler, Where is the meaning?, 2020
[^37]: [Roy F. Baumeister, The Need for Meaning: A Psychological Perspective](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Eco-anxiety: analysis of a contemporary anguish
[^39]: Albert Camus, The Myth of Sisyphus, 1942
[^40]: [Survey "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, Irrational Man 2001
[^43]: Elliot Aronson, The theory of cognitive dissonance
[^44]: Elizabeth Kolbert, The 6th Extinction, 2015
[^45]: Robert Axelrod, The prisoner's dilemma
[^46]: Nancy Huston, The Fabulous Species
[^47]: Serge-Chritophe Kolm, Freedom and Happiness
[^48]: David M. Eagleman, Incognito, 2015
[^49]: William James, The Principles of Psychology, 1980
[^50]: [Wikipedia: Selection bias](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, The Illusion Of Memory
[^52]: David Dunning and Justin Kruger, Unskilled and unaware of it: How difficulties in recognizing one's own incompetence lead to inflated self-assessments, 1997
[^53]: [Festinger and Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones and Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Thinking, Fast and Slow, 2011
[^58]: [Asch, S. E., Forming impressions of personality. , 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. A constant error in psychological ratings, 1920
