---
title: Working together
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## What is the economy

Economics is the human activity that consists in the production, distribution, exchange and consumption of goods and services[^2].

But an economic system doesn't just produce goods and services, it also produces human beings and the relationships between them. The way in which society produces and consumes has a major influence on personalities, characters, knowledge, desires, happiness and types of interpersonal relationships.

We sometimes tend to judge society exclusively on the quantities of various goods and services its members have. But people value not only these goods and services, but also the feelings, attitudes and interpersonal relationships that accompany these transfers, and what they reveal about what it is to be human. These aspects are all too often neglected in economics[^3].

An economic system shapes and forms human beings and their relationships through their production and consumption activities. Society secretes the economic system, which in turn profoundly influences the nature and quality of relationships between people and their psyches.

In an exchange, the giver and receiver always receive more than they actually give to each other, because they also give each other the opportunity to enter into a human relationship.

We have become accustomed to following economic science as if it were a simple fact of nature that dictates what the world should be, without really questioning it deeply. We are impregnated with the world we live in, but once we understand what an economic system is, we become capable of imagining our world operating with other rules.

Something new is happening these days, because in addition to the misery of the world and the domination we have over each other, climate disruption will bring us to the end of our economic growth. [^4]

Every year, the media and climate research organizations give us a date. It's the date when scientists estimate that humanity has consumed more resources than the Earth can produce in the same period.

This figure is the result of experts in various fields taking stock of the resources we are taking from the planet and the rate at which these resources are being replenished.

This date is known as the "day of overshoot". For an economy to be sustainable, this date would have to be on December 31 and not before. In this case, resources can be renewed at the same rate as they are consumed.

In 2022, this date is estimated to be July 22[^1].

This date should help us to take a clearer look at our economic system, and become aware of its human foundations and its vices.

In particular, that continuing to promote an economic system that encourages our great primary reinforcers (= desire for sex, food, information) is without doubt the worst thing we can do.[^5]

Economic theories fail to put forward arguments that really win intellectual support, which demonstrates their weakness. This is because they are often based on our collective imagination and have no real foundation.

So perhaps economics is more philosophical than it likes to let on, and less scientific than it likes to let on.

Without questioning the economic model, our societies, buoyed by the imagination of green capitalism, are engaged in an ecological transition. But history shows that capitalism's innovation does nothing to bring about an energy transition; on the contrary, it merely accumulates the exploitation of energy and raw materials, so that this transition only accelerates the depletion of resources and hence climate disruption. [^37] The system's quest for profit leads it to develop ever more ways of increasing its productivity. The rise of artificial intelligence and robots is not only accelerating the depletion of resources, but these AIs and robots, which will soon be able to completely replace humans in their tasks, are disrupting our economic models and forcing us to question our relationship to work and the place of humans in society.
The direction we're taking is leading us straight into a dystopia, but the warning signs of reality are forcing us to open our eyes. These observations lead us to the possibility, indeed the necessity, of collectively choosing a new economic model capable of bringing about a genuine ecological transition.

And if we're going to change, why not take advantage of the opportunity from the outset to choose one that not only meets the need for sustainability, but also responds to all other human needs and aspirations?

## The plan and the market

There are 2 economic systems that dominate the modern world: the market and planning[^6].

Any political ideology, in terms of economics, wishes to promote one or other of these 2 economic systems, or wishes to compromise between the two.

Each side, on the other side of this duality, defends its system with one social ethic and attacks the other for the realization of another.

For the market, this value is freedom, and for the plan, this value is equality.

But in reality, in systems that use the market, we are very far from being truly free, but we are much freer than in the plan. On the other hand, we're the worst in 2 systems when it comes to equality.

And in the plan, we are very far from being truly equal, but we are much more so than in the market, on the other hand we are the worst of 2 systems when it comes to freedom.

In fact, the current reality is that each of these systems performs badly in the field of the value they defend, but it's true, better than the other for the value defended and much worse according to the other ethical criterion.

At their most extreme, these 2 economic systems have become the capitalist market and totalitarian communism, both of which engender and reinforce nauseating and degrading human traits and relationships, such as egoism, the treatment of others as things, hostility, conflict, competition between people, domination, exploitation, alienation...

Neither of these 2 economic systems has succeeded in exalting human's finest qualities. They even strongly repress some of them. In both, a certain form of domination reigns.

## 3rd Economy: Reciprocity

So is there another alternative?

There is a literature that begins with "There are three types of economic system". But this literature must be sought at the historical or disciplinary margins of economic thought, either among economists of several generations ago, or in economic anthropology.

Such is the case of the economist Karl Polanyi (1886-1964), whose works classify the economic systems of all societies, including past, primitive and traditional ones, into three categories. And he notes their possible coexistence in the same society, specifying however that each assigns a privileged/dominant/principal place to one of them. He first distinguishes one economic system, the market. He calls his second system the system of redistribution, which in its elaborate, modern form is planning, where a political center decides on economic allocation. Polanyi calls the third system "Reciprocity", according to a well-established anthropological tradition, which is even quite clear and precise. [^6]

We don't even know that this type of economy exists, yet it does exist within our societies. Virtually all societies contain all three economic systems, but in all of them, one system is more important than the other two.

Here's a triangle corresponding to different major systems in 1984:
[![Diagram of the 3 types of economy](./../../../GlobalProject/Image/Marché.png)]

Economists built the economy and its thinking on the assumption that human was and always has been what we would call "homo oeconomicus", and they created a caricature of him as a selfish, calculating, asocial being. But human beings were above all what we would call "homo sociabilis", universally practicing reciprocity and universally needing a circle of close friends or a community[^9].

Reciprocity is the mechanism of the community economy that nurtured humanity's beginnings, as hunter-gatherers and then as sedentary farmers.

Reciprocity is always with us, and intra-family economic life is primarily based on this economy. You offer each other services without expecting anything in return, you don't pay family members to render you services, nor are they forced to do so by an outside entity. Reciprocity in our societies has drastically diminished over the last few decades, due to the effect of the market, which seeks to monetize social interactions more and more, precisely to create new markets. But also due to the decline of religion, which used to create numerous communities and actively promote reciprocity. In addition to the family, however, we can still find a great deal of reciprocity in friendly/emotional/love relationships, through volunteering, communities, but also in various fields, such as in the world of IT with opensource and through numerous platforms for sharing information, achievements and so on.

A long series of reciprocal, regular, ongoing and established gifts between two people is what we call reciprocity. General reciprocity is the equivalent, but from everyone. Everyone gives to society, and society receives from everyone else.

Altruism and giving, by their very nature, cannot be obtained either by coercion or by exchange in the strict sense.

This is the difference with the plan: these transfers are not coerced and imposed on the giver by a social entity outside him. And the difference with the market is that they are not exchanges in the strict sense either, where each transactor is exclusively interested in having more, or not having less, of the goods or services they transfer.

In this social system, no one gives to another specifically, so it's impossible to dominate, coerce, humiliate or force a counter-gift, and to exploit economically or ethically in this way. Everyone giving to everyone gives to no one.

In this system, each person's work benefits the consumption of many others, often much later. And everyone benefits from the work of many others, often much earlier. Each receives even in part from generations now gone, and works for generations yet to be born.

The social ethical value of reciprocity could be called "fraternity". Here we have the famous republican trilogy: fraternity is defined as "Do not do to others what you would not want done to you; constantly do to others the good you would want to receive from them.

But the comparison with the trilogy ends there, because reciprocity erases the very desire and meaning of the other two values. For in this system, where everyone cares for each other as if they cared for themselves, wills, and therefore personal freedoms, no longer clash, and demands for equality no longer have any basis.

## Advantage of reciprocity

Reciprocity is based on kindness.

Kindness means emphasizing altruism, love, voluntary solidarity, reciprocal giving, generosity, sharing, free community, love of neighbor and charity, benevolence and friendship, sympathy and compassion. We all lack them, and feel that we need more of them. All our great families of spirit, thought and feeling lay claim to them. But they are denied in our economic life. They dominate the existence of our societies, but the most widespread of aspirations is the rarest of achievements.

General reciprocity is the reconciliation between our altruistic ethics and our scientific technique, between our aspirations for warm community, individual freedom and material progress; it enables the duration of cultural meanings and the beauties of the world in the expansion of our knowledge, abilities and consciousness.

What's more, reciprocity brings with it a host of advantages for economic production. For it offers a coherent, efficient system that will drastically reduce overall working time.

## Is reciprocity possible?

While we've studied markets and planning extensively, we've said almost nothing about the third mode of transferring goods and services - giving - and nothing at all about the group of gifts known as reciprocity.

In the course of history, new economic systems have appeared, and each time there have been people, perhaps even the majority, to think and claim that the possible systems are those that can be observed at the time or that have existed in the past. But these beliefs are constantly contradicted by history, by the existence of new beliefs. And this is certainly still the case today, as there is no proof that any of the systems that exist or have existed is the best possible. Indeed, yes, economic systems that have never been observed are possible.

The flaw in our system is that it prevents individuals from individually choosing giving behavior: a firm that prices too favorably goes bankrupt, an individual can't give away what he produces in a society where nobody else gives him anything.

Psychosociologists have long studied "helping behavior", with some very interesting results: we don't just tend to help those who have helped us, but we tend to help more if we have been helped ourselves, i.e. we tend to help strangers if we have been helped ourselves by strangers. Which is the essence of reciprocity. [^13]

And this is what we saw in chapter 3 on Homo Sapiens, that it was possible to create altruistic beings, because altruism is also motivated by pleasure, and that it is mainly the environment and education that contribute to developing values and motivations such as generosity, compassion and the desire to serve others.

But is it even possible for a society to have an economy based on reciprocity, and in particular general reciprocity?

There are entire societies in which all economic relations are based essentially or even exclusively on reciprocity. But these sub-societies or societies are what we call "face-to-face groups", whose members know each other - families, villages, gangs, etc. - and reciprocity is not essentially general. It was still easy to apply it here, but one person can't know more than a hundred others, so we can legitimately ask whether it's really possible on a large scale.[^14].

For although the economy solves the problem of producing and distributing goods and services, every economic system, including reciprocity, is confronted with two problems fundamental to production: the problem of information and the problem of motivation.

## The information problem

Information is the worker's or the resource or service provider's knowledge of what he needs to do to best satisfy his needs and desires.

And motivation is what drives people to do the work or use the resources in this way.

Plan and market solve these two problems more or less well.

The market solves the information problem through the decentralized mechanisms of the "scarcity and utility indicator" price system: supply and demand.

The plan solves it by organizing this information, i.e. by collecting, calculating, issuing instructions or forecasts, in a relatively centralized way.

The market solves the problem of motivation, by reducing egoism through the incentive of exchange.

The plan solves the problem through constraint, threat or punishment, reward or promotion, but more generally through authority and its various means of constraint or reward, and more or less strong internalization of the need to obey out of habit or a sense of duty.

The same problems apply to reciprocity: how will people know what others need from them, and will they want to give enough to others?

These two problems are mutually reinforcing in a positive way: solving one favors solving the other, and vice versa.

The better the motivation problem is solved, the better the information problem can be solved, as people can more easily pass on the necessary information to each other.

The more the information problem is solved, the more productive the economic system is, and therefore the less wasteful it is, the less work is needed for a given final service, and therefore the less work the incentive system needs to provide, or the less serious its imperfection.

Reciprocally, everyone considers the needs or desires of others as important as their own, and this determines what they give of their labor (time, effort, trouble) and possessions, whatever their origin.

If the problem of motivation is solved, then so is the problem of information, because people can always make the economy at least as efficient as with the market or the plan, by voluntarily transmitting to each other the economic information that circulates in these systems.

Computers have truly revolutionized the information problem. Thanks to the possibility of connecting everyone non-physically, and the development of suitable applications for transferring information, which is now easier than ever before. It's also this innovation that's finally opening up the possibility of seeing the reciprocity economy move from small groups and local communities to a global scale. We can already see the rise of this emergence through the creation of numerous community platforms for reciprocal exchange and sharing, such as couchsurfing, workaway, wikipedia, etc...

There are already large multinationals with thousands of employees, who speak several different languages, and who manage to cooperate internally, to create products that are sometimes complex, touching on multiple fields and requiring multiple skills. Even if we have to change the pyramidal organization of domination that exists there, we can see that our information systems are already very efficient. For the moment, the problem of motivation is largely solved by the salary people receive, but if we manage to solve the problem of motivation by another means, then there's nothing to stop people passing on information to each other at least as efficiently as they do at present.

Transmitting and processing information is part of the job for society, alongside other work. In return, motivation drives us to do what's best for the community. This system can therefore do everything that the market or the plan can do.

What's more, reciprocity considerably reduces the need for information. Through the reduction of competitive or conspicuous consumption, the reduction of extrinsic needs, and the corresponding reduction in the production of goods or services, the reduction of the need to renew products through their design based on sustainability, and the reduction of many services which will no longer be necessary to the functioning of the economy.

## The problem of motivation

When we speak of "human nature", we may hear "man will never change, he has always been selfish, etc.", and then come the ideas about "true" human nature that should ultimately prevail. Historical, anthropological and psychological knowledge, analysis and even simple logic must absolutely be opposed to these naïve convictions: "man will never change" can just as easily be answered by saying that he has only changed[^15][^16][^17].

To "man has always been and is everywhere selfish" we can just as easily retort by asking ourselves what we know about what he "has always been".

Humanity is embarked on a transformation unprecedented in its existence by its depth, and, of course, by its scale.

As we saw in chapter 3 on "Homo Sapiens", there is no such thing as human nature, and humans can become whatever they choose to become.

Here are a few points to answer any doubts about the problem of motivation:

- People have a preference for altruistic and giving relationships[^18].
- All the major religious and secular ethics advocate altruism and giving very strongly. [^20]
- The main human motivation is to receive approval from other humans: in a reciprocal regime, approval is naturally universal and reciprocal[^19].
- Empirical analyses of "helping behavior" prove that people are much more likely to help others if they have been helped themselves.
- Sociologists have argued that the main characteristic of human beings is imitation[^22].
- Habit, that imitation of oneself, is another human phenomenon of prime importance[^23].
- Studies of children's development show that we are initially altruistic, then society makes us selfish[^21].
- Education: as we have seen, the human being is nothing more than a page on which society writes[^24].
- Social reinforcement phenomenon: the more people have a reciprocal mindset, the more they show by their actions and express by their words that it's normal and good to have it, and this encourages others to follow suit. Education and social relations as a whole thus play a major role. If man is first and foremost cultural, and thus malleable, society can make him as much the person of reciprocity as the egoist of the market, or the submissive or hierarch of the plan[^25].
- The more reciprocity there is, the more we see that it's viable and the more we realize its advantages, and therefore the more we accept it, the more we "believe in it", the more we want it, the more we push for it. And so the more it tends to spread.

- In the event that some people who are able to do so do not wish to participate, this is not a serious matter, because if such cases do exist, it is not contempt that should be shown for them, as such a feeling would signify a malaise in society: the person concerned would not fit in to the point of not wanting to participate, so help would have to be put in place, but even in the case where the person didn't want to participate, it wouldn't matter, because not only would we find that the workload demanded per inhabitant would be considerably reduced, but also that this could be counterbalanced by the fact that others would be happy to give "more" than they receive, out of altruism as we've seen, but also out of their own interest in the work.

After all, in today's society, why work at all?

- Direct coercion: forced labor
- Exchange: selling one's work for a wage
- Out of a desire to help others or the community, to "be of service".
- Out of a sense of duty, perhaps to give back to others for what we receive from them
- Out of interest in the work itself.
- To find social contacts and relationships, to integrate into society by contributing to the workplace.
- To show others that you work for them, or that you do beautiful things, or that you're skilled.
- For the social status attached to the work (contribution, responsibility, etc.).
- To keep busy, to have "something to do", to avoid boredom or idleness.
- For play
- Out of habit

Others could be added, but here we can clearly see the importance of occupation itself, and of the social relationships that work enables us to create, as can be seen in the feelings of loneliness and loss of meaning that follow "retirements". As a general rule, people derive more meaning from their productive activity than from their consumer activity. And all these motivations, with the exception of the first two, can encourage people to work in a system of reciprocity, so all we need to do is play on them by increasing the meaning and interest of work.

Work is not condemned to be the torture of its disputed etymological origin, from the word travalhum: tripalium, a torture instrument with a tripod...[^26].

All dimensions of the drudgery of the task must therefore be erased or reduced. Work that is physically or psychologically exhausting, stultifying or mind-numbing, too hard, mechanical, monotonous, too dirty or unpleasant, too devoid of meaning, despicable, etc., must be eliminated, replaced or made less tiring. Must be removed, replaced, restricted, automated, improved, enriched, compensated, rewarded, valued or better shared.

This sharing can be established collectively thanks to the development of tools such as [Ekklesi.app](https://ekklesi.app/), our modular platform for collaborative and democratic decision-making. In addition to enabling organizations to function horizontally, these tools are based on collective intelligence, so that everyone is valued for what they have to contribute. This enhancement of everyone's full potential considerably increases their well-being and commitment, and therefore their motivation.

The very idea is to transform the nature of work so that it becomes an activity that is something like self-realization, the manifestation of one's profound humanity through the transformation and preservation of nature. In this way, work would become the "first need of life". So the expression "working for a living" would no longer mean enduring the pain of work to survive and support oneself, but actually earning a living through an activity rich in meaning and experience.

## Measuring the economy, GNH and HEI

In 2010, Tim Jackson, Director of Research at the Centre for Environmental Strategy at the University of Surrey, published Prosperity without Growth. In particular, he looks at the relationship between countries' GDP levels and trends in the factors that guarantee prosperity (education, health, life expectancy, etc.). In today's economy, GDP growth decouples fairly quickly from prosperity growth, and even, beyond a certain GDP level, prosperity tends to decline. He thus confirms with curves what thinkers such as Ivan Illich had already been predicting since the 1970s[^32].

As far as the climate is concerned, we may already be technically in the early stages of a clean revolution: carbon intensities have already fallen by an average of 0.7% per year since 1990. But this has not prevented emissions from rising by 40%. Because at the same time, trade and production have increased. This mechanism is called the rebound effect: as soon as we become more efficient somewhere, we use the gains made to produce and consume more[^33].

GDP is therefore no measure of whether we are indeed enabling all current human beings and the 80 million generations to come to enjoy a fulfilling life on Earth. We therefore need to replace GDP with new indicators, such as BIB and IHE.

- GNH: Gross Domestic Happiness[^34].
- HEI: Human Environmental Impact

The aim is to have the highest Gross Domestic Happiness and the lowest, or even zero, Human Environmental Impact.

These two indicators are linked: there is a social floor that constitutes the goal to be reached to ensure the fulfillment of each and every individual, but this fulfillment cannot be achieved beyond an outer circle, which is the environmental ceiling.

Equilibrium lies within this ceiling, the safe and just space that enables humanity to keep moving forward.

For further information, see the chapter ["Indicators of a good economy"](https://anotherpossible.world/en/2-Story/Part2-AnotherPossibleWorld/10IndicatorOfAGoodEconomy).

## Symbiotic production mode

To keep this balance, there's a jigsaw puzzle for which when the pieces are properly combined, they reveal the image, and the logic, of an alternative, symbiotic and regenerative economy. From agro-ecology and permaculture, ecological engineering and the construction of horizontal governance in line with the principle of subsidiarity, to the circular economy, the manufacture of modular goods, the economy of functionality that favors the use and sharing of goods over their appropriation, the increase in our computing capacities through decentralized networking of our existing machines, etc., we can see a new logic, both social and economic, taking shape.

The symbiotic economy is an economy that regenerates its resources. The more mankind produces according to its principles in the living, technological and social spheres, the more it is able to regenerate its resources and probably reach a point where it not only becomes a co-creator of planetary equilibrium instead of its gravedigger, but also manages to create more resources than it consumes.

Find out more in the chapter ["Production methods"](https://anotherpossible.world/en/2-Story/Part2-AnotherPossibleWorld/19ModeOfProduction).

## The place of money in reciprocity

We're so used to it that we all believe that money is a necessary condition of life in society, yet money is not a fact of nature, but an imaginary invention of mankind that can be criticized.[^30].

The way money works doesn't explain its origin, it doesn't explain itself, and that's why economists don't explain anything. Nor do they explain themselves for existential reasons.

Money was supposedly invented to solve the problem of double coincidence of needs posed by bartering, because without money, it was too difficult to complete exchanges. How do you compare goods, and what if I need your shoes, but you don't need my apples?

Despite a great deal of anthropological research to the contrary, this myth is one of the most deeply rooted in people's understanding of the economy.

In primitive economies, people simply took what they needed, and socially, they shared their resources. Ignoring the obsession with scarcity that characterizes our market economies, primitive economies were able to bank systematically on abundance[^7][^8].

Some anthropological research, based more closely on historical elements and empirical discoveries, leads us to believe that money comes neither from work nor from exchange, but is first and foremost historically and conceptually a way of creating a social bond between individuals, as it represents the measure of debts contracted between them[^12].

To understand the difference with barter, here's a simplified example: A farmer who has an abundance of wheat, but needs vegetables for his family, asks his neighbor who has an abundance of vegetables, but the neighbor doesn't need wheat. So he writes a promise on a clay tablet that he will give some later, so that his neighbor can return with the tablet the day he needs wheat. So they didn't directly barter their products, but used "money" to measure the debts contracted between them.

This could be seen as deferred bartering, but the nuance is there. This was probably the first step, which later led to the first primitive forms of money, in the form of livestock, shells, precious stones, pearls, animal skins etc.

Some civilizations, like the Incas, knew neither money, nor markets, nor trade. [^10]

The problem with money is that, as long as we need it to access the exchange of goods and services, capital can play with us.

So it's not by having its hands on labor that capitalism assures its domination, but by mastering the conditions of sociality that make labor necessary, and that include, among other things, the necessity of money[^3].

Money marks men's trust in each other. For it takes unshakeable trust to accept a monetary sign from another in exchange for a service rendered or a good supplied: trust in the fact that it is not a forged bill or counterfeit money, trust in the future that this monetary sign represents insofar as it makes future projects possible. Ultimately, money is the horizon of reciprocity through time, not necessarily towards the person who gave me the money, but trust in the fact that in the future there will be someone there who can return to me in the form of goods and services the value of the goods and services I have temporarily sold for money.

For why use a monetary sign if I really trust the other person? The very object, the very concept of money, actually reflects a lack of trust in the other person, whereas if this trust is established, its usefulness disappears.

If the other really trusts me and society, he can give me something without asking me for money in return, because he knows that tomorrow it will be me or someone else who will give him something.

The exchange relationship, mediated by money, is a relationship based on the idea that the participants cannot trust in the duration of their relationship: in fact, they use money to complete the exchange, to finalize the debt, in short, to have nothing more to do with each other once the transaction has been completed.

The current political discussion in our economies is about how to use this money, so that it functions properly, but perhaps money simply can't function properly, perhaps it's doomed to pervert social relations?

Nevertheless, if we had to give it a nature, it would be better to say that money is doomed to pervert social relations in its current form as a medium of exchange, rather than as a "facilitator of the circularization of goods and services".

Money fills all the gaps left by the absence of meaning, it keeps us in control to calm our cingulate cortex, and the fear of death becomes the driving force behind avarice[^11].

Many analyses note the negative effects of the influence of money in society, but fail to propose an adequate response to the problem of the monetization of reality.

In a monetized society, the value of things becomes what money gives them.

Abolishing money seems impossible, so natural has the idea of money become to us. Indeed, the monetization of society increasingly seems to him to be a fact outside of which it is not even possible to think.

The problem is that it is no longer the individual who determines his or her value system, and therefore what he or she aspires to, but that this value system is made objective by money.

Yet by distancing ourselves from money, we can regain a sense of freedom from market values, by reclaiming our own values.

But if we continue to think that money in its present form is the only way to resolve the question of the exchange of goods, and therefore the question of encounters between individuals - in other words, if we believe that money is a prerequisite for sociality - we will find it very difficult to distinguish economic value from ethical and political values.

It is our Western societies that have, very recently, turned the human being into an "economic animal". But not all of us are yet such beings. Homo oeconomicus is not behind us, it's in front of us; like the man of morality and duty; like the man of science and reason. For a very long time, man was something else; and it wasn't long ago that he became a complicated machine equipped with a calculating machine.

Here, we seek to give value to facets of humanity other than the economic relationship of exchanging goods.

Value that enables everyone to flourish in a permanent encounter with others, within a sociality based on the principles of sharing and mutual aid.

> "Some men see things as they are and ask 'Why? I dream of things that never were and ask "Why not?". George Benard Shaw

## Conclusion

The economy of reciprocity, although one of the driving forces behind this first eutopic narrative promoted by the Eutopia Foundation, does not wish to be seen as a new ideology carried by this project. We leave it to those who wish to develop its theories and history, and make it their own.

We believe that this economy has been too little studied, and we'd like to develop a vision of it, because we believe that it has the capacity to give rise to innovative projects. This vision also aims to demonstrate that there are many alternatives to the great ideologies currently on offer in politics. We believe that the wisest thing to do is not to lock yourself into one, but to remain continually open to new ideas and new challenges. That's why this story is not set in stone, but is intended to evolve and be updated collaboratively and democratically throughout the history of humanity, by and for itself..

Indeed, to build a genuine transition to a better society, we believe it's pointless to create fixed ideologies that don't fully belong to those who defend them, and that it's pointless for ideologies that defend the same values to come into conflict with each other through disagreement over the means to a common end. Instead, we prefer to maintain friendly relations, create openness and support each other's projects...

We think it's much more productive to let a multitude of alternatives emerge, and not to spend energy and time trying to extinguish the others, because what needs to be extinguished will extinguish itself when there's no one left to believe in it. Through Eutopia, we don't want to try to convince people head-on at all costs. We believe that this only disturbs others in their reflections, and risks creating an inward-looking attitude on the part of each person and their ideas. We believe that it is only by remaining together, despite our diverse opinions, but open and united in our common quest for self-fulfilment, that we can emerge from the abyss in which we find ourselves.

Without seeking confrontation, to remove any possible doubt, we think it's still essential to draw parallels with the dominant ideologies and explain how the reciprocity economy presented here differs from them.

So we're not trying to reject these ideologies, because the values behind the left: "equality", and the right: "freedom" are beautiful and necessary to a Eutopia, but we think it's now a matter of putting the good of them together.

So we're not just going to say "You're all wrong", we're going to say "You're all partly right, let's combine our thoughts and ideas and go further".

Plans for a transition will be discussed in part 3, "Building the new world", but we want to make it clear in advance that we're not at all looking to move overnight to a cashless society of reciprocity. Through this narrative and analysis, we seek to demonstrate that this is a possible horizon. On the other hand, we do believe that gradually increasing the share of reciprocity in our societies can only be a good thing, and we are committed to contributing to this task through a multitude of innovative projects. We need to gradually change our habits, our beliefs, raise our consciousness, rethink our economic indicators, transform our modes of production to make them symbiotic, and as far as the economic model itself is concerned, we need to go through various intermediate stages to maintain a balance and coherence with the progress made in the other stages carried out in parallel.

### Comparison with the left

Those ideological systems that advocate more planning are the ones that might most closely resemble general reciprocity, yet a few words are enough to totally alter the practical system that emerges.

Socialism = "From each according to his abilities and to each according to his work".
Communism = "From each according to his abilities and to each according to his needs"[^27].
Reciprocity = "From each, voluntarily, according to the needs of others"[^3]

These ideologies could be seen as having shortcomings and defects that are in part the essential causes of what the world is today. They have given rise to deviations, to anti-capitalist revolutions, but through these they have more or less failed to create new types of society in large parts of the world. And since the result was no better than developed capitalism, they protected it. They are now used by all sides to make up the biggest lies and hypocrisies in history.

Equality between people, however defined, can be a limit to some people's freedom. And freedom alone does not usually guarantee equality.

The best of societies is probably very different from the ones we know, such as our society of dominant and dominated social classes. But if those who have tried to reinvent it fail, we think it's first and foremost because they don't specify enough the societies they propose.

If this analytical approach was necessary, we now need to go beyond it to continue the work of genetic denaturalization undertaken by Karl Marx in particular[^28].

The Marxian system is unable to think about money either before or after capitalism; in addition to casting two thousand years of human history into the shadows, it cannot propose any way out.

The difficulties of Marx's thought lie in the possibility of thinking concretely of another form of organization of work and economic sociality than that of the capitalist mode of production.

So, at the age of the noosphere, Marxist movements, dragging with them theories from another era, have great difficulty in reimagining self-managed horizontal work organizations, in reinventing our democratic processes, in imagining a world other than under productivism, in understanding the profound being we are and the means to transform it. All too often, then, they fight AGAINST capitalism, and find it hard to come up with genuine alternative solutions for which we could fight FOR.

We're not criticizing Marx's masterly work here, which was much needed for reflection, but we think we need to go further, and we do think that if he had sincerely admitted that he was also into ethics, and had placed much more of it in his works, if he had said more about the final or good society, and the means to achieve it, if he had put as much altruism into his characteristics as he had in himself, perhaps less of the worst would have happened.[^3].

What's more, we don't think we should dwell too much on class struggle in social change, as these classes will eventually disappear on their own along the way. These classes are no longer as clearly distinguished as they used to be - there's no longer just the bourgeoisie and the proletarians, but they are widely divided, intertwined and have interrelated interests. These analyses don't take enough account of the psychological aspect of these classes to understand them; if we do, we realize that they are all driven by a quest for self-fulfillment, as detailed in Chapter 4 on HomoSapiens. And that it's just as difficult for everyone to get out of their behavior at the neurological level, even if the impact of our behaviors can have enormous differences between them. We therefore prefer to move away from this analysis, so as not to lock ourselves into a battle against each other, but rather to show the benefits of such a system for everyone. So we're not looking to fight an adversary, but rather to fight for something, and invite everyone to join in, demonstrating for ourselves that it leads to our fulfillment, not just in the end, but above all through the very means that bring us to that end.

### Comparison with the right

Right-wing policies are those that promote the market. These are the dominant ideologies currently in power in Western countries.

Everyone can see the negative effects, not only on the planet and our ecosystems, but also on our overall well-being. In this economic system, we spend our time wasting it, and we waste our lives "earning" it. And those who don't waste their time wasting it, whether undergone or not, still reap the disastrous consequences, and if not in the short term, then in the long term.

In the marketplace, people always have chains. But for the most part, they're no longer crude chains of rusty steel, they're "finely nickel-plated", much more "standing" and ultimately even harder to break, especially as some slip gold links into them. We then feel as if we have something precious to lose.

Our system traps each individual by all the others, in the sense that a person simply cannot, alone and freely, manifest his or her finest qualities. In economics, in a competitive system, a company cannot, on pain of bankruptcy, not seek to maximize its profit if others seek to maximize theirs. Nor can it produce for too long, at the risk of running out of buyers in the long term. Similarly, anyone who gives cannot survive in a society where others take.

So there is some good in this system, such as entrepreneurial freedom, and the "freedom" that is more important than under planning. Nor can we deny the progress made. But neither can we deny the consequences it has had, for where a few won in the market game, the majority could only lose... the wealth and material comfort of some, was not only the destruction of our environment, but also the misery of others... those who were exploited, neglected, forgotten... With this account, we can no longer deny that it is impossible to do better than capitalism in terms of freedom, comfort, innovation and fulfillment, while ensuring the realization of all other profound human aspirations while remaining in symbiosis with our environment.

This is where this first version of our analysis of the comparison with right-wing ideology ends. Not that there isn't much to say - on the contrary, there's an enormous amount of literature on capitalism, detailing at length its origins, causes and consequences. But how we look at these ideologies, and how we relate to them, has yet to be defined collectively. Nevertheless, if you feel you don't know enough about this subject, we suggest you consult the many resources available in the literature and on the Internet:[^29][^31].

## Sources

[^1]: Earth Overshoot day 2017 fell on August 2, Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipedia : Economy](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: The End of Growth. Richard Heinberg. 2013
[^5]: Sébastien Bohler. The Human Bug. 2020
[^6]: Karl Polanyi. "The great transformation" and "The economy as Instituted Process" in Trade and Market in the Early Empires: Economies in History and Theory.
[^7]: Marshall Sahlins - Stone-age Economics
[^8]: Marcel Mauss- Essai sur le don
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Inca social organization](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Where's the sense? 2020
[^12]: David Graeber. Debt: 5000 years of history. 2011
[^13]: Lee Alan Dugatkin. The Altruism Equation: Seven Scientists Search for the Origins of Goodness. 2006
[^14]: [Dunbar's Number](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: A Brief History of Humanity
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[^17]: Daniel Kahneman. Thinking, Fast and Slow
[^18]: [Ernst Fehr, The nature of human altruism](https://www.nature.com/articles/nature02043)
[^19]: [The Need for Social Approval](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religion and Morality](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruism in children and the perceived conduct of others in Journal of Abnormal and social Psychology, 1942, 37, p. 2018-233
[^22]: Gabriel Tarde. The laws of imitation, 1890
[^23]: Charles Duhigg. The Power of Habit. 2014
[^24]: Émile Durkheim. Education and Sociology; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORY OF NORMATIVE CONDUCT: A THEORETICAL REFINEMENT AND REEVALUATION OF THE ROLE OF NORMS IN HUMAN BEHAVIOR
[^26]: [Is the etymology of the word work tripalium?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Critique of the Gotha Program (1875). 1891
[^28]: Karl Marx. Capital. 1867
[^29]: [Conference: Communism & Capitalism: the history behind these ideologies](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. The order of money - Critique of economics. 2019
[^31]: [Arte 2014 documentary - Ilan Ziv - Capitalisme](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
[^32]: Tim Jackson. Prospérité sans croissance. 2009
[^33]: [Comprendre l'effet rebond](https://theothereconomy.com/fr/fiches/comprendre-leffet-rebond/)
[^34]: [Bonheur national brut](https://fr.wikipedia.org/wiki/Bonheur_national_brut)
[^35]: Hannah Arendt, Les origines du totalitarisme, 1951
[^36]: Albert Camus, La peste, 1947
[^37]: Jean-Baptiste Fressoz, Sans transition Une nouvelle histoire de l'énergie : La transition énergétique n'aura pas lieu, 2024
