---
title: Planet Earth
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> This page is still being edited, so some parts may be missing.
> {.is-info}

## Quick description of the Earth

### The right world map

[![Gall-Peters projection](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

The Peters projection (or Gall-Peters projection after James Gall (1808-1895) and Arno Peters (1916-2002)) is a cartographic projection which, unlike the Mercator projection, allows the real surface area of continents to be taken into account. But locally, this projection does not preserve angles, which results in the deformation of continents, unlike Mercator's map. [^1]

### Quick description of the different ecosystems

## The water cycle

### Presentation of the different forms of water on Earth

### Description of the water cycle and its interactions with the environment

### Consequences of human activities on the water cycle

## Photosynthesis and oxygen production

### Description of photosynthesis and its role in oxygen production

Ever since its origin, the Earth has been constantly creating information. Thanks to this, ordered movements of matter have been created, giving rise to an exceptional diversity of shapes, colors and movement: life as we know it.

Of all this available information, one has been more of a driving force than any other: the one encoded in the plant genes that drive the mechanisms of photosynthesis. This process, specific to the plant kingdom, makes the living system particularly efficient and has no equivalent. Photosynthesis has both energetic and chemical powers. It captures the raw, immaterial energy of light, stores it and redistributes it in extremely fine detail, on a molecular scale of less than a nanojoule. It also makes it possible to assemble dispersed mineral elements into materials and molecules, which themselves carry information in their interactions and energy in their composition.

This is a prodigious phenomenon in terms of the physical laws of energy. Whereas the latter dictate that energy emitted from a source should disperse and degrade, becoming less and less usable, plants are able to channel it. Thanks to the information contained in its genetic library, the plant thus goes against the physical laws of energy, known as entropy. Instead, it forms a negentropic loop within the universe: matter is ordered and organized, and energy is channeled. Before disintegrating again on its journey through life. Compared to the infinitely greater amount of matter in the universe, organic matter is like lace: it can take on any shape. It's this collection of molecules in the shape of hooks, pulleys, wheels, cables, doors, dots and chains that makes up living matter[^2].

### Consequences of human activities on photosynthesis and oxygen production

## The carbon cycle

### Presentation of the carbon cycle and its interactions with the environment

### Consequences of human activities on the carbon cycle and greenhouse gases

What's more, the materials and energies that enable us to achieve such feats modify the global thermostat, which is composed mainly of carbon, and enrich the atmosphere with greenhouse gases when they are used. And the atmosphere heats up. The limestone that forms the basis of cements, plasters and limes is a carbonaceous rock; oil, coal and gas, which make up 80% of the energy sources we use or 100% of the bitumen we travel on, are carbonaceous rocks. This carbon was not present in the earth's initial crust, but was concentrated in the atmosphere. Its passage from the atmosphere to the earth's crust was achieved by the action of living beings, which absorbed it and deposited it at the bottom of the oceans or in the geological layers of land and lagoons. In just a few decades, we are releasing carbon that took living beings hundreds of millions of years to bury. We are destabilizing a system whose equilibrium is at the origin of most of today's evolved life forms, including our own[^3].

## Biodiversity

### Presentation of the importance of biodiversity for life on Earth.

### Consequences of human activity on biodiversity

More than half of the world's forests and wetlands have disappeared in the last century [^2].

## Soils

### Presentation of the importance of soils for life on Earth

### Consequences of human activities on soils

One third of the world's soils are degraded. [^4]

## Natural resources

### Presentation of the different natural resources (water, air, soil, etc.).

### Sustainable management of natural resources

### Sustainable alternatives for natural resources

## Terrestrial and marine ecosystems

### Presentation of the different terrestrial and marine ecosystems, and their importance

### Consequences of human activities on ecosystems

## Sources

[^1]: [Wikipedia: Peters projection](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, The symbiotic economy, Actes sud | Colibri, 2017
[^3]: [Food and Agriculture Organization of the United Nations (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Food and Agriculture Organization of the United Nations (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
