---
title: Imagining another world
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

So how do you imagine a new world, an ideal, Eutopia?

## The system, its constraints and incentives

If we currently seem to be at an impasse in the face of climate change and the other challenges of our time, it's because the foundations of our system generate constraints and incentives that end up preventing any effective change without disrupting the established coherence, which then drags our society into a destructive inertia that no one seems able to stop.

At every election, our politicians try to convince us of their proposals, and on the face of it, they all think they're doing the right thing, they think they've got the solution that could improve the overall daily lives of the population they're defending. But they are fighting among themselves with these constraints and incentives, and these cannot lead to a lasting and desirable solution. So, powerless in the face of the system, they have no choice but to let themselves be ruled by it.

Insofar as we ever really had it, we humans have lost our power, but worse still, those we elect to represent us have lost it too.

We think that the political system dominates and can change everything else, including the economy and its shape. That we're in a democracy because of the competition between the candidates we vote for.

But an economic system doesn't just produce goods and services, it defines the conditions of our sociality, producing human beings and the relationships between them. [^1]

We'll see that the economy even intervenes in this competitive democratic choice. It influences it and, as a result, the electoral system no longer has the quality of offering good competition between candidates. In the end, then, it is not politics that dominates economics, but economics that dominates politics. [^2]

What we're proposing here is a clean sweep of our system, a complete departure from everything that makes up our current collective imagination. We invite you to start from a new base, to discover a New World quite different from the one you know, a world based on meaning and logic, and then to work out the paths and solutions for a transition towards it together.

## Starting from the ground up

So what is the base?

The base is what's real, what existed at the very beginning of humanity's existence, when we couldn't yet tell ourselves imaginary stories. The base is all that's left once we've erased this collective human imagination.

So we have the Earth and everything that makes it up. In particular, its various ecosystems: an ecosystem is a community of living beings interacting with their environment, whether plant or animal. The planet is a vast system in which an infinite number of subsystems interact and dynamically balance each other. Among all the living beings on our planet, there's us, Homo Sapiens, more commonly known as Humans.

There are 7.9 billion of us humans, and it is predicted that there will be 11 billion by 2100[^3]. We are now everywhere on our planet, and even in sparsely populated areas, humans still have an impact on the environment and on ecosystems.[^4] However, the problem is not the size of the population, but the interactions we maintain within its ecosystems.

So, starting from the ground up means, first of all, understanding the ground up. We need to understand the world and how it works. And since we human beings live within it, we're going to have to understand how we work too. One part will be devoted exclusively to the earth, while another will be devoted to an in-depth understanding of the human being.

These 2 research questions are very broad and require a great deal of knowledge, but we don't need to go into too much detail: we'll concentrate on the essentials, gather them together and summarize them to answer the questions that follow.

## Research questions

Once the skills needed to understand the world and humans have been acquired, we'll need a goal in the form of a research question that establishes our wish for what we're looking for in an ideal world.

This research question is very important, as it will guide the research from which our solutions will flow. Different research questions will lead to different results. These should be as simple as possible, clear and precise, but should capture the essence of what we want.

We'll focus on our own species, but as mentioned above, we live in ecosystems, and we'll discover when we study the world that we're directly linked to them, and therefore dependent on them for our survival.

We seek the ideal. Of course, the ideal depends on the values, beliefs, aspirations and experiences of each individual. We might therefore hope that the ideal for each individual is to live a fulfilling and balanced life, one that meets their needs and aspirations.

That's what we're aiming for, for every human being to have a fulfilling life during their time on Earth. But remember, there are still 80 million generations to come, so this research applies to current generations, but also takes into account all those to come.

With these various elements in mind, here's the formulation of our research question:

> How can we enable all current humans and the 80 million generations to come to enjoy a fulfilling life on Earth?
> {.is-success}

Our great collective project is therefore to ensure that all human beings have a fulfilling life on Earth that meets their needs and aspirations, for the present generations and for the 80 million more to come.

Once we've fully understood how the world and human beings work, we'll have discovered how human beings can be fulfilled, how we can give meaning to their existence, and how they can live in their environment on Earth for the long term.

To complete this research, we'll quickly retrace the different eras of humanity, because knowing the past provides the foundation and inspiration for interpreting the present.[^5] And we need to know the present to determine a possible future transition. For the best possible society is the best possible evolution of societies from now on. [^1]

In order to reach this common goal, our 7.7 billion human beings will have to perform 2 essential actions: collaborate and decide.

## Sub-question: Collaboration

As in any society, we're going to have to work together to provide the goods and services we need to thrive. Our first major sub-question will therefore be:

> How can we enable humans to work together to provide each other with the goods and services we need to achieve this common goal?
> {.is-success}

This is the field of economics. It's one of our system's biggest problems, and the one from which the most constraints and incentives emanate. Starting from scratch and then looking for the best possible economy will enable us to define the necessary foundations for an ideal possible society, avoiding the constraints and incentives that conflict with its realization. As we shall discover, an economic system not only produces goods and services, it also produces human beings and the relationships between them, which are just as essential to their fulfillment.

## Sub-question: Decisions

Finally, our 7.7 billion human beings, throughout their lives and the evolution of their society, will have to make choices, reach agreements and take decisions on both a small and large scale. This is our second major sub-question:

> How can we make decisions together, and ensure that these decisions enable us to achieve our common goal?
> {.is-success}

As you can see from the question, the idea isn't just to reach agreement, because what's the point of having all or part of the population agree on a decision, and have it validated, if it doesn't ultimately help us reach our common goal.

This field is the science of decision making, and it applies to many practices, including politics, where we currently use representative democracy.

We'll be taking a closer look at the different possibilities, taking into account what we've learned about how humans and the world work.

## Conclusion

To sum up, we're going to start by understanding our habitat, the Earth, how it works, its ecosystems and our place within it. Then we're going to gain an in-depth understanding of human beings, how they function and what they need to thrive during their time on Earth.

This knowledge will enable us to respond effectively to the question of how to enable humans to make decisions and cooperate to achieve the common goal of sustainable fulfillment for all.

The functioning of the earth in chapter 2, the understanding of the human being in chapter 3, and the state of the art of human eras in chapter 4 will seem boring to read if you already know these fields or if you don't want to go into too much depth at first. In this case, we advise you to go straight to chapters 5 and 6, where you'll discover a new way of collaborating and making decisions. These 2 parts are essential, as they justify how the society we're about to imagine is possible by creating the foundations that will make it coherent. Nevertheless, chapters 2, 3 and 4 are no less important, leading to the realization and justification of the solutions in chapters 5 and 6. If you wish, summaries have been produced and [are available here](/en/4-Appendices/1Summaries)

> "Knowledge is power. The more we understand the world and ourselves, the more we have the ability to change it for the better." Malala Yousafzai

## Sources

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, Are elections democracy?/ 1977
[^3]: [World Population Prospects 2022" published by the United Nations](https://desapublications.un.org/file/989/download)
[^4]: [IPCC Reports 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Plouc Pride: A new narrative for the countryside. 2021
