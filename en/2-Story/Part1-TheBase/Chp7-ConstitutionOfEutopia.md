---
title: Constitution of Eutopia
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Content in the experimental stage and under development.
> {.is-danger}

Eutopia's constitution is divided into 3 statements:

- The Universal Declaration of Human Rights, which incorporates many of the fundamental principles of the current Universal Declaration of Human Rights[^1], but which we have adapted to add important nuances and extensions to better reflect our values such as sustainability, diversity, interdependence with nature, freedom, solidarity and equity.
- Universal Declaration of the Rights of Nature / Mother Earth, is a charter that was established on the initiative of the Amerindian peoples and formulated at the World Conference of Peoples against Climate Change in 2010[^2].
- Déclaration Universelle des Droits relationnels entre l'espèce humaine et les espèces animales non humaines, is a provisional and experimental charter, intended to establish more precisely our relations with other living animal species.

In order to keep the text as short as possible, the justifications for the various articles have been separated and placed at the end of this same page. These declarations are free to be modified by the citizens of Eutopia through the same democratic process described at the end of the previous chapter on how to decide together.

## Universal Declaration of Human Rights

### Preamble

Guided by the values of equality, freedom, solidarity and sustainability, we recognize the inherent dignity of each individual, a beacon that lights the way to a world where all are born equal in rights and freedom, and to our collective destiny.

We celebrate the infinite diversity that weaves the richness of our human tapestry, and we are united by the profound conviction that in this diversity lies our collective strength. It is in this spirit of unity and harmony that we enunciate this Declaration, affirming that every person has the right to freely express their thoughts, pursue their happiness and contribute to our common prosperity.

Our common quest is shaped by absolute respect for each other's integrity, by the relentless pursuit of fairness and justice, and by a sacred commitment to the preservation of our shared home, Mother Earth.

In writing these universal rights, we erect a powerful shield against oppression, injustice and oblivion. We declare that in every corner of Eutopia, the light of equality illuminates the dark corners of injustice, establishing a world where freedom, love and compassion are inalienable rights.

May this Universal Declaration of Human Rights inspire action, guide our decisions and unite our hearts in the perpetual construction of a society where all, hand in hand, march towards the dawn of a world where human dignity is the compass that guides our journey.

### Proposals and summaries

Article 1. Equality :

- Humans are born and remain free and equal in rights.
- Every citizen of Eutopia has the inalienable right to participate in the decision-making process on an equal footing with all others. No form of discrimination or privilege may impede the equal decision-making power of each individual.

Article 2. Freedom:

- Humans have total freedom to express themselves and to think, and no one can prevent the diversity of opinions and the free circulation of ideas.
- Freedom of belief is equally sacred. Every individual has the right to choose, practice and express his or her spiritual, religious or philosophical beliefs, without fear of persecution or discrimination.
- Human beings have the right to freely choose their clothing, to express themselves personally through clothing, without being subjected to judgment or restrictions imposed by third parties.
- Human beings have the fundamental right to move freely throughout the world, except for reasons of preservation of Nature or for reasons of security of human or material integrity, no frontier may limit this freedom of movement.
- Every individual is free to undertake, to realize, to create and to bring their creations and innovations to the world. Creativity, innovation and individual contribution to the common well-being of Eutopia are to be encouraged.

Article 3. Solidarity (Fraternity):

- Solidarity must guide the decisions taken by the members of Eutopia.

Article 3. Equity:

- Every decision must be designed so that everyone can benefit fully, eliminating barriers and inequalities.

Article 4. Sustainability:

- Preservation of the environment is paramount and a collective responsibility. Decisions must take into account the impact on nature, and ensure a sustainable balance between humanity and the planet.

Article 5. Integrity :

- Under no circumstances may a decision compromise the integrity of others, either physically or mentally. However, each individual remains sovereign and is the sole holder of the power to compromise his or her own integrity.

Article 7. Security :

- Every individual has the right to security, to protection against all physical or mental violence and to a safe environment in which to develop.
- No lethal weapon may be held or produced outside authorized research centers and persons.

Article 8. Trust :

- Sociality is based on collective trust, altruistic mutual aid and recognition of each other's needs. No service rendered can be asked for money from the receiver, nor can it be measured and stored in an imaginary value with a view to future exchange for the same or any other service. Everyone who gives to everyone gives to no one in particular.

Article 9. Diversity :

- Eutopia celebrates diversity in all its forms and is committed to creating an inclusive community, where each individual is respected and valued for their uniqueness.

## Universal Declaration of the Rights of Nature / Mother Earth

### Preamble

We, Eutopians of the Earth :

- Considering that we are all part of Mother Earth, an indivisible community of life composed of interdependent beings intimately linked by a common destiny;
- Gratefully acknowledging that Mother Earth is the source of life, sustenance and education, and that she provides us with everything we need to live well;
- Convinced that, in a community of life involving interdependent relationships, it is impossible to grant rights to human beings alone without causing imbalance within Mother Earth;
- Affirming that in order to guarantee human rights, it is necessary to recognize and defend the rights of Mother Earth and all the living beings that make her up, and that there are cultures, practices and laws that recognize and defend these rights;
- Proclaim this Universal Declaration of the Rights of Mother Earth, so that every person and every institution assumes responsibility for promoting, through teaching, education and the awakening of consciences, respect for the rights recognized in the Declaration, and for ensuring, through diligent and progressive measures and provisions of local and global scope, that they are universally and effectively recognized and applied by all Eutopians of the world.

### Proposals and summaries

Article 1. Mother Earth:

1. Mother Earth is a living being.
2. Mother Earth is a unique, indivisible, self-regulating community of interrelated beings that nourishes, contains and renews all beings.
3. Each being is defined by its relationships as a constituent element of Mother Earth.
4. Mother Earth's intrinsic rights are inalienable, since they derive from the same source as existence itself.
5. Mother Earth and all beings possess all the intrinsic rights recognized in the present Declaration, without any distinction between biological and non-biological beings, nor any distinction based on species, origin, utility for human beings or any other characteristic.
6. Just as human beings enjoy human rights, all other beings have rights specific to their species or type and adapted to the role and function they perform within the communities in which they exist.
7. The rights of each being are limited by those of other beings, and any conflict between their respective rights must be resolved in a way that preserves the integrity, balance and health of Mother Earth.

Article 2. Mother Earth's Inherent Rights :

1. Mother Earth and all her constituent beings possess the following intrinsic rights:
   - the right to live and exist ;
   - the right to respect ;
   - the right to the regeneration of their biocapacity and to the continuity of their vital cycles and processes, free from human disturbance;
   - the right to maintain their identity and integrity as distinct, self-regulating and interrelated beings;
   - the right to water as a source of life;
   - the right to clean air;
   - the right to full health;
   - the right to be free from contamination, pollution and toxic or radioactive waste;
   - the right not to be genetically modified or transformed in a way that harms their integrity or vital, healthy functioning;
   - the right to full and prompt reparation for any violation of the rights recognized in this Declaration resulting from human activities.
2. Each being has the right to occupy a place and play its role within Mother Earth, so that she may function harmoniously.
3. All beings have the right to well-being and to be free from torture or cruel treatment inflicted by human beings.

Article 3. Obligations of human beings towards Mother Earth :

1. Every human being has a duty to respect Mother Earth and to live in harmony with her.
2. Human beings have a duty :
   - a) to act in accordance with the rights and obligations recognized in the present Declaration ;
   - b) to recognize and promote the full implementation of the rights and obligations set forth in this Declaration
   - c) promote and participate in the learning, analysis, interpretation and communication of ways to live in harmony with Mother Earth, in accordance with this Declaration;
   - d) to ensure that the pursuit of human well-being contributes to the well-being of Mother Earth, now and in the future;
   - e) to establish and enforce effective laws and standards for the defense, protection and preservation of the rights of Mother Earth;
   - f) to respect, protect and preserve Mother Earth's vital ecological cycles, processes and balances and, where necessary, to restore their integrity;
   - g) to ensure that damages resulting from human violations of the intrinsic rights recognized in this Declaration are made good, and that those responsible are obliged to restore the integrity and health of Mother Earth;
   - h) to empower human beings and institutions to defend the rights of Mother Earth and of all beings;
   - i) to implement precautionary and restrictive measures to prevent human activities from leading to the extinction of species, the destruction of ecosystems or the disruption of ecological cycles;
   - j) guarantee peace and eliminate nuclear, chemical and biological weapons;
   - k) to promote and encourage practices that respect Mother Earth and all beings, in accordance with their own cultures, traditions and customs;
   - l) to promote economic systems which are in harmony with Mother Earth and consistent with the rights recognized in the present Declaration.

## Universal Declaration of Human Rights in Relation to Non-Human Animal Species

### Preamble

In this Universal Declaration of Relational Rights between the human species and Eutopia's non-human animal species, we lay the foundations for harmonious coexistence based on reciprocity and mutualism. Crucially, the principles set out here apply to any animal with which we choose to cooperate and share symbiotic interactions within Eutopia.

We make this deliberate distinction to preserve the freedom of animals as an integral part of the animal kingdom and their natural habitats, where they can evolve without human interference. In these preserved spaces, we recognize the inestimable value of wild nature and are committed to letting these ecosystems thrive in complete autonomy.

However, where humans reside and choose to build communities, we aspire to live in harmony with selected animal species, establishing relationships based on mutual understanding, respect and beneficial cooperation. Thus, this declaration applies to the animals we integrate into our ecosystem, for whom we become automatically responsible, demonstrating our commitment to a balanced and respectful symbiosis.

### Species rights under human responsibility

Article 1. Respect for animal dignity :

- All animals under the care of the inhabitants of Eutopia have the right to a dignified and cruelty-free life.
- Human interaction with animals must respect their intrinsic nature and natural behavior.

Article 2. Decent living conditions :

- Animals must be housed in conditions that respect their physical and psychological well-being.
- Facilities must offer adequate space, access to suitable food, clean water and optimal sanitary conditions.

Article 3. Access to care :

- Any animal under the care of the inhabitants of Eutopia has the right to regular veterinary care to ensure its health and well-being.
- Medical treatment must be provided in a humane, respectful and ethical manner.

Article 4. End of life :

- Animals intended for human consumption must be given a respectful end-of-life free from unnecessary suffering.

## Justification

### Universal Declaration of Human Rights

In progress...

### Universal Declaration of the Rights of Nature / Mother Earth

Currently being drafted...

### Universal Declaration of Human Rights in Relation to Non-Human Animal Species

Currently being drafted...

## Sources (Non-exhaustive list)

[^1]: [Universal Declaration of Human Rights. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Universal Declaration of the Rights of Mother Earth. 2012 ](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
