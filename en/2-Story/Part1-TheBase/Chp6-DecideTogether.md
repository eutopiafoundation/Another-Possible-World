---
title: Deciding together
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> This page is still being edited, so some parts may be missing.
> {.is-info}

## How can we decide together?

We need to understand how decisions can be made, and how to ensure that these decisions are the right ones to achieve our common goal.

Many decision-making tools exist, and many more have yet to be invented. We'll leave the smaller-scale precision tools to one side here, and concentrate on the global one.

In our Western society, the tool used is democracy. Analyzing and reinventing democracy will enable us to develop smaller-scale tools inspired by it, which can then be adapted to other modes of organization, such as companies, schools, etc...

## What is democracy

Democracy is the Greek combination of demos "the people" and kratos "power". Today, it refers to any political system in which the people are sovereign[^1].

Many countries have conquered the equality of political power through universal suffrage. This victory is now enshrined in their constitutions and is considered one of the most brilliant institutional jewels of their societies.

But "Democracy" is still a much-maligned and misused word.

We proclaim democracy, as if we really had the power.

But our democracy is merely a recurrent abdication of our legitimate power to the politicians who seize it.

And once they've got it, they then feel legitimate in having it, but even they are under the control of a much greater power.

We think that the political system dominates and can change everything else, including the economy and its shape. We think our system is perfectly democratic because of the competition between the candidates we vote for.

But as we shall see, economics plays a part in this competitive choice. So, on the one hand, the electoral system no longer has the quality of offering good competition between candidates, and on the other, it doesn't dominate the economy, since the latter influences it[^2].

So it's the economy that dominates politics.

On climate issues, for example, studies show that there is no correlation between what the majority of people want and what they get, except when they have the same preferences as the richest 10% of the population[^3].

Our democracy needs to be completely overhauled, and we're talking here about numerous flaws: abuse of power, corruption, the influence of lobbies, uncoordinated, unshared decisions, flaws in representativeness, illusions of choice, inequality of opportunity.

And that's when we discover that the real result of our democratic system is the opposite of its ideological aspiration.

Despite all these flaws, democracy is a risk worth taking[^4] if we want to preserve our freedom.

So let's begin by analyzing our democracy, understanding what's wrong with it, and then reinventing a completely different democratic system.

## The problem with our democracies

### The political entrepreneur[^5][^6][^7]

The goal of every politician is to be and remain in power. He makes a career of it, which in turn enables him to earn a living and find his place in society.

They are in competition with each other, but also in collusion: they want to destroy their political adversary, but not the political office.

All politicians, whatever their opposition, have a common interest in strengthening, not diminishing, the power of politicians. And if one of them suggests the idea, he will receive the disapproval of all the others.

The vast majority of political entrepreneurs come from the same social class, and generally also from the same class of private business leaders and bourgeois families.

Candidates do not seek to present the program that constitutes their ideal of the good society.

Whatever their personal ends, they choose the program that best suits the greatest number. They come to sacrifice the defense of their own opinions, ideals and personal preferences, if they have any, to those of what the mass of the population wants.

Nevertheless, this knowledge of voters' desires is for the most part not used to serve the greatest number, but as we shall see, it is used to exploit them for the benefit of the smallest.

Politicians simply react to the incentives that enable them to acquire and remain in power.

And there's no point in hating them: they're simply following the rules of a game. So it's the game that should be hated, not the players[^8].

But the problem with political entrepreneurs is that their aim is not to be right, but for the people and business to be right.

Political entrepreneurs aren't stupid, they're fine tacticians who react to the game's incentives and are very well advised by their team's expertise in the field, so they use various stratagems and alliances to achieve their ends.

In order to present a good image, candidates hide their personalities, disguise themselves and become hypocritical.

Voters think they know who they're voting for, because the emphasis is often placed on the candidate's personality, highlighting his or her seductiveness, reassuring character or charisma, creating a halo effect. And these impressions are much more easily manipulated by the media than the content of a program.

### Political programs[^2]

Candidates build their "programs" not all at once, but by successive additions or modifications of statements and actions, without however being able to go back much on, erase, or contradict previous words or deeds. This constraint can be explained by the time it takes to make a program known, whereas clarifying it as you go along, adding to it and reinforcing it, reaches voters more effectively.

It is also explained by the fact that such contradictions or denials would undermine the candidate's reliability and credibility, creating an evolving, iterative structure of knowledge and program construction.

This often leads to differences in their programs, and imperfections that hamper the democratic quality of the outcome of the process.

On the other hand, programs are global, covering all subjects at once, and you have to choose between them, although you may well prefer one on one point and another on another.

In this case, the programs come to divide the population: one of them could be elected by a majority, while that same majority prefers one point of all the other programs to that of the winner.

This is one of the benefits of specific referendums: to express your opinion on each question separately.

The effect of self-interested financial contributions is to standardize programs, while the effect of idealistic/activist support is to differentiate them.

### Honesty and transparency

Any system that can be manipulated will be manipulated at the worst possible moment[^9].

When designing a system, it's essential to consider how the system's stakeholders will be able to attack it and take dishonest advantage of the system.

Honesty and transparency are persistent problems in democratic systems around the world. Politicians and elected representatives are often accused of lacking transparency, of being corrupt, and of finding themselves in the middle of numerous conflicts of interest by favoring particular interests over the general interest.

In our system, certain rules do not promote honesty and ethics.

It is therefore crucial to design systems that encourage people to act ethically and morally.

We need to develop the principle of disclosure, where participants have a vested interest in being honest in their disclosures, and behaving ethically and morally, as this would be in their interest and benefit them in the long term. We need to include measures for greater transparency.

### The choice of alternatives

Apart from the fact that programs are global, the choice of programs or candidates offered to the voter is generally small.

Campaign financing is one of the factors limiting the choice of alternatives. Candidates are those who have access to sources of funding and can deploy enough information and propaganda about themselves to make their side visible and have a chance of winning[^10].

Political parties may have internal structures that favor certain candidates or political tendencies to the detriment of others. This can limit voters' choices by giving them only a few options that have been approved by the political parties.

### Delegation of power

An election is two things: a vote and a delegation of power.

As mentioned above, our democracy is the recurrent abdication of legitimate power from the people to a group of people.

The delegation of power can also lead to a loss of control over decisions. If one person or group delegates power to another, they can lose control over what decisions are made and how they are implemented.

Under the pretext of delegation, which gives full legitimacy to elected representatives, there can be misappropriation, alienation, spoliation or usurpation of power.

One of the main problems with the delegation of power is the risk of disempowerment. If a person or group delegates decision-making power to another person or group, they can absolve themselves of responsibility in the event of failure or error on the part of the person or group to whom they have delegated power.

Another problem with delegating power is the risk of conflict of interest.
Some operations require time, energy, prior knowledge and intellectual capacity. In such cases, it may be profitable, or even necessary, to make use of the division of labor and specialization by delegating part of the decision-making work. But this delegation is power. For example, a government may delegate its power to a company to manage a project. But the delegatee doesn't know exactly what he's delegating, since by definition he doesn't have all the information about the possibilities. This leaves the door open to abuse[^2].

### Centralization of power

Power is centralized when few decide much and many decide little.

Centralization increases the delegation of power, which in turn reduces democracy.

When a small group of people holds absolute power, it can lead to unilateral decision-making and unfair policies that no longer take account of citizens' interests and opinions.

The centralization of power can make governments more vulnerable to abuse of power, tyranny and dictatorship, as there are fewer checks and balances to limit the authority of a small group of people.

### Voting systems

It's important to realize that an elected candidate is elected by a ballot, and that changing the ballot will change who is elected.

Because the electoral process greatly affects the outcome of a vote. So the choice of ballot affects the future of our societies. So it's important to focus on the choice of ballot you want to put in place.

Single-round and 2round elections have a fundamental problem: they don't allow voters' preferences to be expressed clearly and precisely[^11].

In a one-round system, the voter can only vote for one candidate, which can be problematic if several candidates have significant support. For example, in an election where there are three major candidates, the voter must choose between three different options, but has only one vote to express his or her choice. This can lead to situations where the elected candidate does not have the support of the majority of voters, but simply the highest number of votes.

The problem with two-round elections is that they can sometimes lead to strategic voting tactics and alliances between candidates. In a two-round system, the two candidates with the most votes in the first round face off in a second round. This can lead to situations where voters vote differently in the first round in order to ensure that their preferred candidate qualifies for the second round, even if they are not really convinced by that candidate. In addition, there may be cases where candidates join forces to push another candidate out of the race, rather than seeking to win the election independently.

Generally speaking, majority voting has the serious drawback of not taking into account the intensity of citizens' preferences. Those who are almost indifferent between two alternatives, and those who prefer one much more than the other, have the same weight in the choice between them. For example, 1001 citizens who barely prefer one alternative outweigh the 1000 others for whom it is the worst catastrophe.

These polls encourage useful voting, which encourages politicians to organize themselves into political parties and place voters in predefined boxes, reinforcing the bipolarization effect.

Bipolarization of the electorate in politics refers to the tendency of voters to cluster around two main political parties, creating a distinct political divide between them. This leaves little room for independent parties or candidates, or for smaller political parties. This also has the negative consequence of leading to increased polarization of opinion, a reduction in the diversity of viewpoints, and a reduction in the number of political parties.

With these ballots, we end up voting against a candidate, not for him or her.

But the useful vote does not make the elected official legitimate.

Proportional representation keeps politics from becoming too polarized, but can lead to fragmentation of power and difficulty in forming stable governments. It becomes difficult for one party to win an absolute majority of seats, which can lead to unstable government coalitions or minority governments that find it hard to advance their agenda.

A good ballot must be independent of irrelevant alternatives and close the door on the useful vote dilemma.

Example of a possible alternative ballot:

Approval voting: each voter can approve as many candidates as he or she wishes, in no order of preference. The candidate with the most approvals is elected.

Weighted voting is an alternative electoral system in which each voter is allocated a certain number of points to distribute among the candidates.

Voting by majority judgment: voters assign a mark to each candidate, for example from "very good" to "reject". The candidate with the highest average score is elected.

Condorcet voting is a voting system in which voters compare all candidates two by two[^12].

Randomized Condorcet voting: here each voter compares pairs of candidates as in Condorcet voting, but instead of taking into account all the votes of all the voters, a certain number of comparisons are selected at random to determine the winner.

Mehestan (Sunflower) ballot: This is a voting system that combines point voting and Condorcet balloting. It proposes to compare each candidate one by one and to say which one would prefer for each criterion, but also to define the level of preference for this criterion. This voting method aims to be both fair and representative, allowing voters to vote for their preferred candidate without the risk of splitting the vote, and taking into account the preferences of all voters in determining the winner.

### Political parties

These voting systems encourage politicians to organize themselves into parties, because of their dependence on irrelevant alternatives.

### Institutional slowness

In many cases, political problems require quick and effective solutions to minimize damage and loss. However, slow political processes, such as negotiations, debates, votes and bureaucracy, can delay decision-making and prevent governments from acting quickly.

### Short-term mandates

The elected representatives in question are elected for a certain period of time, usually a few years. During this time, they enjoy substantial personal freedom to choose their course of action. Voters no longer constrain them in any way. Very little forces elected representatives to keep their electoral promises, except their honor, but they can say that things have changed.

Elected officials are often more inclined to focus on short-term results than long-term challenges.

This is because these mandates encourage elected officials to focus on policy issues that are more likely to attract quick voter support, rather than on more complex issues that require a greater, longer-term commitment.

Politicians may be tempted to make decisions that are popular in the short term in order to maximize their chances of re-election. This can lead to policies that are not viable or sustainable in the long term, and which may cause greater problems in the future. This leads to a politician's approach to governance rather than a more pragmatic and thoughtful one.

### Bluffing to expertise

The "expertise bluff" in politics occurs when politicians use their position with a degree in science or administration to give the impression that they have expertise or special knowledge on subjects, when in reality they do not. They can use this strategy to manipulate or persuade voters, but it can lead to poor political decisions.

The valorization of science and administration degrees leads to a concentration of power and influence in the hands of a few elites who have been fortunate enough to gain access to such training. This leads to a loss of diversity and inclusion in decision-making circles, as people from less privileged backgrounds often have fewer opportunities to obtain these degrees.

Yet the possession of a degree does not necessarily guarantee competence or expertise in governance or politics. The expertise bluff, which we discussed earlier, can be used by people who possess degrees, but lack the actual knowledge or skills to make informed decisions[^2].

### Science scorned

Political decisions often have to be made on the basis of complex scientific data and empirical evidence, particularly in areas such as public health, climate change, food safety and so on. When policy-makers flout or ignore scientific evidence, they make decisions that are not based on facts and verifiable data, but rather on beliefs or opinions. This can have serious consequences for society, particularly in terms of health, the environment and safety.

### Propaganda

Voters don't vote for a candidate they know nothing about. Without information about the candidate being passed on to voters, a candidate has no chance. He only begins to have a chance when it exceeds a certain amount: his chances of being elected increase when this information increases.

Political investment is an investment like any other, and the quest is always the same: profit.

The history of the establishment of "electoral democracy" parallels the struggles and formal victories for freedom of expression, of the press, of assembly and of association. But these freedoms, by virtue of their necessary material and economic basis, have essentially been reclaimed by those who hold these economic means. And so to use them for electoral propaganda[^24].

Propaganda is used to manipulate public opinion and influence the outcome of elections or referendums in favor of a particular party or ideology. It can be misleading, partisan and based on lies, half-truths or omissions of important facts. It can also play on people's fears, prejudices and emotions rather than on reason and logic.

### The political hooligan

We can distinguish 3 caricatural types of citizens during elections:[^13][^14]

- The hobbits: those who are not interested in politics and who are very ignorant in this field, they don't have too many biases.
- Hooligans: are biased, but very biased, often victims of confirmation bias, and defend values irrationally.
- Vulcans: People capable of rational thought.

Our models assume that citizens are or can become vulcans, but in practice people are mostly either hobbits or hooligans.

Intuition forces us to form an opinion, and reason is then used to support these opinions. We rationalize our convictions.

We don't use our reason to fathom the truth, but to justify the ideological positions we've already adopted.

For example, if we say the word "Democracy", our intuition tells us it's a good thing, and then our reason manipulates the definition of the word to justify our intuition.

Nowadays, it's easy to confirm our intuition thanks to the Internet, which can provide us with all the arguments we need, and if these arguments or figures don't go our way, we can always question the motivations of the reporter of the facts or interpret them in another way.

What's more, by surrounding ourselves with like-minded people, our environment constantly reinforces our intuition and tends to polarize it.

As a result, political activists become overconfident, and it becomes impossible to change their minds even with facts and figures.

And when intuition predominates in this way, the more we inform ourselves, the more reason we find to believe what we believe, no matter what information we read.

Political commitment only reinforces political hooliganism.

The more interested we become in a political party with a price tag, the more radical our initial inclination will become.

Misleading but well-explained scientific videos and articles reinforce the scientific conviction of those who watch and read them.

As group selection is more efficient than individual selection, the tribes that have survived are those where individuals have collaborated sufficiently for the survival of the group.

And therefore those willing to sacrifice their individual interests for the good of the group. Natural selection has selected political hooliganism.

A vote based on political hooliganism provides enjoyment, whereas a rational vote requires mental effort.

### Citizen ignorance and irrationality

People are ignorant, almost all voters have no notion of science or understanding of the political system, are ill-informed and don't know the numbers[^16].

Some of them are unaware of their political hooliganism.

Most of us are unaware of the different cognitive biases and effects we face. We went over a number of these in Chapter 3 on HomoSapiens. Here are a few to remind you:

- Confirmation bias[^17]
- The halo effect[^18]
- Cognitive dissonance[^19]
- Pair pressure
- Availability risk[^20]

School and graduates can make people believe they're scholars, make them think they're smarter than others, and create overconfidence in fields that aren't their own.

Everyone becomes so sure of themselves. And the problem is that uncertainty is seen as a sign of weakness, whereas it is a fundamental quality.

The Dunning-Kruger effect shows that people with little knowledge or skill in a field tend to overestimate their own level of mastery, while people with higher skills tend to underestimate their level of mastery[^16].

But citizens' ignorance isn't the biggest problem, because even informing citizens doesn't prevent them from voting irrationally.

People behave like hooligans, not scientists.

### Time and energy to get informed

Informing oneself, learning, knowing, understanding, then choosing and deciding, and voting, requires time, energy and prior knowledge on the part of the voter, which in turn requires time and effort. These costs explain a certain delegation of power.

Election campaigns are often short and intense, leaving voters little time to inform themselves and evaluate the various options. What's more, the media often tend to focus on the most spectacular or controversial aspects of political events, rather than providing in-depth, nuanced analysis.

This can lead to a polarization of politics and a reduction in the quality of public debate. Voters may be tempted to turn to simplistic or partisan sources of information that do not necessarily reflect the complexity of the issues.

### Communitarianism

Communitarianism can be defined as a tendency to place more importance on belonging to a group or community than on the individual as such. The main problem with communitarianism is that it can lead to a fragmentation of society, with each group focusing on its own interests rather than the common interest of society as a whole[^21].

Communitarianism used to be geographical, but now it's spreading to the whole internet.

Nobody likes to be in the minority.

When an Internet user stops taking part in a discussion because he or she feels too much a part of the minority, this leads to ideological segregation.

Changing ideological neighborhoods on the Internet is much easier than moving geographically.

People can leave communities where they feel they're in the minority to join communities where they're in the majority.

Filter bubbels are what we call ideological isolations on the web.

We lock ourselves into a world where everything we read and hear confirms what we already think, making it difficult to question our convictions and understand why others think what they think.

These filter bubbles are amplified by the personalized content offered by the web's giants: Facebook, YouTube, etc. They lock us into the community with which we share our ideas, and deliver customized content. Every time we click on a link, like a post, they lock us further into a community with whom we share ideas[^22].

### Convictions

Convictions can be defined as deep-seated beliefs or firmly held opinions on a subject. Although convictions can be beneficial, they are generally more problematic than anything else, especially in the field of politics.

One of the main problems with convictions is that they can lead to polarization and division. If people have strongly held convictions on a subject, they may be less inclined to consider other points of view, or to work with people who hold different convictions. This can lead to polarization and division in society[^23].

They can lead to closed-mindedness. If people are too attached to their beliefs, they may be less inclined to listen to opinions or facts that contradict their beliefs. This can lead to decision-making based on prejudice rather than fact,

Beliefs can lead to resistance to change. If people are firmly attached to their beliefs, they may be less inclined to accept new ideas or changes in society.

## Solutions to the problems of our democracies

### The political entrepreneur

The solution to the political entrepreneur is to eliminate this function, or at least limit it by increasing citizen participation.

We don't want the political entrepreneur to be someone to whom power is delegated and who, from the moment he acquires it, can do whatever he likes with it. But that he should have a representative function, and that as soon as the people who delegate power to him feel that he no longer represents them, they should be able to dismiss him.

Total transparency must be put in place, so that people know precisely to whom they are delegating their power, and what they are doing with it.

And for good diversity, we need to ensure that candidates come from different backgrounds and have the same opportunities as others to access these positions.

### Political programs

One solution to the problem of political programs could be to make them more modular and subject-specific, rather than presenting them as a global package. This would allow voters to choose the proposals they prefer on each issue separately, rather than having to accept or reject the whole program.

### Honesty and transparency

In a democracy, it is important that decision-making is transparent and accountable, and that those in power are held directly responsible for their actions. Delegation of power must be used with care, and the associated risks taken into account, to ensure that the interests of citizens are protected.

### Choosing alternatives

It is important to give the voter a full and wide choice of alternatives, and an equal amount of visibility and information about them. Global programs again limit this possibility, and topics could be separated to allow more specific decisions.

### Delegating power

Voters must be able to choose whether or not to delegate their power. They should also be able to retract or withdraw their delegation at any time.

### Centralization of power

In a democracy, the distribution of power is essential to ensure that citizens' interests and opinions are represented and taken into account in decision-making. This involves the active participation of citizens, the decentralization of power and the creation of accountability and transparency mechanisms to limit the power of elected leaders.

### Voting systems

We need to favor voting systems that take into account the intensity of citizens' preferences, and avoid those that encourage useful voting.

### Institutional slowness

It must be possible to hold rapid referendums in emergency situations. And, more generally, enable citizens to express their views more regularly.

### Short-term mandates

Several possibilities exist, including the introduction of longer terms of office, with the introduction of a procedure giving voters the permanent possibility of revoking their elected representatives. We can also raise citizens' awareness of the long term, and motivate politicians to plan for the long term. We can also limit mandates, or even abolish the function of the political entrepreneur.

### Bluffs to expertise

It's important to recognize that skills and expertise are not exclusive to degrees, and that the inclusion of people from diverse backgrounds can bring a wealth of perspectives that can benefit governance and politics.

### Science scorned

Involve scientists in decision-making processes: Scientists can help inform policy decisions by providing independent, evidence-based data, analysis and assessment.

Enhance transparency and accountability on the scientific data and evidence they use to make policy decisions, and be accountable for their decisions and their consequences. Encourage effective communication between scientists and policy-makers, communicating their findings clearly and concisely, avoiding technical jargon. Foster a culture of respect for science. Educate policy-makers about science: Politicians should be trained in the scientific method and key scientific concepts to better understand scientific data and evidence.

Establish in a constitution principles to be taken into account so that decisions respect inialiénable principles that have reached consensus to ensure, for example, human happiness and balance within our ecosystems on Earth.

### Propaganda

To reduce the impact of propaganda in politics, it is important to encourage free and transparent information, based on facts and verifiable data. The media must play a key role in this process, providing balanced and impartial coverage of political events, fact-checking political claims and promoting independent fact-checking. It is also important to raise public awareness of propaganda and opinion manipulation techniques, educating them on how to recognize and resist these tactics.

### Citizen ignorance

Quality education can help citizens become more informed voters. It's important to teach students the skills of critical thinking, logical reasoning, finding reliable information and understanding political issues.

Transparency and accessibility: public policies and decisions must be transparent and easily accessible to enable citizens to better understand how their government works and what decisions are made.

Communication: it is important that political leaders communicate clearly and regularly with citizens, using simple language and avoiding technical jargon.

Participation: encouraging citizen participation in political processes, such as elections, petitions, public debates, consultations, opinion polls, etc., can help increase citizens' involvement in political life and strengthen their understanding of political issues.

### Lack of time

The problem of time can be solved by giving citizens more time by reducing their working hours. Another possibility is to have an algorithm that will each time accurately select a representative sample of society to avoid everyone having to vote.

### The political hooligan

Make sure you read the programs without political hooliganism, and cross-reference them with economic and sociological research. Ask yourself whether we're following our intuition, which our reason would then have backed up with arguments.

Becoming aware of this hooliganism will lead to better debates, and provide a way of thinking that isn't stuck on intuition, and that allows us to get out of our political hooligan way of thinking. We need to appreciate everything that is counter-intuitive, to appreciate convincing our intuition that our intuition is wrong.

We need to appreciate being wrong, to be able to question ourselves, to understand that it's by unblocking our intuition that we can make the greatest progress.

### Communitarianism

We have to fight against everyone's desire to be part of the majority.
We must then fight to ensure that being part of the minority is not an embarrassing experience.

We must be careful not to give the majority the upper hand simply because they are the majority.

You have to make the experience of being in the minority a pleasant one.

Make people who are in the majority want to be in the minority.

Let everyone try to understand and mingle.

### Convictions

In a democracy, it's important for people to be open-minded and willing to listen to other people's opinions and points of view. Citizens need to be educated and made aware of this.

### Conclusion of solutions

There are many problems inherent in democracy.

For it to function properly, it needs ideal conditions.

Good media to be well informed, and politicians in power to act in the best interests of the people.

But the problem is that our democracy creates a context in which just the opposite happens.

People are either hobbits, not interested in politics, very ignorant in this field, not too biased, or they are hooligans, biased, but very biased, and often victims of confirmation bias, they go so far as to defend values irrationally.

What we need are vulcans, people who can think rationally.

And at the moment, we can't have rational discussions between human politicians in a democracy with elected representatives seeking to be elected.

If we want a radical change in democracy, we absolutely have to take all these rational elements into account, such as the principle of political hooligans, community segregation, and so on.

But democracy is a gamble worth taking, because with it we all feel equal, it offers moral capital and federates us to cooperate.

## Experimental idea for a new deep democracy (V1)

Name: E-Democracy shared by spontaneous referendum, supported by expertise and wisdom.

To enable broader and more direct citizen participation, we are using information and communication technologies to create a digital tool that will facilitate and improve democratic processes.

This tool is a web platform, also available as a mobile application. You can think of it as a wiki, on which you can access information linked to the different layers of society in which you are involved, such as laws, organizational modes, current decisions, etc. These layers are divided and structured in such a way that they can be easily accessed. These layers are divided and structured into categories and sub-categories as necessary according to the nature of that layer.

Anyone can propose modifications to any information entered. As it is impossible for every human being to read all the proposals that could be made by all 7 billion human beings. We use an algorithm that takes a sample representing all the people linked to the proposal. The proposal is then sent to this representative sample. People can vote for, against or abstain, and if there are more than 50% in favor, the proposal moves on to the next level.

The proposal is then sent to a consortium of experts and sages who will study the proposal to give their in-depth opinion, check the technical and scientific aspect, estimate the resources and time to be allocated, but also and above all to check that it fits in with the constitutions established.

Eutopia's constitution will lay the foundations of the society, which will have to be studied and justified as enabling us to achieve our common goal of living a fulfilling and sustainable life in harmony with the Earth. It will then have to be taught to awaken the people to it, to ensure that it guides their proposals and decisions.

Once the work of the experts and sages is complete, their opinions will be attached to the final proposal, so that the citizen has all the keys in hand to make his or her choice. The people appointed as experts or sages will be elected by the citizens in their respective fields. The transparency and veracity of the information contained in these opinions must be ensured, as well as the simplicity of the information, all to enable the citizen to have all the cards in hand to make his or her choice, and to exercise his or her power with a clear conscience.

The proposal will then be re-sent to a sample group with the information and opinions delivered by the experts and sages. If the proposal is once again approved, it is then sent to the entire population for final adoption. The result should then be the same as that of the sample. Nevertheless, when the citizen receives this proposal for the final vote, he or she will know in advance that more than a majority intends to accept it, which has the advantage of giving the citizen one last chance to retract.

So the citizen doesn't receive all the proposals, but only the one that concerns him or her, depending on whether the decision is to be implemented more locally or globally.

Also, before you send in your proposal, we set up a collaborative system that we strongly advise you to use. Your proposal is then first and foremost a suggestion, and these suggestions are accessible to anyone who wishes to take part in them.

You can also delegate your power to someone else, who will then vote on the proposal for you. However, you will have access to the history of voted positions to check that you agree with them, and you can withdraw your delegation of power at any time. You can also choose the level of power you wish to delegate to the different layers of the company.

Finally, it's this democratic system that we'll also be using for collaborative story editing. Or sooner, as we test our system, we'll be able to iterate on it and modify it until we're fully satisfied, and it will ultimately be this final version that will be Eutopia's democracy.

> "Democracy this horizon seems to require time, courage, perseverance... and maybe a touch of madness..." Datageueule[^4]

## Sources

[^1]: [Wikipedia : Democracy ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. Are elections democracy? 1977
[^3]: David Shearman. The Climate Change Challenge and the Failure of Democracy (Politics and the Environment). 2007.
[^4]: [Datageule. Democracy(s)?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All: The Fundamental Principle of Politics](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory. 1656
[^8]: [Science4All. Hassiez the game, not the players. Democracy 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Foster honesty | Democracy 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja and Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail. 2015
[^11]: [Science4All. Our Democracies Divide | Democracy 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. The Randomized Condorcet Ballot | Democracy 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. Are you a political hooligan? Democracy 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[^15]: [Science4All. Rationally Irrational | Democracy 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies. 2008
[^17]: [The Bias Trunk #5. Confirmation Bias](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [AmazingScience - The Halo Effect - Dumb Brain #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [The Bias Trunk #3- Cognitive Dissonance](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Franklin Templeton Academy - Availability bias](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. Small communitarianism will become big | Democracy 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [TED Conference. Eli Pariser warns of "filter bubbles" online](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Dear conviction, mutate into a VIRAL infection!!!! Democracy 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Media concentrations: the billionaires inform you. 2016
