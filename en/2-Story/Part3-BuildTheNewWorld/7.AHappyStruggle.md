---
title: A happy struggle
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> This page is still in the experimental stage and is currently under construction.
> {.is-danger}

The idea here is to provide leads and solutions for a happy struggle, because you can't fight if you're unhappy. This struggle against the system leads us to a whole host of failures that can give us every reason to be unhappy, yet it's possible to see things differently.

Many of us are unhappy, and perhaps that's what brought you here in the first place.

When you want to change the world and be more in tune with your values, it can be hard to fight without seeing the results, hard to constantly re-examine the state of the world, to be aware on a daily basis of the suffering and injustice that fill this world, frightening to see all the indicators in the red and the direction humanity is taking. It's hard to live peacefully and serenely and still enjoy the pleasures of life. That's why we're tempted to give up and ignore it all to be left alone.

But it's just as possible to live in paradise on Earth, knowing you're in hell.

Because life is beautiful, no matter where we are on earth or what we're going through, we can appreciate the taste of it with a simple glance, a simple breath, a simple smile.

The forest has been on fire for thousands of years, and we live in a world full of injustice and suffering.

And there's no evidence that we've become happier as we've evolved and fought our way through history.

Happiness is actually more constant, and our psychology adapts.

So, enjoy, be happy, do your part, be present to exercise your power, your responsibility and your free will. The world is on fire, but don't let it stop you from being happy, from enjoying the people you love.

We are gradually identifying a "new world", distinguishing it from a known "old world". This duality must be the foundation of all emerging consciousness, and structures human thought. And we must collectively become aware that we are not opposed to otherness, but that we are built by the existence of otherness. And, dare we say it, we can only be aware of existing thanks to the existence of this otherness, thanks to this relationship. That's what existence is, it's only the relationship, there's nothing else.

For the new to emerge, we need to learn to "deal with": with otherness, with the Other, with others, with other cultures, with the past, with the world. In order to build itself, a "new world" needs to define itself in relation to an "old world", but we must never identify ourselves with a struggle against the old. Therein lies the shift in relational posture. What we call in common parlance "a higher level of consciousness" is actually a higher level of love, i.e. integration, acceptance and respect for otherness.

More to come... (See the Religion section of the story for more ideas)
