---
title: Dedications
description:
published: true
date: 2023-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:13:59.460Z
---

For the ideal in each of us that can suffer from a world that is not.

For the suffering of those we love and of all those we don't know.

This work is intended to be created by humanity as a whole, and to be updated throughout its evolution, by and for itself.
