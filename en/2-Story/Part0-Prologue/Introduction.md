---
title: Introduction
description:
published: true
date: 2023-03-07T21:16:06.598Z
tags:
editor: markdown
dateCreated: 2023-03-07T20:33:18.121Z
---

## Who are you?

You may not be a great sociologist, or a great philosopher, or a great economist, or a great anything else, in fact maybe you didn't go to university, maybe you were bad at school. That's the case for many of us too, yet despite these very different educational backgrounds, we have the confidence to propose to you here another possible world, even more, a world that aims for the ideal, and we invite you to join us.

So maybe you're wondering why you'd be well placed to make it happen. And maybe part of you, just reading this paragraph, already feels the urge to close this book so as not to waste any more time. But please allow us a few more paragraphs to try and convince you.

## Imagination

Although you're not a great expert in any of the world's major fields, there's one thing you're particularly good at, and that's dreaming. This ability to imagine is the greatest gift of human beings. We invite you here to dream, but above all to help others dream.

When we started out, we were an insignificant species with very limited power to act. What made the difference between our species, Homo Sapiens, and others was our ability to cooperate in large numbers. [^1]

And we owe this to our imagination, our ability to invent stories and tell them to ourselves.

Thanks to collective myths, which we can also call "fictions", we have been able to come together and cooperate.

We have the ability to believe in things that don't exist, we have invented many legends, myths and stories, we have drawn imaginary lines on the world, created nations and customs, we have invented money and given it the highest value in our imagination.

This gift of imagination comes from our need for meaning, our need to predict and understand our environment in order to better survive in it[^2].

We let this collective imagination rule our entire lives, we even let it rule the destiny of humanity as a whole, and its power is such that it leads us to destroy our own environment, to bring about our own destruction.

But all this is basically just a collective imagination, and ceases to exist the moment we all decide to stop believing in it.

In reality, what we more commonly call reason is more often the fact of continuing to believe in and conform to the rules of our collective imagination.

## Reason

Once we decide to leave this imaginary world behind, that's when we can finally find our true reason.

So maybe you feel you don't know much about it, but you'll discover that you don't need to know much to find the solutions and make the choices that would lead us all to a fulfilled and meaningful life.

Because the answers to our questions are already within each of us. To find them, all we have to do is leave our collective imagination behind, open our hearts and listen. It may sound silly, but we're convinced that this is where the real reason lies.

And since we've already begun this process of deconstruction, that's what we propose you discover here, a world truly based on reason.

A world which, in its simplicity, meaning and logic, will seem obvious to you too.

You will then discover that you too, whoever you are, whatever your knowledge, you don't need to know much to judge what is good and what is not, that you don't need much to contribute to the shaping of this world.

> "But then," says Alice, "if the world makes absolutely no sense, what's to stop us inventing one?" Inspired by Lewis Carrol

<!-- > "Every innovation goes through three phases in public opinion: 1) It's ridiculous 2) It's dangerous 3) It's obvious." Apocryphal quote from Arthur Schopenhauer -->

## Eutopia

So you may be thinking that what we're proposing here is a utopia.

Utopia is a word constructed from ancient Greek, derived from "tópos" ("place"), with the prefix "ou" ("not"), literally "no place"[^3].

Utopia is a word that is often misused and abused. It's brandished at every turn to stifle hope, denigrate imagination, repress progress, it's a call to resignation, a key word in the system of exploitation, alienation and domination to accept the status quo.[^4]

But a "non-place" is a society that doesn't exist, not an impossible one. As we can see from history, the future of tomorrow's society is always new and different. In the end, tomorrow's society is always today's utopia.

Utopia is sometimes contrasted with "scientific". But this shows ignorance not only of Greek, but above all of the very nature of science.

The search for the non-existent, the quest for the achievable, the analysis of realistic utopias is indeed a scientific activity. To know what, among the unobserved, is possible and what is not, requires knowledge, a deep awareness of the nature and structure of the world.

It's a scientific attitude that we're adopting here, one that consists in questioning and calling into question everything, in wanting to explain things in depth, in looking for the real causes, in practicing doubt and systematic criticism of received justifications, and in accepting nothing as given, self-evident or immutable. It is in this way of writing that we will be led to radically question our society. Science is about opening our eyes.

So the study of possible societies in relation to the study of realized societies, present or past, is not widespread. Some disciplines and authors don't even imagine that it could have a professional dimension, or even fervently reject the idea. Perhaps these intellectuals want to justify limiting their imagination.

Our system may be terribly bad, but we all know that it's not the worst possible. Radical change could lead to an even more unpleasant society, and so with this justified fear, the mass of the population is conservative or cautiously reformist. It can only be otherwise if people can form a fairly precise and reliable idea of what the new and better system might be.

That's why the analysis of another possible society must be sufficiently serious, thoughtful, precise and complete. And the same goes for the steps involved in its implementation, the transition stages from the existing to the new.

Because what we believe to be possible depends on analysis, people need to be convinced by the project. Only then will they see it through, making the march towards a better society a necessity for the evolution of their own. [^4]

> "Of course, it's in the analysis of completed societies that we find the knowledge we need to build others; but then we have to stop accumulating ingredients without ever doing the cooking." Christophe Kolm

So in its current definition as perceived by our society, utopia is an ideal, a conception that does not take reality into account, and is therefore unattainable.

So yes, in that case, we won't be creating a utopia here.

Reality is not our collective imagination. Reality is the thresholds we can't exceed, so as not to compromise the conditions favorable to our survival. It's the maximum concentration of CO2 in our atmosphere, it's the limit of disruption of the biochemical cycles of phosphorus and nitrogen that are essential for the good condition of soils and waters, it's the maximum erosion of biodiversity, i.e. the extinction of species and their essential role within ecosystems, it's the intensive use of soils, the abusive use of fresh water, the acidification of oceans, etc.....[^5][^6][^7][^8][^9]

This is part of what makes up reality, which is not the case, for example, of the value announced by the small figures on the page of our bank account.

In fact, we're right in the middle of utopia, believing that our society can continue to live like this indefinitely in the long term[^11].

To conceive of the ideal world, we have to leave our imaginary world, start from the ground up, so we can solve problems at source. Because that's how we'll really be able to build another world that makes sense, another world that's actually possible.

What we're trying to create here is an Eutopia, a concept similar to utopia, but which is considered feasible in reality. The prefix "u" becomes "eu", which in Greek means "good", and therefore literally the "right place"[^12].

What guides free humans is their imagination of the future, the possible, the desirable, their utopia, Eutopia. And since human beings move forward far more often attracted by hope than driven by despair, the dream ends up creating the real. Our wish is that reality becomes Eutopia.

> "Utopia has changed sides; he who believes that everything can continue as before is a utopian. Pablo Servigne and Raphaël Stevens[^11]

## Taking a step back

We need to question our perception of the short and long term. Should we define 100 years as short term or long term? It's all a question of point of view, but it's necessary to step back and leave the scale of our short lives behind.

The earth was formed 4 billion years ago, life appeared 500 million years ago, the Hominia, the lineage of human beings, separated from the chimpanzee lineage 7 billion years ago, but from this lineage, it was only 300,000 years ago that our species was born: Homo Sapiens[^13].

In 5 to 7 billion years, the sun will have used up its helium and will transform into a red giant, multiplying its radius by 1,000 and gradually drawing the Earth into its burning halo. The Earth's temperature will then be so high that the oceans will evaporate. But life will have disappeared long before that, due to a lack of oxygen[^14].

In reality, humans still have 2 billion years to live on a habitable planet.

It's important to visualize these figures on a scale to get the full picture.

2 billion years is visually 2,000,000,000 years.

If we consider one generation for every 25 years, that's another 80,000,000 generations. And we are currently only the 12,000th.... Only 0.015% of our life on Earth has passed.

However, the end of the Earth doesn't mean the end of the human species - the universe is vast... But this part will give way to other imaginary tales... Because it would be time to learn how to survive sustainably on our own planet before hoping to be able to survive on others. Which is currently not the case.

So we're only at the very beginning of our story, and when we see the speed at which we're consuming our planet's resources and the relationship we have with its ecosystems as a whole, we can't hope to hold out much longer...

At the current rate of world production, we will have consumed all the oil in 51 years, all the gas in 53 years, all the coal in 114 years, all the uranium in 100 years, and we're not giving much longer for all the rare earths essential to today's various technologies. [^11]And we hope to hold out for 2,000,000,000?

Hence the importance of taking a step back, looking beyond our short human lives and thinking in the long term. We'll come back to this later, but it's important to realize that if we're so lacking in this ability, it's because the Homo Sapiens brain isn't designed for long-term thinking. [^15]

You'll also notice that it's not the earth we're trying to save here, because it still has a long way to go, with or without us. What we're trying to save here is human existence, and with it the various species that make up our ecosystem.

## When dreams become reality

Perhaps you're wondering how imagining another world could help change the existing one?

After all, we might say that it would be better to devote our time and energy to changing what already exists, rather than imagining a new one.

But what happens is not independent of what we try. What we try depends on what we believe is possible. And what we think is possible depends on our analysis.

The principles and values on which the foundations of the world we are proposing you discover are based have been little developed and analyzed by intellectuals. This analysis will enable you to believe that this vision is possible, and then to try it. And, ultimately, to change what already exists. This analysis must be as coherent as possible if it is to win the support of as many people as possible, and lead to collective, sensible and far-reaching action.

By imagining a new world, we discover by comparison the absurdity of the current one, which leads us to question not only the world around us, but ourselves as well.

By imagining a new world, we create material, a space for reflection and debate in the search for a common ideal.

By imagining a new world, we bring people together around a common ideal, enabling them to come together to take action and change what already exists.

By imagining a new world, we can draw inspiration from it to produce tools and principles, apply them in the real world, experiment with them, iterate on them and improve them.

Finally, by imagining a different world, we inspire dreams and hope, a necessary emotion at the root of any great revolution.

We've told you about this gift of the human being, the ability to imagine and tell stories, a faculty that has seen us evolve exponentially since our cognitive revolution 90,000 years ago.

Well, now it's time to change the story to change the story. We need to be driven by a common ideal, change our narrative, modify our beliefs to enable us to mobilize and collaborate to change our future.

> "From time immemorial, stories have been the most powerful vehicles for philosophical, ethical and political change... It is therefore through stories that we can initiate a genuine 'revolution'". A handbook of contemporary resistance[^24]

As you read, you mustn't say to yourself "That's impossible" - you're probably right, it is under current conditions, and we're not proposing to apply this narrative right now. But you must say to yourself "It could become possible".

What you then have to do is ask yourself "What's possible right now to move towards this ideal?", and then build the best possible transition path. But to do that, you first need to know what that ideal is, and where you're going.

Then all you have to do is look at what's possible to do right now, and what new possibilities will open up as you make it happen.

However, the aim here is not to tell you what to do or how to do it, but to develop a vision that will enable you to discover it for yourself and help you get there with the right tools.

It's also important to realize that this vision isn't set in stone: it's something you build yourself, and it's designed to evolve over time as you make progress and discover new things.

So it's quite possible that our system is doomed to collapse no matter what we do. Indeed, it seems to be stuck in its environment-destroying inertia, and out of control.

Books on collapse theories are numerous and based on very serious studies, but if such a collapse were to occur, we wouldn't go from 7 billion to 1 human, and there would still be enough to rebuild a society. The collapse won't happen overnight, it will be gradual with undulations, and it will always be possible to change direction during it. [^16]

In either case, the work we're doing here has just as much value: either we manage to change society in time to avoid collapse, or, while preparing ourselves for the resilience to be able to continue living during this collapse, we prepare the solid foundations for the start-up of a new society.

Because the collapse won't leave room for a brand-new, ready-to-use society, it will be the individuals at that point who will have to build the paths and work out the directions to be shared together.

Our unpreparedness could leave the field wide open to verbose and impotent assemblies. In the same vein, self-management systems cannot be set up without prior trials, as these will almost certainly lead to numerous failures. The failure to establish collaborative, democratic decision-making at all levels of society could, because of the emergency situation, give way to other, undesirable systems of domination. That's why these new avenues need to be explored today. [^4]

Despite everything, we remain convinced that we can avoid collapse. It would create a humanitarian and ecological disaster that would be all the more difficult to repair, which is why we cannot describe it as desirable, even if it would leave room for something new. We will do everything in our power to find and develop solutions to avoid it. Let's never forget that even if the system is powerful, our collective imagination is just as powerful as it is, for it is on this very imagination that the system exists and resists.

Finally, building this vision, even if it's not accepted or wanted, reveals how the world could be. It can make people realize that they're just playing a giant game with rules that create incentives, and that their lives, opportunities and privileges would be very different if the rules were changed.

> "If you want to build a boat, don't get your men and women together to give them orders, to explain every detail, to tell them where to find everything. If you want to build a boat, instill in the hearts of your men and women a desire for the sea." Antoine de Saint Exupéry

## The struggle of transition stories

We saw above that it is the imaginary that creates the real, and from time immemorial it has been narrative and stories that have made our societies evolve. Our times are no exception, and the people in power don't conceive of the end of our societies; they believe in a hyper-modern narrative and in the construction of the transition narrative.

They imagine a future in which we live in artificialized cities crammed with technologies that will, among other things, counter the ravages of climate change. Our food would be increasingly industrial, artificial and synthetic. We will be immersed in instant consumption, supported by an information economy, or rather a surveillance economy. We'd be tenants of all, we'd pose nothing, but we'd be "happy".

But the hyper-modern narrative and the construction of the transition narrative have broken down. This modernity founded on the ideal of infinite material progress, correlated with social progress for all, has sunk into a multidimensional crisis and become completely dissonant. The narrative of hypermodernity has instrumentalized the ideal of progress, while draining it of its emancipatory social essence. It is manipulative and supported by the media, notably through the emergence of a novlanguage. Words are hijacked from their original meaning, emptied of their substance, oriented according to masked interests. Hypermodernity is hyperaccumulation, hyperconsumption born of desire made insatiable by invasive advertising. Modern man, who has everything, is paradoxically the most dissatisfied of human beings. [^17]

The hyper-modern narrative is undergoing three major failures that everyone is beginning to see clearly. The first is its ecological failure, and the promise of green capitalism, i.e. productivism that respects life, seems increasingly implausible. The second failure is economic, with grabbing and inequality on every scale, from local to global, increasing exponentially, despite exceptional material abundance. The third failure is an ethical one: our social relations are being transformed into commercial relationships, from which everyone hopes to emerge a winner, with everything bought and sold. These imbalances generate violence and are no longer sustainable.

So a transition narrative is emerging as an alternative to the hyper-modern one, one that will plunge us into a new era, that of the noosphere. But it's far from being built yet, and it's far from being desirable. That's the aim of the Eutopia project, to build this new narrative...

So we need to understand what people really dream? These dreams must not be neglected. Imagination is powerful, and will be the first step towards realization. For example, do we dream of feeling like we're on holiday all the time, everywhere? What is our collective ideal? We urgently need to answer this question, so that we can give meaning to this movement, and design and implement new policies now.

> Between us and collapse, if we are to build a brighter tomorrow, we need to keep up the ramparts of fraternity and creative imagination. Modified quote from Valérie Jousseaume[^17]

## Collective intelligence

No one individually holds the truth, but together we can at least get closer to it.

We certainly don't ask you to believe anything you read here. On the contrary, we encourage you to believe nothing, never to think you have the truth, and always to keep some doubt within you.

Because to believe you hold the truth is to prevent yourself from seeking it, and therefore preventing yourself from finding it.

That's why we can't imagine this world on our own - we need as many people as possible. To achieve it, we need to use collective intelligence and develop its tools.

So, if you're reading this, we're currently editing this text with a tool called Git, which allows you to version the progress of this story. And it also allows you to join in editing parts of this story, submitting them, so that then all collaborators can, give an opinion, debate, and then finally vote to integrate the modification or not. Once the modification has been validated, it appears directly online, right where you are reading.

The idea is to design a platform entirely dedicated to the collaborative and democratic editing of this story. A tool that will be modular enough to be used in many other projects.

This application is called Ekklésiapp, a mix between application and Ekklesia from the ancient Greek word for "assembly", coming straight from the citizen assemblies of ancient Greek cities, including Athens, the cradle of democracy. Although in those days, democracy was only accessible to the ruling class, we'll see in chapter 5 "How to decide together" just how imperfect it still is, and how much remains to be done. [^18]

Ecclesiapp is a decision-making application that can be used on the scale of a few people up to the whole population, and is modular enough to be able to create an infinite number of different decision-making methods, to easily adapt it to the job in hand. A chosen method can easily be modified, so as to continually search for a method offering the best results.

This method is freely available. It can enable others to create their own utopia, dystopia or eutopia. And that's to develop research in this field, which is necessary and useful for the many reasons cited above.

It also enables other people to carry out other types of collaborative writing. It can be used to manage an organization, a company, or any other project, and finally offers a real democratic tool, giving power back to the grassroots, effective enough to dispense with systems of domination and hierarchy.

But how can we agree on the rules for publishing this story?

Ecclesia is directly inspired by the democratic decision-making process that will be put in place in this ideal world. Well, actually, the democratic decision-making process of this ideal world will be inspired by this platform. Because this platform will directly enable us to test our ideas on the right collective and democratic decision-making process, iterating on it until we find the one that everyone agrees leads to the best results. So not only do you give your opinion on a decision, you also give your opinion on the decision-making process. The decision-making process we've devised will be revealed below.

> "Alone we go faster, together we go further" African proverb

## How we write

As for the skills required to judge or participate in this story, there are none.

If you find the world complicated, it's complicated because we've made it complicated, but in reality, once you start from a good base, everything turns out to be simpler and more logical than it seems.

Everything that seems complicated to us is really just a series of simple elements, some things just need more time than others to be reviewed and assimilated.

In the end, the great scientists are those who have been able to glimpse this simplicity of the world.

Don't doubt yourself: if you don't understand something, it's simply because we haven't taken the time to explain it to you from the basis you know.

Indeed, it's essential to always start from the ground up. It's one of the fundamental principles of this story, and that's why we'll take the time to create a solid foundation in Part 2, before describing Eutopia in detail in Part 3.

This book is intended to be read and understood by everyone. Intellectuals may find it too simplistic, with a vocabulary that's not rich enough, but that's what we're aiming for.

Because we all need to be actors of change, how can we come together if what should unite us is unintelligible and incomprehensible to someone who doesn't have in-depth knowledge of this or that field.

Here we're not trying to use complicated terms or long, complex turns of phrase, nor are we using these methods to appear more scientific, more intelligent, and so potentially create a false sense of truth in you in this way.

This story has to be accessible, understandable and to the point. It is intended to be honest with you, and devoid of the use of psychological effects, we will leave you fully free to believe or disbelieve its content.

In the history of mankind, each production of an author is only the indirect production of a collective intelligence.

This story flows naturally from the constant search for the "truth" that has been carried out throughout humanity by the countless humans who have contributed to the evolution of global thought. These humans were each born at a time, at a moment, when they received collective knowledge from the past and benefited from interactions that led them, with the help of the skills, status and time they enjoyed, to gather and publish new thoughts and knowledge. Direct references to these individuals in our narrative will be annotated, but we will avoid describing at length the process from which their thoughts derive, in order to lighten the narrative, make it accessible but also to avoid over-individualizing these discoveries, and to promote a new paradigm: the understanding that all creation of knowledge and thought in human history has been, even indirectly, only collective. We'd now like to push this collective intelligence even further, with the help of tools to facilitate these mechanisms, such as the one used in this story. Throughout this story, you will also notice a certain language that attempts to break out of this individuality.

In our eyes, Eutopia is therefore the fruit of a collective intelligence produced over thousands of years, and not the fruit of isolated individuals over the course of these eras, ours included. One mental image is that of seeing humanity as a single organism evolving over time, filled with millions, billions of cells, living, dying and being reborn, constantly evolving and readapting to find balance in its environment.

References to the original authors will nevertheless always be annotated at the bottom of the page, so that you can find out more about these individuals and their work. But also to make all our sources available, so that the evidence we put forward can be verified.

A second, richer and more scientific version of this story will also be developed and directly linked to this one, to support the argumentation of its content with academic institutions. This version will be easily accessible at the click of a button, whenever you wish to know more about the history, thoughts and argumentation of a particular subject, and you'll be able to go into greater depth on any given topic.

Numerous studies will be cited throughout your reading, but in general we encourage you to be cautious about the conclusions of the studies you read, as it's easy to influence science. Some may be funded by companies or interest groups with specific motivations and interests that will influence the study.

Studies may be subject to design errors, such as selection bias, measurement error or statistical error, which can lead to unrepresentative or incorrect results. Interpretation of results can be subjective, depending on the beliefs and prejudices of the study author. It is important to consider results in context, and to look at the evidence as a whole. Generally speaking, figures can be presented in different ways to support a specific argument, and therefore lead to erroneous conclusions. [^19][^20][^21]

The idea is not to shun science, but on the contrary, to rely on it, but with a vigilant eye, recognizing that the knowledge produced is in flux and evolving, that new studies contradict previous ones, and that in the current state it is largely influenced by the economic system that funds research. [^22]

Moreover, without going into detail here, metaphysics challenges basic assumptions about what exists and how entities can be categorized and understood. In particular, it explores concepts such as the existence of free will, the nature of mind versus body, and the foundations of reality itself. Although it does not challenge scientific studies directly, metaphysical reflection questions the conceptual frameworks and assumptions underlying scientific research. This may include reflections on what it means to "replicate" a study, how we understand causality, and what it means for something to be "real" or "true" in a scientific context, such as the fact that the act of observation is not neutral but affects the state of the observed object. In short, metaphysics questions the philosophical foundations of what we take for granted in our quest for knowledge, including in the sciences[^23].

This is why this work is not, and never will be, set in stone; its vocation is to be realized by humanity as a whole, and to be updated throughout its evolution, by and for itself. It will therefore always be open to new evidence that may contradict its content, including its deepest foundations.

> Doubt is the beginning of wisdom. Aristotle[^25]

## Conclusion of the introduction

As you will have gathered, you are about to read Eutopia, a story that is now more than necessary for humanity. It's time for us to move away from our primitive state, to allow the global evolution of our society towards true civilization, and to avoid the ecological and human disaster that's looming. The aim of this story is to be realized by humanity as a whole, by itself and for itself, to finally give it back meaning and direction, to ensure a guaranteed future of freedom and happiness.

> "The tensions and contradictions of the human soul will only disappear when the tensions between people, the structural contradictions of the human network, disappear. It will then no longer be the exception, but the rule, for the individual to find that optimal physical equilibrium which the sublime words "Happiness" and "Freedom" are intended to designate: namely, the lasting equilibrium or even perfect accord between his social tasks, the totality of the demands of his social existence on the one hand, and his personal inclinations and needs on the other. Only when the structure of human interrelationships is inspired by this principle, only when cooperation between people - the basis of every individual's very existence - is such that all those who, hand in hand, tackle the complex chain of common tasks have at least the possibility of finding this balance, only then will people be able to claim with a little more reason that they are "civilized". Until then, they are at best engaged in the process of civilization. Until then, they will have to keep repeating: "Civilization is not yet complete. It's in the making! Nober Elias, The Dynamics of the West."

## Sources

[^1]: Yuval Noah Harari, Sapiens : A Brief History of Humanity. 2011
[^2]: Sébastien Bohler, Where is the sense?/ 2020
[^3]: [Etymology of the word utopia](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, La bonne économie: La réciprocité générale. 1984
[^5]: [Maximum concentration of CO2 in the atmosphere](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Limit of disruption of phosphorus and nitrogen biochemical cycles](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Maximum erosion of biodiversity](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Intensive land use](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Overuse of fresh water](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Ocean acidification](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: How everything can collapse, Pablo Servigne & Raphael Stevens
[^12]: [Etymology of the word eutopia](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, The Earth's History
[^14]: The Future of Life on Earth" by Peter D. Ward and Donald Brownlee
[^15]: Sébastien Bohler, The human bug. 2019
[^16]: Jared Diamond, Collapse. 2004
[^17]: Valérie Jousseaume, Plouc Pride: A new narrative for the countryside. 2021
[^18]: Serge Christhonne, Are elections democracy? 1977
[^19]: Ben Goldacre, Bad Science. 2008
[^20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [The Replication Crisis](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documentary: The Factory of Ignorance. 2020
[^23]: Jaegwon Kim, Foundations of metaphysics
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[^25]: Aristotle, Ethics to Eudemus
