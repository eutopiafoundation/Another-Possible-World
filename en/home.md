---
title: Home
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z
---

> Click on the globe at top left to change language.
> {.is-info}

Welcome to the first evolving, collaborative and democratic story of an ideal world.

This project is supported by the [Eutopia Foundation](https://www.fondationeutopia.org/en), which is involved in the battle of imaginations and supports the realization of a genuine ecological and social transition. It helps projects in line with its values, by developing and providing the necessary tools for their creation, networking and self-management.

This first story aspires to reconcile the principles of freedom and equality by promoting an economy based on reciprocity and symbiotic modes of production.

For your reading pleasure, we advise you to follow the order of the pages. You may be tempted to jump straight to part 2 to discover this other possible world in concrete terms, but the basics of part 1 are essential, as they are what justify what is possible.

That's why we've concentrated primarily on Part 1, and why much of Part 2 is still being written.

To make all the ideas in part 2 possible and coherent, one of the key elements is to understand the human being, which we did in the "Homo Sapiens" chapter, but also to choose the right economic system, which we did in the "Working together" chapter.

Aware of the time and motivation we may have in our lives, we recommend these chapters first, and we have also produced a summary for all the chapters in this story. [This is available in the Appendices](/en/4-Appendices/1Summaries)
