# Lettre au nouveau lecteur

Bonjour,

Comme vous en avez pleinement conscience, nous sommes en train de vivre un tournant de l'histoire de l'humanité. Le système est plongé dans une inertie destructrice qui semble inarrêtable. Le travail de sensibilisation est là, nous ne cessons d'informer, de convaincre, mais rien n'y fait malgré la lutte, les dégâts sur notre environnement ne cessent d'augmenter.

Ce travail de sensibilisation j'y ai moi aussi contribué, et j'ai du me résoudre à constater qu'il ne suffirait pas, du moins pas à temps.

Je suis un idéaliste et je n'ai pas plus de sens à donner à ma vie que de me dédier à tout essayer pour rendre ce monde meilleur. Au travers de mon parcours, de mes expériences et de mes longues réflexions sur le monde, je n'ai cessé de chercher après une solution qui pourrait enfin avoir suffisamment d'impact pour enrayer la destruction de notre futur.

Dans cette quête d'une solution, j'ai fini par en trouver une, une qui d'après moi pourrait enfin réellement avoir une chance de faire la différence. Au fils de mes recherches et de la littérature que j'ai été amené à lire pour la développer, j'ai découvert que tout le savoir était là, que tout le monde ensemble possédait une partie de la solution, et qu'il suffisait plus que de tout rassembler pour en créer une solution globale. Cette solution, personne n'a encore entrepris à la réaliser dans sa forme la plus complète. Ça simplicité risquerait d'émaner de vous un sentiment naturel d'impossibilité, mais je ne peux vous la partager autrement. Aussi impossible que cela puisse t'être à vos yeux, je pense, qu'après avoir tout tenté nous n'avons plus rien à perdre à tenter l'impossible.

Je ne vais pas tourner plus longtemps autour du pot, l'objectif est de réaliser le récit d'une Eutopie collaborativement et démocratiquement, de fonder une fondation pour promouvoir sa vision, de permettre ainsi de se rassembler autour d'elle, d'en développer ses idées et ses outils, pour finalement les mettre en pratiquer dans le réel depuis la base de la société et ainsi entamer sa transition collectivement.

Quel que soit votre sentiment à la lecture de ce paragraphe, que celui-ci soit un sentiment d'absurdité, de curiosité, que vous soyez porté par l'espoir ou par le désespoir, ou les deux à la fois. Je vous invite à suivre le lien à la fin de cette lettre pour en découvrir les prémices et en juger de vous-même.

Vous découvrirez que l'idée n'est pas de promouvoir une vision unique et totalitaire. Cette œuvre ne peut de par ça nature être réalisé seul, ni même en groupe restreint, elle se doit même d'être réalisée par le groupe le plus large possible, et dans l'idéale, par l'humanité tout entière. Moi qui vous écris ici je ne possède nullement toutes les compétences et le savoir requis à sa réalisation, personne d'ailleurs, j'ai seulement pris conscience de l'impact que ce projet pourrait avoir, j'ai pris conscience que tous ensemble nous possédons ces compétences requises à sa réalisation.

La concrétisation de ce projet, même si infime peut elle nous sembler, je pense qu'il nous faut la saisir, et c'est ce que j'ai décidé de faire en vous envoyant ce mail.

J'ai besoin de vous, du savoir, et du soutien que vous pourriez apporter à sa réalisation. Je vous contacte, car je pense que vous aussi vous pourrez entrevoir le potentiel de ce projet, car je pense que vous pourriez poser l'une des pierres essentielles à son commencement.

Bien à vous,
Manuel Mareschal
