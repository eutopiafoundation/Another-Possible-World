## Markadown 
### Gras et italique
---
    *Texte en italique*
    **Texte en gras**
    ***Texte en italique et en gras***

### Barré:
---
    ~~Ce texte est barré.~~ mais pas celui-là.

### Les titres :
---
    #  Titre 1
    ## Titre 2
    ###  Titre 3
    #### Titre 4
    #####  Titre 5
    ###### Titre 6

### Les citations
---
    >Ceci est une **zone en retrait**.
    >La zone continue ici

    >Ceci est une autre **zone de retrait**.
    Cette zone continue également dans la ligne suivante.
    Cependan, cette ligne n’est plus en retrait

### Les titres :
---
    Liste tiret :

    - Liste1
    - Liste 2
    - Liste 3

    Liste numéroté :
    1. Liste 1
    2. Liste 2
    3. Liste 3

    Liste à cocher :
    [ ] A
    [x] B
    [ ] C

### Le code :
---

    C’est le `code`.

    ```html
    <html>
    <head>
    </head>
    </html>
    ```

### Images et hyperliens :
---
    Ici ce qui suit [Lien](https://example.com/ "titre de lien optionnel").

    [![Ceci est un exemple d’image](https://example.com/bild.jpg)](https://example.com)

### Les tableaux :
---
    |cellule 1|cellule 2|
    |--------|--------|
    |    A    |    B    |
    |    C    |    D    |
