---
title: Collaborer tous ensemble
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## Qu'est-ce que l'économie

L'économie, est l'activité humaine qui consiste en la production, la distribution, l'échange et la consommation de biens et de services.[^2]

Mais un système économique ne produit pas que des biens et des services, il produit aussi des êtres humains et des relations entre eux. La manière dont la société produit et consomme a une grande influence sur les personnalités, les caractères, les connaissances, les désirs, les bonheurs et les types de relations interpersonnelles.

On a tendance parfois à juger la société exclusivement sur les quantités des divers biens et services qu'ont ses membres. Mais les gens apprécient non seulement ces biens et services, mais aussi les sentiments, les attitudes, les rapports interpersonnels, qui accompagnent ces transferts, ce que ceux-ci manifestent quant à ce que sont les humains. Or ces aspects sont bien trop souvent négligés dans la science économique.[^3]

Un système économique forme et façonne les humains et leurs relations par leurs activités tant de production que de consommation. La société sécrète le système économique, qui à son tour influence profondément la nature et la qualité des relations entre personnes et leurs psychismes.

Lors d'un échange, le donneur et le receveur reçoivent toujours plus que ce qu'ils se donnent concrètement, car ils se donnent en plus la possibilité d'entrer dans une relation humaine.

Nous avons été habitués à suivre la science économique comme si elle était un simple fait de nature indépassable qui dicte ce que le monde devrait être, sans réellement la remettre profondément en question. Nous sommes imprégnés du monde dans lequel nous vivons, mais une fois que l'on comprend ce qu'est un système économique, nous devenons capables d'imaginer notre monde fonctionnant avec d'autres règles.

Quelque chose de neuf arrive ces temps-ci, car à la misère du monde et à la domination que nous avons les uns envers les autres, vient s'ajouter le dérèglement climatique. Celui-ci nous force à constater les limites du réel, et nous oblige à repenser nos modèles économiques.[^4]

Chaque année, les médias et les organisations de recherche sur le climat nous livrent une date. Celle où les scientifiques estiment que l'humanité a consommé plus de ressources que la Terre ne peut en produire durant la même période.

Ce chiffre est le résultat d'experts de divers domaines qui dressent un bilan des ressources que nous prélevons sur la planète et du taux avec lequel ces ressources sont reconstituées.

Cette date est appelée "jour du dépassement". Pour qu'une économie puisse être durable, il faudrait que cette date se situe au 31 décembre et pas avant. Dans ce cas les ressources peuvent alors se renouveler au même rythme qu'elles sont consommées.

En 2022 cette date est estimée au 22 juillet.[^1]

Cette date devrait nous aider à voir avec plus de lucidité notre système économique, à prendre conscience de ses fondements dans l'homme, et de ses vices.

Notamment que continuer à promouvoir un système économique qui encourage nos grands renforceurs primaires est sans doute la pire des choses à faire.[^5]

Les théories économiques n'arrivent pas à avancer d'arguments qui emportent vraiment l'adhésion intellectuelle, ce qui démontre leur faiblesse. C'est par ce que celles-ci reposent bien souvent sur notre imaginaire collectif et n'a pas de fondement réel.

Alors peut-être que la science économique est plus philosophique qu'elle n'aime à le laisser penser, et moins scientifique qu'elle n'aime à le faire croire.

Sans remettre en question le modèle économique, nos sociétés, portées par l'imaginaire d'un capitalisme vert, sont engagées dans une transition écologique. Mais l'histoire démontre que l'innovation du capitalisme ne fait pas du tout advenir de transition énergétique, au contraire il ne fait qu'accumuler l'exploitation des énergies et des matières premières, cette transition ne fait donc qu’accélérer l’épuisement des ressources et donc le dérèglement climatique.[^37] Le système, par sa recherche de profit, est amené à développer toujours plus de moyens pour augmenter sa productivité. L’essor de l'intelligence artificielle et des robots accélère encore non seulement l’épuisement des ressources, mais ces IA et ces robots qui en arrive bientôt a pouvoir remplacer totalement l'humain dans ses tâches, en viennent à perturber nos modèles économiques et nous force à nous poser des questions quant à notre relation au travail et à la place de l'humain en société.

La direction que nous sommes en train de prendre nous amène tout droit à la dystopie, mais les signaux d'alerte du réel nous forcent à ouvrir les yeux. Ces constats nous amènent à une possibilité, une nécessité même, celle de choisir collectivement un nouveau modèle économique capable de porter une véritable transition écologique.

Et quitte à changer, pourquoi ne pas en profiter dès le début pour en choisir un qui réponde non seulement à ce besoin de durabilité, mais également qui réponde à tous les autres besoins et aspirations de l'humain ?

## Le plan et le marché

On peut définir 2 systèmes économiques qui dominent le monde moderne, ceux-ci sont le marché et la planification.[^6]

Toute idéologie politique, en termes d'économie, souhaite augmenter, promouvoir l'un ou l'autre de ces 2 systèmes économiques, ou faire un compromis entre les deux.

Chaque camp à l'opposé de cette dualité, défend son système avec une éthique sociale et attaque l'autre pour la réalisation d'une autre.

Pour le marché cette valeur est la liberté et pour le plan cette valeur est l'égalité.

Mais dans les faits, dans les systèmes qui utilise le marché nous sommes très loin d'être réellement libres, mais nous le sommes bien plus que dans le plan. Par contre nous sommes dans le pire des 2 systèmes pour ce qui s'agit d'égalité.

Dans le plan, nous sommes très loin d'être réellement égaux, mais nous le sommes bien plus que dans le marché, par contre nous sommes dans le pire des 2 systèmes pour ce qui s'agit de liberté.

En faite la réalité actuelle est que chacun de ces systèmes ont de mauvais résultats dans le domaine de la valeur qu'ils défendent, mais il est vrai, meilleur que l'autre pour la valeur défendu et bien pire selon l'autre critère éthique.

Ces 2 systèmes économiques à leur paroxysme sont devenus le marché capitaliste et la communisme totalitaire, tous deux engendrant et renforçant des traits humains et rapports nauséabonds et dégradants, tels que l'égoïsme, le traitement d'autrui comme une chose, l'hostilité, le conflit, la concurrence entre personnes, la domination, l'exploitation, l'aliénation...

Aucun de ces 2 systèmes économiques n'a réussi à exalter les plus belles qualités de l'homme. Ils en répriment même fortement quelqu'une. Dans ces deux régimes règne une certaine forme de domination.

## La 3e économie : La réciprocité

Alors existe-t-il une autre alternative ?

Il existe une littérature qui commence par "Il y a trois types de systèmes économiques". Mais cette littérature doit être recherchée aux marges historiques ou disciplinaires de la pensée économique, soit chez des économistes d'il y a plusieurs générations, soit dans l'anthropologie économique.

Tel est le cas de l'économiste Karl Polanyi (1886-1964), dans ses ouvrages il range en trois catégories les systèmes économiques de toutes les sociétés, passées, primitives ou traditionnelles comprises. Et il relève leur possible coexistence en une même société, en précisant cependant que chacune attribue une place privilégiée/dominante/principale à l'un d'eux. Il distingue d'abord un système économique qui est le marché. Il appelle son second le système de redistribution, qui sous forme élaborée et moderne en est la planification, où un centre politique décide de l'allocation économique. Le troisième système, Polanyi l'appelle la « Réciprocité », selon une tradition anthropologique déjà bien établie, et qui est même assez claire et précise.[^6]

Ce type d'économie nous en ignorons son existence, pourtant elle existe bel et bien au sein même de nos sociétés. Les sociétés contiennent donc pratiquement toutes les trois systèmes économiques, mais dans toutes, l'un des systèmes est plus important que les deux autres.

Voici un triangle correspondant à différent grand régime en 1984 :

<center>

![Schéma des 3 types d'économie](../../0-GlobalProject/Image/Marché.png)

</center>

Les économistes ont construit l'économie et sa pensé en pensant que l'homme était et a toujours été ce qu'on appellerait "l'homo oeconomicus", et ils en ont créé une caricature, comme étant un être égoïste, calculateur et asocial. Mais l'humain était avant tout ce qu'on appellerait "l'homo sociabilis" celui-ci pratiquant universellement la réciprocité et ayant universellement besoin d'un cercle de proches ou d'une communauté.[^9]

La réciprocité est le mécanisme de l'économie communautaire qui a nourri les débuts de l'humanité, comme chasseurs-cueilleurs, puis comme agriculteurs sédentarisés.

La réciprocité est toujours parmi nous, la vie économique intrafamiliale est d'abord fondée sur cette économie. Vous vous offrez des services sans rien attendre en retour, vous ne payez pas les membres de votre famille pour qu'ils vous rendent des services, et ils ne sont pas non plus forcés par une entité extérieure. La réciprocité dans nos sociétés a drastiquement diminué ces dernières décennies, par l'effet du marché qui cherche à monétiser toujours plus les interactions sociales, pour justement créer de nouveau marché. Mais aussi par la diminution de la religion qui créait de nombreuses communautés et faisait la promotion active du don altruiste. Mais outre la famille, on peut néanmoins encore retrouver beaucoup de réciprocité dans les relations amicales/affectives/amoureuses, à travers le bénévolat, le volontariat, les communautés, mais aussi dans différents domaines, comme dans le monde de l'informatique avec l'opensource et à travers de nombreuses plateformes de partages d'informations, d'entraide, de réalisations, etc.

Une longue suite de dons réciproques, réguliers, suivis et établis entre deux personnes est ce qu'on appelle la réciprocité. La réciprocité générale est l'équivalent, mais de la part de tout le monde. Chacun donne à la société, et réciproquement, elle reçoit de l'ensemble des autres.

L'altruisme et le don, de par leur nature même, ne peuvent être obtenus ni par contrainte ni par échange au sens strict.

Telle est la différence avec le plan : c'est que ces transferts ne sont pas contraints et imposés au donneur par une entité sociale extérieure à lui. Et la différence avec le marché c'est que ce ne sont pas non plus des échanges au sens strict ou chaque transacteur est exclusivement intéressé à avoir plus, ou à ne pas avoir moins, de ces biens ou services qu'ils transfèrent.

Dans ce système social, personne ne donne à un autre spécifique, il est donc impossible de dominer, d'obliger, d'humilier ou de forcer à un contre-don, et d'exploiter économiquement ou éthiquement par ce moyen. Chacun se donnant à tous ne se donne à personne.

Dans ce système le travail de chacun bénéficie à la consommation d'un grand nombre d'autres, souvent beaucoup plus tard. Et chacun bénéficie du travail d'un grand nombre d'autres, souvent très antérieur. Chacun reçoit même en partie de générations disparues et travaille pour des générations à naître.

La valeur éthique sociale de la réciprocité pourrait être appelée la "fraternité". Nous avons ici la fameuse trilogie républicaine, la fraternité se définit comme "« Ne faites pas à autrui ce que vous ne voudriez pas qu'on vous fît ; faites constamment aux autres le bien que vous voudriez en recevoir. »"

Mais la comparaison avec la trilogie s'arrête là, car la réciprocité efface le désir et le sens même des deux autres valeurs. Car dans ce système où chacun se soucie de chaque autre comme de lui-même, les volontés, et donc les libertés personnelles ne s'opposent plus, et des demandes d'égalité n'ont plus de fondement.

## Avantage de la réciprocité

La réciprocité se base sur la bonté.

La bonté c'est mettre en avant l'altruisme, l'amour, la solidarité volontaire, le don réciproque, la générosité, le partage, la libre communauté, l'amour du prochain et la charité, la bienveillance et l'amitié, la sympathie et la compassion. Chacun en manque et trouve qu'il en faudrait plus entre tous. Toutes nos grandes familles d'esprit, de pensée et de sentiment s'en réclament. Mais celles-ci sont reniées dans notre vie économique. Or celles-ci dominent l'existence de nos sociétés, mais l'on constate que la plus répandue des aspirations est la plus rare des réalisations.

La réciprocité générale est la réconciliation entre notre éthique altruiste et notre technique scientifique, entre nos aspirations à la fois à la communauté chaleureuse, à la liberté individuelle et au progrès matériel; elle permet la durée des sens culturels et des beautés du monde dans l'expansion de nos savoirs, capacités et consciences.

La réciprocité entraîne surtout avec elles de nombreux avantages à la production économique. Car elle offre un système efficace et cohérent qui va permettre de réduire drastiquement le temps de travail global.

> Plus d'infos dans le chapitre [« Temps de travail »](https://anotherpossible.world/fr/2-Story/Part2-AnotherPossibleWorld/17WorkTime).
> {.is-info}

## La réciprocité est elle possible ?

Si nous avons énormément étudié les marchés et la planification, nous n'avons quasiment rien dit du troisième mode de transferts de biens et services qu'est le don, et rien du tout de ce groupe de dons qu'est la réciprocité.

Déjà il faut comprendre que des systèmes économiques que l'on n'a jamais observés sont possibles. Au fil de l'histoire, des systèmes économiques nouveaux sont apparus et à chaque fois il y avait des gens, peut-être même la majorité, pour penser et clamer que les systèmes possibles sont ceux que l'on peut observer alors ou qui ont existé dans le passé. Mais ces croyances sont sans cesse démenties par l'histoire, par l'existence de nouvelles croyances. Et c'est très certainement encore le cas maintenant, car il n'existe aucune démonstration qu'un des systèmes existants ou ayant existé soit le meilleur des possibles. En effet, oui, des systèmes économiques que l'on n'a jamais observés sont possibles.

L'un des nombreux défauts de notre système est qu'il empêche que des individus puissent choisir individuellement un comportement de don: une firme qui fait des prix trop favorables tombe en faillite, un individu ne peut pas donner ce qu'il produit dans une société ou personne d'autre ne lui donne.

Des psychosociologues ont longtemps étudié les "comportements d'aide" avec des résultats très intéressants, on ne tend pas seulement à aider qui nous a aidés, mais on tend à aider plus si l'on a été aidé soi-même, c'est-à-dire on tend à aider des inconnus si on a été aidé soi-même par des inconnus. Ce qui est l'essence de la réciprocité.[^13]

Et c'est ce que nous avons vu dans le chapitre 3 sur Homo Sapiens, qu'il était possible de créer des êtres altruistes, car l'altruisme est lui aussi motivé par le plaisir, et que c'est principalement l'environnement et l'éducation qui contribuent à développer des valeurs et des motivations telles que la générosité, la compassion et le désir de servir les autres.

Mais est-il tout de même possible qu'une société ait une économie fondée sur la réciprocité et en particulier la réciprocité générale ?

Il existe des sociétés entières à l'intérieur desquelles l'ensemble des relations économiques est fondé essentiellement ou même exclusivement sur la réciprocité. Mais ces sous-sociétés ou sociétés sont ce que l'on appelle des "groupes face-à-face", dont les membres se connaissent mutuellement, familles, villages, bandes, etc. et la réciprocité n'y est pas essentiellement générale. Il était encore ici facile de l'appliquer, mais une personne ne peut pas en connaître plus d'une centaine d'autres et donc on peut légitimement se demander si c'est réellement possible à grande échelle.[^14]

Car bien que l'économie résolve le problème de la production et de la distribution de biens et services, tout système économique, dont la réciprocité, se confronte à deux problèmes fondamentaux pour la production: le problème d'information et le problème de motivation.

## Le problème d'information

L'information est celle du travailleur ou du metteur en œuvre de ressources ou services sur ce qu'il doit faire pour satisfaire finalement au mieux des besoins et désirs.

Et la motivation est celle qui pousse à effectuer ce travail ou à utiliser ainsi ces ressources.

Plan et marché résolvent plus ou moins bien ces deux problèmes.

Le marché résout le problème d'information par les mécanismes décentralisés du système des prix "indicateurs de rareté et d'utilité": l'offre et la demande.

Le plan le résout par l'organisation de cette information, c'est à dire par la collecte, le calcul, les instructions ou prévisions, et cela d'une façon relativement centralisée.

Le marché résout le problème de motivation en renvoyant à l'égoïsme par l'incitation de l'échange.

Le plan résout le problème par la contrainte, la menace ou la punition, la récompense ou la promotion, mais donc plus généralement par l'autorité et ses divers moyens de contrainte ou de récompense, et d'intériorisation plus ou moins forte de la nécessité d'obéissance par habitude ou le sens du devoir.

Les mêmes problèmes s'appliquent à la réciprocité, comment les gens vont savoir ce dont les autres ont besoin de leur part, et vont-ils vouloir donner assez aux autres ?

Ces deux problèmes se renforcent mutuellement positivement, la résolution de l'un favorise celle de l'autre et réciproquement.

Plus le problème de motivation est résolu, mieux celui de l'information peut l'être puisque les personnes se transmettent alors plus aisément les informations nécessaires.

Plus le problème d'information est résolu, plus le système économique est productif, et donc moins il y a de gaspillage, moins il faut de travail pour un service final donné, donc moins on a besoin que le système des motivations fournisse de travail, ou moins son imperfection est grave.

En réciprocité chacun considère les besoins ou désirs des autres comme aussi importants que les siens propres, et cela détermine ce qu'il donne de son travail (temps, effort, peine) et de ses possessions, quelle que soit leur origine.

Si le problème de motivation est résolu alors celui de l'information l'est aussi parce que les personnes peuvent toujours rendre l'économie au moins aussi efficace qu'avec le marché ou le plan, en se transmettant volontairement les informations économiques qui circulent dans ces systèmes.

L'informatique vient véritablement révolutionner le problème d'information. Internet offre la possibilité d'être connecté les uns et les autres au-delà de la limite physique, et il est plus facile que jamais auparavant d'élaborer les applications adéquates aux transferts d'information. C'est cette innovation qui permet d'enfin faire émerger la possibilité d'élargir l'économie de la réciprocité des petits groupes et communautés locales vers une échelle mondiale. On peut déjà voir le début de cette émergence par la création de très nombreuses plateformes communautaires d'échange et de partage réciprocitaire, telle que couchsurfing, workaway, wikipédia, etc...

Il existe déjà de grande multinational multilingue avec des milliers des personnes, qui arrive à coopérer en interne, pour créer des produits parfois complexes, touchant à de multiples domaines et demandant de multiples compétences. Même s’il faut y changer l'organisation pyramidale de domination qu'il y existe, on voit que nos systèmes d'information sont déjà très efficaces. Pour le moment le problème de la motivation est en grande partie résolu par le salaire que les gens reçoivent, mais si on arrive à régler le problème de motivation par un autre moyen, alors rien n'empêche les gens de se transmettre les informations au moins aussi efficacement qu'actuellement.

Transmettre et traiter les informations fait partie du travail à accomplir pour la société. En réciprocité la motivation pousse à agir au mieux pour la collectivité. Ce système peut donc faire tout ce que le marché ou le plan font.

De plus la réciprocité diminue considérablement l'information nécessaire. Via l'abaissement des consommations concurrentielles ou ostentatoires, la diminution des besoins extrinsèques, et la diminution de la production des biens ou services correspondants, la diminution du besoin de renouvellement des produits de par leur conception basée sur la durabilité, et la diminution de nombreux services et métier qui ne seront plus nécessaires à la société.

## Le problème de motivation

Quand on parle de "nature humaine" on peut entendre "l'homme ne changera jamais, il a toujours été égoïste, etc.." et viennent alors les idées sur la "vrai" nature humaine qui devrait finalement l'emporter. Il faut absolument opposer la connaissance historique, anthropologique, psychologique, l'analysée, et même la simple logique, à ces convictions naïves. À "l'homme ne changera jamais" on peut aussi bien répondre qu'il n'a fait que changer.[^15][^16][^17]

À "l’homme a toujours été et est partout égoïste" on peut également fort bien rétorquer en se demandant ce qu'on sait de ce qu'il "a toujours été".

L'humanité est embarquée dans une transformation sans précédent dans son existence par sa profondeur, et, bien sûr, par son échelle.

Comme nous l'avons vu dans le chapitre 3 sur « Homo Sapiens », quelque que soit cette nature humaine, l'humain peut devenir ce qu'il entend de vouloir devenir, le pire comme le meilleur.[^35][^36]

Voici différents points qui répondent au doute sur le problème de la motivation :

- Les gens ont une préférence pour les relations altruistes et de don.[^18]
- Toutes les grandes éthiques religieuses et laïques prônent très fortement l'altruisme et le don. [^20]
- La motivation humaine principale est de recevoir l'approbation d'autres humains: dans un régime réciprocitaire l'approbation est naturellement universelle et réciproque.[^19]
- Les analyses empiriques des "comportements d'aide" prouvent que les gens ont beaucoup plus tendance à aider les autres s'ils ont été aidés eux-mêmes.
- Des sociologues ont défendu l'idée que le trait caractéristique principale de l'être humain est l'imitation.[^22]
- L'habitude, cette imitation de soi-même, est un autre phénomène humain de première importance.[^23]
- Des études sur les développements des enfants montrent que nous sommes initialement altruistes, puis que la société nous rend égoïstes.[^21]
- L'éducation: comme nous l'avons vu, l'humain n'est finalement en grande partie qu'une page planche sur laquelle la société écrit[^24]
- Phénomène de renforcement social: plus les gens ont un état d'esprit de réciprocité, plus ils montrent par leurs actions et expriment par leurs paroles qu'il est normal et bon de l'avoir, et cela incite les autres dans cette voie. L'éducation et l'ensemble des relations sociales jouent ainsi un grand rôle. Si l'homme est d'abord culturel, et ainsi malléable, la société peut autant en faire la personne de la réciprocité que l'égoïste du marché, ou le soumis ou le hiérarque du plan.[^25]
- Plus il y a de réciprocité, plus on voit qu'elle est viable et plus on se rend compte de ses avantages, et donc plus on l'accepte, plus on "y croit", plus on la souhaite, plus on y pousse. Et donc plus elle tend à se répandre.
- Dans le cas ou certaines personnes qui en ont la capacité ne souhaiteraient toute de même pas participer ce n'est pas grave, car si de tels cas existaient, ce n'est pas du mépris ou de la jalousie qu'il faudrait avoir pour eux, car un tel sentiment signifierait un mal être dans la société: la personne concernée n'y trouverait pas sa place au point de ne pas vouloir y participer, des aides seraient alors mises en place, mais même dans le cas ou la personne ne souhaiterait pas participer ce n'est pas grave, car non seulement nous découvrirons que la charge de travail demandée par habitant pour le maintien de société sera considérablement réduite à 2/3h par jour (voir le chapitre sur le temps de travail), mais aussi que cela peut être contrebalancé par le fait qu'il y en aura toujours d'autres qui seront heureuses volontairement de donner "plus" qu'ils reçoivent, par altruisme comme nous l'avons vu, mais aussi par intérêt propre du travail.

Car après tout dans notre société actuelle pourquoi travaille-t-on ?

- La contrainte directe : le travail forcé
- L'échange : on vend son travail contre un salaire
- Par désir d'aider les autres ou la collectivité, de "rendre service"
- Par sens du devoir, éventuellement ce peut être pour rendre aux autres pour ce que l'on reçoit d'eux
- Par intérêt pour l'activité de ce travail elle-même.
- Pour trouver des contacts, des relations, sociaux, une insertion dans la société, par la contribution au milieu du travail.
- Pour montrer aux autres qu'on travaille pour eux, ou qu'on fait de belles choses ou qu'on est habile.
- Pour le statut social qui s'attache à ce travail (contribution, responsabilité, etc.)
- Pour s'occuper, avoir "quelque chose à faire", ne pas s'ennuyer ou être oisif
- Par jeu
- Par habitude

On pourrait en rajouter d'autres, mais on voit bien ici l'importance de l'occupation en elle-même, et des rapports sociaux que le travail nous permet de créer, on peut d'ailleurs le remarquer dans les sentiments de solitude et de perte de sens qui suivent les "mises à la retraite". En règle générale, l'homme tire plus de sens de sa vie de son activité de production que de son activité de consommation. Et toutes ces motivations à l'exception des deux premières peuvent inciter au travail dans un système de réciprocité, il suffit donc de jouer sur ceux-ci en augmentant le sens et l'intérêt au travail.

Le travail n'est pas condamné à être la torture de son origine étymologique contestée, du mot travalhum : tripalium, un instrument de torture avec un trépied...[^26]

Toutes les dimensions de la pénibilité de la tâche doivent ainsi être effacées ou réduites. Les travaux épuisants sur le plan physique ou psychique, abrutissants ou abêtissants, trop durs, mécaniques, monotones, trop sales ou désagréables, trop dépourvus de signification, méprisables, etc. Doivent être supprimés, remplacés, restreints, automatisés, améliorés, enrichis, compensés, récompensés, estimés ou mieux partagés.

Ce partage peut s'établir collectivement grâce au développement d'outil, tel que [Ekklesi.app](https://ekklesi.app/) notre plateforme modulaire de prise de décision collaborative et démocratique. Ces outils, en plus de permettre de faire fonctionner des organisations de façon horizontale, s'appuient sur l'intelligence collective et permettent donc de valoriser chacun pour ce qu'il a à apporter, cette valorisation du plein potentiel de chacun à pour effet d'augmenter considérablement le bien être et l'engagement de chacun, donc leurs motivations.

L'idée même est de transformer la nature du travail pour qu'il devienne une activité qui serait quelque chose comme la réalisation de soi, la manifestation de son humanité profonde par la transformation de la nature et la préservation de celle-ci. C'est en cela que le travail deviendrait le "premier besoin de la vie". Ainsi l'expression "travailler pour « gagner » sa vie", ne signifierait plus subir la peine de travail pour survivre et subvenir à ses besoins, mais bel et bien gagner sa vie par une activité riche de sens et d'expériences.

## La mesure de l'économie, le BIB et l'IHE

En 2010, Tim Jackson, directeur de recherche au centre de stratégie environnementale de l'université du Surrey, publie, Prospérité sans croissance. Il s'intéresse notamment au rapport entre le niveau de PIB des pays et l'évolution des éléments garant de la prospérité des populations (éducation, santé, espérance de vie...). Dans la forme économique actuelle, l'augmentation du PIB se découple assez rapidement de l'augmentation de la prospérité et même, au-delà d'un certain niveau de PIB, la prospérité tend à se décroître. Il confirme ainsi par les courbes ce que des penseurs tels Ivan Illich avaient déjà prédit depuis les années 1970.[^32]

Au niveau climatique, nous avons beau déjà techniquement entamer les débuts d'une révolution propre : les intensités en carbone ont déjà baissé en moyenne de 0,7% par ans depuis 1990. Mais cela n'a pas empêché les émissions d'augmenter de 40%. Car parallèlement, les échanges et la production se sont accrus. Ce mécanisme s'appelle l'effet rebond : dès que nous devenons plus efficients quelque part, nous utilisons les gains réalisés pour produire et consommer davantage.[^33]

<!-- Le PIB est un indicateur qui ne peut pas être décroissant, c'est un indicateur qui permet de savoir si on sera apte à payer les intérêts bancaires et rembourser les dettes. -->

Le PIB ne permet donc pas de mesurer si nous répondons bien à la question de recherche formulée dans le chapitre [Imaginer un autre monde](https://anotherpossible.world/fr/2-Story/Part1-TheBase/Chp1-ImagineAnotherWorld) : « Comment permettre à tous les humains actuels et des 80 millions de générations à venir d'avoir une vie épanouie sur Terre ? ». Il faut donc remplacer le PIB par de nouveaux indicateurs, ceux-ci pourraient être le BIB et l'IHE.

- BIB : Le bonheur intérieur brut[^34]
- IHE : Impact humain environnemental

L'objectif est alors d'avoir le Bonheur intérieur brut le plus élevé et l'Impact humain environnemental le plus faible voire même nul.

Ces deux indicateurs sont liés, il y a un plancher social qui constitue le but à atteindre pour assurer l’épanouissement de chacune et chacun, mais cet épanouissement ne peut se faire au-delà d’un cercle extérieur, qui est le plafond environnemental.

L'équilibre se trouve au sein de cette forme qui ressemble à un donut, c'est l’espace sûr et juste pour permettre à l’humanité de continuer d'avancer.

> Plus d'infos dans le chapitre [« Indicateurs d'une bonne économie »](https://anotherpossible.world/fr/2-Story/Part2-AnotherPossibleWorld/10IndicatorOfAGoodEconomy).
> {.is-info}

## Mode de production symbiotique

Pour permettre de garder cet équilibre, il y a un puzzle pour lequel lorsque les pièces sont dûment associées révèlent l'image, et la logique, d'un mode de production alternatif, symbiotique et régénérateur. De l'agroécologie et de la permaculture, de l'ingénierie écologique jusqu'à la construction d'une gouvernance horizontale, conforme au principe de subsidiarité, en passant par l'économie circulaire, la fabrication de biens modulaires, l'économie de fonctionnalité privilégiant l'usage et la mutualisation des biens à leur appropriation, l'augmentation de nos capacités de calcul par la mise en réseau décentralisé de nos machins existante, etc... on voit alors une nouvelle logique, sociale et économique, qui se profile.

Le mode de production symbiotique est un mode de production régénérateur de ses ressources. Plus l'homme produit selon ses principes dans le vivant, la technologie et la sphère sociale, plus il est capable de régénérer ses ressources et d'arriver à un point non seulement où il devient cocréateur des équilibres planétaires au lieu d'en être le fossoyeur, mais où il parvient également à créer plus de ressources qu'il n'en consomme.

> Plus d'infos dans le chapitre [« Mode de production »](https://anotherpossible.world/fr/2-Story/Part2-AnotherPossibleWorld/19ModeOfProduction).
> {.is-info}

## La place de l'argent dans la réciprocité

Nous sommes tellement habitués à lui que nous croyons tous que l'argent est une condition nécessaire de la vie en société, pourtant l'argent n'est pas un fait de nature, mais une invention imaginaire de l'humain qu'il est possible de critiquer.[^30]

Le fonctionnement de l'argent n'explique pas son origine, il ne s'explique pas de lui-même et c'est d'ailleurs la raison pour laquelle les économistes n'expliquent rien. Et ils ne s'expliquent pas non plus pour des raisons existentielles.

L'argent aurait soi-disant été inventé pour résoudre le problème de la double coïncidence des besoins posé par le troc, car sans argent, il était trop difficile de mener les échanges à terme. Comment comparer les biens entre eux, et comment faire si j'ai besoin de votre chaussure, mais que vous n'avez pas besoin de mes pommes ?

Malgré les nombreuses recherches anthropologiques qui prouvent le contraire, ce mythe est l'un des mieux ancrés dans la représentation que se font les individus de l'économie.

Dans les économies primitives, ils prenaient simplement ce dont ils avaient besoin, et d'un point de vue social, ils partageaient leurs ressources. Ignorant cette obsession de la rareté qui caractérise nos économies de marché, les économies primitives pouvaient miser systématiquement sur l'abondance.[^7][^8]

Certaines recherches anthropologiques qui se basent plus étroitement sur des éléments historiques et des découvertes empiriques amènent à penser que l'argent ne provient ni du travail ni de l'échange, mais qu'il est d'abord historiquement et conceptuellement une façon de créer un lien social entre des individus, car il représente la mesure des dettes contractées entre eux.[^12]

La différence avec le troc est qu’ici, un fermier qui a une abondance de blé, mais qui a besoin de légumes pour sa famille, demande à son voisin qui en une abondance de légume, mais celui-ci n'a pas besoin de blé. Alors, il écrit sur une tablette d'argile la promesse qu'il en donnera plus tard, comme ca son voisin pourra ainsi revenir avec cette tablette le jour où il aura besoin de blé. Ils n'ont donc pas directement troqué leurs produits, mais utilisé la "monnaie" pour mesurer les dettes contractées entre eux.

Cela peut être vu comme du troc différé, mais la nuance est importante. Ce fut donc la première étape, qui a mené par la suite aux premières formes primitives de la monnaie, sous forme de bétail, de coquillages, de pierres précieuses, de perles, peaux d'animaux, etc.

Certaines civilisations, comme les Incas, n'ont d'ailleurs connu ni la monnaie, ni le marché, ni le commerce. [^10]

Le problème avec l'argent, c'est que tant qu'il faut en avoir pour accéder aux échanges de biens et services, le capital peut jouer avec nous.

Ce n'est donc pas ayant la main sur le travail que le capitalisme assure sa domination, mais c'est en maîtrisant les conditions de la socialité qui rendent le travail nécessaire et qui comprennent, entre autres, la nécessité de l'argent.[^3]

L'argent marque la confiance des hommes entre eux. Car il faut une confiance inébranlable pour accepter d'autrui un signe monétaire imaginaire en échange d'un service rendu ou d'un bien fourni: confiance dans le fait qu'il ne s'agit pas d'un faux billet ou d'une fausse monnaie, confiance dans l'avenir que représente ce signe monétaire en tant qu'il rend possible des projets futurs. En fin de compte l'argent c'est l'horizon de la réciprocité à travers le temps, pas nécessairement envers celui qui m'a donné l'argent, mais confiance dans le fait qu'à l'avenir il y aura là quelqu'un qui pourra me rendre sous forme de biens et de services la valeur des biens et des services que j'ai provisoirement vendus contre de l'argent.

Car pourquoi utiliser un signe monétaire si j'ai vraiment confiance dans l'autre ? L'objet même, le concept même d'argent, traduit en réalité le manque de confiance en l'autre alors que si cette confiance est établie son utilité disparaît.

Si l'autre a vraiment confiance en moi et en la société, il peut me donner quelque chose sans me réclamer d'argent en retour, car il sait que, demain, c'est moi ou quelqu'un d'autre qui lui donnerait quelque chose.

La relation d'échange, médiatisée par l'argent, est une relation qui repose sur l'idée que les participants ne peuvent pas avoir confiance dans la durée de leur relation : ils utilisent en fait l'argent pour mener l'échange à terme, pour finaliser la dette, en somme pour n'avoir plus rien à faire les uns avec les autres une fois la transaction effectuée.

La discussion politique actuelle de nos économies porte sur la manière d'utiliser cet argent, afin qu'il fonctionne correctement, mais peut-être l'argent ne peut-il tout simplement pas fonctionner correctement, peut-être est-il voué à pervertir les relations sociales ?

Néanmoins s'il fallait tout de même lui conférer une nature, il vaudrait mieux dire que l'argent est voué à pervertir les relations sociales dans sa forme actuelle de moyen d'échange, plutôt que de "facilitateur de la circularisation de biens et services".

L'argent s'engouffre dans tous les vides laissés par l'absence de sens, il permet de garder le contrôle pour calmer notre cortex cingulaire, et la peur de la mort devient le moteur de l'avarice.[^11]

Beaucoup d'analyses notent bien les effets négatifs de l'influence de l'argent dans la société, mais elles ne parviennent pas à proposer une réponse adéquate au problème de la monétisation du réel.

Dans une société monétisée, la valeur des choses devient celle que leur donne l'argent.

Abolir l'argent nous semble impossible tant l'idée que l'argent nous est devenu naturel. La monétisation de la société semble en effet pour lui de plus en plus comme un fait en dehors duquel il n'est pas même possible de penser.

Le problème c'est que ce n'est plus l'individu qui détermine son système de valeurs et donc ce à quoi il aspire, mais que ce système de valeurs est rendu objectif dans l'argent.

Pourtant cette prise de distance de l'argent permet de retrouver un sens de la liberté par rapport aux valeurs des marchés, en se réappropriant nos valeurs.

Mais si nous continuons à penser que l'argent dans sa forme actuelle est la seule façon de résoudre la question de l'échange des biens, et donc la question de la rencontre entre les individus, en d'autres termes si nous pensons que l'argent est un prérequis de la socialité, nous aurons beaucoup de difficultés à distinguer la valeur économiques des valeurs éthiques et politiques.

Ce sont nos sociétés occidentales qui ont, très récemment, fait de l'homme un "animal économique". Mais nous ne sommes pas encore tous des êtres de ce genre. L'homo oeconomicus n'est pas derrière nous, il est devant nous ; comme l'homme de la morale et du devoir ; comme l'homme de la science et de la raison. L'homme a été très longtemps autre chose ; et il n'y a pas bien longtemps qu'il est une machine compliquée dotée d'une machine à calculer.

Nous cherchons ici à donner de la valeur à d'autres facettes de l'humanité que la relation économique d'échanges de biens.

Valeur qui permet à tous de s'épanouir dans une rencontre permanente avec les autres au sein d'une socialité basée sur des principes de partage et d'entraide.

## Conclusion

L'économie de réciprocité, bien qu'étant l'un des moteurs de ce premier récit eutopique promu par la fondation Eutopia, ne souhaite pas être vue comme une nouvelle idéologie portée par ce projet. Nous laissons le soin à ceux qui le souhaitent d'en développer les théories, l'histoire, de se l'approprier.

Nous pensons que cette économie a été trop peu étudiée, et nous souhaitons en développer la vision, car nous pensons que celle-ci à la capacité de faire émerger des projets innovants. Cette vision a également pour but de démontrer qu'il n'y a pas que les grandes idéologies qui vous sont proposées actuellement en politique, mais qu'il existe bien d'autres alternatives. Nous pensons que le plus sage reste encore de ne pas s'enfermer dans une, mais de rester continuellement ouvert à la nouveauté et à de nouvelle remise en question. C'est pourquoi ce récit n'est pas figé, mais a pour vocation d'évoluer et d'être mis à jour collaborativement et démocratiquement tout au long de l'histoire de l'humanité, par et pour elle-même.

En effet, pour élaborer une véritable transition vers une société meilleure, nous pensons qu'il est inutile de créer des idéologies figées qui n'appartiennent pas pleinement à ceux qui les défendent, et qu'il est inutile que des idéologies qui défendent les mêmes valeurs rentrent en conflit les uns envers les autres par désaccord sur les moyens d'arriver à une fin commune. Nous préférerons au contraire entretenir des relations amicales, créer l'ouverture, nous soutenir dans les projets respectifs.

Nous pensons qu'il est bien plus productif pour avancer de laisser une multitude d'alternatives émerger, et de ne pas utiliser son énergie et son temps à essayer d’éteindre les autres, car ce qui doit s'éteindre s'éteindra de lui-même quand il n'y aura plus personne pour y croire. À travers Eutopia nous ne voulons pas chercher à convaincre de front à tout pris, nous pensons que cela ne fait que perturber l'autre dans ces réflexions, et risque de créer un renfermement de chacun sur lui-même et sur ses idées. Nous pensons que ce n'est qu'en restant ensemble malgré nos opinions diversifiés, mais ouverts et unis dans la quête commune de notre épanouissement que nous pouvons sortir du gouffre dans lesquels nous sommes.

Sans chercher la confrontation, pour enlever tout doute possible, nous pensons qu'il est quand même essentiel de faire le parallèle avec les idéologies dominantes et d'expliquer en quoi l'économie de réciprocité présentée ici se différencie d'eux.

Nous ne cherchons donc pas à rejeter ces idéologies, car les valeurs derrière la gauche : « l'égalité », et la droite : « la liberté » sont belles et nécessaires à une Eutopie, mais nous pensons qu'il s'agit maintenant d'en rassembler le bon, ensemble.

Notre discours ne sera donc pas de juste dire "Vous avez tous tort", mais bien "Vous avez tous en partie raison, associons nos réflexions, nos idées et allons plus loin".

Les plans d'une transition seront discutés dans la partie 3 « Construire le nouveau monde », mais d'avance nous voulons préciser que nous ne cherchons pas du tout à passer du jour au lendemain à une société de réciprocité sans argent. À travers ce récit et cette analyse, nous cherchons à démontrer que c'est un horizon possible. En revanche nous pensons qu'il n'y aura que du bien à augmenter progressivement la part de réciprocité dans nos sociétés et nous nous engageons à contribuer cette tâche par la mise en place de multitude de projets innovants. La transition ne peut pas se faire exclusivement pour le changement du modèle économique, il faut en parallèle changer nos modes d'organisations et nos processus de décisions vers plus de démocratie, d'horizontalité et d'intelligence collective, il faut changer progressivement nos habitudes, nos croyances, élever nos consciences, il faut repenser nos indicateurs économiques, transformer nos modes de production pour les rendre symbiotiques, et pour ce qui est du modèle économique en tant que telle, il faut passer par différentes étapes intermédiaires afin de garder un équilibre et une cohérence avec les progrès réaliser dans les autres étapes réaliser en parallèle.

### Comparaison avec les idéologies de gauche

Ces systèmes idéologiques qui prônent plus de planification sont ceux qui pourraient le plus ressembler à s'y méprendre à celui de la réciprocité générale, pourtant quelques mots suffissent à totalement modifier le système pratique qui en ressort.

Socialisme = "De chacun selon ses capacités et à chacun selon son travail"
Communisme = "De chacun selon ses capacités et à chacun selon ses besoins"[^27]
Réciprocité = "De chacun, volontairement, selon les besoins des autres"[^3]

On pourrait voir dans ces idéologies, des insuffisances et des défauts qui sont ont en partie les causes essentielles de ce qu'est le monde actuellement. Ils ont suscité des déviations, des révolutions anticapitalistes, mais ils ont plus ou moins échoué à travers celle-ci, elles ont créé sur une large partie du globe des sociétés de type nouveau. Et puisque le résultat n'a pas été meilleur que le capitalisme développé, ils ont ainsi protégé celui-ci. Ils sont désormais utilisés par tous les bords pour constituer les plus gros mensonges et hypocrisies de l'histoire.

L'égalité entre personnes, quelle que soit sa définition, peut être une limite à la liberté de certaines personnes. Et la liberté seule n'assure en général pas l'égalité.

La meilleure des sociétés est probablement très certainement fort différente de celles que nous connaissons telles que notre société à classes sociales de dominantes et dominées. Mais si ceux qui ont tenté de la réinventer échouent, nous pensons que c'est d'abord par ce qu'elles ne précisent pas assez les sociétés qu'elles proposent.

Si cette approche analytique était nécessaire, il faut aujourd'hui la dépasser pour continuer le travail de dénaturalisation génétique entrepris par Karl Marx.[^28]

Le système marxien ne peut d'ailleurs penser l'argent ni avant ni après le capitalisme, en plus de rejeter dans l'ombre deux mille ans d'histoire humaine, il ne peut proposer aucune solution pour en sortir.

Les difficultés de la pensée de Marx résident dans la possibilité de proposer concrètement une autre forme d'organisation du travail et de la socialité économique que celle du mode de production capitaliste.

Ainsi à l'ère de la noosphère, les mouvements marxistes qui traînent avec eux des théories d'une autre époque ont de grande difficulté à réimaginer des organisations de travail horizontal autogéré, à réinventer nos processus démocratiques, à imaginer un monde autre que sous le productivisme, à comprendre l'être profond que nous sommes et les moyens de le transformer. Ils se battent alors bien trop souvent CONTRE le capitalisme, et ont du mal à apporter de véritables solutions alternatives pour lesquels nous pourrions nous battre POUR.

Nous ne critiquons pas ici l'œuvre magistrale de Marx, qui était bien nécessaire à la réflexion, mais nous pensons qu'il faut aller plus loin, que s’il avait avoué sincèrement qu'il faisait aussi de l'éthique et en avait placé bien plus dans ses œuvres, que s’il en avait dit plus sur la société finale ou bonne, et sur les moyens d'y parvenir, s’il avait mis autant d'altruisme dans ses caractéristiques qu'il n'en avait en lui, peut être moins du pire serait-il arrivé.[^3]

De plus nous pensons qu'il ne faut pas mettre autant d'énergie sur la lutte des classes dans le changement de société, car ces classes finiront par disparaître d'elle-même sur le chemin. Ces classes ne sont plus aussi clairement distinguées qu'auparavant, il n'y a plus seulement la bourgeoisie et les prolétaires, mais elles sont largement divisées, entrelacées et ont des intérêts liés les unes envers les autres. Les analyses marxistes ne prennent pas assez en compte l'aspect psychologique de ces classes pour les comprendre, si on le fait on se rend compte qu'elles sont en autres toutes poussé par une quête d'épanouissement tel que détaillé dans le Chapitre 4 sur HomoSapiens, qu'elles ne se considèrent pas comme les méchants de l'histoire, mais pensent au contraire contribuer au bien collectif. Il faut comprendre qu'il est autant difficile pour tout le monde de sortir de son comportement au niveau neurologique même si les impacts de nos comportements peuvent avoir d'énorme différence entre elles. Nous préférons donc nous éloigner de ces longues analyses théoriques pour ne plus nous enfermer dans une lutte les uns contre les autres, mais plutôt chercher à montrer les bénéfices d'un tel système pour tout le monde. Ainsi nous ne cherchons pas à nous battre contre un adversaire, mais préférons plus tôt nous battre pour quelque chose, et y inviter tout le monde, en démontrant par nous même que celui-ci mène à notre épanouissement, non pas seulement dans la fin, mais surtout à travers les moyens mêmes qui nous amènent à cette fin.

### Comparaison avec les idéologies de droite

Les politiques de droite sont celles qui font la promotion du marché. Ce sont actuellement les idéologies dominantes au pouvoir dans les pays occidentaux.

Chacun en perçoit les effets négatifs, non seulement sur la planète et nos écosystèmes, mais aussi sur notre épanouissement global. Dans ce système économique, nous passons notre temps à le perdre, et nous perdons notre vie à la "gagner". Et ceux qui ne jouent pas à ce jeu, que ce soit subit ou non, en récolte tout de même les conséquences désastreuses à un moment ou un autre.

Dans le marché, les gens ont toujours des chaînes. Mais pour la plupart, elles ne sont plus de grossières chaînes d'acier rouillé, elles sont "finement nickelées" beaucoup plus "standing" et finalement encore plus difficile à briser, d'autant que certains y glissent des maillons d'or. Nous avons alors l'impression d'avoir quelque chose de précieux à perdre.

Notre système coince chaque individu par l'ensemble des autres, en ce sens qu'une personne ne peut simplement pas, seule et librement, manifester ses plus belles qualités. En économie, dans un système concurrentiel, une entreprise ne peut pas, sous peine de faillite, ne pas chercher à maximiser son profit si les autres cherchent à maximiser le leur. Elle ne peut pas non plus produire trop durablement au risque de ne plus avoir d'acheteur sur le long terme. De même quelqu'un qui donne ne peut pas survivre dans une société ou les autres prennent.

Alors il y a du bon dans ce système, comme la liberté d'entreprendre, et la « liberté » qui y est plus importante que sous la planification. On ne peut pas non plus nier les progrès accomplis. Mais on ne peut pas nier non plus les conséquences que ça a eues, car là où peu gagnaient au jeu du marché, la majorité ne pouvait que perdre... la richesse et le confort matériel des uns, fut non seulement la destruction de notre environnement, mais aussi la misère des autres... ceux-là ont été exploités, délaissés, oubliés... Avec ce récit on ne pourra plus nier qu'il n'est pas possible de faire mieux que le capitalisme en termes de liberté, de confort, d'innovation et d'épanouissement, tout en assurant la réalisation de toutes les autres aspirations profondes de l'humain en restant en symbiose avec notre environnement.

C'est ici que s’arrêtera cette première version de notre analyse quant à la comparaison avec l'idéologie de droite dans un premier temps. Non pas qu'il n'y ait pas grand-chose à dire, il y a au contraire quantité énorme de littérature sur le capitalisme détaillant en long et en large ses origines, ses causes et ses conséquences. Mais le regard que nous voulons porter sur ces idéologies, les relations que nous souhaitons entretenir avec elles, doivent encore être défini collectivement. Néanmoins, si vous jugez ne pas en savoir assez à ce propos, nous vous conseillons à votre guise d'aller consulter les nombreuses ressources disponibles dans la littérature et sur Internet.[^29][^31]

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="5aef3cb8-13dd-2cf8-9d57-d43b9693f8b2" ></div>
</div>

## Sources

[^1]: Earth Overshoot day 2017 fell on August 2, Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipédia : Economie](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: La Fin de la croissance. Richard Heinberg. 2013
[^5]: Sébastien Bohler. Le Bug humain. 2020
[^6]: Karl Polanyi."The great transformation" et "The economy as Instituted Process " dans Trade and Market in the Early Empires : Economies in History and Theory.
[^7]: Marshall Sahlins - Stone-age Economics
[^8]: Marcel Mauss- Essai sur le don
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Organisation sociale des Incas](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Où est le sens ? 2020
[^12]: David Graeber. Dette: 5000 ans d'histoire. 2011
[^13]: Lee Alan Dugatkin. The Altruism Equation: Seven Scientists Search for the Origins of Goodness. 2006
[^14]: [Nombre de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Une brève histoire de l'humanité
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[^17]: Daniel Kahneman. Thinking, Fast and Slow
[^18]: [Ernst Fehr, The nature of human altruism](https://www.nature.com/articles/nature02043)
[^19]: [The Need for Social Approval](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religion and Morality](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruism in children and the perceived conduct of others in Journal of Abnormal and social Psychology, 1942, 37, p. 2018-233
[^22]: Gabriel Tarde. Les lois de l’imitation, 1890
[^23]: Charles Duhigg. The Power of Habit. 2014
[^24]: Émile Durkheim. Éducation et sociologie; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORY OF NORMATIVE CONDUCT: A THEORETICAL REFINEMENT AND REEVALUATION OF THE ROLE OF NORMS IN HUMAN BEHAVIOR
[^26]: [L’étymologie du mot travail est-elle tripalium ?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Critique du programme de Gotha (1875). 1891
[^28]: Karl Marx. Le capital. 1867
[^29]: [Conférence: Communisme & Capitalisme : l'histoire derrière ces idéologies](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. L'ordre de l'argent - Critique de l'économie. 2019
[^31]: [Documentaire Arte 2014 - Ilan Ziv - Capitalisme](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
[^32]: Tim Jackson. Prospérité sans croissance. 2009
[^33]: [Comprendre l'effet rebond](https://theothereconomy.com/fr/fiches/comprendre-leffet-rebond/)
[^34]: [Bonheur national brut](https://fr.wikipedia.org/wiki/Bonheur_national_brut)
[^35]: Hannah Arendt, Les origines du totalitarisme, 1951
[^36]: Albert Camus, La peste, 1947
[^37]: Jean-Baptiste Fressoz, Sans transition Une nouvelle histoire de l'énergie : La transition énergétique n'aura pas lieu, 2024
