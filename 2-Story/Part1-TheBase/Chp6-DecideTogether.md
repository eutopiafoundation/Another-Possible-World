---
title: Décider tous ensemble
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Cette page est encore en cours de rédaction et il est donc possible que certaine partie soit manquante.
> {.is-info}

## Comment décider tous ensemble ?

Il faut comprendre comment on peut prendre des décisions et faire en sorte que ces décisions soit bien les bonnes pour atteindre notre objectif commun.

De nombreux outils de prise de décision existent et de nombreux autres sont encore à inventer. Nous laisserons ici les outils de précision à plus petite échelle de côté pour nous concentrer sur celui à l'échelle globale.

Dans notre société occidentale, l'outil employé est la démocratie. L'analyse de celle-ci et sa réinvention nous permettra par la suite de développer des outils à plus petite échelle qui s'en inspirent et qui seront ainsi adaptés à d'autres modes d'organisation, tel que des entreprises, des écoles, etc...

## Qu'est-ce que la démocratie

Démocratie est la combinaison en grec de demos "le peuple" et kratos "le pouvoir". Ce terme désigne aujourd'hui tout système politique dans lequel le peuple est souverain.[^1]

De nombreux pays ont conquis l'égalité du pouvoir politique dans le suffrage universel. Cette victoire est maintenant inscrite dans leur constitution et est jugée comme l'un des plus brillants joyaux institutionnels de leur société.

Mais le mot "Démocratie" est un mot encore bien malmené et mal utilisé.

Nous clamons la démocratie, comme si nous avions réellement le pouvoir.

Mais notre démocratie n'est qu’une abdication récurrente de notre pouvoir légitime à des politiciens qui s'en emparent.

Et une fois qu'ils l'ont obtenu, ils se sentent alors légitimes de l'avoir, mais même eux sont sous le contrôle d'un pouvoir bien plus grand.

On pense que le système politique domine et qu'il peut changer tout le reste, dont l'économie et sa forme. On pense que notre système est parfaitement démocratique à cause de la concurrence des candidats pour lesquels on accorde notre vote.

Sauf que nous verrons que l'économie intervient dans ce choix concurrentiel. Alors d'une part le système électoral n'a plus cette qualité d'offrir une bonne concurrence des candidats, d'autre part il ne domine pas l'économie puisque celle-ci l'influence.[^2]

C'est donc l'économie qui domine la politique.

Par exemple pour les questions en matière climatique, les études montrent qu'il n'y a pas de corrélation entre ce que la majorité des gens veulent et ce qu'ils obtiennent, sauf quand ils ont les mêmes préférences que les 10% les plus riches de la population.[^3]

Notre démocratie est complètement à revoir, nous parlerons ici de nombreuses failles, abus de pouvoir, corruption, influence des lobbys, décisions non concertées, non partagées, failles de la représentativité, illusions du choix, inégalités des chances.

Et c'est là que l'on découvre que le résultat réel de notre système démocratique est à l'opposé de son aspiration idéologique.

Malgré toutes ces failles, la démocratie est un risque à prendre[^4] si nous voulons préserver notre liberté.

Alors nous allons d'abord analyser notre démocratie, comprendre ce qui pose problème, pour en suite réinventer un tout autre système démocratique

## Le problème de nos démocraties

### L'entrepreneur politique[^5][^6][^7]

Le but de tout homme politique est d'être et de rester au pouvoir, c'est l'objectif qu'il entreprend. Il fait carrière, ce qui lui permet notamment de gagner sa vie et de trouver sa place au sein de la société.

Ils sont en concurrence les uns avec les autres, mais aussi en connivence : ils veulent détruire leur adversaire politique, mais pas la fonction politicienne.

Tous les politiciens ont, quelles que soient leurs oppositions, un intérêt commun au renforcement, et non à la diminution du pouvoir des politiciens. Et si l'un d'entre eux en suggère l'idée, il recevra la désapprobation de tous les autres.

Les entrepreneurs politiques proviennent en très grande majorité de la même classe sociale, et généralement aussi de celle des dirigeants de l'économie privée et des mêmes familles bourgeoises.

Les candidats ne cherchent pas à présenter le programme qui constitue leur idéal de la bonne société.

Quelles que soient leurs fins personnelles, ils choisissent le programme qui convient le mieux au plus grand nombre. Ils en viennent à sacrifier la défense de leurs propres opinions, idéaux et préférences personnelles, s'ils en ont, à celles de ce que veut la masse de la population.

Néanmoins cette connaissance des désirs des électeurs par les candidats n'est pour la plupart pas utilisée pour servir le plus grand nombre, mais nous le verrons, il est utilisé pour l'exploiter au profit du plus petit.

Les politiciens ne font que réagir aux incitations qui leur permettent d'acquérir et de rester au pouvoir.

Il ne sert d'ailleurs à rien de les haïr, ils ne font que suivre les règles d'un jeu. Dès lors c'est le jeu qu'il faut haïr et non pas les joueurs.[^8]

Mais le problème des entrepreneurs politiques c'est que leur objectif n'est donc pas d'avoir raison, mais que le peuple et les entreprises leur donnent raison.

Les entrepreneurs politiques ne sont pas stupides, ce sont de fins tacticiens qui réagissent aux incitations du jeu et sont très bien conseillés par l'expertise de leur équipe en la matière, ils usent alors de différents stratagèmes et alliances pour arriver à leurs fins.

Les candidats pour présenter une bonne image en viennent alors à cacher leur personnalité, à être dissimulateurs, à s'effacer dans une hypocrisie.

L'électeur pense savoir pour qui il vote, car l'accent est souvent mis sur la personnalité du candidat, en faisant valoir sa séduction, son caractère sécurisant ou son charisme, provoquant notamment un effet de halo. Et ces impressions sont bien plus facilement manipulables par les médias que le contenu d'un programme.

### Les programmes politique[^2]

Les candidats construisent leur "programme" non pas en une fois et d'un seul coup, mais par additions ou modifications successives de déclarations et d'actions, sans cependant pouvoir beaucoup revenir dessus, effacer, ou contredire, les mots ou actes antérieurs. Cette contrainte s'explique par le temps très coût pour faire connaître un programme alors que le préciser au fur et à mesure, ajouter, le renforcer atteint mieux les électeurs.

D'autre part, cela s'explique car ces contradictions ou reniements nuiraient à la fiabilité et la crédibilité du candidat, ce qui crée une structure évolutive, itérative des connaissances et des constructions des programmes.

Cela entraîne donc souvent des différences dans leurs programmes et une imperfection qui entrave la qualité démocratique du résultat du processus.

Les programmes sont d'autre part globaux, portant sur tous les sujets à la fois, et on doit choisir entre eux, or on peut fort bien préférer l'un sur un point et l'autre sur un autre point.

Alors les programmes en viennent à diviser la population, l'un d'entre eux pourrait se faire élire à la majorité alors que cette même majorité préfère un point de l'ensemble des autres programmes que celui du vainqueur.

C'est notamment quelque chose que permettent les référendums spécifiques, d'émettre son avis sur chaque question séparément.

L'effet des contributions financières intéressées est plutôt d'uniformiser les programmes, tandis que celui des soutiens idéalistes/militants est aux contraire de les différencier.

### L'honnêteté et la transparence

Tout système qui peut être manipulé sera manipulé, et ce au pire moment.[^9]

Quand on conçoit un système, il faut absolument se demander comment les parties prenantes du système vont pouvoir l'attaquer et tirer parti de façon malhonnête du système.

L'honnêteté et la transparence sont des problèmes persistants dans les systèmes démocratiques à travers le monde. Les politiciens et les élus sont souvent accusés de manquer de transparence, d'être corrompus, et de se retrouver au sein de nombreux conflits d'intérêt en favorisant des intérêts particuliers plutôt que l'intérêt général.

Dans notre système certaines règles ne promeuvent pas l'honnêteté et l'éthique.

Par conséquent, il est crucial de concevoir des systèmes qui incitent les gens à agir de manière éthique et morale.

Il faut développer le principe de révélation où les participants ont tout intérêt à être honnêtes dans leurs révélations, et à se comporter de manière éthique et morale, car cela serait dans leur intérêt et les avantagerait sur le long terme. Il faut inclure des mesures de transparence accrue.

### Le choix des alternatives

Outre le fait que les programmes soit globaux, le choix des programmes ou candidats proposés au choix de l'électeur sont en général en petit nombre.

Le financement des campagnes électorales est l'un des facteurs limitant le choix des alternatives. Les candidats sont ceux qui ont accès à des sources de financement et peuvent déployer assez d'informations et de propagande sur eux pour visibiliser leur partie et avoir des chances de gagner.[^10]

Les partis politiques peuvent avoir des structures internes qui favorisent certains candidats ou tendances politiques au détriment d'autres. Cela peut limiter les choix des électeurs en ne leur donnant que quelques options qui ont été approuvées par les partis politiques.

### La délégation du pouvoir

Une élection est deux choses : un vote et une délégation de pouvoir

Comme dit plus haut, notre démocratie est l'abdication récurrente du pouvoir légitime du peuple à un groupe de personnes.

La délégation du pouvoir peut également entraîner une perte de contrôle sur les décisions. Si une personne ou un groupe délègue son pouvoir à une autre personne ou un autre groupe, elle peut perdre le contrôle sur les décisions qui sont prises et sur la façon dont elles sont mises en œuvre.

Sous prétexte de la délégation qui donne toute légitimité aux élus, il peut y avoir détournement, aliénation, spoliation ou usurpation de pouvoir.

L'un des principaux problèmes de la délégation du pouvoir est le risque de déresponsabilisation. Si une personne ou un groupe délègue son pouvoir de prise de décision à une autre personne ou un autre groupe, elle peut se décharger de sa responsabilité en cas d'échec ou d'erreur de la part de la personne ou du groupe auquel elle a délégué son pouvoir.

Un autre problème de la délégation du pouvoir est le risque de conflit d'intérêts.
Certaines opérations demandent du temps, de l'énergie, des connaissances préalables et des capacités intellectuelles. Il peut alors être profitable, ou même nécessaire, de faire appel à la division du travail et à la spécialisation en déléguant une partie du travail de décision. Mais cette délégation est du pouvoir, par exemple, un gouvernement peut déléguer son pouvoir à une entreprise pour gérer un projet. Mais comme le délégataire ne sait pas exactement ce qu'il délègue puisque par définition même, il n'a pas toute l'information sur les possibilités. Cela laisse la porte ouverte à tous les abus.[^2]

### La centralisation du pouvoir

Il y a centralisation quand peu décident beaucoup et beaucoup décident peu.

La centralisation augmente la délégation de pouvoir qui elle-même diminue la démocratie.

Lorsqu'un petit groupe de personnes détient un pouvoir absolu, cela peut entraîner une prise de décision unilatérale et des politiques injustes qui ne tiennent plus compte des intérêts et des opinions des citoyens.

La centralisation du pouvoir peut rendre les gouvernements plus vulnérables aux abus de pouvoir, à la tyrannie et à la dictature, car il y a moins de contrôle et de contre-pouvoirs pour limiter l'autorité d'un petit groupe de personnes.

### Les modes de scrutin

Il est important de prendre conscience qu'un candidat élu est élu par un scrutin et que modifier le scrutin, modifiera l'élu.

Car le processus électoral affecte grandement l'issu d'un vote. Donc le choix du scrutin affecte le futur de nos sociétés. Il est donc important de se concentrer sur le choix du scrutin que l'on veut mettre en place.

L'élection à un tour et l'élection à 2 tours ont un problème fondamental, elle ne permet pas d'exprimer les préférences des électeurs de manière claire et précise[^11]

Dans un système à un tour, l'électeur ne peut voter que pour un seul candidat, ce qui peut être problématique si plusieurs candidats ont des soutiens importants. Par exemple, dans une élection où il y a trois candidats majeurs, l'électeur doit choisir entre trois options différentes, mais n'a qu'un seul vote pour exprimer son choix. Cela peut conduire à des situations où le candidat élu n'a pas le soutien de la majorité des électeurs, mais simplement le plus grand nombre de voix.

Le problème de l'élection à deux tours est qu'elle peut parfois conduire à des tactiques de vote stratégique et à des alliances entre candidats. Dans un système à deux tours, les deux candidats ayant obtenu le plus de voix au premier tour s'affrontent lors d'un second tour. Cela peut conduire à des situations où les électeurs votent différemment au premier tour dans le but de garantir que leur candidat préféré se qualifie pour le second tour, même s'ils ne sont pas vraiment convaincus par ce candidat. De plus, il peut y avoir des cas où des candidats s'associent pour pousser un autre candidat hors de la course, plutôt que de chercher à gagner l'élection de manière indépendante.

De manière générale le vote à majorité présente le grave défaut de ne pas tenir compte des intensités des préférences des citoyens. Ceux qui sont presque indifférents entre deux alternatives, et ceux qui préfèrent beaucoup plus l'une que l'autre ont le même poids dans le choix entre elles. Par exemple 1001 citoyens qui préfèrent à peine une alternative l'emportent sur les 1000 autres pour qui elle est la pire catastrophe.

Ces scrutins encouragent le vote utile qui encourage les politiciens à s'organiser sous forme de parti politique et placer l'électeur dans des cases prédéfinies, ce qui renforce l'effet de bipolarisation

La bipolarisation de l'électorat en politique fait référence à la tendance des électeurs à se regrouper autour de deux partis politiques principaux, créant ainsi un clivage politique distinct entre ces deux partis. Laissant peu de place pour les partis ou les candidats indépendants ou pour des partis politiques de plus petite taille. Ce qui a également pour conséquence négative de conduire à une polarisation accrue des opinions, une diminution de la diversité des points de vue

Avec ces scrutins, on en arrive à voter contre un candidat et pas pour.

Mais le vote utile ne rend pas légitime l'élu.

La proportionnelle permet de ne pas trop polariser la politique, mais peut conduire à une fragmentation du pouvoir et à une difficulté de former des gouvernements stables. Il devient difficile pour un parti de remporter la majorité absolue des sièges, ce qui peut conduire à des coalitions gouvernementales instables ou à des gouvernements minoritaires qui ont du mal à faire avancer leur programme.

Un bon scrutin doit être indépendant aux alternatives non pertinentes et fermer la porte de dilemme du vote utile.

Exemple d'autre scrutin possible :

Le vote par approbation : chaque électeur peut approuver autant de candidats qu'il le souhaite, sans ordre de préférence. Le candidat ayant reçu le plus d'approbations est élu.

Le vote par points attribués (ou vote pondéré) est un système électoral alternatif dans lequel chaque électeur dispose d'un certain nombre de points qu'il peut répartir entre les candidats.

Le vote par jugement majoritaire : les électeurs attribuent une note à chaque candidat, par exemple de "très bien" à "à rejeter". Le candidat ayant la note moyenne la plus élevée est élu.

Le scrutin de Condorcet est un système de vote où les électeurs votent en comparant tous les candidats deux par deux.[^12]

Le scrutin de condorcet randomisé: ici chaque électeur compare les paires de candidats comme dans le scrutin de Condorcet, mais au lieu de prendre en compte tous les votes de tous les électeurs, on sélectionne un certain nombre de comparaisons au hasard pour déterminer le gagnant.

Le scrutin Mehestan ( Tournesol) : C'est un système de vote qui combine le vote par points et le scrutin de Condorcet. Elle propose de comparer un à un chaque candidat et de dire celui que l'on préférerait pour chaque critère, mais aussi de définir le niveau de préférence de ce critère. Cette méthode de vote vise à être à la fois équitable et représentative, en permettant aux électeurs de voter pour leur candidat préféré sans risque de diviser le vote et en prenant en compte les préférences de tous les électeurs dans la détermination du gagnant.

### Les partis politique

Ces modes de scrutins vus plus haut encouragent les politiciens à s'organiser sous forme de partis, à cause de cette propriété qu'est la dépendance aux alternatives non pertinentes.

### La lenteur institutionnelle

Dans de nombreux cas, les problèmes politiques nécessitent des solutions rapides et efficaces pour minimiser les dommages et les pertes. Cependant, la lenteur des processus politiques, tels que les négociations, les débats, les votes et la bureaucratie, peuvent retarder la prise de décision et empêcher les gouvernements d'agir rapidement.

### Les mandat court terme

Les élus en question le sont pour un certain temps, en général quelques années. Pendant ce temps, ils disposent d'une substantielle liberté personnelle du choix de leur action. Les électeurs ne les contraignent plus en rien. Très peu de choses forcent les élus à tenir leurs promesses électorales, mis à peur leur honneur, mais ceux-ci peuvent très bien dire que les choses ont changé.

Les élus sont souvent plus enclins à se concentrer sur les résultats à court terme plutôt que sur les défis à long terme.

Car ces mandats encouragent les élus à se concentrer sur des enjeux politiques qui sont plus susceptibles de susciter un soutien rapide de la part des électeurs plutôt que sur des questions plus complexes qui nécessitent un engagement plus important et à plus long terme

Les politiciens peuvent être tentés de prendre des décisions qui sont populaires à court terme afin de maximiser leurs chances de réélection. Cela peut conduire à des politiques qui ne sont pas viables ou durables à long terme et qui peuvent causer des problèmes plus importants à l'avenir. Ce qui conduit à une approche politicienne de la gouvernance plutôt qu'à une approche plus pragmatique et réfléchie.

### Le bluff à l'expertise

Le "bluff à l'expertise" en politique se produit lorsque les politiciens utilisent leur position à l'aide de diplôme en science ou en administration pour donner l'impression qu'ils ont une expertise ou des connaissances particulières sur des sujets, alors qu'en réalité, ils n'en ont pas. Ils peuvent utiliser cette stratégie pour manipuler ou persuader les électeurs, mais cela peut conduire à de mauvaises décisions politiques.

La valorisation des diplômes en science ou en administration conduit à une concentration de pouvoir et d'influence entre les mains de quelques élites qui ont eu la chance d'accéder à ces formations. Ce qui engendre une perte de diversité et d'inclusion dans les cercles de décision, car les personnes issues de milieux moins privilégiés ont souvent moins d'opportunités pour obtenir ces diplômes.

Pourtant, la possession d'un diplôme ne garantit pas nécessairement des compétences ou une expertise en matière de gouvernance ou de politique. Le bluff à l'expertise, dont nous avons parlé précédemment, peut être utilisé par des personnes qui possèdent des diplômes, mais qui manquent de connaissances réelles ou de compétences pour prendre des décisions éclairées.[^2]

### La science bafoué

Les décisions politiques doivent souvent être prises à partir de données scientifiques complexes et de preuves empiriques, notamment dans des domaines tels que la santé publique, le changement climatique, la sécurité alimentaire, etc. Lorsque les décideurs politiques bafouent ou ignorent les preuves scientifiques, ils prennent des décisions qui ne sont pas basées sur des faits et des données vérifiables, mais plutôt sur des croyances ou des opinions. Cela peut avoir des conséquences graves pour la société, notamment en matière de santé, d'environnement ou de sécurité.

### La propagande

L'électeur ne vote pas pour un candidat dont il ne connait rien. Sans information sur lui transmise aux électeurs, un candidat n'a aucune chance. Il ne commence à en avoir que lorsqu'elle dépasse un certain montant: ses chances d'être élu croissent quand cette information augmente.

Le placement politique est un investissement comme un autre, la quête est toujours la même, le profit.

L'histoire de l'établissement de la "démocratie électorale" est parallèle aux luttes et aux victoires formelles pour la liberté d'expression, de presse, de réunion, d'associations. Mais ces libertés, de par leur base matérielle et économique nécessaire, ont été pour l'essentiel récupérées par ceux qui détiennent ces moyens économiques. Et ainsi d’en faire l'usage pour la propagande électorale.[^24]

La propagande est utilisée pour manipuler les opinions publiques et influencer les résultats des élections ou des référendums en faveur d'un parti ou d'une idéologie particulière. Celle-ci peut être trompeuse, partisane et basée sur des mensonges, des demi-vérités ou des omissions de faits importants. Elle peut également jouer sur les peurs, les préjugés et les émotions des gens plutôt que sur la raison et la logique.

### L'hooligan politique

On peut distinguer 3 types caricaturaux de citoyens lors des élections:[^13][^14]

- Les hobbits : Ceux qui ne s'intéressent pas à la politique et qui sont très ignorants dans ce domaine, ils n’ont pas trop de partis pris.
- Les hooligans : On un parti pris, mais très pris, souvent victime du biais de confirmation, et défendent des valeurs de façon irrationnelle.
- Les vulcans : Personne capable de penser rationnellement.

Nos modèles supposent que les citoyens sont des vulcans ou peuvent le devenir, mais en pratique les gens sont majoritairement soit des hobbits soit des hooligans.

L'intuition nous force une opinion, la raison est ensuite un outil pour donner raison à ces opinions. Nous rationalisons nos convictions.

Nous n'utilisons pas notre raison pour sonder la vérité, mais pour justifier les positions idéologiques que l'on a déjà adoptées.

Par exemple si l'on donne le mot "Démocratie" notre intuition nous fait dire que c'est quelque chose de bien, et puis notre raison va ensuite manipuler la définition du mot pour justifier notre intuition.

Désormais il devient facile de confirmer notre intuition grâce à internet qui peut nous fournir tous les arguments allant dans notre sens, et si ces arguments ou chiffres ne vont pas dans notre sens il nous est toujours possible de remettre en question les motivations du rapporteur des faits ou des les interpréter d'une autre manière.

De plus, en nous entourant de gens qui pensent comme nous, notre environnement ne cesse de conforter notre intuition et il a tendance à la polariser.

Les militants politiques en deviennent démesurément sûrs d'eux et il devient impossible de les faire changer d'avis même avec des faits et des chiffres.

Et lorsque l'intuition prédomine ainsi, plus on s'informe, plus on trouve de bonnes raisons de croire ce que l'on croit, peu importe l'information qu’on lit.

L'engagement politique ne fait alors que renforcer l'hooliganisme politique.

Plus on s'intéresse à un parti politique avec un parti prix, plus notre penchant initial va se radicaliser.

Des vidéos et articles scientifiques trompeurs, mais bien expliqués renforcent dans leur conviction scientifique ceux qui les regardent et les lisent.

Comme la sélection de groupe est plus efficace que la sélection individuelle, les tribus qui ont survécu sont celles où les individus ont suffisamment collaboré pour la survie du groupe.

Et donc ceux qui sont prêts à sacrifier leur intérêt individuel pour le bien du groupe. La sélection naturelle a sélectionné l'hooliganisme politique.

Un vote basé sur l'hooligan politique procure une jouissance alors qu'un vote rationnel demande un effort mental.

### L'ignorance et l'irrationalité du citoyen

Les gens sont ignorants, la quasi-totalité des électeurs n'ont aucune notion en science ou compréhension du système politique, sont mal informés et ne connaissent pas les chiffres.[^16]

Certains d'entre eux n’ont pas conscience d'être dans un hooliganisme politique.

La majorité d'entre nous ignore nos différent biais cognitif et effet auxquels nous faisons face. Nous avons passer un certain nombre de ceu-ci dans le Chapitre 3 sur HomoSapiens. En voici certains pour rappel :

- Biais de confirmation[^17]
- L'effet de halo[^18]
- Dissonance cognitif[^19]
- Pression des paires
- Éristique de disponibilité[^20]

L'école et les diplômés peuvent faire croire à des gens qu'ils sont des savants, leur faire croire qu'ils sont plus intelligents que d'autres, et créer un excès de confiance dans des domaines qui ne sont pas le leur.

Tout le monde se retrouve si sûr de soi. Et le problème est que l'incertitude est vue comme un signe de faiblesse alors qu'il est une qualité fondamentale.

L'effet Dunning-Kruger montre que des personnes qui ont peu de connaissances ou de compétences dans un domaine ont tendance à surestimer leur propre niveau de maîtrise, tandis que les personnes qui ont des compétences plus élevées ont tendance à sous-estimer leur niveau de maîtrise.[^16]

Mais l'ignorance des citoyens n'est pas le plus grand problème, car même informer le citoyen ne l'empêche pas de voter de manière irrationnelle.

Les gens se comportent comme des hooligans et non pas comme des scientifiques.

### Le temps et l'énergie pour s'informer

S'informer, apprendre, savoir, comprendre, puis choisir et décider, et voter, requiert, de la part de l'électeur, du temps, de l'énergie, de la connaissance antérieure qui elle-même demande du temps et de l'effort. Ces coûts expliquent une certaine délégation de pouvoir.

Les campagnes électorales sont souvent courtes et intenses, laissant peu de temps aux électeurs pour s'informer et évaluer les différentes options. De plus, les médias ont souvent tendance à se concentrer sur les aspects les plus spectaculaires ou controversés des événements politiques, plutôt que de fournir une analyse approfondie et nuancée.

Cela peut conduire à une polarisation de la politique et à une réduction de la qualité du débat public. Les électeurs peuvent être tentés de se tourner vers des sources d'information simplistes ou partisanes qui ne reflètent pas nécessairement la complexité des enjeux.

### Le communautarisme

Le communautarisme peut être défini comme une tendance à accorder plus d'importance à l'appartenance à un groupe ou à une communauté qu'à l'individu en lui-même. Le principal problème du communautarisme est qu'il peut conduire à une fragmentation de la société, où chaque groupe se concentre sur ses propres intérêts plutôt que sur l'intérêt commun de la société dans son ensemble.[^21]

Le communautarisme pouvait être géographique, mais désormais celui-ci s'éteint à tout l'internet.

Personne n’aime faire partie de la minorité.

Quand un internaute cesse de participer à une discussion, car il se sent trop minoritaire, cela provoque la ségrégation idéologique

Changer de quartier idéologique sur internet et bien plus facile que de déménager géographiquement.

Les gens peuvent quitter les communautés où ils se sentent en minorité pour intégrer le communauté où ils sont dans la majorité.

Les Filter bubbel sont ce qu'on appelle des isolations idéologiques dans le web.

Nous nous enfermons dans un univers où tout ce que l'on lit et ce que l'on entend confirment ce que l'on pense déjà, ce qui rend difficile le processus de remise en cause de nos convictions et de compréhension de pourquoi les autres pensent ce qu'ils pensent.

Ces filter bubble sont amplifiés par les personnalisations de contenu qu'offrent les géants du web: Facebook YouTube, ceux-ci nous enferment dans la communauté avec qui on partage nos idées, ils nous livrent du contenu sur mesure. Chaque fois qu'on clique sur un lien, like un post, ceux-ci nous enferment toujours plus dans une communauté avec qui on partage les idées.[^22]

### Les convictions

Les convictions peuvent être définies comme des croyances profondes ou des opinions fermement ancrées sur un sujet. Bien que les convictions puissent être bénéfiques, elles sont généralement plus problématiques qu'autre chose, et notamment dans le domaine de la politique.

L'un des principaux problèmes liés aux convictions est qu'elles peuvent conduire à une polarisation et à une division. Si les gens ont des convictions fermement ancrées sur un sujet, ils peuvent être moins enclins à considérer d'autres points de vue ou à travailler avec des personnes qui ont des convictions différentes. Cela peut conduire à une polarisation et à une division dans la société.[^23]

Elles peuvent conduire à une fermeture d'esprit. Si les gens sont trop attachés à leurs convictions, ils peuvent être moins enclins à écouter les opinions ou les faits qui contredisent leurs croyances. Cela peut entraîner une prise de décision basée sur des préjugés plutôt que sur des faits,

Les convictions peuvent conduire à une résistance au changement. Si les gens sont fermement attachés à leurs convictions, ils peuvent être moins enclins à accepter de nouvelles idées ou des changements dans la société.

## Les solutions aux problèmes de nos démocraties

### L'entrepreneur politique

La solution à l'entrepreneur politique est de supprimer cette fonction ou du moins de la limiter en augmentant la participation citoyenne.

Non plus faire de l'entrepreneur politique quelque'un à qui l'on délègue son pouvoir et que dès cet instant où il l'acquiert il puisse faire ce qu'il en veut. Mais qu'il ait bien une fonction de représentativité, et que dès lors que les personnes qui lui délèguent du pouvoir estiment qu'il ne les représente plus, ceux-ci aient la possibilité de le révoquer.

Une transparence totale doit être mise en place pour que les gens sachent avec précision à qui ils délèguent leur pouvoir et ce qu'ils en font.

Et pour une bonne diversité, il faut s'assurer que les candidats proviennent bien d'horizons différents et ont les mêmes chances que les autres d'accès à ces positions.

### Les programmes politique

Une solution pour résoudre le problème des programmes politiques pourrait être de les rendre plus modulaires et plus spécifiques à chaque sujet, plutôt que de les présenter comme un package global. Cela permettrait aux électeurs de choisir les propositions qu'ils préfèrent sur chaque question séparément, plutôt que de devoir accepter ou rejeter l'ensemble du programme.

### L'honnêteté et la transparence

Dans une démocratie, il est important que la prise de décision soit transparente et responsable, et que les personnes au pouvoir soient tenues directement responsables de leurs actions. La délégation du pouvoir doit être utilisée avec précaution et les risques associés doivent être pris en compte afin de garantir que les intérêts des citoyens sont protégés.

### Le choix des alternatives

Il est important de donner à l’électeur un choix complet et large d'alternatives, et une visibilité et quantité d'informations égale sur ceux-ci. Les programmes globaux limites encore une fois cette possibilité, les sujets pourrait être séparé pour permettre des décisions plus spécifiques.

### La délégation du pouvoir

L’électeur doit pouvoir choisir de délégué son pouvoir ou non. Ainsi que se rétracter et retirer sa délégation de pouvoir à tout moment.

### La centralisation du pouvoir

Dans une démocratie, la distribution du pouvoir est essentielle pour garantir que les intérêts et les opinions des citoyens sont représentés et pris en compte dans la prise de décision. Cela implique la participation active des citoyens, la décentralisation du pouvoir et la création de mécanismes de responsabilisation et de transparence pour limiter le pouvoir des dirigeants élus.

### Les modes de scrutin

Il faut favoriser les scrutins qui prennent en compte les intensités des préférences des citoyens et éviter ceux qui encouragent le vote utile.

### La lenteur institutionnelle

Des possibilités de referendum rapide doivent pouvoir être mise en place pour les cas de d'urgence. Et plus globalement permettre au citoyen de se prononcer plus régulièrement.

### Les mandats court terme

Plusieurs possibilités existe dont l'instauration de mandats plus longs avec instauration d'une procédure donnant aux électeurs la possibilité permanente de révoquer leur élus. On peut également conscientiser les citoyens à voir sur long terme et également motiver les politiciens à planifier sur le long terme. On peut également limiter les mandats, voir supprimer la fonction de l'entrepreneur politique.

### Le bluffs à l'expertise

Il est important de reconnaître que les compétences et l'expertise ne sont pas exclusives à l'obtention de diplômes et que l'inclusion de personnes d'horizons divers peut apporter une richesse de perspectives qui peut être bénéfique pour la gouvernance et la politique.

### La science bafouée

Impliquer les scientifiques dans les processus de prise de décision : Les scientifiques peuvent aider à informer les décisions politiques en fournissant des données, des analyses et des évaluations indépendantes et fondées sur des preuves.

Renforcer la transparence et la responsabilité sur les données et les preuves scientifiques qu'ils utilisent pour prendre des décisions politiques, et être responsables de leurs décisions et de leurs conséquences. Encourager la communication efficace entre scientifiques et décideurs politiques, communiquer leurs résultats de manière claire et concise, en évitant le jargon technique. Favoriser une culture de respect pour la science. Éduquer les décideurs politiques sur la science : Les politiciens devraient être formés à la méthode scientifique et aux concepts scientifiques clés afin de mieux comprendre les données et les preuves scientifiques.

Mise en place dans une constitution de principe à prendre en compte pour que que les décisions respect des principes inialiénables qui ont fait consensus pour par exemple assurer le bonheur humain et un équilibre au sein de nos écosystèmes sur Terre.

### La propagande

Pour réduire l'impact de la propagande en politique, il est important d'encourager une information libre et transparente, qui soit basée sur des faits et des données vérifiables. Les médias doivent jouer un rôle clé dans ce processus, en fournissant une couverture équilibrée et impartiale des événements politiques, en fact-checking des affirmations politiques et en promouvant la vérification indépendante des faits. Il est également important de sensibiliser le public aux techniques de propagande et de manipulation de l'opinion, en les éduquant sur la façon de reconnaître et de résister à ces tactiques.

### L'ignorance du citoyen

Une éducation de qualité peut aider les citoyens à devenir des électeurs plus avertis. Il est important d'enseigner aux élèves des compétences de pensée critique, de raisonnement logique, de recherche d'informations fiables et de comprendre les enjeux politiques.

La transparence et l'accessibilité : les politiques et les décisions publiques doivent être transparentes et facilement accessibles pour permettre aux citoyens de mieux comprendre comment leur gouvernement fonctionne et quelles décisions sont prises.

Communication : il est important que les dirigeants politiques communiquent clairement et régulièrement avec les citoyens, en utilisant un langage simple et en évitant le jargon technique.

La participation : encourager la participation des citoyens dans les processus politiques, tel que les élections, les pétitions, les débats publics, les consultations, les sondages d'opinion, etc. peuvent aider à accroître l'implication des citoyens dans la vie politique et à renforcer leur compréhension des enjeux politiques.

### Le manque de temps

Le problème du temps se règle en accordant plus de temps au citoyen par la réduction du temps de travail. Une autre possibilité est aussi d'avoir un algorithme qui va à chaque fois sélectionner avec précision un échantillon représentatif de la société pour éviter à tout le monde de devoir voter.

### L'hooligan politique

Faire attention de lire les programmes sans hooliganisme politique, les croiser avec des recherches en économie et sociologie. Se demander si l'on ne suit pas notre intuition que notre raison aurait ensuite confortée avec des arguments.

Prendre conscience de cet hooliganisme pour amener à de meilleurs débats, apporter une façon de réfléchir qui ne soit pas collée à l'intuition et qui permette de sortir de notre mode de penser d'hooligan politique. Il faut apprécier tout ce qui est contre-intuitif, apprécier le fait de convaincre notre intuition, que notre intuition a tort.

Il faut apprécier le fait d'avoir tort, de pouvoir se remettre en question, comprendre que c'est en débloquant notre intuition que l'on peut faire les plus grands progrès.

### Le communautarisme

Il faut lutter contre le désir de chacun de faire partie de la majorité.
Il faut alors lutter pour faire en sorte que faire partie de la minorité ne soit pas une expérience gênante et embarrassante.

Il faut faire attention à ne pas donner raison à la majorité simplement par ce qu'elle est la majorité.

Il faut rendre plaisante l'expérience d'être dans la minorité.

Donner aux gens qui sont dans la majorité l'envie de se retrouver dans la minorité.

Que chacun essaye de comprendre et de se mêler.

### Les convictions

Dans une démocratie, il est important que les gens soient ouverts d'esprit et disposés à écouter les opinions et les points de vue des autres. Il faut conscientiser et éduquer le citoyen là-dessus.

### Conclusion des solutions

Il y a beaucoup de problèmes inhérents à la démocratie.

Pour quel fonctionne bien il faut des conditions idéales.

Des bons médias pour être bien informé et que les hommes politiques au pouvoir agissent réellement pour l’intérêt des citoyens.

Mais le problème c'est que notre démocratie crée un contexte ou c'est tout le contraire qui va se produire.

Les gens sont soit des hobbits, ne s'intéressent pas à la politique, sont très ignorants dans ce domaine, ils n’ont pas trop de partis pris, soit ils sont hooligan, on un parti pris, mais très pris, et souvent victime du biais de confirmation, ils vont jusqu'à défendre des valeurs de façon irrationnelle.

Ce qu'il nous faut c'est des vulcans, des gens capable de penser rationnellement.

Et à l'heure actuelle on ne peut pas discuter rationnellement entre humains politiques dans une démocratie avec des élus qui cherchent à être élus.

Si nous voulons un changement radical de démocratie il faut absolument prendre tous ces les éléments rationnels en compte comme le principe des hooligans politiques, ségrégation communauté, etc

Mais la démocratie est un pari à prendre, car avec lui on se sent tous égaux, il offre du capitale moral et nous fédère pour coopérer.

## Idée expérimentale pour une nouvelle démocratie profonde (V1)

Nom : E-Démocratie participative partagée par referendum spontané soutenue par l'expertise et la sagesse.

Afin de rendre possible une participation plus large et plus directe des citoyens, nous utilisons les moyens technologiques de l'information et de communication pour créer un outil numérique qui facilitera et améliorera les processus démocratiques.

Cet outil est une plateforme web, aussi disponible sous forme d'application mobile. Vous pouvez vous la représenter sous la forme d'un wiki, dessus vous pouvez accéder à l'information liée aux différentes couches de la société auquel vous faites part, tel que les lois, les modes d'organisation, les décisions en cours, etc. Ces couches sont divisées et structurées en catégories et sous-catégories autant que nécessaire en fonction de la nature de cette couche.

Tout le monde à la possibilité de proposer des modifications sur toute information inscrite. Comme il est impossible pour chaque humaine de lire l'ensemble des propositions que pourraient faire les 7 milliards d'êtres humains. Nous utilisons un algorithme qui prend un échantillon qui représente l'ensemble des personnes liées à la proposition. La proposition est alors envoyée à cet échantillon représentatif. Les gens peuvent voter pour, contre ou s'abstenir, s’il y a plus de 50% pour alors la proposition passe au niveau supérieur.

La proposition est alors envoyée au niveau d'un consortium d'experts et de sages qui étudieront la proposition afin d'émettre leur avis approfondi, de vérifier l'aspect technique et scientifique, d'estimer les ressources, le temps à allouer, mais aussi et surtout de vérifier qu'il rentre bien en accord avec les constitutions établit.

La constitution d'Eutopia posera les fondements de la société, celle-ci devra être étudiée et justifier comme permettant d'atteindre notre objectif commun de vivre une vie épanouie et durable en harmonie avec la Terre. Elle devra alors être enseignée pour éveiller le peuple à celle-ci afin de s'assurer qu'elle guidera leurs propositions et leurs décisions.

Une fois le travail des experts et des sages terminé, leurs avis seront joints à la proposition finale pour permettre au citoyen d'avoir toutes les clés en main pour réaliser son choix. Ces humains nommés comme experts ou sages seront élus par les citoyens de leurs domaines respectifs. La transparence et la véracité de l'information émise dans ces avis devront être assurées, mais aussi la simplicité de l'information, le tout pour permettre au citoyen d'avoir toutes les cartes en main pour effectuer son choix et d’exécuter son pouvoir en toute conscience.

La proposition sera alors de nouveau envoyée à un échantillon avec les informations et les avis délivré par les experts et les sages. Si la proposition est de nouveau approuvée, elle est alors envoyée à l'ensemble de la population pour l'adoption finale. Le résultat devrait alors être le même que celui de l'échantillon. Néanmoins lorsque le citoyen reçoit cette proposition de vote final il aura conscience d'avance que plus de la majorité compte l'accepter, ce qui a pour avantage de permettre au citoyen une dernière chance de se rétracter.

Alors le citoyen ne reçoit pas toutes les propositions, mais uniquement celle qui le concerne en fonction de la mise en application plus locale ou globale de la décision.

Également avant que vous n'envoyiez votre proposition nous mettons en place un système collaboratif qu'il est fortement conseillé d'utiliser, votre proposition est alors d'abord une suggestion, ces suggestions sont accessibles à tous ceux qui le souhaitent, et tous peuvent y participer, l’outil qui peut être vu comme un forum vous permet de débattre, de recueillir des avis, de l'aide pour améliorer votre proposition finale, de peaufiner, de corriger vos propos et de vous aider à l'aide d'outil d'intelligence collective.

Vous avez également la possibilité de déléguer votre pouvoir à quelqu'un, il votera alors la proposition pour vous, vous aurez néanmoins accès à l'historique des positions votées pour vérifier que vous êtes en accord avec elle, et vous pouvez à tout moment retirer votre délégation de pouvoir. Vous pouvez également choisir quel niveau de pouvoir en lien avec les différentes couches de la société vous souhaitez délégué.

Finalement, c'est ce système démocratique que nous utiliserons également pour l'édition collaborative de récit. Ou plus tôt, en testant notre système nous pourrons itérer dessus et le modifier jusqu'à ce que nous en soyons pleinement satisfaits, et ce sera finalement cette version finale qui sera la démocratie d'Eutopia.

> "La démocratie cet horizon semble demander du temps, du courage, de la persévérance… et peut être un grain de folie…" Datageueule[^4]

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="439f4771-b8e5-fcce-16d6-bc2519c937a6" ></div>
</div>

## Sources

[^1]: [Wikipedia : Démocratie ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. Les éléctions sont-elles la démocratie ? 1977
[^3]: David Shearman. The Climate Change Challenge and the Failure of Democracy (Politics and the Environment). 2007.
[^4]: [Datageule. Démocratie(s) ?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All : Le principe fondamental de la politique](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory. 1656
[^8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Favoriser l'honnêteté | Démocratie 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja et Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail. 2015
[^11]: [Science4All. Nos démocraties divisent | Démocratie 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. Le scrutin de Condorcet randomisé | Démocratie 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. Êtes-vous un hooligan politique ? Démocratie 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[^15]: [Science4All. Rationnellement irrationnels | Démocratie 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies. 2008
[^17]: [La Tronche en Biais #5. Les Biais de Confirmation](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [ScienceEtonnante - L'effet de Halo — Crétin de cerveau ! #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [La Tronche en Biais #3- La dissonance cognitive](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Franklin Templeton Academy – Biais de disponibilité](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. Petit communautarisme deviendra grand | Démocratie 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [TED Conférence. Eli Pariser nous met en garde contre "les bulles de filtres" en ligne.](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Chère conviction, mute-toi en infection VIRALE !!! Démocratie 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Concentrations des médias : les milliardaires vous informent. 2016
