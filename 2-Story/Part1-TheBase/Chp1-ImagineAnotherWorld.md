---
title: Imaginer un autre monde
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

Alors comment imaginer un nouveau monde, un idéal, l'Eutopie ?

## Le système, ses contraintes et ses incitations

Si nous semblons actuellement dans une impasse face au changement climatique et aux autres défis de notre temps, c'est parce que les bases de notre système génèrent des contraintes et des incitations qui finissent par empêcher tout changement efficace sans perturber la cohérence établie, ce qui entraîne alors notre société dans une inertie destructrice que plus personne ne semble pouvoir être en mesure d'arrêter.

À chaque élection, nos politiciens tentent de nous convaincre de leurs propositions, et à priori ils pensent tous bien faire, ils pensent détenir la solution qui pourrait permettre l'amélioration du quotidien global de la population qu'ils défendent. Mais ils se battent entre eux avec ces contraintes et ces incitations, et celles-ci ne peuvent amener à une solution durable et souhaitable. Alors impuissants face au système et ils n'ont pas d'autre choix que de finalement se laisser diriger par lui.

Pour autant que nous l'ayons réellement eu un jour, nous les humains avons perdu notre pouvoir, mais pire encore ceux que nous élisons pour nous représenter l'ont également perdu.

On pense que le système politique domine et qu'il peut changer tout le reste, dont l'économie et sa forme. Que nous sommes donc bien en démocratie à cause de la concurrence des candidats pour lesquels on accorde notre vote.

Mais un système économique ne produit pas que des biens et des services, il défini les conditions de notre socialité, produit des êtres humains et des relations entre eux. [^1]

Nous verrons que l'économie intervient même dans ce choix concurrentiel démocratique. Qu'il l'influence et que de ce fait le système électoral n'a plus cette qualité d'offrir la bonne concurrence des candidats. Ce n'est donc finalement pas la politique qui domine l'économie, mais l'économie qui domine la politique. [^2]

Ce que l'on vous propose ici, c'est de faire table rase de notre système, de quitter totalement tout ce qui compose notre imaginaire collectif actuel. Nous vous invitons à repartir d'une nouvelle base, de découvrir un Nouveau Monde bien différent de celui-ci que vous connaissez, un monde qui repose sur du sens et de la logique, et d'ensuite élaborer les pistes et solutions d'une transition pour tendre ensemble vers celui-ci.

## Repartir de la base

Mais alors qu'est-ce que donc la base ?

La base c'est le réel, c'est ce qui existe bel et bien à l'origine de l'existence de l'humanité quand nous ne pouvions pas encore nous raconter des histoires imaginaires. La base c'est tout ce qu'il reste une fois que l'on a effacé cet imaginaire collectif des humains.

Nous avons donc la Terre et tout ce qui la compose. Notamment ses différents écosystèmes: un écosystème c'est un ensemble formé par une communauté d'êtres vivants en interaction avec leur environnement, ces êtres vivants peuvent être végétaux ou animaux. La planète est un grand système dans lequel interagissent une infinité de sous-systèmes qui s'équilibrent entre eux de manière dynamique. Parmi tous les êtres vivants au sein de notre planète, il y a nous, Homo Sapiens, plus communément appelé les Humains.

Nous les Humains nous sommes 7,9 milliards et nous serons 11 milliards en 2100 d'après les prévisions.[^3] Nous sommes maintenant un peu partout sur notre planète, et même dans les endroits peu peuplés, l'humain a toujours un impact sur l'environnement et sur les écosystèmes.[^4] Cependant, le problème n'est aucunement la taille de la population, mais les interactions que nous entretenons au sein de ses écosystèmes.

Alors, partir de la base, ça veut tout d'abord dire qu'il faut comprendre la base. Il nous faut comprendre le monde et son fonctionnement. Et comme nous les êtres humains, nous vivons en son sein, nous allons donc aussi devoir comprendre notre fonctionnement. Une partie sera consacrée exclusivement à la terre et une autre partie sera quant à elle consacrée à la compréhension en profondeur de l'être humain.

C'est 2 questions de recherche sont très larges et demandent beaucoup de connaissances, mais nous n'avons pas besoin de rentrer dans les détails, nous allons nous concentrer sur l'essentiel, le rassembler et en faire le résumé qui permettra de répondre aux questions qui vont suivre.

## Questions de recherche

Une fois que les compétences nécessaires pour comprendre le monde et les humains ont été acquises, nous aurons besoin d'un objectif sous forme d'une question de recherche qui établira notre souhait quant à ce que nous recherchons d'un monde idéal.

Cette question de recherche est très importante, car c'est elle qui guidera les recherches desquelles découleront nos solutions. Des questions de recherches tournées différemment amèneront à des résultats différents. Celles-ci doivent être les plus simples possible, claires et précises, mais doivent reprendre l'essentiel de ce que nous souhaitons.

Nous nous concentrerons sur notre espèce, mais comme cité précédemment nous cohabitons au sein d'écosystèmes, et nous découvrirons lorsque nous étudierons le monde que nous sommes directement liée à eux et que nous en dépendons donc pour notre survie.

Nous recherchons l'idéal. Bien évidemment, l'idéal dépend des valeurs, des croyances, des aspirations et des expériences de chacun. Dès lors nous pourrions souhaiter que l'idéal pour chaque individu soit de vivre une vie épanouissante et équilibrée, qui réponde à ses besoins et à ses aspirations.

C'est ce que nous rechercherons, que chaque humain puisse avoir une vie épanouie lors de son passage sur Terre. Mais souvenez-vous qu’il y a encore 80 millions de générations à venir, cette recherche s'applique donc aux générations actuelles, mais prend également en compte toute celles à venir.

Avec ces différents éléments, voici la formulation de notre question de recherche :

> Comment permettre à tous les humains actuel et des 80 millions de générations à venir d'avoir une vie épanouie sur Terre ?
> {.is-success}

Notre grand projet collectif c'est donc de faire en sorte que tous les êtres humains aient une vie épanouie sur Terre qui réponde à leur besoins et aspirations, et ce pour les générations actuel et pour les 80 millions autres à venir.

Après avoir bien compris comment fonctionnent le monde et l'humain nous aurons découvert comment un humain peut être épanoui, comment on peut donner du sens à son existence, et comment il peut vivre sur le long terme dans son environnement sur Terre.

Pour compléter cette recherche nous retracerons rapidement les différentes ères de l'humanité, car connaître le passé donne assise et souffle à l'interprétation du présent.[^5] Et nous avons besoin de connaître le présent pour déterminer une transition futur possible. Car la meilleure société, possible, reste celle de la meilleure évolution possible des sociétés à partir de maintenant. [^1]

Ensuite pour pouvoir arriver à cet objectif commun, nos 7,7 milliards d'êtres humains vont devoir réaliser 2 actions essentiel, collaborer et décider.

## Sous-question : Collaborations

Comme dans toute société nous allons devoir collaborer pour nous apporter les biens et services nécessaires à notre épanouissement. Notre première grande sous question sera alors :

> Comment permettre aux humains de collaborer tous ensemble afin de s'apporter les biens et services qui permettent d'atteindre cet objectif commun ?
> {.is-success}

C'est ici le domaine de l'économie. C'est l'un des gros problèmes de notre système et d'où émane le plus de contraintes et d'incitations. Repartir de zéro et chercher après la meilleure économie possible va nous permettre de définir les bases nécessaires à une société idéale possible, en évitant les contraintes et incitations qui rentrent en conflit avec sa réalisation. Nous le découvrirons, mais un système économique ne produit pas que des biens et des services, il produit aussi des êtres humains et des relations entre eux, tout aussi essentielles à leur épanouissement.

## Sous-question : Décisions

Pour finir nos 7,7 milliards d'êtres humains, tout au long de leur vie et de l'évolution de leur société, vont devoir effectuer des choix, se mettre d'accord, prendre des décisions à petite et à grande échelle. Ce sera là notre deuxième grande sous-question :

> Comment prendre des décisions ensemble et que ces décisions nous permettent d'atteindre notre objectif commun ?
> {.is-success}

Comme vous pouvez le voir dans la question, l'idée n'est pas juste d'arriver à se mettre d'accord, car à quoi bon avoir toute ou une partie de la population d'accord sur une décision, et que celle-ci soit validée si elle ne permet au final pas d'atteindre notre objectif commun.

Ce domaine est la science de la décision, et elle s'applique à de nombreuses pratiques, notamment la politique où nous utilisons actuellement une démocratie représentative.

Nous étudierons plus en profondeur les différentes possibilités en prenant en compte ce que nous aurons appris dans le fonctionnement de l'humain et du monde.

## Conclusion

Pour récapituler, nous allons donc commencer par comprendre notre habitat, la Terre, son fonctionnement, ses écosystèmes et notre place au sein de celui-ci. Puis nous allons comprendre en profondeur l'humain, son fonctionnement, ses besoins pour lui assurer un épanouissement lors de son passage sur la Terre.

Cette connaissance va nous permettre de répondre efficacement à la question qui est de savoir comment permettre aux humains de prendre des décisions et de coopérer pour atteindre l'objectif commun qui est l'épanouissement durable de chacun d'entre ceux-ci.

Le fonctionnement de la terre au chapitre 2, la compréhension de l'être humain au chapitre 3, et l'état des lieux des ères de l'humanité du chapitre 4 pourrons vont sembler barbant à lire si vous connaissez déjà ces domaines où si vous ne souhaitez pas rentrer trop en profondeur dans un premier temps. Dans ce cas nous vous conseillons de directement aller au chapitre 5 et 6 où vous découvrirez une nouvelle façon de collaborer et de décider, ces 2 parties sont essentiel car elles justifies en quoi la société que l'on va imaginer est possible en créant des bases qui l'a rendront cohérente. Néanmoins le chapitre 2, 3 et 4 n'en restent pas moins important et ont amené à la réalisation et à la justification des solutions du chapitre 5 et 6. Si vous le désirez des résumés on été réaliser et [sont disponibles ici.](/fr/4-Appendices/1Summaries)

> « La connaissance est le pouvoir. Plus nous comprenons le monde et nous-mêmes, plus nous avons la capacité de le changer pour le mieux. » Malala Yousafzai

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="7ab74a26-28f3-ed09-d01c-7f0127f4dbca" ></div>
</div>

## Sources

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, Les éléctions sont-elles la démocratie ?/ 1977
[^3]: [Perspectives de la population mondiale 2022" publiées par les Nations Unies](https://desapublications.un.org/file/989/download)
[^4]: [Rapports du GIEC 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes. 2021
