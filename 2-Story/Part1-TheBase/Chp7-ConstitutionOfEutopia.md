---
title: Constitution d'Eutopia
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Contenu au stade expérimental et en cours de rédaction.
> {.is-danger}

La constitution d'Eutopia est divisée en 3 déclarations :

- La Déclaration Universelle des Droits d'Humains, qui reprend de nombreux principes fondamentaux de la Déclaration universelle des droits de l'homme actuels[^1], mais que nous avons adaptés pour ajouter des nuances et des extensions importantes afin de mieux refléter nos valeurs telles que la durabilité, la diversité, l'interdépendance avec la nature,la liberté, la solidarité et l'équité.
- Déclaration Universelle des Droits de la Nature / Terre Mère, est une charte qui a été établie à l'initiative des peuples amérindiens et formulée à la Conférence mondiale des peuples contre le changement climatique en 2010.[^2]
- Déclaration Universelle des Droits relationnels entre l'espèce humaine et les espèces animales non humaines, est une charte provisoire et expérimentale, qui a pour vocation d'établir plus précisément nos relations avec les autres espèces animales vivantes.

Afin de ne pas alourdir ceux-ci, les justifications des différents articles ont été séparées et placées à la fin de cette même page. Ces déclarations sont libres d'être modifiées par les citoyens d'Eutopia par le même processus démocratique décrit à la fin du précédent chapitre sur comment décider ensemble.

## Déclaration Universelle des Droits d'Humains

### Préambule

Guidés par les valeurs d'égalité, de liberté, de solidarité et de durabilité, nous reconnaissons la dignité inhérente à chaque individu, un flambeau qui éclaire le chemin vers un monde où tous naissent égaux en droits et en liberté, et ce jusqu'à notre destin collectif.

Nous célébrons la diversité infinie qui tisse la richesse de notre tapis humain, et nous sommes unis par la conviction profonde que dans cette diversité réside notre force collective. C'est dans cet esprit d'unité et d'harmonie que nous énonçons cette Déclaration, affirmant que chaque personne a le droit d'exprimer librement sa pensée, de poursuivre son bonheur et de contribuer à la prospérité commune.

Notre quête commune est façonnée par le respect absolu de l'intégrité de chacun, par la recherche incessante de l'équité et de la justice, et par l'engagement sacré envers la préservation de notre foyer partagé, la Terre Mère.

En écrivant ces droits universels, nous érigeons un bouclier puissant contre l'oppression, l'injustice et l'oubli. Nous déclarons que dans chaque coin d'Eutopia, la lumière de l'égalité éclaire les sombres recoins de l'injustice, établissant ainsi un monde où la liberté, l'amour et la compassion sont des droits inaliénables.

Que cette Déclaration Universelle des Droits d'Humains inspire l'action, guide nos décisions et unisse nos cœurs dans la construction perpétuelle d'une société où tous, main dans la main, marchent vers l'aube d'un monde où la dignité humaine est la boussole qui guide notre voyage.

### Propositions et résumés

Article 1. Égalité :

- Les humains naissent et demeurent libres et égaux en droits.
- Chaque citoyen d'Eutopia a le droit inaliénable de participer au processus décisionnel de manière égale à tous les autres. Aucune forme de discrimination ou de privilège ne peut entraver l'égalité de pouvoir de décision de chaque individu.

Article 2. Liberté :

- Les humains ont une liberté totale de s'exprimer et de penser, personne ne peut empêcher la diversité des opinions et la libre circulation des idées.
- La liberté de croyance est également sacrée. Chaque individu a le droit de choisir, pratiquer et exprimer ses croyances spirituelles, religieuses ou philosophiques, sans crainte de persécution ou de discrimination.
- Les humains ont droit de choisir librement leur tenue vestimentaire, de s’exprimer personnellement à travers le vêtement, sans subir de jugement ou de restriction imposée par des tiers.
- Les humains ont le droit fondamental de se déplacer librement à travers le monde, mis à part pour des raisons de préservation de la Nature ou pour des raisons de sécurité de l'intégrité humaine ou matériel, aucune frontière ne peut limiter cette liberté de mouvement.
- Chaque individu est libre d'entreprendre, de se réaliser, de créer et d'apporter ses créations et innovations au monde. La créativité, l'innovation et la contribution individuelle au bien-être commun d'Eutopia doivent être encouragées.

Article 3. Solidarité (Fraternité) :

- La solidarité doit guider les décisions prises par les membres d'Eutopia.

Article 3. Équité :

- Chaque décision doit être conçue de manière à ce que chacun puisse en bénéficier pleinement, en éliminant les barrières et les inégalités.

Article 4. Durabilité :

- La préservation de l'environnement est primordiale et est une responsabilité collective. Les décisions doivent prendre en considération l'impact sur la nature, et assurer un équilibre durable entre l'humanité et la planète.

Article 5. Intégrité :

- En aucune circonstance, une décision ne peut compromettre l'intégrité d'autrui, que ce soit sur le plan physique ou mental. Cependant, chaque individu demeure souverain et est le seul détenteur du pouvoir de porter atteinte à sa propre intégrité.

Article 7. Sécurité :

- Chaque individu a le droit à la sécurité, à la protection contre toute violence physique ou mentale et à un environnement sûr pour son épanouissement.
- Nulle arme létale ne peut être détenue ou produite en dehors des centres de recherches et des personnes autorisés.

Article 8. Confiance :

- La socialité est basée sur la confiance collective de l'entraide altruiste et de la reconnaissance des besoins de chacun. Aucun service effectuer ne peux être demandé pour du auprès du receveur ni mesuré et stocké dans une valeur imaginaire en vue d'échange futur pour ce même service, ou tout autre. Chacun se donnant à tous ne se donne à personne en particulier.

Article 9. Diversité :

- Eutopia célèbre la diversité sous toutes ses formes et s'engage à créer une communauté inclusive, où chaque individu est respecté et valorisé pour sa singularité.

## Déclaration Universelle des Droits de la Nature / Terre Mère

### Préambule

Nous, Eutopiens de la Terre :

- Considérant que nous faisons tous partie de la Terre Mère, communauté de vie indivisible composée d’êtres interdépendants et intimement liés entre eux par un destin commun ;
- Reconnaissant avec gratitude que la Terre Mère est source de vie, de subsistance, d’enseignement et qu’elle nous prodigue tout ce dont nous avons besoin pour bien vivre ;
- Convaincus que, dans une communauté de vie impliquant des relations d’interdépendance, il est impossible de reconnaître des droits aux seuls êtres humains sans provoquer de déséquilibre au sein de la Terre Mère ;
- Affirmant que pour garantir les droits humains il est nécessaire de reconnaître et de défendre les droits de la Terre Mère et de tous les êtres vivants qui la composent et qu’il existe des cultures, des pratiques et des lois qui reconnaissent et défendent ces droits ;
- Proclamons la présente Déclaration universelle des droits de la Terre Mère, afin que chaque personne et chaque institution assume la responsabilité de promouvoir, par l’enseignement, l’éducation et l’éveil des consciences, le respect des droits reconnus dans la Déclaration, et à faire en sorte, par des mesures et des dispositions diligentes et progressives d’ampleur local et globale, qu’ils soient universellement et effectivement reconnus et appliqués par tous les Eutopiens du monde.

### Propositions et résumés

Article 1. La Terre Mère :

1. La Terre Mère est un être vivant.
2. La Terre Mère est une communauté unique, indivisible et autorégulée d’êtres intimement liés entre eux, qui nourrit, contient et renouvelle tous les êtres.
3. Chaque être est défini par ses relations comme élément constitutif de la Terre Mère.
4. Les droits intrinsèques de la Terre Mère sont inaliénables puisqu’ils découlent de la même source que l’existence même.
5. La Terre Mère et tous les êtres possèdent tous les droits intrinsèques reconnus dans la présente Déclaration, sans aucune distinction entre êtres biologiques et non biologiques ni aucune distinction fondée sur l’espèce, l’origine, l’utilité pour les êtres humains ou toute autre caractéristique.
6. Tout comme les êtres humains jouissent de droits humains, tous les autres êtres ont des droits propres à leur espèce ou à leur type et adaptés au rôle et à la fonction qu’ils exercent au sein des communautés dans lesquelles ils existent.
7. Les droits de chaque être sont limités par ceux des autres êtres, et tout conflit entre leurs droits respectifs doit être résolu d’une façon qui préserve l’intégrité, l’équilibre et la santé de la Terre Mère.

Article 2. Les Droits Inhérents de la Terre Mère :

1. La Terre Mère et tous les êtres qui la composent possèdent les droits intrinsèques suivants :
   - le droit de vivre et d’exister ;
   - le droit au respect ;
   - le droit à la régénération de leur biocapacité et à la continuité de leurs cycles et processus vitaux, sans perturbations d’origine humaine ;
   - le droit de conserver leur identité et leur intégrité comme êtres distincts, autorégulés et intimement liés entre eux ;
   - le droit à l’eau comme source de vie ;
   - le droit à l’air pur ;
   - le droit à la pleine santé ;
   - le droit d’être exempts de contamination, de pollution et de déchets toxiques ou radioactifs ;
   - le droit de ne pas être génétiquement modifiés ou transformés d’une façon qui nuise à leur intégrité ou à leur fonctionnement vital et sain ;
   - le droit à une entière et prompte réparation en cas de violation des droits reconnus dans la présente Déclaration résultant d’activités humaines.
2. Chaque être a le droit d’occuper une place et de jouer son rôle au sein de la Terre Mère pour qu’elle fonctionne harmonieusement.
3. Tous les êtres ont droit au bien-être et de ne pas être victimes de tortures ou de traitements cruels infligés par des êtres humains.

Article 3. Obligations des êtres humains envers la Terre Mère :

1. Tout être humain se doit de respecter la Terre Mère et de vivre en harmonie avec elle.
2. Les êtres humains ont le devoir :
   - a) d’agir en accord avec les droits et obligations reconnus dans la présente Déclaration ;
   - b) de reconnaître et de promouvoir la pleine et entière application des droits et obligations énoncés dans la présente Déclaration
   - c) de promouvoir et de participer à l’apprentissage, l’analyse et l’interprétation des moyens de vivre en harmonie avec la Terre Mère ainsi qu’à la communication à leur sujet, conformément à la présente Déclaration ;
   - d) de veiller à ce que la recherche du bien-être de l’homme contribue au bien-être de la Terre Mère, aujourd’hui et à l’avenir ;
   - e) d’établir et d’appliquer des normes et des lois efficaces pour la défense, la protection et la préservation des droits de la Terre Mère ;
   - f) de respecter, protéger et préserver les cycles, processus et équilibres écologiques vitaux de la Terre Mère et, au besoin, de restaurer leur intégrité ;
   - g) de garantir la réparation des dommages résultant de violations par l’homme des droits intrinsèques reconnus dans la présente Déclaration et que les responsables soient tenus de restaurer l’intégrité et la santé de la Terre Mère ;
   - h) d’investir les êtres humains et les institutions du pouvoir de défendre les droits de la Terre Mère et de tous les êtres ;
   - i) de mettre en place des mesures de précaution et de restriction pour éviter que les activités humaines n’entraînent l’extinction d’espèces, la destruction d’écosystèmes ou la perturbation de cycles écologiques ;
   - j) de garantir la paix et d’éliminer les armes nucléaires, chimiques et biologiques ;
   - k) de promouvoir et d’encourager les pratiques respectueuses de la Terre Mère et de tous les êtres, en accord avec leurs propres cultures, traditions et coutumes ;
   - l) de promouvoir des systèmes économiques qui soient en harmonie avec la Terre Mère et conformes aux droits reconnus dans la présente Déclaration.

## Déclaration Universelle des Droits relationnels entre l'espèce humaine et les espèces animales non humaine

### Préambule

Dans cette Déclaration Universelle des Droits relationnels entre l'espèce humaine et les espèces animales non humaines d'Eutopia, nous posons les fondements d'une coexistence harmonieuse basée sur la réciprocité et le mutualisme. Il est crucial de souligner que les principes énoncés ici s'appliquent à tout animal avec lequel nous choisissons de coopérer et de partager des interactions symbiotiques au sein d'Eutopia.

Nous faisons cette distinction délibérée pour préserver la liberté des animaux faisant partie intégrante du règne animal et de leurs habitats naturels, où ils peuvent évoluer sans les interférences humaines. Dans ces espaces préservés, nous reconnaissons la valeur inestimable de la nature sauvage et nous nous engageons à laisser ces écosystèmes prospérer en toute autonomie.

Cependant, là où les humains résident et choisissent de construire des communautés, nous aspirons à vivre en harmonie avec les espèces animales sélectionnées, établissant des relations basées sur la compréhension mutuelle, le respect et la coopération bénéfique. Ainsi, cette déclaration s'applique aux animaux que nous intégrons à notre écosystème, pour lesquels nous devenons automatique responsable, démontrant ainsi notre engagement envers une symbiose équilibrée et respectueuse.

### Droits des espèces sous la responsabilité humaine

Article 1. Respect de la dignité animale :

- Tout animal sous la responsabilité d'habitants d'Eutopia a le droit à une vie digne et exempte de cruauté.
- Les interactions humaines avec les animaux doivent respecter leur nature intrinsèque et leur comportement naturel.

Article 2. Conditions de vie décentes :

- Les animaux doivent être hébergés dans des conditions qui respectent leur bien-être physique et psychologique.
- Les installations doivent offrir un espace adéquat, un accès à une alimentation appropriée, de l'eau propre et des conditions sanitaires optimales.

Article 3. Accès aux soins :

- Tout animal sous la responsabilité des habitants d'Eutopia a le droit à des soins vétérinaires réguliers pour assurer sa santé et son bien-être.
- Les traitements médicaux doivent être prodigués de manière humaine, respectueuse et conforme aux normes éthiques.

Article 4. Fin de vie :

- Les animaux destinés doivent bénéficier d'une fin de vie respectueuse et dénuée de toute souffrance inutile.

## Justification

### Déclaration Universelle des Droits d'Humains

En cours de rédaction...

### Déclaration Universelle des Droits de la Nature / Terre Mère

En cours de rédaction...

### Déclaration Universelle des Droits relationnels entre l'espèce humaine et les espèces animales non humaine

En cours de rédaction...

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="23935b28-fe2e-f431-c27e-848afd2263bd" ></div>
</div>

## Sources

[^1]: [Déclaration universelle des droits de l'homme. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Déclaration Universelle des Droits de la Terre Mère. 2012 ](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
