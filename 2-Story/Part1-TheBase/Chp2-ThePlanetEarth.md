---
title: La planète Terre
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Cette page est encore en cours de rédaction et il est donc possible que certaine partie soit manquante.
> {.is-info}

## Description rapide de la Terre

### La bonne carte du monde

[![Projection de Gall-Peters](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

La projection de Peters (ou projection de Gall-Peters d'après James Gall (1808-1895) et Arno Peters (1916-2002)) est une projection cartographique qui, contrairement à la projection de Mercator, permet de prendre en compte la superficie réelle des continents. Mais localement, cette projection ne conserve pas les angles, ce qui se traduit par la déformation des continents au contraire de la carte de Mercator. [^1]

### Description rapide des différents écosystèmes

## Le cycle de l'eau

### Présentation des différentes formes d'eau sur Terre

### Description du cycle de l'eau et de ses interactions avec le milieu

### Conséquences des activités humaine sur le cycle de l'eau

## La photosynthèse et la production d'oxygène

### Description de la photosynthèse et de son rôle dans la production d'oxygène

Depuis son origine, la Terre n'a cessé de créer de l'information. Grâce à elle, des mouvements ordonnés de la matière se sont créés, donnant lieu à une diversité de formes, de couleurs, de mouvement, exceptionnelle : la vie telle que nous la connaissons.

De cette information disponible, l'une a été motrice plus que tout autre : il s'agit de celle qui est codée dans les gènes du végétal portant les mécanismes de la photosynthèse. Ce processus spécifique au règne végétal rend le système vivant particulièrement efficient et n'a pas d'équivalent. La photosynthèse possède à la fois un pouvoir énergétique et un pouvoir chimique. Elle permet de capter une énergie brute et immatérielle, l'énergie lumineuse, de la stocker et de la redistribuer de façon extrêmement fine, à l'échelle moléculaire, c'est-à-dire inférieure au nanojoule. Elle permet aussi de rassembler des éléments minéraux dispersés en des matériaux et des molécules, porteuses elles-mêmes d'informations dans leurs interactions et d’énergie dans leur composition.

C'est un phénomène prodigieux au regard des lois physiques de l'énergie. Alors que ces dernières commandent que l'énergie émise d'une source de disperse et se dégrade, devenant de moins en moins utilisable, le végétal et capable de la canaliser. Grâce aux informations contenues dans sa bibliothèque génétique, le végétal va ainsi à l'encontre des lois physiques de l'énergie, qu'on appelle l'entropie. Il forme au contraire une boucle néguentropique au sein de l'univers : la matière s'ordonne, s'organise, l'énergie se canalise. Avant de se désintégrer de nouveau au cours de son voyage dans le vivant. Au regard de la matière commune de l'univers - infiniment plus importante -, la matière organique est une dentelle : elle peut prendre toutes les formes. C'est cet ensemble de molécules en forme de crochets, de poulies, de roues, de câbles, de portes, de points, de chaînes qui constitue le vivant.[^2]

### Conséquences des activités humaines sur la photosynthèse et la production d'oxygène

## Le cycle du carbone

### Présentation du cycle du carbone et de ses intéractions avec le milieu

### Conséquences des activités humaine ssur le cycle du carbone et les gaz à efffet de serre

De plus, les matériaux et les énergies nous permettant de réaliser de telles prouesses modifient le thermostat global, composé majoritairement de carbone, ils enrichissent l'atmosphère en gaz à effet de serre lorsqu'ils sont utilisés. Et l'atmosphère chauffe. Le calcaire à la base des ciments, des plâtres et des chaux est une roche carbonée ; le pétrole, le charbon et le gaz, qui constituent 80% des sources d'énergie que nous utilisons ou 100% des bitumes sur lesquels nous nous déplaçons, sont des roches carbonées. Ce carbone n'était pas présent dans la croûte terrestre initiale, il était concentré dans l'atmosphère. Son passage de l'atmosphère à la croûte terrestre s'est réalisé par l'action des êtres vivants qui l'ont absorbé et déposé au fond des océans ou dans les couches géologiques terrestres et lagunaires. Nous relâchons en quelques décennies un carbone que des êtres vivants ont mis des centaines de millions d'années à enfouir. Nous déstabilisons un système dont l'équilibre est à l'origine de la plupart des formes de vie évoluées actuelles, y compris la nôtre.[^3]

## La biodiversité

### Présentation de l'importance de la biodiversité pour la vie sur Terre

### Conséquences des activité humaines sur la biodiversité

Plus de la moitié, des forêts et des zones humides du monde ont disparu en un siècle [^2]

## Les sols

### Présentation de l'importance des sols pour la vie sur Terre

### Conséquences des activités humaines sur les sols

Un tiers des sols de la planète sont dégradés. [^4]

## Les ressources naturelles

### Présentation des différentes ressource naturelles (eau, air, sol, etc..)

### Gestion durable des ressources naturelles

### Alternatives durables pour les ressources naturelles

## Les écosystèmes terrestres et marins

### Présentation des différents écosystèmes terrestres et marins, et de leurs importances

### Conséquences des activités humaines sur les écosystèmes

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="bbca6c7c-50d4-3eca-2ed4-4a95ef61c276" ></div>
</div>

## Sources

[^1]: [Wikipédia : Projection de Peters](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, L'économie symbiotique, Actes sud | Colibri, 2017
[^3]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
