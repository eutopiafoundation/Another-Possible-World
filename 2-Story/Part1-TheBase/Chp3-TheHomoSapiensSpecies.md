---
title: L'espèce Homo Sapiens
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## Homo Sapiens

L'espèce humaine a beau être dotée d'une redoutable intelligence et d'une conscience, d'un savoir technologique de plus en plus grand, on ne peut cependant que constater que lorsque l'humain est confronté, chiffres à l'appui, aux conséquences présentes et futures du dérèglement climatique, il n'arrive pas à changer de trajectoire.[^1]

D'où la nécessité de bien comprendre l'humain et son fonctionnement, car c'est seulement de cette façon que nous pouvons nous assurer que l'environnement créé dans notre Eutopie est possible, et que nous pourrons prévoir notre comportement au sein de celui-ci.

Comprendre les "faiblesses" de l'humain permettra d'adapter et de développer les outils en conséquence afin de les exploiter pour en créer une force.

Nous allons comprendre les mécanismes au niveau individuel, ses différents aspects physiques, psychologiques... Mais cette approche serait insuffisante et est accompagnée de toute une réflexion globale et sociologique.

<!-- ## Bref histoire d'Homo sapiens -->

## Les besoins d'Homo Sapiens

Revenons à notre question de recherche:

"Comment permettre à tous les humains d'avoir une vie épanouie sur Terre pour les 80 millions de générations à venir ?"

Nous sommes partis du principe qu'une vie épanouie pour chaque humain est une vie épanouissante, qui réponde à ses besoins et à ses aspirations. Un besoin est une nécessité ressentie, d'ordre physique, social ou mental.

### Classification des besoins

Pour nous aider dans cette recherche, nous allons commencer par reprendre la pyramide d'Abraham Maslow formulé en 1943. Celle-ci classe les besoins de l'être humain en 5 étapes et est une base intéressante pour avoir une vue d'ensemble des grands besoins de l'être humain.[^2]

Noter que ce modèle est remise en cause aujourd'hui, car selon Maslow on ne ressentirait un besoin supérieur que lorsque le précédent est atteint. Mais ceux-ci ne sont en réalité nullement pyramidaux, aujourd'hui de nombreuses avancées ont démontré que le besoin social est tout aussi important que les besoins physiologiques, ce qui ne veut pas dire qu'il devrait se trouver à la base de la pyramide, mais plutôt omniprésent à chaque étage, et c'est le cas pour les autres, chacun les ressent tous de façon concomitante.[^3]

Mais nous n'avons de toute façon pas besoin ici de comprendre avec exactitude quel besoin est le plus important par rapport à un autre et en quel circonstence, car l'objectif ici est de tous les combler sans exception.

Nous compléterons donc ci dessous cette pyramide par une actualisation de cette théorie au regard de l'évolution des savoirs en psychologie et en neurosciences.

![Pyramide de maslow](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

Au premier étage nous avons les besoins physiologiques, ce sont les besoins de base nécessaires pour maintenir la vie et le bon fonctionnement de l'organisme, tels que manger, boire, la sexualité, dormir, respirer...

On notera déjà ici l'échec total de notre système actuel pour combler les besoins de base de l'ensemble de la population. Aujourd'hui 25 0000 personnes sont mortes de faim, parmi ceux-ci 15 000 enfants de moins de 5 ans.[^4] De même pour ce qui est de la sexualité, celle-ci est bien souvent taboue, stigmatisée, discriminée, non éduquée, ce qui engendre de nombreux troubles, transmission de maladie, actes de non consentement, traumatismes, problèmes de santé mentale et physique, manques ou des addictions de plaisirs, manque de sentiment de liberté et de confiance, tout ca créant globalement un manque de bien-être avec sa sexualité pour une très grande partie de la population.

Au deuxième étage nous avons les besoins de sécurité. La sécurité d'avoir un abri, tel qu'un logement où pouvoir vivre, la sécurité d'accéder à des ressources désirées, comme du matériel, des outils, des jeux... La sécurité physique contre la violence, les agressions... La sécurité morale et psychologique, la sécurité et la stabilité affective, la sécurité médicale/sociale, et de santé.

Ici aussi on notera les différents manquements de notre société à ce niveau-là aussi, même les pays occidentaux, on remarque que la sécurité de logement est un véritable défi pour de nombreuses personnes, en particulier les sans-abris qui sont souvent confrontés à des conditions précaires et inhumaines.[^5] L'accès à des ressources peut aussi être limité pour certaines personnes en fonction de leur statut socio-économique, ce qui peut affecter leur bien-être et leur qualité de vie. Concernant la sécurité médicale et psychologique, il y a un manque de professionnels de la santé mentale, ce qui rend difficile l'accès à des soins. Que même avec avec la sécurité sociale de pays comme la france, le coût des soins peut être prohibitif pour certaines personnes, ce qui les empêche de recevoir les soins dont ils ont besoin.[^6]

Le 3e étage sont les besoins d'appartenance, le besoin d'aimer et d'être d'aimé, d'avoir des relations intimes, d'avoir des amis, de faire partie intégrante d'un groupe cohésif, de se sentir accepté, de ne pas se sentir seul ou rejeté.

Nous vivons dans une société ou l'individualisme est très présent et/ou les relations humaines sont souvent négligées au profit du succès personnel. Une grande partie de la population ressent une solitude croissante et une perte de lien social. Certaines sont rejetées et isolées, ce qui entraîne des problèmes de santé mentale tels que la dépression et l'anxiété. [^7]Notre modèle social favorisant la compétition généralisée et la relation intéressée ou marchandes pour seul modèle relationnel, ce qui promeut une véritable culture de l'égoisme.

Le quatrième étage représente les besoins d'estime, l'être humain a besoin de se sentir considéré, de se sentir apprécié et respecté par les autres, de se sentir reconnu pour ses réalisations et de se sentir capable de réussir. Ses besoins sont accomplis en accomplissant des choses qui sont valorisées par la société, en se fixant des objectifs et en travaillant pour les atteindre, ou en s'engageant dans des activités qui permettent de se développer personnellement.

Le cinquième étage est le besoin d'auto accomplissement. C'est le besoin de se réaliser, d'exploiter et de mettre en valeur son potentiel personnel. Il se manifeste par le désir de développer ses talents et ses compétences, de s'engager dans des activités qui apportent un sens et une satisfaction, et de se dépasser soi-même.

Dans notre société il est difficile de satisfaire ce besoin d'autoaccomplissement, les pressions sociales et les attentes élevées qui sont imposées à la plupart des individus peuvent rendre difficile la poursuite de ses passions et de ses intérêts personnels. Notre société très compétitive et son aliénation de l'individu, ne laisse que peu de temps pour explorer et développer ses talents et ses compétences. De nombreuses personnes sont également exclues du système, notamment les personnes ayant un handicap qui ne peuvent pas s'y insérer par manque d'adaptation de celui-ci. [^9]

Néanmoins ce besoin d'auto accomplissement a été quelque peu récusée en 2010 pour les travaux de 4 psychologues, qui ont proposé une actualisation au regards de la théorie de l'évolution. [^3] Celle-ci retire le besoin d'auto-accomplissement pour laisser la place à trois besoins reliés à la pulsion de reproduction : La rencontre, la continuité et la postérité.

|                                                                                             |                                                                            |
| ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| Besoin de postérité                                                                         | Repousse la mort inéluctable                                               |
| Besoin de continuité : Garder son partenaire, son statut, etc                               | Repousse la perte                                                          |
| Besoin de rencontre : Acquérir un partenaire                                                | Repousse les concurrents et l'isolement social                             |
| Besoin d'estime et de statut : Etre reconnu comme un être unique                            | Repousse le jugement social                                                |
| Besoin d'appartenance ou d'affiliation : Etre aimé et acceppté tel qu'on est                | Repousser l'abandon et la solitude                                         |
| Besoin de protection : Abri, sécurité +physique, familiale, psychologique, financière, etc. | Repousser la violence                                                      |
| Besoin physiologiques : Respirer, boire, manger, faire ses besoins, dormir, se réchauffer   | Repousser la faim, la soif, le froid, l'ashpyxie, la fatigue et la maladie |

\*Liste des sept besoins humains fondamentaux inconscients et concomitants d'après D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Ces besoins sont des besoins évolutifs inconscients, ils s'expriment de mille manière différentes dans la vie de chacun. Par exemple, la créativité artistique peut être fondée tout aussi bien sur le besoin d'acquérir un statut et d'être reconnu socialement comme un être unique que sur celui de séduire des partenaires, ou de perdurer dans le temps en laissant une oeuvre à la postérité.

Nous venons de passer en revue les 5/7 types de besoins, mais il faut savoir que Abarham Maslow avait ajouté vers la fin de sa vie un 6e besoin, le "dépassement de soi même". Celui-ci se situe au sommet de la pyramide, il dira à son propos que :

> « L'être humain pleinement développé travaillant dans les meilleures conditions a tendance à être motivé par des valeurs qui transcendent son "moi" »[^9]

Il est intéressant de déjà prendre conscience qu'un être humain comblé et qui dépasse l'accomplissement de soi serait pleinement ouvert à dépasser son individualité pour s'engager au service d'autrui.

Pour terminé, ces besoins peuvent également être complété par les réflexions de Hannah Arendt.

Dans notre condition humaine nous serions poussé vers une quête d'éternité : la vita contemplativa. Quête de plus en plus difficile à satisfaire dans nos société modernes qui ne croient plus en Dieu. Nous sommes également poussé vers une quête de l'immortalité, la vita activa. [^10]

La quête de l'immortalité est ce qui pousse un être humain mortel à poser des paroles et des actes créatifs qui dépassent sa propre nécessité vitale, sa propre existence même et le projettent dans le futur au delà de sa propre mort.

A l'origine, l'ère de la modernité reconnaissait socialement le besoin d’immortalité, par exemple à travers l'héroisme des soldats , à travers des titres et devises, ou par le courage et l'authenticité d'une parole sans compromission :: «On ne pense, on ne parle avec force que du fond de son tombeau : c'est là qu'il faut se placer, c'est de la qu'il faut s'adresser aux humain » (Diderot, 1782). Aujourd'hui, tous ces titres, toutes ces paroles, toutes ces actions, ne trouvent plus écho dans notre société. Hannah Arendt affirme que le refoulement social de la quête de l’éternité hors du domaine public, doublée du refoulement social de la quête de l'immortalité, explique comment « l'époque moderne - qui commença par une explosion d'activité humaine si neuve, si riche de promesses - s’achève dans la passivité la plus inerte, la plus stérile que l'histoire n'ait jamais connue. [^10][^11]

Emmanuel Todd exprime un propos convergent. Il affirme que le passage de «l'Homme qui croit en l'Homme » à « l'Homme qui croit en Dieu » à généré une énergie collective important. Écarter de la quête d'éternité a renforcé les énergies vers la quête d'immortalité. Quand l’église comme cadre social s'est effondrée, un nouvel idéal humaniste laïque s'est développé. Ce fut l'espoir immense en l'Homme nouveau qui fut porté par les mouvements révolutionnaires du XIX siècle. En revanche, l'effondrement de l'idéal social communiste et de l'idéal républicain révolutionnaire marque la fin social de la quête d'immortalité. « L'Homme qui croit en 'Homme, disparaît à son tour et devient « l'Homme qui ne croit plus en rien du tout », un être humain nihiliste, apathique, sans espoir ni but, comblant son vide existentiel par la consommation.[^12]

Qu'est-ce qui assure la satisfaction de notre besoin de postérité? Selon Hannah Harendt, c'est l'œuvre. L'œuvre est ce qui est propre à laisser une postérité, c'est-à-dire une enfant. C'est aussi planter des arbres sous lesquels le temps qui passe ne permettra pas de venir soi-même se mettre à l'ombre. C'est transmettre quelque chose aux enfants. C'est écrire des poésie ou sculpter des morceaux de bois. C'est sacrifier héroïquement sa vie. C'est accomplir un travail qui a du sens. L'œuvre est de ce qui dure, Elle offre sécurité intérieure et maîtrise de soi de l'être humain. Par son action, la personne manifeste son œuvre unique au monde et laisse entrevoir son immortalité. Les œuvre de chacun, crées en privé et offertes en public, sont ce qui fait que le monde humain est monde. [^10]

Le travail de subsistance n'est que survie. Selon la philosophe, le travail est de l'ordre de la nécessité vital, il soutient notre existence physiologique. Le travail est une dimension étriquée de la vie humaine sur laquelle on n'a pas véritablement pouvoir. Hannah Arendt déplore le rabougrissement du projet de vie proposé collectivement par l’incroyable place prise, dans les sociétés modernes, par le travail de subsistance. Selon elle, le travail devrait être replacé dans la sphère privée et n'être que secondaire. C'est l'œuvre qui devrait être au centre de la vie social publique.

La réflexion philosophique de Hannah Arendt sur la place excessive du travail de nécessité vitale et sur la disparition de l'œuvre de postérité permet de nombreux liens explicatifs de la situation contemporaine. Elle permet de comprendre la disparition des citoyens, remplacés par les consommateurs. Ele permet de comprendre l'absolu désarroi existentiel que peut produire le chômage dans une société qui a oublié l'œuvre et qui n'est organisée publiquement que sur le travail de subsistance. Elle permet de comprendre l'attitude des jeunes générations face au travail de subsistance et leur quête de sens dans un métier d'œuvre. Enfin, permet de décliner la pyramide des cette réflexion besoins dans facettes de l'humain.

Du point de vue corporel, perdurer c'est se reproduire et les différentes avoir des enfants. Du point de vue spirituel, c'est être éternel. Du point de vue social, c'est être immortel, c'est laisser trace de son passage sur terre. La quête d'immortalité, au sens où l'entend Hannah Arendt, est l'expression social du besoin de postérité qui s'inscrit à tous les niveaux de notre vie physique, psychologique, sociale ou spirituelle. La descendance rejoint ici la transcendance. Il convient de retenir que, pour Hannah Arendt comme pour Kenrick Douglas et ses collègues, le besoin de postérité allie deux nécessités: la production intime d'une oeuvre tout à fait unique et personnelle, alliée à une relation au monde pouvoir la manifester

Ce besoin multidimensionnel de postérité est complètement occulté par notre société fonctionnaliste et utilitaire. Et d'est pourtant là, dans ces actes qui n'ont aucune signification économique et rationnelle apparente pour la vie quotidienne ou le développement d'avantages immédiats, que se trouve un sens, une grandeur. La société moderne actuelle, en proposant une vie limitée à la survie vitale et au développement matériel, a peu a peu coupé l'accès à la satisfaction du besoin de postérité. D'où une sensation lancinante d'insatisfaction et d'inaccomplissement, dans une société où paradoxalement l'abondance et le plaisir règnent. lci naît la quête de sens. Cette quête de sens ne semble pas pouvoir trouver reconnaissance dans la société hyper-moderne. [^11]

Voici donc ci-dessous les besoins humains fondamentaux complétés par les réflexions de Hannah Arendt. Les flèches représentent les différentes dimensions du corps humain selon les médecines orientales. Cette représentation a été au choisi plus tôt que les distinctions classiques de la médecine moderne occidentale, car elle permet d'élargir le spectre et d'inclure la vita comtemplativa de Hannah Arendt. Les flèches de celle-ci sont en pointillé pour désigner qu'elles sont hors du champ de la science actuel.

[![Les besoins humains fondamentaux complétés par les réflexions de Hannah Arendt](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)]

### Besoins ou désirs intrinsèques et extrinsèques

Il faut faire la différence entre les besoins, il y a les besoins ou désirs intrinsèques et les extrinsèques.

Les besoins intrinsèques sont des motivations internes et personnelles qui proviennent de l'intérieur de l'individu.

Les besoins extrinsèques sont des motivations externes qui proviennent de l'environnement et de l'individu. Ils se réfèrent à l'image que la personne veut donner d'elle aux autres, voire à elle-même. La consommation peut satisfaire des besoins de ces deux types en même temps, la plupart de celles qui satisfont des besoins intrinsèques ont aussi une dimension extrinsèque.

Les besoins extrinsèques se relient étroitement aux normes sociales diverses, au statut, au comportement éthique, à tout l'imaginaire collectif. Parfois la satisfaction des besoins extrinsèques a un inconvénient sur les besoins intrinsèques.[^13]

La distinction sociale et l'imitation créent des désirs extrinsèques.

Toute société va former des désirs et besoins extrinsèques.

Dans notre société on manipule puissamment sans cesse ces désirs, l'éducation et l'ambiance sociale inculquent des modèles de style de vie, ou très souvent le sentiment d'avoir plus est mieux, et la publicité crée désir après désir.[^14]

L'insatisfaction va provenir de l'écart entre les désirs et leur satisfaction, ou en d'autres termes entre aspirations et réalisations.

Pour restreindre cette insatisfaction, on peut a priori jouer sur deux termes : diminuer le désir ou le satisfaire. L'accent est souvent mis sur le second moyen, le satisfaire, mais on oublie ou néglige presque entièrement l'existence du premier.

Ce qu'il faut donc d'abord, c'est comprendre comment vont se former nos désirs et comment on peut les maîtriser. Et c'est ce que nous allons faire.

## La nature humaine

Mais avant tout chose il est essentiel de faire un point sur la nature humaine car ce point revient régulièrement dans les débats. Depuis longtemps, les sciences sociales et les neurosciences essaient de faire la part entre ce qui est inné et ce qui est acquis, mais il n'y a aucun consensus sur la nature humaine.

Alors que les sciences naturelles ont semblé donner raison à l’utilitarisme pendant plus d’un demi-siècle, il devient aujourd’hui possible de contredire cette hypothèse : l’homme n’est pas uniquement égoïste, en plus du fait qu’il n’est pas parfaitement rationnel. Ce non-utilitarisme de l’être humain s’applique aux formes les plus primitives de la vie : la coopération est inscrite dès les origines et serrait, avec la survie du plus apte, un des moteurs essentiels de l’évolution.[^15]

Croire que notre évolution repose exclusivement sur une compétition interindividus apporte une vision biaisée de l’évolution, alors que le rôle de la coopération était déjà présent dans la théorie darwinienne de l’évolution. [^16]

Mais, quelle que soit la nature humaine, l'histoire par ses innombrables exemples prouve que celui-ci peut tout devenir: le pire comme le meilleur.[^17][^18] La nature humaine ne doit justifier quoi que ce soit sur ce que l'on souhaite devenir. Nous pouvons assurément décider de ce que nous souhaitons devenir dans notre société, car nous en avons tout simplement la possibilité.[^19]

Ce qui est sûr en revanche c'est que l'humain n'est pas un ermite, il vit en société et il manifeste une sociabilité. L'humain est avant toute chose homo sociabilis, il a une propension à se rapprocher de ses congénères et relationner avec eux. Il a le besoin de parler, d'échanger des informations, de partager des connaissances et des émotions, mais aussi et même fondamentalement un besoin d'exister dans le regard des autres. Il recherche l'interaction humaine pour la sociabilité qu'elle lui procure et pour l'échange d'images positives et gratifiantes dont il se nourrit. [^20][^21]

## Formation de nos désirs

Nous avons précédemment dit que l'on devait d'abord comprendre comment se forment nos désirs et comment on peut les maîtriser.

Nous avons depuis plus d'un milliard et demi d'années dans notre boîte crânienne un petit organe appelé striatum.

Le striatum libère de la dopamine, une substance qui nous procure le sentiment de plaisir et qui va renforcer un comportement identifié comme un succès. [^22]

Il fait partie de tout un mécanisme de coopération et dialogue entre différentes structures et participe à la décision, reçoit des informations et compare les différentes options, gains potentiels, efforts nécessaires, immédiateté de la récompense.

Ce système est souvent appelé circuit de la récompense, mais la dopamine n'est qu'est une hormone de prédiction du résultat d'une action. [^23]

Ces décharges de dopamine font naître des incitations qui nous poussent à l'action, et renforceront ce comportement ou cette activité dans le futur.

Ces renforceurs, que l'ont peut aussi appelé motivations profondes de notre cerveau sont : [^22]

- La nourriture
- Le sexe
- La position sociale
- Le besoin d'information
- Le moindre effort
- La stimulation intellectuelle

Ces motivations étaient essentielles à notre survie et aux développements de notre espèce dans des temps primitifs et sont toujours actuellement très sensibles. Les neurones du striatum et de l'ensemble du circuit de la récompense nous attirent vers ce qui comble nos renforceurs, donc tout ce qui procure de la nourriture, du sexe, du statut, de la facilité et de l'information.
Elles ne sont pas intrinsèquement néfastes, mais comme on peut le constater à l'heure actuelle, elles peuvent être exploitées pour développer des économies productivistes, consuméristes, individualistes et destructrices de l'environnement.

La particularité de ce mécanisme qui conditionne notre cerveau à non seulement chercher de la nourriture, de l'argent, du sexe et du statut social, c'est qu'il cherche a se procurer toujours plus d'argent, toujours plus de nourriture, toujours plus de sexe ou toujours plus de statuts.

Lorsque l'on place un rat près d'un couloir avec un morceau de fromage au bout, ses neurones libèrent de la dopamine dès l'entrée du couloir. La première fois lorsqu'il trouve un morceau de fromage au bout du couloir, ses neurones déchargent de la dopamine, la deuxième fois s’il trouve le fromage, ses neurones à dopamine ne font rien. Mais s’il trouve deux morceaux de fromage au lieu d'un, ses neurones à dopamine déchargeront de nouveau. [^22]

Le striatum ne fait tout cela que dans la mesure où il peut le faire toujours plus.

Nous ne parvenons donc à stimuler nos circuits du plaisir qu'en augmentant les doses.

Un autre de nos mécanismes est que le plaisir et la facilité que nous pouvons nous offrir maintenant a cent fois plus de poids dans nos décisions que la considération d'un avenir lointain. Plus un avantage est éloigné dans le temps, moins il a de valeur pour notre cerveau. C'est ce qu'on appelle la dévalorisation temporelle.

On privilégie un plaisir immédiat à un plaisir décuplé à venir, ou à faire un choix qui nous avantage maintenant, mais qui aura des conséquences néfastes à long terme.

Plus le délai est long, plus la réponse anticipatoire est faible.[^24]

Il y a surement des différences de nature génétique entre les personnes, et certaines sont plus sensibles à leur striatum, mais ce sont avant tout des éléments liés à l'éducation et à l'environnement socioculturel qui s'avèrent déterminants. [^25][^26]

Ces neurones sont en réalité à la base de nos processus d'apprentissage

Ils nous conditionnent: une fois que nous avons appris que la nourriture suit un geste, le geste devient attirant en lui-même. C'est le principe de l'apprentissage social, nous sommes alors conditionnés pour aimer tel ou tel comportement. Le principe du conditionnement opérant règne en maître dans un lieu d'apprentissage par excellence, l'école.

L'égoïsme et l'altruisme sont l'un et l'autre tout autant motivés par le plaisir.

Lorsque nous offrons un cadeau à quelqu'un que nous aimons, il ne nous vient pas consciemment à l'idée de le faire pour nous sentir bien, et pourtant nous nous sentons bien.

Alors on pourrait se demander, si l'altruisme nous fait du bien personnellement, et qu'en plus de cela, nous en sommes conscient, est ce que cela est-il encore de l'altruisme, ne serait-ce pas plus une sorte d'intérêt personnel bien placé ?

La frontière entre altruisme et égoïsme est fine, c'est même une frontière qui n'existe pas au niveau biologique, il n'y a pas une hormone de l'altruisme ou une hormone de l’égoïsme. Il y a un acte pour lequel notre cerveau réalisé un calcule de probabilité pour savoir si celui-ci nous seras bénéfique, et nous en tirons alors du plaisir. Les zones impliqué dans le cerveau sont commune, ce que nous remarquons cependant c'est qu'il y a une activation plus importante du cortex préfrontal dorsolatéral dans l'égoïsme en raison de l'évaluation plus prononcée des calculs coûts-avantages. [^27] Mais globalement, c'est nous qui classifions les actes d'altruiste ou d’égoïste, et cette définition peut donc varié en fonction des modes de pensées.

Nous dirons que dans l'acte égoïste il n'y a que pour but d'en retirer des bénéfices personnels.

Dans l'acte altruiste il y a pour intention de donner du bénéfices à l'autre, tout en recevant un bénéfice recherché inconsciemment. Mais il peut aussi y avoir l'intention de donner du bénéfices à l'autre, tout en recevant un bénéfice recherché consciemment. [^28]

L'acte altruisme est donc un échange mutuelle qui va dans les deux sens, là ou l'acte égoïste ne va que dans un sens.

L'égoïsme et l'altruisme peuvent donc tous deux amener au plaisir individuel, et nous découvrirons laquelle de ces 2 motivations est la plus efficace pour atteindre notre objectif commun.

Mais donc si des gens sont plus généreux c'est probablement parce que leurs cerveaux ont été configurés de cette façon dès leur plus jeune âge. Telle que mère Teresa reconnue pour son dévouement sans faille à aider les autres, sa compassion et son amour inconditionnel pour tous ceux qu'elle rencontrait, elle aura consacré sa vie à faire du bien aux autres, sans se soucier de sa propre sécurité ou de son confort personnel, et est devenue l'un des symboles les plus importants de la charité et de l'altruisme dans le monde entier. Lorsqu'elle n'était âgée que de 6 ans, sa mère l'emmenait avec elle visiter les plus démunis, les pauvres, alcooliques et orphelins. Et elle lui livrait une recommandation invariable et obstinée "Ma fille n'accepte jamais une bouchée qui ne soit partagée avec d'autres". Le partage, enseigné au berceau de manière régulière et dans un constant souci de mise en application et d'exemplarité, devient ici un conditionnement si puissant que le striatum de mère Teresa ne fléchira plus en quatre-vingts ans de vocation. Son environnement et son éducation ont ainsi contribué à développer en elle des valeurs et des motivations telles que la générosité, la compassion et le désir de servir les autres.[^29][^30] Bien sûr la religion en prônant ces valeurs morales de générosité et d'altruisme joue une part importante dans la configuration du cerveau de Mère Teresa, et celle-ci découle de notre recherche de sens dont nous parlerons plus loin

Des études révèlent également l'existence d'une prime à l'agilité mentale et à la capacité de résolution de problème, qui engendre une activation des zones de la mémoire et une meilleure mémorisation des informations communiquées par le professeur. Ainsi la dopamine reçu par un élève lorsqu'il reçoit de beau point va moduler la plasticité synaptique et en consolider les souvenirs. [^31]

Bien entendu le striatum n'est pas la seule partie de notre cerveau à prendre les décisions. Le cortex frontal est la partie la plus antérieure du cerveau humain, et est devenu de plus en plus épais au cours de l'évolution, reflétant l'augmentation de nos capacités cognitives. C'est le siège de la volonté et de la planification.

Par exemple lorsque l'on demande à des participants de choisir entre vingt euros tout de suite ou 30euro dans 2 semaines, les participants qui prenaient l'argent tout de suite voyaient le striatum s'allumer, sinon c'était le cortex frontal.[^32]

Si nous voulons donc former une collectivité humaine apte à saisir les enjeux de l'avenir, ce sont ces connexions qu'il faut développer par l'exigence, la conscience et la persévérance.

## Maîtrise de nos désirs

Pour limiter les effets néfastes de notre striatum, il y a 3 solutions possibles,

- Réprimer notre striatum: Dans l'histoire, l'activité du striatum a été bloquée par des commandements moraux et par l'effort de la volonté dressée contre la tentation. Mais ce n'est pas là une solution, si on réprime notre striatum, on peut affecter notre capacité à ressentir de la motivation ou du plaisir, ce qui peut entraîner des problèmes tels que la dépression et divers troubles. La répression a des conséquences négatives sur le bien-être et le fonctionnement des individus et n'est donc ni une solution efficace ni une solution durable. [^33]

- Prendre le striatum à son propre jeu, nous pouvons en effet jouer sur les renforceurs primaires pour atteindre une société idéale, par exemple faire en sorte que la norme sociale soit d'avoir un mode de vie sain et durable. Pousser le développement des renforceurs essentiels à une bonne société.

- Mais une solution plus efficace et durable est de faire appel à la capacité unique de l'être humain, la conscience. Car la force du striatum vient de ce que ses commandements sont non conscients.

Lorsque les neurones du striatum se sont habitués à un certain niveau social, ils s'émoussent et l'on ne ressent plus rien, de sorte qu'il devient impératif de s'élever d'un cran supplémentaire pour les stimuler. Ce processus d'incrémentation ne produit aucune satisfaction durable, il ne peut pas apporter le bonheur.

La plupart de nos actes sont entrepris avec très faible niveau de conscience. Nous agissons plus souvent de manière machinale. Même lorsque nous nous livrons à des activités intellectuelles, il serait faux d'affirmer que nous le faisons en toute conscience. [^34]

Nous sommes dotés d'un cortex cérébral d'une très grande puissance de calcul, que nous employons essentiellement à des fins utilitaires, de performance et de technique. Depuis plusieurs millénaires, notre pouvoir d'abstraction de conceptualisation et de la planification nous sert principalement à concevoir des outils qui rassasient notre striatum.

Nous sommes des êtres dotés d'un haut niveau d'intelligence, mais d'un faible niveau de conscience. [^22]

L'intelligence élabore des solutions, génère des calculs, met en application des objectifs et des programmes. Mais elle peut très bien le faire sans la conscience. L'exemple le plus flagrant est l'intelligence artificielle qui peut réaliser des tâches d'une extrême complexité sans aucune conscience.

Ces grande intelligence artificielle que vous connaissez actuellement fonctionnement également avec une notion de probabilité, et par un système renforcement, qui implique une récompense ou une punition à ses actions afin de favoriser l'apprentissage de comportements désirables. Autrement dit ces système fonctionne de manière similaire à la dopamine chez l'humain.

Et si l'on veut donc créer une conscience chez l'IA, il suffit de comprendre comment la notre se créé. Ce mystère
n'est à l'heure actuelle pas encore entièrement compris par les neurosciences et nous y reviendrons plus tard. Mais si elle l'est et que nous arrivons à créer cette conscience, la différence entre nous et une machine deviendra très fine. Cette avancé possible soulèvent énormément de question éthique et nous amène à réfléchir sur la perception de notre propre conscience.

Quoi qu'il en soit, une partie de la solution au enjeux de notre espèce, est donc de rajouter plus de conscience dans nos actes du quotidien.

La conscience est également une caisse de résonance pour nos perceptions.

En développant notre caisse de résonance sensorielle, nous pouvons faire croire à notre striatum qu'il obtient davantage de plaisir, alors que nous lui en donnons moins quantitativement.

Un outil très pratique pour développer notre conscience est la médiation. [^35]

Amener notre degré de conscience à un niveau comparable avec notre niveau d'intelligence sera sans doute un enjeu de premier plan pour élaborer une bonne société et assurer l'avenir de notre espèce. L'évolution vers une société de la conscience et vers une économie de la croissance mentale.

## Le besoin de sens et de certitudes

Une fois que l'individu a l'autonomie de ses choix et de ses croyances, c'est à dire le droit de donner à sa vie la direction qu'il souhaite, définir cette direction est devenu une tâche bien ardue. La direction n'étant plus plus fixée par la religion ni par un régime politique totalitaire, chacun de nous doit créer son propre sens. Or lorsque nous prenons conscience que notre existence est brève et vouée au néant, nous sommes confrontés à trouver une justification à notre existence et à nos actes et cette prise de conscience peut devenir insupportable. [^36]

Car le sens est plus important pour vivre que tout le reste, il a une valeur de survie pour l'être l'humain. Alors pour combler notre manque nous plongeons de la recherche d'argent, de statut etc...

Le signal du plaisir qui est généré en amont de ce que l'animal cherche est une prédiction réalisée par notre cerveau sur ce qui va se produire. Cette production a lieu, car c'est un avantage évolutif, un mécanisme qui augmente les chances de survie de l'animal.

En étant capable de produire ce qui va arriver à partir de ce qu'il observe autour de lui, il décuple son pouvoir de contrôle et de décision. Il peut rechercher les situations avantageuses et fuir celles qui sont potentiellement dangereuses. Il a un temps d'avance sur le réel.

Le cerveau des animaux vertébrés a inventé un moyen de faire des prédictions et de gagner un temps d'avance sur l'état du réel. Cette capacité à établir des liens entre l'état de son environnement à l'instant T et son état futur est la base de ce qu'on appelle chez une espèce hautement célébrée comme Homo Sapiens, le sens.

Nous avons créé un sens à la société humaine, nous savons que nous pouvons y être acceptés et y trouver notre voie pourvu que l'on y respecte les règles et codes établis. Cette société n'est pas livrée au chaos, elle a un ordre et cet ordre intelligible nous est fondamentalement rassurant.

Notre tendance à déceler des liens de sens au sein de notre environnement est si développée et si irrépressible qu'elle nous amène parfois à discerner des liens là ou il n'en existe pas forcément.

Par exemple lorsqu'un guerrier qui part chasser une proie, met un collier et chasse une proie avec, la prochaine fois s’il met ce même collier, le guerrier recevra une petite décharge de dopamine sur l'éventualité de capturer une autre proie. Le guerrier se sentira plus confiant sur son succès futur.

Ce système d'anticipation permet de réduire le sentiment d'incertitude sur l'avenir. Observer, prédire, anticiper les événements futurs, diminuer l'angoisse, tout cela participe de la notion de sens. Cet avantage est si décisif qu'il y a tout lieu de penser qu'il a été sélectionné par l'évolution.

Déceler du sens autour de nous est si crucial pour notre survie que les situations où ce sens nous échappe provoquent l'apparition d'une angoisse physiologique aiguë. Cette réaction est provoquée par notre organisme par instinct de survie.[^37]

À l'aide des outils d'imagerie cérébrale, on peut découvrir ce qu'il se passe dans le cerveau. Une petite bande de cortex cérébraux, localisée à quelques centimètres au-dessus du striatum entre en action. Il s'agit d'un repli du cortex cérébral situé à l'interface entre deux hémisphères cérébraux, eux-mêmes connectés au striatum, et nommé cortex cingulaire antérieur. Ce cortex cingulaire intérieur s'allume dès que les prévisions faites ne sont plus confirmées par ce qui se produit dans les faits.

Ce signal d'alarme signifie une violation de la prédiction. Si trop de prédictions sont invalidées, il devient difficile de s'organiser et on a l'impression de tomber dans le chaos. Surcollicité, ce signal d'erreur devient nocif pour la santé physique et mentale de l'individu. Il déclenche dans l'organisme une puissante réaction de stress, le cortex cingulaire active un circuit nerveux à plusieurs maillons qui descend jusqu'à un centre cérébral impliqué dans la peur et l'angoisse, l'amygdale, puis aux glandes corticosurrénales situées sur les reins et à des noyaux neuronaux du tronc cérébral qui libèrent des hormones comme le cortisol ou la noradrénaline, dont l'effet est de placer le corps en posture de fuite voir la paralysie, et de provoquer une angoisse qui peut devenir existentielle.

Notre cortex cingulaire joue donc donc le rôle de signal d'alarme qui nous avertit quand notre monde n'a plus de sens décelable. Les conséquences de cette réaction vont des troubles du sommeil à la dépression, en passant par l'anxiété, le déclin de la mémoire, les maladies cardiovasculaires et le diabète.

Si le niveau d'ordre et d'organisation dans notre environnement commence à baisser, cette partie centrale de notre cerveau s'active et nous alerte sur la présence d'un potentiel danger pour notre survie. Si la société est relativement stable, où les structures du travail, de la famille et des rapports humains ne changent pas de façon trop imprévisible et arbitraire, le cortex cingulaire est facteur d'adaptation et d'ajustement, mais si les repères changent trop vite, et de façon constante sans laisser de répit à l'individu, il peut être extrêmement dangereux pour lui-même et pour les autres.

Mais face au vide de sens, comment réagit l'esprit humain ? Il se construit des systèmes de représentation pétris de signification, d'ordre et de cohérence. Ainsi depuis que l'homme existe, il ne fait qu'insuffler du sens à la réalité.

Les premières tentatives ont pris la forme de récits mythiques de la création et de la nature. Car le besoin de sens naît du besoin de contrôle, il est une émanation de notre désir de survie. Et le fait de se représenter le monde comme un lieu habité par un sens apaise le système d'alerte interne en cas d'erreur de prédiction ponctuelle dans le monde concret.

Dès lors la réaction d'alerte déclenchée par le cortex cingulaire est naturellement atténuée chez les croyants. Mais aujourd'hui, pour la plupart des personnes dans le monde occidental, nous n'y croyons plus, et nous avons perdu ce sens.

Les grands systèmes de sens religieux, mais aussi idéologiques, démocratiques ou philosophiques ne sont pour ainsi dire que des référents dévalues, fragilisées par les connaissances scientifiques et par la coexistence de multiples messages spirituels ou idéologiques que l'on s'efforce de tolérer, mais dont la seule multiplicité suffit à réduire à néant l'espoir que l'un d'entre eux puisse à lui seul détenir une vérité absolue.

Dans l'histoire, les rituels sont systématiquement apparus avant les systèmes moraux. Cette synchronisation et ce mimétisme qui est à l'œuvre dans les rituels collectifs rendent les humains plus sensibles aux sentiments, aux désirs et aux émotions de leurs semblables. Cette capacité à nous mettre à la place d'autrui est appelée empathie cognitive, elle nous permet de nous plonger "dans la peau de l'autre" de ressentir ce qu'il ressent et de penser ce qu'il pense. Et cette capacité est décuplée par l'imitation, les partenaires synchronisés éprouvent de la compassion l'un pour l'autre.

Les rituels permettent d'apaiser notre cortex cingulaire en pouvant prédire avec plus de fiabilité ce que nos semblables sont susceptibles de faire ou de ne pas faire par le simple fait que l'on peut déjà prédire leur mouvement lors du rituel.

Mais il est plus difficile de prédire les pensées des gens que leur mouvement. Pour y arriver, il s'agit de partager, non plus de simples gestes, mais des représentations mentales. Autrement dit, des valeurs et des visions du monde.

Il peut exister différentes visions du monde, certaines sont dites normatives, en ce sens qu'elles prescrivent certains comportements, et d'autres sont seulement positives (ou factuelles), et à l'inverse ne prescrivent pas de comportements. Lorsque cette vision prescrit ce qu'il est bon de faire ou pas, et que les gens y adhèrent et voient le monde de la même façon que nous, cela permet de réduire considérablement l'incertitude et de facto l'activité du cortex cingulaire.

Mais leur violation, à l'inverse, l'active fortement (ainsi que d'autre structure cérébrale). Pour notre cortex cingulaire antérieur, le non-respect des normes morales est une erreur de prédiction fondamentale. Ce qui pour la régulation du comportement d'autrui constitue en tant que telle un puissant facteur d'apaisement du cortex cingulaire qui cherche à se repérer parmi ses semblables.

Dès l'instant ou vous êtes certain que les autres croient en les mêmes valeurs morales sacrées que vous, vous pouvez commencer à prédire avec une fiabilité accrue ce qu'ils sont susceptibles de faire ou de ne pas faire.

Dans le terme religion, le verbe latin religere signifie relier, on comprend que la religion permet principalement de relier les individus entre eux.

Au fil des découvertes l'humanité a perdu sur le plan social et moral au profit de la prédiction sur le monde matériel. Comme l'a dit le célèbre philosophe Friedrich Nietzsche "Dieu est mort, tout est permis".

Pour notre cortex cingulaire, c'est le début de l'angoisse. Car le sens fourni par la science n'offre qu'une vision positive (ou factuelle), elle ne recèle donc pas tout à fait le même pouvoir rassurant que celui fourni par la religion et la morale.

La science introduit un sens factuel, mais pas, du moins au début, un sens moral.

Certes le sens factuel obtenu par la science est colossal. Aujourd'hui, l'être humain sait d'une certaine façon, comment s'est formée la terre et son arrivée sur celle-ci, il peut retracer assez précisément la suite d'événements qui ont conduit les espèces à évoluer depuis les bactéries jusqu'aux premiers vertébrés, aux mammifères, aux singes et à Homo Sapiens. La question de la cause de son existence, de sa nature et de ses caractéristiques physiques et mentales est entendue. De même la connaissance des lois de la physique et du vivant lui permet de prédire une quantité effarante de faits concrets, tels que le temps qu'il fera, les éclipses de la lune et du soleil, la puissance d'un moteur à combustion en fonction du degré de raffinement de l'essence utilisé et de la cylindrée, etc.

Mais nous ne savons plus ce qui a un sens sur le plan humain et existentiel. Car si nous avons tué le sens, nous n’avons pas tué le besoin de sens. La partie de notre cerveau qui avait mis des centaines de milliers d'années à se conformer pour trouver du sens dans le monde et dans les structures sociales est toujours bien là. [^36]

Dans notre société nous sommes en train de vivre le mythe de Sisyphe, un personnage mythologique grec condamné à rouler éternellement un rocher jusqu'en haut d'une colline et qui en redescendait chaque fois avant de parvenir au sommet. [^39] Dans notre société nous n'avons plus de sens dans notre façon de produire. Nous sommes aujourd'hui dans un monde jetable. Et quand rien ne dure, la vie devient peine perdue. Nous sommes tous plus ou moins confrontés à ce problème.

De plus, aujourd'hui avec le dérèglement climatique plus rien n'est stable, ni le cycle des saisons, ni le niveau de la mer, ni l'apparition des catastrophes naturelles. Le monde est désormais en mouvement, et ce mouvement va dépasser nos capacités d'endettement.

Le mot est dur, mais l'incertitude tue, elle mine notre cerveau, elle détruit l'aspiration humaine fondamentale au sens.

## Nos réactions face à l'incertitude

Le sentiment d'indétermination active le cortex cingulaire, qui relève douloureusement l'impossibilité de faire des prédictions sur l'avenir, de se définir soi-même et d'entrevoir une trajectoire de vie claire.

L'éco-anxiété par exemple est une nouvelle forme d'angoisse qui résulte de la conscience paralysante de la destruction de notre planète. [^38]

Dans un monde ou règne un niveau maximal d'incertitude, le cortex cingulaire va s'arranger pour restaurer de la certitude à de plus petites échelles, puisqu'il ne peut le faire à celle, plus globale, de son existence. C'est ce qu'on appelle micro certitude.

La drogue qui joue sur nos neurones peut être un substitut au sens. La cocaïne par exemple détruit l'incertitude.

L'estime de soi est l'invention autocentrée d'un monde individualiste et une façon de compenser la perte de sens.

L'argent est le passeport absolu pour se libérer de l'angoisse de l'incertitude.

Mais dans le jeu de la compétition, les inégalités se creusent. Les perdants de cette course, déshumanisés par la rupture du cordon, vont alors dans le pire se tourner vers une forme dégradée d'humanité. Celle qui entend nier la part d'humanité chez les autres pour s'en assurer l'exclusivité. On parle ici de l'identité.

Car cette tension fait automatiquement naître un besoin urgent : savoir une fois pour toutes qui l'on est. Et le moyen simple de clarifier son identité est de se définir par référence à un groupe d'appartenance. Se définir à travers sa relation d'appartenance à un groupe remplit un besoin fondamental, trouver un moyen de se conformer à des règles en vigueur, ce qui limitera les réactions intempestives du cortex cingulaire.

D'ailleurs une des principales sources de sens régulièrement évoquées dans les enquêtes est le sentiment d'appartenance à un groupe social.[^40]

Les inégalités précipitent les sociétés vers le repli identitaire. Où seule compte la capacité à utiliser la machine de production à ses propres fins. Le modèle économique néolibéral, fondé sur la compétition, la mobilité des personnes, l'accélération des rythmes de travail, la réduction des dépenses publiques dédiées à leur protection et à la répartition équitables des richesses provoque une pulsion autoritaire et identitaire destinée à apaiser les réactions de l'individu face à l'incertitude et au déficit d'appartenance sociale.

L'anomie désigne le sentiment d'une perte d'ordre et de logique dans la société.

Les personnes qui ont le plus souvent des pensées nostalgiques sont aussi celles qui ont l'impression la plus nette que l'existence a un sens.[^41]

Notre cerveau est extrêmement doué pour le déni.

Quand nos actes ne sont pas en accord avec nos représentations mentales, cela crée de la dissonance cognitive. La contradiction active également la dissonance cognitive.

Car le cerveau humain cherche la cohérence et la raison en toutes circonstances. Il va donc s'efforcer de résoudre la dissonance qui représente une incertitude, pour cela il va soit modifier ses actes pour qu'ils s'alignent avec la pensée, ou alors modifier la pensée pour qu'elle soit en phase avec les actes.

Les recherches sur la dissonance cognitive montrent que la plupart du temps ce sont les pensées qui s'adaptent aux actes et non l'inverse. [^42][^43]

Nous décrochons du réel, parce que nous ne savons plus l'interpréter. Le sens nous protège parce qu'il nous propose des manières d'interpréter le monde et d'agir plus efficacement pour le maîtriser et nous protéger des menaces qu'il renferme.

À mesure que se rapprochera le spectre des grandes catastrophes, les replis communautaires ne feront que s'accentuer, de même que les comportements compensatoires d'hyperconsommation, d'inflation de l'égo et le déni.

Le manque de systèmes de signification nous pousse à consommer des biens matériels et tout particulièrement dans les situations de forte incertitude, d'accélération et de compétition. L'hypermatérialisme est le palliatif à l'incertitude et à la compétition.[^36]

## Solutions face au manque de sens

Le cerveau cherche à prédire le monde afin de mieux le contrôler, et il y parvient soit par le sens, soit par la technique. Quand la technique progresse, il n'a plus besoin du sens. Et quand la technique lui fait défaut, il lui faut du sens.

Aujourd'hui nous avons la technique, mais celle-ci a échoué, car elle est en train de signer notre extinction. [^44]

Au vu du désastre en cours et à venir, nous devons arrêter au plus vite la machine à consommer qu'est devenue l'humanité tout en cherchant activement des moyens de réparer la terre.

Si plus le sens diminue et plus la consommation augmente, alors à l'inverse plus le sens augmente et plus la consommation diminue.

Nous avons 8 milliards de cortex cingulaires occupés à tromper leur peur de la mort et de l'imprévisible, il faut maintenant les occuper à tromper cette peur en les réunissant autour d'un sens partagé.[^36]

Ce sens doit emmener des milliards de personnages d'origines culturelles différentes, d'héritage civilisationnel épars, de formations intellectuelles hétéroclites, pour les faire oublier l'obsession de posséder et d'exploiter.

Il existe 2 sens: le sens cosmique et le sens social.

Le premier repose sur notre compréhension des lois de la nature, de la matière et de l'univers. La deuxième fait appel à notre capacité à agir, en contexte social, en conformité avec ce que nous croyons être le bien et le mal.

Ils créent un système moral, ils définissent un bien et un mal qui permettent de réguler l'action sociale et individuelle.

Le sacré est le pivot à partir duquel se construit le sens dans les assemblées d'humains. Aujourd'hui, l'humanité moderne a perdu le sacré en déconstruisant le réel et en découvrant que tout, depuis le mouvement des atomes dans une étoile en fusion jusqu'aux flux d'ions dans la membrane de vos neurones pendant que vous lisez ces lignes, obéit à des lois mécanistiques où le bien et le mal n'ont pas leur place.

La conséquence de cette démoralisation du monde est que chaque individu à la surface de la Terre peut décider de ce qu'il trouve bon ou mal.

Or aucun sens collectif ne peut exister sans la notion de sacré. La solution est de recrée du sacré, et celui-ci peut être notre terre.

Le sacré ne peut se manifester qu'au travers de rituel, l'humanité devra inventer de nouveaux rituels pour sortir de l'impasse où elle s'est enferrée. Et on l'a vu, les pratiques synchrones permettent donc d'apaiser le cortex cingulaire. De tels rituels devront sacraliser la préservation de la planète.

En procédant de la sorte, les humains peuvent se doter de moyens efficaces pour faire tomber la charge mentale d'angoisse liée à l'individualisme. Savoir que les citoyens de mon pays, du continent tout entier, si possible de la planète tout entière tiennent pour sacré ce que je tiens moi-même pour une valeur indépassable crée le fondement d'un nouveau pacte d'appartenance et de confiance.

L'homme est un coopérateur conditionnel, il est capable d'immenses sacrifices à condition d'avoir l'assurance que les autres membres de sa communauté feront de même. [^45]

En se donnant pour valeurs sacrées la pureté carbone et la préservation des équilibres biologique et géologique, les milliards d'humains peuvent faire revenir l'activité de leurs cortex cingulaire à des niveaux acceptables et cesser d'être tributaires de biens matériels, de drogues, d'argents et de substituts d'égo pour supporter leur propre existence.

L'écologie peut proposer une vision du monde commune à tous les humains.

Avec la connaissance, la liste des actes compatibles avec la valeur sacrée est affaire de science, de mesures et de calculs réalisés par le collectif humain.

L'enjeu est la création d'un sentiment d'identité globale qui donne à chacun le sentiment d'être accepté dans un groupe, sans qu'il ait besoin de prouver sa valeur et son éligibilité par l'accumulation de biens matériels ou de supports d'ego.

Aujourd'hui il faut accepter que la science puisse dire non seulement le vrai et le faux, mais aussi le bien et le mal.

Le troisième sens à venir sera donc le sens écologique.

Pour le développer, il faut développer la connaissance par l'enseignement pour transmettre la connaissance des systèmes vivants, de la biodiversité, de la climatologie, de l'écologie et de l'évolution des espèces.

Ce qui permettra de créer l'émerveillement, car l'émerveillement constitue un antidote puissant contre l'angoisse existentielle qui nous saisit, par la découverte de la richesse infinie du vivant, de la beauté des paysages, de la flore et de la faune, car on y trouve une source de sens.

Habitant tous la même Terre, agissant tous sur elle et subissant tous les conséquences de sa dégradation, nous sommes donc amenés à reconnaitre une même valeur sacrée.

Ainsi les citoyens pourront s'engager dans des actions qui sont cohérentes avec leurs valeurs et leurs convictions, ils éprouveront alors le sentiment de sens.

Devenir acteur de ce changement est en soi déjà porteur de sens, et d'alignement de nos opinions et de nos actes.

Il faut revoir les fondements mêmes de notre civilisation. La logique de production et de consommation à flux tendu qui sous-tendent nos économies et ne sont pas viables. Vous découvrez dans le chapitre suivant notre proposition en termes d'économie'.

Nous avons au fond de nous tous une aspiration immense, nous sommes en quête de sens et nous ne savons pas comment l'assouvir. Cette oeuvre a pour objectif de l'assouvir, et ce déjà sans même attendre sa concrétisation, car on le trouve déjà au commencement même de sa réalisation.

## Besoin de fiction - Le cerveau conteur

Le cerveau humain, cette extraordinaire machine à fabuler, est au cœur de notre capacité à créer des récits, tant pour le meilleur que pour le pire. Il forge les histoires qui nous permettent de justifier nos actions et de leur donner un sens. Cette faculté narrative n'est pas un hasard mais le produit d'une évolution complexe, révélant la profondeur de notre besoin de sens et d'ordre dans un monde chaotique. [^46]

Ainsi le propre de notre espèce n'est pas par exemple qu'elle se livre à la guerre depuis la nuit des temps, les chimpanzés et les fourmis le font tout autant, mais c'est qu'elle en fait toute une Histoire... et des millions d'histoires.

Les autres animaux ne font pas du mal pour le mal ni du reste pour le bien, ils agissent principalement sous l'impulsion de besoins immédiats ou instincts. Les êtres humains eux cherchent un sens à leurs actions et à celles d'autrui, souvent à travers le prisme de la moralité, du bien et du mal. Cette quête de sens est donc une manifestation de nos fonctions cérébrales supérieures, qui nous poussent à rechercher des explications et à justifier nos comportements dans un contexte plus large.

Ce que nous considérons comme le "soi" n'est pas une entité isolée ou autonome, mais plutôt une mosaïque de traits, de croyances et de valeurs empruntés, partagés et parfois contestés au sein de notre entourage. Chaque interaction, chaque échange culturel ou social contribue à façonner notre identité, la rendant dynamique et évolutive. Notre soi est tissé des autres. Nous nous définissons en miroir de nos relations, chaque personne étant un reflet qui nous aide à former notre propre image.[^49]

Ce processus de construction narrative résonne avec les enseignements du bouddhisme sur l'illusion du soi. Selon le bouddhisme, le "moi" que nous percevons comme une entité stable et continue est une illusion, une construction de l'esprit. [^47] Nous aurons l'occasion de revenir plus en profondeur sur ce point dans la partie Religion de la partie 2. De la même manière, les neurosciences révèlent que notre identité est une histoire que nous nous racontons, une suite cohérente fabriquée de toutes pièces par notre cerveau pour maintenir une illusion de continuité.[^48]

L'identification à nos histoires et à notre "moi" construit peut nous enfermer dans des schémas de pensée limitants. Reconnaître cette construction narrative comme une illusion peut être libérateur. Cela nous invite à remettre en question les récits traditionnels et à nous ouvrir à de nouvelles possibilités de perception et d'expérience du monde.

L'interaction entre "vraie vie" et fiction prend alors une nouvelle dimension. Plutôt que de voir ces sphères comme distinctes, nous pouvons comprendre qu'elles sont intrinsèquement liées, se nourrissant mutuellement. Nos narrations personnelles et collectives ne sont pas de simples distractions ou évasions mais des outils puissants de transformation et de compréhension.

Reconnaître la nature narrative de notre cerveau et reconnaître l'illusion du soi ne signifie pas pour autant rejeter notre identité ou nos histoires mais les voir pour ce qu'elles sont : des constructions flexibles, modifiables et profondément humaines. Ca nous encourage à ne plus suivre les récits traditionnels et se dire "Voici comment est le monde depuis toujours et pour toujours", mais comprendre et se dire "On ne voit que ce qu'on voit".

Cela nous offre une voie vers une compréhension plus riche et plus nuancée de nous-mêmes et du monde, de ne plus se coller à son identité comme une réalité inamovible et de s'y identifier exclusivement, mais de laisse entrevoir dans nos vies la possibilité de réimaginer le monde et de nous réimaginer nous mêmes.

## Effets et biais cognitif

Nous allons ici passer en revue les différents effets et biais de notre cerveau, car ils peuvent affecter notre perception, notre mémoire, nos jugements, nos décisions et notre comportement de diverses manières, en influençant la façon dont nous traitons et interagissons avec l'information. Cette compréhension de nous-mêmes nous permettra de développer des outils efficaces pour notre société, notamment pour l'élaboration de notre processus de prise de décision et d’éviter les conséquences de ces biais qui nous empêcheraient d'atteindre notre but commun.

La lecture détaillée des effets et biais cognitifs bien que conseillé n'est nullement nécessaire et peut être passé pour votre confort de lecture, ceux-ci vous ont été résumés dans la section suivante.

### Résumé des effets et bias cognitif

Voici une liste non exhaustive de biais cognitifs :

- Biais de sélection : Nous sommes plus enclins à sélectionner les informations qui correspondent à nos croyances et opinions préconçues, plutôt que de considérer un éventail plus large d'informations. [^50]

- Illusion mémorielle : Nous avons tendance à surestimer la fiabilité de nos souvenirs et à les colorer avec notre perception actuelle.[^51]

- Rationalisation des convictions : Nous avons tendance à justifier nos décisions et nos comportements, même s'ils sont erronés ou nuisibles, en les interprétant de manière favorable pour nous-mêmes.[^53]

- Biais de la surestimation : Nous avons tendance à surestimer notre capacité à prévoir les événements futurs et à contrôler les situations.[^52]

- Biais de l'optimisme : Nous avons tendance à surestimer la probabilité de bénéfices futurs et à sous-estimer les risques potentiels. [^54]

- Biais de la réduction de la complexité : Nous avons tendance à simplifier la complexité des situations pour les rendre plus gérables.[^55]

- Biais de l'inférence causale : Nous tendons à attribuer une cause à un événement même lorsque cela n'est pas nécessairement justifié ou raisonnable.[^56]

- Biais de l'effet de réduction : Nous tendons à surestimer les effets positifs d'une action et à sous-estimer les effets négatifs.[^57]

- Biais de l'effet de primauté : Nous sommes influencés par les premières informations que nous recevons et avons tendance à les pondérer plus lourdement que les informations ultérieures.[^58]

- Biais de l'effet de similarité : Nous sommes plus enclins à apprécier et à faire confiance aux personnes qui nous sont similaires.[^59]
- Heuristique de disponibilité : Nous utilisons souvent une estimation rapide et intuitive pour évaluer la probabilité ou la fréquence d'un événement, basée sur les informations les plus facilement disponibles dans notre mémoire.[^60]

- Pression des pairs : Nous sommes influencés par la perception de ce que les autres pensent de nous et sommes soumis à la pression pour conformité à leurs opinions et comportements.[^61]

- Biais de halo : Nous tendons à généraliser une opinion positive ou négative sur une personne en fonction d'une seule caractéristique.[^62]

### Explication approfondie des effets et biais cognitif

- Biais de confirmation

Le biais de confirmation est un biais cognitif, c'est quand on privilégie les informations qui vont dans le sens de notre opinion et de nos croyances. Comme nous cherchons du sens, nous sélectionnons les informations qui vont dans notre sens. La solution à ça est d'être son propre contradicteur, de soumettre nos pensées à rude épreuve et d'essayer de prouver qu'elles sont fausses.

Ainsi les contradictions des jugements ne doivent pas nous blesser, mais nous éveiller et nous mettre en action. Nous n'aimons pas la rectification de nos opinions, mais il faudrait au contraire s'y prêter et s'y offrir, et que celles-ci puissent venir sous forme de conversation et non de leçon magistrale.

À chaque opposition, il ne faut pas à tort ou à raison, chercher comment s'en débarrasser, mais au contraire chercher en profondeur s’il elle peut être juste.

- Biais de disponibilités

Le biais de disponibilité est un autre biais cognitif qui peut affecter la prise de décision. Il se produit lorsque nous estimons la probabilité d'un événement ou la fréquence d'une occurrence en fonction des exemples qui nous viennent rapidement à l'esprit. Ce biais peut nous pousser à sous-estimer la probabilité d'événements rares et à surestimer celle des événements courants ou récents. Nous surestimons notre raisonnement en privilégiant les informations directement accessibles à notre mémoire.

Pour éviter ce biais, il est important de prendre en compte un échantillon représentatif d'informations et d'essayer d'être objectif lorsque nous évaluons les probabilités. Il est également important de ne pas se fier uniquement aux informations qui sont immédiatement disponibles, mais de rechercher des informations supplémentaires qui peuvent nous donner une vue plus complète et équilibrée.

- Effet de halo

L'effet de halo est le mécanisme qui nous fait attribuer des qualités à quelqu'un de par son apparence physique.

Par exemple un site de rencontre nommée cupidexe a fait une expérience, la possibilité de noter sur la personnalité et sur physique le profil des gens. Ceux qui ont une note basse en physique l'ont en personnalité et inversement une note haute en physique, une bonne personnalité. On pourrait croire que c'est par ce qu'ils semblent avoir une bonne personnalité qu'on leur attribue un bon physique, mais dans le top des 1% certains profils en personnalité sont vides…
Ils ont refait une expérience pour vérifier, ici les utilisateurs tombaient sur le profil avec du texte et parfois non, et que vous ayez du texte ou pas on vous attribuait la même note.

Il existe notamment d'autres études et statistique qui confirment cet effet. Par exemple tous les présidents américains sont grands, et plus on est grand, plus on gagne d'argent.

- Effet barnum

L'effet de Barnum désigne la tendance des gens à croire que des commentaires vagues ou généraux sont des descriptions précises de leur personnalité ou de leur vie. Ce phénomène est nommé en l'honneur du célèbre showman P.T. Barnum, qui a utilisé des lectures de personnalité vagues pour attirer les gens à ses spectacles.

Par exemple, un test de personnalité qui affirme que "vous êtes une personne attentionnée et aimante envers votre famille et vos amis" est suffisamment vague pour être considéré comme vrai pour la plupart des gens. Cependant, cela ne donne pas de détails précis sur la personne, ce qui peut mener à un sentiment d'identification avec la description générale, même si elle n'est pas très personnelle.

L'effet de Barnum peut être renforcé par le besoin de reconnaissance et la soif d'autodétermination des gens. Les gens aiment se sentir compris et valorisés, ce qui peut les inciter à accepter des descriptions générales comme étant vraies pour eux.

Il est important de prendre en compte l'effet de Barnum lorsque vous analysez des tests de personnalité ou des commentaires sur votre vie ou votre personnalité. Il est toujours préférable de chercher des informations plus spécifiques et détaillées pour avoir une compréhension plus précise de soi-même.

- Effet d'ancrage

L'effet d'ancrage désigne la tendance des gens à se baser sur une première information, appelée "ancre", pour faire des estimations ou prendre des décisions. Cette première information peut avoir un impact important sur les estimations ultérieures, même si elle est peu pertinente ou même erronée.

Par exemple, si on vous demande de deviner le nombre de personnes vivant dans votre ville et que l'on vous donne comme première information un chiffre élevé, vous aurez tendance à faire une estimation plus élevée que si on vous avait donné un chiffre plus bas. De même, si vous êtes en train de négocier le prix d'un objet, la première proposition sera souvent considérée comme l'ancre et aura une influence sur les propositions suivantes.

L'effet d'ancrage peut être utilisé de manière consciente ou inconsciente dans des situations de négociation, de vente ou de persuasion, ce qui peut mener à des décisions peu rationnelles et biaisées.

- Pression des pairs

La pression de groupe est un phénomène social qui décrit la tendance des individus à se conformer aux opinions, attitudes et comportements d'un groupe auquel ils appartiennent ou avec lequel ils se sentent affiliés. Ce phénomène peut se produire lorsque les individus cherchent à se conformer aux normes sociales et à éviter la désapprobation ou l'isolement du groupe.

Par exemple, lorsqu'une personne se retrouve dans un groupe de personnes qui a une opinion ou une attitude particulière, elle peut être influencée par ces opinions et ces attitudes, même si elles ne correspondent pas à ses propres opinions ou attitudes. Cela peut être particulièrement vrai dans les situations où la personne veut être acceptée par le groupe et éviter la désapprobation ou l'exclusion.

La pression de groupe peut également se manifester lorsqu'une personne se sent obligée de se conformer aux comportements d'un groupe pour éviter de se sentir différente ou mal à l'aise. Par exemple, une personne qui se trouve dans un groupe qui boit de l'alcool pourra être influencée à consommer de l'alcool elle-même, même si elle n'en a pas envie.

Il est important de prendre conscience de la pression de groupe et de ne pas céder aux opinions ou aux comportements d'un groupe simplement pour éviter la désapprobation ou pour se conformer aux normes sociales. Il est toujours préférable de suivre ses propres opinions et convictions plutôt que de céder à la pression de groupe.

- Heuristique de disponibilité

L'heuristique de disponibilité est un raccourci mental que nous utilisons souvent pour évaluer la fréquence ou la probabilité d'un événement en nous basant sur les exemples les plus récents et les plus facilement disponibles dans notre mémoire. Cela peut conduire à des erreurs dans notre jugement, car il peut y avoir des biais dans les exemples que nous avons en tête.

Par exemple, si une personne vient d'entendre parler d'un accident d'avion à la télévision, elle peut se sentir plus inquiète pour sa sécurité lors de son prochain vol, car cet événement est frais dans sa mémoire et facilement disponible. Cependant, cette personne peut sous-estimer la sécurité de l'aviation en général, car les accidents aériens sont en réalité très rares.

Il est important de se rappeler que l'heuristique de disponibilité peut entraîner des erreurs dans notre jugement et de chercher des informations plus complètes et fiables pour évaluer la fréquence ou la probabilité d'un événement. Il est également utile de se rappeler que les médias peuvent influencer notre perception des événements en mettant en avant certaines histoires plutôt que d'autres. Cela peut conduire à une surreprésentation des événements rares ou dramatiques, et donner une fausse impression de leur fréquence réelle. Il est donc important de se rappeler de ne pas se laisser influencer uniquement par ce que nous voyons ou entendons dans les médias.

En conclusion, l'heuristique de disponibilité peut être utile pour prendre des décisions rapides dans des situations quotidiennes, mais il est important de le considérer avec prudence lorsqu'il s'agit de faire des jugements plus importants ou de prendre des décisions qui ont des conséquences à long terme. Il est préférable de rechercher des informations complètes et fiables pour évaluer les probabilités et les risques, plutôt que de se fier uniquement à ce qui est disponible facilement dans notre mémoire.

## Conclusion

A travers cette partie sur Homo Sapiens, on peut déjà voir de nombreuses pistes s'ouvrir. Nous en développerons quelque un dans cette conclusion, mais cette liste est non-exhaustive. Nous en développeront surtout de nombreuses autres dans les chapitres suivants en nous appuyons sur cette base développer ici.

Nous connaissons maintenant les besoins de l'être humain qu'ils nous faut assouvir pour un épanouissement total. Nos avons vu qu'il était possible transformer nos besoins extrinsèques et qu'il sera nécessaire de le faire pour les rendre compatibles à un projet commun qui permettra à tous de combler notre besoin de sens.

Ce sens collectif doit exister par la notion de sacré. Et il n'y a rien de plus sacré que ce que nous avons tous en commun, et dont la survie est directement liée à nous, c'est à dire notre terre et ses écosystèmes. Ainsi la science et l'écologie, n'est plus seulement capable de nous dire ce qui est vrai ou faux, mais est aussi capable de nous dicter nos valeurs, ce qui est bien ou mal.

Nous avons vu qu'il n'y a pas de nature humaine pour nous empêcher de réussir ce projet, n'y même d'un soi inamovible et dans cette tâche le développement de notre conscience sera notre meilleur allié. L'éducation des nouvelles générations en favorisant l'altruisme et l'émerveillement de la nature pour la protéger le sera tout autant.

A travers l'étude des différents effets et biais cognitif, nous avons récolté toujours plus d'informations nécessaire à la compréhension de nous mêmes et donc aussi au développement d'outils qui prendront en compte ces paramètres pour les rendre efficaces afin de nous aider dans notre projet commun.

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="f8551629-0988-d794-7945-f763b16b1182" ></div>
</div>

## Sources

[^1]: [Rapport du GIEC](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipédia : Pyramide des besoins](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectives on Pshychological Science
[^4]: [Organisation des Nations Unies pour l'alimentation et l'agriculture (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Chiffres et analyses sur le mal-logement en France, y compris le nombre de personnes sans-abris et les conditions de vie précaires.](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Le renoncement aux soins pour raisons financières dans l’agglomération parisienne :](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [La Fondation de France publie la : 13e édition de son étude sur les solitudes](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap international : Les personnes handicapées restent parmi les plus exclues au monde](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivation and Personality, 1970 (deuxième édition)
[^10]: Arendt Hannah, La condition de l'homme moderne, 1961
[^11]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes
[^12]: Emmanuel Todd, L'origine des systèmes familliaux, 2011 - Où en sommes-nous ? Une esquisse de l'histoire humaine, 2017 - La lutte des classes en France au XXI siècle, 2020.
[^13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997
[^14]: Jean Baudrillard, La société de consommation, 1970
[^15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994
[^16]: Charles Darwin, L'Origine des espèces : Chapitre IV / III, 1859 & La Descendance de l'homme et la sélection sexuelle : Chapitre VII, 1871
[^17]: Hannah Arendt, Les origines du totalitarisme, 1951
[^18]: Albert Camus, La peste, 1947
[^19]: Jean-Paul Sartre, L'existentialisme est un humanisme
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, L'homo Sociabilis : La réciprocité
[^21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, Le bug humain
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, The Primal Brain
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, L'altruisme : une énigme ?
[^29]: Matthieu Ricard, "L'altruisme : Le gène ou l'éducation ?"
[^30]: Kathryn Spink, "Mère Teresa : Une vie au service des autres"
[^31]: Wolfram Schultz, The Role of Dopamine in Learning and Memory, 2007
[^32]: Read, D., & Northoff, G, Neural correlates of impulsive and reflective decisions. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, Le Cerveau et le Plaisir
[^34]: David Eagleman, Le Cerveau inconscient
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 and Gloria Y. Yeh3, A Systematic Review and Meta-analysis of the Effects of Meditation on Empathy, Compassion, and Prosocial Behaviors, 2018
[^36]: Sébastien Bohler, Où est le sens ?, 2020
[^37]: [Roy F. Baumeister, The Need for Meaning: A Psychological Perspective](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Eco-anxiété : analyse d’une angoisse contemporaine
[^39]: Albert Camus, Le Mythe de Sisyphe, 1942
[^40]: [Enquête "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, L'homme irrationnel 2001
[^43]: Elliot Aronson, La théorie de la dissonance cognitive
[^44]: Elizabeth Kolbert, La 6e Extinction, 2015
[^45]: Robert Axelrod, Le dilemme du prisonnier
[^46]: Nancy Huston, L'epèce fabulatrice
[^47]: Serge-Chritophe Kolm, Le bonheur liberté
[^48]: David M. Eagleman, Incognito, 2015
[^49]: William James, The Principles of Psychology, 1980
[^50]: [Wikipedia : Biais de sélection](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, L'illusion De La Mémoire
[^52]: David Dunning et Justin Kruger, Unskilled and unaware of it: How difficulties in recognizing one's own incompetence lead to inflated self-assessments, 1997
[^53]: [Festinger et Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones et Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Thinking, Fast and Slow, 2011
[^58]: [Asch, S. E., Forming impressions of personality. , 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. A constant error in psychological ratings, 1920
