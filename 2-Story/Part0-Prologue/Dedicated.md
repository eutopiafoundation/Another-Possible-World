---
title: Dédicaces
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---


Pour l'idéal en chacun de nous qui peut souffrir d'un monde qui ne l'est pas.

Pour la souffrance de ceux qu'on aime et celle de tous ceux que nous ne connaissons pas.

Cette œuvre a pour vocation d'être réalisée par l'humanité tout entière et d'être mise à jour tout au long de son évolution\, par et pour elle\-même.

<center>
  
---


<p style="font-size: larger; font-weight: bold; text-decoration: underline">Propositions directes sur cette page</p>
</center>

<div style="margin-top: 20px;" data-canny id="cannyBoardToken" data-token="c34fd2a0-1759-f5a0-46e0-9d07e3c1bf8f" ></div>
</div>
