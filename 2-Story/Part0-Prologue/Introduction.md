---
title: Introduction
description:
published: true
date: 2023-03-07T21:16:06.598Z
tags:
editor: markdown
dateCreated: 2023-03-07T20:33:18.121Z
---

## Qui êtes vous ?

Vous n'êtes peut être pas un grand sociologue, ni un grand philosophe, ni un grand économiste, ni un grand quoi que ce soit d'autre, à vrai dire peut être n'avez-vous pas fait de grandes études universitaires, peut être étiez vous même mauvais à l'école. C'est le cas pour bon nombre d'entre nous aussi, pourtant malgré ces parcours scolaires si différents, nous avons la confiance de vous proposer ici un autre monde possible, plus encore, un monde qui vise l'idéal, et nous vous invitons à nous rejoindre.

Alors peut être que vous vous demandez pourquoi vous seriez bien placé pour sa réalisation. Et peut-être qu'une partie de vous, à la simple lecture de ce paragraphe, ressent déjà l'envie de fermer ce livre pour ne pas perdre plus de temps. Mais nous vous prions de nous accorder encore quelques paragraphes pour tenter de vous convaincre.

## L'imaginaire

Bien que vous ne soyez pas un grand expert dans aucun des grands domaines de ce monde, il y a une chose pour laquelle vous êtes particulièrement doué, ce don, c'est de rêver. Cette faculté de pouvoir imaginer, c'est le plus grand don de l'être humain. Nous vous invitons ici à rêver, mais surtout à venir faire rêver les autres.

Au point de départ, nous n'étions qu'une espèce insignifiante et notre pouvoir d'action était très limité. Ce qui a fait la différence entre notre espèce, Homo Sapiens et les autres, c'est notre capacité à coopérer en grand nombre. [^1]

Et celle-ci nous la devons à notre imagination, cette capacité à inventer des récits et à nous les raconter.

Grâce aux mythes collectifs, que l'on peut aussi appeler "fictions", nous avons pu nous rassembler et coopérer.

Nous avons la faculté de croire en des choses qui n'existent pas, nous avons inventé de nombreuses légendes, des mythes, des récits, nous avons tracé des lignes imaginaires sur le monde, créé des nations, des coutumes, nous avons inventé l'argent et nous lui avons conféré la plus grande des valeurs de notre imaginaire.

Ce don de l'imagination nous vient de notre besoin de sens, ce besoin que l'on a de prédire et de comprendre notre environnement afin de mieux y survivre.[^2]

Cet imaginaire collectif, nous le laissons diriger notre vie entière, nous le laissons même diriger le destin de l'humanité tout entière, sa puissance est telle qu'elle nous amène jusqu'à détruire notre propre environnement, provoquer notre propre destruction.

Mais tout ça ne reste fondamentalement qu'une pure imagination collective et cesse d'exister dès l'instant où nous décidons tous de cesser d'y croire.

En réalité ce que nous appelons plus communément la raison est plus souvent le fait de continuer à croire et à se conformer aux règles de notre imaginaire collectif.

## La raison

Une fois que l'on décide de quitter cet imaginaire, c'est là que nous pouvons enfin réellement retrouver la véritable raison.

Alors peut-être que vous estimez ne pas y connaître grand-chose, mais vous découvrirez qu'il n'y a pas besoin de savoir grand-chose pour trouver les solutions et réaliser les choix qui nous mèneraient tous vers une vie épanouie et riche de sens.

Car les réponses à nos questions sont déjà en chacun de nous. Pour les trouver, il suffit de quitter notre imaginaire collectif, d'ouvrir notre cœur et de l'écouter. Cela peut vous paraitre niais, mais nous sommes convaincus que c'est bien là que se trouve la véritable raison.

Et nous qui avons déjà entamé ce processus de déconstruction, c'est ce que l'on vous propose de découvrir ici, un monde réellement basé sur la raison.

Un monde qui de par sa simplicité, son sens et sa logique vous semblera à vous aussi, évident.

Vous découvrirez alors que vous aussi, qui que vous soyez, quelles que soient vos connaissances, vous n'avez pas besoin de savoir grand-chose pour juger de ce qui est bon et de ce qui ne l'est pas, que vous n'avez pas besoin de grand-chose pour contribuer à l'élaboration de ce monde.

> « Mais alors, dit Alice, si le monde n'a absolument aucun sens, qui nous empêche d'en inventer un ? » Inspiré par Lewis Carrol

<!-- > « Toute innovation passe par trois phases dans l’opinion publiques : 1) C’est ridicule 2) C’est dangereux 3) C’est évident. » Citation apocryphe d'Arthur Schopenhauer -->

<div style="width:full;display:flex;justify-content:center">
<img style="max-width:800px;" src="/0-GlobalProject/Image/presentation/1.webp" />
</div>
<center><a style="font-weight:500;color:#13af36;"href="https://www.instagram.com/another.possible.world/" target="_blank">See more concept art from Eutopia</a></center>

## L'eutopie

Alors vous vous dites peut être que ce que l'on vous propose ici c'est une utopie.

Utopie est un mot construit à partir du grec ancien, dérivé de « tópos » (« lieu »), avec le préfixe « ou » (« non »), littéralement « non lieu ».[^3]

L'utopie est un mot bien souvent mal utilisé, malmené. Il est brandi à tout va pour étouffer l'espoir, dénigrer l'imagination, refouler le progrès, il est un appel à la résignation, un mot clé du système de l'exploitation, de l'aliénation et de la domination pour accepter le statu quo.[^4]

Mais un "non lieu", c'est une société qui n'existe pas, non pas une société impossible. Et on peut le voir à travers l'histoire, l'avenir de la société de demain est toujours autre et neuf. Finalement la société de demain est toujours une utopie d'aujourd'hui.

L'Utopie est parfois opposée à "scientifique". Mais cela dénote de l'ignorance non seulement du grec, mais surtout de la nature même de la science.

La recherche des inexistants possibles, la quête des ailleurs réalisables, l'analyse des utopies réalistes est bien une activité scientifique. Pour savoir ce qui, parmi l'inobservé, est possible ou ne l'est pas, il faut avoir une connaissance, une conscience profonde de la nature et de la structure du monde.

C'est bien une attitude scientifique que nous prenons ici, celle qui consiste à tout mettre en question et en cause, à vouloir expliquer au fond, à chercher les causes réelles, à pratiquer le doute, la critique systématique à l'égard des justifications reçues, et à ne rien accepter comme acquis, évident ou immuable. C'est dans ce mode de rédaction que nous serons conduits à la mise en cause radicale de notre société. La science, c'est d'ouvrir les yeux.

Alors l'étude des sociétés possibles par rapport à celle des sociétés réalisées, présentes ou passées est peu répandue. Certaines disciplines et certains auteurs n'imaginent même pas que celle-ci puisse avoir une dimension professionnelle, ou même, ils en repoussent l'idée avec ferveur. Peut-être, ces intellectuels veulent-ils par là justifier une limitation de leur imagination.

Notre système a beau être terriblement mauvais, nous savons tous intimement qu'il n'est pas le pire possible. Un changement radical pourrait amener à une société encore plus désagréable, et donc avec cette crainte justifiée, la masse de la population est conservatrice ou prudemment réformiste. Il ne peut en devenir autrement que si les gens peuvent se faire une idée assez précise et fiable de ce que pourrait être le nouveau et le meilleur système.

C'est pour cette raison que l'analyse d'une autre société possible doit être suffisamment sérieuse, réfléchie, précise et complète. Et il en va de même pour les étapes de sa mise en application, des étapes de transition de l'existante vers la nouvelle.

Car comme ce que l'on croit possible dépend de l'analyse, les humains doivent être convaincu par ce projet. C'est seulement alors qu'ils le prendront pour fin, faisant ainsi de la marche vers une société meilleure une nécessité de l'évolution de la leur. [^4]

> « Certes c'est dans l'analyse des sociétés réalisées qu'on trouve les connaissances nécessaires pour construire les autres ; mais il faut alors cesser d'accumuler les ingrédients sans jamais faire la cuisine. » Christophe Kolm

Alors dans sa définition actuelle telle que perçue par notre société, l'utopie est un idéal, une conception qui ne prend pas en compte la réalité, et donc irréalisable.

Alors oui dans ce cas nous n'allons effectivement pas créer d'utopie ici.

La réalité ce n'est pas notre imaginaire collectif. La réalité ce sont des seuils que nous ne pouvons pas dépasser, pour ne pas compromettre les conditions favorables à notre survie. C'est la concentration maximale de CO2 dans notre atmosphère, c'est la limite de perturbation des cycles biochimiques du phosphore et de l'azote indispensables au bon état des sols et des eaux, c'est l'érosion maximale de la biodiversité, c'est-à-dire l'extinction des espèces et de leur rôle essentiel au sein des écosystèmes, c'est l'usage intensif des sols, l'utilisation abusive d'eau douce, l'acidification des océans, etc...[^5][^6][^7][^8][^9]

Voici une partie de ce qui compose la réalité, ce qui n'est pas le cas par exemple de la valeur qu'annoncent les petits chiffres sur la page de notre compte en banque.

En fait, l'utopie, nous sommes en plein dedans, à croire que notre société peut continuer sur le long terme à vivre indéfiniment de la sorte.[^11]

Pour concevoir le monde idéal, il faut quitter notre imaginaire, repartir de la base, pour pouvoir régler les problèmes à la source. Car c'est comme ca que nous pourrons réellement construire un autre monde qui a du sens, un autre monde réellement possible.

Nous préférerons donc un autre terme pour nous différencier, ce que nous chercherons à créer ici est une Eutopie, un concept similaire à l'utopie, mais qui est considéré comme réalisable dans la réalité. Le préfix "u" devient "eu", ce qui signifie en grec "bon", et donc littéralement le "bon lieu".[^12]

Ce qui guide des humains libres, c'est leur imagination du futur, du possible, du désirable, c'est leur utopie, l'Eutopie. Et comme l'humain avance bien plus souvent attiré par l'espoir que poussé par le désespoir, le rêve finit par créer le réel. Notre souhait est que le réel devienne l'Eutopie.

> « L'utopie a changé de camp, est utopiste celui qui croit que tout peut continuer comme avant. » Pablo Servigne et Raphaël Stevens[^11]

## Prendre du recul

Nous devons nous interroger sur notre perception du court et du long terme. Faut-il définir 100 ans comme du court terme ou du long terme ? Tout est une question de point de vue, mais il est nécessaire de prendre du recul et de quitter l'échelle de notre courte vie.

La terre s'est formée il y a maintenant 4 milliards d'années, la vie est apparue il y 500 millions d'années, les Hominia, la ligné des êtres humains se sont séparés de la lignée des chimpanzés il y a 7 milliards d'années, mais de cette lignée, c'est il y a seulement 300 000 années que notre espèce est née: Homo Sapiens.[^13]

Dans 5 à 7 milliards d'années, le soleil aura épuisé son hélium et se transformera en géante rouge, multipliant son rayon par 1.000 et happera peu à peu la Terre dans son halo brûlant. La température de la Terre sera alors tellement élevée que les océans s'évaporeront. Mais la vie aura disparu bien avant cela par manque d'oxygène.[^14]

Il reste en réalité encore 2 milliards d'années à l'humain à vivre sur une planète habitable.

Il est important de bien visualiser ces chiffres sur une échelle pour bien en prendre conscience.

2 milliards d'années, c'est visuellement 2 000 000 000 années.

Si l'on considère une génération par tranche de 25 ans, ça nous fait encore 80 000 000 générations. Et nous sommes actuellement que la 12 000e.... Seulement 0,015 % de notre vie sur Terre s'est écoulée.

Cependant, la fin de la Terre ne signifie pas pour autant la fin de l'espèce humaine, l'univers est vaste… Mais cette partie laissera place à d'autres récits imaginaires… Car il serait d'abord temps d'apprendre à survivre durablement sur notre propre planète avant d'espérer pouvoir survivre sur d'autres. Ce qui n'est actuellement pas le cas.

Nous ne sommes donc qu'au tout début de notre histoire, et quand on voit la vitesse à laquelle nous consommons les ressources de notre planète et la relation que nous avons avec et l'ensemble de ses écosystèmes, on ne peut espérer à se rythme tenir très longtemps...

Au rythme de la production mondiale, nous aurons consommé l'ensemble du pétrole dans 51ans, l'ensemble du gaz dans 53ans, l'ensemble du charbon dans 114ans, l'ensemble de l'uranium dans 100ans, et nous ne donnons pas plus longtemps pour l'ensemble des terres rares essentielles aux différentes technologies d'aujourd’hui. [^11]Et nous espérons tenir 2 000 000 000 ?

D’où l'importance de prendre du recul, de voir au-delà de notre courte vie humaine et de penser sur le long terme. Nous y reviendrons plus tard, mais il faut prendre conscience qui si ça nous fait tellement défaut c'est par ce que le cerveau d'Homo Sapiens n'est pas prévu pour voir sur le long terme. [^15]

Vous remarquerez également que ce n'est pas la terre que l'on cherche à sauver ici, car elle a encore de beaux jours devant elle, avec ou sans nous, ce que l'on cherche à sauver ici, c'est bel et bien l'existence humaine, et avec celle-ci les différentes espèces qui composent notre écosystème.

## Quand le rêve devient réel

Peut-être vous demandez-vous en quoi imaginer un autre monde permettrait d'aider à changer l'existant ?

Après tout, on pourrait se dire qu'il vaudrait mieux consacrer notre temps et notre énergie à changer l'existant plutôt que d'en imaginer un nouveau.

Mais ce qui arrive n'est pas indépendant de ce que l'on tente. Ce que l'on tente dépend de ce qu'on croit possible. Et ce qu'on croit possible dépend de l'analyse.

Les principes et les valeurs sur lesquels reposent les fondements du monde que nous vous proposons de découvrir n'ont été que très peu développés et analysés par les intellectuels. Cette analyse vous permettra de croire cette vision possible, et alors de la tenter. Et donc finalement, de changer l'existant. Cette analyse doit être la plus cohérente possible pour remporter l'adhésion du plus grand nombre et déboucher sur une action collective, sensée, et de grande ampleur.

En imaginant un nouveau monde, nous découvrons par comparaison l'absurdité de l'actuel, ce qui nous amène à une profonde remise en question du monde qui nous entoure, mais aussi de nous-mêmes.

En imaginant un nouveau monde, nous créons une matière, un espace de réflexion et de débat pour la recherche de l'idéal commun.

En imaginant un nouveau monde, nous rassemblons des gens autour d'un idéal commun, ce qui leur permet de se rassembler pour se mettre en action et changer l'existant.

En imaginant un nouveau monde, nous pouvons nous en inspirer pour en produire les outils et les principes, les mettre en applications dans le réel, les expérimenter, itérer dessus et les améliorer.

Finalement, en imaginant un autre monde, nous suscitons le rêve et l'espoir, une émotion nécessaire, à la base de toute grande révolution.

Nous vous avons parlé de ce don de l'être humain, cette capacité à imaginer et à nous raconter des histoires, cette faculté qui nous a fait évoluer de manière exponentielle depuis notre révolution cognitive, il y a 90 000 maintenant.

Et bien il est désormais temps d'à nouveau changer d'histoire pour changer l'histoire. Nous devons être portés par un idéal commun, changer de récit, modifier nos croyances afin de nous permettre de nous mobiliser et de collaborer pour changer notre avenir.

> « De tout temps, ce sont les histoires, les récits qui ont porté le plus puissamment les mutations philosophiques, éthiques, politiques… Ce sont donc par les récits que nous pouvons engager une véritable "révolution" » Petit manuel de résistance contemporaine[^24]

Lors de votre lecture, vous ne devez pas vous dire "C'est impossible", vous avez sans doute raison, ça l'est dans les conditions actuelles et on ne propose pas d'appliquer là maintenant tout de suite ce récit. Mais vous devez vous dire "Cela pourrait devenir possible".

Ce qu'il faudra alors faire c'est de se demander "Qu'est-ce qui est possible là maintenant pour tendre vers cette idéal?", et ainsi construire au mieux les étapes de sa transition. Mais pour ca il faut d'abord connaître cette idéal, savoir où l'on va.

Il suffit alors de regarder ce qu'il est possible de faire là maintenant et quelles seront les nouvelles possibilités qui s'ouvriront lors de ces concrétisations pour en réaliser d'autres.

Néanmoins, le but ici n'est pas de vous dicter ce qu'il faut faire ni comment, mais de développer une vision qui vous permettra de le découvrir vous-même et de vous y aider avec les outils adéquats.

Il faut aussi prendre conscience que cette vision n'est pas figée, elle se construit par vous et a pour vocation d'évoluer avec le temps en fonction des avancés et des découvertes.

Alors c'est tout de même possible que notre système soit voué à s'effondrer quoi que l'on y fasse. En effet celui-ci semble bloqué dans son inertie destructrice de son environnement, et hors de contrôle.

Les livres sur les théories de l'effondrement sont nombreux et se basent sur des études très sérieuses, mais si un tel effondrement devait advenir nous ne passerons pas de 7 milliards à 1 humain, il y aura toujours de quoi rebâtir une société. L'effondrement ne se déroulera pas du jour au lendemain, il sera progressif avec des ondulations, et il sera toujours possible de changer de direction au cours de celui-ci. [^16]

Dans un cas comme dans l'autre le travail effectué ici a tout autant d'intérêt: soit grâce à lui on arrive à changer de société à temps pour éviter l'effondrement, soit tout en nous préparant à la résilience pour pouvoir continuer à vivre lors de cet effondrement, nous préparons les bases solides au démarrage d'une nouvelle société.

Car l'effondrement ne laissera pas la place à une société toute neuve et prête à l'emploi, ce seront les individus à ce moment-là qui devront construire les chemins et élaborer les directions à partager ensemble.

Dès lors, notre impréparation pourrait laisser le champ libre à des assemblées verbeuses et impuissantes. Dans le même ordre d'idées, des systèmes d'autogestion ne peuvent s'installer sans essais préalables, car ceux-ci amèneront très certainement à de nombreux échecs. L'échec de la mise en place de prise de décision collaborative et démocratique à tous les nouveaux de la société pourrait en raison de la situation d'urgence laisser la place à d'autres systèmes de domination qui ne sont pas souhaitables. C'est pour cette raison que ces nouvelles voies sont à explorer dès aujourd'hui. [^4]

Malgré tout, nous resterons convaincus ici par le fait que nous pouvons éviter l'effondrement. Celui-ci engendrerait un désastre humanitaire et écologique qui sera d'autant plus difficile à réparer, c'est pourquoi nous ne pouvons pas le qualifier de souhaitable, même s'il laisserait la place à du neuf. Nous ferons alors tout pour chercher et développer les solutions permettant de l'éviter. N'oublions jamais que même si le système est puissant, notre imagination collective a tout autant de puissance que lui, car c'est sur cette même imagination que celui-ci existe et résiste.

Pour finir, la construction de cette vision, même si elle venait à ne pas être acceptée et à être voulue, permet de dévoiler comment le monde pourrait être. Elle peut faire prendre conscience aux gens qu'ils ne font que jouer dans un jeu géant avec des règles qui engendrent des incitations, et que leur vie, leurs possibilités, leurs privilèges seraient bien différents si l'on en changeait les règles.

> « Si tu veux construire un bateau, ne rassemble pas tes hommes et femmes pour leur donner des ordres, pour expliquer chaque détail, pour leur dire où trouver chaque chose. Si tu veux construire un bateau, fais naître dans le cœur de tes hommes et femmes le désir de la mer. » Antoine de Saint Exupéry

<div style="width:full;display:flex;justify-content:center">
<img style="max-width:800px;" src="/0-GlobalProject/Image/presentation/3.webp" />
</div>
<center><a style="font-weight:500;color:#13af36;"href="https://www.instagram.com/another.possible.world/" target="_blank">See more concept art from Eutopia</a></center>

## La lutte des récits de la transition

Nous avons vu plus haut que c'est l'imaginaire qui crée le réel, et de tout temps c'est bien le récit et les histoires qui on fait évolué nos sociétés. Notre époque n'y échappe pas, nous avons été il y a encore récemment porté par le rêve américain et le rêve du progrès. Et nous sommes encore aujourd'hui pour la majorité d'entre nous toujours dans ces imaginaires, les gens de pouvoir ne conçoivent pas l’effondrement de nos sociétés, ils croient et sont porté par un récit hyper moderne.

Ils imaginent un futur où nous vivrions dans des villes artificialisées bourrées de technologies, qui permettront notamment de contrer les ravages du changement climatique. Notre nourriture serait de plus en plus industrielle, artificielle et synthétique. Nous serions plongés dans une consommation de l'instantané soutenue par une économie de l'information ou plus tôt de la surveillance. Nous serons locataires de tous, nous ne poserions plus rien, mais nous serions "heureux".

Mais le récit hyper moderne et la construction du récit de la transition sont en panne. Cette modernité fondée sur l'idéale d'un progrès matériel infini, corrélé à un progrès social pour tous, a sombré dans une crise multidimensionnelle et est devenue complètement dissonant. Le récit de l’hypermodernité a instrumentalisé l’idéal de progrès tout en l’ayant vidé de son essence sociale émancipatrice. Il est manipulateur et soutenu médiatiquement, notamment par l’émergence d’une novlangue. Les mots sont détournés de leur sens initial, vidé de leur substance, orientés en fonction d’intérêts masqués. L'hypermodernité, c'est l'hyperaccumulation, l'hyperconsommation née du désir rendu insatiable par une publicité envahissante. L'homme moderne, qui a tout, est paradoxalement le plus insatisfait des humains. [^17]

Le récit hyper modernité est en train de subir trois grands échecs que tout le monde commence à percevoir clairement. Le premier est son échec écologique et la promesse d’un capitalisme vert, c’est-à-dire un productivisme respectueux de la vie, paraît de plus en plus peu crédible. Le deuxième échec est de nature économique, l'accaparement et l'inégalité à toutes les échelles, du local au mondial, 'accroissent de façon exponentielle, malgré une abondance matérielle exceptionnelle. Le troisième échec est éthique, nos rapports sociaux se transforment en relation marchande, dont chacun espère sortir gagnant, tout s'achète et tout se vend. Ces déséquilibres sont générateurs de violence et ne sont plus tenables.

Alors est en train d'émerger un contre récit de la transition comme alternatives aux récits hyper modernes, un récit qui doit arriver à convaincre et être aussi puissant que ces précédents pour arriver à changer les représentations mentales des gens, pour les mettre en mouvement, et amener notre société vers la nouvelle ère, celle de la noosphère. Actuellement l'imaginaire derrière le mot écologie ne donne pas envie, il donne le sentiment que nous allons devoir nous restreindre, faire des concessions sur notre confort et notre épanouissement, il en est de même derrière le mot décroissance. L'imaginaire du communisme avec les faits historiques c'est lui aussi complètement effondré, la majorité des gens n'en veulent pas et il sert d'argument au système pour conserver le statu quo. Ce nouveau récit est donc encore à construire. Tel est l'objectif que se donne le projet Eutopia.

<!-- Il faut alors comprendre de quoi rêvent véritablement les gens au fond ? Ces rêves ne doivent pas être négligés, l’imagination est puissante, et sera la première marche de toute concrétisation. Rêvons-nous par exemple de nous sentir comme en vacance tout le temps et en tout lieu ? Quel est donc notre idéal collectif ? Il est urgent de répondre à cette question afin que de donner sens à ce mouvement, de concevoir et de mettre en œuvre dès à présent de nouvelles politiques. -->

> Entre nous et l’effondrement, il faudra, pour concevoir des lendemains qui chantent, que résistent encore les remparts de fraternité et de l’imaginaire créatif. Citation modifiée de Valérie Jousseaume[^17]

## L'intelligence collective

Personne ne détient individuellement la vérité, mais ensemble il est au moins possible de s'en rapprocher.

Il ne vous est certainement pas demandé de croire ce que vous pourrez lire ici, vous êtes au contraire encouragé à ne rien croire avec certitude, à ne jamais penser détenir la vérité, à toujours garder une part de doute en vous.

Car croire détenir la vérité c'est s'empêcher de la chercher, et donc s'empêcher de la trouver.

C'est pour cette raison que ce monde, il ne peut s'imaginer seul, nous avons besoin du plus grand nombre. Pour sa réalisation, nous avons besoin d'utiliser l'intelligence collective et d'en développer ses outils.

Alors, si vous lisez cette ligne, nous éditions actuellement ce texte avec un outil qui s'appelle Git, il permet de versionner l'état d'avancement de ce récit. Et il vous permet également de rejoindre d'éditer des parties de ce récit, les soumettre, pour qu'ensuite l'ensemble des collaborateurs puissent, émettre un avis, débattre, et puis finalement voter afin d'intégrer la modification ou non. Une fois la modification validée, celle-ci s'affiche directement en ligne ici même où vous êtes en train de lire.

Cette outil est temporaire, l'idée est de concevoir une plateforme tout entière dédiée à l'édition collaborative et démocratique de ce récit. Un outil qui sera assez modulaire que pour pouvoir être utilisé dans de nombreux autres projets.

Cette application est appelée Ekklésiapp, un mix entre application et Ekklesia du grec ancien "assemblée", venant tout droit des assemblées citoyennes des cités antiques grecques dont la cité d'Athènes, berceau de la démocratie. Bien qu'à cette époque, la démocratie n'était accessible qu'à la classe dominante, nous verrons dans le chapitre 5 « Comment décider ensemble ? » à quel points celle-ci est toujours imparfaite et reste encore à être réaliser. [^18]

Ekklésiapp est une application de prise de décision, pouvant être utilisée à l'échelle de quelques personnes jusqu'à l'échelle de la population tout entière, elle est assez modulaire pour pouvoir créer une infinité de méthodes de prise de décision différentes, pour l'adapter facilement au travail à effectuer. Une méthode choisie peut facilement être modifiée afin de continuellement être à la recherche d'une méthode offrant les meilleurs résultats.

Celle-ci est libre d'accès. Elle peut permettre à ce que d'autres puissent également créer leur utopie, dystopie, Eutopie. Et cela pour ainsi développer la recherche dans ce domaine, ce qui est nécessaire et utile pour les nombreuses raisons cité plus haut.

Elle permet aussi à d'autres personnes de réaliser d'autres types de travaux de rédaction collaborative. Elle peut être utilisée pour la gestion d'une organisation, d'une entreprise, ou de tout autre projet, et offre enfin un réel outil démocratique, permettant de redonner le pouvoir à la base, assez efficace pour pouvoir se passer des systèmes de dominations et de hiérarchies.

Mais comment se mettre d'accord sur les règles d'édition de ce récit ?

Ekklésia est directement inspirée du processus de décision démocratique qui sera mis en place dans ce monde idéal. Enfin, en réalité, le processus de décision démocratique de ce monde idéal sera inspiré de cette plateforme. Car cette plateforme va directement nous permettre de pouvoir tester nos idées sur le bon processus de décision collectif et démocratique, d'itérer dessus jusqu'à trouver celui qui mettra tout le monde d'accord sur le fait qu'il amène effectivement au meilleur des résultats. Ainsi on donne non seulement son avis pour une décision, mais on donne également son avis pour le processus de décision. Le processus de décision actuellement conçu vous sera dévoilé plus loin.

> « Seul on va plus vite, ensemble on va plus loin » Proverbe africain

## Le mode de rédaction

Pour ce qui est des compétence nécessaires pour juger ce récit ou pour y participer, il n'y en a pas.

Si vous trouvez le monde compliqué, sachez qu'il l'est avant tout par ce que nous l'avons compliqué, mais en réalité, lorsque l'on repart d'une bonne base, tout se révèle plus simple et logique qu'il n'y parait.

Tout ce qui nous semble compliqué n'est en réalité qu'une suite d'éléments simples, certaines choses demandent juste plus de temps que d'autres pour être passées en revue et assimilée.

Les grands savants sont finalement ceux qui ont su entrevoir cette simplicité du monde.

Ne doutez pas de vous, si vous ne comprenez pas quelque chose, c'est simplement qu'on n’a pas pris le temps de vous l'expliquer simplement en repartant de la base que vous connaissez.

En effet, il est essentiel de toujours repartir de la base. C'est l'un des principes fondamentaux de ce récit, et c'est pour cela que nous prendrons le temps de créer une base solide dans la partie 2, avant de vous décrire en détail Eutopia dans la partie 3.

Cet ouvrage a pour but de pouvoir être lu et compris de tous. Alors peut-être que les intellectuels trouveront ce livre écrit d'une manière trop simpliste, avec un vocabulaire pas assez riche, mais tel est le but recherché.

Car nous devons tous être acteurs du changement, comment pourrions-nous nous rassembler si ce qui devrait nous unir est imbuvable et incompréhensible à lire pour quelqu'un qui ne possède pas la connaissance approfondie de tel ou de tel domaine.

Ici nous ne chercherons pas à utiliser des termes compliqués ou à réaliser des tournures de phrase longues et complexes, nous ne cherchons pas non plus à l'aide de ces méthodes à paraître plus scientifiques, plus intelligents, et donc de créer potentiellement un faux sentiment de vérité en vous de cette manière.

Ce récit ce doit d'être accessibles, compréhensibles et doit aller à l'essentiel. Il se veut honnête envers vous, et dénoué d'utilisation d'effet psychologique pour vous laisser pleinement la possibilité de croire ou non son contenu.

Dans l'histoire de l'humanité, chaque production d'un auteur n'est que la production indirect d'une intelligence collective.

Ce récit découle naturellement de cette recherche constante de la « vérité » effectuée tout au long de l'humanité par ses innombrables humains qui ont contribué à faire évoluer les pensées globales. Ces humains sont à chaque fois née à une époque, à un instant, où ils ont reçu à chaque fois un savoir collectif du passé et ont bénéficié d'interactions qui les ont amené à l'aide des compétences, status et temps dont ils ont bénéficié, à rassembler et à publier de nouvelles pensées et savoirs. Les références direct à ces individus dans notre récit seront annoté, mais nous éviterons de décrire en long et en large le processus du quel découle ses pensées et découvertes, ce afin d'alléger ce récit, de le rendre accessible mais aussi pour éviter de trop individualisé ces découvertes, et ce afin de promouvoir un nouveau paradigme : entendre que toute création de savoir et de pensées dans l'histoire de l'humanité n'a été, même indirectement, que collective. Cette intelligence collective, nous souhaitons maintenant la pousser encore plus loin à l'aide d'outils pour faciliter ces mécanismes, tel que celui utiliser dans ce récit. A travers ce récit vous remarquez donc également un certain language qui tente de sortir de cette individualisme.

À nos yeux, Eutopia est donc le fruit d'une intelligence collective produite sur des milliers d'années et non pas le fruit d'individu isolé au cours de ces époques, la nôtre comprise. Une image mentale est celle d'imaginer l'humanité comme un seul et même organisme qui évolue dans le temps, remplie de millions, milliards de cellules, qui vivent, meurent et renaissent, qui évolue et qui se réadapte constamment pour retrouver un équilibre dans son environnement.

Les références aux auteurs originelles seront néanmoins toujours annotées en bas de page pour en savoir plus sur ces individus et retrouver leurs travaux. Mais aussi pour rendre disponibles et permettre la vérification de nos sources quant aux informations que nous avançons.

Une deuxième version plus riche et plus scientifique de ce récit sera également développée et sera directement liée à celle-ci afin d'appuyer l'argumentation de son contenu auprès des institutions académiques. Cette version sera facilement accessible d'un simple clic dès que vous souhaiterez en savoir plus sur l'histoire, les réflexions et l'argumentation de telle ou telle sujet. Vous aurez la possibilité, aux aléas de vos clics, de rentrer toujours plus en profondeur sur tel ou tel sujet.

De nombreuses études seront citées tout au long de votre lecture, cependant de manière générale nous vous encourageons à être prudent sur les conclusions des études que vous lisez, car il est aisé d'influencer la science. Certaines peuvent être financées par des entreprises ou des groupes d'intérêts qui ont des motivations et des intérêts spécifiques et vont influencer l'étude.

Les études peuvent être sujettes à des erreurs de conception, telles que des biais de sélection, des erreurs de mesure ou des erreurs statistiques, qui peuvent conduire à des résultats non représentatifs ou incorrects. L'interprétation des résultats peut être subjective et dépendre des croyances et des préjugés de l'auteur de l'étude. Il est important de considérer les résultats dans leur contexte et d'examiner les preuves dans leur ensemble. De manière générale, les chiffres peuvent être présentés de différentes manières pour soutenir un argument spécifique, et donc conduire à des conclusions erronées. [^19][^20][^21]

L'idée n'est pas de fuir la science, mais d'au contraire, de bien s'appuyer dessus, mais avec un regard vigilant, en reconnaissant que le savoir produit est en mouvement et évolue, que de nouvelles études contredisse de précédente, et que dans l'état actuel celle-ci est largement influencé par le système économique qui financent la recherche. [^22]

De plus, sans rentrer dans les détails ici, la métaphysique remet en question les présuppositions de base sur ce qui existe et comment les entités peuvent être catégorisées et comprises. Elle explore notamment des concepts comme l'existence du libre arbitre, la nature de l'esprit par rapport au corps, et les fondements de la réalité elle-même. Bien qu'elle ne remette pas en cause les études scientifiques de manière directe, la réflexion métaphysique questionne les cadres conceptuels et les hypothèses sous-jacentes à la recherche scientifique. Cela peut inclure des réflexions sur ce que signifie « répliquer » une étude, comment nous comprenons la causalité, et ce que cela signifie pour quelque chose d'être « réel » ou « vrai » dans un contexte scientifique, comme le fait que l'acte d'observation n'est pas neutre mais affecte l'état de l'objet observé. En résumé la métaphysique interroge les fondations philosophiques de ce que nous prenons pour acquis dans notre quête de connaissance, y compris dans les sciences.[^23]

C'est pourquoi cette œuvre n'est pas, et ne sera jamais figée, elle a pour vocation d'être réalisée par l'humanité tout entière et d'être mise à jour tout au long de son évolution, par et pour elle-même. Elle sera donc toujours ouverte aux nouvelles preuves qui peuvent en contredire son contenue, y compris ces plus profondes fondations.

> Le doute est le commencement de la sagesse. Aristote[^25]

## Conclusion de l'introduction

Vous l'aurez compris, vous vous apprêtez à lire ici l'Eutopie, récit désormais plus que nécessaire à l'humanité. Il est temps pour nous de définitivement nous éloigner de notre état primitif, de permettre l'évolution globale de notre société vers la véritable civilisation afin d'éviter le désastre écologique et humain qui se profile. Ce récit a pour objectif d'être réalisé par l'humanité tout entière, par elle-même et pour elle-même, d'enfin lui redonner du sens et une direction, afin de lui assurer un avenir garanti de liberté et de bonheur.

> « Les tensions et contradictions de l'âme humaine ne s'effaceront que lorsque s'effaceront les tensions entre les hommes, les contradictions structurelles du réseau humain. Ce ne sera plus alors l'exception, mais la règle que l'individu trouve cet équilibre physique optimal qu'entendent désigner les mots sublimes de "Bonheur" et de "Liberté" : à savoir l'équilibre durable ou même l'accord parfait entre ses tâches sociales, l'ensemble des exigences de son existence sociale d'une part et ses penchants et besoins personnels de l'autre. C'est seulement lorsque la structure des interrelations humaines s'inspirera de ce principe, que la coopération entre les hommes, base de l'existence même de tout individu, se fera de telle manière que tous ceux qui, la main dans la main, s'attelleront à la chaîne complexe des tâches communes aient au moins la possibilité de trouver cet équilibre, c'est alors seulement que les hommes pourront affirmer avec un peu plus de raison qu'ils sont "civilisés". Jusque-là, ils sont dans la meilleure des hypothèses engagés dans le processus de la civilisation. Jusque-là, force leur sera de répéter encore souvent : "La civilisation n'est pas encore achevée. Elle est en train de se faire ! » Nober Elias, la dynamique de l'occident."

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="935519bb-1353-58e7-6b3e-a4c10eb3b495" ></div>
</div>

## Sources

[^1]: Yuval Noah Harari, Sapiens : Une brève histoire de l'humanité. 2011
[^2]: Sébastien Bohler, Où est le sens ?/ 2020
[^3]: [Étymologie du mot utopie](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, La bonne économie : La réciprocité générale. 1984
[^5]: [Concentration maximale de CO2 dans l'atmosphère](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Limite de perturbation des cycles biochimiques du phosphore et de l'azote](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Érosion maximale de la biodiversité](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Usage intensif des sols](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Utilisation abusive d'eau douce](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Acidification des océans](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Comment tout peut s'effondrer, Pablo Servigne & Raphael Stevens
[^12]: [Étymologie du mot eutopie](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, The Earth's History
[^14]: The Future of Life on Earth" par Peter D. Ward et Donald Brownlee
[^15]: Sébastien Bohler, Le bug humain. 2019
[^16]: Jared Diamond, Effondrement. 2004
[^17]: Valérie Jousseaume, Plouc Pride : Un nouveau récit pour les campagnes. 2021
[^18]: Serge Christhonne, Les élections sont-elles la démocratie ?. 1977
[^19]: Ben Goldacre, Bad Science. 2008
[^20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [The Replication Crisis](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documentaire : La fabrique de l'ignorance. 2020
[^23]: Jaegwon Kim, Fondements de la métaphysique
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[^25]: Aristote, Éthique à Eudème
