---
title: Une lutte heureuse
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Cette page est encore au stade expérimental et est actuellement en cours de construction.
> {.is-danger}

L'idée est ici de donner des pistes et solutions pour mener une lutte heureuse, car l'on ne peut lutter si l'on est malheureux. Cette lutte contre le système nous amène à tout un tas d’échecs qui peuvent nous donner toutes les raisons de l'être, pourtant il est possible de voir les choses autrement.

Lorsque l'on souhaite changer le monde et être en meilleur accord avec ses valeurs, il peut être dure de lutter sans en voir les résultats, dure de se replonger constamment dans l'état du monde, d'avoir au quotidien la conscience de la souffrance et l'injustice qui remplient ce monde, effrayant de voir tous les indicateurs au rouge et la direction que prend l'humanité. Dure de vivre paisiblement et sereinement tout en continuant de profiter des plaisirs de la vie. C'est d'ailleurs pour ca que nous sommes tentés d'abandonner et d'en faire abstraction pour être tranquilles.

Mais il est tout aussi possible de vivre le paradis sur Terre en ayant conscience de se trouver en enfer.

Car la vie est belle, quels que soient l'endroit sur terre et les épreuves que nous traversons, nous pouvons en apprécier le goût par un simple regard, par une simple respiration, par un simple sourire.

Ca fait des millénaires que la forêt est en feux et que nous vivons dans un monde rempli d'injustice et de souffrance.

Et il n'est pas prouvé que nous sommes plus heureux au fils de notre évolution et de nos luttes dans l'histoire.

Le bonheur est en réalité plus constant, et notre psychologie s'adapte.

Alors, profitez, soyez heureux, faites votre part, soyez présent pour exercer votre pouvoir, votre responsabilité et votre libre arbitre. Le monde est en feux, mais ne le laissez pas vous empêcher d'être heureux, de profiter des êtres que vous aimez.

Nous identifions petit à petit un « nouveau monde », en le distinguant d’un « vieux monde » connu. Cette dualité doit être le fondement de toute émergence de la conscience, et structure la pensée humaine. Et on doit prendre conscience collectivement qu’on ne se pose pas contre l’altérité, mais qu’on se construit par l’existence d’une altérité. Et même, peut-on oser dire, on ne peut avoir conscience d’exister que grâce à l’existence de cette altérité, grâce à cette relation. Tel est l'existence, elle n’est que la relation, il n’y a rien d’autre.

Pour que puisse advenir du nouveau, il nous faut donc apprendre à « faire avec » : avec l’altérité, avec l’Autre, les autres, avec les autres cultures, avec le passé, avec le monde. Un « nouveau monde » a besoin, pour se construire, de se définir par rapport à un « ancien monde », mais il convient de garder à l’esprit de ne jamais s’identifier à une lutte contre cet ancien. Là se trouve le changement de posture relationnelle. Ce que nous appelons dans le langage commun, « un plus haut niveau de conscience » est en réalité un plus haut niveau d’amour, c’est-à-dire d’intégration, d’acceptation et de respect de l’altérité.

Suite en cours de rédaction... (Voir la partie Religion et Art de vivre du récit pour plus de pistes)

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="4c113405-a046-760a-9865-9b70fd71c3d6" ></div>
