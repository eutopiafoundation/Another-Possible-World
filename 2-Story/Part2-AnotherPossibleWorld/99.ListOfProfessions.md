---
title: Liste des professions
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Cette page est encore au stade expérimentale et est actuellement en cours de construction.
> {.is-danger}

## Liste non exhaustives de métier supprimés :

- Métier liée au banque ( En france : 370 000 employée soit 1,7% de l'emploi salarié priv[^1])
- Métier liée à l'assurance ( En france : 230 000[^1] )
- Courtier
- Trader
- Gestionnaire de fonds
- Contrôleur de train ( En france : 10 000 en incluant les commerciaux[^4])
- Métier liée à l'armée ( En france : 270 746 millitaires incluant le personnel civil[^5])
- Fabricants d'armes et d'équipements militaires (En france: 5 000 entreprises et 400 000 emplois dans le secteur de la défense dont 165 000 emplois directs dans l’armement)
- Conducteur de bus ()
- Conducteur de camion (Au Etats Unis : 1 495 000 )
- Taxi / Uber (En france : 26 000)
- Métier liée aux énergie fossile : Opérateurs de raffineries de pétrole (80% de la production d'énergie mondiale)
- Métier liée aux industries de l'extraction minière non durable
- ...

## Liste non exhaustives de métier transformer

- Métier du management -> Orienté pour créer des processus de management horizontal et favoriser l'intelligence collective au sein des organisations.
- Métier du marketing -> Marketing axé sur l'éthique, la durabilité, et l'utilité réelle des produits pour les gens.
- Métier liée à l'économie -> Orienté vers la mise en place et l'optimisation des flux qui favorise le BIB : Le bonheur intérieur brut et l'IHE : Impact humain environnemental.
- Métier liée à la publicité -> Comme pour les métiers du marketing et orienté vers la promotion des produits dans des espaces réservé choisi collectivement, qui ne cherche pas à créer de nouveau besoin.
- Métier liée à l'administration -> Rôles administratifs axés sur la facilitation des processus collaboratifs par le transfers et la gestion de l'information.
- Métier liée au Droit en entreprise -> Axée sur l'éthique des projets, la responsabilité sociale, le droit environnemental et la constitution d'Eutopia.
- Métier liée à l'immobilier -> Se référer au chapitre sur le logement pour imaginer toutes les nouvelles facettes possible de se métier.
- Métier liée au vêtements -> Orienter vers la création de vêtements de qualité qui peuvent durer plusieurs générations.
- Métiers de l'énergie -> Orientés vers le développement, l'entretien et l'optimisation des énergies renouvelables.
- Métiers de la construction -> Axés sur des techniques de construction durable et la rénovation éco-énergétique des bâtiments existants.
- ...

## Liste non exhaustives de métier considérablement augmenter :

- Médecin / Naturopathe
- Psychologue
- Agriculteur / Conseillers en permaculture
- Enseignants
- Scientifique et chercheurs
- Artisans et créateurs locaux
- Professionnels de la réparation et la maintenance
- Spécialistes en réhabilitation écologique
- Artistes visuels : Peintres, sculpteurs, et artistes numériques...
- Musiciens et compositeur
- Écrivains et poètes
- Danseurs et chorégraphes
- Acteurs et metteurs en scène
- Photographes
- Designers (graphiques, industriels, de mode)
- Créateurs de contenu numérique : Blogueurs, vidéastes, et podcasteurs qui...
- Développeurs de jeux vidéo et concepteurs de jeux
- Animateurs culturels et médiateurs artistiques
- Curateurs et gestionnaires de galeries d'art
- ...

## Liste non exhaustives de nouveau métier créer :

- Gardien de la paix
- Facilitateurs de communauté
- Conseillers en réciprocité économique
- ...

## Notes relatives à la transition

En cours de rédaction...

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="c34fd2a0-1759-f5a0-46e0-9d07e3c1bf8f" ></div>

## Sources

[^1]: [ Les 10 chiffres clés du secteur de la banque et de l'assurance ](https://www.hellowork.com/fr-fr/medias/chiffres-banque-assurance.html)
[^2]: []()
[^3]:
[^4]: [Commercial à bord des trains](https://www.emploi.sncf.com/nos-metiers/commercial-a-bord-des-trains/)
[^5]: [Forces armées françaises](https://fr.wikipedia.org/wiki/Forces_arm%C3%A9es_fran%C3%A7aises)
[^6]: [](https://www.diplomatie.gouv.fr/fr/politique-etrangere-de-la-france/diplomatie-economique-et-commerce-exterieur/soutenir-les-entreprises-francaises-a-l-etranger/les-secteurs-economiques-de-pointe-un-atout-pour-la-france-soutien-aux-secteurs/article/industries-et-technologies-de-defense)
[^7]: [Shortage of drivers in the EU](https://truckmobility-info.com/shortage-of-drivers-in-the-eu/)
[^8]: [La constante évolution des VTC et des taxis ](https://www.journaldunet.com/mobilites/1494709-la-constante-evolution-des-vtc-et-des-taxis/)
