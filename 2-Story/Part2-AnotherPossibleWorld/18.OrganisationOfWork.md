---
title: Organisation du travail
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Contenu au stade expérimental et en cours de rédaction.
> {.is-danger}

<div id="image-container" style="width:full;display:flex;justify-content:space-evenly;overflow:visible;">
    <img style="max-width:32%; box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.5);" src="/0-GlobalProject/Image/work_organization/democratic_collaborative_process.png" />
    <img style="max-width:32%;box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.5);" src="/0-GlobalProject/Image/work_organization/collective_intelligence.png" />
    <img style="max-width:32%;box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.5);" src="/0-GlobalProject/Image/work_organization/social_sustainable_ethical_goals.png" />
</div>
<center><a style="font-weight:500;color:#13af36;" href="https://www.instagram.com/another.possible.world/" target="_blank">See more concept art from Eutopia</a></center>

## Thèmes à explorer et à rédiger :

- Hiérarchie horizontal
- Intelligence collective
- Rotation et partages des tâches équitable et démocratique
- Formation continue
- Coopératives et structures décentralisées
- Bien-être au travail
- Application de gestion collective, démocratique et équitable

## Qu'est-ce qu'une organisation de travail ?

Une organisation de travail a pour but de produire des biens et/ou des services nécessaires à l'épanouissement de la population.

Une organisation de travail peut être une école, un magasin, une entreprise de télécommunication, etc...

Si elles ne sont pas individuelles, toutes les organisations de travail fonctionnent horizontalement sur base de l'intelligence collective.

Elles ont toutes un statut coopératif et appartienne aux personnes qui y travaillent.

De nombreux outils informatiques seront développés en fonction des besoins de chaque organisation de travail pour permettre un travail collaboratif, efficace et qui permette des prises de décisions démocratiques efficace et sans hiérarchie de pouvoir.

Il existe actuellement un grand fossé qui sépare profondément ceux qui savent et ceux qui ne savent pas, ce fossé devra être comblé, non pas en niant la nécessité de la compétence, mais en reconnaissant que tous les degrés de compétences peuvent coexister et donc développer l'intelligence collective.

Les organisations de travail sont directement fondées en fonction des besoins que les citoyens émettent.

L'économie de marché offre un principe de libre entreprise et de concurrence, si un entrepreneur innovant estime qu'il y a une demande potentielle pour un produit nouveau, bien ou service qui en remplacera un autre, ou pour un produit moins couteux, il peut se risquer à le fabriquer à la mettre sur le marché. On ne peut se passer de ce principe efficace de libre entreprise et de concurrence et cette capacité à révéler de la demande.

Tout citoyen est libre de fonder sa propose organisation de travail en dehors des besoins de la société sur son temps libre en dehors du travail collectif nécessaire, mais peut par la suite être valorisé par la citoyenne comme utile et compter comme heure utile à la société.

Il est également possible comme pour les systèmes de crowfounding actuels de recevoir l'approbation des citoyens pour l'idée et le développement d'un projet et être dispensé directement d'heure de travail collectif nécessaire au bon fonctionnement de la société pour pouvoir s'y consacrer pleinement.

## Partage du travail

Nous allons commencer par l'Oasis. On peut définir à l'avance ce qui est nécessaire au bon fonctionnement d'un Oasis pour permettre son autosuffisance en fonction d'une population donnée.

Chacun est libre de faire ce qu'il veut au sein de son Oasis pour son bon fonctionnement, vous devez imaginer une application qui vous permet de vous connecter à l'organisation de celui-ci.

Vous pouvez choisir le travail que vous souhaitez, s’il y a plus de personnes que nécessaire pour un travail donné, alors celui-ci peut être partagé et il est alors partagé ; par exemple : s’il faut 5h par aller ramasser les pommes d'un arbre fruitier, le temps est divisé par le nombre de volontaires, ou si quelqu'un souhaite enseigner alors le nombre d'élèves est divisé pour permettre un enseignement plus favorable en petit groupe.

Soit ci celui-ci est un travail qui ne peut pas être partagé, par exemple un prof qui a tout de même besoin de suivre une classe avec un minimum d'élève tout au long de l'année et que la division est déjà effectuée à son nombre maximal, alors vous êtes placés sur une file d'attente, il reste cependant à définir une fois la place libérée qui pourra remplacer, si l'on choisit un système de fils d'attente ou si l'on fonctionne par tirage au sort.

S’il n'y a pas assez de personnes pour un travail donné, celui-ci est alors partagé entre les différents membres de la communauté avec toute même une possibilité de noter vos préférences. Par exemple a priori tout membre d'un Oasis sera voué à consacrer un jour de l'année au ramassage des poubelles ou pour tondre les abords des voiries.

Pour un Oasis de 1000 habitants par exemple l'estimation est que chaque habitant devrait consacrer 5h par semaine à son bon fonctionnement, ce qui correspond 89 personnes travaillant 8h par jour.

Mais des habitants peuvent cependant décider de consacrer plus de 8h par jour à l'oasis, car il peuvent être heureux de donner "plus" qu'ils reçoivent, par altruisme ou intérêt propre du travail. Ce qui allège la charge de travail globale.

Car le travail relatif à l'Oasis n'est pas le seul, il y a aussi le travail relatif aux Edens, aux Eldorados et aussi au Nirvana, et donc des personnes sont amenées à apporter leur contribution à d'autres échelles de la communauté.

L'idée est que chacun fasse ce dont il a envie pour contribuer à la société, et que le travail restant que personne ne souhaite faire soit partagé entre la population. Celui-ci est partagé, mais il n'y a cependant aucune obligation d'ordre autoritaire à sa réalisation, c'est là un des principes fondamentaux et obligatoires afin de respecter la valeur de liberté.

Mais alors comment sont définis les créneaux de travail disponible ? Ils sont définis par les organisations elles-mêmes, les organisations fonctionnent toutes horizontalement et cela est enfin rendu possible à l'aide des outils informatiques spécialisés développés. Nous développerons plus en profondeur cet aspect dans le point suivant.

## Mise en situation

Vous souhaitez enseigner ? Placez-vous dans le créneau à l'année pour être enseignant dans l'école de votre Oasis. En fonction du nombre de profs, les classes pourront être séparées en plus petites classes, ce qui est plus favorable à l'enseignement. S’il n'y a pas assez de profs alors une demande sera faite à d'autres Oasis pour avoir des renforts, s’il n'y a pas de renforts disponibles, le créneau sera marqué comme prioritaire, et des membres de l'oasis qui le souhaite pourront réaliser la formation.

Personne ne sait inscrites dans le créneau pour le ramassage des poubelles. Chaque membre de l'oasis se verra assigner un shift durant l'année pour se partager cette tâche.

> « Lorsque l'économie est symbiotique dans sa production et sa consommation mais pas dans sa gouvernance, elle peut être tout à fait verte, mais pas forcément rose. » Isabelle Delannoy[^1]

## Notes relatives à la transition

> La transformation de nos organisations de travail est un élément clé de la transition. Nous devons développer des outils pour faire face au problèmes de prise de décision en grand nombre. Cette première étape permettra d'intérioriser chez chacun la possibilité de pouvoir donner son avis et de constater les facilités de l'utilisation de tel outils. Dès l'école, l'introduction de ces méthodes encouragera les individus à les adopter dans leur vie professionnelle. En valorisant les contributions de chacun, nous améliorons non seulement le bien-être au travail mais aussi les attentes des employés vis-à-vis de leurs organisations. En conséquence, les organisations devront évoluer pour répondre à ces nouvelles exigences et retenir leurs employées. Couplé à une réduction du temps de travail, il est fort probable que les gens voudront passer à l'échelle supérieure et devenir acteur à tous les échelons de notre société, cette motivation ouvrira l'opportunité pour la réalisation d'une véritable démocratie.
> {.is-warning}

En cours de rédaction...

## Propositions directes sur cette page

<div style="margin-top: 20px; " data-canny id="cannyBoardToken" data-token="a0eb794e-0fc5-869f-afd6-c23a63086ad3" ></div>

## Sources

[^1]: Isabelle Delannoy. L'économie symbiotique : Rénégéner la planète, l'économie et la société.
