---
title: Arbeitstempo
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Inhalt im Versuchsstadium und noch in Bearbeitung.
> {.is-danger}

Arbeit, vom Wort travalhum, kommt von tripalium, einem dreibeinigen Folterinstrument. Aber Arbeit ist nicht dazu verdammt, die Folter ihres etymologischen Ursprungs zu sein.

Für viele von uns erscheint es unvermeidlich, dass sie in ihrer Natur und Dauer repetitiv, langweilig und lebensraubend ist.

Das Leben erscheint uns leer und wir sind enttäuscht, wenn wir von einer Arbeit aufgefressen werden, die ihm keinen Sinn verleiht.

Doch die Gesellschaft ermöglicht es uns dennoch nicht, unsere wahren Wünsche zu erfüllen. Denn die Unzufriedenheit, die uns dazu bringen könnte, dieses Leben in Frage zu stellen und ein anderes zu fordern, wird vom System kanalisiert, das unsere intrinsischen Bedürfnisse und die von ihnen erzeugten extrinsischen Bedürfnisse befriedigt und uns gleichzeitig daran hindert, das zu verwirklichen, was uns fehlt.

Wo Arbeit es uns ermöglichen sollte, etwas zu schaffen, das unsere Handschrift trägt, uns selbst auszudrücken und unserem Leben einen kollektiven Sinn zu verleihen.

Alle Dimensionen der Schwere der Arbeit können und müssen ausgelöscht oder verringert werden. Physisch oder psychisch erschöpfende, verdummende oder verblödende, zu harte, mechanische, monotone, zu schmutzige oder unangenehme, zu bedeutungslose, verachtende usw. Arbeiten. Müssen abgeschafft, ersetzt, eingeschränkt, automatisiert, verbessert, bereichert, entschädigt, belohnt, geschätzt oder besser geteilt werden.

Die eigentliche Idee besteht darin, das Wesen der Arbeit so umzugestalten, dass sie zu einer Tätigkeit wird, die so etwas wie Selbstverwirklichung ist, die Manifestation des eigenen tiefsten Menschseins in Harmonie mit der Natur. In diesem Sinne würde die Arbeit zum "ersten Bedürfnis des Lebens" werden. So würde der Ausdruck "arbeiten, um seinen Lebensunterhalt zu verdienen" nicht mehr bedeuten, die Mühen der Arbeit auf sich zu nehmen, um zu überleben und sich selbst zu versorgen, sondern tatsächlich seinen Lebensunterhalt zu verdienen.[^1].

## Arbeitszeitverkürzung

Wir werden Ihnen zeigen, dass das Wirtschaftsmodell der Gegenseitigkeit es ermöglicht, die für das Funktionieren der Gesellschaft notwendige Arbeitszeit erheblich zu reduzieren, bis hin zu einer Quote von 2 bis 3 Stunden pro Person.[^2][^8]

Produktivität ist das Verhältnis zwischen dem Output und der Anzahl der Arbeitsstunden, die zu seiner Erreichung erforderlich waren.

Die Produktivität ist in den letzten Jahrhunderten deutlich gestiegen.

Zunächst mit dem Einsetzen der industriellen Revolution im 18. und 19. Jahrhundert, in deren Verlauf die Produktionsprozesse mechanisiert und neue Technologien wie Dampf und Elektrizität eingeführt wurden.

Seitdem hat sich die Produktivität mit unterschiedlicher Geschwindigkeit weiterentwickelt, ist aber mit der Entwicklung neuer Technologien und Produktionssysteme stetig gestiegen.

Es kann schwierig sein, zuverlässige Daten über den Produktivitätsanstieg in Frankreich zu finden, die über 100 Jahre zurückreichen. Die Methoden zur Messung der Produktivität haben sich im Laufe der Zeit weiterentwickelt, was die Vergleichbarkeit der Daten über lange Zeiträume erschweren kann.

Aber zum Beispiel ist die Arbeitsproduktivität in Frankreich zwischen 1950 und 2020 im Durchschnitt um etwa 2% pro Jahr gestiegen. Das bedeutet, dass ein durchschnittlicher Arbeitnehmer in Frankreich an einem Arbeitstag etwa doppelt so viele Waren und Dienstleistungen produzieren kann wie im Jahr 1950.[^3].

Im selben Zeitraum ist die durchschnittliche Wochenarbeitszeit jedoch nicht um 50%, sondern um 12,5% gesunken, von 40 Stunden im Jahr 1950 auf etwa 35 Stunden im Jahr 2020.

Hier wurde eine zivilisatorische Entscheidung getroffen, die Möglichkeiten zu nutzen, die sich durch die ungeheure Entwicklung der Technik in den letzten Jahrhunderten eröffnet haben, nicht um die Arbeitszeit zu verkürzen, sondern um die Produktion und die daraus resultierenden Profite immer weiter zu steigern.

Das liegt an der eigentlichen Grundlage des Kapitalismus, dem Wachstum, von dem sein Überleben abhängt, dem Wunsch des Unternehmers nach Profit und dem Wunsch des Verbrauchers nach Akkumulation, die sich durch immer mehr verbinden.

Der Produktionsapparat muss also auf Hochtouren laufen. Das Ziel ist es, immer mehr zu verkaufen und die Gewinnmöglichkeiten zu vervielfachen.

Dies führt zu einem Unsinn in der Art und Weise, wie Güter und Dienstleistungen produziert und konsumiert werden.

Aber die Verkürzung der Arbeitszeit ist der Feind unserer Gesellschaft, denn die freie Zeit kann genutzt werden, um zu diskutieren, sich an der Basis zu organisieren und Formen des kollektiven Lebens zu erfinden. Dann stehen der Macht nicht mehr anonyme, isolierte, akzeptanzbereite Individuen gegenüber, sondern bewusste, starke und schwerer zu manipulierende Gruppen.

Wer also die Arbeitszeit verkürzt, muss weniger produzieren? Und damit katastrophale Auswirkungen auf unsere Lebensqualität?

Nein, es geht nicht darum, unsere Lebensqualität zu ändern, sondern sie für die gesamte Bevölkerung kontinuierlich zu erhöhen. Obwohl die Lebensqualität ein Konzept ist, das sich auf die individuellen und gesellschaftlichen Sitten einer bestimmten Epoche bezieht. Wir werden zeigen, dass es möglich ist, erhebliche Einsparungen vorzunehmen, die, ohne unseren Lebensstandard zu beeinträchtigen, eine erhebliche Verringerung der Produktion und damit der Arbeitszeit ermöglichen.

Hier zeigt sich die Macht der Reziprozität, denn sobald wir den Markt verlassen haben, können wir uns endlich effizient Lösungen zur Reduzierung der Arbeitszeit vorstellen, ohne dass diese mit unserer Wirtschaft in Konflikt geraten.

Denn die Absurditäten und Ungerechtigkeiten, die wir aufdecken werden, sind nicht das Ergebnis von Fehlern oder Ungeschicklichkeit, sie sind vielmehr notwendig für das Überleben des Kapitalismus.

In den folgenden Punkten werden die verschiedenen Möglichkeiten zur Reduzierung der Gesamtarbeitszeit beschrieben:

## Produktion/Konsum reduzieren

### Unsere extrinsischen Bedürfnisse ändern

Es fällt uns schwer, uns vorzustellen, wie wir außerhalb des Konsumsystems, das uns immer mehr verspricht, das Mehr bekommen können, das dem Leben Farbe und Geschmack verleiht, ebenso wie die Gegenstände, die unsere Identität sichern. Denn durch die Dinge, die wir besitzen, die Kleidung, die wir tragen, die Orte, die uns vertraut sind, fühlen wir uns den anderen so ähnlich, dass sie uns nicht ablehnen, und gleichzeitig so verschieden, dass wir sicher sein können, dass wir wir selbst sind.

Doch wenn wir unseren Konsum einschränken, können wir unseren Wunsch zu lieben und zu erschaffen offenbaren und die Zeit zum Leben freimachen, die uns heute so sehr fehlt. Dann werden wir die Freude erleben, das Leben fließen zu lassen, dessen Rauschen uns sagt, wer wir sind, die Freude zu lieben, wenn es keine Eile gibt, die Freude, eine Technik zu beherrschen, die uns eigen ist, die Freude, selbst Gegenstände zu erschaffen, ob wir sie nun als Zeichen für unseren Raum oder als Geschenk für unsere Lieben verwenden.

Diese Vision lässt uns verstehen, warum die Idee, die Arbeitszeit in unserer Gesellschaft zu verkürzen, auf so viel Widerstand stößt, denn die Zeit, die die Arbeit in Anspruch nimmt, zu befreien, bedeutet, eine Bresche in das Korsett zu schlagen, in das die kapitalistische Gesellschaft unser Leben gezwängt hat.

Das Wachstum wird aufrechterhalten, da es niemals die "Bedürfnisse" befriedigen kann, die aus einer Ungleichheit, die es nicht korrigiert, immer wieder neu geschaffen werden.

Diese Bedürfnisse sind Wünsche nach Status, Zugehörigkeit, Selbstwertgefühl, Macht oder sozialer Identifikation. Sie werden durch Werbung, Marketing und sozialen Druck geschaffen.

Werbung soll die Verbraucher davon überzeugen, dass sie bestimmte Produkte oder Dienstleistungen brauchen, um in der Gesellschaft glücklich, zufrieden oder angesehen zu sein. Sie verwenden Bilder oder Botschaften, die Emotionen, Wünsche und unsere Sehnsüchte ausnutzen.

### Die Lebensdauer der produzierten Gegenstände erhöhen

Ein Beispiel ist das Auto: Es erfüllt ein persönliches Bedürfnis nach Autonomie, aber auch ein extrinsisches Bedürfnis nach Prestige, das durch den Besitz des neuesten Modells oder des Spitzenmodells verliehen wird.

Die Automobilfirma sichert sich ihren Profit und damit ihr Überleben durch ein immer wieder erneuertes Produkt, indem sie die Wünsche der Verbraucher durch immer ausgefeiltere und teurere Maschinen weckt.

Wenn man die Stunden, die man mit einem Auto verdient, mit den Stunden vergleicht, die man für die Bezahlung des Autos arbeitet, gibt ein Arbeitnehmer durchschnittlich 375 Stunden pro Jahr für den Kauf, die Wartung, den Betrieb und die Versicherung aus, was zwei Monaten Arbeit entspricht.

Die ersten Autos konnten 200.000 bis 300.000 km fahren, der "Fortschritt" bestand in erster Linie darin, empfindlichere Autos zu bauen. Der Hersteller spart Geld, indem er bei den Abmessungen und der Qualität der Materialien spart und große Gewinne mit Ersatzteilen und der immer schnelleren Erneuerung des Materials erzielt.

Das Auto muss so konstruiert sein, dass es 20 Jahre oder länger hält, bis es vielleicht sogar abgeschafft wird, aber das wird im Kapitel über den Verkehr ausführlicher behandelt.

Die Denkmäler der Vergangenheit, Kathedralen und Schlösser, geben uns einen Einblick in die sozialen Beziehungen und die Ideologie ihrer Zeit. Unsere Denkmäler sind diese Maschinen, deren fantastische Komplexität den Stand unserer wissenschaftlichen Kenntnisse und unseres technologischen Know-hows widerspiegelt, die aber schon bei ihrer Geburt veralten und nach einigen Jahrzehnten auf dem Schrottplatz landen. Die Gebäude und Wohnungen, die richtig gebaut wurden, könnten problemlos über ein Jahrhundert lang halten.

Wie viele Geräte müssen repariert werden, oft schon kurz nach dem Kauf, und nur allzu oft ist die Reparatur schwierig oder zu teuer.

Die ersten Strümpfe waren unverwüstlich, heute werden sie nach der Herstellung in ein Säurebad getaucht, um ihre Festigkeit zu verringern. Die Firma, die die Leuchtstoffröhren erfunden hat, brachte sie erst auf den Markt, nachdem sie herausgefunden hatte, wie man ihre Betriebsdauer auf 1.000 Stunden reduzieren konnte. Es ist vorgesehen, dass die Qualität von Rasierklingen stumpf wird. Glühbirnen werden derzeit auf eine Lebensdauer von nur 1000 Stunden hochgezüchtet. Die gleichen Glühbirnen könnten 3.000 Stunden halten.

Die meisten Haushaltsgeräte könnten 30 Jahre lang halten.

Die Verschlechterung der Qualität von Gegenständen ist offensichtlich, aber es ist ein Geheimnis, das von der Industrie gut gehütet wird und schwer zu beweisen ist. Nur die Industrie weiß, wie viel es sie zusätzlich kosten würde, Gegenstände herzustellen, die zwei- bis dreimal so lange halten würden.

Das ist das Paradoxon: Ein zu langlebiges Produkt kann in einer Marktlogik, die auf monetären Gewinn abzielt, langfristig nicht rentabel sein, wohl aber in einer Logik der Gegenseitigkeit, die auf weniger Arbeit abzielt.

### Verschwendung reduzieren

In unserer Gesellschaft produzieren wir viel mehr, als wir tatsächlich benötigen, was zu einer ineffizienten Nutzung natürlicher Ressourcen und einer Anhäufung von Abfällen führt.

Lebensmittelverschwendung: Nach Angaben der FAO geht jedes Jahr etwa ein Drittel der weltweit produzierten Lebensmittel verloren oder wird verschwendet. Dies kann auf Überproduktion, schlechtes Management der Lieferkette, hohe ästhetische Standards oder Konsumverhalten wie Impulskäufe oder Überkonsum zurückzuführen sein.

Energieverschwendung: Wir neigen dazu, mehr Energie zu verbrauchen, als wir für unsere täglichen Bedürfnisse benötigen. Zum Beispiel, indem wir elektronische Geräte im Standby-Modus belassen, ineffiziente Verkehrsmittel benutzen oder unnötig Licht brennen lassen.

Materialverschwendung: Unsere Gesellschaft produziert häufig Einweg- oder minderwertige Güter, die schnell weggeworfen oder ersetzt werden, anstatt langlebige und leicht reparierbare Güter zu bevorzugen. Dies führt zu einer ineffizienten Nutzung natürlicher Ressourcen und zu einer Anhäufung von Abfall.

### Güter teilen

Durch das Teilen von Gütern können Einzelpersonen ihren Verbrauch und damit auch ihre Produktion reduzieren, da sie nicht so viele Gegenstände kaufen müssen, um ihre Bedürfnisse zu befriedigen. Beispielsweise können sich mehrere Personen eine Bohrmaschine teilen, anstatt dass jeder eine Bohrmaschine für den gelegentlichen Gebrauch kauft. Dadurch wird die Produktion von Bohrmaschinen reduziert.

90% der Autos sind unbeweglich, also muss man sich zusammenschließen, um weniger zu produzieren.

Außerdem werden Güter, die gemeinsam genutzt werden, tendenziell besser genutzt und gepflegt, was die Lebensdauer der Gegenstände verlängert und damit auch die Produktion neuer Güter. Wenn die Menschen Miteigentümer sind, gibt es 30 % weniger Schäden.

## Steigerung der Produktivität

### Den technischen Fortschritt nutzen

Technischer Fortschritt wird häufig mit Produktivitätssteigerung in Verbindung gebracht, da er es ermöglicht, mehr Waren oder Dienstleistungen mit weniger Ressourcen, Zeit und Aufwand zu produzieren. Technischer Fortschritt hat bereits die Effizienz und Qualität unserer Produktionsprozesse verbessert und dazu beigetragen, die Produktionskosten zu senken.

Eine vierte industrielle Revolution ist im Gange, die durch künstliche Intelligenz, das Internet der Dinge, fortgeschrittene Robotik, Augmented Reality, Blockchain, Nanotechnologie und Biotechnologie ausgelöst wird. Und diese Technologien werden die Art und Weise, wie wir produzieren, wirklich verändern.

Es muss jedoch darauf geachtet werden, dass die Nutzung des technischen Fortschritts keine Auswirkungen auf die Auswirkungen auf die menschliche Umwelt und das Bruttoinlandsprodukt hat, sondern immer zu einer nachhaltigen und gerechten Entwicklung beiträgt.

### Kürzere Arbeitszeiten nutzen

Jede Arbeit, auch die einfachste, erfordert ein Mindestmaß an Ausbildung, Training und Handwerk. Wenn die Arbeitszeit sinkt, kann die Produktivität nur bis zu einem bestimmten Punkt steigen, danach sinkt sie wieder.

Denn je mehr man beherrscht, desto produktiver ist man, aber je länger man arbeitet, desto müder und damit weniger produktiv kann man werden.

Und es ist möglich, dass bei vielen Berufen, die Know-how erfordern, dieser Punkt mit "2 bis 3 Stunden pro Tag" überschritten wird, weshalb diese 2 bis 3 Stunden pro Tag nicht wörtlich genommen werden sollten. Die Arbeit jedes Einzelnen kann so organisiert werden, dass es Zeiten gibt, in denen mehr Arbeit anfällt, und Zeiten, in denen weniger Arbeit anfällt oder die Arbeit völlig frei ist.

Zum Beispiel können drei 8-Stunden-Tage pro Woche über zwei Jahre hinweg zu drei freien Jahren führen.

## Arbeit abschaffen

### Gebundene Arbeit in freie Arbeit umwandeln

In unserer heutigen Gesellschaft gibt es wie in jeder Gesellschaft unangenehme und angenehme Berufe. Im Allgemeinen werden die mühsamen und langweiligen Arbeiten schlecht bezahlt und haben wenig Prestige, und die Tätigkeiten, die am meisten Geld einbringen, sind in der Regel die, die gesellschaftlich am besten angesehen und für diejenigen, die sie ausüben, interessant sind.

Man kann zwischen 2 Arten von Arbeit unterscheiden.

Die mühsame und langweilige Arbeit, die notwendig ist, um die Gesellschaft als Ganzes am Laufen zu halten, wird als "gebundene Arbeit" bezeichnet.

Und die freie Arbeit, die für denjenigen, der sie verrichtet, einen Sinn hat, aus einer kreativen Tätigkeit hervorgeht, die in sich selbst ihre Rechtfertigung findet, wobei diese natürlich subjektiv und individuell sein kann.

In Wirklichkeit entzieht sich 3/4 der Arbeit, die in unserer kapitalistischen Gesellschaft geleistet wird, der Marktwirtschaft.

Mit mehr Zeit und einer besseren Gesellschaft können wir uns auf ein Netzwerk gegenseitiger Hilfe stützen, um unsere alltäglichen Probleme zu diskutieren und zu lösen, unsere Ernährung, unser Sexualleben, unsere Ängste vor dem Leben zu diskutieren, so wie es bereits in großem Umfang in großen Facebook-Gruppen geschieht.

Die Geräte könnten so konzipiert werden, dass sie von den Nutzern beherrschbar sind, mit standardisierten Werkzeugen leicht zusammen- und auseinandergebaut werden können, die Modelle ausgetauscht werden, wenn echte technische Fortschritte dies erfordern, wobei gewährleistet wird, dass Ersatzteile über sehr lange Zeiträume zu vernünftigen Preisen geliefert werden, den Kunden detaillierte und klare Anleitungen zur Verfügung gestellt werden, lokale Montage- und Reparaturwerkstätten eingerichtet werden, in denen die notwendigen Werkzeuge zu finden sind und Hilfe und Erfahrungen ausgetauscht werden können.

Einrichtung von Wohnungen kann von den Bewohnern selbst vorgenommen werden. Was heute eine untragbare Last ist, ist der Bau einer Wohnung mit einem so schweren Stundenplan. Diese Tätigkeit könnte jedoch mit der gegenseitigen Unterstützung der Gemeinschaft zu einem kreativen Akt werden, der Spaß macht.

Das Unterrichten könnte nicht das Monopol eines Berufsstandes bleiben, sondern flexibler werden und die Funktion des Lehrers könnte breiter geteilt werden, und jeder, der Wissen zu bieten hat, könnte ermutigt werden, pädagogisch zu helfen.

In der Tat könnte ein Großteil der Arbeit, die von den Mitarbeitern bestimmter Berufsgruppen geleistet wird, von anderen erledigt werden und in unserem Eutopia zu freier Arbeit werden, wodurch diese Berufsgruppen entlastet würden.

### Von der Neuorganisation der Arbeit profitieren

Das nächste Kapitel befasst sich mit der Reorganisation der Arbeit, d. h. mit der Art und Weise, wie die Unternehmen in unserem Eutopia organisiert sind.

Diese würden durch ihre Horizontalität eine gewisse Hierarchie abschaffen, die Managementarbeit und Entscheidungsfindung leistet. Diese wären nicht mehr relevant, da diese Arbeit nun kollaborativ von allen Kooperanten eines Unternehmens erledigt wird.

### Bullshit-Job und notwendigerer Beruf

In unserem Eutopia würden viele Berufe abgeschafft werden, da diese in Wirklichkeit das Produkt von Fehlfunktionen in der heutigen Gesellschaft sind und somit nicht mehr notwendig wären.

- Werbung beläuft sich auf 1% des BIP[^4].
- Das Militär 3% des BIP.
- Verlorene Arbeit bei Versicherungsgesellschaften.

Eine vollständige Liste der aus Eutopia gestrichenen Berufe finden Sie am Ende dieser Erzählung.

## Erhöhung der Anzahl der Arbeiter

### Beseitigung der Arbeitslosigkeit

In einem Land wie Belgien liegt die Arbeitslosenquote derzeit bei 5,5 %. Unter Berücksichtigung der gesamten Erwerbsbevölkerung in Belgien von 5,8 Millionen Menschen gemäß den Eurostat-Daten für 2021 bedeutet dies, dass etwa 319.000 Menschen ohne Arbeit sind.[^7].

Da es keine Beschäftigung im eigentlichen Sinne gibt, geht der Effekt des Verschwindens der Lohnarbeit mit dem Verschwinden der Arbeitslosigkeit einher.

Sie baut soziale Inklusion und eine offene Gesellschaft auf, in der jedes neue Mitglied mit seinen Fähigkeiten und Kenntnissen jeglicher Art willkommen ist, um einen Beitrag zur Gesellschaft zu leisten und die Gesamtarbeitszeit zu verringern.

### Die Zahl derer erhöhen, die sich an der gebundenen Arbeit beteiligen

Die Gesellschaft schließt eine ganze Reihe von Menschen vom Arbeitsleben aus, darunter auch Menschen mit Behinderungen.

Das kann durch Diskriminierung, Stigmatisierung, fehlenden Zugang oder fehlende Unterstützung geschehen.

Das System entscheidet oftmals über alles oder nichts, obwohl es für sie besser ist, weiterhin Unterstützung zu erhalten, als zu arbeiten und die Unterstützung zu verlieren.

Die allgemeinen Qualifikationen der Bevölkerung könnten erheblich gesteigert werden, da Expertinnen derzeit meist aus den oberen sozialen Schichten rekrutiert werden. Mit der Möglichkeit für alle, das zu werden, was sie wollen, und den entsprechenden Mitteln, würde der Anteil der Experten jedoch weitaus höher liegen.

Ältere Menschen, die nach der Pensionierung aus dem Markt gedrängt werden, die diskriminiert werden, etc.

### Reduzierung von Gesundheitsproblemen

Laut einer Umfrage der Unternehmensberatungsgruppe Mercer unter europäischen Arbeitnehmern aus dem Jahr 2020 gaben rund 33% der europäischen Arbeitnehmer an, dass sie sich aufgrund von Stress oder psychischen Problemen krankschreiben ließen.[^5] Darüber hinaus ergab eine Umfrage der Europäischen Kommission aus dem Jahr 2018, dass Burnout bei 28% der Arbeitnehmer in der EU vorkommt.[^6].

## Quellen (Liste nicht vollständig)

[^1]: Serge-Christophe Kolms. Die gute Wirtschaft. 1984
[^2]: Adret. Zwei Stunden am Tag arbeiten. 1977
[^3]: [Insee. Wachstum - Produktivität] (https://www.insee.fr/fr/statistiques/4277770?sommaire=4318291)
[^4]: Bullshit Job. David Graeber. 2018
[^5]: [Mercer Marsh Benefits Studie - Absentismus-Barometer](https://www.mercer.com/fr-fr/insights/total-rewards/employee-benefits-strategy/barometre-de-labsenteisme-2023/)
[^6]: [MITTEILUNG DER KOMMISSION AN DAS EUROPÄISCHE PARLAMENT, DEN RAT, DEN EUROPÄISCHEN WIRTSCHAFTS- UND SOZIALAUSSCHUSS UND DEN AUSSCHUSS DER REGIONEN über einen umfassenden Ansatz im Bereich der psychischen Gesundheit](https://health.ec.europa.eu/system/files/2023-06/com_2023_298_1_act_fr.pdf)
[^7]: [ STATBEL - Beschäftigung und Arbeitslosigkeit ](https://statbel.fgov.be/fr/themes/emploi-formation/marche-du-travail/emploi-et-chomage)
[^8]: Keynes. J. Brief an unsere Enkelkinder. Essay über die Zukunft des Kapitalismus. 1930.
