---
title: Arbeitsorganisation
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Inhalt im Versuchsstadium und noch in Bearbeitung.
> {.is-danger}

## Themen, die erkundet und verfasst werden :

- Horizontale Hierarchie
- Kollektive Intelligenz
- Faire und demokratische Rotation und Aufgabenteilung.
- Lebenslanges Lernen
- Genossenschaften und dezentralisierte Strukturen
- Wohlbefinden am Arbeitsplatz
- Anwendung kollektiven, demokratischen und fairen Managements

## Was ist eine Arbeitsorganisation?

Eine Arbeitsorganisation hat das Ziel, Güter und/oder Dienstleistungen zu produzieren, die für die Entfaltung der Bevölkerung notwendig sind.

Eine Arbeitsorganisation kann eine Schule, ein Geschäft, ein Telekommunikationsunternehmen usw. sein.

Wenn sie nicht individuell sind, funktionieren alle Arbeitsorganisationen horizontal auf der Grundlage kollektiver Intelligenz.

Sie haben alle einen genossenschaftlichen Status und gehören den Menschen, die in ihnen arbeiten.

Es werden zahlreiche IT-Tools entwickelt, die auf die Bedürfnisse der jeweiligen Arbeitsorganisation zugeschnitten sind, um ein kollaboratives, effizientes Arbeiten zu ermöglichen, das eine effektive demokratische Entscheidungsfindung ohne Machthierarchien erlaubt.

Diese Kluft muss überbrückt werden, nicht indem man die Notwendigkeit von Kompetenz leugnet, sondern indem man anerkennt, dass alle Grade von Kompetenz koexistieren und somit kollektive Intelligenz entwickeln können.

Arbeitsorganisationen werden direkt auf der Grundlage der Bedürfnisse gegründet, die die Bürger äußern.

Wenn ein innovativer Unternehmer der Meinung ist, dass es eine potenzielle Nachfrage nach einem neuen Produkt, einer Ware oder Dienstleistung, die ein anderes Produkt ersetzt, oder nach einem kostengünstigeren Produkt gibt, kann er es wagen, dieses herzustellen und auf den Markt zu bringen. Dieses wirksame Prinzip des freien Unternehmertums und des Wettbewerbs sowie die Fähigkeit, Nachfrage aufzudecken, sind unverzichtbar.

Jeder Bürger hat die Freiheit, seine eigene Arbeitsorganisation außerhalb der Bedürfnisse der Gesellschaft zu gründen, in seiner Freizeit außerhalb der notwendigen kollektiven Arbeit.

Es ist auch möglich, wie bei den heutigen Crowfounding-Systemen, die Zustimmung der Bürger für die Idee und Entwicklung eines Projekts zu erhalten und direkt von der für das Funktionieren der Gesellschaft notwendigen kollektiven Arbeitsstunde befreit zu werden, um sich voll und ganz diesem Projekt widmen zu können.

## Arbeitsteilung

Wir beginnen mit der Oase. Man kann im Voraus festlegen, was für den reibungslosen Betrieb einer Oase notwendig ist, um ihre Selbstversorgung in Bezug auf eine bestimmte Bevölkerung zu ermöglichen.

Jeder kann innerhalb seiner Oase tun, was er will, damit sie reibungslos funktioniert. Sie müssen sich eine Anwendung ausdenken, mit der Sie sich in die Organisation der Oase einklinken können.

Sie können die Arbeit wählen, die Sie möchten. Wenn es mehr Personen gibt, als für eine bestimmte Arbeit benötigt werden, dann kann diese geteilt werden und wird dann geteilt; z. B.: wenn es 5 Stunden dauert, um Äpfel von einem Obstbaum zu pflücken, dann wird die Zeit durch die Anzahl der Freiwilligen geteilt, oder wenn jemand unterrichten möchte, dann wird die Anzahl der Schüler geteilt, um einen günstigeren Unterricht in kleinen Gruppen zu ermöglichen.

Oder es handelt sich um eine Aufgabe, die nicht geteilt werden kann, z. B. ein Lehrer, der das ganze Jahr über eine Klasse mit einer Mindestanzahl an Schülern betreuen muss, und die Teilung ist bereits bis zur maximalen Anzahl durchgeführt, dann werden Sie in eine Warteschlange eingereiht.

Wenn es nicht genügend Personen für eine bestimmte Arbeit gibt, wird diese unter den verschiedenen Mitgliedern der Gemeinschaft aufgeteilt, wobei es jedoch möglich ist, Ihre Präferenzen zu notieren. Zum Beispiel wird jedes Mitglied einer Oase einen Tag im Jahr damit verbringen, den Müll zu sammeln oder die Straßen zu mähen.

Für eine Oase mit 1000 Einwohnern wird beispielsweise geschätzt, dass jeder Einwohner 5 Stunden pro Woche für das Funktionieren der Oase aufwenden muss, was 89 Personen entspricht, die 8 Stunden pro Tag arbeiten.

Es kann jedoch sein, dass einige Bewohner mehr als 8 Stunden pro Tag für die Oase aufwenden, da sie aus Altruismus oder eigenem Interesse an der Arbeit gerne "mehr" geben, als sie erhalten. Dadurch wird die Gesamtarbeitsbelastung verringert.

Denn die Arbeit in Bezug auf die Oase ist nicht die einzige, es gibt auch Arbeit in Bezug auf die Edens, die Eldorados und auch das Nirvana, und so werden Menschen dazu gebracht, ihren Beitrag auf anderen Ebenen der Gemeinschaft zu leisten.

Die Idee ist, dass jeder das tut, wozu er Lust hat, um einen Beitrag zur Gesellschaft zu leisten, und dass die verbleibende Arbeit, die niemand tun möchte, unter der Bevölkerung aufgeteilt wird. Dies ist eines der grundlegenden und verbindlichen Prinzipien, um den Wert der Freiheit zu respektieren.

Wie werden dann aber die verfügbaren Arbeitszeiten festgelegt? Sie werden von den Organisationen selbst festgelegt, die Organisationen arbeiten alle horizontal und dies wird schließlich durch die entwickelten speziellen IT-Tools ermöglicht. Wir werden diesen Aspekt im nächsten Punkt näher erläutern.

## Situationsbeschreibung

Sie möchten unterrichten? Platzieren Sie sich in dem ganzjährigen Zeitfenster, um Lehrer an der Schule Ihrer Oase zu werden. Je nach Anzahl der Lehrer können die Klassen in kleinere Klassen aufgeteilt werden, was für den Unterricht günstiger ist. Wenn es nicht genügend Lehrer gibt, werden andere Oasen um Verstärkung gebeten. Wenn keine Verstärkung verfügbar ist, wird der Slot als vorrangig markiert und Oasenmitglieder werden geschult, um Verstärkung zu bekommen.

Niemand weiß, wer in der Zeitnische für das Einsammeln der Mülltonnen eingetragen ist. Jedes Oasenmitglied bekommt eine Schicht im Jahr zugewiesen, um diese Aufgabe untereinander aufzuteilen.

> "Wenn die Wirtschaft in ihrer Produktion und ihrem Konsum symbiotisch ist, aber nicht in ihrer Führung, kann sie ganz grün sein, aber nicht unbedingt rosa." Isabelle Delannoy[^1]]

## Quellen (Nicht erschöpfende Liste)

[^1]: Isabelle Delannoy. Die symbiotische Wirtschaft: Regeneration des Planeten, der Wirtschaft und der Gesellschaft.
