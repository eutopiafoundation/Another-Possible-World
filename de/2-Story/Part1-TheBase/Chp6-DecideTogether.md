---
title: Gemeinsam entscheiden
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Diese Seite befindet sich noch im Aufbau, daher kann es sein, dass einige Teile fehlen.
> {.is-info}

## Wie können wir alle gemeinsam entscheiden?

Man muss verstehen, wie man Entscheidungen trifft und wie man sicherstellt, dass diese Entscheidungen die richtigen sind, um unser gemeinsames Ziel zu erreichen.

Es gibt viele Werkzeuge zur Entscheidungsfindung, und viele weitere müssen noch erfunden werden. Wir lassen hier die Präzisionswerkzeuge auf kleinerer Ebene beiseite und konzentrieren uns auf das auf globaler Ebene.

In unserer westlichen Gesellschaft ist das eingesetzte Instrument die Demokratie. Die Analyse und Neuerfindung der Demokratie wird es uns ermöglichen, kleinere Werkzeuge zu entwickeln, die auf ihr basieren und an andere Organisationsformen wie Unternehmen, Schulen usw. angepasst werden können.

## Was ist Demokratie

Demokratie ist die griechische Kombination aus demos "das Volk" und kratos "die Macht". Der Begriff bezeichnet heute jedes politische System, in dem das Volk der Souverän ist.[^1].

Viele Länder haben die Gleichheit der politischen Macht im allgemeinen Wahlrecht erobert. Dieser Sieg ist nun in ihren Verfassungen verankert und wird als eines der strahlendsten institutionellen Juwelen ihrer Gesellschaft beurteilt.

Doch das Wort "Demokratie" ist ein immer noch viel missbrauchtes und missbrauchtes Wort.

Wir proklamieren Demokratie, als ob wir tatsächlich die Macht hätten.

Doch unsere Demokratie ist nichts anderes als eine wiederkehrende Abdankung unserer legitimen Macht an Politiker, die sie an sich reißen.

Und wenn sie sie erst einmal erlangt haben, fühlen sie sich dann legitimiert, sie zu haben, aber selbst sie stehen unter der Kontrolle einer viel größeren Macht.

Man glaubt, dass das politische System dominiert und alles andere verändern kann, einschließlich der Wirtschaft und ihrer Form. Man glaubt, dass unser System aufgrund des Wettbewerbs der Kandidaten, denen man seine Stimme gibt, vollkommen demokratisch ist.

Nur werden wir sehen, dass die Wirtschaft in diese konkurrierende Wahl eingreift. Dann hat das Wahlsystem einerseits nicht mehr diese Qualität, einen guten Wettbewerb der Kandidaten zu bieten, andererseits dominiert es nicht die Wirtschaft, da diese es beeinflusst.[^2].

Es ist also die Wirtschaft, die die Politik dominiert.

Zum Beispiel bei Klimafragen zeigen Studien, dass es keinen Zusammenhang zwischen dem gibt, was die Mehrheit der Menschen will und dem, was sie bekommt, außer wenn sie die gleichen Präferenzen haben wie die reichsten 10 % der Bevölkerung.[^3].

Unsere Demokratie ist völlig überholungsbedürftig. Wir sprechen hier von zahlreichen Mängeln, Machtmissbrauch, Korruption, Einfluss von Lobbyisten, nicht abgestimmten, nicht geteilten Entscheidungen, Mängeln der Repräsentativität, Illusionen der Wahlmöglichkeiten, Chancenungleichheit.

Und hier entdecken wir, dass das tatsächliche Ergebnis unseres demokratischen Systems das Gegenteil von dem ist, was es ideologisch anstrebt.

Trotz all dieser Mängel ist die Demokratie ein Risiko, das wir eingehen müssen[^4], wenn wir unsere Freiheit bewahren wollen.

Wir werden also zunächst unsere Demokratie analysieren, verstehen, wo die Probleme liegen, um dann ein völlig anderes demokratisches System neu zu erfinden.

## Das Problem unserer Demokratien

### Der politische Unternehmer[^5][^6][^7]

Das Ziel eines jeden Politikers ist es, an der Macht zu sein und zu bleiben; dieses Ziel unternimmt er. Er macht Karriere, was ihm unter anderem ermöglicht, seinen Lebensunterhalt zu verdienen und seinen Platz in der Gesellschaft zu finden.

Sie stehen miteinander im Wettbewerb, aber auch im Einvernehmen: Sie wollen ihren politischen Gegner zerstören, aber nicht das Amt des Politikers.

Alle Politiker haben, unabhängig von ihren Gegensätzen, ein gemeinsames Interesse daran, die Macht der Politiker zu stärken und nicht zu schmälern. Und wenn einer von ihnen die Idee dazu vorschlägt, wird er die Missbilligung aller anderen erfahren.

Die politische Unternehmer kommen in der überwiegenden Mehrheit aus der gleichen sozialen Schicht, in der Regel auch aus der der Führungskräfte der Privatwirtschaft und aus den gleichen bürgerlichen Familien.

Die Kandidaten versuchen nicht, das Programm zu präsentieren, das ihr Ideal der guten Gesellschaft darstellt.

Unabhängig von ihren persönlichen Zielen wählen sie das Programm, das für die meisten Menschen am besten geeignet ist. Dabei gehen sie so weit, dass sie die Verteidigung ihrer eigenen Meinungen, Ideale und persönlichen Vorlieben, sofern sie welche haben, dem opfern, was die Masse der Bevölkerung will.

Dennoch wird dieses Wissen der Kandidaten über die Wünsche der Wähler größtenteils nicht genutzt, um der größeren Zahl zu dienen, sondern, wie wir sehen werden, wird es genutzt, um sie zum Vorteil der kleineren Zahl auszubeuten.

Die Politiker reagieren lediglich auf die Anreize, die es ihnen ermöglichen, Macht zu erlangen und an der Macht zu bleiben.

Es hat auch keinen Sinn, sie zu hassen, denn sie befolgen nur die Regeln eines Spiels. Daher ist es das Spiel, das man hassen sollte, und nicht die Spieler.

Das Problem der politischen Unternehmer ist jedoch, dass ihr Ziel nicht darin besteht, Recht zu haben, sondern dass das Volk und die Unternehmen ihnen Recht geben.

Politische Unternehmer sind nicht dumm, sie sind geschickte Taktiker, die auf die Anreize des Spiels reagieren und durch die Expertise ihres Teams in diesem Bereich sehr gut beraten werden. Sie wenden dann verschiedene Tricks und Allianzen an, um ihr Ziel zu erreichen.

Um ein gutes Bild abzugeben, gehen die Kandidaten dazu über, ihre Persönlichkeit zu verbergen, sich zu verstellen und sich in eine Heuchelei zurückzuziehen.

Der Wähler glaubt zu wissen, wen er wählt, da der Schwerpunkt häufig auf der Persönlichkeit des Kandidaten liegt, indem er seine Attraktivität, seine Sicherheit oder sein Charisma hervorhebt, was insbesondere einen Halo-Effekt hervorruft. Und diese Eindrücke lassen sich von den Medien viel leichter manipulieren als der Inhalt eines Programms.

### Politische Programme[^2]

Die Kandidaten bauen ihr "Programm" nicht auf einmal und in einem Zug auf, sondern durch sukzessive Hinzufügung oder Änderung von Aussagen und Handlungen, ohne jedoch viel daran ändern zu können, frühere Worte oder Handlungen zu löschen oder ihnen zu widersprechen. Dieser Zwang erklärt sich dadurch, dass es sehr zeitaufwändig ist, ein Programm bekannt zu machen, während es die Wähler besser erreicht, wenn man es nach und nach präzisiert, hinzufügt und verstärkt.

Andererseits erklärt es sich, weil diese Widersprüche oder Verleugnungen die Zuverlässigkeit und Glaubwürdigkeit des Kandidaten beeinträchtigen würden, was eine evolutionäre, iterative Struktur des Wissens und der Programmkonstruktionen schafft.

Dies führt also häufig zu Unterschieden in ihren Programmen und zu einer Unvollkommenheit, die die demokratische Qualität des Prozessergebnisses beeinträchtigt.

Andererseits sind die Programme global, behandeln alle Themen gleichzeitig und man muss zwischen ihnen wählen.

Dann spalten die Programme die Bevölkerung, denn ein Programm könnte von der Mehrheit gewählt werden, während die Mehrheit einen Punkt aus allen anderen Programmen vorzieht, als den des Gewinners.

Das ist etwas, was spezielle Referenden ermöglichen, bei denen man seine Meinung zu jeder Frage einzeln abgeben kann.

Der Effekt von interessierten finanziellen Beiträgen ist eher, dass die Programme vereinheitlicht werden, während der Effekt von idealistischen/aktivistischen Unterstützungen im Gegenteil dazu führt, dass sie sich unterscheiden.

### Ehrlichkeit und Transparenz

Jedes System, das manipuliert werden kann, wird manipuliert werden, und zwar zum ungünstigsten Zeitpunkt.[^9].

Wenn man ein System entwirft, muss man sich unbedingt fragen, wie die Systembeteiligten das System angreifen und auf unlautere Weise ausnutzen können.

Ehrlichkeit und Transparenz sind hartnäckige Probleme in demokratischen Systemen auf der ganzen Welt. Politiker und gewählte Vertreter werden häufig beschuldigt, nicht transparent zu sein, korrupt zu sein und sich in zahlreichen Interessenkonflikten zu befinden, da sie Einzelinteressen gegenüber dem Gemeinwohl fördern.

In unserem System gibt es einige Regeln, die Ehrlichkeit und Ethik nicht fördern.

Daher ist es von entscheidender Bedeutung, Systeme zu entwerfen, die die Menschen dazu bringen, ethisch und moralisch zu handeln.

Es muss das Prinzip der Offenbarung entwickelt werden, bei dem die Teilnehmer ein Interesse daran haben, bei ihren Offenbarungen ehrlich zu sein und sich ethisch und moralisch zu verhalten, da dies in ihrem eigenen Interesse liegt und ihnen langfristig zugutekommt. Es sollten Maßnahmen für mehr Transparenz aufgenommen werden.

### Die Wahl der Alternativen

Abgesehen von der Tatsache, dass die Programme global sind, gibt es in der Regel nur eine geringe Anzahl von Programmen oder Kandidaten, die dem Wähler zur Auswahl stehen.

Die Finanzierung von Wahlkampagnen ist einer der Faktoren, die die Auswahl an Alternativen einschränken. Kandidaten sind diejenigen, die Zugang zu Finanzierungsquellen haben und genügend Informationen und Propaganda über sich selbst entfalten können, um ihre Partei sichtbar zu machen und eine Chance auf den Sieg zu haben.[^10].

Politische Parteien können interne Strukturen haben, die bestimmte Kandidaten oder politische Tendenzen auf Kosten anderer bevorzugen. Dies kann die Wahlmöglichkeiten der Wähler einschränken, indem ihnen nur einige wenige Optionen angeboten werden, die von den politischen Parteien gebilligt wurden.

### Die Delegation von Macht

Eine Wahl besteht aus zwei Dingen: einer Abstimmung und einer Machtübertragung

Wie bereits gesagt, ist unsere Demokratie die wiederkehrende Abdankung der legitimen Macht des Volkes an eine Gruppe von Personen.

Die Delegation von Macht kann auch zu einem Verlust der Kontrolle über Entscheidungen führen. Wenn eine Person oder eine Gruppe ihre Macht an eine andere Person oder Gruppe delegiert, kann sie die Kontrolle darüber verlieren, welche Entscheidungen getroffen werden und wie sie umgesetzt werden.

Unter dem Vorwand der Delegation, die den gewählten Vertretern jegliche Legitimität verleiht, kann es zu einer Entwendung, Entfremdung, Beraubung oder Usurpation von Macht kommen.

Eines der Hauptprobleme bei der Delegation von Macht ist die Gefahr der Entmachtung. Wenn eine Person oder eine Gruppe ihre Entscheidungsbefugnis an eine andere Person oder Gruppe delegiert, kann sie die Verantwortung für ein Versagen oder einen Fehler der Person oder Gruppe, an die sie ihre Macht delegiert hat, abgeben.

Ein weiteres Problem bei der Delegation von Macht ist das Risiko eines Interessenkonflikts.
Manche Geschäfte erfordern Zeit, Energie, Vorwissen und intellektuelle Fähigkeiten. Dann kann es vorteilhaft oder sogar notwendig sein, auf Arbeitsteilung und Spezialisierung zurückzugreifen, indem man einen Teil der Entscheidungsarbeit delegiert. Diese Delegation ist jedoch Macht. So kann z. B. eine Regierung ihre Macht an ein Unternehmen delegieren, um ein Projekt zu verwalten. Da der Delegatar aber nicht genau weiß, was er delegiert, da er per Definition nicht alle Informationen über die Möglichkeiten hat. Das lässt die Tür für Missbrauch offen[^2].

### Die Zentralisierung der Macht

Eine Zentralisierung liegt vor, wenn wenige viel und viele wenig entscheiden.

Die Zentralisierung erhöht die Delegation von Macht, was wiederum die Demokratie verringert.

Wenn eine kleine Gruppe von Menschen absolute Macht besitzt, kann dies zu einseitiger Entscheidungsfindung und ungerechten Politiken führen, die die Interessen und Meinungen der Bürger nicht mehr berücksichtigen.

Die Zentralisierung von Macht kann Regierungen anfälliger für Machtmissbrauch, Tyrannei und Diktatur machen, da es weniger Kontrolle und Gegenkräfte gibt, um die Autorität einer kleinen Gruppe von Personen zu beschränken.

### Die Wahlmodi

Es ist wichtig, sich bewusst zu machen, dass ein gewählter Kandidat durch eine Wahl gewählt wird und dass eine Änderung der Wahl den Gewählten ändern wird.

Denn der Wahlprozess hat einen großen Einfluss auf das Ergebnis einer Wahl. Die Wahlentscheidung beeinflusst also die Zukunft unserer Gesellschaften. Daher ist es wichtig, sich auf die Wahl der Wahl zu konzentrieren, die man einführen möchte.

Sowohl die Wahl mit einem Wahlgang als auch die Wahl mit zwei Wahlgängen haben ein grundlegendes Problem: Sie können die Präferenzen der Wähler nicht klar und deutlich zum Ausdruck bringen[^11].

Bei einem System mit einem Wahlgang kann der Wähler nur für einen Kandidaten stimmen, was problematisch sein kann, wenn mehrere Kandidaten große Unterstützer haben. Bei einer Wahl mit drei wichtigen Kandidaten muss der Wähler beispielsweise zwischen drei verschiedenen Optionen wählen, hat aber nur eine einzige Stimme, um seine Wahl auszudrücken. Dies kann zu Situationen führen, in denen der gewählte Kandidat nicht die Unterstützung der Mehrheit der Wähler hat, sondern lediglich die meisten Stimmen.

Das Problem bei Wahlen mit zwei Wahlgängen ist, dass sie manchmal zu strategischen Wahltaktiken und zu Allianzen zwischen Kandidaten führen können. In einem System mit zwei Wahlgängen treten die beiden Kandidaten, die im ersten Wahlgang die meisten Stimmen erhalten haben, in einem zweiten Wahlgang gegeneinander an. Dies kann zu Situationen führen, in denen die Wähler im ersten Wahlgang unterschiedlich abstimmen, um sicherzustellen, dass ihr bevorzugter Kandidat in die zweite Runde kommt, auch wenn sie von diesem Kandidaten nicht wirklich überzeugt sind. Außerdem kann es Fälle geben, in denen sich Kandidaten zusammenschließen, um einen anderen Kandidaten aus dem Rennen zu drängen, anstatt zu versuchen, die Wahl unabhängig zu gewinnen.

Generell hat die Mehrheitswahl den gravierenden Nachteil, dass sie die Intensität der Präferenzen der Bürger nicht berücksichtigt. Diejenigen, die zwischen zwei Alternativen fast gleichgültig sind, und diejenigen, die die eine Alternative viel mehr bevorzugen als die andere, haben das gleiche Gewicht bei der Wahl zwischen ihnen. Beispielsweise überwiegen 1001 Bürger, die eine Alternative kaum bevorzugen, gegenüber den anderen 1000, für die sie die schlimmste Katastrophe ist.

Diese Abstimmungen fördern die nützliche Stimmabgabe, die Politiker dazu ermutigt, sich in Form einer politischen Partei zu organisieren und die Wähler in vordefinierte Schubladen zu stecken, was den Effekt der Bipolarisierung verstärkt.

Die Bipolarisierung der Wählerschaft in der Politik bezieht sich auf die Tendenz der Wähler, sich um zwei politische Hauptparteien zu gruppieren und so eine deutliche politische Spaltung zwischen diesen beiden Parteien zu schaffen. Dadurch bleibt wenig Raum für unabhängige Parteien oder Kandidaten oder für kleinere politische Parteien. Dies hat auch die negative Folge, dass es zu einer stärkeren Polarisierung der Meinungen, einer Verringerung der Vielfalt der Standpunkte

Bei diesen Wahlgängen kommt es dazu, dass man gegen einen Kandidaten stimmt und nicht für ihn.

Die nützliche Stimme macht den Gewählten jedoch nicht legitim.

Das Verhältniswahlrecht sorgt dafür, dass die Politik nicht zu sehr polarisiert wird, kann aber zu einer Zersplitterung der Macht führen und die Bildung stabiler Regierungen erschweren. Es wird für eine Partei schwierig, die absolute Mehrheit der Sitze zu erringen, was zu instabilen Regierungskoalitionen oder Minderheitsregierungen führen kann, die Schwierigkeiten haben, ihre Agenda voranzutreiben.

Eine gute Wahl muss unabhängig von irrelevanten Alternativen sein und die Tür zum Dilemma der nützlichen Stimmabgabe schließen.

Beispiel für eine mögliche alternative Wahl :

Zustimmungswahl: Jeder Wähler kann so vielen Kandidaten zustimmen, wie er möchte, ohne dass es eine Reihenfolge der Präferenzen gibt. Der Kandidat mit den meisten Zustimmungen ist gewählt.

Die Wahl nach vergebenen Punkten (oder gewichtete Wahl) ist ein alternatives Wahlsystem, bei dem jeder Wähler eine bestimmte Anzahl von Punkten hat, die er auf die Kandidaten verteilen kann.

Die Wahl nach Mehrheitsurteilen: Die Wähler geben jedem Kandidaten eine Note, z. B. von "sehr gut" bis "abzulehnen". Der Kandidat mit der höchsten Durchschnittsnote ist gewählt.

Die Condorcet-Wahl ist ein Wahlsystem, bei dem die Wähler ihre Stimme abgeben, indem sie alle Kandidaten paarweise vergleichen.[^12].

Randomisierte Condorcet-Wahl: Hier vergleicht jeder Wähler Kandidatenpaare wie bei der Condorcet-Wahl, aber anstatt die Stimmen aller Wähler zu berücksichtigen, wird eine zufällige Anzahl von Vergleichen ausgewählt, um den Gewinner zu ermitteln.

Die Mehestan-Wahl ( Tournesol): Dies ist ein Wahlsystem, das die Punktwahl und die Condorcet-Wahl kombiniert. Sie schlägt vor, jeden Kandidaten einen nach dem anderen zu vergleichen und zu sagen, welchen man für jedes Kriterium bevorzugen würde. Diese Abstimmungsmethode soll sowohl fair als auch repräsentativ sein, indem sie es den Wählern ermöglicht, für ihren bevorzugten Kandidaten zu stimmen, ohne dass die Gefahr besteht, dass die Stimme geteilt wird, und indem die Präferenzen aller Wähler bei der Bestimmung des Gewinners berücksichtigt werden.

### Politische Parteien

Diese oben gesehenen Wahlmodi ermutigen Politiker, sich aufgrund der Eigenschaft, von irrelevanten Alternativen abhängig zu sein, in Form von Parteien zu organisieren.

### Institutionelle Langsamkeit

In vielen Fällen erfordern politische Probleme schnelle und effektive Lösungen, um den Schaden und die Verluste zu minimieren. Langsame politische Prozesse wie Verhandlungen, Debatten, Abstimmungen und Bürokratie können jedoch die Entscheidungsfindung verzögern und die Regierungen daran hindern, schnell zu handeln.

### Die kurzfristigen Mandate

Die betreffenden Abgeordneten sind für eine bestimmte Zeit gewählt, in der Regel für einige Jahre. Während dieser Zeit verfügen sie über eine erhebliche persönliche Freiheit bei der Wahl ihres Handelns. Die Wähler zwingen sie zu nichts mehr. Es gibt nur sehr wenig, was die Gewählten dazu zwingt, ihre Wahlversprechen einzuhalten, abgesehen von ihrer Ehre.

Gewählte Politiker sind oft eher geneigt, sich auf kurzfristige Ergebnisse zu konzentrieren als auf langfristige Herausforderungen.

Denn diese Mandate ermutigen die Gewählten, sich auf politische Themen zu konzentrieren, die eher schnelle Unterstützung von den Wählern erhalten, als auf komplexere Fragen, die ein größeres und langfristigeres Engagement erfordern.

Politiker können versucht sein, Entscheidungen zu treffen, die kurzfristig populär sind, um ihre Chancen auf eine Wiederwahl zu maximieren. Dies kann zu politischen Maßnahmen führen, die langfristig nicht tragfähig oder nachhaltig sind und in der Zukunft größere Probleme verursachen können. Dies führt zu einem politisierenden Ansatz des Regierens anstatt zu einem pragmatischeren und durchdachteren Ansatz.

### Der Bluff zum Fachwissen

Der "Expertise-Bluff" in der Politik tritt auf, wenn Politiker ihre Position mithilfe eines Abschlusses in Wissenschaft oder Verwaltung ausnutzen, um den Eindruck zu erwecken, dass sie über Expertise oder besondere Kenntnisse zu Themen verfügen, obwohl sie dies in Wirklichkeit nicht haben. Sie können diese Strategie nutzen, um die Wähler zu manipulieren oder zu überzeugen, aber das kann zu falschen politischen Entscheidungen führen.

Die Aufwertung von Abschlüssen in Wissenschaft oder Verwaltung führt zu einer Konzentration von Macht und Einfluss in den Händen einiger weniger Eliten, die das Glück hatten, Zugang zu diesen Bildungsgängen zu erhalten. Dies führt zu einem Verlust an Vielfalt und Inklusion in den Entscheidungszirkeln, da Menschen aus weniger privilegierten Verhältnissen oft weniger Möglichkeiten haben, diese Abschlüsse zu erwerben.

Dabei ist der Besitz eines Abschlusses nicht unbedingt eine Garantie für Kompetenzen oder Fachwissen in den Bereichen Regierungsführung oder Politik. Der bereits erwähnte Bluff mit Fachwissen kann von Personen eingesetzt werden, die zwar einen Abschluss besitzen, denen es aber an tatsächlichem Wissen oder Fähigkeiten mangelt, um fundierte Entscheidungen zu treffen.[^2].

### Die Wissenschaft mit Füßen getreten

Politische Entscheidungen müssen oft auf der Grundlage komplexer wissenschaftlicher Daten und empirischer Beweise getroffen werden, insbesondere in Bereichen wie öffentliche Gesundheit, Klimawandel, Lebensmittelsicherheit usw. Wenn politische Entscheidungsträger wissenschaftliche Beweise missachten oder ignorieren, treffen sie Entscheidungen, die nicht auf überprüfbaren Fakten und Daten beruhen, sondern auf Überzeugungen oder Meinungen. Dies kann schwerwiegende Folgen für die Gesellschaft haben, z. B. in den Bereichen Gesundheit, Umwelt oder Sicherheit.

### Propaganda

Der Wähler gibt seine Stimme nicht für einen Kandidaten ab, über den er nichts weiß. Ohne Informationen über ihn, die den Wählern übermittelt werden, hat ein Kandidat keine Chance. Seine Chancen, gewählt zu werden, steigen mit zunehmender Information.

Politisches Investment ist eine Investition wie jede andere, das Ziel ist immer das gleiche: Profit.

Die Geschichte der Etablierung der "Wahldemokratie" verläuft parallel zu den Kämpfen und formalen Siegen für die Meinungs-, Presse-, Versammlungs- und Vereinsfreiheit. Doch diese Freiheiten wurden aufgrund ihrer notwendigen materiellen und wirtschaftlichen Grundlage im Wesentlichen von denjenigen vereinnahmt, die diese wirtschaftlichen Mittel besitzen. Und sie so für Wahlpropaganda zu nutzen.[^24].

Propaganda wird eingesetzt, um die öffentliche Meinung zu manipulieren und die Ergebnisse von Wahlen oder Volksabstimmungen zugunsten einer bestimmten Partei oder Ideologie zu beeinflussen. Diese kann irreführend und parteiisch sein und auf Lügen, Halbwahrheiten oder der Auslassung wichtiger Fakten beruhen. Sie kann auch mit den Ängsten, Vorurteilen und Emotionen der Menschen spielen, anstatt mit Vernunft und Logik.

### Der politische Hooligan

Es gibt drei karikaturistische Typen von Bürgern bei Wahlen:[^13][^14].

- Hobbits: Diejenigen, die sich nicht für Politik interessieren und in diesem Bereich sehr unwissend sind, sind nicht allzu parteiisch.
- Hooligans: Sind voreingenommen, aber sehr voreingenommen, oft Opfer des confirmation bias, und verteidigen Werte auf irrationale Weise.
- Vulcans: Eine Person, die in der Lage ist, rational zu denken.

Unsere Modelle gehen davon aus, dass die Bürger Vulkane sind oder werden können, aber in der Praxis sind die Menschen überwiegend entweder Hobbits oder Hooligans.

Die Intuition zwingt uns zu einer Meinung, die Vernunft ist dann ein Werkzeug, um diesen Meinungen Recht zu geben. Wir rationalisieren unsere Überzeugungen.

Wir benutzen unseren Verstand nicht, um die Wahrheit zu ergründen, sondern um die ideologischen Positionen zu rechtfertigen, die wir bereits eingenommen haben.

Wenn wir zum Beispiel das Wort "Demokratie" nennen, lässt uns unsere Intuition sagen, dass es etwas Gutes ist, und dann manipuliert unsere Vernunft später die Definition des Wortes, um unsere Intuition zu rechtfertigen.

Und wenn diese Argumente oder Zahlen nicht in unserem Sinne sind, können wir immer noch die Beweggründe des Berichterstatters in Frage stellen oder die Fakten auf andere Weise interpretieren.

Wenn wir uns mit Gleichgesinnten umgeben, bestätigt unser Umfeld unsere Intuition und polarisiert sie.

Politische Aktivisten werden dadurch übermäßig selbstbewusst und können selbst mit Fakten und Zahlen nicht mehr von ihrer Meinung abgebracht werden.

Und wenn die Intuition auf diese Weise vorherrscht, finden wir, je mehr wir uns informieren, immer mehr gute Gründe, das zu glauben, was wir glauben, egal welche Informationen wir lesen.

Politisches Engagement verstärkt dann nur noch den politischen Hooliganismus.

Je mehr wir uns für eine politische Partei mit einem Parteipreis interessieren, desto radikaler wird unsere ursprüngliche Neigung werden.

Irreführende, aber gut erklärte wissenschaftliche Videos und Artikel bestärken diejenigen, die sie anschauen und lesen, in ihrer wissenschaftlichen Überzeugung.

Da die Gruppenauslese effektiver ist als die individuelle Auslese, überlebten nur Stämme, in denen die Individuen ausreichend für das Überleben der Gruppe zusammenarbeiteten.

Und damit auch diejenigen, die bereit sind, ihre individuellen Interessen zum Wohle der Gruppe zu opfern. Die natürliche Auslese hat den politischen Hooliganismus selektiert.

Eine Abstimmung, die auf politischem Hooliganismus beruht, verschafft Genuss, während eine rationale Abstimmung geistige Anstrengung erfordert.

### Die Unwissenheit und Irrationalität des Bürgers

Die Menschen sind ignorant, fast alle Wähler haben keine Ahnung von Wissenschaft oder verstehen das politische System nicht, sind schlecht informiert und kennen die Zahlen nicht.[^16].

Einige von ihnen sind sich nicht bewusst, dass sie sich in einem politischen Hooliganismus befinden.

Die meisten von uns kennen die verschiedenen kognitiven Verzerrungen und Auswirkungen nicht, mit denen wir konfrontiert sind. Wir haben eine Reihe von ihnen in Kapitel 3 über HomoSapiens behandelt. Hier sind einige davon zur Erinnerung:

- Bestätigungsbias[^17]
- Der Halo-Effekt[^18].
- Kognitive Dissonanz[^19].
- Paardruck
- Verfügbarkeitseristik[^20]

Schule und Hochschulabsolventen können Menschen dazu bringen, sich als Gelehrte zu fühlen, sie glauben zu lassen, dass sie intelligenter sind als andere, und ein Übermaß an Vertrauen in Bereiche zu schaffen, die nicht ihre eigenen sind.

Am Ende ist jeder so selbstsicher. Und das Problem ist, dass Unsicherheit als Zeichen von Schwäche gesehen wird, obwohl sie eine grundlegende Eigenschaft ist.

Der Dunning-Kruger-Effekt zeigt, dass Menschen mit wenig Wissen oder Fähigkeiten in einem Bereich dazu neigen, ihr eigenes Niveau der Meisterschaft zu überschätzen, während Menschen mit höheren Fähigkeiten dazu neigen, ihr Niveau der Meisterschaft zu unterschätzen.[^16].

Die Unwissenheit der Bürger ist jedoch nicht das größte Problem, denn selbst die Information der Bürger hält sie nicht davon ab, irrational abzustimmen.

Die Menschen verhalten sich wie Hooligans und nicht wie Wissenschaftler.

### Zeit und Energie, um sich zu informieren

Sich zu informieren, zu lernen, zu wissen, zu verstehen und dann zu wählen, zu entscheiden und zu wählen, erfordert vom Wähler Zeit, Energie und Vorwissen, das wiederum Zeit und Mühe kostet. Diese Kosten erklären eine gewisse Delegation von Macht.

Wahlkämpfe sind oft kurz und intensiv und lassen den Wählern wenig Zeit, sich zu informieren und die verschiedenen Optionen zu bewerten. Außerdem neigen die Medien oft dazu, sich auf die spektakulärsten oder umstrittensten Aspekte politischer Ereignisse zu konzentrieren, anstatt eine gründliche und differenzierte Analyse zu liefern.

Dies kann zu einer Polarisierung der Politik und zu einer Verringerung der Qualität der öffentlichen Debatte führen. Die Wähler könnten versucht sein, sich an vereinfachende oder parteiische Informationsquellen zu wenden, die nicht unbedingt die Komplexität der Themen widerspiegeln.

### Kommunitarismus

Kommunitarismus kann als eine Tendenz definiert werden, der Zugehörigkeit zu einer Gruppe oder Gemeinschaft mehr Bedeutung beizumessen als dem Individuum an sich. Das Hauptproblem des Kommunitarismus ist, dass er zu einer Zersplitterung der Gesellschaft führen kann, bei der sich jede Gruppe eher auf ihre eigenen Interessen als auf das gemeinsame Interesse der Gesellschaft als Ganzes konzentriert.[^21].

Kommunitarismus konnte geografisch sein, doch nun erlischt dieser im gesamten Internet.

Niemand gehört gerne zu einer Minderheit.

Wenn ein Internetnutzer aufhört, sich an einer Diskussion zu beteiligen, weil er sich zu sehr als Minderheit fühlt, führt dies zu ideologischer Segregation

Im Internet das ideologische Viertel zu wechseln ist viel einfacher als ein geografischer Umzug.

Menschen können Gemeinschaften verlassen, in denen sie sich als Minderheit fühlen, und sich einer Gemeinschaft anschließen, in der sie zur Mehrheit gehören.

Filter bubbel sind sogenannte ideologische Isolationen im Internet.

Wir schließen uns in einem Universum ein, in dem alles, was wir lesen und hören, das bestätigt, was wir bereits denken. Das erschwert den Prozess, unsere Überzeugungen zu hinterfragen und zu verstehen, warum andere denken, was sie denken.

Diese Filterblasen werden durch die personalisierten Inhalte der Internetriesen Facebook und YouTube noch verstärkt. Sie schließen uns in die Gemeinschaft ein, mit der wir unsere Gedanken teilen, und liefern uns Inhalte, die auf uns zugeschnitten sind. Jedes Mal, wenn wir auf einen Link klicken oder einen Beitrag liken, schließen uns diese immer mehr in eine Gemeinschaft ein, mit der wir unsere Ideen teilen.[^22].

### Die Überzeugungen

Überzeugungen können als tiefe Überzeugungen oder fest verankerte Meinungen zu einem Thema definiert werden. Obwohl Überzeugungen förderlich sein können, sind sie in der Regel eher problematisch als alles andere, insbesondere im Bereich der Politik.

Eines der Hauptprobleme im Zusammenhang mit Überzeugungen ist, dass sie zu Polarisierung und Spaltung führen können. Wenn Menschen feste Überzeugungen zu einem Thema haben, sind sie möglicherweise weniger geneigt, andere Standpunkte in Betracht zu ziehen oder mit Menschen zusammenzuarbeiten, die andere Überzeugungen haben. Dies kann zu einer Polarisierung und Spaltung in der Gesellschaft führen.[^23].

Sie können zu einer Verschlossenheit führen. Wenn Menschen zu sehr an ihren Überzeugungen festhalten, sind sie möglicherweise weniger geneigt, Meinungen oder Fakten anzuhören, die ihren Überzeugungen widersprechen. Dies kann dazu führen, dass Entscheidungen eher auf Vorurteilen als auf Fakten beruhen,

Überzeugungen können zu Widerstand gegen Veränderungen führen. Wenn Menschen fest an ihren Überzeugungen festhalten, sind sie möglicherweise weniger bereit, neue Ideen oder Veränderungen in der Gesellschaft zu akzeptieren.

## Lösungen für die Probleme unserer Demokratien

### Der politische Unternehmer

Die Lösung für den politischen Unternehmer besteht darin, diese Funktion abzuschaffen oder zumindest durch eine Erhöhung der Bürgerbeteiligung einzuschränken.

Nicht, dass der politische Unternehmer zu jemandem wird, dem man Macht überträgt und der von dem Moment an, in dem er sie erlangt, tun und lassen kann, was er will. Vielmehr sollte er eine repräsentative Funktion haben, und sobald die Personen, die ihm Macht übertragen, der Meinung sind, dass er sie nicht mehr repräsentiert, sollten sie die Möglichkeit haben, ihn abzuberufen.

Es muss eine vollständige Transparenz geschaffen werden, damit die Menschen genau wissen, wem sie Macht übertragen und was sie damit tun.

Und für eine gute Vielfalt muss sichergestellt werden, dass die Kandidaten tatsächlich aus verschiedenen Bereichen kommen und die gleichen Chancen wie alle anderen haben, diese Positionen zu erreichen.

### Politische Programme

Eine Lösung für das Problem der politischen Programme könnte darin bestehen, sie modularer und themenspezifischer zu gestalten, anstatt sie als Gesamtpaket zu präsentieren. Dies würde es den Wählern ermöglichen, die Vorschläge, die sie zu jedem Thema bevorzugen, separat auszuwählen, anstatt das gesamte Programm annehmen oder ablehnen zu müssen.

### Ehrlichkeit und Transparenz

In einer Demokratie ist es wichtig, dass die Entscheidungsfindung transparent und rechenschaftspflichtig ist und dass die Personen an der Macht direkt für ihre Handlungen zur Verantwortung gezogen werden. Die Delegation von Macht muss mit Bedacht eingesetzt werden und die damit verbundenen Risiken müssen berücksichtigt werden, um sicherzustellen, dass die Interessen der Bürger geschützt werden.

### Die Wahl der Alternativen

Es ist wichtig, dem Wähler eine umfassende und breite Auswahl an Alternativen zu bieten, sowie die gleiche Sichtbarkeit und Menge an Informationen über diese. Globale Programme schränken diese Möglichkeit wieder ein, Themen könnten getrennt werden, um spezifischere Entscheidungen zu ermöglichen.

### Delegation von Macht

Der Wähler sollte die Möglichkeit haben, zu entscheiden, ob er seine Macht delegieren möchte oder nicht. Der Wähler kann jederzeit die Übertragung seiner Macht zurücknehmen.

### Die Zentralisierung der Macht

In einer Demokratie ist die Verteilung der Macht entscheidend, um sicherzustellen, dass die Interessen und Meinungen der Bürger vertreten und bei der Entscheidungsfindung berücksichtigt werden. Dies beinhaltet die aktive Beteiligung der Bürger, die Dezentralisierung der Macht und die Schaffung von Mechanismen zur Rechenschaftslegung und Transparenz, um die Macht der gewählten Führungspersonen zu begrenzen.

### Die Wahlmodi

Es sollten Wahlgänge gefördert werden, die die Intensität der Präferenzen der Bürger berücksichtigen, und solche vermieden werden, die das nützliche Wählen fördern.

### Die institutionelle Langsamkeit

Für Notfälle sollten schnelle Referendumsmöglichkeiten geschaffen werden können. Und generell soll es den Bürgern ermöglicht werden, regelmäßiger abzustimmen.

### Kurzfristige Mandate

Es gibt mehrere Möglichkeiten, darunter die Einführung längerer Amtszeiten mit einem Verfahren, das den Wählern die permanente Möglichkeit gibt, ihre gewählten Vertreter abzuberufen. Man kann auch das Bewusstsein der Bürger für langfristiges Denken schärfen und die Politiker dazu motivieren, langfristig zu planen. Man kann auch die Amtszeit begrenzen oder die Funktion des politischen Unternehmers abschaffen.

### Bluffs mit Fachwissen

Es ist wichtig, anzuerkennen, dass Fähigkeiten und Fachwissen nicht ausschließlich auf den Erwerb von Abschlüssen zurückzuführen sind und dass die Einbeziehung von Menschen mit unterschiedlichen Hintergründen einen Reichtum an Perspektiven mit sich bringen kann, der für das Regieren und die Politik von Vorteil sein kann.

### Die verhöhnte Wissenschaft

Wissenschaftler in Entscheidungsprozesse einbeziehen: Wissenschaftler können dazu beitragen, politische Entscheidungen zu informieren, indem sie unabhängige und evidenzbasierte Daten, Analysen und Bewertungen bereitstellen.

Transparenz und Rechenschaftspflicht über die wissenschaftlichen Daten und Beweise, die sie für politische Entscheidungen heranziehen, stärken und für ihre Entscheidungen und deren Folgen verantwortlich sein. Eine effektive Kommunikation zwischen Wissenschaftlern und politischen Entscheidungsträgern fördern, ihre Ergebnisse klar und prägnant kommunizieren und Fachjargon vermeiden. Eine Kultur des Respekts für die Wissenschaft fördern. Politische Entscheidungsträger über Wissenschaft aufklären: Politiker sollten in wissenschaftlichen Methoden und wissenschaftlichen Schlüsselkonzepten geschult werden, um wissenschaftliche Daten und Beweise besser verstehen zu können.

Einführung von Prinzipien in eine Verfassung, die berücksichtigt werden müssen, damit Entscheidungen unumstößliche Prinzipien respektieren, die einen Konsens gefunden haben, wie z. B. das menschliche Glück und ein Gleichgewicht in den Ökosystemen der Erde zu gewährleisten.

### Propaganda

Um die Auswirkungen von Propaganda in der Politik zu verringern, ist es wichtig, freie und transparente Informationen zu fördern, die auf überprüfbaren Fakten und Daten beruhen. Die Medien müssen in diesem Prozess eine Schlüsselrolle spielen, indem sie eine ausgewogene und unparteiische Berichterstattung über politische Ereignisse liefern, politische Behauptungen faktenchecken und die unabhängige Überprüfung von Fakten fördern. Außerdem ist es wichtig, die Öffentlichkeit für die Techniken der Propaganda und Meinungsmanipulation zu sensibilisieren und sie darüber aufzuklären, wie sie diese Taktiken erkennen und ihnen widerstehen können.

### Die Unwissenheit der Bürger

Eine qualitativ hochwertige Bildung kann den Bürgern helfen, zu sachkundigeren Wählern zu werden. Es ist wichtig, den Schülern Fähigkeiten wie kritisches Denken, logisches Denken, die Suche nach verlässlichen Informationen und das Verständnis für politische Themen zu vermitteln.

Transparenz und Zugänglichkeit: Öffentliche Politik und Entscheidungen müssen transparent und leicht zugänglich sein, damit die Bürger besser verstehen können, wie ihre Regierung funktioniert und welche Entscheidungen getroffen werden.

Kommunikation: Es ist wichtig, dass politische Führer klar und regelmäßig mit den Bürgern kommunizieren, eine einfache Sprache verwenden und Fachjargon vermeiden.

Partizipation: Die Förderung der Beteiligung der Bürger an politischen Prozessen wie Wahlen, Petitionen, öffentliche Debatten, Konsultationen, Meinungsumfragen usw. kann dazu beitragen, dass sich die Bürger stärker am politischen Leben beteiligen und ein besseres Verständnis für politische Themen entwickeln.

### Zeitmangel

Das Zeitproblem lässt sich lösen, indem man den Bürgern durch Arbeitszeitverkürzung mehr Zeit einräumt. Eine andere Möglichkeit ist auch, einen Algorithmus zu haben, der jedes Mal einen repräsentativen Querschnitt der Gesellschaft genau auswählt, damit nicht jeder zur Wahl gehen muss.

### Der politische Hooligan

Darauf achten, die Programme ohne politischen Hooliganismus zu lesen, sie mit Forschungen in Wirtschaft und Soziologie kreuzen. Sich fragen, ob wir nicht unserer Intuition folgen, die unser Verstand dann mit Argumenten untermauert hätte.

Sich dieses Hooliganismus bewusst werden, um zu besseren Debatten zu führen, eine Denkweise einzubringen, die nicht an der Intuition klebt und die es uns ermöglicht, aus unserer Denkweise als politischer Hooligan auszubrechen. Wir müssen alles schätzen, was kontraintuitiv ist, es schätzen, wenn wir unsere Intuition überzeugen, dass unsere Intuition falsch liegt.

Man muss die Tatsache schätzen, dass man falsch liegt, dass man sich selbst in Frage stellen kann, dass man versteht, dass man die größten Fortschritte machen kann, wenn man seine Intuition entblockt.

### Der Kommunitarismus

Man muss gegen den Wunsch jedes Einzelnen kämpfen, Teil der Mehrheit zu sein.
Dann muss man dafür kämpfen, dass es nicht zu einer peinlichen und unangenehmen Erfahrung wird, zur Minderheit zu gehören.

Man muss darauf achten, dass man der Mehrheit nicht Recht gibt, nur weil sie die Mehrheit ist.

Man muss die Erfahrung, in der Minderheit zu sein, angenehm machen.

Menschen, die in der Mehrheit sind, sollen Lust darauf bekommen, sich in der Minderheit wiederzufinden.

Jeder soll versuchen, zu verstehen und sich einzumischen.

### Die Überzeugungen

In einer Demokratie ist es wichtig, dass die Menschen aufgeschlossen und bereit sind, sich die Meinungen und Ansichten anderer anzuhören. Die Bürger müssen diesbezüglich sensibilisiert und erzogen werden.

### Schlussfolgerung der Lösungen

Es gibt viele Probleme, die der Demokratie innewohnen.

Damit sie gut funktioniert, braucht sie ideale Bedingungen.

Gute Medien, um gut informiert zu sein und damit die Politiker an der Macht wirklich im Interesse der Bürger handeln.

Das Problem ist jedoch, dass unsere Demokratie einen Kontext schafft, in dem genau das Gegenteil passiert.

Die Menschen sind entweder Hobbits, die sich nicht für Politik interessieren, sehr ignorant in diesem Bereich sind und nicht allzu parteiisch sind, oder sie sind Hooligans, die parteiisch sind, aber sehr parteiisch, und oft Opfer des confirmation bias, sie gehen sogar so weit, Werte auf irrationale Weise zu verteidigen.

Was wir brauchen, sind Vulcans, Menschen, die rational denken können.

Und derzeit kann man unter politischen Menschen in einer Demokratie mit gewählten Vertretern, die gewählt werden wollen, nicht rational diskutieren.

Wenn wir eine radikale Veränderung der Demokratie wollen, müssen wir alle diese rationalen Elemente wie das Prinzip der politischen Hooligans, die Segregation der Gemeinschaft usw. in Betracht ziehen.

Aber die Demokratie ist eine Wette, die man eingehen sollte, denn mit ihr fühlen wir uns alle gleich, sie bietet moralisches Kapital und vereint uns zur Zusammenarbeit.

## Experimentelle Idee für eine neue, tiefe Demokratie (V1)

Name: Gemeinsame partizipative E-Demokratie durch spontane Referenden, unterstützt durch Fachwissen und Weisheit.

Um eine breitere und direktere Beteiligung der Bürger zu ermöglichen, nutzen wir Informations- und Kommunikationstechnologien, um ein digitales Werkzeug zu schaffen, das demokratische Prozesse erleichtern und verbessern wird.

Dieses Werkzeug ist eine webbasierte Plattform, die auch als mobile Anwendung verfügbar ist. Sie können sie sich als Wiki vorstellen, über das Sie auf Informationen zugreifen können, die mit den verschiedenen Schichten der Gesellschaft, an denen Sie beteiligt sind, in Verbindung stehen, wie Gesetze, Organisationsformen, aktuelle Entscheidungen etc. Diese Schichten werden in Kategorien und Unterkategorien unterteilt und strukturiert, soweit dies für die jeweilige Schicht erforderlich ist.

Jeder hat die Möglichkeit, Änderungen an jeder eingetragenen Information vorzuschlagen. Da es für jeden Menschen unmöglich ist, alle Vorschläge zu lesen, die alle 7 Milliarden Menschen machen könnten. Wir verwenden einen Algorithmus, der eine Stichprobe nimmt, die alle Menschen repräsentiert, die mit dem Vorschlag in Verbindung stehen. Der Vorschlag wird dann an diese repräsentative Stichprobe gesendet. Die Menschen können für, gegen oder mit Enthaltung stimmen. Wenn mehr als 50% dafür sind, kommt der Vorschlag auf die nächste Ebene.

Der Vorschlag wird dann an ein Konsortium von Experten und Weisen weitergeleitet, die den Vorschlag prüfen, um ihre Meinung zu äußern, den technischen und wissenschaftlichen Aspekt zu prüfen, die Ressourcen und die Zeit abzuschätzen und vor allem, um zu prüfen, ob er mit den bestehenden Verfassungen übereinstimmt.

Die Verfassung von Eutopia wird die Grundlage der Gesellschaft bilden, die untersucht und begründet werden muss, um unser gemeinsames Ziel, ein erfülltes und nachhaltiges Leben in Harmonie mit der Erde zu führen, zu erreichen. Sie muss dann gelehrt werden, um das Bewusstsein der Menschen dafür zu schärfen und sicherzustellen, dass sie ihre Vorschläge und Entscheidungen danach ausrichten.

Sobald die Arbeit der Experten und Weisen abgeschlossen ist, werden ihre Stellungnahmen dem endgültigen Vorschlag beigefügt, damit die Bürger alle Schlüssel in der Hand haben, um ihre Wahl zu treffen. Diese Menschen, die zu Experten oder Weisen ernannt werden, werden von den Bürgern in ihrem jeweiligen Bereich gewählt. Die Informationen in diesen Stellungnahmen müssen transparent und wahrheitsgetreu sein, aber auch einfach gehalten werden, damit der Bürger alle Karten in der Hand hat, um seine Wahl zu treffen und seine Macht nach bestem Wissen und Gewissen ausüben kann.

Der Vorschlag wird dann zusammen mit den Informationen und Meinungen der Experten und Weisen erneut an eine Stichprobe geschickt. Wenn der Vorschlag erneut angenommen wird, wird er zur endgültigen Verabschiedung an die gesamte Bevölkerung gesendet. Das Ergebnis sollte dann das gleiche sein wie das der Stichprobe. Wenn der Bürger jedoch den Vorschlag zur endgültigen Abstimmung erhält, ist ihm bereits bewusst, dass mehr als die Mehrheit ihn annehmen wird.

Der Bürger erhält also nicht alle Vorschläge, sondern nur die, die ihn betreffen, je nachdem, ob die Entscheidung eher lokal oder global umgesetzt wird.

Bevor Sie Ihren Vorschlag einreichen, richten wir ein kollaboratives System ein, dessen Nutzung dringend empfohlen wird. Ihr Vorschlag ist dann zunächst ein Vorschlag, diese Vorschläge sind für alle zugänglich, die dies wünschen, und alle können daran teilnehmen.

Sie haben auch die Möglichkeit, Ihre Macht an jemanden zu delegieren. Diese Person stimmt dann für Sie über den Vorschlag ab, Sie haben jedoch Zugang zur Historie der abgestimmten Positionen, um zu überprüfen, ob Sie damit einverstanden sind, und Sie können Ihre Machtdelegation jederzeit zurückziehen. Sie können auch wählen, welche Ebene der Macht in Verbindung mit den verschiedenen Schichten der Gesellschaft Sie delegieren möchten.

Schließlich ist es dieses demokratische System, das wir auch für die kollaborative Bearbeitung von Erzählungen verwenden werden. Oder früher: Wenn wir unser System testen, können wir darüber iterieren und es verändern, bis wir vollends zufrieden sind, und schließlich wird es diese endgültige Version sein, die die Demokratie von Eutopia darstellt.

> "Demokratie dieser Horizont scheint Zeit, Mut, Ausdauer ... und vielleicht ein Körnchen Verrücktheit ... zu erfordern". Datageueule[^4]

## Quellen (Nicht erschöpfende Liste)

[^1]: [Wikipedia: Demokratie ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. Sind Wahlen die Demokratie? 1977
[^3]: David Shearman. The Climate Change Challenge and the Failure of Democracy (Politics and the Environment). 2007.
[^4]: [Datageule. Democracy(s)?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All: Das Grundprinzip der Politik](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory. 1656.
[^8]: [Science4All: Hasset das Spiel, nicht die Spieler. Demokratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Ehrlichkeit fördern | Demokratie 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja und Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail. 2015
[^11]: [Science4All. Unsere Demokratien spalten | Demokratie 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. Die randomisierte Condorcet-Wahl | Demokratie 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. Sind Sie ein politischer Hooligan? Demokratie 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[^15]: [Science4All. Rational irrational | Demokratie 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies. 2008.
[^17]: [La Tronche en Biais #5: The Confirmation Bias](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [Erstaunliche Wissenschaft - Der Halo-Effekt - Hirnficker! #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [The Trunk in Bias #3- Cognitive Dissonance](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Franklin Templeton Academy - Verfügbarkeitsschranke](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. Kleiner Kommunitarismus wird groß werden | Demokratie 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [TED-Konferenz. Eli Pariser warnt vor Online-Filterblasen](https://www.youtube.com/watch?v=B8ofWFx525s).
[^23]: [Science4All. Liebe Überzeugung, mutiere zu einer VIRALEN Infektion!!! Demokratie 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Medienkonzentrationen: die Milliardäre informieren Sie. 2016
