---
title: Alle zusammenarbeiten
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## Was ist Wirtschaft?

Die Wirtschaft ist die menschliche Tätigkeit, die sich mit der Produktion, der Verteilung, dem Austausch und dem Verbrauch von Gütern und Dienstleistungen befasst[^2].

Ein Wirtschaftssystem produziert jedoch nicht nur Waren und Dienstleistungen, sondern auch Menschen und die Beziehungen zwischen ihnen. Die Art und Weise, wie die Gesellschaft produziert und konsumiert, hat einen großen Einfluss auf Persönlichkeiten, Charaktere, Wissen, Wünsche, Glück und die Art der zwischenmenschlichen Beziehungen.

Manchmal neigt man dazu, die Gesellschaft ausschließlich nach den Mengen der verschiedenen Waren und Dienstleistungen zu beurteilen, die ihre Mitglieder haben. Aber die Menschen schätzen nicht nur diese Güter und Dienstleistungen, sondern auch die Gefühle, Einstellungen und zwischenmenschlichen Beziehungen, die mit diesen Transfers einhergehen und die zeigen, was Menschen sind. Diese Aspekte werden in der Wirtschaftswissenschaft allzu oft vernachlässigt.[^3].

Ein Wirtschaftssystem formt und prägt die Menschen und ihre Beziehungen durch ihre Produktions- und Konsumtätigkeiten. Die Gesellschaft sekretiert das Wirtschaftssystem, das wiederum die Art und Qualität der Beziehungen zwischen Menschen und ihrer Psyche tiefgreifend beeinflusst.

Bei einem Tausch erhalten Geber und Empfänger immer mehr als das, was sie einander konkret geben, denn sie geben einander zusätzlich die Möglichkeit, in eine menschliche Beziehung zu treten.

Wir haben uns daran gewöhnt, der Wirtschaftswissenschaft zu folgen, als sei sie eine einfache, unumstößliche Naturtatsache, die vorschreibt, wie die Welt sein sollte, ohne sie wirklich tiefgreifend zu hinterfragen. Wir sind von der Welt, in der wir leben, geprägt, aber sobald wir verstehen, was ein Wirtschaftssystem ist, werden wir fähig, uns vorzustellen, wie unsere Welt nach anderen Regeln funktionieren würde.

In diesen Tagen passiert etwas Neues, denn zu dem Elend in der Welt und der Herrschaft, die wir übereinander haben, kommt der Klimawandel hinzu, und der wird uns das Ende unseres Wirtschaftswachstums bringen. [^4]

Jedes Jahr liefern uns die Medien und die Klimaforschungsorganisationen ein Datum. Das Datum, an dem Wissenschaftler schätzen, dass die Menschheit mehr Ressourcen verbraucht hat, als die Erde im selben Zeitraum produzieren kann.

Diese Zahl ist das Ergebnis von Experten aus verschiedenen Bereichen, die eine Bilanz ziehen, wie viele Ressourcen wir dem Planeten entnehmen und wie schnell diese Ressourcen wieder aufgefüllt werden.

Dieses Datum wird als "Tag der Überschreitung" bezeichnet. Damit eine Wirtschaft nachhaltig sein kann, müsste dieser Tag am 31. Dezember liegen und nicht früher. In diesem Fall können sich die Ressourcen dann mit der gleichen Rate erneuern, mit der sie verbraucht werden.

Für 2022 wird dieses Datum auf den 22. Juli geschätzt.[^1].

Dieses Datum sollte uns helfen, unser Wirtschaftssystem mit mehr Klarheit zu betrachten, uns seiner Grundlagen im Menschen und seiner Laster bewusst zu werden.

Insbesondere, dass die weitere Förderung eines Wirtschaftssystems, das unsere großen primären Verstärker (= Verlangen nach Sex, Nahrung, Information) fördert, wohl das Schlimmste ist, was wir tun können.[^5].

Wirtschaftstheorien schaffen es nicht, Argumente vorzubringen, die wirklich intellektuelle Zustimmung finden, was ihre Schwäche zeigt. Das liegt daran, dass sie oft auf unserer kollektiven Vorstellungskraft beruhen und keine reale Grundlage haben.

Vielleicht ist die Wirtschaftswissenschaft also philosophischer, als sie gerne glauben machen möchte, und weniger wissenschaftlich, als sie gerne glauben machen möchte.

Gibt es also andere Möglichkeiten für ein Wirtschaftssystem?

## Der Plan und der Markt

Es gibt derzeit 2 Wirtschaftssysteme, die die moderne Welt beherrschen, den Markt und die Planung.[^6].

Jede Seite verteidigt ihr System mit einer sozialen Ethik und greift die andere für die Verwirklichung eines anderen an.

Für den Markt ist dieser Wert die Freiheit und für den Plan ist dieser Wert die Gleichheit.

Auf dem Markt sind wir weit davon entfernt, wirklich frei zu sein, aber wir sind freier als auf dem Plan.

Und im Plan sind wir sehr weit davon entfernt, wirklich gleich zu sein, aber wir sind gleicher als auf dem Markt, dafür sind wir am schlimmsten, wenn es um Freiheit geht.

In der Realität sieht es so aus, dass jedes dieser Systeme im Bereich des Wertes, den es verteidigt, schlecht abschneidet.

Diese 2 Wirtschaftssysteme auf ihrem Höhepunkt wurden zum kapitalistischen Markt und zum totalitären Kommunismus, die beide übelriechende und entwürdigende menschliche Eigenschaften und Beziehungen hervorbringen und verstärken, wie Egoismus, andere wie eine Sache zu behandeln, Feindseligkeit, Konflikte, Konkurrenz zwischen Menschen, Dominanz, Ausbeutung, Entfremdung....

Keines dieser beiden Wirtschaftssysteme hat es geschafft, die schönsten Eigenschaften des Menschen hervorzuheben. Sie unterdrücken sogar eine davon stark. In beiden Systemen herrscht eine bestimmte Form der Herrschaft.

## Die 3. Ökonomie: Reziprozität

Gibt es also eine andere Alternative?

Es gibt Literatur, die mit dem Satz "Es gibt drei Arten von Wirtschaftssystemen" beginnt. Diese Literatur muss jedoch an den historischen oder disziplinären Rändern des ökonomischen Denkens gesucht werden, entweder bei Ökonomen von vor mehreren Generationen oder in der Wirtschaftsanthropologie.

So zum Beispiel der Ökonom Karl Polanyi (1886-1964), der in seinen Werken die Wirtschaftssysteme aller Gesellschaften, einschließlich vergangener, primitiver und traditioneller Gesellschaften, in drei Kategorien einteilt. Er weist darauf hin, dass sie in ein und derselben Gesellschaft nebeneinander existieren können, wobei er jedoch darauf hinweist, dass jede Gesellschaft einem der beiden Systeme eine bevorzugte/dominante/prinzipielle Stellung einräumt. Er unterscheidet zunächst ein Wirtschaftssystem, das der Markt ist. Sein zweites System nennt er das Umverteilungssystem, das in seiner ausgearbeiteten und modernen Form die Planung ist, bei der ein politisches Zentrum über die wirtschaftliche Zuteilung entscheidet. Das dritte System nennt Polanyi die "Reziprozität", gemäß einer bereits etablierten anthropologischen Tradition, die sogar ziemlich klar und präzise ist. [^6]

Diese Art von Wirtschaft wissen wir nicht, dass es sie gibt, dennoch existiert sie sehr wohl innerhalb unserer Gesellschaften. Die Gesellschaften enthalten also praktisch alle drei Wirtschaftssysteme, aber in allen ist eines der Systeme wichtiger als die anderen beiden.

Hier ist ein Dreieck, das verschiedenen großen Systemen im Jahr 1984 entspricht:
[!Schema der drei Wirtschaftsformen](./../../../GlobalProject/Image/Markt.png)]

Die Ökonomen haben die Wirtschaft und ihr Denken in der Annahme aufgebaut, dass der Mensch das ist und schon immer war, was man als "homo oeconomicus" bezeichnen würde, und sie haben eine Karikatur davon geschaffen, als ein egoistisches, berechnendes und asoziales Wesen. Aber der Mensch war vor allem das, was man den "homo sociabilis" nennen würde, der universell Reziprozität praktiziert und universell einen Kreis von Verwandten oder eine Gemeinschaft braucht.[^9].

Die Reziprozität ist der Mechanismus der Gemeinschaftswirtschaft, der die Anfänge der Menschheit als Jäger und Sammler und später als sesshafte Landwirte nährte.

Die Reziprozität ist immer noch unter uns, das innerfamiliäre Wirtschaftsleben beruht zunächst auf dieser Wirtschaft. Sie bieten einander Dienste an, ohne eine Gegenleistung zu erwarten, z. B. bezahlen Sie ein Familienmitglied nicht dafür, dass es Sie von der Schule abholt, und diese Person wird auch nicht von einer externen Instanz gezwungen. Die Gegenseitigkeit in unserer Gesellschaft hat in den letzten Jahrzehnten drastisch abgenommen, da der Markt versucht, soziale Interaktionen immer mehr zu monetarisieren, um eben einen neuen Markt zu schaffen. Neben der Familie gibt es jedoch noch viel Reziprozität in Freundschaften, Beziehungen und Liebesbeziehungen, durch Freiwilligenarbeit, ehrenamtliche Tätigkeiten und Gemeinschaften, aber auch in verschiedenen Bereichen, wie z. B. in der Welt der Informatik durch Opensource und zahlreiche Plattformen, auf denen Informationen, Errungenschaften, Gegenstände usw. geteilt werden können.

Eine lange Reihe von regelmäßigen, verfolgten und etablierten gegenseitigen Geschenken zwischen zwei Personen wird als Reziprozität bezeichnet. Die allgemeine Reziprozität ist das Äquivalent, aber von jedem. Jeder gibt der Gesellschaft und umgekehrt erhält sie von allen anderen.

Altruismus und Geben können aufgrund ihrer Natur weder durch Zwang noch durch Tausch im engeren Sinne erreicht werden.

Das ist der Unterschied zum Plan: dass diese Transfers dem Geber nicht von einer sozialen Entität außerhalb seiner selbst aufgezwungen und aufgezwungen werden. Und der Unterschied zum Markt besteht darin, dass es sich auch nicht um Tauschgeschäfte im strengen Sinne handelt, bei denen jeder Transakteur ausschließlich daran interessiert ist, mehr oder nicht weniger von den Gütern oder Dienstleistungen zu erhalten, die er überträgt.

In diesem sozialen System gibt niemand einem spezifischen anderen, daher ist es unmöglich, zu dominieren, zu zwingen, zu erniedrigen oder zu einer Gegengabe zu zwingen und auf diese Weise wirtschaftlich oder ethisch auszubeuten. Jeder, der allen etwas gibt, gibt niemandem etwas.

In diesem System kommt die Arbeit jedes Einzelnen dem Konsum vieler anderer zugute, oft erst viel später. Und jeder profitiert von der Arbeit einer großen Anzahl anderer, die oft viel früher entstanden ist. Jeder erhält sogar teilweise von verstorbenen Generationen und arbeitet für ungeborene Generationen.

Der sozialethische Wert der Reziprozität könnte als "Brüderlichkeit" bezeichnet werden. Wir haben hier die berühmte republikanische Trilogie, die Brüderlichkeit wird definiert als ""Tue anderen nicht an, was du nicht möchtest, dass man dir antut; tue anderen ständig das Gute, das du von ihnen empfangen möchtest.""

Doch hier endet der Vergleich mit der Trilogie, denn die Reziprozität löscht den Wunsch und den eigentlichen Sinn der beiden anderen Werte aus. Denn in diesem System, in dem sich jeder um jeden anderen wie um sich selbst kümmert, stehen sich die Willen und damit die persönlichen Freiheiten nicht mehr entgegen, und Forderungen nach Gleichheit haben keine Grundlage mehr.

## Vorteil der Gegenseitigkeit

Gegenseitigkeit beruht auf Güte.

Güte bedeutet, Altruismus, Liebe, freiwillige Solidarität, gegenseitiges Geben, Großzügigkeit, Teilen, freie Gemeinschaft, Nächstenliebe und Barmherzigkeit, Wohlwollen und Freundschaft, Sympathie und Mitgefühl zu betonen. Jeder hat davon zu wenig und findet, dass es unter allen mehr davon geben sollte. Alle unsere großen Familien des Geistes, des Denkens und der Gefühle berufen sich auf sie. Aber in unserem Wirtschaftsleben werden sie verleugnet. Dabei dominieren sie das Leben in unseren Gesellschaften, aber man stellt fest, dass das am weitesten verbreitete Bestreben am seltensten verwirklicht wird.

Die allgemeine Gegenseitigkeit ist die Versöhnung zwischen unserer altruistischen Ethik und unserer wissenschaftlichen Technik, zwischen unseren Bestrebungen nach herzlicher Gemeinschaft, individueller Freiheit und materiellem Fortschritt.

Darüber hinaus bringt die Gegenseitigkeit zahlreiche Vorteile für die wirtschaftliche Produktion mit sich. Denn sie bietet ein effizientes, kohärentes System, das die globale Arbeitszeit drastisch reduzieren wird.

## Ist Reziprozität möglich?

Während wir uns intensiv mit Märkten und Planung befasst haben, haben wir kaum etwas über die dritte Form des Transfers von Gütern und Dienstleistungen, das Schenken, und nichts über die Gruppe der Schenkungen, die Reziprozität, gesagt.

Im Laufe der Geschichte sind immer wieder neue Wirtschaftssysteme entstanden, und jedes Mal gab es Menschen, vielleicht sogar die Mehrheit, die glaubten und behaupteten, dass die möglichen Systeme diejenigen sind, die man damals beobachten konnte oder die es in der Vergangenheit gegeben hat. Doch diese Überzeugungen werden immer wieder durch die Geschichte, durch die Existenz neuer Überzeugungen widerlegt. Und das ist höchstwahrscheinlich auch jetzt noch der Fall, denn es gibt keinen Beweis dafür, dass eines der Systeme, die existieren oder existiert haben, das beste der möglichen Systeme ist. In der Tat, ja, Wirtschaftssysteme, die man nie beobachtet hat, sind möglich.

Eine Firma, die zu günstige Preise anbietet, geht bankrott, und ein Einzelner kann nicht geben, was er in einer Gesellschaft produziert, in der ihm niemand etwas gibt.

Sozialpsychologen haben lange Zeit das "Hilfeverhalten" untersucht, mit sehr interessanten Ergebnissen: Man neigt nicht nur dazu, dem zu helfen, der einem geholfen hat, sondern man neigt auch dazu, mehr zu helfen, wenn man selbst Hilfe erhalten hat, d. h. man neigt dazu, Fremden zu helfen, wenn man selbst von Fremden geholfen wurde. Dies ist das Wesen der Reziprozität. [^13]

Und das ist es, was wir in Kapitel 3 über Homo Sapiens gesehen haben, dass es möglich ist, altruistische Wesen zu erschaffen, denn auch Altruismus wird durch Freude motiviert, und es sind vor allem die Umwelt und die Erziehung, die dazu beitragen, Werte und Motivationen wie Großzügigkeit, Mitgefühl und den Wunsch, anderen zu dienen, zu entwickeln.

Aber ist es trotzdem möglich, dass eine Gesellschaft eine Wirtschaft hat, die auf Reziprozität und insbesondere auf allgemeiner Reziprozität beruht?

Es gibt ganze Gesellschaften, innerhalb derer die gesamten Wirtschaftsbeziehungen hauptsächlich oder sogar ausschließlich auf Gegenseitigkeit beruhen. Aber diese Untergesellschaften oder Gesellschaften sind sogenannte "face-to-face groups", deren Mitglieder sich gegenseitig kennen, Familien, Dörfer, Banden usw., und die Reziprozität ist in ihnen nicht wesentlich allgemein. Hier war es noch einfach, sie anzuwenden, aber eine Person kann nicht mehr als hundert andere kennen, und daher kann man sich berechtigterweise fragen, ob dies in großem Maßstab wirklich möglich ist.[^14].

Denn obwohl die Wirtschaft das Problem der Produktion und Verteilung von Waren und Dienstleistungen löst, steht jedes Wirtschaftssystem (einschließlich der Reziprozität) vor zwei grundlegenden Problemen für die Produktion: dem Informationsproblem und dem Motivationsproblem.

## Das Informationsproblem

Das Informationsproblem besteht darin, dass der Arbeitnehmer oder der Anbieter von Ressourcen oder Dienstleistungen darüber informiert wird, was er tun muss, um seine Bedürfnisse und Wünsche bestmöglich zu erfüllen.

Und die Motivation ist diejenige, die dazu führt, diese Arbeit zu verrichten oder diese Ressourcen auf diese Weise zu nutzen.

Plan und Markt lösen diese beiden Probleme mehr oder weniger gut.

Der Markt löst das Informationsproblem durch die dezentralen Mechanismen des Preissystems, die "Indikatoren für Knappheit und Nutzen" sind: Angebot und Nachfrage.

Der Plan löst das Problem durch die Organisation dieser Informationen, d. h. durch Sammlung, Berechnung, Anweisungen oder Prognosen, und zwar auf relativ zentrale Weise.

Der Markt löst das Motivationsproblem, indem er durch den Anreiz des Tausches auf den Egoismus zurückwirft.

Der Plan löst das Problem durch Zwang, Drohung oder Bestrafung, Belohnung oder Beförderung, aber also allgemeiner durch die Autorität und ihre verschiedenen Mittel des Zwangs oder der Belohnung und der mehr oder weniger starken Verinnerlichung der Notwendigkeit des Gehorsams aus Gewohnheit oder Pflichtgefühl.

Die gleichen Probleme gelten für die Reziprozität, woher sollen die Menschen wissen, was andere von ihnen brauchen, und werden sie den anderen genug geben wollen?

Diese beiden Probleme verstärken sich gegenseitig positiv, die Lösung des einen fördert die Lösung des anderen und umgekehrt.

Je besser das Motivationsproblem gelöst ist, desto besser kann auch das Informationsproblem gelöst werden, da die Menschen sich dann leichter gegenseitig die benötigten Informationen weitergeben.

Je besser das Informationsproblem gelöst ist, desto produktiver ist das Wirtschaftssystem, d. h. es gibt weniger Verschwendung, desto weniger Arbeit ist für eine bestimmte Endleistung erforderlich und desto weniger Arbeit muss das Anreizsystem bereitstellen bzw. desto weniger gravierend ist seine Unvollkommenheit.

Im Gegenzug betrachtet jeder die Bedürfnisse oder Wünsche anderer als ebenso wichtig wie seine eigenen, und dies bestimmt, was er von seiner Arbeit (Zeit, Anstrengung, Mühe) und seinen Besitztümern, unabhängig von deren Herkunft, gibt.

Wenn das Motivationsproblem gelöst ist, dann ist auch das Informationsproblem gelöst, denn die Menschen können die Wirtschaft immer noch mindestens so effizient machen wie mit dem Markt oder dem Plan, indem sie die wirtschaftlichen Informationen, die in diesen Systemen zirkulieren, freiwillig an sich selbst weitergeben.

Die Informatik hat diesen Bereich revolutioniert, da wir nun alle miteinander verbunden sind und durch die Entwicklung geeigneter Anwendungen die Übertragung von Informationen einfacher als je zuvor wird.

Das Übertragen und Verarbeiten von Informationen ist neben anderen Arbeiten ein Teil der Arbeit für die Gesellschaft. Im Gegenzug treibt die Motivation dazu, das Beste für die Allgemeinheit zu tun. Dieses System kann also alles tun, was auch der Markt oder der Plan tut.

Darüber hinaus verringert die Reziprozität den Informationsbedarf erheblich. Über die Senkung des Wettbewerbskonsums oder des protzigen Konsums, die Verringerung der extrinsischen Bedürfnisse und die Verringerung der Produktion der entsprechenden Güter oder Dienstleistungen, die Verringerung des Erneuerungsbedarfs der Produkte aufgrund ihrer auf Nachhaltigkeit basierenden Konzeption und die Verringerung zahlreicher Dienstleistungen, die für das Funktionieren der Wirtschaft nicht mehr notwendig sind.

## Das Motivationsproblem

Wenn man von der "menschlichen Natur" spricht, kann man hören "Der Mensch wird sich nie ändern, er war schon immer egoistisch usw." und dann kommen die Vorstellungen von der "wahren" menschlichen Natur, die letztendlich die Oberhand gewinnen sollte. Man muss unbedingt historisches, anthropologisches, psychologisches, analytisches Wissen und sogar einfache Logik diesen naiven Überzeugungen entgegensetzen, auf "der Mensch wird sich nie ändern" kann man genauso gut antworten, dass er sich nur geändert hat.[^15][^16][^17]

Auf "Der Mensch war schon immer und ist überall egoistisch" kann man ebenfalls sehr gut mit der Frage kontern, was man darüber weiß, was er "schon immer war".

Die Menschheit befindet sich in einer Transformation, die in ihrer Existenz in ihrer Tiefe und natürlich auch in ihrem Ausmaß beispiellos ist.

Wie wir gesehen haben, gibt es keine menschliche Natur und der Mensch kann alles werden, was er werden will.

Hier sind verschiedene Punkte, die den Zweifel am Problem der Motivation beantworten:

- Menschen haben eine Vorliebe für altruistische und gebende Beziehungen[^18].
- Alle großen religiösen und säkularen Ethiken befürworten Altruismus und Geben sehr stark. [^20]
- Die Hauptmotivation des Menschen ist es, die Zustimmung anderer Menschen zu erhalten: In einem reziproken System ist die Zustimmung natürlich universell und reziprok.[^19].
- Empirische Analysen des "Hilfeverhaltens" belegen, dass Menschen anderen eher helfen, wenn ihnen selbst geholfen wurde.
- Soziologen haben die Ansicht vertreten, dass das Hauptmerkmal des Menschen die Nachahmung ist[^22].
- Die Gewohnheit, diese Nachahmung von sich selbst, ist ein weiteres menschliches Phänomen von größter Bedeutung.[^23].
- Studien über die Entwicklung von Kindern zeigen, dass wir anfangs altruistisch sind und dann von der Gesellschaft zu Egoisten gemacht werden.
- Erziehung: Wie wir gesehen haben, ist der Mensch letztlich nur ein Blatt Papier, das von der Gesellschaft beschrieben wird[^24].
- Soziale Verstärkung: Je mehr Menschen eine Einstellung zur Gegenseitigkeit haben, desto mehr zeigen sie durch ihre Taten und Worte, dass es normal und gut ist, sie zu haben, und das spornt andere an, dies auch zu tun. Die Erziehung und die Gesamtheit der sozialen Beziehungen spielen somit eine große Rolle. Wenn der Mensch in erster Linie kulturell geprägt und damit formbar ist, kann die Gesellschaft ihn ebenso zum Menschen der Gegenseitigkeit machen wie zum Egoisten des Marktes oder zum Untergebenen oder Hierarchen des Plans[^25].
- Je mehr Reziprozität es gibt, desto mehr sieht man, dass sie praktikabel ist, und desto mehr erkennt man ihre Vorteile, und daher akzeptiert man sie, desto mehr "glaubt" man an sie, desto mehr wünscht man sie sich, desto mehr drängt man auf sie. Und damit auch, je mehr sie dazu tendiert, sich zu verbreiten.

- Wenn einige Menschen, die dazu in der Lage sind, dennoch nicht teilnehmen möchten, ist das nicht schlimm, denn wenn es solche Fälle gibt, ist es keine Verachtung, die man ihnen entgegenbringen sollte, denn ein solches Gefühl würde auf ein Missbefinden in der Gesellschaft hindeuten: Aber selbst wenn die Person nicht teilnehmen möchte, ist das nicht schlimm, denn wir werden nicht nur feststellen, dass die Arbeitsbelastung pro Einwohner erheblich reduziert wird, sondern auch, dass dies durch die Tatsache ausgeglichen werden kann, dass andere gerne "mehr" geben, als sie erhalten, und zwar aus Altruismus, wie wir gesehen haben, aber auch aus eigenem Interesse an der Arbeit.

Denn schließlich ist es in unserer heutigen Gesellschaft so, dass wir überhaupt arbeiten.

- Direkter Zwang: Zwangsarbeit
- Durch Tausch: Man verkauft seine Arbeit gegen einen Lohn.
- Aus dem Wunsch heraus, anderen oder der Gemeinschaft zu helfen, "einen Dienst zu erweisen".
- Aus Pflichtgefühl, eventuell kann es sein, dass man den anderen etwas zurückgibt für das, was man von ihnen erhält.
- Aus Interesse an der Tätigkeit dieser Arbeit selbst.
- Um soziale Kontakte, Beziehungen und eine Eingliederung in die Gesellschaft zu finden, indem man einen Beitrag zum Arbeitsumfeld leistet.
- Um anderen zu zeigen, dass man für sie arbeitet, schöne Dinge tut oder geschickt ist.
- Für den sozialen Status, der mit dieser Arbeit verbunden ist (Beitrag, Verantwortung etc.).
- Um sich zu beschäftigen, "etwas zu tun" zu haben, sich nicht zu langweilen oder müßig zu sein.
- Durch Spiel
- Aus Gewohnheit

Man könnte noch mehr hinzufügen, aber hier wird deutlich, wie wichtig die Beschäftigung an sich und die sozialen Beziehungen sind, die wir durch die Arbeit herstellen können, was man übrigens auch an den Gefühlen nach der "Verrentung" erkennen kann. Im Allgemeinen zieht der Mensch mehr Sinn aus seinem Leben aus seiner produktiven Tätigkeit als aus seiner konsumtiven Tätigkeit. Und alle diese Motivationen mit Ausnahme der ersten beiden können in einem System der Gegenseitigkeit zur Arbeit anregen, man muss also nur mit ihnen spielen, indem man den Sinn und das Interesse an der Arbeit steigert.

Die Arbeit ist nicht dazu verurteilt, die Folter ihres umstrittenen etymologischen Ursprungs aus dem Wort travalhum: tripalium, ein Folterinstrument mit einem Dreifuß...[^26] zu sein.

Alle Dimensionen der Schwere der Arbeit müssen somit ausgelöscht oder reduziert werden. Physisch oder psychisch erschöpfende, abstumpfende oder verdummende, zu harte, mechanische, monotone, zu schmutzige oder unangenehme, zu bedeutungslose, verachtenswerte usw. Arbeiten sind zu vermeiden. Müssen abgeschafft, ersetzt, eingeschränkt, automatisiert, verbessert, bereichert, entschädigt, belohnt, geschätzt oder besser geteilt werden.

Die eigentliche Idee besteht darin, das Wesen der Arbeit so umzugestalten, dass sie zu einer Tätigkeit wird, die so etwas wie Selbstverwirklichung ist, die Manifestation der eigenen tiefen Menschlichkeit durch die Umwandlung der Natur und die Erhaltung der Natur. In diesem Sinne würde die Arbeit zum "ersten Bedürfnis des Lebens" werden. So würde der Ausdruck "arbeiten, um seinen Lebensunterhalt zu verdienen", nicht mehr bedeuten, die Mühen der Arbeit zu erdulden, um zu überleben und seine Bedürfnisse zu befriedigen, sondern sehr wohl seinen Lebensunterhalt durch eine sinn- und erlebnisreiche Tätigkeit zu verdienen.

## Der Platz des Geldes in der Gegenseitigkeit

Wir sind so sehr an ihn gewöhnt, dass wir alle glauben, Geld sei eine notwendige Bedingung des gesellschaftlichen Lebens, doch Geld ist keine Naturgegebenheit, sondern eine imaginäre Erfindung des Menschen, die man kritisieren kann.[^30].

Die Funktionsweise des Geldes erklärt nicht seine Entstehung, sie erklärt sich nicht von selbst, und das ist auch der Grund, warum Ökonomen nichts erklären. Und sie erklären sich auch nicht aus existenziellen Gründen.

Das Geld soll angeblich erfunden worden sein, um das durch den Tauschhandel entstandene Problem der doppelten Übereinstimmung der Bedürfnisse zu lösen, denn ohne Geld wäre es zu schwierig, den Handel zu Ende zu führen. Wie kann man Güter miteinander vergleichen, und was mache ich, wenn ich Ihren Schuh brauche, Sie aber meine Äpfel nicht?

Trotz zahlreicher anthropologischer Untersuchungen, die das Gegenteil beweisen, ist dieser Mythos einer der am besten verankerten in der Vorstellung des Einzelnen von der Wirtschaft.

In primitiven Volkswirtschaften nahmen sie einfach das, was sie brauchten, und aus sozialer Sicht teilten sie ihre Ressourcen. Ohne die Besessenheit von Knappheit, die unsere Marktwirtschaften kennzeichnet, konnten die primitiven Volkswirtschaften systematisch auf Überfluss setzen.[^7][^8].

Einige anthropologische Forschungen, die sich enger auf historische Elemente und empirische Befunde stützen, führen zu der Annahme, dass Geld weder durch Arbeit noch durch Tausch entsteht, sondern historisch und konzeptionell zunächst eine Möglichkeit ist, eine soziale Bindung zwischen Individuen herzustellen, da es das Maß für die zwischen ihnen eingegangenen Schulden darstellt.[^12].

Einige Zivilisationen, wie die Inkas, kannten übrigens weder Geld, noch Markt, noch Handel. [^10]

Das Problem mit dem Geld ist, dass das Kapital mit uns spielen kann, solange wir es haben müssen, um Zugang zum Austausch von Waren und Dienstleistungen zu haben.

Der Kapitalismus sichert seine Herrschaft also nicht dadurch, dass er die Arbeit in der Hand hat, sondern dadurch, dass er die Bedingungen der Sozialität beherrscht, die die Arbeit notwendig machen und zu denen unter anderem die Notwendigkeit des Geldes gehört.[^3].

Geld markiert das Vertrauen der Menschen untereinander. Denn es bedarf eines unerschütterlichen Vertrauens, um von anderen ein Geldzeichen als Gegenleistung für eine erbrachte Leistung oder ein geliefertes Gut anzunehmen: Vertrauen darauf, dass es sich nicht um eine falsche Banknote oder falsches Geld handelt, Vertrauen in die Zukunft, die dieses Geldzeichen repräsentiert, da es zukünftige Pläne ermöglicht. Letztendlich ist Geld der Horizont der Reziprozität über die Zeit hinweg, nicht unbedingt gegenüber demjenigen, der mir das Geld gegeben hat, sondern Vertrauen darauf, dass es in der Zukunft jemanden geben wird, der mir den Wert der Waren und Dienstleistungen, die ich vorübergehend für Geld verkauft habe, in Form von Waren und Dienstleistungen zurückgeben kann.

Denn warum sollte ich ein Geldzeichen verwenden, wenn ich dem anderen wirklich vertraue? Der Gegenstand selbst, das Konzept des Geldes, spiegelt in Wirklichkeit den Mangel an Vertrauen in den anderen wider, während, wenn dieses Vertrauen hergestellt ist, sein Nutzen verschwindet.

Wenn der andere wirklich Vertrauen in mich und die Gesellschaft hat, kann er mir etwas geben, ohne dafür Geld zu verlangen, weil er weiß, dass ich oder jemand anderes ihm morgen etwas geben würde.

Die durch Geld vermittelte Tauschbeziehung ist eine Beziehung, die auf der Vorstellung beruht, dass die Beteiligten kein Vertrauen in die Dauer ihrer Beziehung haben können: Sie verwenden das Geld in Wirklichkeit, um den Tausch zu Ende zu führen, um die Schuld zu begleichen, kurz gesagt, um nach Abschluss der Transaktion nichts mehr miteinander zu tun zu haben.

Die aktuelle politische Diskussion in unseren Volkswirtschaften dreht sich um die Frage, wie man dieses Geld verwenden kann, damit es richtig funktioniert, aber vielleicht kann Geld einfach nicht richtig funktionieren, vielleicht ist es dazu bestimmt, soziale Beziehungen zu pervertieren?

Wenn man ihm dennoch eine Natur verleihen müsste, wäre es besser zu sagen, dass Geld in seiner derzeitigen Form als Tauschmittel und nicht als "Ermöglicher der Zirkularisierung von Waren und Dienstleistungen" dazu bestimmt ist, soziale Beziehungen zu pervertieren.

Geld stößt in jede Lücke, die die Sinnlosigkeit hinterlässt, es hilft uns, die Kontrolle zu behalten, um unseren cingulären Cortex zu beruhigen, und die Angst vor dem Tod wird zum Motor der Gier.[^11].

Viele Analysen stellen zwar die negativen Auswirkungen des Einflusses des Geldes in der Gesellschaft fest, aber es gelingt ihnen nicht, eine angemessene Antwort auf das Problem der Monetisierung des Realen vorzuschlagen.

In einer monetarisierten Gesellschaft wird der Wert der Dinge zu dem Wert, den das Geld ihnen verleiht.

Das Geld abzuschaffen scheint uns unmöglich, so sehr ist uns die Vorstellung, dass Geld natürlich geworden ist. Die Monetisierung der Gesellschaft scheint für ihn tatsächlich immer mehr zu einer Tatsache zu werden, außerhalb derer es nicht einmal möglich ist, zu denken.

Das Problem ist, dass es nicht mehr das Individuum ist, das sein Wertesystem und damit das, wonach es strebt, bestimmt, sondern dass dieses Wertesystem im Geld objektiviert wird.

Doch diese Distanzierung vom Geld ermöglicht es, ein Gefühl der Freiheit von den Werten der Märkte wiederzuerlangen, indem wir uns unsere Werte wieder aneignen.

Wenn wir jedoch weiterhin glauben, dass das Geld in seiner gegenwärtigen Form die einzige Möglichkeit ist, die Frage des Güteraustauschs und damit die Frage der Begegnung zwischen den Menschen zu lösen, mit anderen Worten, wenn wir glauben, dass das Geld eine Voraussetzung für die Sozialität ist, werden wir große Schwierigkeiten haben, den wirtschaftlichen Wert von den ethischen und politischen Werten zu unterscheiden.

Es sind unsere westlichen Gesellschaften, die in jüngster Zeit den Menschen zum "Wirtschaftstier" gemacht haben. Aber noch sind nicht alle von uns ein solches Wesen. Der homo oeconomicus steht nicht hinter uns, er steht vor uns; wie der Mensch der Moral und der Pflicht; wie der Mensch der Wissenschaft und der Vernunft. Der Mensch war sehr lange etwas anderes; und es ist noch nicht sehr lange her, dass er eine komplizierte Maschine mit einer Rechenmaschine war.

Wir versuchen hier, anderen Facetten der Menschheit als der wirtschaftlichen Beziehung des Güteraustauschs einen Wert zu verleihen.

Wert, der es allen ermöglicht, sich in einer ständigen Begegnung mit anderen in einer Sozialität zu entfalten, die auf den Prinzipien des Teilens und der gegenseitigen Hilfe beruht.

> "Manche Menschen sehen die Dinge, wie sie sind, und fragen "Warum"? Ich träume von Dingen, die nie waren und frage "Warum nicht?". George Benard Shaw

## Vergleich mit aktuellen Ideologien

Diese Darstellung hier führt unweigerlich zur Schaffung einer neuen Ideologie. Wir sind der Meinung, dass es wichtig ist, die bestehenden Ideologien zu vergleichen und zu erklären, warum sie anders sind und warum diese anderen Ideologien nicht nachhaltig oder effektiv sind.

Aber es geht nicht darum, diese Ideologien abzulehnen, denn die Werte hinter der Linken, Gleichheit, und der Rechten, Freiheit, sind schön und notwendig für ein Eutopia, aber es geht jetzt darum, das Richtige daraus zu sammeln. Um uns gemeinsam zu versammeln.

Unsere Rede wird daher nicht einfach nur "Ihr habt alle Unrecht" lauten, sondern "Ihr habt alle teilweise Recht".

Daher werden wir nicht versuchen, mit bereits bestehenden Bewegungen in Konfrontation zu treten. Stattdessen ziehen wir es vor, freundschaftliche Beziehungen zu pflegen, Offenheit zu schaffen und andere bei Projekten zu unterstützen, die mit den Werten, die wir vertreten, in Einklang stehen. Die Idee ist, den anderen für das zu respektieren, was er glaubt, um zu verlangen, dass er uns für das respektieren kann, was wir ebenfalls glauben. Wir glauben, dass die Wahrheiten irgendwann von selbst ans Licht kommen werden und dass wir uns nicht in unseren Überlegungen stören lassen sollten, da wir sonst Gefahr laufen, dass sich jeder in sich selbst verschließt. Wir glauben, dass wir nur so aus dem Abgrund, in dem wir uns befinden, herauskommen können, indem wir trotz unserer unterschiedlichen Meinungen zusammenbleiben, vereint im gemeinsamen Streben nach Selbstverwirklichung.

### Vergleich mit der Linken

Diese ideologischen Systeme, die sich für mehr Planung oder sogar totale Planung aussprechen, ähneln am ehesten unserem Eutopia, doch nur wenige Worte können das praktische System, das daraus hervorgeht, völlig verändern.

Sozialismus = "Jedem nach seinen Fähigkeiten und jedem nach seiner Arbeit".
Kommunismus = "Jeder nach seinen Fähigkeiten und jedem nach seinen Bedürfnissen"[^27].
Reziprozität = "Von jedem freiwillig nach den Bedürfnissen des anderen"[^3].

Die Unzulänglichkeiten und Mängel dieser anderen Ideologien sind zum Teil die wesentlichen Ursachen für das, was die Welt heute ist. Sie haben Abweichungen hervorgerufen, antikapitalistische Revolutionen, aber sie sind durch diese mehr oder weniger gescheitert, sie haben auf einem großen Teil des Globus Gesellschaften neuen Typs geschaffen. Und da das Ergebnis nicht offensichtlich besser ist als der entwickelte Kapitalismus, haben sie diesen auf diese Weise geschützt. Nun werden sie von allen Seiten dazu benutzt, die gigantischste Täuschung und Heuchelei der Geschichte zu bilden.

Die Gleichheit zwischen Menschen, wie auch immer sie definiert ist, kann eine Einschränkung der Freiheit bestimmter Menschen sein. Und Freiheit allein gewährleistet in der Regel keine Gleichheit.

Die beste aller Gesellschaften unterscheidet sich wahrscheinlich sehr stark von den uns bekannten Gesellschaften wie unserer Klassengesellschaft aus Herrschenden und Beherrschten. Diejenigen, die versucht haben, sie neu zu erfinden, scheitern jedoch zunächst einmal daran, dass sie die Gesellschaften, die sie vorschlagen, nicht ausreichend spezifizieren.

Wenn dieser analytische Ansatz notwendig war, muss er heute überwunden werden, um die Arbeit der genetischen Denaturalisierung fortzusetzen, die insbesondere von dem berühmten Karl Marx begonnen wurde.[^28].

Das Marxsche System kann übrigens weder vor noch nach dem Kapitalismus über Geld nachdenken. Es wirft nicht nur zweitausend Jahre Menschheitsgeschichte in den Schatten, sondern kann auch keine Lösung anbieten, um aus dem Kapitalismus herauszukommen.

Die Schwierigkeiten des Marxschen Denkens liegen in der Möglichkeit, eine andere Form der Organisation der Arbeit und der wirtschaftlichen Sozialität als die der kapitalistischen Produktionsweise zu denken.

So haben marxistische Bewegungen, die 150 Jahre alte Theorien mit sich herumschleppen, im Zeitalter der Noosphäre große Schwierigkeiten, sich selbstverwaltete horizontale Arbeitsorganisationen neu vorzustellen, unsere demokratischen Prozesse neu zu erfinden, sich eine andere Welt als die des Produktivismus vorzustellen, das tiefe Wesen, das wir sind, und die Mittel, es zu verändern, zu verstehen. Sie kämpfen dann viel zu oft GEGEN den Kapitalismus und haben Schwierigkeiten, echte Alternativen zu bieten, für die wir FÜR kämpfen könnten.

Wir kritisieren hier nicht das meisterhafte Werk von Marx, das zum Nachdenken durchaus notwendig war, aber wir glauben, dass, wenn er aufrichtig zugegeben hätte, dass er auch Ethik betreibt und viel mehr davon in seinen Werken untergebracht hätte, wenn er mehr über die endgültige oder gute Gesellschaft und die Wege dorthin gesagt hätte, wenn er so viel Altruismus in seine Eigenschaften gelegt hätte, wie er in sich selbst hatte, vielleicht weniger vom Schlimmsten passiert wäre.[^3].

### Vergleich mit der Rechten

Rechte Politik, d. h. Politik, die den Markt oder sogar den totalen freien Markt fördert, ist die vorherrschende ideologische Bewegung in den westlichen Ländern, zumindest diejenige, die an der Macht ist, und sie ist sogar die vorherrschende ideologische Bewegung in der Welt und diejenige, die sie am meisten beeinflusst.

Jeder nimmt die negativen Auswirkungen wahr, nicht nur auf den Planeten und unsere Ökosysteme, sondern auch auf unsere allgemeine Entfaltung. Kurz zusammengefasst: In diesem Wirtschaftssystem verbringen wir unsere Zeit damit, sie zu verschwenden, und wir verschwenden unser Leben damit, es zu "verdienen". Und diejenigen, die ihre Zeit nicht damit verschwenden, sie zu verlieren, egal ob sie es tun oder nicht, ernten dennoch die katastrophalen Folgen, und wenn nicht kurzfristig, dann langfristig.

Auf dem Markt haben die Menschen immer Ketten. Aber für die meisten sind es keine groben Ketten aus rostigem Stahl mehr, sondern "fein vernickelt", viel "standing" und letztlich noch schwerer zu zerreißen, zumal manche auch noch Goldglieder hineinschieben. Wir haben dann das Gefühl, dass wir etwas Wertvolles zu verlieren haben.

Unser System drängt jeden Einzelnen durch die Gesamtheit der anderen in die Enge, indem eine Person einfach nicht allein und frei ihre besten Eigenschaften zeigen kann. In der Wirtschaft kann ein Unternehmen in einem wettbewerbsorientierten System bei Strafe des Bankrotts nicht nicht versuchen, seinen Gewinn zu maximieren, wenn andere versuchen, ihren Gewinn zu maximieren. Ebenso kann jemand, der gibt, in einer Gesellschaft, in der andere nehmen, nicht überleben.

Es gibt Gutes in diesem System, wie die unternehmerische Freiheit und die "Freiheit" im Allgemeinen, die dort herrscht, und man kann die Wirksamkeit nicht leugnen, die es für viele Fortschritte hatte. Aber man kann auch nicht die Folgen leugnen, die es hatte, denn wo sich einige wiederfanden, fanden sich andere nicht wieder... diese wurden vernachlässigt, ausgebeutet, vergessen.... Und es ist höchste Zeit, einen Schritt weiterzugehen und endlich alle einzubeziehen.

An dieser Stelle soll diese erste Version unserer Analyse in Bezug auf den Vergleich mit der Rechten enden. Nicht, dass es nicht viel zu sagen gäbe, im Gegenteil, es gibt eine riesige Menge an Literatur über den Kapitalismus, in der seine Ursprünge, Ursachen und Folgen ausführlich beschrieben werden. Aber wie wir diese Ideologien betrachten wollen, wie wir mit ihnen umgehen wollen und schließlich alle Fragen im Zusammenhang mit dem Engagement unserer Bewegung in der konkreten Politik müssen noch kollektiv festgelegt werden. Wenn Sie jedoch der Meinung sind, dass Sie nicht genug darüber wissen, empfehlen wir Ihnen, die zahlreichen Ressourcen in der Literatur und im Internet zu nutzen.[^29][^31]

## Quellen (Nicht erschöpfende Liste)

[^1]: Earth Overshoot day 2017 fell on August 2, Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipedia : Wirtschaft](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_human)>)
[^3]: Serge-Christophe Kolm. Die gute Wirtschaft. 1984
[^4]: Das Ende des Wachstums. Richard Heinberg. 2013
[^5]: Sebastian Bohler. Der menschliche Bug. 2020
[^6]: Karl Polanyi: "The great transformation" und "The economy as Instituted Process", in: Trade and Market in the Early Empires: Economies in History and Theory.
[^7]: Marshall Sahlins - Stone-age Economics.
[^8]: Marcel Mauss- Versuch über die Gabe
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. Der Homo sociabilis. 2019
[^10]: [Soziale Organisation der Inkas](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Wo ist der Sinn? 2020
[^12]: David Graeber. Schulden: 5000 Jahre Geschichte. 2011
[^13]: Lee Alan Dugatkin. The Altruism Equation: Seven Scientists Search for the Origins of Goodness (Die Altruismusgleichung: Sieben Wissenschaftler suchen nach den Ursprüngen der Güte). 2006
[^14]: [Dunbar-Zahl](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Eine kurze Geschichte der Menschheit.
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[^17]: Daniel Kahneman. Thinking, Fast and Slow.
[^18]: [Ernst Fehr, Die Natur des menschlichen Altruismus](https://www.nature.com/articles/nature02043)
[^19]: [The Need for Social Approval](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religion and Morality](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruism in children and the perceived conduct of others in Journal of Abnormal and social Psychology, 1942, 37, S. 2018-233.
[^22]: Gabriel Tarde. Die Gesetze der Nachahmung, 1890.
[^23]: Charles Duhigg. The Power of Habit. 2014
[^24]: Émile Durkheim. Erziehung und Soziologie; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORY OF NORMATIVE CONDUCT: A THEORETICAL REFINEMENT AND REEVALUATION OF THE ROLE OF NORMS IN HUMAN BEHAVIOR.
[^26]: [Ist die Etymologie des Wortes Arbeit tripalium?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Kritik des Gothaer Programms (1875). 1891
[^28]: Karl Marx. Das Kapital. 1867
[^29]: [Vortrag: Kommunismus & Kapitalismus: Die Geschichte hinter diesen Ideologien](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. Die Ordnung des Geldes - Kritik der Ökonomie. 2019
[^31]: [Dokumentarfilm Arte 2014 - Ilan Ziv - Kapitalismus](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
