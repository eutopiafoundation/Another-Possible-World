---
title: Die Spezies Homo Sapiens
description: Beschreibung:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

## Homo Sapiens

Die Menschheit mag zwar über eine enorme Intelligenz, ein Bewusstsein und ein immer größeres technologisches Wissen verfügen, aber wenn sie mit den gegenwärtigen und zukünftigen Folgen des Klimawandels konfrontiert wird, kann sie nicht von ihrem Kurs abweichen.[^1].

Daraus ergibt sich die Notwendigkeit, den Menschen und seine Funktionsweise zu verstehen, denn nur so können wir sicherstellen, dass die in unserem Eutopia geschaffene Umwelt möglich ist, und wir können unser Verhalten in ihr vorhersagen.

Wenn wir die "Schwächen" des Menschen verstehen, können wir die Werkzeuge entsprechend anpassen und entwickeln, um sie zu nutzen und daraus eine Stärke zu machen.

Wir werden die Mechanismen auf individueller Ebene verstehen, seine verschiedenen physischen, psychologischen und anderen Aspekte. Dieser Ansatz wäre jedoch unzureichend und wird von einer ganzen Reihe globaler und soziologischer Überlegungen begleitet.

<!-- ## Kurze Geschichte des Homo sapiens -->

## Die Bedürfnisse von Homo Sapiens

Kehren wir zu unserer Forschungsfrage zurück:

"Wie können wir allen Menschen für die nächsten 80 Millionen Generationen ein erfülltes Leben auf der Erde ermöglichen?"

Wir sind davon ausgegangen, dass ein erfülltes Leben für jeden Menschen ein erfülltes Leben ist, das seine Bedürfnisse und Sehnsüchte befriedigt. Ein Bedürfnis ist eine gefühlte Notwendigkeit, die körperlicher, sozialer oder geistiger Art ist.

### Klassifizierung von Bedürfnissen

Um uns bei dieser Suche zu helfen, greifen wir zunächst auf die 1943 formulierte Pyramide von Abraham Maslow zurück. Diese ordnet die Bedürfnisse des Menschen in fünf Stufen und ist eine interessante Grundlage, um sich einen Überblick über die großen Bedürfnisse des Menschen zu verschaffen.[^2].

Anzumerken ist, dass dieses Modell heute in Frage gestellt wird, denn laut Maslow würde man ein höheres Bedürfnis erst dann verspüren, wenn das vorherige erreicht ist. Aber diese sind in Wirklichkeit keineswegs pyramidenförmig, heute haben zahlreiche Fortschritte gezeigt, dass das soziale Bedürfnis genauso wichtig ist wie die physiologischen Bedürfnisse, was nicht bedeutet, dass es sich an der Basis der Pyramide befinden sollte, sondern vielmehr allgegenwärtig in jedem Stockwerk, und das ist auch bei den anderen der Fall, jeder empfindet sie alle gleichzeitig.[^3].

Aber wir müssen hier ohnehin nicht genau verstehen, welches Bedürfnis im Vergleich zu einem anderen am wichtigsten ist und unter welchen Umständen, denn das Ziel hier ist es, alle ausnahmslos zu erfüllen.

Im Folgenden wird die Pyramide durch eine Aktualisierung der Theorie im Hinblick auf die Entwicklung der Psychologie und der Neurowissenschaften ergänzt.

Maslowsche Pyramide](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

Im ersten Stockwerk haben wir die physiologischen Bedürfnisse, das sind die Grundbedürfnisse, die notwendig sind, um das Leben und das gute Funktionieren des Organismus aufrechtzuerhalten, wie Essen, Trinken, Sexualität, Schlafen, Atmen....

Bereits hier ist das völlige Versagen unseres derzeitigen Systems zu erkennen, die Grundbedürfnisse der gesamten Bevölkerung zu erfüllen. Das führt zu zahlreichen Störungen, zur Übertragung von Krankheiten, zur Verweigerung der Zustimmung, zu Traumata, zu Problemen der geistigen und körperlichen Gesundheit, zu Mangel oder Sucht nach Vergnügen, zu einem Mangel an Freiheitsgefühl und Vertrauen, und insgesamt zu einem Mangel an Wohlbefinden in Bezug auf die eigene Sexualität bei einem Großteil der Bevölkerung.

Im zweiten Stockwerk befinden sich die Bedürfnisse nach Sicherheit. Die Sicherheit, einen Schutzraum zu haben, wie z. B. eine Wohnung, in der man leben kann, die Sicherheit, Zugang zu gewünschten Ressourcen wie Material, Werkzeug, Spiele usw. zu haben. Physische Sicherheit vor Gewalt und Übergriffen... Moralische und psychologische Sicherheit, emotionale Sicherheit und Stabilität, medizinische/soziale und gesundheitliche Sicherheit.

Auch hier sind die verschiedenen Mängel unserer Gesellschaft auch auf dieser Ebene festzustellen, selbst in den westlichen Ländern fällt auf, dass die Wohnungssicherheit für viele Menschen eine echte Herausforderung darstellt, insbesondere für Obdachlose, die oft mit unsicheren und unmenschlichen Bedingungen konfrontiert sind.[^5] Auch der Zugang zu Ressourcen kann für manche Menschen aufgrund ihres sozioökonomischen Status eingeschränkt sein, was sich auf ihr Wohlbefinden und ihre Lebensqualität auswirken kann. Was die medizinische und psychologische Sicherheit betrifft, so gibt es einen Mangel an psychosozialen Fachkräften, was den Zugang zur Versorgung erschwert. Dass selbst mit der Sozialversicherung von Ländern wie Frankreich die Kosten für die Versorgung für manche Menschen unerschwinglich sein können, was sie daran hindert, die Versorgung zu erhalten, die sie benötigen.[^6].

Die dritte Stufe sind die Bedürfnisse nach Zugehörigkeit, das Bedürfnis zu lieben und geliebt zu werden, intime Beziehungen zu haben, Freunde zu haben, Teil einer kohäsiven Gruppe zu sein, sich akzeptiert zu fühlen, sich nicht allein zu fühlen oder zurückgewiesen zu werden.

Wir leben in einer Gesellschaft, in der der Individualismus stark ausgeprägt ist und/oder zwischenmenschliche Beziehungen oft zugunsten des persönlichen Erfolgs vernachlässigt werden. Ein Großteil der Bevölkerung spürt eine zunehmende Einsamkeit und den Verlust sozialer Bindungen. Einige werden abgelehnt und isoliert, was zu psychischen Gesundheitsproblemen wie Depressionen und Angstzuständen führt. [^7]Unser Gesellschaftsmodell, das den allgemeinen Wettbewerb und die interessierte oder kommerzielle Beziehung als einziges Beziehungsmodell begünstigt, fördert eine regelrechte Kultur des Egoismus.

Die vierte Etage stellt die Bedürfnisse nach Wertschätzung dar. Der Mensch hat das Bedürfnis, sich von anderen beachtet, geschätzt und respektiert zu fühlen, sich für seine Leistungen anerkannt zu fühlen und das Gefühl zu haben, erfolgreich sein zu können. Seine Bedürfnisse werden erfüllt, indem er Dinge tut, die von der Gesellschaft wertgeschätzt werden, indem er sich Ziele setzt und darauf hinarbeitet, sie zu erreichen, oder indem er sich in Aktivitäten engagiert, die der persönlichen Entwicklung dienen.

Die fünfte Stufe ist das Bedürfnis nach Selbstverwirklichung. Es ist das Bedürfnis, sich selbst zu verwirklichen und sein persönliches Potenzial auszuschöpfen und zur Geltung zu bringen. Es äußert sich in dem Wunsch, seine Talente und Fähigkeiten zu entwickeln, sich in sinnstiftenden und befriedigenden Aktivitäten zu engagieren und sich selbst zu übertreffen.

In unserer Gesellschaft ist es schwierig, dieses Bedürfnis nach Selbstverwirklichung zu befriedigen. Der soziale Druck und die hohen Erwartungen, die an die meisten Menschen gestellt werden, können es schwierig machen, seinen persönlichen Leidenschaften und Interessen nachzugehen. Unsere stark wettbewerbsorientierte Gesellschaft mit ihrer Entfremdung vom Individuum lässt nur wenig Zeit, um die eigenen Talente und Fähigkeiten zu erforschen und zu entwickeln. Viele Menschen werden auch vom System ausgeschlossen, insbesondere Menschen mit Behinderungen, die sich aufgrund mangelnder Anpassung des Systems nicht in dieses einfügen können. [^9]

Dennoch wurde das Bedürfnis nach Selbstverwirklichung im Jahr 2010 durch die Arbeit von vier Psychologen, die eine Aktualisierung der Evolutionstheorie vorschlugen, etwas in Frage gestellt. [^3] Die Evolutionstheorie nimmt das Bedürfnis nach Selbstverwirklichung zurück, um Platz für drei Bedürfnisse zu schaffen, die mit dem Reproduktionstrieb verbunden sind: Begegnung, Kontinuität und Nachkommenschaft.

| | |
| ------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| Bedürfnis nach Nachkommenschaft | Schiebt den unausweichlichen Tod hinaus |.
| Bedürfnis nach Kontinuität: Behalte deinen Partner, deinen Status etc. | Wehre den Verlust ab |
| Bedürfnis nach Begegnung: Einen Partner erwerben | Wehrt Konkurrenten und soziale Isolation ab |.
| Bedürfnis nach Wertschätzung und Status: Als einzigartiges Wesen anerkannt werden | Wehrt soziale Urteile ab |
| Bedürfnis nach Zugehörigkeit: Geliebt und akzeptiert zu werden, so wie man ist | Abwehr von Verlassenheit und Einsamkeit | | Bedürfnis nach Selbstwertgefühl | | Bedürfnis nach Selbstwertgefühl
| Schutzbedürfnis: Schutz, +physische, familiäre, psychologische, finanzielle usw. Sicherheit | Gewalt abwehren |
| Physiologische Bedürfnisse: Atmen, Trinken, Essen, Toiletten, Schlafen, Wärme | Hunger, Durst, Kälte, Askese, Müdigkeit und Krankheit abwehren |

\*Liste der sieben unbewussten und begleitenden menschlichen Grundbedürfnisse nach D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Diese Bedürfnisse sind unbewusste, sich entwickelnde Bedürfnisse, die sich im Leben eines jeden Menschen auf tausend verschiedene Arten ausdrücken. So kann beispielsweise künstlerische Kreativität auf dem Bedürfnis beruhen, einen Status zu erlangen und sozial als einzigartiges Wesen anerkannt zu werden, Partner zu verführen oder die Zeit zu überdauern und ein Werk für die Nachwelt zu hinterlassen.

Wir haben gerade die 5/7 Arten von Bedürfnissen besprochen, aber es ist wichtig zu wissen, dass Abarham Maslow gegen Ende seines Lebens ein sechstes Bedürfnis hinzugefügt hat, das "über sich selbst hinauswachsen". Dieses befindet sich an der Spitze der Pyramide:

> "Der voll entwickelte Mensch, der unter den besten Bedingungen arbeitet, neigt dazu, von Werten motiviert zu werden, die über sein 'Ich' hinausgehen."[^9].

Es ist interessant, sich bereits bewusst zu machen, dass ein erfüllter Mensch, der über die Selbstverwirklichung hinausgeht, voll und ganz dafür offen wäre, seine Individualität zu überwinden und sich für den Dienst an anderen einzusetzen.

Zum Abschluss können diese Bedürfnisse auch durch die Überlegungen von Hannah Arendt ergänzt werden.

In unserem menschlichen Zustand würden wir zu einer Suche nach Ewigkeit gedrängt werden: der vita contemplativa. Dieses Streben ist in unserer modernen Gesellschaft, die nicht mehr an Gott glaubt, immer schwieriger zu erfüllen. Wir werden auch zu einem Streben nach Unsterblichkeit getrieben, der vita activa. [^10]

Das Streben nach Unsterblichkeit ist das, was einen sterblichen Menschen dazu bringt, kreative Worte und Taten zu setzen, die über seine eigene Lebensnotwendigkeit, seine eigene Existenz hinausgehen und ihn über seinen eigenen Tod hinaus in die Zukunft projizieren.

Ursprünglich wurde in der Moderne das Bedürfnis nach Unsterblichkeit gesellschaftlich anerkannt, z. B. durch den Heldenmut von Soldaten, durch Titel und Mottos oder durch den Mut und die Authentizität einer kompromisslosen Rede: "Man denkt und spricht mit Kraft nur aus der Tiefe seines Grabes: dort muss man sich hinstellen, von dort muss man sich an die Menschen wenden" (Diderot, 1782). Heute finden all diese Titel, all diese Worte und all diese Taten in unserer Gesellschaft keinen Widerhall mehr. Hannah Arendt behauptet, dass die gesellschaftliche Verdrängung des Strebens nach Ewigkeit aus dem öffentlichen Bereich, gepaart mit der gesellschaftlichen Verdrängung des Strebens nach Unsterblichkeit, erklärt, wie "die moderne Epoche - die mit einer Explosion menschlicher Aktivität begann, die so neu und so reich an Versprechungen war - in der leblosesten, sterilsten Passivität endete, die die Geschichte je gekannt hat. [^10][^11]

Emmanuel Todd bringt eine konvergierende Aussage zum Ausdruck. Er behauptet, dass der Übergang vom "Menschen, der an den Menschen glaubt" zum "Menschen, der an Gott glaubt" eine große kollektive Energie erzeugt hat. Die Abkehr vom Streben nach Ewigkeit hat die Energien für das Streben nach Unsterblichkeit verstärkt. Als die Kirche als sozialer Rahmen zusammenbrach, entwickelte sich ein neues, säkulares humanistisches Ideal. Es war die immense Hoffnung auf den neuen Menschen, die von den revolutionären Bewegungen des 19. Jahrhunderts getragen wurde. Im Gegensatz dazu markiert der Zusammenbruch des kommunistischen Gesellschaftsideals und des revolutionären republikanischen Ideals das soziale Ende des Strebens nach Unsterblichkeit. "Der Mensch, der an den Menschen glaubt, verschwindet seinerseits und wird zum "Menschen, der an gar nichts mehr glaubt", einem nihilistischen, apathischen Menschen ohne Hoffnung und Ziel, der seine existentielle Leere mit Konsum füllt.[^12].

Was sorgt dafür, dass unser Bedürfnis nach Nachkommenschaft befriedigt wird? Laut Hannah Harendt ist es das Werk. Ein Werk ist das, was geeignet ist, eine Nachkommenschaft zu hinterlassen, d. h. ein Kind. Es bedeutet auch, Bäume zu pflanzen, unter denen man im Laufe der Zeit nicht selbst in den Schatten gehen kann. Es bedeutet, etwas an Kinder weiterzugeben. Es bedeutet, Gedichte zu schreiben oder Holzstücke zu schnitzen. Es bedeutet, sein Leben heldenhaft zu opfern. Es bedeutet, eine sinnvolle Arbeit zu verrichten. Ein Werk ist etwas, das Bestand hat. Es bietet innere Sicherheit und Selbstbeherrschung des Menschen. Durch sein Handeln manifestiert der Mensch sein einzigartiges Werk in der Welt und lässt seine Unsterblichkeit erahnen. Die Werke jedes Einzelnen, die privat geschaffen und öffentlich dargeboten werden, sind das, was die menschliche Welt zur Welt macht. [^10]

Arbeit für den Lebensunterhalt ist nur Überleben. Laut der Philosophin ist Arbeit lebensnotwendig, sie unterstützt unsere physiologische Existenz. Arbeit ist eine enge Dimension des menschlichen Lebens, über die wir keine wirkliche Macht haben. Hannah Arendt beklagt die Verkümmerung des kollektiven Lebensentwurfs durch die unglaubliche Bedeutung, die die Subsistenzarbeit in den modernen Gesellschaften einnimmt. Ihrer Meinung nach sollte die Arbeit in den privaten Bereich verlegt werden und nur eine untergeordnete Rolle spielen. Die Arbeit sollte im Mittelpunkt des öffentlichen gesellschaftlichen Lebens stehen.

Hannah Arendts philosophische Überlegungen zum übermäßigen Stellenwert der Arbeit aus Lebensnotwendigkeit und zum Verschwinden des Werkes aus der Nachwelt ermöglichen zahlreiche Erklärungszusammenhänge für die heutige Situation. Sie ermöglicht es, das Verschwinden der Bürger zu verstehen, die durch Konsumenten ersetzt werden. Sie ermöglicht es, die absolute existentielle Verwirrung zu verstehen, die Arbeitslosigkeit in einer Gesellschaft hervorrufen kann, die das Werk vergessen hat und die öffentlich nur noch auf Subsistenzarbeit organisiert ist. Sie ermöglicht es, die Haltung der jüngeren Generation gegenüber der Subsistenzarbeit und ihre Suche nach Sinn in einem Werkberuf zu verstehen. Und schließlich ermöglicht es, die Pyramide der diese Reflexion Bedürfnisse in Facetten des Menschlichen zu deklinieren.

Aus körperlicher Sicht bedeutet Fortbestand, sich zu reproduzieren und Kinder zu haben. Aus spiritueller Sicht bedeutet es, ewig zu sein. Aus sozialer Sicht bedeutet es, unsterblich zu sein und eine Spur seiner Zeit auf der Erde zu hinterlassen. Das Streben nach Unsterblichkeit im Sinne Hannah Arendts ist der soziale Ausdruck des Bedürfnisses nach Nachkommenschaft, das sich auf allen Ebenen unseres physischen, psychologischen, sozialen oder spirituellen Lebens niederschlägt. Die Abstammung trifft hier auf die Transzendenz. Es sei daran erinnert, dass das Bedürfnis nach Nachkommenschaft für Hannah Arendt wie auch für Kenrick Douglas und seine Kollegen zwei Notwendigkeiten miteinander verbindet: die intime Produktion eines einzigartigen und persönlichen Werkes, verbunden mit einer Beziehung zur Welt, die dieses Werk manifestieren kann.

Dieses mehrdimensionale Bedürfnis nach Nachwelt wird von unserer funktionalistischen und utilitaristischen Gesellschaft völlig ausgeblendet. Und doch liegt gerade hier, in diesen Handlungen, die keine offensichtliche wirtschaftliche und rationale Bedeutung für das tägliche Leben oder die Entwicklung unmittelbarer Vorteile haben, ein Sinn, eine Größe. Die heutige moderne Gesellschaft, die ein Leben anbietet, das sich auf das Überleben und die materielle Entwicklung beschränkt, hat nach und nach den Zugang zur Befriedigung des Bedürfnisses nach Nachkommenschaft abgeschnitten. Dies führt zu einem quälenden Gefühl der Unzufriedenheit und Unerfülltheit in einer Gesellschaft, in der paradoxerweise Überfluss und Genuss herrschen. Hier entsteht die Suche nach Sinn. Diese Suche nach Sinn scheint in der hypermodernen Gesellschaft keine Anerkennung finden zu können. [^11]

Im Folgenden werden die menschlichen Grundbedürfnisse durch die Überlegungen von Hannah Arendt ergänzt. Die Pfeile stellen die verschiedenen Dimensionen des menschlichen Körpers gemäß der östlichen Medizin dar. Diese Darstellung wurde früher gewählt als die klassischen Unterscheidungen der modernen westlichen Medizin, da sie das Spektrum erweitert und Hannah Arendts vita comtemplativa mit einbezieht. Die Pfeile in ihr sind gestrichelt, um zu kennzeichnen, dass sie außerhalb des Bereichs der heutigen Wissenschaft liegen.

[!![Die menschlichen Grundbedürfnisse, ergänzt durch Hannah Arendts Überlegungen](./../../0-GlobalProject//Image/grundbedürfnisse-hannah-arendt.png)]

### Intrinsische und extrinsische Bedürfnisse oder Wünsche

Man muss zwischen Bedürfnissen unterscheiden, es gibt intrinsische und extrinsische Bedürfnisse oder Wünsche.

Intrinsische Bedürfnisse sind innere, persönliche Motivationen, die aus dem Inneren des Individuums kommen.

Extrinsische Bedürfnisse sind externe Motivationen, die von der Umwelt und dem Individuum ausgehen. Sie beziehen sich auf das Bild, das die Person anderen oder sogar sich selbst von sich vermitteln möchte. Konsum kann Bedürfnisse beider Arten gleichzeitig befriedigen. Die meisten Konsumformen, die intrinsische Bedürfnisse befriedigen, haben auch eine extrinsische Dimension.

Extrinsische Bedürfnisse stehen in engem Zusammenhang mit verschiedenen sozialen Normen, Status, ethischem Verhalten und der gesamten kollektiven Vorstellungswelt. Manchmal hat die Befriedigung extrinsischer Bedürfnisse einen Nachteil gegenüber den intrinsischen Bedürfnissen.[^13].

Soziale Distinktion und Nachahmung schaffen extrinsische Wünsche.

Jede Gesellschaft wird extrinsische Wünsche und Bedürfnisse ausbilden.

In unserer Gesellschaft werden diese Wünsche ständig stark manipuliert, Erziehung und soziales Umfeld prägen Lebensstilmodelle ein, oder sehr oft das Gefühl, dass mehr besser ist, und die Werbung schafft einen Wunsch nach dem anderen.

Unzufriedenheit entsteht durch die Diskrepanz zwischen Wünschen und deren Erfüllung, oder anders ausgedrückt zwischen Bestrebungen und Realisierungen.

Um diese Unzufriedenheit einzuschränken, kann man a priori mit zwei Begriffen spielen: das Verlangen verringern oder es erfüllen. Die Betonung liegt oft auf dem zweiten Mittel, der Befriedigung, aber die Existenz des ersten wird fast vollständig vergessen oder vernachlässigt.

Wir müssen also erst einmal verstehen, wie unsere Wünsche entstehen und wie wir sie kontrollieren können. Und das ist es, was wir tun werden.

## Die menschliche Natur

Zuallererst ist es jedoch unerlässlich, einen Blick auf die menschliche Natur zu werfen, da dieser Punkt immer wieder in Debatten auftaucht. Seit langem versuchen die Sozialwissenschaften und die Neurowissenschaften, zwischen angeboren und erworben zu unterscheiden, aber es gibt keinen Konsens über die menschliche Natur.

Während die Naturwissenschaften über ein halbes Jahrhundert lang dem Utilitarismus Recht zu geben schienen, wird es heute möglich, dieser Annahme zu widersprechen: Der Mensch ist nicht nur egoistisch, abgesehen davon, dass er nicht vollkommen rational ist. Dieser Nicht-Nutzwert des Menschen gilt für die primitivsten Formen des Lebens: Kooperation ist von Anfang an angelegt und wäre neben dem Überleben des Fähigsten eine der wichtigsten Triebfedern der Evolution.[^15].

Der Glaube, dass unsere Evolution ausschließlich auf dem Wettbewerb zwischen Individuen beruht, führt zu einer verzerrten Sicht der Evolution, während die Rolle der Kooperation bereits in der darwinistischen Evolutionstheorie enthalten war. [^16]

Was auch immer die menschliche Natur ist, die Geschichte beweist mit ihren unzähligen Beispielen, dass der Mensch alles werden kann: das Schlechteste und das Beste.[^17][^18] Die menschliche Natur sollte keine Rechtfertigung dafür sein, was wir werden wollen. Wir können durchaus entscheiden, was wir in unserer Gesellschaft werden wollen, denn wir haben einfach die Möglichkeit dazu.[^19].

Sicher ist jedoch, dass der Mensch kein Einsiedler ist, sondern in der Gesellschaft lebt und Sozialität zeigt. Der Mensch ist in erster Linie homo sociabilis, er hat eine Neigung, sich seinen Mitmenschen zu nähern und mit ihnen in Beziehung zu treten. Er hat das Bedürfnis zu sprechen, Informationen auszutauschen, Wissen und Emotionen zu teilen, aber auch und sogar grundsätzlich das Bedürfnis, in den Augen der anderen zu existieren. Er sucht die menschliche Interaktion wegen der Geselligkeit, die sie ihm verschafft, und wegen des Austauschs positiver und befriedigender Bilder, von denen er sich nährt. [^20][^21]

## Entstehung unserer Wünsche

Wir haben zuvor gesagt, dass wir zunächst verstehen müssen, wie unsere Wünsche entstehen und wie wir sie kontrollieren können.

Wir haben seit über 1,5 Milliarden Jahren in unserem Schädel ein kleines Organ, das Striatum genannt wird.

Das Striatum setzt Dopamin frei, eine Substanz, die uns das Gefühl von Freude vermittelt und die ein als Erfolg erkanntes Verhalten verstärken wird. [^22]

Es ist Teil eines ganzen Mechanismus der Kooperation und des Dialogs zwischen verschiedenen Strukturen und ist an der Entscheidung beteiligt, erhält Informationen und vergleicht verschiedene Optionen, potenzielle Gewinne, notwendige Anstrengungen, Unmittelbarkeit der Belohnung.

Dieses System wird oft als Belohnungskreislauf bezeichnet, aber Dopamin ist nur ein Hormon, das das Ergebnis einer Handlung vorhersagt. [^23]

Durch diese Dopaminausschüttungen entstehen Anreize, die uns zu einer Handlung veranlassen und dieses Verhalten oder diese Aktivität in der Zukunft verstärken werden.

Diese Verstärker, die man auch als Tiefenmotivationen unseres Gehirns bezeichnen kann, sind : [^22]

- Essen
- Sex
- Die soziale Stellung
- Das Bedürfnis nach Informationen
- Die geringste Anstrengung
- Die intellektuelle Stimulation

Diese Motivationen waren für unser Überleben und die Entwicklung unserer Spezies in Urzeiten von entscheidender Bedeutung und sind auch gegenwärtig noch sehr empfindlich. Die Neuronen im Striatum und im gesamten Belohnungskreislauf ziehen uns zu dem hin, was unsere Verstärker erfüllt, also alles, was Nahrung, Sex, Status, Bequemlichkeit und Information verschafft.
Sie sind nicht von Natur aus schädlich, aber wie wir heute sehen können, können sie ausgenutzt werden, um produktivistische, konsumorientierte, individualistische und umweltzerstörende Wirtschaften zu entwickeln.

Das Besondere an diesem Mechanismus, der unser Gehirn darauf konditioniert, nicht nur nach Nahrung, Geld, Sex und Status zu streben, ist, dass es sich immer mehr Geld, immer mehr Nahrung, immer mehr Sex oder immer mehr Status verschaffen will.

Wenn man eine Ratte in die Nähe eines Ganges mit einem Stück Käse am Ende setzt, schütten ihre Neuronen schon beim Betreten des Ganges Dopamin aus. Beim ersten Mal, wenn sie am Ende des Ganges ein Stück Käse findet, schütten ihre Neuronen Dopamin aus, beim zweiten Mal, wenn sie den Käse findet, tun ihre Dopaminneuronen nichts. Findet er aber zwei Stück Käse statt einem, entladen seine Dopaminneuronen erneut. [^22]

Das Striatum tut all dies nur in dem Maße, in dem es immer mehr tun kann.

Wir schaffen es also nur, unsere Lustkreisläufe zu stimulieren, wenn wir die Dosis erhöhen.

Ein weiterer unserer Mechanismen ist, dass der Genuss und die Bequemlichkeit, die wir uns jetzt gönnen können, bei unseren Entscheidungen hundertmal mehr Gewicht hat als die Überlegung einer fernen Zukunft. Je weiter ein Vorteil in der Zukunft liegt, desto weniger Wert hat er für unser Gehirn. Dies wird als zeitliche Abwertung bezeichnet.

Wir ziehen ein sofortiges Vergnügen einem zehnfachen Vergnügen in der Zukunft vor oder treffen eine Entscheidung, die uns jetzt Vorteile bringt, aber langfristig negative Folgen hat.

Je länger der Zeitraum, desto geringer ist die antizipatorische Reaktion[^24].

Sicherlich gibt es genetisch bedingte Unterschiede zwischen den Menschen, und manche reagieren empfindlicher auf ihr Striatum, aber es sind vor allem Elemente, die mit der Erziehung und dem soziokulturellen Umfeld zusammenhängen, die sich als ausschlaggebend erweisen. [^25][^26]

Diese Neuronen sind in Wirklichkeit die Grundlage unserer Lernprozesse

Sie konditionieren uns: Sobald wir gelernt haben, dass Essen auf eine Geste folgt, wird die Geste an sich attraktiv. Das ist das Prinzip des sozialen Lernens: Wir werden dazu konditioniert, ein bestimmtes Verhalten zu mögen. Das Prinzip der operanten Konditionierung herrscht in der Schule, dem Ort des Lernens, vor.

Egoismus und Altruismus sind gleichermaßen lustgetrieben.

Wenn wir jemandem, den wir lieben, ein Geschenk machen, kommen wir nicht bewusst auf die Idee, dies zu tun, um uns gut zu fühlen, und doch fühlen wir uns gut.

Man könnte sich also fragen: Wenn Altruismus uns persönlich gut tut und wir uns darüber hinaus dessen bewusst sind, ist das dann noch Altruismus, ist es nicht vielmehr eine Art gut platziertes Eigeninteresse?

Die Grenze zwischen Altruismus und Egoismus ist schmal, es ist sogar eine Grenze, die auf biologischer Ebene nicht existiert, es gibt kein Altruismushormon oder Egoismushormon. Es gibt eine Handlung, bei der unser Gehirn eine Wahrscheinlichkeitsberechnung durchführt, um zu sehen, ob sie uns nützt, und wir empfinden dann Freude daran. Die beteiligten Bereiche des Gehirns sind gleich, was wir jedoch feststellen ist, dass es eine stärkere Aktivierung des dorsolateralen präfrontalen Kortex beim Egoismus gibt, da die Kosten-Nutzen-Rechnungen stärker ausgeprägt sind. [^27] Insgesamt sind wir es aber, die Handlungen als altruistisch oder egoistisch klassifizieren.

Wir würden sagen, dass es bei einer egoistischen Handlung nur darum geht, persönliche Vorteile zu erlangen.

Bei einer altruistischen Handlung ist es die Absicht, dem anderen einen Nutzen zu geben, während man selbst einen unbewusst angestrebten Nutzen erhält. Es kann aber auch die Absicht bestehen, dem anderen einen Nutzen zu geben, während man selbst einen bewusst angestrebten Nutzen erhält. [^28]

Die altruistische Handlung ist also ein gegenseitiger Austausch, der in beide Richtungen geht, während die egoistische Handlung nur in eine Richtung geht.

Sowohl Egoismus als auch Altruismus können also zu individuellem Vergnügen führen. Wir werden herausfinden, welche dieser beiden Motivationen effektiver ist, um unser gemeinsames Ziel zu erreichen.

Wenn Menschen also großzügiger sind, liegt das wahrscheinlich daran, dass ihre Gehirne von klein auf auf diese Weise konfiguriert wurden. Wie Mutter Teresa, die für ihre unermüdliche Hilfsbereitschaft, ihr Mitgefühl und ihre bedingungslose Liebe für alle, denen sie begegnete, bekannt war, widmete sie ihr Leben dem Guten für andere, ohne sich um ihre eigene Sicherheit oder ihren persönlichen Komfort zu kümmern, und wurde zu einem der wichtigsten Symbole für Nächstenliebe und Altruismus auf der ganzen Welt. Als sie erst sechs Jahre alt war, nahm ihre Mutter sie mit zu den Bedürftigsten, den Armen, Alkoholikern und Waisenkindern. Und sie gab ihr eine unveränderliche und hartnäckige Empfehlung mit auf den Weg: "Meine Tochter soll nie einen Bissen annehmen, der nicht mit anderen geteilt wird. Das Teilen, das ihr regelmäßig in die Wiege gelegt wurde, wurde zu einer so starken Konditionierung, dass Mutter Teresas Striatum in den 80 Jahren ihrer Berufung nicht mehr nachgab. Ihr Umfeld und ihre Erziehung haben dazu beigetragen, Werte und Motivationen wie Großzügigkeit, Mitgefühl und den Wunsch, anderen zu dienen, in ihr zu entwickeln[^29][^30] Natürlich spielt die Religion, die diese moralischen Werte der Großzügigkeit und des Altruismus predigt, eine wichtige Rolle bei der Konfiguration von Mutter Teresas Gehirn, und diese ergibt sich aus unserer Suche nach Sinn, von der wir weiter unten sprechen werden.

Studien zeigen auch, dass es einen Bonus für geistige Beweglichkeit und Problemlösungskompetenz gibt, der eine Aktivierung der Gedächtnisbereiche und eine bessere Speicherung der vom Lehrer vermittelten Informationen bewirkt. So wird das Dopamin, das ein Schüler erhält, wenn er einen schönen Punkt erhält, die synaptische Plastizität modulieren und die Erinnerungen festigen. [^31]

Natürlich ist das Striatum nicht der einzige Teil unseres Gehirns, der Entscheidungen trifft. Der frontale Kortex ist der vorderste Teil des menschlichen Gehirns und ist im Laufe der Evolution immer dicker geworden, was die Zunahme unserer kognitiven Fähigkeiten widerspiegelt. Sie ist der Sitz des Willens und der Planung.

Wenn man beispielsweise Teilnehmer auffordert, zwischen zwanzig Euro sofort oder 30euro in zwei Wochen zu wählen, leuchtet bei den Teilnehmern, die das Geld sofort nehmen, das Striatum auf, ansonsten der frontale Kortex.[^32].

Wenn wir also eine menschliche Gemeinschaft bilden wollen, die in der Lage ist, die Herausforderungen der Zukunft zu erfassen, sind es diese Verbindungen, die wir durch Anspruch, Gewissenhaftigkeit und Beharrlichkeit entwickeln müssen.

## Beherrschung unserer Wünsche

Um die schädlichen Auswirkungen unseres Striatums zu begrenzen, gibt es drei mögliche Lösungen,

- Unterdrückung des Striatums: In der Geschichte wurde die Aktivität des Striatums durch moralische Gebote und die Anstrengung des Willens, der sich gegen die Versuchung stemmt, blockiert. Wenn wir unser Striatum unterdrücken, kann dies unsere Fähigkeit, Motivation oder Freude zu empfinden, beeinträchtigen, was zu Problemen wie Depressionen und anderen Störungen führen kann. Unterdrückung hat negative Auswirkungen auf das Wohlbefinden und die Funktionsfähigkeit des Einzelnen und ist daher weder eine wirksame noch eine dauerhafte Lösung. [^33]

- Nehmen wir das Striatum bei seinem eigenen Spiel, können wir tatsächlich mit den primären Verstärkern spielen, um eine ideale Gesellschaft zu erreichen, z. B. dafür sorgen, dass es zur gesellschaftlichen Norm wird, einen gesunden und nachhaltigen Lebensstil zu pflegen. Die Entwicklung der grundlegenden Verstärker für eine gute Gesellschaft vorantreiben.

- Eine effektivere und nachhaltigere Lösung ist es jedoch, an die einzigartige Fähigkeit des Menschen zu appellieren, das Bewusstsein. Denn die Stärke des Striatums rührt daher, dass seine Gebote nicht bewusst sind.

Wenn sich die Neuronen des Striatums an ein bestimmtes soziales Niveau gewöhnt haben, stumpfen sie ab und man spürt nichts mehr, sodass es zwingend notwendig wird, eine weitere Stufe höher zu steigen, um sie zu stimulieren. Dieser Inkrementierungsprozess erzeugt keine dauerhafte Befriedigung, er kann kein Glück bringen.

Die meisten unserer Handlungen werden mit einem sehr niedrigen Bewusstseinsniveau unternommen. Am häufigsten handeln wir maschinell. Selbst wenn wir uns intellektuell betätigen, wäre es falsch zu behaupten, dass wir dies mit vollem Bewusstsein tun. [^34]

Wir verfügen über eine Großhirnrinde mit einer sehr hohen Rechenleistung, die wir vor allem für Nutz-, Leistungs- und technische Zwecke einsetzen. Seit mehreren Jahrtausenden dient unser Abstraktionsvermögen der Konzeptualisierung und Planung hauptsächlich dazu, Werkzeuge zu entwerfen, die unser Striatum sättigen.

Wir sind Wesen mit einem hohen Maß an Intelligenz, aber einem niedrigen Maß an Bewusstsein. [^22]

Die Intelligenz erarbeitet Lösungen, generiert Berechnungen, setzt Ziele und Programme um. Sie kann dies aber auch sehr gut ohne Bewusstsein tun. Das prominenteste Beispiel ist die künstliche Intelligenz, die extrem komplexe Aufgaben ohne jedes Bewusstsein bewältigen kann.

Die großen künstlichen Intelligenzen, die Sie heute kennen, arbeiten auch mit Wahrscheinlichkeiten und mit einem Verstärkungssystem, das Belohnungen oder Bestrafungen für Handlungen vorsieht, um das Erlernen erwünschter Verhaltensweisen zu fördern. Mit anderen Worten, dieses System funktioniert ähnlich wie Dopamin beim Menschen.

Wenn wir also ein Bewusstsein in der KI schaffen wollen, müssen wir nur verstehen, wie unser Bewusstsein entsteht. Dieses Geheimnis
wird derzeit von den Neurowissenschaften noch nicht vollständig verstanden, und wir werden später darauf zurückkommen. Aber wenn es so weit ist und wir es schaffen, dieses Bewusstsein zu erschaffen, wird der Unterschied zwischen uns und einer Maschine sehr gering werden. Dieser mögliche Fortschritt wirft eine Menge ethischer Fragen auf und bringt uns dazu, über die Wahrnehmung unseres eigenen Bewusstseins nachzudenken.

Wie dem auch sei, ein Teil der Lösung für die Probleme unserer Spezies besteht darin, mehr Bewusstsein in unsere alltäglichen Handlungen einzubringen.

Das Bewusstsein ist auch ein Resonanzkörper für unsere Wahrnehmungen.

Wenn wir unseren sensorischen Resonanzkörper erweitern, können wir unserem Striatum vorgaukeln, dass es mehr Vergnügen bekommt, obwohl wir ihm quantitativ weniger Vergnügen bereiten.

Ein sehr praktisches Werkzeug zur Entwicklung unseres Bewusstseins ist die Mediation. [^35]

Unser Bewusstsein auf ein Niveau zu bringen, das mit unserem Intelligenzniveau vergleichbar ist, wird zweifellos eine der wichtigsten Herausforderungen für die Gestaltung einer guten Gesellschaft und die Sicherung der Zukunft unserer Spezies sein. Die Entwicklung hin zu einer Gesellschaft des Bewusstseins und zu einer Wirtschaft des geistigen Wachstums.

## Das Bedürfnis nach Sinn und Gewissheit

Sobald der Einzelne die Autonomie seiner Entscheidungen und Überzeugungen besitzt, d. h. das Recht, seinem Leben die Richtung zu geben, die er wünscht, ist das Festlegen dieser Richtung zu einer sehr schwierigen Aufgabe geworden. Da die Richtung nicht mehr von der Religion oder einem totalitären politischen Regime vorgegeben wird, muss jeder von uns seinen eigenen Sinn erschaffen. Wenn wir uns bewusst werden, dass unsere Existenz kurz und dem Nichts geweiht ist, müssen wir eine Rechtfertigung für unsere Existenz und unsere Handlungen finden, und diese Erkenntnis kann unerträglich werden. [^36]

Denn Sinn ist für das Leben wichtiger als alles andere, er hat einen Überlebenswert für das Wesen des Menschen. Um unseren Mangel zu beheben, tauchen wir also in die Suche nach Geld, Status usw. ein.

Das Lustsignal, das vor dem, was das Tier sucht, erzeugt wird, ist eine Vorhersage unseres Gehirns über das, was passieren wird. Diese Produktion findet statt, weil es sich um einen evolutionären Vorteil handelt, einen Mechanismus, der die Überlebenschancen des Tieres erhöht.

Indem es in der Lage ist, aus dem, was es um sich herum beobachtet, zu produzieren, was passieren wird, verzehnfacht es seine Kontroll- und Entscheidungskraft. Es kann nach vorteilhaften Situationen Ausschau halten und vor potenziell gefährlichen Situationen fliehen. Er ist der Realität einen Schritt voraus.

Das Gehirn von Wirbeltieren hat einen Weg erfunden, Vorhersagen zu treffen und dem Zustand der Realität einen Schritt voraus zu sein. Diese Fähigkeit, Verbindungen zwischen dem Zustand seiner Umwelt zum Zeitpunkt T und seinem zukünftigen Zustand herzustellen, ist die Grundlage für das, was man bei einer hochgefeierten Spezies wie Homo Sapiens als Sinn bezeichnet.

Wir haben in der menschlichen Gesellschaft einen Sinn geschaffen, wir wissen, dass wir in ihr akzeptiert werden und unseren Weg finden können, solange wir uns an die festgelegten Regeln und Kodizes halten. Diese Gesellschaft ist nicht dem Chaos ausgeliefert, sie hat eine Ordnung, und diese verständliche Ordnung ist für uns grundsätzlich beruhigend.

Unsere Neigung, Sinnzusammenhänge in unserer Umwelt zu erkennen, ist so ausgeprägt und unaufhaltsam, dass sie uns manchmal dazu bringt, Zusammenhänge zu erkennen, wo es nicht unbedingt welche gibt.

Wenn zum Beispiel ein Krieger, der auf Beutejagd geht, ein Halsband anlegt und damit eine Beute jagt, wird der Krieger beim nächsten Mal, wenn er das gleiche Halsband anlegt, einen kleinen Dopaminschub erhalten, weil er eine andere Beute fangen könnte. Der Krieger wird sich in Bezug auf seinen zukünftigen Erfolg zuversichtlicher fühlen.

Dieses System der Antizipation reduziert das Gefühl der Unsicherheit über die Zukunft. Beobachten, vorhersagen, zukünftige Ereignisse antizipieren, Ängste verringern - all das ist Teil des Konzepts der Sinnhaftigkeit. Dieser Vorteil ist so entscheidend, dass es Grund zu der Annahme gibt, dass er von der Evolution ausgewählt wurde.

Das Erkennen von Sinn in unserer Umgebung ist für unser Überleben so entscheidend, dass Situationen, in denen uns der Sinn entgeht, akute physiologische Angst auslösen. Diese Reaktion wird von unserem Organismus aus Überlebensinstinkt hervorgerufen.[^37].

Mithilfe von bildgebenden Verfahren im Gehirn können wir herausfinden, was im Gehirn vor sich geht. Ein kleiner Streifen der Großhirnrinde, der einige Zentimeter über dem Striatum lokalisiert ist, tritt in Aktion. Es handelt sich um eine Falte der Großhirnrinde, die sich an der Schnittstelle zwischen zwei Großhirnhemisphären befindet, die wiederum mit dem Striatum verbunden sind, und die als anteriorer cingulärer Cortex bezeichnet wird. Dieser innere cinguläre Cortex leuchtet auf, sobald die gemachten Vorhersagen nicht mehr durch das, was tatsächlich passiert, bestätigt werden.

Dieses Alarmsignal bedeutet eine Verletzung der Vorhersage. Wenn zu viele Vorhersagen für ungültig erklärt werden, wird es schwierig, sich zu organisieren, und man hat das Gefühl, ins Chaos zu stürzen. Überreizt wird dieses Fehlersignal schädlich für die körperliche und geistige Gesundheit des Einzelnen. Es löst im Körper eine starke Stressreaktion aus, der cinguläre Cortex aktiviert einen mehrgliedrigen Nervenkreislauf, der bis zu einem an Angst und Furcht beteiligten Gehirnzentrum, der Amygdala, hinunterreicht, dann zu den auf den Nieren befindlichen Nebennierenrinden und zu neuronalen Kernen im Hirnstamm, die Hormone wie Cortisol oder Noradrenalin freisetzen, deren Wirkung darin besteht, den Körper in eine Fluchthaltung zu versetzen siehe Lähmung und Angst auszulösen, die existenziell werden kann.

Unser cingulärer Cortex spielt also die Rolle eines Alarmsignals, das uns darauf aufmerksam macht, dass unsere Welt keinen erkennbaren Sinn mehr hat. Die Folgen dieser Reaktion reichen von Schlafstörungen über Angstzustände, Gedächtnisschwund, Herz-Kreislauf-Erkrankungen und Diabetes bis hin zu Depressionen.

Beginnt das Maß an Ordnung und Organisation in unserer Umgebung zu sinken, wird dieser zentrale Teil unseres Gehirns aktiv und warnt uns vor einer potenziellen Gefahr für unser Überleben. Wenn die Gesellschaft relativ stabil ist, in der sich die Strukturen der Arbeit, der Familie und der zwischenmenschlichen Beziehungen nicht zu unvorhersehbar und willkürlich ändern, ist der cinguläre Cortex ein Faktor der Anpassung und Anpassung. Wenn sich die Bezugspunkte jedoch zu schnell und ständig ändern, ohne dem Einzelnen eine Pause zu gönnen, kann er für sich selbst und andere äußerst gefährlich werden.

Doch wie reagiert der menschliche Geist angesichts der Sinnentleerung? Er baut sich Repräsentationssysteme auf, die von Bedeutung, Ordnung und Kohärenz durchdrungen sind. Seitdem der Mensch existiert, haucht er der Realität also lediglich Bedeutung ein.

Die ersten Versuche fanden in Form von mythischen Schöpfungs- und Naturerzählungen statt. Denn das Bedürfnis nach Sinn entsteht aus dem Bedürfnis nach Kontrolle, es ist ein Ausfluss unseres Überlebenswunsches. Und sich die Welt als einen von Sinn bewohnten Ort vorzustellen, beruhigt das innere Warnsystem im Falle eines punktuellen Vorhersagefehlers in der konkreten Welt.

Von daher ist die vom cingulären Kortex ausgelöste Alarmreaktion bei Gläubigen natürlich gedämpft. Doch heute glauben wir für die meisten Menschen in der westlichen Welt nicht mehr daran, und wir haben diesen Sinn verloren.

Die großen religiösen, aber auch ideologischen, demokratischen oder philosophischen Sinnsysteme sind sozusagen nur noch entwertete Referenten, die durch wissenschaftliche Erkenntnisse und durch die Koexistenz zahlreicher spiritueller oder ideologischer Botschaften brüchig geworden sind, die man zwar zu tolerieren versucht, deren bloße Vielzahl aber ausreicht, um die Hoffnung zunichte zu machen, dass eine von ihnen allein eine absolute Wahrheit innehaben könnte.

In der Geschichte sind Rituale systematisch vor Moralsystemen aufgetaucht. Diese Synchronisation und Mimikry, die bei kollektiven Ritualen am Werk ist, macht uns Menschen sensibler für die Gefühle, Wünsche und Emotionen unserer Mitmenschen. Diese Fähigkeit, sich in andere hineinzuversetzen, wird als kognitive Empathie bezeichnet. Sie ermöglicht es uns, "in die Haut des anderen zu schlüpfen", zu fühlen, was er fühlt, und zu denken, was er denkt. Und diese Fähigkeit wird durch Nachahmung um ein Vielfaches gesteigert: Synchronisierte Partner empfinden Mitgefühl füreinander.

Rituale beruhigen unseren cingulären Cortex, indem wir zuverlässiger vorhersagen können, was unsere Mitmenschen wahrscheinlich tun oder nicht tun werden, einfach dadurch, dass wir ihre Bewegungen während des Rituals bereits vorhersagen können.

Allerdings ist es schwieriger, die Gedanken von Menschen vorherzusagen als ihre Bewegungen. Um dies zu erreichen, geht es darum, nicht mehr nur einfache Gesten, sondern mentale Repräsentationen zu teilen. Mit anderen Worten: Werte und Weltanschauungen.

Es kann verschiedene Weltanschauungen geben, manche sind normativ, d. h. sie schreiben bestimmte Verhaltensweisen vor, und andere sind nur positiv (oder faktisch) und schreiben umgekehrt keine Verhaltensweisen vor. Wenn diese Sichtweise vorschreibt, was gut ist und was nicht, und die Menschen sich daran halten und die Welt auf die gleiche Weise sehen wie wir, kann dies die Unsicherheit und de facto die Aktivität des cingulären Cortex erheblich reduzieren.

Ihre Verletzung hingegen aktiviert ihn (und andere Gehirnstrukturen) stark. Für unseren anterioren cingulären Cortex ist die Nichteinhaltung moralischer Normen ein grundlegender Vorhersagefehler. Was für die Regulierung des Verhaltens anderer an sich ein starker Beruhigungsfaktor für den cingulären Cortex ist, der versucht, sich unter seinen Mitmenschen zurechtzufinden.

In dem Moment, in dem Sie sicher sind, dass andere an dieselben heiligen moralischen Werte glauben wie Sie, können Sie beginnen, mit größerer Zuverlässigkeit vorherzusagen, was sie wahrscheinlich tun oder nicht tun werden.

Im Begriff Religion steckt das lateinische Verb religere, das verbinden bedeutet.

Im Laufe der Entdeckungen hat die Menschheit auf sozialer und moralischer Ebene zugunsten von Vorhersagen über die materielle Welt verloren. Wie der berühmte Philosoph Friedrich Nietzsche sagte: "Gott ist tot, alles ist erlaubt".

Für unseren cingulären Cortex ist dies der Beginn der Angst. Denn der von der Wissenschaft gelieferte Sinn bietet nur eine positive (oder faktische) Sicht, er birgt also nicht ganz dieselbe beruhigende Kraft wie der von Religion und Moral gelieferte Sinn.

Die Wissenschaft führt einen faktischen Sinn ein, aber zumindest anfangs keinen moralischen Sinn.

Sicherlich ist der von der Wissenschaft erzielte faktische Sinn kolossal. Heute weiß der Mensch in gewisser Weise, wie die Erde entstanden ist und wie er auf die Erde kam, er kann die Abfolge der Ereignisse, die dazu führten, dass sich die Arten von den Bakterien bis zu den ersten Wirbeltieren, Säugetieren, Affen und Homo Sapiens entwickelten, ziemlich genau nachvollziehen. Die Frage nach der Ursache ihrer Existenz, ihrer Natur und ihren körperlichen und geistigen Merkmalen ist beantwortet. Ebenso ermöglicht ihm die Kenntnis der Gesetze der Physik und des Lebendigen, eine erschreckende Menge an konkreten Tatsachen vorherzusagen, wie z. B. das Wetter, die Mond- und Sonnenfinsternis, die Leistung eines Verbrennungsmotors in Abhängigkeit vom Raffinationsgrad des verwendeten Benzins und des Hubraums etc.

Aber wir wissen nicht mehr, was auf menschlicher und existenzieller Ebene Sinn macht. Denn wenn wir den Sinn getötet haben, haben wir nicht das Bedürfnis nach Sinn getötet. Der Teil unseres Gehirns, der Hunderttausende von Jahren gebraucht hatte, um sich anzupassen, um in der Welt und in den sozialen Strukturen Sinn zu finden, ist immer noch da. [^36]

In unserer Gesellschaft erleben wir den Mythos von Sisyphos, einer griechischen mythologischen Figur, die dazu verurteilt war, ewig einen Felsen einen Hügel hinaufzurollen und jedes Mal wieder hinabzusteigen, bevor sie den Gipfel erreichte. [^39] In unserer Gesellschaft haben wir keinen Sinn mehr in der Art und Weise, wie wir produzieren. Wir befinden uns heute in einer Wegwerfwelt. Und wenn nichts von Dauer ist, wird das Leben zur verlorenen Mühe. Wir alle sind mehr oder weniger mit diesem Problem konfrontiert.

Hinzu kommt, dass heute mit dem Klimawandel nichts mehr stabil ist, weder der Zyklus der Jahreszeiten, noch der Meeresspiegel, noch das Auftreten von Naturkatastrophen. Die Welt ist nun in Bewegung, und diese Bewegung wird unsere Verschuldungskapazität übersteigen.

Das Wort ist hart, aber die Ungewissheit tötet, sie untergräbt unser Gehirn und zerstört das grundlegende menschliche Streben nach Sinn.

## Unsere Reaktionen auf die Ungewissheit

Das Gefühl der Unbestimmtheit aktiviert den cingulären Kortex, der die Unmöglichkeit, Vorhersagen über die Zukunft zu treffen, sich selbst zu definieren und einen klaren Lebensweg zu erahnen, schmerzhaft registriert.

Die Öko-Angst zum Beispiel ist eine neue Form der Angst, die aus dem lähmenden Bewusstsein der Zerstörung unseres Planeten resultiert. [^38]

In einer Welt, in der ein Höchstmaß an Unsicherheit herrscht, wird der cinguläre Cortex dafür sorgen, dass die Gewissheit auf kleineren Skalen wiederhergestellt wird, da er dies auf der globaleren Ebene der eigenen Existenz nicht tun kann. Dies wird als Mikro-Gewissheit bezeichnet.

Drogen, die mit unseren Neuronen spielen, können ein Ersatz für Sinn sein. Kokain zum Beispiel zerstört die Ungewissheit.

Das Selbstwertgefühl ist die selbstzentrierte Erfindung einer individualistischen Welt und eine Möglichkeit, den Verlust von Sinn zu kompensieren.

Geld ist der absolute Pass, um sich von der Angst vor der Ungewissheit zu befreien.

Doch im Spiel des Wettbewerbs werden die Ungleichheiten immer größer. Die Verlierer dieses Wettlaufs, die durch das Zerreißen der Nabelschnur entmenschlicht wurden, werden sich dann im schlimmsten Fall einer degradierten Form der Menschlichkeit zuwenden. Diejenige, die beabsichtigt, den Teil der Menschlichkeit in anderen zu verleugnen, um sich dessen Exklusivität zu sichern. Wir sprechen hier von Identität.

Denn diese Spannung lässt automatisch ein dringendes Bedürfnis entstehen: ein für alle Mal zu wissen, wer man ist. Und der einfache Weg, seine Identität zu klären, besteht darin, sich durch Bezugnahme auf eine Zugehörigkeitsgruppe zu definieren. Sich über die Beziehung zu einer Gruppe zu definieren, erfüllt ein Grundbedürfnis, nämlich einen Weg zu finden, sich an geltende Regeln zu halten, was die überschießenden Reaktionen des cingulären Cortex einschränken wird.

Übrigens ist eine der wichtigsten Sinnquellen, die in Umfragen regelmäßig genannt wird, das Gefühl der Zugehörigkeit zu einer sozialen Gruppe.[^40].

Ungleichheit stürzt die Gesellschaften in einen identitären Rückzug. Wo nur die Fähigkeit zählt, die Produktionsmaschine für die eigenen Zwecke zu nutzen. Das neoliberale Wirtschaftsmodell, das auf Wettbewerb, Mobilität der Menschen, Beschleunigung des Arbeitsrhythmus und Reduzierung der öffentlichen Ausgaben für ihren Schutz und die gerechte Verteilung des Reichtums beruht, löst einen autoritären und identitätsstiftenden Impuls aus, der die Reaktionen des Einzelnen auf die Unsicherheit und das Defizit an sozialer Zugehörigkeit besänftigen soll.

Anomie bezeichnet das Gefühl des Verlusts von Ordnung und Logik in der Gesellschaft.

Die Menschen, die am häufigsten nostalgische Gedanken haben, sind auch diejenigen, die am deutlichsten den Eindruck haben, dass das Dasein einen Sinn hat.[^41].

Unser Gehirn ist extrem gut darin, Dinge zu verleugnen.

Wenn unsere Handlungen nicht mit unseren mentalen Repräsentationen übereinstimmen, entsteht kognitive Dissonanz. Auch Widerspruch aktiviert kognitive Dissonanz.

Denn das menschliche Gehirn strebt unter allen Umständen nach Kohärenz und Vernunft. Es wird sich also bemühen, die Dissonanz, die eine Unsicherheit darstellt, aufzulösen. Dazu wird es entweder seine Handlungen so ändern, dass sie mit den Gedanken übereinstimmen, oder aber die Gedanken so ändern, dass sie mit den Handlungen übereinstimmen.

Die Forschung zu kognitiver Dissonanz zeigt, dass es meistens die Gedanken sind, die sich den Handlungen anpassen und nicht umgekehrt. [^42][^43]

Wir lösen uns von der Realität ab, weil wir sie nicht mehr deuten können. Sinn schützt uns, weil er uns Wege aufzeigt, die Welt zu interpretieren und effektiver zu handeln, um sie zu beherrschen und uns vor den Bedrohungen in ihr zu schützen.

Je näher das Gespenst der großen Katastrophen rückt, desto stärker wird der Rückzug in die Gemeinschaft, das kompensatorische Verhalten des Hyperkonsums, der Ego-Inflation und der Verleugnung.

Der Mangel an Bedeutungssystemen treibt uns dazu, materielle Güter zu konsumieren, und zwar ganz besonders in Situationen, die von großer Unsicherheit, Beschleunigung und Konkurrenz geprägt sind. Hypermaterialismus ist die Notlösung für Unsicherheit und Wettbewerb.[^36].

## Lösungen für den Mangel an Sinn

Das Gehirn versucht, die Welt vorherzusagen, um sie besser kontrollieren zu können, und das gelingt ihm entweder durch Sinn oder durch Technik. Wenn die Technik Fortschritte macht, braucht es den Sinn nicht mehr. Und wenn die Technik versagt, braucht es Sinn.

Heute haben wir die Technik, aber die Technik hat versagt, denn sie ist dabei, unser Aussterben zu besiegeln. [^44]

Angesichts der gegenwärtigen und zukünftigen Katastrophe müssen wir die Konsummaschine, zu der die Menschheit geworden ist, so schnell wie möglich stoppen und gleichzeitig aktiv nach Möglichkeiten suchen, die Erde zu reparieren.

Wenn mit abnehmendem Sinn der Konsum steigt, dann gilt umgekehrt, dass mit zunehmendem Sinn der Konsum sinkt.

Wir haben 8 Milliarden cinguläre Cortex, die damit beschäftigt sind, ihre Angst vor dem Tod und dem Unvorhersehbaren zu überlisten, jetzt müssen wir sie damit beschäftigen, diese Angst zu überlisten, indem wir sie um einen gemeinsamen Sinn versammeln.[^36].

Dieser Sinn muss Milliarden von Charakteren mit unterschiedlichem kulturellem Hintergrund, verstreutem zivilisatorischem Erbe und heterogener intellektueller Bildung mitnehmen, um sie von der Besessenheit des Besitzens und Ausbeutens abzulenken.

Es gibt zwei Bedeutungen: die kosmische und die soziale.

Der erste beruht auf unserem Verständnis der Gesetze der Natur, der Materie und des Universums. Die zweite Bedeutung bezieht sich auf unsere Fähigkeit, in einem sozialen Kontext im Einklang mit dem zu handeln, was wir für richtig und falsch halten.

Sie schaffen ein moralisches System, sie definieren ein Gut und ein Böse, mit denen sich soziales und individuelles Handeln regulieren lässt.

Das Heilige ist der Dreh- und Angelpunkt, von dem aus der Sinn in den Versammlungen der Menschen aufgebaut wird. Heute hat die moderne Menschheit das Heilige verloren, indem sie die Realität dekonstruiert und entdeckt, dass alles - von der Bewegung der Atome in einem geschmolzenen Stern bis zum Ionenfluss in der Membran Ihrer Neuronen, während Sie diese Zeilen lesen - mechanistischen Gesetzen gehorcht, in denen Gut und Böse keinen Platz haben.

Die Folge dieser Demoralisierung der Welt ist, dass jedes Individuum auf der Erdoberfläche selbst entscheiden kann, was es für gut oder schlecht hält.

Ohne das Konzept des Heiligen kann jedoch kein kollektiver Sinn existieren. Die Lösung ist, das Heilige neu zu erschaffen, und das kann unsere Erde sein.

Das Heilige kann sich nur durch Rituale manifestieren, und die Menschheit muss neue Rituale erfinden, um aus der Sackgasse herauszukommen, in die sie sich manövriert hat. Und wie wir gesehen haben, ermöglichen synchrone Praktiken also eine Beruhigung des cingulären Cortex. Solche Rituale müssen die Erhaltung des Planeten heiligen.

Auf diese Weise können sich die Menschen wirksame Mittel an die Hand geben, um die mit dem Individualismus verbundene mentale Belastung durch Ängste abzubauen. Das Wissen, dass die Bürger meines Landes, des gesamten Kontinents und wenn möglich des gesamten Planeten das für heilig halten, was ich selbst für einen unüberwindbaren Wert halte, schafft die Grundlage für einen neuen Pakt der Zugehörigkeit und des Vertrauens.

Der Mensch ist ein bedingter Kooperator, er ist zu immensen Opfern fähig, wenn er die Gewissheit hat, dass die anderen Mitglieder seiner Gemeinschaft das Gleiche tun. [^45]

Indem sie sich die Reinheit des Kohlenstoffs und die Erhaltung des biologischen und geologischen Gleichgewichts als heilige Werte setzen, können die Milliarden von Menschen die Aktivität ihrer cingulären Kortex auf ein akzeptables Niveau zurückbringen und aufhören, von materiellen Gütern, Drogen, Geld und Egoersatz abhängig zu sein, um ihre eigene Existenz zu ertragen.

Die Ökologie kann eine Weltanschauung anbieten, die allen Menschen gemeinsam ist.

Zusammen mit dem Wissen ist die Liste der Handlungen, die mit dem heiligen Wert vereinbar sind, eine Sache der Wissenschaft, der Messungen und Berechnungen, die vom menschlichen Kollektiv durchgeführt werden.

Es geht um die Schaffung eines globalen Identitätsgefühls, das jedem das Gefühl gibt, in einer Gruppe akzeptiert zu werden, ohne dass er seinen Wert und seine Wählbarkeit durch die Anhäufung materieller Güter oder Ego-Träger beweisen muss.

Heute muss man akzeptieren, dass die Wissenschaft nicht nur wahr und falsch, sondern auch gut und böse sagen kann.

Der dritte kommende Sinn wird daher der ökologische Sinn sein.

Um ihn zu entwickeln, muss man das Wissen durch Bildung ausbauen, um Wissen über lebende Systeme, Biodiversität, Klimatologie, Ökologie und die Evolution der Arten zu vermitteln.

Dadurch wird es möglich, Staunen zu erzeugen, denn Staunen ist ein starkes Gegenmittel gegen die Existenzangst, die uns ergreift, durch die Entdeckung des unendlichen Reichtums des Lebendigen, der Schönheit der Landschaften, der Flora und Fauna, denn darin findet man eine Quelle des Sinns.

Da wir alle dieselbe Erde bewohnen, alle auf ihr handeln und alle die Folgen ihrer Zerstörung erleiden, sind wir also dazu veranlasst, denselben heiligen Wert anzuerkennen.

So können sich die Bürger für Handlungen engagieren, die mit ihren Werten und Überzeugungen übereinstimmen, und sie werden ein Gefühl der Sinnhaftigkeit erleben.

An diesem Wandel mitzuwirken, ist an sich schon sinnstiftend und bringt unsere Meinungen und Handlungen in Einklang.

Wir müssen die Grundfesten unserer Zivilisation überdenken. Die Logik der Just-in-time-Produktion und des Just-in-time-Konsums, die unserer Wirtschaft zugrunde liegt, ist nicht nachhaltig. Im nächsten Kapitel erfahren Sie mehr über unseren Vorschlag für die Wirtschaft.

Tief in uns allen steckt eine große Sehnsucht, wir sind auf der Suche nach Sinn und wissen nicht, wie wir sie befriedigen können. Dieses Werk zielt darauf ab, sie zu stillen, und zwar schon, ohne auf ihre Verwirklichung zu warten, denn sie ist bereits am Anfang ihrer Verwirklichung zu finden.

## Bedürfnis nach Fiktion - Das erzählende Gehirn

Das menschliche Gehirn, diese außergewöhnliche Fabuliermaschine, ist das Herzstück unserer Fähigkeit, Geschichten zu erschaffen - sowohl im Guten als auch im Schlechten. Es schmiedet die Geschichten, mit denen wir unsere Handlungen rechtfertigen und ihnen einen Sinn verleihen können. Diese narrative Fähigkeit ist kein Zufall, sondern das Produkt einer komplexen Evolution, die die Tiefe unseres Bedürfnisses nach Sinn und Ordnung in einer chaotischen Welt offenbart. [^46]

Das Besondere an unserer Spezies ist also zum Beispiel nicht, dass sie seit Urzeiten Kriege führt - Schimpansen und Ameisen tun das genauso -, sondern dass sie eine ganze Geschichte daraus macht ... und Millionen von Geschichten.

Andere Tiere tun nicht Böses um des Bösen willen und auch nicht das Übrige um des Guten willen, sondern handeln hauptsächlich aus unmittelbaren Bedürfnissen oder Instinkten heraus. Menschen suchen nach einem Sinn in ihren Handlungen und denen anderer, oft durch die Brille der Moral, des Guten und des Bösen. Diese Sinnsuche ist also eine Manifestation unserer höheren Gehirnfunktionen, die uns dazu bringen, nach Erklärungen zu suchen und unser Verhalten in einem größeren Zusammenhang zu rechtfertigen.

Was wir als "Selbst" betrachten, ist keine isolierte oder autonome Einheit, sondern vielmehr ein Mosaik aus Merkmalen, Überzeugungen und Werten, die wir von unseren Mitmenschen übernommen, geteilt und manchmal auch angefochten haben. Jede Interaktion, jeder kulturelle oder soziale Austausch trägt dazu bei, unsere Identität zu formen, und macht sie dynamisch und entwicklungsfähig. Unser Selbst ist mit den anderen verwoben. Wir definieren uns im Spiegel unserer Beziehungen, jede Person ist ein Spiegelbild, das uns hilft, unser eigenes Bild zu formen.[^49].

Dieser Prozess der narrativen Konstruktion schwingt mit den Lehren des Buddhismus über die Illusion des Selbst zusammen. Dem Buddhismus zufolge ist das "Selbst", das wir als stabile und kontinuierliche Einheit wahrnehmen, eine Illusion, eine Konstruktion des Geistes. [^47] Wir werden Gelegenheit haben, diesen Punkt im Abschnitt Religion in Teil 2 ausführlicher zu behandeln. In ähnlicher Weise enthüllen die Neurowissenschaften, dass unsere Identität eine Geschichte ist, die wir uns selbst erzählen, eine zusammenhängende Abfolge, die von unserem Gehirn aus dem Nichts erschaffen wurde, um die Illusion der Kontinuität aufrechtzuerhalten.[^48].

Die Identifikation mit unseren Geschichten und unserem konstruierten "Ich" kann uns in einschränkenden Denkmustern gefangen halten. Diese narrative Konstruktion als Illusion zu erkennen, kann befreiend sein. Es fordert uns auf, traditionelle Erzählungen in Frage zu stellen und uns für neue Möglichkeiten der Wahrnehmung und Erfahrung der Welt zu öffnen.

Die Interaktion zwischen "echtem Leben" und Fiktion erhält so eine neue Dimension. Anstatt diese Sphären als getrennt zu betrachten, können wir verstehen, dass sie intrinsisch miteinander verbunden sind und sich gegenseitig nähren. Unsere persönlichen und kollektiven Erzählungen sind keine bloßen Ablenkungen oder Fluchten, sondern mächtige Werkzeuge der Transformation und des Verständnisses.

Die narrative Natur unseres Gehirns zu erkennen und die Selbsttäuschung anzuerkennen bedeutet nicht, unsere Identität oder unsere Geschichten abzulehnen, sondern sie als das zu sehen, was sie sind: flexible, veränderbare und zutiefst menschliche Konstrukte. Es ermutigt uns, nicht mehr den traditionellen Erzählungen zu folgen und zu sagen "So sieht die Welt seit jeher und für immer aus", sondern zu verstehen und zu sagen "Wir sehen nur, was wir sehen".

Es bietet uns einen Weg zu einem reicheren und nuancierteren Verständnis von uns selbst und der Welt, nicht mehr an der eigenen Identität als unverrückbare Realität festzuhalten und sich ausschließlich mit ihr zu identifizieren, sondern lässt in unserem Leben die Möglichkeit offen, die Welt und uns selbst neu zu imaginieren.

## Effekte und kognitive Verzerrung

An dieser Stelle wollen wir die verschiedenen Effekte und Verzerrungen unseres Gehirns durchgehen, da sie unsere Wahrnehmung, unser Gedächtnis, unsere Urteile, unsere Entscheidungen und unser Verhalten auf vielfältige Weise beeinflussen können, indem sie die Art und Weise, wie wir Informationen verarbeiten und mit ihnen interagieren, beeinflussen. Dieses Verständnis von uns selbst wird es uns ermöglichen, effektive Werkzeuge für unsere Gesellschaft zu entwickeln, insbesondere für die Gestaltung unseres Entscheidungsprozesses, und die Folgen dieser Verzerrungen zu vermeiden, die uns daran hindern würden, unser gemeinsames Ziel zu erreichen.

Die ausführliche Lektüre der kognitiven Effekte und Verzerrungen, obwohl empfohlen, ist keineswegs notwendig und kann für Ihren Lesekomfort übersprungen werden; diese wurden im nächsten Abschnitt zusammengefasst.

### Zusammenfassung der kognitiven Effekte und Verzerrungen

Hier ist eine nicht erschöpfende Liste von kognitiven Verzerrungen:

- Selektionsbias: Wir sind eher geneigt, Informationen auszuwählen, die unseren Überzeugungen und vorgefassten Meinungen entsprechen, anstatt ein breiteres Spektrum an Informationen zu berücksichtigen. [^50]

- Gedächtnistäuschung: Wir neigen dazu, die Zuverlässigkeit unserer Erinnerungen zu überschätzen und sie mit unserer aktuellen Wahrnehmung zu färben[^51].

- Rationalisierung von Überzeugungen: Wir neigen dazu, unsere Entscheidungen und Verhaltensweisen, selbst wenn sie falsch oder schädlich sind, zu rechtfertigen, indem wir sie in einer für uns selbst günstigen Weise interpretieren.[^53].

- Überschätzungsbias: Wir neigen dazu, unsere Fähigkeit, zukünftige Ereignisse vorherzusehen und Situationen zu kontrollieren, zu überschätzen[^52].

- Optimismus-Bias: Wir neigen dazu, die Wahrscheinlichkeit künftiger Gewinne zu überschätzen und potenzielle Risiken zu unterschätzen. [^54]

- Bias der Komplexitätsreduktion: Wir neigen dazu, die Komplexität von Situationen zu vereinfachen, um sie besser handhabbar zu machen.[^55].

- Kausalinferenzbias: Wir neigen dazu, einem Ereignis eine Ursache zuzuschreiben, auch wenn dies nicht unbedingt gerechtfertigt oder vernünftig ist.[^56].

- Bias des Reduktionseffekts: Wir neigen dazu, die positiven Auswirkungen einer Maßnahme zu überschätzen und die negativen Auswirkungen zu unterschätzen.[^57].

- Bias des Primacy-Effekts: Wir werden von den ersten Informationen, die wir erhalten, beeinflusst und neigen dazu, diese stärker zu gewichten als spätere Informationen.[^58].

- Bias des Ähnlichkeitseffekts: Wir neigen eher dazu, Personen, die uns ähnlich sind, zu schätzen und ihnen zu vertrauen[^59].
- Verfügbarkeitsheuristik: Wir verwenden oft eine schnelle und intuitive Schätzung, um die Wahrscheinlichkeit oder Häufigkeit eines Ereignisses zu beurteilen, die auf den Informationen basiert, die in unserem Gedächtnis am leichtesten verfügbar sind.[^60].

- Gruppendruck: Wir werden durch die Wahrnehmung dessen, was andere über uns denken, beeinflusst und stehen unter dem Druck, ihren Meinungen und Verhaltensweisen zu entsprechen.[^61].

- Halo Bias: Wir neigen dazu, eine positive oder negative Meinung über eine Person aufgrund eines einzigen Merkmals zu verallgemeinern.[^62].

### Ausführliche Erklärung der Effekte und kognitive Verzerrung

- Bestätigungsbias

Confirmation Bias ist ein kognitiver Bias, das heißt, wenn wir Informationen bevorzugen, die in Richtung unserer Meinung und unserer Überzeugungen gehen. Da wir nach Bedeutung suchen, wählen wir die Informationen aus, die in unsere Richtung gehen. Die Lösung dafür ist, sein eigener Widersacher zu sein, unsere Gedanken auf eine harte Probe zu stellen und zu versuchen, zu beweisen, dass sie falsch sind.

So sollen die Widersprüche der Urteile uns nicht verletzen, sondern uns wachrütteln und in Aktion bringen. Wir mögen die Berichtigung unserer Meinungen nicht, aber wir sollten uns stattdessen dazu bereit erklären und sie anbieten, und sie sollten in Form eines Gesprächs und nicht einer lehrhaften Belehrung kommen können.

Bei jeder Opposition sollte man nicht zu Recht oder zu Unrecht versuchen, sie loszuwerden, sondern tief in sich gehen und prüfen, ob sie richtig sein kann.

- Schräglage der Verfügbarkeit

Der Verfügbarkeitsbias ist eine weitere kognitive Verzerrung, die sich auf die Entscheidungsfindung auswirken kann. Sie tritt auf, wenn wir die Wahrscheinlichkeit eines Ereignisses oder die Häufigkeit eines Auftretens anhand von Beispielen einschätzen, die uns schnell in den Sinn kommen. Diese Verzerrung kann dazu führen, dass wir die Wahrscheinlichkeit seltener Ereignisse unterschätzen und die Wahrscheinlichkeit häufiger oder kürzlich eingetretener Ereignisse überschätzen. Wir überschätzen unsere Argumentation, indem wir Informationen bevorzugen, die unserem Gedächtnis direkt zugänglich sind.

Um diese Verzerrung zu vermeiden, ist es wichtig, eine repräsentative Auswahl an Informationen zu berücksichtigen und zu versuchen, objektiv zu sein, wenn wir Wahrscheinlichkeiten einschätzen. Es ist auch wichtig, sich nicht nur auf Informationen zu verlassen, die unmittelbar verfügbar sind, sondern nach zusätzlichen Informationen zu suchen, die uns ein vollständigeres und ausgewogeneres Bild vermitteln können.

- Halo-Effekt

Der Halo-Effekt ist der Mechanismus, der uns dazu bringt, jemandem aufgrund seiner physischen Erscheinung Eigenschaften zuzuschreiben.

Zum Beispiel hat eine Dating-Website namens cupidexe ein Experiment durchgeführt, bei dem die Profile von Personen nach Persönlichkeit und Aussehen bewertet wurden. Diejenigen, die eine niedrige Bewertung für ihr Aussehen haben, haben eine niedrige Bewertung für ihre Persönlichkeit und umgekehrt, eine hohe Bewertung für ihr Aussehen, eine gute Persönlichkeit. Man könnte meinen, dass man ihnen ein gutes Aussehen zuschreibt, weil sie eine gute Persönlichkeit zu haben scheinen, aber in der Top-1%-Liste sind einige Persönlichkeitsprofile leer...
Sie führten ein weiteres Experiment durch, um dies zu überprüfen, hier stießen die Nutzer auf Profile mit Text und manchmal ohne, und ob man Text hatte oder nicht, wurde gleich bewertet.

Es gibt auch andere Studien und Statistiken, die diesen Effekt bestätigen. Zum Beispiel sind alle US-Präsidenten groß, und je größer man ist, desto mehr Geld verdient man.

- Barnum-Effekt

Der Barnum-Effekt bezeichnet die Neigung von Menschen, vage oder allgemeine Kommentare für genaue Beschreibungen ihrer Persönlichkeit oder ihres Lebens zu halten. Dieses Phänomen ist nach dem berühmten Showman P.T. benannt. Barnum, der vage Persönlichkeitslesungen benutzte, um Menschen in seine Shows zu locken.

So ist beispielsweise ein Persönlichkeitstest, der besagt, dass "Sie eine fürsorgliche und liebevolle Person gegenüber Ihrer Familie und Ihren Freunden sind", vage genug, um von den meisten Menschen als wahr angesehen zu werden. Allerdings werden dadurch keine genauen Details über die Person genannt, was zu einem Gefühl der Identifikation mit der allgemeinen Beschreibung führen kann, auch wenn diese nicht sehr persönlich ist.

Der Barnum-Effekt kann durch das Bedürfnis der Menschen nach Anerkennung und den Drang nach Selbstbestimmung verstärkt werden. Menschen fühlen sich gerne verstanden und wertgeschätzt, was dazu führen kann, dass sie allgemeine Beschreibungen als für sie zutreffend akzeptieren.

Es ist wichtig, den Barnum-Effekt zu berücksichtigen, wenn Sie Persönlichkeitstests oder Kommentare über Ihr Leben oder Ihre Persönlichkeit analysieren. Es ist immer besser, nach spezifischeren und detaillierteren Informationen zu suchen, um ein genaueres Verständnis von sich selbst zu erhalten.

- Anker-Effekt

Der Ankereffekt bezeichnet die Tendenz von Menschen, sich bei Einschätzungen oder Entscheidungen auf eine erste Information, den sogenannten "Anker", zu stützen. Diese erste Information kann einen großen Einfluss auf spätere Einschätzungen haben, selbst wenn sie wenig relevant oder sogar falsch ist.

Wenn Sie z. B. raten sollen, wie viele Menschen in Ihrer Stadt leben, und Ihnen als erste Information eine hohe Zahl genannt wird, neigen Sie dazu, eine höhere Schätzung abzugeben, als wenn Ihnen eine niedrigere Zahl genannt worden wäre. Ähnlich verhält es sich, wenn Sie über den Preis eines Gegenstandes verhandeln: Der erste Vorschlag wird oft als Anker angesehen und hat Einfluss auf die folgenden Vorschläge.

Der Ankereffekt kann bewusst oder unbewusst in Verhandlungs-, Verkaufs- oder Überzeugungssituationen eingesetzt werden, was zu wenig rationalen und voreingenommenen Entscheidungen führen kann.

- Druck von Gleichaltrigen

Gruppendruck ist ein soziales Phänomen, das die Tendenz von Einzelpersonen beschreibt, sich an die Meinungen, Einstellungen und Verhaltensweisen einer Gruppe anzupassen, der sie angehören oder mit der sie sich verbunden fühlen. Dieses Phänomen kann auftreten, wenn Einzelpersonen versuchen, sich an soziale Normen zu halten und Missbilligung oder Isolation durch die Gruppe zu vermeiden.

Wenn sich eine Person beispielsweise in einer Gruppe von Menschen mit einer bestimmten Meinung oder Einstellung wiederfindet, kann sie von diesen Meinungen und Einstellungen beeinflusst werden, auch wenn sie nicht mit ihren eigenen Meinungen oder Einstellungen übereinstimmen. Dies kann besonders in Situationen der Fall sein, in denen die Person von der Gruppe akzeptiert werden und Missbilligung oder Ausgrenzung vermeiden möchte.

Gruppendruck kann auch auftreten, wenn sich eine Person gezwungen fühlt, sich dem Verhalten einer Gruppe anzupassen, um zu vermeiden, dass sie sich anders oder unwohl fühlt. Beispielsweise kann eine Person, die sich in einer Gruppe befindet, die Alkohol trinkt, beeinflusst werden, selbst Alkohol zu konsumieren, auch wenn sie das nicht möchte.

Es ist wichtig, sich des Gruppendrucks bewusst zu sein und sich nicht den Meinungen oder Verhaltensweisen einer Gruppe zu beugen, nur um Missbilligung zu vermeiden oder um sich an soziale Normen anzupassen. Es ist immer besser, seinen eigenen Meinungen und Überzeugungen zu folgen, als dem Gruppendruck nachzugeben.

- Heuristik der Verfügbarkeit

Die Verfügbarkeitsheuristik ist eine gedankliche Abkürzung, die wir oft verwenden, um die Häufigkeit oder Wahrscheinlichkeit eines Ereignisses auf der Grundlage der jüngsten und am leichtesten verfügbaren Beispiele in unserem Gedächtnis zu bewerten. Dies kann zu Fehlern in unserem Urteil führen, da es bei den Beispielen, die wir im Kopf haben, zu Verzerrungen kommen kann.

Wenn eine Person z. B. gerade im Fernsehen von einem Flugzeugabsturz gehört hat, könnte sie sich bei ihrem nächsten Flug mehr um ihre Sicherheit sorgen, da dieses Ereignis frisch im Gedächtnis und leicht verfügbar ist. Allerdings könnte diese Person die Sicherheit des Fliegens im Allgemeinen unterschätzen, da Flugunfälle in Wirklichkeit sehr selten sind.

Es ist wichtig, sich daran zu erinnern, dass die Verfügbarkeitsheuristik zu Fehlern in unserem Urteilsvermögen führen kann, und nach umfassenderen und zuverlässigeren Informationen zu suchen, um die Häufigkeit oder Wahrscheinlichkeit eines Ereignisses zu beurteilen. Es ist auch hilfreich, sich daran zu erinnern, dass die Medien unsere Wahrnehmung von Ereignissen beeinflussen können, indem sie bestimmte Geschichten gegenüber anderen hervorheben. Dies kann zu einer Überrepräsentation seltener oder dramatischer Ereignisse führen und einen falschen Eindruck von ihrer tatsächlichen Häufigkeit vermitteln. Es ist daher wichtig, daran zu denken, dass wir uns nicht nur von dem beeinflussen lassen, was wir in den Medien sehen oder hören.

Zusammenfassend lässt sich sagen, dass die Verfügbarkeitsheuristik nützlich sein kann, um in alltäglichen Situationen schnelle Entscheidungen zu treffen. Sie sollte jedoch mit Vorsicht betrachtet werden, wenn es darum geht, wichtigere Urteile zu fällen oder Entscheidungen mit langfristigen Folgen zu treffen. Es ist besser, nach umfassenden und zuverlässigen Informationen zu suchen, um Wahrscheinlichkeiten und Risiken abzuschätzen, als sich nur auf das zu verlassen, was in unserem Gedächtnis leicht verfügbar ist.

## Schlussfolgerung

Anhand dieses Abschnitts über Homo Sapiens können wir bereits erkennen, dass sich viele neue Wege auftun. Einige davon werden wir in dieser Schlussfolgerung weiter ausführen, aber diese Liste ist nicht erschöpfend. In den folgenden Kapiteln werden wir, aufbauend auf den hier entwickelten Grundlagen, viele weitere entwickeln.

Wir kennen nun die Bedürfnisse des Menschen, die wir befriedigen müssen, damit er sich voll entfalten kann. Wir haben gesehen, dass es möglich ist, unsere extrinsischen Bedürfnisse umzuwandeln, und dass es notwendig sein wird, dies zu tun, um sie mit einem gemeinsamen Projekt in Einklang zu bringen, das es uns allen ermöglicht, unser Bedürfnis nach Sinn zu stillen.

Dieser kollektive Sinn muss durch den Begriff des Heiligen existieren. Und es gibt nichts Heiligeres als das, was wir alle gemeinsam haben und dessen Überleben direkt mit uns verbunden ist, nämlich unsere Erde und ihre Ökosysteme. Die Wissenschaft und die Ökologie sind also nicht mehr nur in der Lage, uns zu sagen, was wahr oder falsch ist, sondern können uns auch unsere Werte diktieren, was gut oder schlecht ist.

Wir haben gesehen, dass es keine menschliche Natur gibt, die uns am Erfolg dieses Projekts hindern könnte, nicht einmal ein unverrückbares Selbst, und bei dieser Aufgabe wird die Entwicklung unseres Bewusstseins unser bester Verbündeter sein. Die Erziehung der neuen Generationen, die Altruismus und das Staunen über die Natur fördert, um sie zu schützen, wird ebenso wichtig sein.

Durch die Untersuchung der verschiedenen Effekte und kognitiven Verzerrungen haben wir immer mehr Informationen gesammelt, die für unser Verständnis von uns selbst und damit auch für die Entwicklung von Hilfsmitteln notwendig sind, die diese Parameter berücksichtigen und sie wirksam machen, um uns bei unserem gemeinsamen Projekt zu helfen.

## Quellen (nicht erschöpfende Liste)

[^1]: [IPCC-Bericht](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia: Bedürfnispyramide](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspektiven auf die Pshychological Science.
[^4]: [Ernährungs- und Landwirtschaftsorganisation der Vereinten Nationen (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Zahlen und Analysen zur Wohnungsnot in Frankreich, einschließlich der Zahl der Obdachlosen und der prekären Lebensbedingungen](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023).
[^6]: [Der Verzicht auf medizinische Versorgung aus finanziellen Gründen im Großraum Paris:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [Die Fondation de France veröffentlicht die: 13. Ausgabe ihrer Studie über Einsamkeit](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap International: Menschen mit Behinderungen gehören weiterhin zu den am stärksten ausgegrenzten Menschen der Welt](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivation and Personality, 1970 (zweite Auflage).
[^10]: Arendt Hannah, Der Zustand des modernen Menschen, 1961
[^11]: Valérie Jousseaume, Plouc Pride: Eine neue Erzählung für die Kampagne.
[^12]: Emmanuel Todd, Der Ursprung der Familiensysteme, 2011 - Wo stehen wir? Eine Skizze der Menschheitsgeschichte, 2017 - Der Klassenkampf in Frankreich im 21. Jahrhundert, 2020.
[^13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997.
[^14]: Jean Baudrillard, Die Konsumgesellschaft, 1970.
[^15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994
[^16]: Charles Darwin, Die Entstehung der Arten: Kapitel IV / III, 1859 & Die Abstammung des Menschen und die sexuelle Selektion: Kapitel VII, 1871.
[^17]: Hannah Arendt, Die Ursprünge des Totalitarismus, 1951
[^18]: Albert Camus, Die Pest, 1947
[^19]: Jean-Paul Sartre, Der Existentialismus ist ein Humanismus.
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, L'homo Sociabilis: La réciprocité (Der Homo Sociabilis: Die Reziprozität).
[^21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler: Der menschliche Käfer.
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, The Primal Brain (dt. Das Urhirn).
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior.
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, Altruismus: ein Rätsel?
[^29]: Matthieu Ricard, "Altruismus: Das Gen oder die Erziehung?"
[^30]: Kathryn Spink, "Mutter Teresa: Ein Leben im Dienste anderer".
[^31]: Wolfram Schultz, The Role of Dopamine in Learning and Memory, 2007.
[^32]: Read, D., & Northoff, G, Neural correlates of impulsive and reflective decisions. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, Das Gehirn und der Genuss.
[^34]: David Eagleman, Das unbewusste Gehirn.
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 and Gloria Y. Yeh3, A Systematic Review and Meta-analysis of the Effects of Meditation on Empathy, Compassion, and Prosocial Behaviors, 2018.
[^36]: Sébastien Bohler, Wo ist der Sinn?, 2020
[^37]: [Roy F. Baumeister, The Need for Meaning: A Psychological Perspective] (https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Öko-Angst: Analyse einer zeitgenössischen Angst.
[^39]: Albert Camus, Der Mythos von Sisyphos, 1942
[^40]: [Umfrage "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, Der irrationale Mensch 2001.
[^43]: Elliot Aronson, Die Theorie der kognitiven Dissonanz.
[^44]: Elizabeth Kolbert, Das sechste Aussterben, 2015
[^45]: Robert Axelrod, Das Dilemma des Gefangenen
[^46]: Nancy Huston, Die fabulierende Spezies.
[^47]: Serge-Chritophe Kolm, Das Glück - Freiheit?
[^48]: David M. Eagleman, Incognito, 2015
[^49]: William James, The Principles of Psychology, 1980
[^50]: [Wikipedia: Selektionsfehler](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw: Die Illusion des Gedächtnisses.
[^52]: David Dunning und Justin Kruger, Unskilled and unaware of it: How difficulties in recognizing one's own incompetence lead to inflated self-assessments, 1997.
[^53]: [Festinger und Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones und Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Thinking, Fast and Slow, 2011.
[^58]: [Asch, S. E., Forming impressions of personality, 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. A constant error in psychological ratings, 1920
