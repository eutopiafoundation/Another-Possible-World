---
title: Eine andere Welt vorstellen
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

Wie kann man sich also eine neue Welt vorstellen, ein Ideal, Eutopia?

## Das System, seine Zwänge und Anreize

Der Grund, warum wir uns angesichts des Klimawandels und anderer Herausforderungen unserer Zeit derzeit in einer Sackgasse zu befinden scheinen, liegt darin, dass die Grundlagen unseres Systems Zwänge und Anreize erzeugen, die letztendlich jede wirksame Veränderung verhindern, ohne die etablierte Kohärenz zu stören, was unsere Gesellschaft dann in eine zerstörerische Trägheit führt, die scheinbar niemand mehr aufhalten kann.

Bei jeder Wahl versuchen unsere Politiker, uns von ihren Vorschlägen zu überzeugen, und auf den ersten Blick glauben sie alle, dass sie das Richtige tun, dass sie die Lösung haben, die das allgemeine Leben der Bevölkerung, für die sie sich einsetzen, verbessern könnte. Aber sie bekämpfen sich gegenseitig mit diesen Zwängen und Anreizen, und diese können nicht zu einer dauerhaften und wünschenswerten Lösung führen. So stehen sie dem System hilflos gegenüber und haben keine andere Wahl, als sich schließlich von ihm regieren zu lassen.

Sofern wir sie jemals wirklich hatten, haben wir Menschen unsere Macht verloren, aber noch schlimmer ist, dass diejenigen, die wir wählen, um uns zu vertreten, sie ebenfalls verloren haben.

Es wird angenommen, dass das politische System dominiert und alles andere ändern kann, einschließlich der Wirtschaft und ihrer Form. Dass wir uns also aufgrund des Wettbewerbs der Kandidaten, denen wir unsere Stimme geben, sehr wohl in einer Demokratie befinden.

Aber ein Wirtschaftssystem produziert nicht nur Waren und Dienstleistungen, es definiert auch die Bedingungen unserer Sozialität, produziert Menschen und Beziehungen zwischen ihnen. [^1]

Wir werden sehen, dass die Wirtschaft sogar in diese demokratische Wettbewerbswahl eingreift. Dass sie sie beeinflusst und dass das Wahlsystem dadurch nicht mehr die Qualität hat, einen guten Wettbewerb der Kandidaten zu bieten. Es ist also letztlich nicht die Politik, die die Wirtschaft dominiert, sondern die Wirtschaft, die die Politik dominiert. [^2]

Was wir Ihnen hier vorschlagen, ist, mit unserem System reinen Tisch zu machen und alles, was unsere derzeitige kollektive Vorstellungswelt ausmacht, vollständig zu verlassen. Wir laden Sie ein, von einer neuen Basis aus zu beginnen, eine Neue Welt zu entdecken, die sich von der Ihnen bekannten Welt unterscheidet, eine Welt, die auf Sinn und Logik beruht, und dann Wege und Lösungen für einen Übergang zu erarbeiten, um gemeinsam auf diese Welt hinzuarbeiten.

## Von der Basis aus starten

Was ist also die Basis?

Die Basis ist das Reale, das, was am Anfang der Existenz der Menschheit existierte, als wir uns noch keine imaginären Geschichten erzählen konnten. Die Basis ist alles, was übrig bleibt, wenn wir diese kollektive Vorstellungswelt der Menschen ausradiert haben.

Wir haben also die Erde und alles, was sie ausmacht. Insbesondere die verschiedenen Ökosysteme: Ein Ökosystem ist eine Einheit aus einer Gemeinschaft von Lebewesen, die mit ihrer Umwelt interagieren, wobei diese Lebewesen Pflanzen oder Tiere sein können. Der Planet ist ein großes System, in dem unzählige Subsysteme miteinander interagieren, die sich dynamisch gegenseitig ausbalancieren. Unter allen Lebewesen innerhalb unseres Planeten gibt es uns, Homo Sapiens, besser bekannt als Menschen.

Wir Menschen sind 7,9 Milliarden und werden laut Prognosen im Jahr 2100 11 Milliarden sein.[^3] Wir leben mittlerweile überall auf unserem Planeten, und selbst in dünn besiedelten Gebieten hat der Mensch immer noch einen Einfluss auf die Umwelt und die Ökosysteme.[^4] Das Problem ist jedoch keineswegs die Größe der Bevölkerung, sondern die Interaktionen, die wir innerhalb der Ökosysteme pflegen.

Von der Basis auszugehen bedeutet also zunächst einmal, dass wir die Basis verstehen müssen. Wir müssen die Welt und ihre Funktionsweise verstehen. Und da wir Menschen in ihr leben, werden wir auch unsere Funktionsweise verstehen müssen. Ein Teil wird sich ausschließlich mit der Erde beschäftigen und ein anderer Teil wird sich mit dem tieferen Verständnis des Menschen beschäftigen.

Wir konzentrieren uns auf das Wesentliche, sammeln es und fassen es zusammen, um die folgenden Fragen zu beantworten.

## Forschungsfragen

Sobald wir uns die Fähigkeiten angeeignet haben, die Welt und die Menschen zu verstehen, brauchen wir ein Ziel in Form einer Forschungsfrage, die unseren Wunsch nach einer idealen Welt festlegt.

Diese Forschungsfrage ist sehr wichtig, da sie die Forschungsarbeiten leiten wird, aus denen sich unsere Lösungen ergeben. Unterschiedlich formulierte Forschungsfragen führen zu unterschiedlichen Ergebnissen. Die Forschungsfragen sollten so einfach wie möglich sein, klar und präzise, aber sie sollten das Wesentliche dessen wiedergeben, was wir uns wünschen.

Wir werden uns auf unsere Spezies konzentrieren, aber wie bereits erwähnt, leben wir in Ökosystemen und werden, wenn wir die Welt erforschen, feststellen, dass wir direkt mit ihnen verbunden sind und für unser Überleben von ihnen abhängen.

Wir streben nach dem Ideal. Natürlich hängt das Ideal von den Werten, Überzeugungen, Sehnsüchten und Erfahrungen jedes Einzelnen ab. Daher könnten wir uns wünschen, dass das Ideal für jeden Einzelnen darin besteht, ein erfülltes und ausgeglichenes Leben zu führen, das seinen Bedürfnissen und Sehnsüchten entspricht.

Das ist es, was wir anstreben werden, dass jeder Mensch während seiner Zeit auf der Erde ein erfülltes Leben führen kann. Aber denken Sie daran, dass es noch 80 Millionen Generationen gibt, die noch kommen werden.

Die Forschungsfrage lautet daher wie folgt:

> Wie können wir allen Menschen heute und den 80 Millionen zukünftigen Generationen ein erfülltes Leben auf der Erde ermöglichen?
> {.is-success}

Unser großes gemeinsames Projekt besteht also darin, allen Menschen ein erfülltes Leben auf der Erde zu ermöglichen, das ihren Bedürfnissen und Wünschen entspricht, und zwar sowohl für die jetzige als auch für die kommenden 80 Millionen Generationen.

Nachdem wir verstanden haben, wie die Welt und der Mensch funktionieren, werden wir herausgefunden haben, wie ein Mensch erfüllt sein kann, wie man seinem Leben einen Sinn geben kann und wie er langfristig in seiner Umgebung auf der Erde leben kann.

Um diese Forschung zu vervollständigen, werden wir kurz die verschiedenen Ären der Menschheit nachzeichnen, denn die Vergangenheit zu kennen, gibt der Interpretation der Gegenwart Halt und Atem,[^5] und wir müssen die Gegenwart kennen, um einen möglichen Übergang in die Zukunft zu bestimmen. Denn die beste, mögliche Gesellschaft bleibt die der bestmöglichen Entwicklung der Gesellschaften von jetzt an. [^1]

Um dieses gemeinsame Ziel zu erreichen, müssen unsere 7,7 Milliarden Menschen zwei wesentliche Handlungen ausführen: zusammenarbeiten und entscheiden.

## Unterfrage: Zusammenarbeiten

Wie in jeder Gesellschaft müssen wir zusammenarbeiten, um uns die Güter und Dienstleistungen zu beschaffen, die wir brauchen, um uns zu entfalten. Unsere erste große Unterfrage lautet also

> Wie können wir es den Menschen ermöglichen, zusammenzuarbeiten, um uns gegenseitig die Güter und Dienstleistungen zu liefern, die es uns ermöglichen, dieses gemeinsame Ziel zu erreichen?
> {.is-success}

Hier handelt es sich um den Bereich der Wirtschaft. Dies ist eines der großen Probleme unseres Systems und der Bereich, von dem die meisten Zwänge und Anreize ausgehen. Bei Null anzufangen und nach der bestmöglichen Wirtschaft zu suchen, wird es uns ermöglichen, die notwendigen Grundlagen für eine mögliche ideale Gesellschaft zu definieren, indem wir die Zwänge und Anreize vermeiden, die mit ihrer Verwirklichung kollidieren. Wir werden feststellen, dass ein Wirtschaftssystem nicht nur Waren und Dienstleistungen produziert, sondern auch Menschen und die Beziehungen zwischen ihnen, die für ihre Entfaltung ebenso wichtig sind.

## Unterfrage: Entscheidungen

Schließlich werden unsere 7,7 Milliarden Menschen im Laufe ihres Lebens und der Entwicklung ihrer Gesellschaft Entscheidungen treffen, sich einigen und Entscheidungen im Großen wie im Kleinen treffen müssen. Dies wird unsere zweite große Unterfrage sein:

> Wie können wir gemeinsam Entscheidungen treffen und diese Entscheidungen dazu führen, dass wir unser gemeinsames Ziel erreichen?
> {.is-success}

Wie Sie aus der Frage ersehen können, geht es nicht nur darum, eine Einigung zu erzielen, denn was nützt es, wenn alle oder ein Teil der Bevölkerung einer Entscheidung zustimmen und diese bestätigt wird, wenn sie letztendlich nicht dazu führt, dass wir unser gemeinsames Ziel erreichen.

Dieser Bereich ist die Wissenschaft der Entscheidungsfindung und wird auf viele verschiedene Praktiken angewandt, insbesondere auf die Politik, wo wir derzeit eine repräsentative Demokratie anwenden.

Wir werden uns eingehender mit den verschiedenen Möglichkeiten befassen und dabei berücksichtigen, was wir über die Funktionsweise des Menschen und der Welt gelernt haben.

## Schlussfolgerung

Zusammenfassend werden wir also zunächst unseren Lebensraum, die Erde, ihre Funktionsweise, ihre Ökosysteme und unseren Platz darin verstehen. Dann werden wir den Menschen, seine Funktionsweise und seine Bedürfnisse gründlich verstehen, um ihm während seiner Zeit auf der Erde ein erfülltes Leben zu ermöglichen.

Dieses Wissen wird es uns ermöglichen, eine effektive Antwort auf die Frage zu finden, wie wir Menschen in die Lage versetzen können, Entscheidungen zu treffen und zu kooperieren, um das gemeinsame Ziel zu erreichen, das in der nachhaltigen Erfüllung jedes Einzelnen von ihnen besteht.

Die Funktionsweise der Erde in Kapitel 2, das Verständnis des Menschen in Kapitel 3 und die Bestandsaufnahme der Zeitalter der Menschheit in Kapitel 4 werden Ihnen vielleicht langweilig erscheinen, wenn Sie diese Bereiche bereits kennen oder wenn Sie zunächst nicht zu tief in die Materie einsteigen möchten. In diesem Fall empfehlen wir Ihnen, direkt zu Kapitel 5 und 6 überzugehen, in denen Sie eine neue Art der Zusammenarbeit und Entscheidungsfindung kennenlernen werden. Diese beiden Teile sind wichtig, weil sie begründen, warum die Gesellschaft, die wir uns vorstellen, möglich ist, indem sie die Grundlagen schaffen, die sie kohärent machen. Die Kapitel 2, 3 und 4 sind jedoch nicht weniger wichtig und führen zur Umsetzung und Rechtfertigung der Lösungen in Kapitel 5 und 6. Falls Sie es wünschen, wurden Zusammenfassungen erstellt und [sind hier verfügbar](/de/4-Appendices/1Summaries).

> "Wissen ist Macht. Je mehr wir die Welt und uns selbst verstehen, desto mehr haben wir die Fähigkeit, sie zum Besseren zu verändern." Malala Yousafzai

## Quellen (Nicht erschöpfende Liste)

[^1]: Serge-Christophe Kolm, Die gute Wirtschaft. 1984
[^2]: Serge-Christophe Kolm, Sind Wahlen Demokratie? 1977
[^3]: [Weltbevölkerungsausblick 2022", herausgegeben von den Vereinten Nationen] (https://desapublications.un.org/file/989/download)
[^4]: [IPCC-Berichte 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Plouc Pride: Eine neue Erzählung für die Kampagne. 2021
