---
title: Der Planet Erde
description:
published: true
date: 2023-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:09:19.311Z
---

> Diese Seite befindet sich noch im Aufbau, daher kann es sein, dass einige Teile fehlen.
> {.is-info}

## Kurzbeschreibung der Erde

### Die richtige Weltkarte

[!!! Gall-Peters-Projektion](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

Die Peters-Projektion (oder Gall-Peters-Projektion nach James Gall (1808-1895) und Arno Peters (1916-2002)) ist eine kartografische Projektion, die im Gegensatz zur Mercator-Projektion die tatsächliche Fläche der Kontinente berücksichtigt. Lokal behält diese Projektion jedoch nicht die Winkel bei, was sich im Gegensatz zur Mercatorkarte in der Verformung der Kontinente niederschlägt. [^1]

### Kurzbeschreibung der verschiedenen Ökosysteme

## Der Wasserkreislauf

### Vorstellung der verschiedenen Wasserformen auf der Erde

### Beschreibung des Wasserkreislaufs und seiner Wechselwirkungen mit der Umwelt

### Auswirkungen menschlicher Aktivitäten auf den Wasserkreislauf

## Photosynthese und die Produktion von Sauerstoff

### Beschreibung der Photosynthese und ihrer Rolle bei der Sauerstoffproduktion

Seit ihrer Entstehung hat die Erde ständig Informationen geschaffen. Dank ihr entstanden geordnete Bewegungen der Materie, die zu einer außergewöhnlichen Vielfalt an Formen, Farben und Bewegungen führten: das Leben, wie wir es kennen.

Von diesen verfügbaren Informationen war eine mehr als jede andere treibende Kraft: Es handelt sich um diejenige, die in den Genen der Pflanzen kodiert ist, die die Mechanismen der Photosynthese tragen. Dieser für das Pflanzenreich spezifische Prozess macht das lebende System besonders effizient und hat keine Entsprechung. Die Photosynthese besitzt sowohl eine energetische als auch eine chemische Kraft. Sie ermöglicht es, eine rohe und immaterielle Energie, die Lichtenergie, einzufangen, zu speichern und auf extrem feiner Ebene, auf molekularer Ebene, d. h. unterhalb eines Nanojoule, wieder zu verteilen. Sie ermöglicht es auch, verstreute mineralische Elemente zu Materialien und Molekülen zusammenzufügen, die ihrerseits Informationen in ihren Interaktionen und Energie in ihrer Zusammensetzung tragen.

Dies ist im Hinblick auf die physikalischen Gesetze der Energie ein wunderbares Phänomen. Während diese besagen, dass die von einer Quelle ausgestrahlte Energie sich zerstreut und abbaut und immer weniger nutzbar wird, ist die Pflanze in der Lage, sie zu kanalisieren. Dank der Informationen in ihrer genetischen Bibliothek widerspricht die Pflanze den physikalischen Gesetzen der Energie, die als Entropie bezeichnet werden. Stattdessen bildet sie eine negentropische Schleife innerhalb des Universums: Die Materie ordnet sich, organisiert sich, die Energie wird kanalisiert. Bevor sie auf ihrer Reise durch das Lebendige wieder zerfällt. Im Vergleich zur gemeinsamen Materie des Universums - die unendlich viel größer ist - ist die organische Materie wie eine Spitze: Sie kann jede Form annehmen. Es ist diese Ansammlung von Molekülen in Form von Haken, Rollen, Rädern, Kabeln, Türen, Punkten und Ketten, die das Lebendige ausmachen.[^2].

### Auswirkungen menschlicher Aktivitäten auf die Photosynthese und die Sauerstoffproduktion

## Der Kohlenstoffkreislauf

### Darstellung des Kohlenstoffkreislaufs und seiner Wechselwirkungen mit der Umwelt

### Auswirkungen menschlicher Aktivitäten auf den Kohlenstoffkreislauf und die Treibhausgase

Darüber hinaus verändern die Materialien und Energien, die uns solche Leistungen ermöglichen, den globalen Thermostat, der überwiegend aus Kohlenstoff besteht, und reichern die Atmosphäre bei ihrer Verwendung mit Treibhausgasen an. Und die Atmosphäre heizt sich auf. Der Kalkstein, der Zement, Gips und Kalk zugrunde liegt, ist ein kohlenstoffhaltiges Gestein; Öl, Kohle und Gas, die 80% der von uns genutzten Energiequellen oder 100% des Bitumens, auf dem wir uns bewegen, ausmachen, sind kohlenstoffhaltige Gesteine. Dieser Kohlenstoff war in der ursprünglichen Erdkruste nicht vorhanden, er war in der Atmosphäre konzentriert. Sein Übergang von der Atmosphäre in die Erdkruste erfolgte durch die Wirkung von Lebewesen, die ihn aufnahmen und auf dem Meeresboden oder in den geologischen Schichten der Erde und der Lagunen deponierten. Wir setzen in wenigen Jahrzehnten Kohlenstoff frei, den Lebewesen in Hunderten von Millionen Jahren vergraben haben. Wir destabilisieren ein System, dessen Gleichgewicht die Grundlage für die meisten der heutigen entwickelten Lebensformen, einschließlich unserer eigenen, bildet.[^3].

## Biodiversität

### Darstellung der Bedeutung der Biodiversität für das Leben auf der Erde

### Auswirkungen menschlicher Aktivitäten auf die Biodiversität

Mehr als die Hälfte der Wälder und Feuchtgebiete der Welt sind in einem Jahrhundert verschwunden [^2].

## Die Böden

### Darstellung der Bedeutung von Böden für das Leben auf der Erde

### Auswirkungen menschlicher Aktivitäten auf die Böden

Ein Drittel der Böden auf der Erde ist degradiert. [^4]

## Natürliche Ressourcen

### Vorstellung der verschiedenen natürlichen Ressourcen (Wasser, Luft, Boden, etc.)

### Nachhaltiger Umgang mit natürlichen Ressourcen

### Nachhaltige Alternativen für natürliche Ressourcen

## Ökosysteme an Land und im Meer

### Vorstellung der verschiedenen Land- und Meeresökosysteme und ihrer Bedeutung

### Auswirkungen menschlicher Aktivitäten auf die Ökosysteme

## Quellen

[^1]: [Wikipedia: Peters-Projektion](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, L'économie symbiotique, Actes sud | Colibri, 2017.
[^3]: [Ernährungs- und Landwirtschaftsorganisation der Vereinten Nationen (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Ernährungs- und Landwirtschaftsorganisation der Vereinten Nationen (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
