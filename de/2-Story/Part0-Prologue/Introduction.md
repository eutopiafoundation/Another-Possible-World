---
title: Einführung
description: Beschreibung:
published: true
date: 2023-03-07T21:16:06.598Z
tags:
editor: markdown
dateCreated: 2023-03-07T20:33:18.121Z
---

## Wer bist du?

Vielleicht sind Sie kein großer Soziologe, kein großer Philosoph, kein großer Ökonom oder sonst etwas, vielleicht haben Sie auch keine große Universitätsausbildung absolviert, vielleicht waren Sie sogar schlecht in der Schule. Trotz dieser unterschiedlichen Bildungswege haben wir das Vertrauen, Ihnen hier eine andere mögliche Welt vorzuschlagen, mehr noch, eine Welt, die nach dem Ideal strebt, und wir laden Sie ein, sich uns anzuschließen.

Dann fragen Sie sich vielleicht, warum Sie für ihre Verwirklichung gut geeignet sein sollen. Und vielleicht verspürt ein Teil von Ihnen schon beim bloßen Lesen dieses Absatzes den Drang, dieses Buch zu schließen, um nicht noch mehr Zeit zu verlieren. Aber wir bitten Sie, uns noch ein paar Absätze zu gestatten, um zu versuchen, Sie zu überzeugen.

## Die Vorstellungskraft

Obwohl Sie in keinem der großen Bereiche dieser Welt ein großer Experte sind, gibt es eine Sache, für die Sie besonders begabt sind, diese Gabe, und das ist das Träumen. Diese Fähigkeit, sich etwas vorstellen zu können, ist die größte Gabe des Menschen. Wir laden Sie hier zum Träumen ein, aber vor allem laden wir Sie ein, zu kommen und andere zum Träumen zu bringen.

Am Anfang waren wir nur eine unbedeutende Spezies und unsere Handlungsfähigkeit war sehr begrenzt. Was unsere Spezies, den Homo Sapiens, von anderen unterscheidet, ist unsere Fähigkeit, in großer Zahl zu kooperieren. [^1]

Und diese verdanken wir unserer Vorstellungskraft, der Fähigkeit, Geschichten zu erfinden und sie uns zu erzählen.

Dank kollektiver Mythen, die man auch als "Fiktionen" bezeichnen kann, konnten wir uns zusammenfinden und kooperieren.

Wir haben die Fähigkeit, an Dinge zu glauben, die es nicht gibt, wir haben zahlreiche Legenden, Mythen und Erzählungen erfunden, wir haben imaginäre Linien auf der Welt gezogen, Nationen und Bräuche geschaffen, wir haben das Geld erfunden und ihm den höchsten Wert in unserer Vorstellungswelt verliehen.

Diese Gabe der Vorstellungskraft kommt von unserem Bedürfnis nach Sinn, dem Bedürfnis, das wir haben, unsere Umwelt vorherzusagen und zu verstehen, um besser in ihr überleben zu können.[^2].

Wir lassen zu, dass diese kollektive Vorstellungswelt unser gesamtes Leben bestimmt, wir lassen sie sogar das Schicksal der gesamten Menschheit bestimmen, ihre Macht ist so groß, dass sie uns sogar dazu bringt, unsere eigene Umwelt zu zerstören, unsere eigene Zerstörung herbeizuführen.

Aber all das bleibt im Grunde genommen nur eine kollektive Einbildung und hört in dem Moment auf zu existieren, in dem wir alle beschließen, nicht mehr daran zu glauben.

In Wirklichkeit ist das, was wir gemeinhin als Vernunft bezeichnen, viel häufiger die Tatsache, dass wir weiterhin glauben und uns an die Regeln unserer kollektiven Vorstellungswelt halten.

## Die Vernunft

Sobald wir uns dazu entschließen, diese Vorstellungswelt zu verlassen, ist dies der Punkt, an dem wir endlich wirklich die wahre Vernunft finden können.

Sie werden aber feststellen, dass man nicht viel wissen muss, um Lösungen zu finden und Entscheidungen zu treffen, die uns alle zu einem erfüllten und sinnvollen Leben führen.

Denn die Antworten auf unsere Fragen sind bereits in jedem von uns vorhanden. Um sie zu finden, müssen wir nur unsere kollektive Vorstellungswelt verlassen, unser Herz öffnen und ihm zuhören. Das mag Ihnen vielleicht albern vorkommen, aber wir sind davon überzeugt, dass genau hier der wahre Grund liegt.

Und wir, die wir bereits mit diesem Dekonstruktionsprozess begonnen haben, schlagen Ihnen vor, hier eine Welt zu entdecken, die wirklich auf Vernunft beruht.

Eine Welt, die aufgrund ihrer Einfachheit, ihrer Bedeutung und ihrer Logik auch Ihnen einleuchtend erscheinen wird.

Sie werden dann entdecken, dass auch Sie, wer auch immer Sie sind, was auch immer Ihr Wissen ist, nicht viel wissen müssen, um zu beurteilen, was gut und was nicht gut ist, dass Sie nicht viel brauchen, um zur Gestaltung dieser Welt beizutragen.

> "Aber dann", sagte Alice, "wenn die Welt absolut keinen Sinn hat, wer hindert uns dann daran, einen Sinn zu erfinden?" Inspiriert von Lewis Carrol

<!-- > "Jede Innovation durchläuft in der öffentlichen Meinung drei Phasen: 1) Es ist lächerlich 2) Es ist gefährlich 3) Es ist offensichtlich." Apokryphes Zitat von Arthur Schopenhauer -->.

## Eutopie

Dann sagen Sie sich vielleicht, dass das, was wir Ihnen hier vorschlagen, eine Utopie ist.

Utopie ist ein Wort, das aus dem Altgriechischen konstruiert wurde, abgeleitet von "tópos" ("Ort") mit der Vorsilbe "ou" ("nicht"), wörtlich "Nicht-Ort".[^3]

Utopie ist ein Wort, das oft missbraucht und missbraucht wird. Es wird hochgehalten, um die Hoffnung zu ersticken, die Fantasie zu verunglimpfen, den Fortschritt zu verdrängen, es ist ein Aufruf zur Resignation, ein Schlüsselwort des Systems der Ausbeutung, Entfremdung und Herrschaft, um den Status quo zu akzeptieren.[^4].

Aber ein "Nicht-Ort" ist eine Gesellschaft, die nicht existiert, nicht eine unmögliche Gesellschaft. Und wir können es in der Geschichte sehen, die Zukunft der Gesellschaft von morgen ist immer anders und neu. Letztendlich ist die Gesellschaft von morgen immer eine Utopie von heute.

Utopie wird manchmal dem Begriff "wissenschaftlich" gegenübergestellt. Dies zeugt jedoch von Unkenntnis nicht nur des Griechischen, sondern vor allem des Wesens der Wissenschaft selbst.

Die Suche nach möglichen Nichtexistenzen, die Suche nach realisierbaren Anderswo, die Analyse realistischer Utopien ist sehr wohl eine wissenschaftliche Tätigkeit. Um zu wissen, was von dem Unbeobachteten möglich ist und was nicht, muss man ein Wissen, ein tiefes Bewusstsein über die Natur und die Struktur der Welt haben.

Es ist tatsächlich eine wissenschaftliche Haltung, die wir hier einnehmen, die darin besteht, alles in Frage zu stellen und zu hinterfragen, im Grunde erklären zu wollen, nach den tatsächlichen Ursachen zu suchen, Zweifel und systematische Kritik an erhaltenen Begründungen zu üben und nichts als gegeben, selbstverständlich oder unveränderlich zu akzeptieren. In diesem Modus des Schreibens werden wir zu einer radikalen Infragestellung unserer Gesellschaft geführt. Wissenschaft bedeutet, die Augen zu öffnen.

Dann ist die Untersuchung möglicher Gesellschaften im Vergleich zu realisierten, gegenwärtigen oder vergangenen Gesellschaften wenig verbreitet. Einige Disziplinen und Autoren können sich nicht einmal vorstellen, dass sie eine berufliche Dimension haben könnte, oder sie weisen die Idee sogar mit Inbrunst zurück. Vielleicht wollen diese Intellektuellen damit eine Beschränkung ihrer Vorstellungskraft rechtfertigen.

Unser System mag zwar schrecklich schlecht sein, aber wir wissen alle, dass es nicht das schlimmstmögliche ist. Eine radikale Veränderung könnte zu einer noch unangenehmeren Gesellschaft führen, und so ist die Masse der Bevölkerung mit dieser begründeten Angst konservativ oder vorsichtig reformorientiert. Anders kann es nur werden, wenn die Menschen eine ziemlich genaue und zuverlässige Vorstellung davon haben, wie das neue und beste System aussehen könnte.

Aus diesem Grund muss die Analyse einer möglichen anderen Gesellschaft hinreichend ernsthaft, durchdacht, präzise und umfassend sein. Und das Gleiche gilt für die Schritte zu ihrer Umsetzung, Schritte des Übergangs von der bestehenden zur neuen.

Denn da das, was man für möglich hält, von der Analyse abhängt, müssen die Menschen von diesem Projekt überzeugt sein. Erst dann werden sie es als abgeschlossen betrachten und so den Marsch in eine bessere Gesellschaft zu einer Notwendigkeit für die Entwicklung ihrer eigenen machen. [^4]

> "Gewiss findet man in der Analyse der verwirklichten Gesellschaften die notwendigen Kenntnisse, um die anderen aufzubauen; aber dann muss man aufhören, die Zutaten anzuhäufen, ohne jemals die Küche zu kochen." Christophe Kolm

Also in ihrer aktuellen Definition, wie sie von unserer Gesellschaft wahrgenommen wird, ist die Utopie ein Ideal, eine Vorstellung, die die Realität nicht berücksichtigt und daher nicht realisierbar ist.

In diesem Fall werden wir hier keine Utopie schaffen.

Die Realität ist nicht unsere kollektive Vorstellungswelt. Die Realität sind Schwellenwerte, die wir nicht überschreiten dürfen, um die günstigen Bedingungen für unser Überleben nicht zu gefährden. Das ist die maximale CO2-Konzentration in unserer Atmosphäre, das ist die Grenze der Störung der biochemischen Zyklen von Phosphor und Stickstoff, die für einen guten Zustand der Böden und Gewässer unerlässlich sind, das ist die maximale Erosion der Biodiversität, d. h. das Aussterben von Arten und ihrer wesentlichen Rolle innerhalb der Ökosysteme, das ist die intensive Nutzung der Böden, der Missbrauch von Süßwasser, die Versauerung der Ozeane etc...[^5][^6][^7][^8][^9]

Dies ist ein Teil dessen, was die Realität ausmacht, was z. B. nicht für den Wert gilt, den die kleinen Zahlen auf der Seite unseres Bankkontos verkünden.

In der Tat sind wir mitten in der Utopie, zu glauben, dass unsere Gesellschaft auf lange Sicht unbegrenzt so weiterleben kann.[^11].

Um die ideale Welt zu entwerfen, müssen wir unsere Vorstellungswelt verlassen und von Grund auf neu beginnen, damit wir die Probleme an der Wurzel packen können. Denn nur so können wir wirklich eine andere Welt aufbauen, die Sinn macht, eine andere Welt, die wirklich möglich ist.

Wir bevorzugen daher einen anderen Begriff, um uns zu unterscheiden. Was wir hier schaffen wollen, ist eine Eutopie, ein Konzept, das der Utopie ähnelt, aber in der Realität als realisierbar angesehen wird. Das Präfix "u" wird zu "eu", was im Griechischen "gut" bedeutet, also wortwörtlich der "gute Ort".[^12].

Was freie Menschen leitet, ist ihre Vorstellung von der Zukunft, dem Möglichen, dem Wünschenswerten, es ist ihre Utopie, das Eutopia. Und da der Mensch viel häufiger von der Hoffnung angezogen als von der Verzweiflung getrieben voranschreitet, erschafft der Traum schließlich das Reale. Unser Wunsch ist es, dass die Realität zu Eutopia wird.

> "Die Utopie hat die Seiten gewechselt, Utopist ist, wer glaubt, dass alles so weitergehen kann wie bisher." Pablo Servigne und Raphaël Stevens[^11]

## Einen Schritt zurücktreten

Wir müssen unsere Wahrnehmung von Kurz- und Langfristigkeit hinterfragen. Sollten wir 100 Jahre als kurzfristig oder langfristig definieren? Alles ist eine Frage des Standpunkts, aber es ist notwendig, einen Schritt zurückzutreten und die Skala unseres kurzen Lebens zu verlassen.

Die Erde wurde vor 4 Milliarden Jahren gebildet, das Leben entstand vor 500 Millionen Jahren, die Hominia, die Linie der Menschen, trennte sich vor 7 Milliarden Jahren von der Linie der Schimpansen, aber aus dieser Linie entstand vor nur 300.000 Jahren unsere Spezies: Homo Sapiens.[^13].

In 5 bis 7 Milliarden Jahren wird die Sonne ihr Helium verbraucht haben und sich in einen Roten Riesen verwandeln, der seinen Radius um das 1.000-fache vergrößert und die Erde in seinen heißen Halo zieht. Die Temperatur auf der Erde wird dann so hoch sein, dass die Ozeane verdampfen werden. Doch schon lange vorher wird das Leben aufgrund von Sauerstoffmangel verschwunden sein.[^14].

In Wirklichkeit bleiben den Menschen noch 2 Milliarden Jahre, um auf einem bewohnbaren Planeten zu leben.

Es ist wichtig, sich diese Zahlen auf einer Skala zu vergegenwärtigen, um sie richtig zu verstehen.

2 Milliarden Jahre sind visuell gesehen 2 000 000 000 Jahre.

Wenn wir eine Generation pro 25 Jahre betrachten, sind das immer noch 80.000.000 Generationen. Und wir sind derzeit nur die 12.000ste..... Nur 0,015% unseres Lebens auf der Erde sind vergangen.

Das Ende der Erde bedeutet jedoch nicht das Ende der menschlichen Spezies, das Universum ist groß... Aber dieser Teil wird Platz für weitere Fantasiegeschichten lassen... Denn es wäre an der Zeit, erst einmal zu lernen, wie man dauerhaft auf unserem eigenen Planeten überlebt, bevor wir hoffen, auf anderen Planeten überleben zu können. Was derzeit nicht der Fall ist.

Wir stehen also erst am Anfang unserer Geschichte, und wenn man bedenkt, wie schnell wir die Ressourcen unseres Planeten verbrauchen und in welchem Verhältnis wir zu seinen Ökosystemen stehen, können wir nicht erwarten, dass wir uns lange halten können...

Bei dem Tempo der weltweiten Produktion werden wir das gesamte Öl in 51 Jahren verbraucht haben, das gesamte Gas in 53 Jahren, die gesamte Kohle in 114 Jahren, das gesamte Uran in 100 Jahren, und wir geben nicht länger für alle seltenen Erden, die für die verschiedenen Technologien von heute unerlässlich sind. [^11]Und wir hoffen, dass wir 2.000.000.000?

Daher ist es so wichtig, einen Schritt zurückzutreten, über unser kurzes menschliches Leben hinauszublicken und langfristig zu denken. Wir werden später darauf zurückkommen, aber wir müssen uns bewusst sein, dass wir uns so sehr daran scheitern, weil das Gehirn des Homo Sapiens nicht dafür ausgelegt ist, langfristig zu denken. [^15]

Was wir hier retten wollen, ist die menschliche Existenz und mit ihr die verschiedenen Arten, die unser Ökosystem bilden.

## Wenn der Traum Wirklichkeit wird

Vielleicht fragen Sie sich, wie die Vorstellung einer anderen Welt dazu beitragen kann, das Bestehende zu verändern.

Schließlich könnte man sagen, dass es besser wäre, unsere Zeit und Energie darauf zu verwenden, das Bestehende zu verändern, anstatt sich eine neue Welt vorzustellen.

Aber das, was passiert, ist nicht unabhängig von dem, was man versucht. Was man versucht, hängt davon ab, was man für möglich hält. Und was man für möglich hält, hängt von der Analyse ab.

Die Prinzipien und Werte, auf denen die Grundlagen der Welt beruhen, die wir Ihnen vorstellen, wurden von Intellektuellen nur sehr wenig entwickelt und analysiert. Diese Analyse wird es Ihnen ermöglichen, diese Vision für möglich zu halten und sie dann auch zu versuchen. Und damit letztlich auch, das Bestehende zu verändern. Diese Analyse muss so kohärent wie möglich sein, damit sie von möglichst vielen Menschen unterstützt wird und zu einer sinnvollen und weitreichenden kollektiven Aktion führt.

Wenn wir uns eine neue Welt vorstellen, entdecken wir im Vergleich dazu die Absurdität der gegenwärtigen, was zu einer tiefgreifenden Infragestellung der Welt um uns herum, aber auch von uns selbst führt.

Indem wir uns eine neue Welt vorstellen, schaffen wir einen Stoff, einen Raum zum Nachdenken und für Debatten auf der Suche nach einem gemeinsamen Ideal.

Indem wir uns eine neue Welt vorstellen, versammeln wir Menschen um ein gemeinsames Ideal, wodurch sie sich zusammenschließen können, um aktiv zu werden und das Bestehende zu verändern.

Wenn wir uns eine neue Welt vorstellen, können wir uns von ihr inspirieren lassen, um ihre Werkzeuge und Prinzipien zu produzieren, sie in der Realität umzusetzen, mit ihnen zu experimentieren, über sie zu iterieren und sie zu verbessern.

Schließlich wecken wir durch die Vorstellung einer anderen Welt Träume und Hoffnung, eine notwendige Emotion, die die Grundlage für jede große Revolution bildet.

Wir haben Ihnen von dieser menschlichen Gabe erzählt, der Fähigkeit, sich etwas vorzustellen und Geschichten zu erzählen.

Nun ist es an der Zeit, erneut die Geschichte zu ändern, um die Geschichte zu ändern. Wir müssen uns von einem gemeinsamen Ideal leiten lassen, die Erzählung ändern, unsere Überzeugungen modifizieren, damit wir uns mobilisieren und zusammenarbeiten können, um unsere Zukunft zu verändern.

> "Zu allen Zeiten waren es Geschichten, Erzählungen, die philosophische, ethische und politische Veränderungen am stärksten getragen haben... Es sind also die Erzählungen, durch die wir eine echte "Revolution" einleiten können". Kleines Handbuch des zeitgenössischen Widerstands[^24]]

Beim Lesen sollten Sie nicht denken: "Das ist unmöglich", denn Sie haben wahrscheinlich recht, unter den gegenwärtigen Bedingungen ist es unmöglich, und wir schlagen nicht vor, diese Erzählung jetzt sofort umzusetzen. Aber Sie sollten sich sagen: "Es könnte möglich sein".

Dann müssen Sie sich fragen: "Was ist jetzt möglich, um dieses Ideal anzustreben?", und so die Schritte des Übergangs bestmöglich gestalten. Dazu muss man aber erst einmal wissen, was das Ideal ist und wohin man will.

Es genügt, sich anzusehen, was man jetzt tun kann und welche neuen Möglichkeiten sich durch die Umsetzung eröffnen.

Es geht hier nicht darum, Ihnen zu sagen, was Sie tun sollen oder wie Sie es tun sollen, sondern darum, eine Vision zu entwickeln, die es Ihnen ermöglicht, es selbst zu entdecken und sich mit den richtigen Hilfsmitteln dabei zu helfen.

Diese Vision ist nicht starr, sondern wird von Ihnen entwickelt und soll sich im Laufe der Zeit mit den Fortschritten und Entdeckungen weiterentwickeln.

Es ist also durchaus möglich, dass unser System zum Zusammenbruch verurteilt ist, egal, was wir dagegen tun. Das System scheint in seiner zerstörerischen Trägheit gegenüber seiner Umwelt gefangen zu sein und außer Kontrolle zu geraten.

Es gibt viele Bücher über Kollaps-Theorien, die auf sehr ernsthaften Studien basieren, aber wenn es zu einem solchen Kollaps kommen sollte, werden wir nicht von 7 Milliarden auf einen Menschen schrumpfen, sondern es wird immer noch genug geben, um eine Gesellschaft neu aufzubauen. Der Zusammenbruch wird nicht über Nacht stattfinden, er wird allmählich mit Wellenbewegungen verlaufen, und es wird immer möglich sein, während des Zusammenbruchs die Richtung zu ändern. [^16]

In beiden Fällen ist die Arbeit, die hier geleistet wird, gleichermaßen sinnvoll: Entweder gelingt es uns, die Gesellschaft rechtzeitig zu wechseln, um den Zusammenbruch zu verhindern, oder wir bereiten uns auf die Resilienz vor, um während des Zusammenbruchs weiterleben zu können, und schaffen gleichzeitig eine solide Grundlage für den Start einer neuen Gesellschaft.

Denn der Zusammenbruch wird nicht den Weg für eine brandneue, einsatzbereite Gesellschaft freimachen, sondern es werden die Menschen zu diesem Zeitpunkt sein, die die Wege bauen und die Richtungen ausarbeiten müssen, die wir gemeinsam teilen.

Unsere Unvorbereitetheit könnte daher das Feld für wortkarge und hilflose Versammlungen freigeben. Auch Selbstverwaltungssysteme können nicht ohne vorherige Tests eingerichtet werden, da diese mit großer Wahrscheinlichkeit zu zahlreichen Fehlschlägen führen werden. Wenn es nicht gelingt, eine kollaborative und demokratische Entscheidungsfindung auf allen Ebenen der Gesellschaft zu etablieren, könnte die Notsituation anderen Herrschaftssystemen Platz machen, die nicht wünschenswert sind. Aus diesem Grund sollten diese neuen Wege schon heute erforscht werden. [^4]

Trotz allem werden wir hier weiterhin davon überzeugt sein, dass wir den Zusammenbruch verhindern können. Dieser würde zu einer humanitären und ökologischen Katastrophe führen, die umso schwerer zu beheben wäre, weshalb wir ihn nicht als wünschenswert bezeichnen können, auch wenn er Platz für Neues schaffen würde. Wir werden dann alles tun, um Lösungen zu suchen und zu entwickeln, mit denen sie verhindert werden kann. Wir dürfen nie vergessen, dass selbst wenn das System mächtig ist, unsere kollektive Vorstellungskraft genauso mächtig ist wie das System, denn es ist genau diese Vorstellungskraft, auf der das System existiert und Widerstand leistet.

Schließlich kann die Konstruktion dieser Vision, selbst wenn sie nicht akzeptiert und gewollt werden sollte, enthüllen, wie die Welt sein könnte. Sie kann den Menschen bewusst machen, dass sie nur in einem riesigen Spiel mit Regeln spielen, die Anreize schaffen, und dass ihr Leben, ihre Möglichkeiten und ihre Privilegien ganz anders aussehen würden, wenn man die Regeln ändern würde.

> "Wenn du ein Schiff bauen willst, versammle nicht deine Männer und Frauen, um ihnen Befehle zu erteilen, jedes Detail zu erklären, ihnen zu sagen, wo sie jedes Ding finden können. Wenn du ein Schiff bauen willst, dann wecke in den Herzen deiner Männer und Frauen die Sehnsucht nach dem Meer." Antoine de Saint Exupéry

## Der Kampf der Übergangserzählungen

Wir haben bereits gesehen, dass es das Imaginäre ist, das das Reale schafft, und seit jeher sind es Erzählungen und Geschichten, die unsere Gesellschaften weiterentwickelt haben. Sie glauben an eine hypermoderne Erzählung und an den Aufbau der Erzählung des Übergangs.

Sie stellen sich eine Zukunft vor, in der wir in künstlich geschaffenen Städten leben, die mit Technologien vollgestopft sind, die insbesondere den verheerenden Auswirkungen des Klimawandels entgegenwirken sollen. Unsere Nahrung würde zunehmend industriell, künstlich und synthetisch hergestellt werden. Wir würden in einen Instant-Konsum eintauchen, der von einer Informations- oder eher Überwachungswirtschaft unterstützt wird. Wir wären Mieter von allen, wir würden nichts mehr weglegen, aber wir wären "glücklich".

Aber das hypermoderne Narrativ und die Konstruktion des Narrativs des Übergangs sind ins Stocken geraten. Diese Moderne, die auf dem Ideal eines unendlichen materiellen Fortschritts beruht, der mit einem sozialen Fortschritt für alle korreliert, ist in eine mehrdimensionale Krise geraten und völlig dissonant geworden. Das Narrativ der Hypermoderne hat das Ideal des Fortschritts instrumentalisiert und es gleichzeitig seines emanzipatorischen sozialen Wesens beraubt. Es ist manipulativ und wird medial unterstützt, insbesondere durch die Entstehung einer Neusprache. Wörter werden von ihrer ursprünglichen Bedeutung entfremdet, ihrer Substanz beraubt und nach versteckten Interessen ausgerichtet. Die Hypermoderne ist die Hyperakkumulation, der Hyperkonsum, der aus einem Verlangen entsteht, das durch eine aufdringliche Werbung unersättlich gemacht wird. Der moderne Mensch, der alles hat, ist paradoxerweise der unzufriedenste aller Menschen. [^17]

Die hypermoderne Erzählung erleidet drei große Misserfolge, die jedermann allmählich deutlich wahrnimmt. Die erste ist ihr ökologisches Versagen und das Versprechen eines grünen Kapitalismus, d.h. eines lebensfreundlichen Produktivismus, erscheint zunehmend unglaubwürdig. Der zweite Misserfolg ist wirtschaftlicher Natur: Die Aneignung und die Ungleichheit auf allen Ebenen, von der lokalen bis zur globalen, nehmen exponentiell zu, trotz eines außergewöhnlichen materiellen Überflusses. Das dritte Versagen ist ethischer Natur: Unsere sozialen Beziehungen verwandeln sich in Handelsbeziehungen, aus denen jeder als Gewinner hervorgehen will, alles wird gekauft und alles verkauft. Diese Ungleichgewichte führen zu Gewalt und sind nicht länger tragbar.

Als Alternative zu den hypermodernen Erzählungen entsteht eine Übergangsgeschichte, die uns in ein neues Zeitalter, das Zeitalter der Noosphäre, eintauchen lässt. Aber diese ist noch lange nicht gebaut und noch weit davon entfernt, wünschenswert zu sein. Genau das ist das Ziel des Eutopia-Projekts, nämlich diese neue Erzählung aufzubauen...

Dann gilt es zu verstehen, wovon die Menschen wirklich träumen? Diese Träume dürfen nicht vernachlässigt werden, denn die Vorstellungskraft ist mächtig und wird der erste Schritt zur Verwirklichung sein. Träumen wir zum Beispiel davon, uns immer und überall wie im Urlaub zu fühlen? Was ist also unser kollektives Ideal? Diese Frage muss dringend beantwortet werden, um der Bewegung einen Sinn zu geben und neue Strategien zu entwerfen und umzusetzen.

> Zwischen uns und dem Zusammenbruch müssen die Bollwerke der Brüderlichkeit und der kreativen Vorstellungskraft bestehen bleiben, um ein besseres Morgen zu gestalten. Modifiziertes Zitat von Valérie Jousseaume[^17]

## Kollektive Intelligenz

Niemand ist individuell im Besitz der Wahrheit, aber gemeinsam ist es zumindest möglich, sich ihr anzunähern.

Sie werden im Gegenteil dazu ermutigt, nichts zu glauben, nie zu glauben, die Wahrheit zu besitzen, immer einen Teil des Zweifels in sich zu behalten.

Denn zu glauben, die Wahrheit zu besitzen, bedeutet, sich selbst davon abzuhalten, sie zu suchen, und damit auch davon abzuhalten, sie zu finden.

Aus diesem Grund kann man sich diese Welt nicht alleine vorstellen, wir brauchen die größtmögliche Anzahl an Menschen. Um sie zu verwirklichen, müssen wir die kollektive Intelligenz nutzen und ihre Werkzeuge entwickeln.

Wenn Sie diese Zeile lesen, bearbeiten wir diesen Text derzeit mit einem Tool namens Git, das es ermöglicht, den Fortschritt dieser Erzählung zu versionieren. Und es ermöglicht Ihnen auch, sich an der Bearbeitung von Teilen dieser Geschichte zu beteiligen, sie einzureichen, damit dann alle Mitarbeiter eine Meinung abgeben, diskutieren und schließlich darüber abstimmen können, ob die Änderung übernommen werden soll oder nicht. Sobald die Änderung bestätigt wurde, wird sie direkt hier online angezeigt, genau dort, wo Sie gerade lesen.

Dieses Tool ist zeitlich begrenzt. Die Idee ist, eine Plattform zu entwickeln, die ganz der kollaborativen und demokratischen Bearbeitung dieser Geschichte gewidmet ist. Ein Tool, das so modular sein wird, dass es auch in vielen anderen Projekten verwendet werden kann.

Diese Anwendung wird Ekklesiapp genannt, eine Mischung aus Anwendung und Ekklesia, dem altgriechischen Wort für "Versammlung", das direkt von den Bürgerversammlungen der antiken griechischen Städte stammt, darunter Athen, die Wiege der Demokratie. Obwohl die Demokratie damals nur der herrschenden Klasse zugänglich war, werden wir in Kapitel 5 "Wie entscheiden wir gemeinsam?" sehen, wie unvollkommen sie immer noch ist und noch verwirklicht werden muss. [^18]

Ecclesiapp ist eine Anwendung zur Entscheidungsfindung, die von wenigen Personen bis hin zur gesamten Bevölkerung eingesetzt werden kann. Sie ist modular genug, um unendlich viele verschiedene Methoden zur Entscheidungsfindung zu erstellen, sodass sie leicht an die jeweilige Aufgabe angepasst werden kann. Eine einmal gewählte Methode kann leicht modifiziert werden, so dass man ständig auf der Suche nach einer Methode mit den besten Ergebnissen ist.

Diese ist frei zugänglich. Sie kann es anderen ermöglichen, ebenfalls ihre Utopien, Dystopien oder Eutopien zu erschaffen. Und das, um die Forschung in diesem Bereich zu fördern, was aus den vielen oben genannten Gründen notwendig und nützlich ist.

Sie ermöglicht es auch anderen Personen, andere Arten von kollaborativer Schreibarbeit zu leisten. Sie kann für das Management einer Organisation, eines Unternehmens oder eines anderen Projekts verwendet werden und bietet schließlich ein echtes demokratisches Werkzeug, das es ermöglicht, die Macht wieder an die Basis zurückzugeben, was effektiv genug ist, um auf Systeme von Herrschaft und Hierarchien verzichten zu können.

Aber wie können wir uns auf die Regeln für die Veröffentlichung dieser Erzählung einigen?

Ecclesia ist direkt von dem demokratischen Entscheidungsprozess inspiriert, der in dieser idealen Welt eingeführt werden soll. Das heißt, in Wirklichkeit wird der demokratische Entscheidungsprozess in dieser idealen Welt von dieser Plattform inspiriert sein. Denn diese Plattform wird es uns direkt ermöglichen, unsere Ideen über den richtigen kollektiven und demokratischen Entscheidungsprozess zu testen und so lange zu iterieren, bis wir denjenigen gefunden haben, bei dem sich alle einig sind, dass er tatsächlich zum besten Ergebnis führt. So gibt man nicht nur seine Meinung für eine Entscheidung ab, sondern auch seine Meinung für den Entscheidungsprozess. Der aktuelle Entscheidungsprozess wird später vorgestellt.

> "Allein geht man schneller, gemeinsam geht man weiter" Afrikanisches Sprichwort

## Die Art des Schreibens

Was die Fähigkeiten betrifft, die notwendig sind, um diese Geschichte zu beurteilen oder sich an ihr zu beteiligen, so gibt es keine.

Wenn Sie die Welt kompliziert finden, dann ist sie es vor allem deshalb, weil wir sie kompliziert gemacht haben, aber in Wirklichkeit ist alles einfacher und logischer, als es scheint, wenn man von einer guten Basis ausgeht.

Alles, was uns kompliziert erscheint, ist in Wirklichkeit eine Aneinanderreihung von einfachen Elementen.

Die großen Gelehrten sind schließlich diejenigen, die diese Einfachheit der Welt erahnt haben.

Zweifeln Sie nicht an sich selbst, wenn Sie etwas nicht verstehen, dann liegt das daran, dass man sich nicht die Zeit genommen hat, es Ihnen einfach zu erklären, indem man von der Basis ausgeht, die Sie kennen.

Es ist in der Tat von entscheidender Bedeutung, immer wieder von der Basis auszugehen. Das ist eines der Grundprinzipien dieser Geschichte, und deshalb werden wir uns in Teil 2 die Zeit nehmen, eine solide Basis zu schaffen, bevor wir Ihnen in Teil 3 Eutopia ausführlich beschreiben.

Dieses Buch soll von allen gelesen und verstanden werden können. Intellektuelle werden es vielleicht als zu einfach geschrieben empfinden, mit einem zu geringen Wortschatz, aber das ist der Zweck des Buches.

Wie können wir uns zusammenschließen, wenn das, was uns vereinen sollte, für jemanden, der nicht über fundierte Kenntnisse in diesem oder jenem Bereich verfügt, unverständlich und unlesbar ist?

Wir versuchen auch nicht, mithilfe dieser Methoden wissenschaftlicher oder intelligenter zu erscheinen und auf diese Weise möglicherweise ein falsches Gefühl der Wahrheit in Ihnen zu erzeugen.

Diese Geschichte soll zugänglich, verständlich und auf das Wesentliche reduziert sein. Sie soll ehrlich zu Ihnen sein und frei von psychologischen Effekten, und wir überlassen es Ihnen, ob Sie den Inhalt glauben oder nicht.

In der Geschichte der Menschheit ist jede Produktion eines Autors nur die indirekte Produktion einer kollektiven Intelligenz.

Diese Geschichte ist ein natürliches Ergebnis der ständigen Suche nach der "Wahrheit", die im Laufe der Menschheit von unzähligen Menschen durchgeführt wurde, die dazu beigetragen haben, das globale Denken zu verändern. Diese Menschen wurden jeweils zu einer bestimmten Zeit geboren, in der sie kollektives Wissen aus der Vergangenheit erhielten und von Interaktionen profitierten, die sie mithilfe ihrer Fähigkeiten, ihres Status und ihrer Zeit dazu brachten, neue Gedanken und neues Wissen zu sammeln und zu veröffentlichen. Die direkten Verweise auf diese Personen in unserer Erzählung werden mit Anmerkungen versehen, aber wir vermeiden es, den Prozess, aus dem ihre Gedanken hervorgegangen sind, ausführlich zu beschreiben, um die Erzählung leichter und zugänglicher zu machen, aber auch um eine zu starke Individualisierung dieser Entdeckungen zu vermeiden und ein neues Paradigma zu fördern: die Annahme, dass jede Schaffung von Wissen und Gedanken in der Geschichte der Menschheit, wenn auch nur indirekt, nur kollektiv erfolgt ist. Diese kollektive Intelligenz wollen wir nun mithilfe von Hilfsmitteln, die diese Mechanismen erleichtern, wie dem in dieser Geschichte verwendeten, noch weiter vorantreiben. In dieser Geschichte sehen Sie also auch eine gewisse Sprache, die versucht, aus dieser Individualität auszubrechen.

Unserer Meinung nach ist Eutopia das Ergebnis einer kollektiven Intelligenz, die über Tausende von Jahren entstanden ist, und nicht das Ergebnis einzelner Individuen, die in diesen Epochen, einschließlich der unseren, isoliert waren. Ein geistiges Bild ist, die Menschheit als einen einzigen Organismus zu sehen, der sich im Laufe der Zeit entwickelt, voller Millionen und Milliarden von Zellen, die leben, sterben und wiedergeboren werden, die sich weiterentwickeln und sich ständig neu anpassen, um ein Gleichgewicht in ihrer Umgebung zu finden.

Die Verweise auf die ursprünglichen Autoren werden jedoch immer am Ende der Seite kommentiert, um mehr über diese Personen zu erfahren und ihre Werke zu finden. Aber auch, um alle unsere Quellen zugänglich zu machen und so die Überprüfung unserer Quellen hinsichtlich der von uns behaupteten Evidenz zu ermöglichen.

Eine zweite, reichhaltigere und wissenschaftlichere Version dieser Erzählung wird ebenfalls entwickelt und direkt mit dieser verlinkt, um die Argumentation ihres Inhalts gegenüber akademischen Institutionen zu unterstützen. Diese Version wird mit einem einfachen Klick zugänglich sein, wenn Sie mehr über die Geschichte, die Überlegungen und die Argumentation zu diesem oder jenem Thema erfahren möchten.

Im Laufe der Lektüre werden zahlreiche Studien zitiert, doch generell raten wir Ihnen, bei den Schlussfolgerungen der Studien, die Sie lesen, vorsichtig zu sein, da es leicht ist, die Wissenschaft zu beeinflussen. Manche Studien können von Unternehmen oder Interessengruppen finanziert werden, die spezifische Motive und Interessen haben und die Studie beeinflussen werden.

Studien können anfällig für Designfehler sein, wie z. B. Auswahlverzerrungen, Messfehler oder statistische Fehler, die zu nicht repräsentativen oder falschen Ergebnissen führen können. Die Interpretation der Ergebnisse kann subjektiv sein und von den Überzeugungen und Vorurteilen des Studienautors abhängen. Es ist wichtig, die Ergebnisse in ihrem Kontext zu betrachten und die Beweise als Ganzes zu prüfen. Generell können Zahlen auf unterschiedliche Weise dargestellt werden, um ein bestimmtes Argument zu unterstützen, und so zu falschen Schlussfolgerungen führen. [^19][^20][^21]

Die Idee ist nicht, die Wissenschaft zu meiden, sondern sich auf sie zu stützen, aber mit einem wachsamen Auge, indem man anerkennt, dass das produzierte Wissen in Bewegung ist und sich weiterentwickelt, dass neue Studien früheren widersprechen können und dass sie in ihrem derzeitigen Zustand weitgehend vom Wirtschaftssystem beeinflusst wird, das die Forschung finanziert. [^22]

Darüber hinaus stellt die Metaphysik, ohne hier ins Detail zu gehen, die grundlegenden Annahmen darüber in Frage, was existiert und wie Entitäten kategorisiert und verstanden werden können. Sie untersucht insbesondere Konzepte wie die Existenz des freien Willens, die Natur des Geistes im Verhältnis zum Körper und die Grundlagen der Realität selbst. Obwohl sie wissenschaftliche Studien nicht direkt in Frage stellt, hinterfragt metaphysisches Denken die konzeptuellen Rahmen und Annahmen, die der wissenschaftlichen Forschung zugrunde liegen. Dazu können Überlegungen darüber gehören, was es bedeutet, eine Studie zu "replizieren", wie wir Kausalität verstehen und was es für etwas bedeutet, in einem wissenschaftlichen Kontext "real" oder "wahr" zu sein, wie etwa die Tatsache, dass der Akt der Beobachtung nicht neutral ist, sondern den Zustand des beobachteten Objekts beeinflusst. Zusammengefasst hinterfragt die Metaphysik die philosophischen Grundlagen dessen, was wir in unserem Streben nach Wissen, auch in den Wissenschaften, für selbstverständlich halten.[^23].

Aus diesem Grund ist dieses Werk nicht starr und wird es auch nie sein. Es ist dazu bestimmt, von der gesamten Menschheit geschaffen zu werden und im Laufe ihrer Entwicklung durch und für sie selbst aktualisiert zu werden. Es wird daher immer offen für neue Beweise sein, die seinem Inhalt widersprechen können, einschließlich seiner tiefsten Fundierung.

> Der Zweifel ist der Anfang der Weisheit. Aristoteles[^25]

## Fazit der Einleitung

Wie Sie sicher bemerkt haben, lesen Sie hier Eutopia, eine Erzählung, die für die Menschheit mittlerweile mehr als notwendig ist. Es ist an der Zeit, dass wir uns endgültig von unserem primitiven Zustand entfernen, die globale Entwicklung unserer Gesellschaft hin zu einer wahren Zivilisation ermöglichen, um die sich abzeichnende ökologische und menschliche Katastrophe abzuwenden. Diese Erzählung soll von der gesamten Menschheit, durch sie selbst und für sie selbst verwirklicht werden, um ihr endlich wieder Sinn und Richtung zu geben, um ihr eine garantierte Zukunft in Freiheit und Glück zu sichern.

> "Die Spannungen und Widersprüche der menschlichen Seele werden sich erst auflösen, wenn die Spannungen zwischen den Menschen, die strukturellen Widersprüche des menschlichen Netzwerks, verschwinden. Dann wird es nicht mehr die Ausnahme, sondern die Regel sein, dass der Einzelne jenes optimale physische Gleichgewicht findet, das die erhabenen Worte "Glück" und "Freiheit" bezeichnen wollen: nämlich das dauerhafte Gleichgewicht oder sogar die vollkommene Übereinstimmung zwischen seinen sozialen Aufgaben, der Gesamtheit der Anforderungen seiner gesellschaftlichen Existenz einerseits und seinen persönlichen Neigungen und Bedürfnissen andererseits. Erst wenn sich die Struktur der menschlichen Beziehungen an diesem Prinzip orientiert, erst wenn die Zusammenarbeit zwischen den Menschen, die Grundlage der Existenz eines jeden Individuums, in einer Weise erfolgt, dass alle, die Hand in Hand die komplexe Kette gemeinsamer Aufgaben bewältigen, zumindest die Möglichkeit haben, dieses Gleichgewicht zu finden, erst dann können die Menschen mit etwas mehr Recht behaupten, dass sie "zivilisiert" sind. Bis dahin befinden sie sich im besten Fall im Prozess der Zivilisation. Bis dahin müssen sie noch oft wiederholen: "Die Zivilisation ist noch nicht vollendet. Sie wird noch gemacht!" Nober Elias, Die Dynamik des Westens".

## Quellen (Nicht erschöpfende Liste)

[^1]: Yuval Noah Harari, Sapiens: Eine kurze Geschichte der Menschheit. 2011
[^2]: Sébastien Bohler, Wo ist der Sinn / 2020
[^3]: [Etymologie des Wortes Utopie] (https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, Die gute Wirtschaft: Die allgemeine Reziprozität. 1984
[^5]: [Maximale CO2-Konzentration in der Atmosphäre](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Grenzwert für die Störung der biochemischen Kreisläufe von Phosphor und Stickstoff](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Maximale Erosion der biologischen Vielfalt](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Intensive Landnutzung](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Missbrauch von Süßwasser](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Versauerung der Ozeane](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Wie alles zusammenbrechen kann, Pablo Servigne & Raphael Stevens.
[^12]: [Etymologie des Wortes Eutopie](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, The Earth's History (Die Geschichte der Erde).
[^14]: The Future of Life on Earth" von Peter D. Ward und Donald Brownlee.
[^15]: Sébastien Bohler, Der menschliche Käfer. 2019
[^16]: Jared Diamond, Zusammenbruch. 2004
[^17]: Valérie Jousseaume, Plouc Pride: Eine neue Erzählung für die Kampagne. 2021
[^18]: Serge Christhonne, Sind Wahlen die Demokratie? 1977
[^19]: Ben Goldacre, Bad Science. 2008
[^20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [The Replication Crisis](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Dokumentarfilm: Die Fabrik der Ignoranz. 2020
[^23]: Jaegwon Kim, Grundlagen der Metaphysik.
[^24]: Cyril Dion, Kleines Handbuch des zeitgenössischen Widerstands, 2018
[^25]: Aristoteles, Ethik des Eudemos
