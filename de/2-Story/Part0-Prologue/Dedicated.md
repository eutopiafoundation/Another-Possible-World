---
title: Widmungen
description:
published: true
date: 2023-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2023-03-07T21:13:59.460Z
---

Für das Ideal in jedem von uns, das unter einer Welt leidet, die nicht ideal ist.

Für das Leid derer, die wir lieben, unser eigenes, das derer, die wir nicht kennen, und all derer, die noch kommen werden...

Dieses Werk soll von der gesamten Menschheit geschaffen werden und im Laufe ihrer Entwicklung durch und für sie selbst aktualisiert werden.
