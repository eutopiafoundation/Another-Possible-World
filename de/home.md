---
title: Home
description:
published: true
date: 2023-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2023-03-12T14:24:47.978Z
---

> Currently being translated.
> {.is-danger}

---

title: Startseite
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z

---

> Klicken Sie auf die Weltkugel oben links, um die Sprache zu ändern.
> {.is-info}

Willkommen bei der kollaborativen und demokratischen Erzählung, die von der Stiftung Eutopia gefördert wird.

Wir empfehlen Ihnen, Ihre Lektüre mit der Widmungsseite zu beginnen und dann der Seitenreihenfolge zu folgen.

Sie werden versucht sein, direkt zu Teil 2 überzugehen, um die Welt, die wir Ihnen vorschlagen, konkret kennenzulernen, dennoch sind die Grundlagen von Teil 1 wesentlich, denn sie sind es, die das Mögliche begründen, und ohne sie laufen Sie also Gefahr, nur das Unmögliche zu sehen...

Aus diesem Grund haben wir uns in erster Linie auf Teil 1 konzentriert und einen Großteil von Teil 2 noch nicht fertiggestellt.

Wie können wir alle Ideen aus Teil 2 umsetzen? Ein Schlüsselelement ist es, den Menschen zu verstehen, was wir im Kapitel "Homo Sapiens" getan haben, aber auch das richtige Wirtschaftssystem zu wählen, was wir im Kapitel "Zusammenarbeiten" getan haben.

Da wir uns der Zeit und der Motivation bewusst sind, die wir in unserem Leben haben können, empfehlen wir Ihnen diese Kapitel zuerst, und wir haben Ihnen auch eine Zusammenfassung für alle Kapitel unserer Erzählung erstellt. [Diese ist in den Anhängen zu finden](/de/4-Appendices/1Summaries).
