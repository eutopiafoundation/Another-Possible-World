---
title: Tiempo de trabajo
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

> Contenido en fase experimental y en proceso de redacción.
> {.is-danger}

Trabajo, de la palabra travalhum, proviene de tripalium, un trípode utilizado como instrumento de tortura. Pero el trabajo no está condenado a ser la tortura de su origen etimológico.

Para muchos de nosotros, la naturaleza y la duración del trabajo son inevitablemente repetitivas, aburridas y nos quitan la vida.

La vida parece vacía y nos decepciona cuando nos consume un trabajo que no le da sentido.

Pero la sociedad no nos permite satisfacer nuestros verdaderos deseos. Porque la insatisfacción que podría llevarnos a cuestionar esta vida y exigir otra es canalizada por el sistema que satisface nuestras necesidades intrínsecas y las necesidades extrínsecas que generan, que al mismo tiempo nos impide darnos cuenta de lo que nos falta.

Donde el trabajo debe permitirnos aportar al mundo una creación que lleve nuestra impronta, expresarnos y dar sentido colectivo a nuestras vidas.

Todas las dimensiones de la monotonía de la tarea pueden y deben borrarse o reducirse. El trabajo que es física o psicológicamente agotador, embrutecedor o embrutecedor, demasiado duro, mecánico, monótono, demasiado sucio o desagradable, demasiado carente de sentido, despectivo, etcétera. Hay que eliminarlas, sustituirlas, restringirlas, automatizarlas, mejorarlas, enriquecerlas, compensarlas, recompensarlas, valorarlas o compartirlas mejor.

La idea misma es transformar la naturaleza del trabajo para que se convierta en una actividad que sea algo así como la autorrealización, la manifestación de la humanidad más profunda de cada uno en armonía con la naturaleza. De este modo, el trabajo se convertiría en la "primera necesidad de la vida". De este modo, la expresión "trabajar para vivir" ya no significaría pasar por el trabajo para sobrevivir y mantenerse, sino realmente ganarse la vida. ^1]

## Reducción de la jornada laboral

Vamos a demostrarte que el modelo económico de reciprocidad permite reducir considerablemente el tiempo de trabajo necesario para que la sociedad funcione correctamente, hasta una cuota de 2 a 3 horas por persona[^2][^8].

La productividad es la relación entre la producción y el número de horas de trabajo necesarias para alcanzarla.

La productividad ha aumentado considerablemente en los últimos siglos.

En primer lugar, con la llegada de la revolución industrial en los siglos XVIII y XIX, que supuso la mecanización de los procesos de producción y la introducción de nuevas tecnologías como el vapor y la electricidad.

Desde entonces, la productividad ha seguido aumentando a un ritmo variable, pero constante, a medida que se desarrollaban nuevas tecnologías y sistemas de producción.

Resulta difícil encontrar datos fiables sobre el crecimiento de la productividad en Francia desde hace más de 100 años. Los métodos de medición de la productividad han evolucionado con el tiempo, lo que puede dificultar la comparación de datos durante largos periodos.

Pero, por ejemplo, entre 1950 y 2020, la productividad laboral en Francia aumentó una media de alrededor del 2% anual. Esto significa que un trabajador medio en Francia puede producir aproximadamente el doble de bienes y servicios en una jornada laboral que en 1950[^3].

Sin embargo, durante el mismo periodo, la semana laboral media no ha disminuido un 50%, sino un 12,5%, pasando de 40 horas en 1950 a unas 35 horas en 2020.

Aquí se ha hecho una elección de civilización, utilizar las posibilidades abiertas por el prodigioso desarrollo de la tecnología en los últimos siglos, no para reducir las horas de trabajo, sino para aumentar la producción y los beneficios que conlleva.

Esto procede de la base misma del capitalismo, del crecimiento del que depende su supervivencia, del afán de lucro del empresario y del afán de acumulación del consumidor, que se combina cada vez con más.

Por tanto, el aparato productivo debe mantenerse funcionando a pleno rendimiento. El objetivo es vender cada vez más y multiplicar las oportunidades de beneficio.

Esto conduce a un sinsentido en la forma de producir y consumir bienes y servicios.

Pero la reducción del tiempo de trabajo es enemiga de nuestra sociedad, porque el tiempo libre puede utilizarse para el debate, para la organización popular y para inventar formas de vida colectiva. Entonces el poder ya no se enfrentaría a individuos anónimos, aislados y dispuestos a aceptar, sino a grupos conscientes y fuertes, más difíciles de manipular.

Entonces, ¿una reducción de la jornada laboral significa una reducción de la producción? ¿Con consecuencias catastróficas para nuestra calidad de vida?

No, no se trata en absoluto de modificar nuestra calidad de vida; al contrario, se trata de mejorarla continuamente para el conjunto de la población. Aunque la calidad de vida sigue siendo un concepto totalmente relativo a las costumbres individuales y sociales de una época. Vamos a demostrar que es posible realizar ahorros significativos que, sin afectar a nuestro nivel de vida, nos permitirán reducir considerablemente la producción y, por tanto, el tiempo de trabajo.

Aquí es donde descubrimos el poder de la reciprocidad. Una vez fuera del mercado, podemos por fin idear eficazmente soluciones para reducir el tiempo de trabajo sin que ello entre en conflicto con nuestra economía.

Porque los absurdos y las injusticias que vamos a revelar no son fruto de errores o torpezas; al contrario, son necesarios para la supervivencia del capitalismo.

He aquí, en los puntos siguientes, las distintas soluciones para reducir el tiempo de trabajo global:

## Reducir la producción/consumo

### Cambiar nuestras necesidades extrínsecas

Es difícil imaginar cómo, fuera de este sistema de consumo que siempre nos promete más, podríamos obtener ese algo extra que da color y sabor a la vida, al igual que los objetos que aseguran nuestra identidad. Porque es a través de las cosas que poseemos, de la ropa que vestimos, de los lugares que nos son familiares, como nos sentimos a la vez lo bastante parecidos a los demás para que no nos rechacen y lo bastante diferentes para estar seguros de ser nosotros mismos.

Pero limitar nuestro consumo nos permite revelar nuestro deseo de amar y crear, liberar el tiempo para vivir que tanto nos falta hoy. Entonces conoceremos la alegría de dejar fluir la vida, cuyo murmullo nos dice lo que somos, el placer de amar cuando no hay prisa, el placer de dominar una técnica que es toda nuestra, de crear nosotros mismos los objetos, tanto si pretendemos que firmen nuestro espacio como si son el regalo que reservamos para los que amamos.

Esta visión nos ayuda a comprender por qué la idea de reducir la jornada laboral encuentra tanta oposición en nuestra sociedad, ya que liberar el tiempo que ocupa el trabajo significa abrir una brecha en la camisa de fuerza en la que la sociedad capitalista ha encorsetado nuestras vidas.

El crecimiento se mantiene porque nunca puede satisfacer las "necesidades" que se recrean constantemente sobre la base de una desigualdad que no corrige.

Estas necesidades son deseos de estatus, pertenencia, autoestima, poder o identificación social. Son creadas por la publicidad, el marketing y las presiones sociales.

La publicidad pretende convencer a los consumidores de que necesitan determinados productos o servicios para ser felices, estar satisfechos o ser respetados en la sociedad. Utilizan imágenes o mensajes que explotan nuestras emociones, deseos y aspiraciones.

### Aumentan la vida útil de los productos.

Un coche, por ejemplo, satisface una necesidad de autonomía personal, pero también una necesidad extrínseca de adquirir el prestigio que confiere poseer el último modelo o el modelo de gama alta.

La empresa automovilística asegura su rentabilidad y, por tanto, su supervivencia renovando constantemente su producto, desarrollando el deseo del consumidor de máquinas cada vez más sofisticadas y caras, pero también por su fragilidad y, por tanto, su necesidad de renovación.

Si comparamos las horas que nos ahorra un coche con las horas que pasamos trabajando para pagarlo, el trabajador medio dedica el equivalente a 375 horas al año, es decir, 2 meses de trabajo, a comprar, mantener, hacer funcionar y asegurar un coche.

Los primeros coches podían circular entre 200.000 y 300.000 km, pero el "progreso" se ha centrado en fabricar coches más frágiles. Este es el precio del crecimiento, pero a costa del consumidor, mientras que el fabricante ahorra reduciendo los índices y la calidad de los materiales, obteniendo grandes beneficios con las piezas de recambio y renovando los equipos cada vez más deprisa.

El automóvil debe estar diseñado para durar 20 años o más, quizá incluso hasta que se desguace, pero este punto se tratará con mayor profundidad en el capítulo dedicado al transporte.

Los monumentos del pasado, catedrales y castillos, nos muestran las relaciones sociales y la ideología de su época. Nuestros propios monumentos son estas máquinas, cuya fantástica complejidad refleja el apogeo de nuestros conocimientos científicos y nuestro saber tecnológico, pero que están obsoletas nada más nacer, y serán desechadas al cabo de unas décadas. Los edificios y viviendas bien construidos podrían durar fácilmente más de un siglo.

Cuántos electrodomésticos han tenido que ser reparados, a menudo poco tiempo después de su compra, y con demasiada frecuencia la reparación es demasiado difícil o demasiado cara, lo que lleva a desechar el aparato y sustituirlo por otro.

Las primeras medias eran indestructibles, pero ahora se sumergen en un baño de ácido tras su fabricación para reducir su resistencia. La empresa que inventó los tubos fluorescentes no los sacó al mercado hasta que descubrió cómo reducir su tiempo de funcionamiento a 1.000 horas. Las cuchillas de afeitar están diseñadas para desafilarse. Actualmente, las bombillas están diseñadas para durar sólo 1.000 horas. Esas mismas bombillas podrían durar 3.000 horas.

La mayoría de los electrodomésticos podrían durar 30 años.

El deterioro de la calidad de los objetos es evidente, pero es un secreto muy bien guardado por la industria y difícil de demostrar. Sólo los fabricantes saben cuánto más les costaría fabricar objetos que duraran 2 o 3 veces más.

Esta es la paradoja: un producto demasiado duradero no puede ser rentable a largo plazo en una lógica de mercado que persigue el beneficio monetario, pero es rentable en una lógica de reciprocidad que persigue menos trabajo.

### Reducir los residuos

En nuestra sociedad, producimos mucho más de lo que realmente necesitamos, lo que da lugar a un uso ineficaz de los recursos naturales y a una acumulación de residuos.

Desperdicio de alimentos: según la FAO, alrededor de un tercio de los alimentos producidos en el mundo se pierde o desperdicia cada año. Esto puede deberse a un exceso de producción, a una mala gestión de la cadena de suministro, a estrictas normas estéticas o a comportamientos de los consumidores como la compra impulsiva o el consumo excesivo.

Despilfarro energético: Tendemos a consumir más energía de la que necesitamos para nuestras necesidades diarias. Por ejemplo, dejando aparatos electrónicos en modo de espera, utilizando medios de transporte poco eficientes o dejando luces encendidas innecesariamente.

Desperdicio de materiales: Nuestra sociedad suele producir bienes de un solo uso o de baja calidad que se tiran o sustituyen rápidamente, en lugar de favorecer los bienes duraderos y fácilmente reparables. Esto conduce a un uso ineficiente de los recursos naturales y a la acumulación de residuos.

### Compartir bienes

Al compartir los bienes, las personas pueden reducir su consumo y, por tanto, su producción, ya que no necesitan comprar tantos artículos para satisfacer sus necesidades. Por ejemplo, varias personas pueden compartir un taladro en lugar de comprar uno cada una para un uso ocasional. Esto reduce la producción de taladros.

El 90% de los coches son estacionarios, así que tenemos que aunar esfuerzos para producir menos.

Además, cuando los bienes se comparten, tienden a utilizarse y mantenerse mejor, lo que alarga la vida útil de los objetos y, por tanto, de la nueva producción. Cuando las personas son copropietarias, se producen un 30% menos de daños.

## Aumenta la productividad

### Utilizar el progreso técnico

El progreso técnico suele asociarse al aumento de la productividad, ya que nos permite producir más bienes o servicios con menos recursos, menos tiempo y menos esfuerzo. Los avances tecnológicos ya han mejorado la eficiencia y la calidad de nuestros procesos de producción, y han contribuido a reducir los costes de producción.

Está en marcha una 4ª revolución industrial, en la que intervienen la inteligencia artificial, la Internet de los objetos, la robótica avanzada, la realidad aumentada, el blockchain, la nanotecnología y la biotecnología. Y estas tecnologías transformarán realmente nuestra forma de producir.

Sin embargo, tenemos que tener cuidado de que el uso del progreso tecnológico no tenga consecuencias para el Impacto Medioambiental Humano y la Felicidad Interior Bruta, que siempre deben contribuir a un desarrollo sostenible y equitativo.

### Aprovechar al máximo la reducción de la jornada laboral

Todo trabajo, incluso el más sencillo, requiere un mínimo de formación, práctica y destreza. A medida que disminuyen las horas de trabajo, la productividad sólo puede aumentar hasta cierto punto, tras lo cual vuelve a caer.

Cuanto más domines, más productivo serás, pero cuanto más tiempo trabajes, más cansado estarás y menos productivo serás.

Y es posible que para muchas ocupaciones que requieren conocimientos técnicos este punto se supere con las "2 ó 3 horas diarias", por lo que estas 2 ó 3 horas diarias no deben tomarse al pie de la letra. El trabajo de cada uno puede organizarse en periodos más ocupados y otros menos o totalmente libres.

Por ejemplo, 3 días de 8 horas semanales durante 2 años pueden dar paso a 3 años de tiempo libre.

## Quitar trabajo

### Transformar el trabajo atado en trabajo libre

En la sociedad actual, como en cualquier sociedad, hay trabajos desagradables y otros agradables. En general, las tareas tediosas y aburridas están mal pagadas y no gozan de mucho prestigio, y las actividades más remunerativas suelen ser las mejor valoradas socialmente y las más interesantes para quienes las realizan.

Hay dos tipos de trabajo.

El trabajo duro y aburrido que sigue siendo necesario para que la sociedad funcione en su conjunto, al que llamaremos "trabajo atado".

Y el trabajo libre, que tiene sentido para la persona que lo realiza, emanando de una actividad creativa que encuentra su justificación en sí misma. Por supuesto, esto puede ser subjetivo y específico de cada individuo.

En realidad, 3/4 partes del trabajo realizado en nuestra sociedad capitalista escapan a la economía de mercado.

Con más tiempo y una sociedad mejor, podemos apoyarnos en una red de autoayuda para discutir y resolver nuestros problemas cotidianos, hablar de nuestras dietas, de nuestra vida sexual, de nuestras angustias ante la vida, como ya empieza a ocurrir a gran escala en los grandes grupos de Facebook.

Los electrodomésticos podrían diseñarse para que fueran manejables por los usuarios, fáciles de montar y desmontar con herramientas normalizadas, cambiando de modelo cuando el progreso técnico real lo hiciera necesario, garantizando al mismo tiempo el suministro de piezas de recambio a precios razonables durante periodos muy largos, proporcionando a los clientes instrucciones claras y detalladas, creando talleres locales de montaje y reparación donde se pudieran encontrar las herramientas necesarias e intercambiar ayuda y experiencia.

Las mejoras en el hogar pueden ser llevadas a cabo por los propios ocupantes. Lo que hoy es una carga insoportable, construir su propia casa con un programa de trabajo tan pesado. Sin embargo, con la ayuda de la comunidad, esta actividad podría convertirse en una actividad creativa y agradable.

La enseñanza no podría seguir siendo monopolio de un cuerpo profesional, sino al contrario ser más flexible y la función de profesor podría compartirse más ampliamente, y cualquiera que tenga conocimientos que ofrecer podría animarse a ayudar en la enseñanza.

De hecho, gran parte del trabajo que realiza el personal de ciertas profesiones podría ser realizado por otros y convertirse en trabajo gratuito en nuestra Eutopía, y aliviar así a estas profesiones.

### Aprovechar la reorganización del trabajo

El próximo capítulo tratará de la reorganización del trabajo, es decir, de las formas de organización de las empresas en nuestra Eutopía.

El carácter horizontal de ésta eliminará cierta jerarquía que realiza tareas de gestión y toma de decisiones. Este trabajo ya no será necesario, puesto que ahora lo realizarán en colaboración todos los cooperativistas de una empresa.

### Trabajo de mierda y trabajo más necesario

En nuestra Eutopía, se suprimirían muchos empleos, porque en realidad son producto de disfunciones de la sociedad actual y, por tanto, ya no serían necesarios.

- La publicidad representa el 1% del PIB[^4].
- El ejército el 3% del PIB.
- El trabajo perdido en las compañías de seguros.

Al final de este artículo figura una lista completa de los empleos perdidos en Eutopía.

## Aumentar el número de trabajadores

### Desmantelar el desempleo

En un país como Bélgica, la tasa de desempleo es actualmente del 5,5%. Teniendo en cuenta que la población activa total en Bélgica es de 5,8 millones de personas, según datos de Eurostat para 2021, esto representa unas 319.000 personas sin trabajo[^7].

Al no existir puestos de trabajo como tales, el efecto de la desaparición del empleo asalariado va acompañado de una desaparición del desempleo.

Construye la inclusión social y una sociedad abierta, en la que cada nuevo miembro es bienvenido para aportar sus competencias y conocimientos, sean cuales sean, a la sociedad y reducir el tiempo total de trabajo.

### Aumentar el número de personas que participan en el trabajo vinculado

La sociedad excluye de la vida laboral a toda una serie de personas, en particular a las discapacitadas.

Esto puede deberse a la discriminación, la estigmatización, la falta de accesibilidad o la falta de apoyo.

Por el sistema, para quienes a menudo es todo o nada, aunque para ellos sea mejor seguir recibiendo sus ayudas que trabajar y que se las retiren.

La cualificación general de la población podría aumentar considerablemente. En la actualidad, las expertas suelen proceder de las clases sociales más privilegiadas. Pero si todos tuvieran la oportunidad de llegar a ser lo que quisieran y se les dieran los medios para ello, el número de expertos sería mucho mayor.

Personas mayores excluidas del mercado una vez jubiladas, discriminadas, etc.

### Reducir los problemas de salud

Según una encuesta realizada en 2020 a empleados europeos por el grupo de consultoría de gestión Mercer, alrededor del 33% de los trabajadores europeos afirmaron haber cogido la baja por motivos relacionados con el estrés o la salud mental[^5] Además, una encuesta realizada en 2018 por la Comisión Europea reveló que el burnout estaba presente en el 28% de los trabajadores de la UE[^6].

## Fuentes (Lista no exhaustiva)

[^1]: Serge-Christophe Kolms. La bonne économie. 1984
[^2]: Adret. Trabajar dos horas al día. 1977
[^3]: [Insee. Croissance - Productivité](https://www.insee.fr/fr/statistiques/4277770?sommaire=4318291)
[^4]: Trabajo de mierda. David Graeber. 2018
[^5]: [Estudio Mercer Marsh sobre beneficios - Barómetro del absentismo](https://www.mercer.com/fr-fr/insights/total-rewards/employee-benefits-strategy/barometre-de-labsenteisme-2023/)
[^6]: [COMUNICACIÓN DE LA COMISIÓN AL PARLAMENTO EUROPEO, AL CONSEJO, AL COMITÉ ECONÓMICO Y SOCIAL EUROPEO Y AL COMITÉ DE LAS REGIONES relativa a un enfoque global en materia de salud mental](https://health.ec.europa.eu/system/files/2023-06/com_2023_298_1_act_fr.pdf)
[^7]: [ STATBEL. Empleo y desempleo ](https://statbel.fgov.be/fr/themes/emploi-formation/marche-du-travail/emploi-et-chomage)
[^8]: Keynes. J. Carta a nuestros nietos. Ensayo sobre el futuro del capitalismo. 1930.
