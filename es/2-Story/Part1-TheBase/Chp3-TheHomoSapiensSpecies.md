---
title: La especie Homo Sapiens
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

## Homo Sapiens

Puede que la especie humana esté dotada de una inteligencia formidable y de una conciencia y unos conocimientos tecnológicos cada vez mayores, pero está claro que cuando nos enfrentamos a las consecuencias presentes y futuras del cambio climático, con cifras que las avalan, somos incapaces de cambiar nuestro rumbo[^1].

De ahí la necesidad de comprender al ser humano y su funcionamiento, porque sólo así podremos garantizar que el entorno creado en nuestra Eutopía sea posible, y que podamos predecir nuestro comportamiento en él.

Comprender las "debilidades" humanas nos permitirá adaptarnos y desarrollar herramientas en consecuencia, de modo que podamos explotarlas para crear una fortaleza.

Vamos a comprender los mecanismos a nivel individual, sus diferentes aspectos físicos y psicológicos... Pero este enfoque sería insuficiente y va acompañado de toda una serie de consideraciones globales y sociológicas.

<!-- ## Breve historia del Homo sapiens -->

## Las necesidades del Homo Sapiens

Volvamos a nuestra pregunta de investigación:

"¿Cómo podemos hacer que todos los humanos tengan una vida plena en la Tierra durante las próximas 80 millones de generaciones?".

Partimos del principio de que una vida plena para todo ser humano es una vida plena que satisfaga sus necesidades y aspiraciones. Una necesidad es una necesidad percibida, ya sea física, social o mental.

### Clasificación de las necesidades

Para ayudarnos en nuestra investigación, empezaremos con la pirámide de Abraham Maslow, formulada en 1943. Ésta clasifica las necesidades humanas en 5 etapas y proporciona una base interesante para una visión general de las principales necesidades de los seres humanos[^2].

Nótese que este modelo se cuestiona hoy en día, porque según Maslow, sólo sentimos una necesidad superior cuando se ha alcanzado la anterior. Pero estas necesidades no son en absoluto piramidales, y muchos avances han demostrado ahora que las necesidades sociales son tan importantes como las fisiológicas, lo que no significa que deban estar en la base de la pirámide, sino que son omnipresentes en todos los niveles, y todo el mundo las siente al mismo tiempo[^3].

Pero en cualquier caso, no hace falta saber exactamente qué necesidad es más importante que otra y en qué circunstancias, porque de lo que se trata es de satisfacerlas todas sin excepción.

A continuación, completaremos la pirámide actualizando la teoría a la luz de la evolución de los conocimientos psicológicos y neurocientíficos.

Pirámide de Maslow](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

En el primer nivel tenemos las necesidades fisiológicas, que son las necesidades básicas necesarias para mantener la vida y el buen funcionamiento del organismo, como comer, beber, la sexualidad, dormir, respirar...

Cabe destacar el fracaso total de nuestro sistema actual para satisfacer las necesidades básicas de toda la población. Hoy en día, 25.000 personas han muerto de hambre, de las cuales 15.000 son niños menores de 5 años[^4]. Del mismo modo, en lo que respecta a la sexualidad, a menudo es tabú, está estigmatizada, discriminada y carece de educación, lo que provoca numerosos trastornos, transmisión de enfermedades, actos no consentidos, traumas, problemas de salud mental y física, falta o adicción a los placeres, falta de sensación de libertad y confianza, todo lo cual crea una falta general de bienestar con la propia sexualidad para una parte muy importante de la población.

En el segundo piso tenemos la necesidad de seguridad. La seguridad de tener un refugio, como un lugar donde vivir, la seguridad de tener acceso a los recursos deseados, como equipos, herramientas, juegos... La seguridad física frente a la violencia, las agresiones, etc. Seguridad moral y psicológica, seguridad y estabilidad emocional, seguridad médica/social y seguridad sanitaria.

Una vez más, constatamos las diversas carencias de nuestra sociedad también en este ámbito, incluso en los países occidentales, donde la seguridad de la vivienda es un verdadero reto para muchas personas, en particular para las personas sin hogar, que a menudo se enfrentan a condiciones precarias e inhumanas[^5]. El acceso a los recursos también puede ser limitado para algunas personas en función de su estatus socioeconómico, lo que puede afectar a su bienestar y calidad de vida. En cuanto a la seguridad médica y psicológica, faltan profesionales de la salud mental, lo que dificulta el acceso a la atención. Incluso con seguridad social en países como Francia, el coste de la atención puede ser prohibitivo para algunas personas, lo que les impide recibir los cuidados que necesitan[^6].

El tercer nivel son las necesidades de pertenencia, la necesidad de amar y ser amado, de tener relaciones íntimas, de tener amigos, de formar parte de un grupo cohesionado, de sentirse aceptado, de no sentirse solo ni rechazado.

Vivimos en una sociedad en la que abunda el individualismo y/o en la que a menudo se descuidan las relaciones humanas en favor del éxito personal. Una gran parte de la población siente cada vez más soledad y pérdida de vínculos sociales. Algunos se sienten rechazados y aislados, lo que conduce a problemas de salud mental como la depresión y la ansiedad. [^7]Nuestro modelo social favorece la competencia generalizada y las relaciones interesadas o comerciales como único modelo relacional, promoviendo una verdadera cultura del egoísmo.

El cuarto nivel representa las necesidades de estima. El ser humano necesita sentirse considerado, sentirse apreciado y respetado por los demás, sentirse reconocido por sus logros y sentirse capaz de triunfar. Estas necesidades se satisfacen haciendo cosas que son valoradas por la sociedad, fijándose objetivos y trabajando para alcanzarlos, o participando en actividades que permiten el desarrollo personal.

El quinto nivel es la necesidad de autorrealización. Es la necesidad de realizarse, de explotar y aprovechar al máximo el potencial personal. Se manifiesta en el deseo de desarrollar los propios talentos y aptitudes, de participar en actividades que aporten significado y satisfacción, y de superarse a sí mismo.

En nuestra sociedad es difícil satisfacer esta necesidad de autorrealización, ya que las presiones sociales y las elevadas expectativas impuestas a la mayoría de las personas pueden dificultar la búsqueda de las propias pasiones e intereses personales. Nuestra sociedad altamente competitiva y su alienación del individuo dejan poco tiempo para explorar y desarrollar talentos y habilidades. Además, muchas personas quedan excluidas del sistema, sobre todo los discapacitados, que no pueden encajar en él porque no está adaptado a ellos. [^9]

Sin embargo, esta necesidad de autorrealización se vio en cierto modo cuestionada en 2010 por el trabajo de 4 psicólogos, que propusieron una actualización de la teoría de la evolución[^3]. [^3] Esta suprime la necesidad de autorrealización para dejar sitio a tres necesidades ligadas al impulso reproductor: encuentro, continuidad y posteridad.

|     |     |
| --- | --- |

| Necesidad de posteridad: Aplazar la muerte inevitable.
Necesidad de continuidad: Conservar la pareja, el estatus, etc. | Repele la pérdida.
| Necesidad de encuentro: Repele competidores y aislamiento social.
| Necesidad de estima y estatus: Ser reconocido como un ser único.
| Necesidad de pertenencia o afiliación: Ser querido y aceptado por lo que se es.
| Necesidad de protección: Refugio, seguridad +física, familiar, psicológica, financiera, etc. | Repeler la violencia.
| Necesidades fisiológicas: Respirar, beber, comer, hacer sus necesidades, dormir, mantenerse caliente.

|Lista de las siete necesidades humanas fundamentales inconscientes y concomitantes de D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Se trata de necesidades inconscientes y evolutivas que se expresan de mil maneras distintas en la vida de cada persona. Por ejemplo, la creatividad artística puede basarse tanto en la necesidad de adquirir estatus y ser reconocido socialmente como un ser único como en la necesidad de seducir a la pareja, o de perdurar en el tiempo dejando una obra para la posteridad.

Acabamos de repasar los 5/7 tipos de necesidades, pero hay que señalar que hacia el final de su vida, Abarham Maslow añadió una 6ª necesidad, "superarse a sí mismo". Ésta se sitúa en la cúspide de la pirámide:

> "El ser humano plenamente desarrollado que trabaja en las mejores condiciones tiende a estar motivado por valores que trascienden su 'yo'"[^9].

Es interesante darse cuenta de que un ser humano realizado que va más allá de la realización personal estaría plenamente abierto a ir más allá de su individualidad para comprometerse al servicio de los demás.

Para concluir, estas necesidades también pueden complementarse con las reflexiones de Hannah Arendt.

En nuestra condición humana, estamos impulsados hacia una búsqueda de eternidad: la vita contemplativa. Una búsqueda cada vez más difícil de satisfacer en nuestras sociedades modernas, que ya no creen en Dios. También nos impulsa la búsqueda de la inmortalidad, la vita activa. [^10]

La búsqueda de la inmortalidad es lo que impulsa al ser humano mortal a decir y hacer cosas creativas que van más allá de su propia necesidad vital, de su propia existencia, y lo proyectan hacia el futuro más allá de su propia muerte.

Al principio, la era de la modernidad reconocía socialmente la necesidad de inmortalidad, por ejemplo a través del heroísmo de los soldados, de los títulos y lemas, o del valor y la autenticidad de la palabra sin concesiones: "Sólo se piensa, sólo se habla con fuerza desde el fondo de la tumba: ahí es donde hay que situarse, ahí es donde hay que dirigirse a los seres humanos" (Diderot, 1782). Hoy, todos estos títulos, todas estas palabras, todas estas acciones, ya no resuenan en nuestra sociedad. Hannah Arendt sostiene que la represión social de la búsqueda de la eternidad fuera del dominio público, unida a la represión social de la búsqueda de la inmortalidad, explica cómo "la era moderna -que comenzó con una explosión de actividad humana tan nueva, tan rica en promesas- terminó en la pasividad más inerte y estéril que la historia haya conocido jamás. [^10][^11]

Emmanuel Todd sostiene algo parecido. Sostiene que la transición del "Hombre que cree en el Hombre" al "Hombre que cree en Dios" ha generado una importante energía colectiva. Alejarse de la búsqueda de la eternidad ha reforzado las energías hacia la búsqueda de la inmortalidad. Cuando la Iglesia como marco social se derrumbó, se desarrolló un nuevo ideal humanista secular. Se trataba de la inmensa esperanza en el Hombre Nuevo que portaban los movimientos revolucionarios del siglo XIX. Por otra parte, el colapso del ideal social comunista y del ideal republicano revolucionario marca el fin social de la búsqueda de la inmortalidad. "El Hombre que cree en el Hombre desaparece a su vez y se convierte en el Hombre que ya no cree en nada en absoluto, un ser humano nihilista y apático, sin esperanza ni propósito, que llena su vacío existencial a través del consumo"[^12].

¿Qué satisface nuestra necesidad de posteridad? Según Hannah Harendt, es la obra. Un trabajo es aquello capaz de dejar una posteridad, es decir, un hijo. Significa también plantar árboles bajo los cuales el paso del tiempo no nos permitirá venir a darnos sombra. Significa transmitir algo a los niños. Significa escribir poesía o tallar piezas de madera. Significa sacrificar heroicamente tu vida. Significa hacer un trabajo que tenga sentido. Ofrece seguridad interior y autocontrol. A través de sus acciones, las personas muestran su obra única al mundo y dejan entrever su inmortalidad. El trabajo de cada persona, creado en privado y ofrecido en público, es lo que hace del mundo humano un mundo. [^10]

El trabajo de subsistencia es mera supervivencia. Según el filósofo, el trabajo es una necesidad vital que sostiene nuestra existencia fisiológica. El trabajo es una dimensión estrecha de la vida humana sobre la que no tenemos ningún poder real. Hannah Arendt lamenta que el proyecto de vida propuesto colectivamente se haya visto truncado por el increíble lugar que ocupa, en las sociedades modernas, el trabajo de subsistencia. En su opinión, el trabajo debería quedar relegado a la esfera privada y tener una importancia secundaria. El trabajo debería estar en el centro de la vida social pública.

La reflexión filosófica de Hannah Arendt sobre la excesiva importancia del trabajo de necesidad vital y la desaparición del trabajo de posteridad ofrece muchos vínculos explicativos con la situación contemporánea. Nos ayuda a comprender la desaparición de los ciudadanos, sustituidos por consumidores. Nos permite comprender el absoluto desconcierto existencial que puede producir el desempleo en una sociedad que ha olvidado el trabajo y que se organiza públicamente sólo en torno al trabajo de subsistencia. Nos ayuda a comprender la actitud de las jóvenes generaciones ante el trabajo de subsistencia y su búsqueda de sentido en un oficio. Por último, nos permite examinar la pirámide de necesidades en las facetas del ser humano.

Desde un punto de vista físico, sobrevivir significa reproducirse y que los diferentes tengan hijos. Desde un punto de vista espiritual, significa ser eterno. Desde un punto de vista social, ser inmortal significa dejar huella de su paso por la tierra. La búsqueda de la inmortalidad, tal y como la entiende Hannah Arendt, es la expresión social de la necesidad de posteridad que impregna todos los aspectos de nuestra vida física, psicológica, social y espiritual. En este sentido, la descendencia se encuentra con la trascendencia. Conviene recordar que, tanto para Hannah Arendt como para Kenrick Douglas y sus colegas, la necesidad de posteridad combina dos necesidades: la producción íntima de una obra completamente única y personal, combinada con una relación con el mundo que pueda manifestarla.

Esta necesidad multidimensional de posteridad está completamente eclipsada por nuestra sociedad funcionalista y utilitarista. Y, sin embargo, es aquí, en estos actos que no tienen un significado económico o racional aparente para la vida cotidiana o el desarrollo de beneficios inmediatos, donde encontramos el sentido, la grandeza. La sociedad moderna actual, al proponer una vida limitada a la supervivencia vital y al desarrollo material, ha ido cortando el acceso a la satisfacción de la necesidad de posteridad. El resultado es un persistente sentimiento de insatisfacción e incumplimiento, en una sociedad en la que, paradójicamente, reinan la abundancia y el placer. Aquí es donde entra en juego la búsqueda de sentido. Esta búsqueda de sentido parece incapaz de encontrar reconocimiento en la sociedad hipermoderna. [^11]

He aquí, pues, las necesidades humanas fundamentales completadas con las reflexiones de Hannah Arendt. Las flechas representan las diferentes dimensiones del cuerpo humano según la medicina oriental. Se eligió esta representación antes que las distinciones clásicas de la medicina occidental moderna, porque amplía el espectro e incluye la vita comtemplativa de Hannah Arendt. Las flechas están punteadas para indicar que quedan fuera del alcance de la ciencia actual.

[Necesidades humanas básicas complementadas con las reflexiones de Hannah Arendt](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)].

### Necesidades o deseos intrínsecos y extrínsecos

Existe una diferencia entre necesidades o deseos intrínsecos y extrínsecos.

Las necesidades intrínsecas son motivaciones internas y personales que proceden del interior del individuo.

Las necesidades extrínsecas son motivaciones externas que proceden del entorno y del individuo. Están relacionadas con la imagen que el individuo quiere proyectar a los demás, e incluso a sí mismo. El consumo puede satisfacer necesidades de ambos tipos al mismo tiempo, y la mayor parte del consumo que satisface necesidades intrínsecas también tiene una dimensión extrínseca.

Las necesidades extrínsecas están estrechamente vinculadas a diversas normas sociales, al estatus, al comportamiento ético y al imaginario colectivo. A veces, la satisfacción de las necesidades extrínsecas tiene una desventaja sobre las intrínsecas[^13].

La distinción social y la imitación crean deseos extrínsecos.

Toda sociedad creará deseos y necesidades extrínsecos.

En nuestra sociedad, estos deseos se manipulan constante y poderosamente: la educación y el entorno social inculcan modelos de vida, o muy a menudo la sensación de que más es mejor, y la publicidad crea un deseo tras otro[^14].

La insatisfacción surge del desfase entre los deseos y su realización, o dicho de otro modo, entre las aspiraciones y los logros.

Para limitar esta insatisfacción, hay dos enfoques posibles: reducir el deseo o satisfacerlo. A menudo se hace hincapié en el segundo medio, la satisfacción del deseo, pero se olvida o descuida casi por completo la existencia del primero.

Así que lo primero que tenemos que hacer es comprender cómo se forman nuestros deseos y cómo podemos controlarlos. Y eso es lo que vamos a hacer.

## Human Nature

Pero antes de nada es esencial hacer un balance de la naturaleza humana, porque es un punto que aparece regularmente en los debates. Las ciencias sociales y las neurociencias llevan mucho tiempo intentando distinguir entre lo que es innato y lo que es adquirido, pero no hay consenso sobre la naturaleza humana.

Mientras que las ciencias naturales parecían apoyar el utilitarismo durante más de medio siglo, ahora es posible contradecir esta hipótesis: el hombre no sólo es egoísta, sino que tampoco es perfectamente racional. Este no utilitarismo del ser humano se aplica a las formas de vida más primitivas: la cooperación está inscrita desde los orígenes y sería, con la supervivencia del más apto, uno de los motores esenciales de la evolución[^15].

Creer que nuestra evolución se basa exclusivamente en la competencia interindividual es adoptar una visión sesgada de la evolución, cuando el papel de la cooperación ya estaba presente en la teoría evolutiva darwiniana. [^16]

Pero sea cual sea la naturaleza humana, los innumerables ejemplos de la historia demuestran que puede convertirse en cualquier cosa: tanto en lo peor como en lo mejor[^17][^18] La naturaleza humana no debería justificar nada sobre lo que deseamos llegar a ser. Sin duda podemos decidir en qué queremos convertirnos en nuestra sociedad, porque simplemente tenemos la oportunidad de hacerlo[^19].

Lo que es cierto, sin embargo, es que los humanos no son ermitaños; viven en sociedad y son sociables. El ser humano es ante todo un homo sociabilis, propenso a acercarse a sus congéneres y a interactuar con ellos. Necesitan hablar, intercambiar información, compartir conocimientos y emociones, pero también tienen una necesidad fundamental de existir a los ojos de los demás. Buscan la interacción humana por la sociabilidad que les proporciona y por el intercambio de imágenes positivas y gratificantes de las que se alimentan. [^20][^21]

## Dar forma a nuestros deseos

Antes hemos dicho que primero debemos entender cómo se forman nuestros deseos y cómo podemos controlarlos.

Desde hace más de mil millones y medio de años tenemos un pequeño órgano en el cráneo llamado estriado.

El cuerpo estriado libera dopamina, una sustancia que nos proporciona una sensación de placer y que reforzará el comportamiento identificado como un éxito. [^22]

Forma parte de todo un mecanismo de cooperación y diálogo entre diferentes estructuras y participa en la decisión, recibe información y compara las diferentes opciones, las ganancias potenciales, el esfuerzo requerido, la inmediatez de la recompensa.

Este sistema suele denominarse circuito de recompensa, pero la dopamina no es más que una hormona que predice el resultado de una acción. [^23]

Estas descargas de dopamina dan lugar a incentivos que nos impulsan a la acción y reforzarán ese comportamiento o actividad en el futuro.

Estos reforzadores, que también podemos llamar motivaciones profundas de nuestro cerebro, son : [^22]

- La comida
- el sexo
- Posición social
- Necesidad de información
- Menor esfuerzo
- Estimulación intelectual

Estas motivaciones fueron esenciales para nuestra supervivencia y el desarrollo de nuestra especie en tiempos primitivos y siguen siendo muy sensibles hoy en día. Las neuronas del cuerpo estriado y de todo el circuito de recompensa nos atraen hacia lo que satisface nuestros reforzadores, es decir, cualquier cosa que nos proporcione comida, sexo, estatus, facilidad e información.
No son intrínsecamente perjudiciales, pero, como vemos hoy, pueden explotarse para desarrollar economías productivistas, consumistas, individualistas y destructoras del medio ambiente.

Lo especial de este mecanismo, que condiciona nuestro cerebro no sólo a buscar comida, dinero, sexo y estatus social, es que busca obtener cada vez más dinero, cada vez más comida, cada vez más sexo o cada vez más estatus.

Cuando se coloca una rata cerca de un pasillo con un trozo de queso al final, sus neuronas liberan dopamina en cuanto entra en el pasillo. La primera vez que encuentra un trozo de queso al final del pasillo, sus neuronas descargan dopamina; la segunda vez, si encuentra el queso, sus neuronas dopaminérgicas no hacen nada. Pero si encuentra dos trozos de queso en lugar de uno, sus neuronas dopaminérgicas volverán a descargar. [^22]

El cuerpo estriado sólo hace todo esto en la medida en que puede hacer más y más.

Así que sólo conseguimos estimular nuestros circuitos del placer aumentando las dosis.

Otro de nuestros mecanismos es que el placer y la facilidad que podemos permitirnos ahora tienen cien veces más peso en nuestras decisiones que la consideración de un futuro lejano. Cuanto más lejano está un beneficio, menos valor tiene para nuestro cerebro. Es lo que se conoce como devaluación temporal.

Preferimos el placer inmediato al futuro, o tomar una decisión que nos beneficia ahora pero tendrá consecuencias perjudiciales a largo plazo.

Cuanto mayor es el retraso, más débil es la respuesta anticipatoria[^24].

Sin duda existen diferencias genéticas entre las personas, y algunas son más sensibles a su estriado, pero son sobre todo los elementos ligados a la educación y al entorno sociocultural los que resultan decisivos. [^25][^26]

En realidad, estas neuronas están en la base de nuestros procesos de aprendizaje

Nos condicionan: una vez que hemos aprendido que la comida sigue a un gesto, el propio gesto se vuelve atractivo. Es el principio del aprendizaje social: estamos condicionados a que nos guste tal o cual comportamiento. El principio del condicionamiento operante reina en un lugar de aprendizaje por excelencia, la escuela.

El egoísmo y el altruismo están igualmente motivados por el placer.

Cuando hacemos un regalo a alguien a quien queremos, no se nos ocurre conscientemente hacerlo para sentirnos bien, y sin embargo nos sentimos bien.

Así que podríamos preguntarnos: si el altruismo nos hace sentir bien personalmente, y además somos conscientes de ello, ¿sigue siendo altruismo, o no es más bien una especie de interés propio bien entendido?

Hay una delgada línea entre el altruismo y el egoísmo, una línea que ni siquiera existe a nivel biológico. No hay una hormona del altruismo o una hormona del egoísmo. Hay un acto para el que nuestro cerebro calcula la probabilidad de saber si será beneficioso para nosotros, y entonces obtenemos placer de él. Las áreas implicadas en el cerebro son comunes, pero lo que observamos es que hay una mayor activación del córtex prefrontal dorsolateral en el egoísmo debido a la evaluación más pronunciada de los cálculos de coste-beneficio. [^27] Pero, en general, somos nosotros quienes clasificamos los actos como altruistas o egoístas, por lo que esta definición puede variar según los modos de pensamiento.

Diremos que en el acto egoísta sólo existe la finalidad de obtener beneficios personales.

En el acto altruista, la intención es dar beneficios a los demás, al tiempo que se recibe un beneficio que se busca inconscientemente. Pero también puede existir la intención de dar beneficios a otro, mientras se recibe un beneficio buscado conscientemente. [^28]

El acto altruista es, por tanto, un intercambio mutuo que va en ambas direcciones, mientras que el acto egoísta sólo va en una dirección.

Por tanto, tanto el egoísmo como el altruismo pueden conducir al placer individual, y descubriremos cuál de estas 2 motivaciones es la más eficaz para lograr nuestro objetivo común.

Así que si algunas personas son más generosas, probablemente se deba a que sus cerebros han sido configurados de esa manera desde una edad temprana. Como la Madre Teresa, célebre por su inquebrantable dedicación a ayudar a los demás, su compasión y su amor incondicional por todas las personas que conocía, dedicó su vida a hacer el bien a los demás, sin preocuparse por su propia seguridad o comodidad personal, y se convirtió en uno de los símbolos más importantes de la caridad y el altruismo en todo el mundo. Cuando sólo tenía 6 años, su madre la llevaba a visitar a los más pobres entre los pobres, alcohólicos y huérfanos. Y le daba una recomendación invariable y obstinada: "Mi hija nunca acepta un bocado de comida que no se comparta con los demás". Compartir, enseñado en la cuna de forma habitual y con una preocupación constante por la aplicación y la ejemplaridad, se convirtió en una forma de condicionamiento tan poderosa que el estriado de la Madre Teresa nunca flaqueó en los ochenta años de su vocación. Su entorno y su educación contribuyeron así a desarrollar en ella valores y motivaciones como la generosidad, la compasión y el deseo de servir a los demás[^29][^30] Por supuesto, la religión, al propugnar estos valores morales de generosidad y altruismo, desempeña un papel importante en la configuración del cerebro de la Madre Teresa, y esto se deriva de nuestra búsqueda de sentido, de la que hablaremos más adelante.

Los estudios también han revelado la existencia de un plus de agilidad mental y de capacidad para resolver problemas, lo que conlleva la activación de zonas de memoria y una mejor memorización de la información comunicada por el profesor. Así, la dopamina que recibe un alumno cuando obtiene un buen punto modulará la plasticidad sináptica y consolidará los recuerdos. [^31]

Por supuesto, el cuerpo estriado no es la única parte de nuestro cerebro que toma decisiones. El córtex frontal es la parte más anterior del cerebro humano, y se ha hecho cada vez más grueso a lo largo de la evolución, reflejando nuestras crecientes capacidades cognitivas. Es la sede de la fuerza de voluntad y la planificación.

Por ejemplo, cuando se pidió a los participantes que eligieran entre veinte euros inmediatamente o treinta dentro de dos semanas, los que cogieron el dinero inmediatamente vieron cómo se iluminaba el cuerpo estriado, mientras que los que no lo hicieron vieron cómo se iluminaba el córtex frontal[^32].

Así pues, si queremos formar una comunidad humana capaz de afrontar los retos del futuro, estas son las conexiones que debemos desarrollar mediante el trabajo duro, la concienciación y la perseverancia.

## Controlar nuestros deseos

Para limitar los efectos nocivos de nuestro estriado, existen 3 soluciones posibles,

- Reprimir nuestro cuerpo estriado: Históricamente, la actividad del cuerpo estriado ha sido bloqueada por órdenes morales y por el esfuerzo de la voluntad contra la tentación. Pero esto no es una solución: si reprimimos nuestro cuerpo estriado, podemos afectar a nuestra capacidad de sentir motivación o placer, lo que puede dar lugar a problemas como la depresión y diversos trastornos. La represión tiene consecuencias negativas para el bienestar y el funcionamiento del individuo, por lo que no es una solución eficaz ni sostenible. [^33]

- Si jugamos con el estriado, podemos jugar con los reforzadores primarios para conseguir una sociedad ideal, por ejemplo, convirtiendo en norma social llevar un estilo de vida saludable y sostenible. Impulsar el desarrollo de los reforzadores esenciales para una buena sociedad.

- Pero una solución más eficaz y sostenible es recurrir a la capacidad única del ser humano, la conciencia. Porque la fuerza del cuerpo estriado proviene del hecho de que sus órdenes no son conscientes.

Cuando las neuronas del estriado se han acostumbrado a un determinado nivel social, se embotan y ya no sentimos nada, por lo que se hace imperativo subir otra muesca para estimularlas. Este proceso de incremento no produce ninguna satisfacción duradera, no puede traer la felicidad.

La mayoría de nuestras acciones se llevan a cabo con un nivel de conciencia muy bajo. La mayoría de las veces actuamos mecánicamente. Incluso cuando nos entregamos a actividades intelectuales, sería erróneo decir que lo hacemos conscientemente. [^34]

Disponemos de una corteza cerebral con una enorme potencia de cálculo, que utilizamos principalmente con fines utilitarios, de rendimiento y técnicos. Durante varios milenios, nuestro poder de abstracción, conceptualización y planificación se ha utilizado principalmente para diseñar herramientas que satisfagan a nuestro estriado.

Somos seres con un alto nivel de inteligencia pero un bajo nivel de conciencia. [^22]

La inteligencia desarrolla soluciones, genera cálculos, implementa objetivos y programas. Pero puede hacer todo esto sin conciencia. El ejemplo más evidente es la inteligencia artificial, que puede llevar a cabo tareas extremadamente complejas sin ninguna consciencia.

Los grandes sistemas de inteligencia artificial que conocemos hoy funcionan también con una noción de probabilidad y un sistema de refuerzo, que consiste en recompensar o castigar acciones para favorecer el aprendizaje de comportamientos deseables. En otras palabras, estos sistemas funcionan de forma similar a la dopamina en los seres humanos.

Por tanto, si queremos crear una conciencia en la IA, lo único que tenemos que hacer es comprender cómo se crea la nuestra. Este misterio
aún no está completamente entendido por las neurociencias, y volveremos a ello más tarde. Pero si lo es y conseguimos crear esta conciencia, la diferencia entre nosotros y una máquina se hará muy fina. Este posible avance plantea un gran número de cuestiones éticas y nos lleva a reflexionar sobre la percepción de nuestra propia conciencia.

Sea como fuere, parte de la solución a los retos a los que se enfrenta nuestra especie pasa por añadir más conciencia a nuestras acciones cotidianas.

La conciencia es también una caja de resonancia de nuestras percepciones.

Al desarrollar nuestra cámara de resonancia sensorial, podemos hacer creer a nuestro cuerpo estriado que está obteniendo más placer, mientras que nosotros le estamos dando menos en términos cuantitativos.

Una herramienta muy práctica para desarrollar nuestra conciencia es la mediación. [^35]

Elevar nuestro nivel de conciencia a un nivel comparable con nuestro nivel de inteligencia será sin duda un reto importante para desarrollar una buena sociedad y garantizar el futuro de nuestra especie. La evolución hacia una sociedad de la conciencia y hacia una economía del crecimiento mental.

## La necesidad de sentido y certidumbre

Una vez que el individuo dispone de autonomía de elección y creencia, es decir, del derecho a dar a su vida la dirección que desee, definir esa dirección se ha convertido en una tarea muy difícil. Como la dirección ya no la marca la religión ni un régimen político totalitario, cada uno de nosotros tiene que crear su propio sentido. Sin embargo, cuando tomamos conciencia de que nuestra existencia es breve y está condenada a la nada, nos enfrentamos a la necesidad de encontrar una justificación para nuestra existencia y nuestros actos, y esta toma de conciencia puede llegar a ser insoportable. [^36]

Porque el sentido es más importante para la vida que cualquier otra cosa, tiene valor de supervivencia para el ser humano. Así que para compensar nuestra falta de sentido vamos en busca de dinero, estatus, etc...

La señal de placer que se genera antes de lo que busca el animal es una predicción que hace nuestro cerebro sobre lo que va a ocurrir. Esta producción tiene lugar porque es una ventaja evolutiva, un mecanismo que aumenta las posibilidades de supervivencia del animal.

Al ser capaz de producir lo que va a ocurrir basándose en lo que observa a su alrededor, multiplica por diez su control y su poder de decisión. Puede buscar situaciones ventajosas y huir de las potencialmente peligrosas. Van un paso por delante de la realidad.

Los cerebros de los animales vertebrados han inventado una forma de hacer predicciones y adelantarse a la realidad. Esta capacidad de establecer vínculos entre el estado del entorno en un momento dado y su estado futuro es la base de lo que llamamos, en una especie tan célebre como el Homo Sapiens, sentido común.

Hemos creado un sentido de la sociedad humana, y sabemos que podemos ser aceptados y encontrar nuestro camino en ella, siempre que respetemos las normas y códigos establecidos. Esta sociedad no es un caos, tiene un orden, y este orden inteligible nos tranquiliza fundamentalmente.

Nuestra tendencia a detectar vínculos de significado en nuestro entorno está tan desarrollada y es tan irrefrenable que a veces nos lleva a discernir vínculos donde necesariamente no los hay.

Por ejemplo, cuando un guerrero sale a cazar una presa, se pone un collar y caza a la presa con él, la próxima vez que se ponga el mismo collar, el guerrero recibirá una pequeña sacudida de dopamina ante la posibilidad de capturar otra presa. El guerrero se sentirá más seguro de su futuro éxito.

Este sistema de anticipación ayuda a reducir la sensación de incertidumbre sobre el futuro. Observar, predecir y anticipar acontecimientos futuros y reducir la ansiedad forman parte de la noción de sentido. Esta ventaja es tan decisiva que hay muchas razones para creer que ha sido seleccionada por la evolución.

Detectar el sentido que nos rodea es tan crucial para nuestra supervivencia que las situaciones en las que este sentido se nos escapa desencadenan una ansiedad fisiológica aguda. Esta reacción la provoca nuestro organismo como instinto de supervivencia[^37].

Gracias a las herramientas de imagen cerebral, podemos averiguar qué ocurre en el cerebro. Entra en acción una pequeña franja de corteza cerebral, situada unos centímetros por encima del cuerpo estriado. Se trata de un pliegue de corteza cerebral situado en la interfaz entre dos hemisferios cerebrales, a su vez conectados al cuerpo estriado, y se denomina corteza cingulada anterior. Este córtex cingulado interno se enciende en cuanto las predicciones realizadas dejan de confirmarse por lo que realmente ocurre.

Esta señal de alarma significa una violación de la predicción. Si se invalidan demasiadas predicciones, resulta difícil organizarse y tenemos la impresión de caer en el caos. Sobrecargada, esta señal de error se vuelve perjudicial para la salud física y mental del individuo. Desencadena una potente reacción de estrés en el organismo: el córtex cingulado activa un circuito nervioso multirelacionado que desciende hasta un centro cerebral implicado en el miedo y la ansiedad, la amígdala, luego a las glándulas adrenocorticales situadas en los riñones y a núcleos neuronales del tronco encefálico, que liberan hormonas como el cortisol y la noradrenalina, cuyo efecto es poner el cuerpo en posición de huida, incluso de parálisis, y provocar una ansiedad que puede llegar a ser existencial.

Nuestro córtex cingulado desempeña así el papel de una señal de alarma que nos avisa cuando nuestro mundo ya no tiene ningún sentido detectable. Las consecuencias de esta reacción van de los trastornos del sueño a la depresión, la ansiedad, el deterioro de la memoria, las enfermedades cardiovasculares y la diabetes.

Si el nivel de orden y organización de nuestro entorno empieza a disminuir, esta parte central de nuestro cerebro se activa y nos alerta de la presencia de un peligro potencial para nuestra supervivencia. Si la sociedad es relativamente estable, donde las estructuras del trabajo, la familia y las relaciones humanas no cambian de forma demasiado imprevisible y arbitraria, el córtex cingulado es un factor de adaptación y ajuste, pero si los puntos de referencia cambian demasiado deprisa y constantemente sin dar tregua al individuo, puede ser extremadamente peligroso para sí mismo y para los demás.

Pero ante el vacío de sentido, ¿cómo reacciona la mente humana? Construye sistemas de representación imbuidos de sentido, orden y coherencia. Desde que el hombre existe, no ha hecho más que insuflar sentido a la realidad.

Los primeros intentos adoptaron la forma de relatos míticos de la creación y la naturaleza. La necesidad de sentido se deriva de la necesidad de control, y es un vástago de nuestro deseo de sobrevivir. E imaginar el mundo como un lugar habitado por el sentido calma el sistema interno de alerta en caso de error puntual de predicción en el mundo concreto.

Como resultado, la reacción de alerta desencadenada por el córtex cingulado se atenúa de forma natural en los creyentes. Pero hoy, para la mayoría de los habitantes del mundo occidental, ya no creemos y hemos perdido este sentido.

Los grandes sistemas de sentido religioso, pero también ideológico, democrático o filosófico, no son, por así decirlo, más que referentes devaluados, debilitados por el conocimiento científico y por la coexistencia de múltiples mensajes espirituales o ideológicos que intentamos tolerar, pero cuya pura multiplicidad basta para reducir a la nada la esperanza de que alguno de ellos pueda albergar por sí solo una verdad absoluta.

A lo largo de la historia, los rituales han aparecido sistemáticamente antes que los sistemas morales. La sincronización y la mímica de los rituales colectivos hacen al ser humano más sensible a los sentimientos, deseos y emociones de sus semejantes. Esta capacidad de ponerse en el lugar del otro se denomina empatía cognitiva y nos permite "ponernos en el lugar del otro", sentir lo que siente y pensar lo que piensa. Y esta capacidad se multiplica por diez con la imitación: los compañeros sincronizados sienten compasión el uno por el otro.

Los rituales calman nuestro córtex cingulado al permitirnos predecir con mayor fiabilidad lo que nuestros semejantes harán o dejarán de hacer por el simple hecho de que ya podemos predecir sus movimientos durante el ritual.

Pero es más difícil predecir los pensamientos de las personas que sus movimientos. Para lograrlo, ya no se trata simplemente de compartir gestos, sino de compartir representaciones mentales. Es decir, valores y visiones del mundo.

Puede haber distintas visiones del mundo, algunas de las cuales se dice que son normativas, en el sentido de que prescriben determinados comportamientos, y otras que sólo son positivas (o fácticas) y, a la inversa, no prescriben ningún comportamiento. Cuando esta visión prescribe lo que es bueno hacer o no, y la gente se adhiere a ella y ve el mundo de la misma manera que nosotros, esto reduce considerablemente la incertidumbre y de facto la actividad del córtex cingulado.

Pero su violación, en cambio, la activa fuertemente (así como otras estructuras cerebrales). Para nuestro córtex cingulado anterior, no respetar las normas morales es un error de predicción fundamental. Esto en sí mismo es un poderoso factor tranquilizador para el córtex cingulado, que busca orientarse entre sus semejantes.

En cuanto se tiene la certeza de que los demás creen en los mismos valores morales sagrados que uno, se puede empezar a predecir con mayor fiabilidad lo que es probable que hagan o dejen de hacer.

En el término religión, el verbo latino religere significa unir, por lo que entendemos que la religión sirve principalmente para unir a los individuos.

Con el paso del tiempo, la humanidad ha perdido su enfoque social y moral en favor de las predicciones sobre el mundo material. Como dijo el famoso filósofo Friedrich Nietzsche, "Dios ha muerto, todo vale".

Para nuestro córtex cingulado, éste es el principio de la angustia. Como el sentido de la ciencia sólo ofrece una visión positiva (o factual), no tiene el mismo poder tranquilizador que la religión y la moral.

La ciencia introduce un significado factual, pero no, al menos inicialmente, uno moral.

Hay que reconocer que el significado fáctico obtenido por la ciencia es colosal. Hoy en día, los seres humanos saben hasta cierto punto cómo se formó la Tierra y cómo llegaron a ella, y pueden trazar con bastante precisión la secuencia de acontecimientos que condujeron a la evolución de las especies, desde las bacterias hasta los primeros vertebrados, los mamíferos, los simios y el Homo Sapiens. Se ha dado respuesta a la pregunta sobre la causa de su existencia, su naturaleza y sus características físicas y mentales. Del mismo modo, el conocimiento de las leyes de la física y de los organismos vivos permite predecir un número asombroso de hechos concretos, como el tiempo atmosférico, los eclipses de luna y de sol, la potencia de un motor de combustión según el grado de refinamiento de la gasolina utilizada y la cilindrada del motor, etc.

Pero ya no sabemos lo que tiene sentido en términos humanos y existenciales. Porque aunque hayamos acabado con el sentido, no hemos acabado con la necesidad de sentido. La parte de nuestro cerebro que tardó cientos de miles de años en conformarse para encontrar sentido al mundo y a las estructuras sociales sigue estando ahí. [^36]

En nuestra sociedad vivimos el mito de Sísifo, un personaje mitológico griego condenado a hacer rodar eternamente una roca hasta la cima de una colina y volver a bajarla cada vez antes de llegar a la cima[^39]. [^39] En nuestra sociedad, la forma en que producimos ya no tiene sentido. Estamos en un mundo de usar y tirar. Y cuando nada dura, la vida se convierte en un despilfarro. Todos nos enfrentamos a este problema en mayor o menor medida.

Además, con el cambio climático, ya nada es estable, ni el ciclo de las estaciones, ni el nivel del mar, ni las catástrofes naturales. El mundo está ahora en movimiento, y este movimiento va a superar nuestras capacidades de endeudamiento.

La palabra es dura, pero la incertidumbre mata, mina nuestros cerebros, destruye la aspiración humana fundamental al sentido.

## Nuestras reacciones ante la incertidumbre

El sentimiento de indeterminación activa el córtex cingulado, que constata con dolor la imposibilidad de hacer predicciones sobre el futuro, de definirse a uno mismo y de ver una trayectoria vital clara.

La ecoansiedad, por ejemplo, es una nueva forma de ansiedad derivada de la conciencia paralizante de la destrucción de nuestro planeta. [^38]

En un mundo en el que existe un nivel máximo de incertidumbre, el córtex cingulado se las arreglará para restablecer la certeza a escalas más pequeñas, ya que no puede hacerlo a la escala más global de su existencia. Es lo que se denomina microcertidumbre.

Las drogas que afectan a nuestras neuronas pueden ser un sustituto del sentido. La cocaína, por ejemplo, destruye la incertidumbre.

La autoestima es la invención egocéntrica de un mundo individualista y una forma de compensar la pérdida de sentido.

El dinero es el pasaporte definitivo para liberarse de la angustia de la incertidumbre.

Pero en el juego de la competición crecen las desigualdades. Los perdedores de esta carrera, deshumanizados por la ruptura del cordón umbilical, se convertirán entonces, en el peor de los casos, en una forma degradada de humanidad. Es el tipo de humanidad que niega la humanidad de los demás para garantizar su propia exclusividad. Estamos hablando de identidad.

Porque de esta tensión surge automáticamente una necesidad urgente: saber de una vez por todas quién eres. Y la manera más sencilla de aclarar tu identidad es definirte a ti mismo por referencia a un grupo al que perteneces. Definirse a través de su relación con un grupo satisface una necesidad fundamental: encontrar una manera de ajustarse a las normas vigentes, lo que limitará las reacciones intempestivas del córtex cingulado.

De hecho, una de las principales fuentes de sentido mencionadas regularmente en las encuestas es el sentimiento de pertenencia a un grupo social[^40].

Las desigualdades empujan a las sociedades hacia un repliegue identitario. Donde lo único que cuenta es la capacidad de utilizar la máquina de producción para los propios fines. El modelo económico neoliberal, basado en la competencia, la movilidad de los individuos, la aceleración del ritmo de trabajo y la reducción del gasto público dedicado a la protección y la distribución equitativa de la riqueza, provoca un impulso autoritario e identitario destinado a calmar las reacciones del individuo ante la incertidumbre y la falta de sentimiento de pertenencia social.

La anomia se refiere al sentimiento de pérdida de orden y lógica en la sociedad.

Las personas que más a menudo tienen pensamientos nostálgicos son también las que tienen la impresión más clara de que la existencia tiene sentido[^41].

Nuestro cerebro es extremadamente bueno para la negación.

Cuando nuestras acciones no concuerdan con nuestras representaciones mentales, se crea la disonancia cognitiva. La contradicción también activa la disonancia cognitiva.

Esto se debe a que el cerebro humano busca la coherencia y la razón en cualquier circunstancia. Por lo tanto, intentará resolver la disonancia que representa la incertidumbre, y para ello modificará sus acciones para que estén en consonancia con su pensamiento, o modificará su pensamiento para que esté en fase con sus acciones.

Las investigaciones sobre la disonancia cognitiva demuestran que la mayoría de las veces son los pensamientos los que se adaptan a las acciones y no al revés. [^42][^43]

Perdemos el contacto con la realidad porque ya no sabemos interpretarla. El sentido nos protege porque nos sugiere formas de interpretar el mundo y de actuar más eficazmente para dominarlo y protegernos de las amenazas que contiene.

A medida que se acerque el espectro de las grandes catástrofes, los repliegues comunitarios se acentuarán, al igual que los comportamientos compensatorios de hiperconsumo, inflación del ego y negación.

La falta de sistemas de sentido nos impulsa a consumir bienes materiales, sobre todo en situaciones de gran incertidumbre, aceleración y competencia. El hipermaterialismo es el paliativo de la incertidumbre y la competencia[^36].

## Soluciones a la falta de sentido

El cerebro busca predecir el mundo para controlarlo mejor, y lo consigue o bien a través del sentido o bien a través de la tecnología. Cuando la tecnología progresa, ya no necesita sentido. Y cuando la tecnología fracasa, necesita sentido.

Hoy tenemos tecnología, pero ha fracasado, porque está firmando nuestra extinción. [^44]

Ante el desastre actual y el que se avecina, debemos detener cuanto antes la máquina de consumo en que se ha convertido la humanidad, al tiempo que buscamos activamente formas de reparar la Tierra.

Si cuanto más disminuye el sentido, más aumenta el consumo, a la inversa, cuanto más aumenta el sentido, más disminuye el consumo.

Tenemos 8.000 millones de córtex cingulados ocupados engañando su miedo a la muerte y a lo impredecible, ahora necesitamos mantenerlos ocupados engañando este miedo uniéndolos en torno a un significado compartido[^36].

Este sentido debe reunir a miles de millones de personas de orígenes culturales diferentes, con herencias civilizatorias dispersas y bagajes intelectuales heterogéneos, para hacerles olvidar su obsesión por poseer y explotar.

Hay dos significados: el cósmico y el social.

El primero se basa en nuestra comprensión de las leyes de la naturaleza, la materia y el universo. El segundo implica nuestra capacidad de actuar, en un contexto social, de acuerdo con lo que creemos que está bien o mal.

Crean un sistema moral, definiendo un bien y un mal que regulan la acción social e individual.

Lo sagrado es el eje a partir del cual se construye el sentido en las asambleas humanas. Hoy, la humanidad moderna ha perdido lo sagrado al deconstruir la realidad y descubrir que todo, desde el movimiento de los átomos en una estrella fundida hasta el flujo de iones en la membrana de tus neuronas mientras lees estas líneas, obedece a leyes mecanicistas en las que el bien y el mal no tienen cabida.

La consecuencia de esta desmoralización del mundo es que cada individuo sobre la superficie de la Tierra puede decidir lo que le parece bueno o malo.

Pero ningún sentido colectivo puede existir sin la noción de lo sagrado. La solución es recrear lo sagrado, y ese sagrado puede ser nuestra Tierra.

Lo sagrado sólo puede manifestarse a través del ritual, y la humanidad tendrá que inventar nuevos rituales para salir del punto muerto al que ha llegado. Como hemos visto, las prácticas sincrónicas ayudan a calmar el córtex cingulado. Tales rituales tendrán que hacer sagrada la preservación del planeta.

Procediendo de este modo, los seres humanos podrán dotarse de medios eficaces para reducir la carga mental de ansiedad ligada al individualismo. Saber que los ciudadanos de mi país, de todo el continente y, si es posible, de todo el planeta, consideran sagrado lo que yo mismo considero un valor insuperable crea las bases de un nuevo pacto de pertenencia y confianza.

El hombre es un cooperador condicional; es capaz de inmensos sacrificios siempre que tenga la seguridad de que los demás miembros de su comunidad harán lo mismo. [^45]

Al hacer de la pureza del carbono y de la preservación del equilibrio biológico y geológico valores sagrados, los miles de millones de humanos pueden devolver la actividad de sus córtex cingulados a niveles aceptables y dejar de depender de bienes materiales, drogas, dinero y sustitutos del ego para mantener su propia existencia.

La ecología puede ofrecer una visión del mundo común a todos los humanos.

Con el conocimiento, la lista de actos compatibles con el valor sagrado es una cuestión de ciencia, de mediciones y cálculos realizados por el colectivo humano.

El reto consiste en crear un sentimiento de identidad global que ofrezca a todos la sensación de ser aceptados en un grupo, sin necesidad de demostrar su valía y elegibilidad acumulando bienes materiales o apoyos del ego.

Hoy tenemos que aceptar que la ciencia puede decirnos no sólo lo que es verdad y lo que es mentira, sino también lo que está bien y lo que está mal.

El tercer significado que vendrá será, por tanto, ecológico.

Para desarrollarlo, necesitamos desarrollar el conocimiento a través de la educación, transmitir el conocimiento de los sistemas vivos, la biodiversidad, la climatología, la ecología y la evolución de las especies.

Esto creará un sentido del asombro, porque el asombro es un poderoso antídoto contra la angustia existencial que nos atenaza, mediante el descubrimiento de la infinita riqueza de los seres vivos, la belleza de los paisajes, la flora y la fauna, porque en ellos encontramos una fuente de sentido.

Puesto que todos habitamos la misma Tierra, actuamos sobre ella y sufrimos las consecuencias de su degradación, todos somos llevados a reconocer el mismo valor sagrado.

De este modo, los ciudadanos podrán comprometerse con acciones coherentes con sus valores y convicciones, y experimentarán una sensación de sentido.

Convertirse en protagonista de este cambio ya es en sí mismo una fuente de sentido, y de alineación entre nuestras opiniones y nuestras acciones.

Tenemos que revisar los fundamentos mismos de nuestra civilización. La lógica de producción y consumo "justo a tiempo" que sustenta nuestras economías es insostenible. En el próximo capítulo descubrirás nuestras propuestas en materia de "economía".

En el fondo, todos tenemos una aspiración inmensa, buscamos un sentido y no sabemos cómo satisfacerla. El objetivo de esta obra es satisfacer ese anhelo sin esperar a que se haga realidad, porque ya está ahí desde el principio.

## Necesidad de ficción - El cerebro narrador

El cerebro humano, esa extraordinaria máquina de contar historias, está en el centro de nuestra capacidad para crear historias, para bien o para mal. Forja las historias que nos permiten justificar nuestros actos y darles sentido. Esta capacidad narrativa no es un accidente, sino el producto de una compleja evolución, que revela la profundidad de nuestra necesidad de sentido y orden en un mundo caótico. [^46]

Así que lo que hace especial a nuestra especie no es, por ejemplo, que lleve haciendo la guerra desde el alba de los tiempos -los chimpancés y las hormigas lo hacen igual-, sino que haya hecho de ello toda una historia... y millones de historias.

Los demás animales no hacen el mal por el mal, ni hacen nada por el bien; actúan principalmente por impulso de necesidades o instintos inmediatos. Los seres humanos, en cambio, buscan un sentido a sus actos y a los de los demás, a menudo a través del prisma de la moral, el bien y el mal. Esta búsqueda de sentido es, por tanto, una manifestación de nuestras funciones cerebrales superiores, que nos impulsan a buscar explicaciones y justificar nuestro comportamiento en un contexto más amplio.

Lo que consideramos el "yo" no es una entidad aislada o autónoma, sino más bien un mosaico de rasgos, creencias y valores tomados, compartidos y a veces impugnados de quienes nos rodean. Cada interacción, cada intercambio cultural o social contribuye a conformar nuestra identidad, haciéndola dinámica y evolutiva. Nuestro yo se teje a partir de los demás. Nos definimos en el espejo de nuestras relaciones, cada persona es un reflejo que nos ayuda a formar nuestra propia imagen[^49].

Este proceso de construcción narrativa resuena con las enseñanzas budistas sobre la ilusión del yo. Según el budismo, el "yo" que percibimos como una entidad estable y continua es una ilusión, una construcción de la mente[^47]. [^47] Volveremos sobre este punto con mayor profundidad en la sección de Religión de la Parte 2. Del mismo modo, la neurociencia revela que nuestra identidad es una historia que nos contamos a nosotros mismos, una secuencia coherente fabricada desde cero por nuestro cerebro para mantener una ilusión de continuidad[^48].

Identificarnos con nuestras historias y nuestro "yo" construido puede encerrarnos en patrones de pensamiento limitantes. Reconocer que esta construcción narrativa es una ilusión puede ser liberador. Nos invita a cuestionar los relatos tradicionales y a abrirnos a nuevas posibilidades de percibir y experimentar el mundo.

La interacción entre "vida real" y ficción adquiere una nueva dimensión. En lugar de ver estas esferas como distintas, podemos entender que están intrínsecamente vinculadas, alimentándose mutuamente. Nuestras narrativas personales y colectivas no son meras distracciones o escapatorias, sino poderosas herramientas de transformación y comprensión.

Reconocer la naturaleza narrativa de nuestro cerebro y admitir la ilusión del yo no significa rechazar nuestra identidad o nuestras historias, sino verlas como lo que son: construcciones flexibles, modificables y profundamente humanas. Nos anima a dejar de seguir las narrativas tradicionales y decirnos a nosotros mismos "Así es como el mundo siempre ha sido y siempre será", sino a comprender y decirnos a nosotros mismos "Sólo vemos lo que vemos".

Nos ofrece un camino hacia una comprensión más rica y matizada de nosotros mismos y del mundo, dejando de aferrarnos a nuestra identidad como realidad inamovible e identificándonos exclusivamente con ella, para permitirnos vislumbrar en nuestras vidas la posibilidad de reimaginar el mundo y a nosotros mismos.

## Efectos y sesgos cognitivos

Aquí vamos a repasar los diferentes efectos y sesgos de nuestro cerebro, ya que pueden afectar a nuestra percepción, memoria, juicios, decisiones y comportamiento de diversas maneras, influyendo en la forma en que procesamos e interactuamos con la información. Esta comprensión de nosotros mismos nos permitirá desarrollar herramientas eficaces para nuestra sociedad, en particular para modelar nuestro proceso de toma de decisiones y evitar las consecuencias de estos sesgos que nos impedirían alcanzar nuestro objetivo común.

La lectura detallada de los efectos cognitivos y los sesgos, aunque aconsejable, no es en absoluto necesaria y puede omitirse para su comodidad lectora. Se han resumido en la sección siguiente.

### Resumen de efectos cognitivos y sesgos

He aquí una lista no exhaustiva de los sesgos cognitivos:

- Sesgo de selección: Somos más propensos a seleccionar la información que se ajusta a nuestras creencias y opiniones preconcebidas, en lugar de considerar una gama más amplia de información. [^50]

- Ilusión de memoria: tendemos a sobrestimar la fiabilidad de nuestros recuerdos y a colorearlos con nuestra percepción actual[^51].

- Racionalización de las creencias: tendemos a justificar nuestras decisiones y comportamientos, aunque sean erróneos o perjudiciales, interpretándolos de forma favorable para nosotros[^53].

- Sesgo de sobrestimación: tendemos a sobrestimar nuestra capacidad para predecir acontecimientos futuros y controlar situaciones[^52].

- Sesgo de optimismo: tendemos a sobrestimar la probabilidad de obtener beneficios en el futuro y a infravalorar los riesgos potenciales. [^54]

- Sesgo de reducción de la complejidad: tendemos a simplificar la complejidad de las situaciones para hacerlas más manejables[^55].

- Sesgo de inferencia causal: tendemos a atribuir una causa a un suceso aunque no esté necesariamente justificado o sea razonable[^56].

- Sesgo del efecto reductor: tendemos a sobrestimar los efectos positivos de una acción y a infravalorar los negativos[^57].

- Sesgo del efecto de primacía: nos influye la primera información que recibimos y tendemos a darle más peso que a la información posterior[^58].

- Sesgo del efecto de similitud: es más probable que nos gusten las personas parecidas a nosotros y que confiemos en ellas[^59].
- Heurística de disponibilidad: a menudo utilizamos una estimación rápida e intuitiva para evaluar la probabilidad o frecuencia de un suceso, basándonos en la información más fácilmente disponible en nuestra memoria[^60].

- Presión de grupo: nos influye la percepción de lo que los demás piensan de nosotros y nos sentimos presionados para ajustarnos a sus opiniones y comportamientos[^61].

- Sesgo del halo: tendemos a generalizar una opinión positiva o negativa de una persona basándonos en una sola característica[^62].

### Explicación detallada de los efectos y el sesgo cognitivo

- Sesgo de confirmación

El sesgo de confirmación es un sesgo cognitivo que se produce cuando favorecemos la información que respalda nuestras opiniones y creencias. Como buscamos un significado, seleccionamos la información que va en nuestra dirección. La solución a esto es ser nuestro propio contradictor, poner a prueba nuestros pensamientos e intentar demostrar que son erróneos.

Así que las contradicciones en nuestros juicios no deben herirnos, sino despertarnos y espolearnos a la acción. No nos gusta la corrección de nuestras opiniones, pero al contrario, debemos estar abiertos a ella y ofrecernos a ella, y que llegue en forma de conversación y no de sermón.

No debemos, con razón o sin ella, buscar la manera de deshacernos de cada oposición, sino profundizar para ver si puede ser acertada.

- Sesgo de disponibilidad

El sesgo de disponibilidad es otro sesgo cognitivo que puede afectar a la toma de decisiones. Se produce cuando estimamos la probabilidad de un suceso o la frecuencia con que ocurre basándonos en ejemplos que nos vienen rápidamente a la mente. Este sesgo puede llevarnos a infravalorar la probabilidad de sucesos raros y a sobrevalorar la de sucesos comunes o recientes. Sobreestimamos nuestro razonamiento favoreciendo la información directamente accesible a nuestra memoria.

Para evitar este sesgo, es importante tener en cuenta una muestra representativa de información y tratar de ser objetivo al evaluar las probabilidades. También es importante no basarse únicamente en la información inmediatamente disponible, sino buscar información adicional que pueda darnos una visión más completa y equilibrada.

- Efecto halo

El efecto halo es el mecanismo que nos hace atribuir cualidades a alguien basándonos en su aspecto físico.

Por ejemplo, una web de citas llamada cupidexe llevó a cabo un experimento en el que se puntuaban los perfiles de las personas en función de su personalidad y su aspecto físico. Los que tienen una puntuación física baja tienen una puntuación de personalidad baja y, a la inversa, una puntuación física alta significa una buena personalidad. Uno pensaría que como parecen tener una buena personalidad tendrían un buen físico, pero en el 1% superior algunos perfiles de personalidad están vacíos...
Hicieron otro experimento para comprobarlo. En él, los usuarios se encontraban con perfiles con texto y otras veces no, y tuvieras texto o no se te daba la misma puntuación.

Hay otros estudios y estadísticas que confirman este efecto. Por ejemplo, todos los presidentes americanos son altos, y cuanto más alto eres, más dinero ganas.

- Efecto Barnum

El efecto Barnum se refiere a la tendencia de las personas a creer que los comentarios vagos o generales son descripciones exactas de su personalidad o su vida. Este fenómeno debe su nombre al famoso showman P.T. Barnum. Barnum, que utilizaba lecturas vagas de la personalidad para atraer a la gente a sus espectáculos.

Por ejemplo, un test de personalidad que diga "usted es una persona afectuosa y cariñosa con su familia y amigos" es lo suficientemente vago como para ser considerado cierto para la mayoría de las personas. Sin embargo, no da detalles concretos sobre la persona, lo que puede provocar un sentimiento de identificación con la descripción general, aunque no sea muy personal.

El efecto Barnum puede verse reforzado por la necesidad de reconocimiento y la sed de autodeterminación de las personas. A la gente le gusta sentirse comprendida y valorada, lo que puede llevarles a aceptar descripciones generales como verdaderas para ellos.

Es importante tener en cuenta el efecto Barnum al analizar tests de personalidad o comentarios sobre tu vida o personalidad. Siempre es mejor buscar información más específica y detallada para tener un conocimiento más preciso de uno mismo.

- Efecto de anclaje

El efecto de anclaje se refiere a la tendencia de las personas a basar sus estimaciones o decisiones en un primer dato, denominado "ancla". Este primer dato puede influir mucho en las estimaciones posteriores, aunque sea irrelevante o incluso erróneo.

Por ejemplo, si le piden que adivine el número de habitantes de su ciudad y le dan una cifra alta como primer dato, tenderá a hacer una estimación más alta que si le hubieran dado una cifra más baja. Del mismo modo, si estás negociando el precio de un objeto, la primera propuesta suele considerarse el ancla e influirá en las siguientes.

El efecto de anclaje puede utilizarse consciente o inconscientemente en situaciones de negociación, venta o persuasión, lo que puede dar lugar a decisiones poco racionales y sesgadas.

- Presión de grupo

La presión de grupo es un fenómeno social que describe la tendencia de los individuos a ajustarse a las opiniones, actitudes y comportamientos de un grupo al que pertenecen o con el que se sienten afiliados. Puede darse cuando los individuos intentan ajustarse a las normas sociales y evitar la desaprobación o el aislamiento del grupo.

Por ejemplo, cuando una persona se encuentra en un grupo de personas que tienen una determinada opinión o actitud, puede verse influida por estas opiniones y actitudes, aunque no se correspondan con sus propias opiniones o actitudes. Esto puede ser especialmente cierto en situaciones en las que la persona quiere ser aceptada por el grupo y evitar la desaprobación o la exclusión.

La presión de grupo también puede surgir cuando una persona se siente obligada a ajustarse al comportamiento de un grupo para evitar sentirse diferente o incómoda. Por ejemplo, alguien de un grupo que bebe alcohol puede verse influido a beber alcohol él mismo, aunque no quiera.

Es importante ser consciente de la presión del grupo y no ceder a las opiniones o el comportamiento de un grupo simplemente para evitar la desaprobación o ajustarse a las normas sociales. Siempre es mejor seguir las propias opiniones y convicciones que ceder a la presión del grupo.

- Heurística de la disponibilidad

La heurística de la disponibilidad es un atajo mental que solemos utilizar para evaluar la frecuencia o probabilidad de un acontecimiento basándonos en los ejemplos más recientes y fácilmente disponibles en nuestra memoria. Esto puede llevarnos a cometer errores de juicio, ya que los ejemplos que tenemos en mente pueden estar sesgados.

Por ejemplo, si una persona acaba de enterarse por televisión de un accidente de avión, es posible que se sienta más preocupada por su seguridad en el próximo vuelo, ya que este suceso está fresco en su memoria y fácilmente disponible. Sin embargo, esta persona puede subestimar la seguridad de la aviación en general, ya que los accidentes aéreos son en realidad muy poco frecuentes.

Es importante recordar que la heurística de la disponibilidad puede llevarnos a cometer errores de juicio y a buscar información más completa y fiable para evaluar la frecuencia o probabilidad de un suceso. También conviene recordar que los medios de comunicación pueden influir en nuestra percepción de los acontecimientos destacando más unas noticias que otras. Esto puede llevar a una sobrerrepresentación de sucesos raros o dramáticos, y dar una falsa impresión de su verdadera frecuencia. Por eso es importante recordar que no debemos dejarnos influir únicamente por lo que vemos u oímos en los medios de comunicación.

En conclusión, la heurística de la disponibilidad puede ser útil para tomar decisiones rápidas en situaciones cotidianas, pero es importante considerarla con cautela cuando se trata de hacer juicios más importantes o tomar decisiones con consecuencias a largo plazo. Es preferible buscar información completa y fiable a la hora de evaluar probabilidades y riesgos, en lugar de confiar únicamente en lo que tenemos a mano en la memoria.

## Conclusión

Esta sección sobre el Homo Sapiens ya ha abierto varias vías. Desarrollaremos algunas de ellas en esta conclusión, pero esta lista no es exhaustiva. Sobre todo, desarrollaremos muchas otras en los capítulos siguientes, partiendo de la base aquí desarrollada.

Ahora conocemos las necesidades humanas que deben satisfacerse para alcanzar la plenitud total. Hemos visto que es posible transformar nuestras necesidades extrínsecas y que será necesario hacerlo para hacerlas compatibles con un proyecto común que nos permita a todos satisfacer nuestra necesidad de sentido.

Este sentido colectivo debe existir a través de la noción de lo sagrado. Y no hay nada más sagrado que lo que todos tenemos en común y cuya supervivencia está directamente ligada a nosotros, es decir, nuestra tierra y sus ecosistemas. Así pues, la ciencia y la ecología ya no sólo son capaces de decirnos lo que es verdad o mentira, sino también de dictarnos nuestros valores, lo que está bien o mal.

Hemos visto que no hay naturaleza humana que nos impida tener éxito en este proyecto, que no hay un yo inamovible, y en esta tarea el desarrollo de nuestra conciencia será nuestro mejor aliado. Igualmente importante será la educación de las nuevas generaciones, animándolas a ser altruistas y a maravillarse con la naturaleza para protegerla.

A través del estudio de los diferentes efectos cognitivos y sesgos, hemos reunido cada vez más información necesaria para entendernos a nosotros mismos y, por tanto, también para desarrollar herramientas que tengan en cuenta estos parámetros para que sean eficaces a la hora de ayudarnos en nuestro proyecto común.

## Fuentes (lista no exhaustiva)

[^1]: [Informe del IPCC](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia : Pirámide de necesidades](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectivas de la ciencia psicológica
[^4]: [Organización de las Naciones Unidas para la Agricultura y la Alimentación (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Cifras y análisis sobre la vivienda precaria en Francia, incluido el número de personas sin hogar y las condiciones de vida precarias](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Renuncia a la asistencia sanitaria por motivos económicos en la aglomeración de París:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [La Fondation de France publica la 13ª edición de su estudio sobre la soledad](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap International: las personas con discapacidad siguen estando entre las más excluidas del mundo](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivación y personalidad, 1970 (segunda edición)
[^10]: Arendt Hannah, La condición del hombre moderno, 1961
[^11]: Valérie Jousseaume, Orgullo de Plouc: una nueva narrativa para el campo
[^12]: Emmanuel Todd, El origen de los sistemas familiares, 2011 - ¿Dónde estamos? Un esbozo de la historia de la humanidad, 2017 - La lucha de clases en Francia en el siglo XXI, 2020.
[^13]: Robert J. Vallerand, Hacia un modelo jerárquico de motivación intrínseca y extrínseca, 1997.
[^14]: Jean Baudrillard, La sociedad de consumo, 1970
[^15]: Robert Wright, El animal moral: por qué somos como somos, 1994
[^16]: Charles Darwin, El origen de las especies: Capítulo IV / III, 1859 & La descendencia del hombre y la selección sexual: Capítulo VII, 1871
[^17]: Hannah Arendt, Los orígenes del totalitarismo, 1951
[^18]: Albert Camus, La peste, 1947
[^19]: Jean-Paul Sartre, El existencialismo es un humanismo
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, Homo Sociabilis: Reciprocidad
[^21]: [RIM Dunbar, La hipótesis del cerebro social y sus implicaciones para la evolución social](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, El bicho humano
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, El papel del cuerpo estriado en la recompensa y la toma de decisiones](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, El cerebro primigenio
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior (El papel de la cultura en el comportamiento relacionado con la recompensa).
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, Altruismo: ¿un enigma?
[^29]: Matthieu Ricard, "Altruismo: ¿gen o educación?".
[^30]: Kathryn Spink, "Madre Teresa: una vida al servicio de los demás"
[^31]: Wolfram Schultz, "El papel de la dopamina en el aprendizaje y la memoria", 2007
[^32]: Read, D., & Northoff, G, Correlatos neuronales de las decisiones impulsivas y reflexivas. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, El cerebro y el placer.
[^34]: David Eagleman, El cerebro inconsciente
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 y Gloria Y. Yeh,3 A Systematic Review and Meta-analysis of the Effects of Meditation on Empathy, Compassion, and Prosocial Behaviors (Revisión sistemática y metaanálisis de los efectos de la meditación en la empatía, la compasión y los comportamientos prosociales), 2018.
[^36]: Sébastien Bohler, ¿Dónde está el sentido?, 2020
[^37]: [Roy F. Baumeister, La necesidad de sentido: una perspectiva psicológica](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, La ecoansiedad: análisis de una angustia contemporánea
[^39]: Albert Camus, El mito de Sísifo, 1942
[^40]: [Encuesta "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides y Tim Wildschut, Nostalgia y búsqueda de sentido: exploración de los vínculos entre la nostalgia y el sentido de la vida en Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, El hombre irracional 2001
[^43]: Elliot Aronson, La teoría de la disonancia cognitiva
[^44]: Elizabeth Kolbert, La sexta extinción, 2015
[^45]: Robert Axelrod, El dilema del prisionero
[^46]: Nancy Huston, La especie fabulista
[^47]: Serge-Chritophe Kolm, Felicidad y libertad
[^48]: David M. Eagleman, Incógnito, 2015
[^49]: William James, Los principios de la psicología, 1980
[^50]: [Wikipedia: Sesgo de selección](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, La ilusión de la memoria
[^52]: David Dunning y Justin Kruger, Unskilled and unaware of it: How difficulties in recognising one's own incompetence lead to inflated self-assessments, 1997.
[^53]: [Festinger y Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Optimismo poco realista sobre los acontecimientos futuros de la vida, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones y Victor H. Harris, "La atribución de actitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Pensar, rápido y despacio, 2011
[^58]: [Asch, S. E., Formación de impresiones de la personalidad. , 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. El paradigma de la atracción, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Disponibilidad: Una heurística para juzgar la frecuencia y la probabilidad, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opiniones y presión social, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. A constant error in psychological ratings, 1920
