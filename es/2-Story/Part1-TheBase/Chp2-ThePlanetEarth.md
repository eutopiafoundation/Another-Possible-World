---
title: Planet Earth
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

> Esta página aún se está escribiendo, por lo que pueden faltar algunas partes.
> {.is-info}

## Descripción rápida de la Tierra

### El mapa del mundo correcto

[![Proyección Gall-Peters](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

La proyección Peters (o proyección Gall-Peters en honor a James Gall (1808-1895) y Arno Peters (1916-2002)) es una proyección cartográfica que, a diferencia de la proyección Mercator, permite tener en cuenta la superficie real de los continentes. Pero localmente, esta proyección no conserva los ángulos, lo que provoca la deformación de los continentes, a diferencia del mapa de Mercator. [^1]

### Descripción rápida de los diferentes ecosistemas

## El ciclo del agua

### Presentación de las diferentes formas de agua en la Tierra

### Descripción del ciclo del agua y sus interacciones con el medio ambiente

### Consecuencias de las actividades humanas en el ciclo del agua

## Fotosíntesis y producción de oxígeno

### Descripción de la fotosíntesis y su papel en la producción de oxígeno.

Desde su origen, la Tierra no ha dejado de crear información. Gracias a esta información, se ha creado el movimiento ordenado de la materia, dando lugar a una diversidad excepcional de formas, colores y movimiento: la vida tal y como la conocemos.

De toda esta información disponible, una ha sido la fuerza motriz más que ninguna otra: se trata de la información codificada en los genes de las plantas portadoras de los mecanismos de la fotosíntesis. Este proceso, específico del reino vegetal, hace que el sistema vivo sea particularmente eficaz y no tenga equivalente. La fotosíntesis tiene poderes tanto energéticos como químicos. Capta la energía bruta e inmaterial -la energía luminosa-, la almacena y la redistribuye con una precisión extrema, a una escala molecular inferior a un nanojulio. También permite combinar elementos minerales dispersos en materiales y moléculas, que a su vez contienen información en sus interacciones y energía en su composición.

Se trata de un fenómeno prodigioso desde el punto de vista de las leyes físicas de la energía. Mientras que las leyes de la física dictan que la energía emitida por una fuente debe dispersarse y degradarse, volviéndose cada vez menos utilizable, las plantas son capaces de canalizarla. Gracias a la información contenida en su biblioteca genética, la planta va así en contra de las leyes físicas de la energía, lo que se conoce como entropía. En su lugar, forma un bucle de entropía negativa dentro del universo: la materia se ordena y organiza, y la energía se canaliza. Antes de volver a desintegrarse en su viaje por la vida. Comparada con la cantidad infinitamente mayor de materia del universo, la materia orgánica es como un encaje: puede adoptar cualquier forma. Es este conjunto de moléculas en forma de ganchos, poleas, ruedas, cables, puertas, puntos y cadenas lo que constituye la materia viva[^2].

### Consecuencias de las actividades humanas sobre la fotosíntesis y la producción de oxígeno

## El ciclo del carbono

### Presentación del ciclo del carbono y su interacción con el medio ambiente.

### Consecuencias de las actividades humanas sobre el ciclo del carbono y los gases de efecto invernadero.

Además, los materiales y las energías que nos permiten realizar tales proezas modifican el termostato global, compuesto principalmente de carbono, y añaden gases de efecto invernadero a la atmósfera cuando se utilizan. Y la atmósfera se calienta. La piedra caliza que constituye la base de cementos, yesos y cales es una roca carbonosa; el petróleo, el carbón y el gas, que constituyen el 80% de las fuentes de energía que utilizamos o el 100% del betún sobre el que viajamos, son rocas carbonosas. Este carbono no estaba presente en la corteza terrestre inicial, sino que se concentraba en la atmósfera. Fue transferido de la atmósfera a la corteza terrestre por los organismos vivos, que lo absorbieron y depositaron en el fondo de los océanos o en las capas geológicas de la tierra y las lagunas. En sólo unas décadas, estamos liberando carbono que los seres vivos tardaron cientos de millones de años en enterrar. Estamos desestabilizando un sistema cuyo equilibrio está en el origen de la mayoría de las formas de vida evolucionadas actuales, incluida la nuestra[^3] [^4].

## Biodiversidad

### Presentación de la importancia de la biodiversidad para la vida en la Tierra

### Consecuencias de la actividad humana sobre la biodiversidad

Más de la mitad de los bosques y humedales del mundo han desaparecido en un siglo [^2].

## El suelo

### Presentación de la importancia de los suelos para la vida en la Tierra

### Consecuencias de las actividades humanas sobre los suelos

Un tercio de los suelos del mundo están degradados. [^4]

## Recursos naturales

### Presentación de los diferentes recursos naturales (agua, aire, suelo, etc.).

### Gestión sostenible de los recursos naturales

### Alternativas sostenibles para los recursos naturales

## Ecosistemas terrestres y marinos

### Presentación de los diferentes ecosistemas terrestres y marinos y su importancia

### El impacto de la actividad humana en los ecosistemas

## Fuentes

[^1]: [Wikipedia : Peters projection](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, La economía simbiótica, Actes sud | Colibri, 2017.
[^3]: [Organización de las Naciones Unidas para la Agricultura y la Alimentación (FAO)](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Organización de las Naciones Unidas para la Agricultura y la Alimentación (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
