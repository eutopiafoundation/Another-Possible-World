---
title: Imaginando otro mundo
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

¿Cómo imaginas un mundo nuevo, un ideal, Eutopía?

## El sistema, sus limitaciones e incentivos

Si actualmente parecemos estar en un callejón sin salida frente al cambio climático y los demás retos de nuestro tiempo, es porque los fundamentos de nuestro sistema generan limitaciones e incentivos que acaban impidiendo cualquier cambio efectivo sin perturbar la coherencia establecida, lo que arrastra entonces a nuestra sociedad a una inercia destructiva que nadie parece capaz de detener.

En cada elección, nuestros políticos intentan convencernos de sus propuestas, y a primera vista todos piensan que están haciendo lo correcto, creen que tienen la solución que podría mejorar la vida cotidiana en general de las personas a las que defienden. Pero están luchando entre ellos con esas limitaciones e incentivos, y eso no puede conducir a una solución duradera y deseable. Así que, impotentes ante el sistema, no les queda más remedio que dejarse gobernar por él.

En la medida en que alguna vez lo tuvimos realmente, los seres humanos hemos perdido nuestro poder, pero lo que es peor, aquellos a quienes elegimos para que nos representen también lo han perdido.

Pensamos que el sistema político domina y que puede cambiar todo lo demás, incluida la economía y su forma. Que estamos en democracia por la competencia entre los candidatos a los que votamos.

Pero un sistema económico no sólo produce bienes y servicios, sino que define las condiciones de nuestra socialidad, produce seres humanos y las relaciones entre ellos. [^1]

Veremos que la economía interviene incluso en esta elección democrática competitiva. Que influye en ella y que, en consecuencia, el sistema electoral ya no tiene la cualidad de ofrecer una buena competencia entre los candidatos. Al final, por tanto, no es la política la que domina a la economía, sino la economía la que domina a la política. [^2]

Lo que proponemos aquí es una limpieza de nuestro sistema, una ruptura total con todo lo que conforma nuestro actual imaginario colectivo. Os invitamos a partir de una nueva base, a descubrir un Nuevo Mundo muy distinto del que conocéis, un mundo basado en el sentido y la lógica, y a elaborar juntos las vías y las soluciones para una transición hacia él.

## Empezar de cero

¿Cuál es la base?

La base es la realidad, es lo que realmente existía al principio de la existencia de la humanidad, cuando aún no podíamos contarnos historias imaginarias. La base es todo lo que queda una vez que hemos borrado esta imaginación humana colectiva.

Así pues, tenemos la Tierra y todo lo que la compone. En particular, sus distintos ecosistemas: un ecosistema es un conjunto formado por una comunidad de seres vivos que interactúan con su entorno, y estos seres vivos pueden ser plantas o animales. El planeta es un gran sistema en el que un número infinito de subsistemas interactúan y se equilibran dinámicamente. Entre todos los seres vivos de nuestro planeta, estamos nosotros, el Homo Sapiens, más comúnmente conocido como Humanos.

Los humanos somos 7.900 millones y se prevé que seremos 11.000 millones en 2100[^3]. Ahora estamos en casi todo el planeta, e incluso en las zonas poco pobladas, los humanos seguimos teniendo un impacto en el medio ambiente y en los ecosistemas[^4]. Sin embargo, el problema no es el tamaño de la población, sino las interacciones que tenemos dentro de sus ecosistemas.

Así que empezar desde la base significa, en primer lugar, que tenemos que entender la base. Tenemos que entender el mundo y cómo funciona. Y como los seres humanos vivimos en él, también tendremos que entender cómo funcionamos. Una parte se dedicará exclusivamente a la tierra y otra a comprender en profundidad al ser humano.

Estas 2 preguntas de investigación son muy amplias y requieren muchos conocimientos, pero no es necesario entrar en todos los detalles. Vamos a concentrarnos en lo esencial, reunirlo y resumirlo para responder a las preguntas que siguen.

## Preguntas de investigación

Una vez adquiridos los conocimientos necesarios para comprender el mundo y a los seres humanos, necesitaremos un objetivo en forma de pregunta de investigación que establezca nuestro deseo de lo que buscamos en un mundo ideal.

Esta pregunta de investigación es muy importante, porque guiará la investigación de la que surgirán nuestras soluciones. Diferentes preguntas de investigación conducirán a diferentes resultados. Las preguntas deben ser lo más sencillas posible, claras y precisas, pero deben captar la esencia de lo que queremos.

Nos centraremos en nuestra especie, pero, como ya hemos dicho, vivimos en ecosistemas, y al estudiar el mundo descubriremos que estamos directamente vinculados a ellos y que, por tanto, dependemos de ellos para sobrevivir.

Buscamos el ideal. Por supuesto, el ideal depende de los valores, creencias, aspiraciones y experiencias de cada individuo. Por lo tanto, podríamos esperar que el ideal para cada individuo sea vivir una vida plena y equilibrada que satisfaga sus necesidades y aspiraciones.

Eso es lo que pretendemos, que cada ser humano tenga una vida plena durante su estancia en la Tierra. Pero recordemos que aún quedan 80 millones de generaciones por venir, por lo que esta investigación se aplica a las generaciones actuales, pero también tiene en cuenta a todas las venideras.

Teniendo en cuenta estos diversos elementos, he aquí la formulación de nuestra pregunta de investigación:

> ¿Cómo podemos conseguir que todos los seres humanos actuales y las 80 millones de generaciones venideras disfruten de una vida plena en la Tierra?
> {.es-éxito}

Nuestro gran proyecto colectivo es, pues, conseguir que todos los seres humanos tengan una vida plena en la Tierra que responda a sus necesidades y aspiraciones, tanto para las generaciones actuales como para los 80 millones más que vendrán.

Cuando comprendamos bien cómo funcionan el mundo y los seres humanos, habremos descubierto cómo pueden realizarse los seres humanos, cómo podemos dar sentido a su existencia y cómo pueden vivir a largo plazo en su entorno en la Tierra.

Para completar esta investigación, recorreremos rápidamente las distintas épocas de la humanidad, porque conocer el pasado proporciona la base y la inspiración para interpretar el presente [^5], y necesitamos conocer el presente para determinar una posible transición futura. Porque la mejor sociedad posible es la mejor evolución posible de las sociedades a partir de ahora. [^1]

Para alcanzar este objetivo común, nuestros 7.700 millones de seres humanos tendrán que llevar a cabo 2 acciones esenciales: colaborar y decidir.

## Sub-pregunta: Colaboración

Como en cualquier sociedad, vamos a tener que colaborar para proporcionar los bienes y servicios que necesitamos para prosperar. Por tanto, nuestra primera gran subpregunta será :

> ¿Cómo podemos hacer que las personas colaboren para proporcionarse mutuamente los bienes y servicios que necesitamos para alcanzar este objetivo común?
> {.es-éxito}

Este es el campo de la economía. Es uno de los grandes problemas de nuestro sistema, y de donde proceden la mayoría de las limitaciones e incentivos. Partir de cero y buscar la mejor economía posible nos permitirá definir las bases necesarias para una sociedad ideal posible, evitando las limitaciones y los incentivos que se oponen a su realización. Como descubriremos, un sistema económico no sólo produce bienes y servicios, sino también seres humanos y las relaciones entre ellos, que son igualmente esenciales para su realización.

## Subpregunta: Decisiones

Por último, nuestros 7.700 millones de seres humanos, a lo largo de su vida y del desarrollo de sus sociedades, tendrán que elegir, llegar a acuerdos y tomar decisiones a pequeña y gran escala. Esta es nuestra segunda gran subpregunta:

> ¿Cómo podemos tomar decisiones juntos y garantizar que estas decisiones nos permitan alcanzar nuestro objetivo común?
> {.es-éxito}

Como se desprende de la pregunta, no se trata sólo de llegar a un acuerdo, porque de qué sirve que toda la población o parte de ella se ponga de acuerdo sobre una decisión, y que ésta sea validada, si al final no nos permite alcanzar nuestro objetivo común.

Este campo es la ciencia de la toma de decisiones, y se aplica a muchas prácticas, incluida la política, donde actualmente utilizamos la democracia representativa.

Profundizaremos en las distintas posibilidades, teniendo en cuenta lo que hemos aprendido sobre el funcionamiento del ser humano y del mundo.

## Conclusión

En resumen, vamos a empezar por entender nuestro hábitat, la Tierra, cómo funciona, sus ecosistemas y nuestro lugar dentro de ella. Después vamos a profundizar en el conocimiento del ser humano, cómo funciona y qué necesita para prosperar durante su estancia en la Tierra.

Este conocimiento nos permitirá responder eficazmente a la cuestión de cómo hacer que los seres humanos tomen decisiones y cooperen para alcanzar el objetivo común del desarrollo sostenible de todos y cada uno de ellos.

El funcionamiento de la Tierra en el capítulo 2, la comprensión del ser humano en el capítulo 3 y el repaso de las eras de la humanidad en el capítulo 4 pueden parecerte aburridos de leer si ya estás familiarizado con estas áreas o si no quieres profundizar demasiado al principio. En ese caso, le aconsejamos que pase directamente a los capítulos 5 y 6, donde descubrirá una nueva forma de trabajar juntos y de tomar decisiones. Estas 2 partes son esenciales porque explican cómo es posible la sociedad que vamos a imaginar, creando las bases que la harán coherente. Sin embargo, los capítulos 2, 3 y 4 no son menos importantes y han llevado a la realización y justificación de las soluciones de los capítulos 5 y 6. Si lo desea, se han elaborado resúmenes y [están disponibles aquí](/es/4-Appendices/1Summaries)

> El conocimiento es poder. Cuanto más comprendamos el mundo y a nosotros mismos, más capacidad tendremos de cambiarlo a mejor. Malala Yousafzai

## Fuentes (Lista no exhaustiva)

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, ¿Son las elecciones democracia?/ 1977
[^3]: [World Population Prospects 2022" publicado por las Naciones Unidas](https://desapublications.un.org/file/989/download)
[^4]: [Informes del IPCC 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Orgullo de Plouc: una nueva narrativa para el campo. 2021
