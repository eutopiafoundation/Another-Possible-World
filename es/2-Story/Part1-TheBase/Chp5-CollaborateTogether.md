---
title: Trabajar juntos
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

## ¿Qué es la economía?

La economía es la actividad humana que consiste en la producción, distribución, intercambio y consumo de bienes y servicios[^2].

Pero un sistema económico no sólo produce bienes y servicios, sino también seres humanos y las relaciones entre ellos. La forma en que la sociedad produce y consume tiene una gran influencia en la personalidad, el carácter, los conocimientos, los deseos, la felicidad y los tipos de relaciones interpersonales.

A veces tendemos a juzgar a la sociedad únicamente por las cantidades de los distintos bienes y servicios de que disponen sus miembros. Pero las personas no sólo valoran estos bienes y servicios, sino también los sentimientos, actitudes y relaciones interpersonales que los acompañan, lo que éstos transmiten sobre lo que es ser humano. Estos aspectos se descuidan con demasiada frecuencia en economía[^3].

Un sistema económico forma y moldea a los seres humanos y sus relaciones a través de sus actividades de producción y consumo. La sociedad segrega el sistema económico, que a su vez influye profundamente en la naturaleza y la calidad de las relaciones entre las personas y su psique.

En un intercambio, el que da y el que recibe siempre reciben más de lo que realmente se dan, porque también se dan la oportunidad de entablar una relación humana.

Nos hemos acostumbrado a seguir la ciencia económica como si fuera un simple hecho de la naturaleza que dicta cómo debe ser el mundo, sin cuestionarla realmente en profundidad. Estamos impregnados del mundo en que vivimos, pero una vez que comprendemos lo que es un sistema económico, nos volvemos capaces de imaginar nuestro mundo funcionando con otras reglas.

Algo nuevo está ocurriendo estos días, porque a la miseria del mundo y a la dominación que tenemos unos sobre otros, se suma el cambio climático, que nos llevará al fin de nuestro crecimiento económico. [^4]

Cada año, los medios de comunicación y las organizaciones de investigación sobre el clima nos dan una fecha. Es la fecha en que los científicos estiman que la humanidad ha consumido más recursos de los que la Tierra puede producir en el mismo periodo.

Esta cifra es el resultado de que expertos en diversos campos hagan balance de los recursos que estamos extrayendo del planeta y del ritmo al que se reponen.

Esta fecha se conoce como el "día del rebasamiento". Para que una economía sea sostenible, esta fecha debería ser el 31 de diciembre y no antes. En este caso, los recursos pueden renovarse al mismo ritmo que se consumen.

En 2022, se calcula que esta fecha será el 22 de julio[^1].

Esta fecha debería ayudarnos a tener una mirada más clara sobre nuestro sistema económico, a tomar conciencia de sus fundamentos humanos y de sus vicios.

En particular, seguir promoviendo un sistema económico que fomenta nuestros grandes reforzadores primarios (= deseo de sexo, comida, información) es probablemente lo peor que podemos hacer[^5].

Las teorías económicas no consiguen presentar argumentos que realmente ganen apoyo intelectual, lo que demuestra su debilidad. Ello se debe a que a menudo se basan en nuestro imaginario colectivo y carecen de fundamento real.

Quizá la economía sea más filosófica de lo que parece y menos científica de lo que parece.

¿Existen otras posibilidades para un sistema económico?

## El plan y el mercado

Actualmente hay 2 sistemas económicos que dominan el mundo moderno, el mercado y la planificación[^6].

Cada bando defiende su sistema con una ética social y ataca al otro por la realización de otra.

Para el mercado este valor es la libertad y para la planificación este valor es la igualdad.

Pero en el mercado estamos muy lejos de ser verdaderamente libres, aunque somos más libres que en el plan, pero estamos peor en lo que se refiere a la igualdad.

Y en el plan estamos muy lejos de ser verdaderamente iguales, pero lo somos más que en el mercado, en cambio somos los peores en lo que se refiere a la libertad.

De hecho, la realidad actual es que cada uno de estos sistemas funciona mal en el ámbito del valor que defienden, pero es cierto, mejor que el otro para el valor defendido e incluso peor según el otro criterio ético.

Estos 2 sistemas económicos en su máxima expresión se han convertido en el mercado capitalista y el comunismo totalitario, ambos engendran y refuerzan rasgos y relaciones humanas nauseabundas y degradantes, como el egoísmo, el tratar a los demás como cosas, la hostilidad, el conflicto, la competencia entre personas, la dominación, la explotación, la alienación...

Ninguno de estos 2 sistemas económicos ha conseguido exaltar las mejores cualidades del hombre. Incluso reprimen fuertemente algunas de ellas. En ambos sistemas existe una cierta forma de dominación.

## La 3ª economía: la reciprocidad

¿Existe otra alternativa?

Existe una literatura que comienza con "Hay tres tipos de sistemas económicos". Pero esta literatura hay que buscarla en los márgenes históricos o disciplinarios del pensamiento económico, bien entre los economistas de hace varias generaciones, bien en la antropología económica.

Es el caso del economista Karl Polanyi (1886-1964), cuyos trabajos clasifican en tres categorías los sistemas económicos de todas las sociedades, incluidas las pasadas, primitivas y tradicionales. Y señala que pueden coexistir en una misma sociedad, aunque cada una asigna un lugar privilegiado/dominante/principal a uno de ellos. Distingue en primer lugar un sistema económico, el mercado. Al segundo sistema lo denomina sistema de redistribución, que en su forma elaborada y moderna es la planificación, en la que un centro político decide la asignación económica. Al tercer sistema, Polanyi lo llama "Reciprocidad", según una tradición antropológica ya bien establecida, y que es incluso bastante clara y precisa. [^6]

Ignoramos la existencia de este tipo de economía, pero existe en el seno de nuestras sociedades. Prácticamente todas las sociedades contienen los tres sistemas económicos, pero en todas ellas uno de los sistemas es más importante que los otros dos.

He aquí un triángulo correspondiente a los distintos sistemas principales en 1984:
[![Diagrama de los 3 tipos de economía](./../../../GlobalProject/Image/Marché.png)]

Los economistas construyeron la economía y su pensamiento sobre el supuesto de que el hombre era y siempre ha sido lo que llamaríamos "homo oeconomicus", y crearon una caricatura de él como ser egoísta, calculador y asocial. Pero el ser humano era ante todo lo que llamaríamos "homo sociabilis", universalmente practicante de la reciprocidad y universalmente necesitado de un círculo de allegados o de una comunidad[^9].

La reciprocidad es el mecanismo de la economía comunitaria que alimentó los inicios de la humanidad, como cazadores-recolectores y luego como agricultores sedentarios.

La reciprocidad sigue entre nosotros, y la vida económica en el seno de la familia se basa principalmente en esta economía. Se ofrecen servicios unos a otros sin esperar nada a cambio, por ejemplo, no se paga a un miembro de la familia para que te recoja del colegio, ni esa persona se ve obligada a hacerlo por una entidad externa. La reciprocidad en nuestras sociedades ha disminuido drásticamente en las últimas décadas, como consecuencia del mercado, que busca monetizar cada vez más las interacciones sociales, precisamente para crear nuevos mercados. Sin embargo, aparte de la familia, todavía podemos encontrar mucha reciprocidad en las relaciones amistosas/afectivas/amorosas, a través del voluntariado, las comunidades, pero también en diversos campos, como el mundo de la informática con el opensource y a través de numerosas plataformas para compartir información, logros, objetos, etc.

Una larga serie de regalos recíprocos, regulares, continuos y establecidos entre dos personas es lo que llamamos reciprocidad. La reciprocidad general es el equivalente, pero de todos. Todo el mundo da a la sociedad, y la sociedad recibe de todo el mundo.

El altruismo y la donación, por su propia naturaleza, no pueden obtenerse ni por coacción ni por intercambio en sentido estricto.

Esta es la diferencia con el plan: estas transferencias no son coaccionadas e impuestas al dador por una entidad social ajena a él. Y la diferencia con el mercado es que tampoco son intercambios en sentido estricto, en los que cada transactor está exclusivamente interesado en tener más, o no tener menos, de los bienes o servicios que transfiere.

En este sistema social, nadie da a otro específicamente, por lo que es imposible dominar, coaccionar, humillar o forzar una contra-donación, y explotar económica o éticamente por este medio. Quien da a todos no da a nadie.

En este sistema, el trabajo de cada persona beneficia el consumo de un gran número de otras, a menudo mucho más tarde. Y todo el mundo se beneficia del trabajo de muchos otros, a menudo mucho antes. Cada persona recibe incluso parte del trabajo de generaciones que han desaparecido y trabaja para generaciones que aún no han nacido.

El valor ético social de la reciprocidad podría denominarse "fraternidad". Aquí tenemos la famosa trilogía republicana: la fraternidad se define como "No hagas a los demás lo que no te gustaría que te hicieran a ti; haz siempre a los demás el bien que te gustaría recibir de ellos".

Pero la comparación con la trilogía termina ahí, porque la reciprocidad borra el deseo y el sentido mismo de los otros dos valores. Porque en este sistema, en el que todos se preocupan por los demás como si se preocuparan por sí mismos, las voluntades, y por tanto las libertades personales, ya no chocan, y las exigencias de igualdad dejan de tener fundamento.

## Ventajas de la reciprocidad

La reciprocidad se basa en la bondad.

La bondad significa hacer hincapié en el altruismo, el amor, la solidaridad voluntaria, la donación recíproca, la generosidad, el compartir, la libre comunidad, el amor al prójimo y la caridad, la benevolencia y la amistad, la simpatía y la compasión. Todos carecemos de ellos y sentimos que necesitamos más. Todas nuestras grandes familias de espíritu, pensamiento y sentimiento los reclaman. Pero se nos niegan en nuestra vida económica. Dominan la existencia de nuestras sociedades, pero podemos ver que la más extendida de las aspiraciones es el más raro de los logros.

La reciprocidad general es la conciliación entre nuestra ética altruista y nuestra técnica científica, entre nuestras aspiraciones a una comunidad cálida, a la libertad individual y al progreso material; permite la duración de los significados culturales y las bellezas del mundo en la expansión de nuestros conocimientos, capacidades y conciencias.

Además, la reciprocidad aporta muchas ventajas a la producción económica. Porque ofrece un sistema coherente y eficaz que reducirá drásticamente el tiempo total de trabajo.

## ¿Es posible la reciprocidad?

Aunque hemos estudiado ampliamente los mercados y la planificación, no hemos dicho casi nada sobre el tercer modo de transferir bienes y servicios, es decir, los regalos, y nada en absoluto sobre el grupo de regalos conocido como reciprocidad.

A lo largo de la historia han aparecido nuevos sistemas económicos y cada vez ha habido personas, quizá incluso la mayoría, que piensan y afirman que los sistemas posibles son los que se pueden observar en ese momento o que han existido en el pasado. Pero estas creencias son constantemente desmentidas por la historia, por la existencia de nuevas creencias. Y ciertamente esto sigue siendo así hoy en día, porque no hay pruebas de que ninguno de los sistemas que existen o han existido sea el mejor posible. De hecho, sí, son posibles sistemas económicos que nunca se han observado.

El defecto de nuestro sistema es que impide que los individuos elijan individualmente dar: una empresa que ofrece precios demasiado favorables quiebra; un individuo no puede dar lo que produce en una sociedad en la que nadie le da nada.

Los psicosociólogos llevan mucho tiempo estudiando el "comportamiento de ayuda", con resultados muy interesantes: no sólo tendemos a ayudar a quienes nos han ayudado, sino que también tendemos a ayudar más si nosotros mismos hemos sido ayudados, es decir, tendemos a ayudar a extraños si nosotros mismos hemos sido ayudados por extraños. Esa es la esencia de la reciprocidad. [^13]

Y esto es lo que vimos en el capítulo 3 sobre el Homo Sapiens, que fue posible crear seres altruistas, porque el altruismo también está motivado por el placer, y que son principalmente el entorno y la educación los que contribuyen a desarrollar valores y motivaciones como la generosidad, la compasión y el deseo de servir a los demás.

Pero, ¿es posible que una sociedad tenga una economía basada en la reciprocidad y, en particular, en la reciprocidad general?

Hay sociedades enteras en las que todas las relaciones económicas se basan esencialmente o incluso exclusivamente en la reciprocidad. Pero estas subsociedades o sociedades son lo que llamamos "grupos cara a cara", cuyos miembros se conocen, familias, pueblos, bandas, etc., y la reciprocidad no es esencialmente general. Todavía era fácil aplicarla aquí, pero una persona no puede conocer a más de otras cien, por lo que podemos preguntarnos legítimamente si es realmente posible a gran escala[^14].

Pues aunque la economía resuelve el problema de la producción y distribución de bienes y servicios, cualquier sistema económico (incluida la reciprocidad) se enfrenta a dos problemas fundamentales para la producción: el problema de la información y el problema de la motivación.

## El problema de la información

La información es el conocimiento que tiene el trabajador o el proveedor de recursos o servicios de lo que tiene que hacer para satisfacer sus necesidades y deseos de la mejor manera posible.

Y la motivación es el incentivo para realizar el trabajo o utilizar los recursos de esta manera.

El plan y el mercado resuelven más o menos bien estos dos problemas.

El mercado resuelve el problema de la información mediante los mecanismos descentralizados del sistema de precios, los "indicadores de escasez y utilidad": la oferta y la demanda.

La planificación resuelve el problema organizando esta información, es decir, recopilando, calculando y emitiendo instrucciones o previsiones, de forma relativamente centralizada.

El mercado resuelve el problema de la motivación reduciendo el egoísmo mediante el incentivo del intercambio.

El plan resuelve el problema mediante la coacción, la amenaza o el castigo, la recompensa o la promoción, pero más generalmente mediante la autoridad y sus diversos medios de coacción o recompensa, y la interiorización más o menos fuerte de la necesidad de obedecer por costumbre o por sentido del deber.

Los mismos problemas se aplican a la reciprocidad: ¿cómo sabrá la gente lo que los demás necesitan de ella y querrá dar lo suficiente a los demás?

Estos dos problemas se refuerzan mutuamente en el sentido de que resolver uno ayuda al otro y viceversa.

Cuanto mejor se resuelva el problema de la motivación, mejor podrá resolverse el problema de la información, porque entonces las personas podrán transmitirse más fácilmente la información necesaria.

Cuanto más se resuelva el problema de la información, más productivo será el sistema económico y, por tanto, menos despilfarro habrá, menos trabajo se necesitará para un determinado servicio final y, por tanto, menos necesidad habrá de que el sistema de motivación proporcione trabajo, o menos grave será su imperfección.

Recíprocamente, cada cual considera las necesidades o deseos de los demás tan importantes como los suyos propios, y esto determina lo que da de su trabajo (tiempo, esfuerzo, molestias) y de sus posesiones, sea cual sea su origen.

Si el problema de la motivación está resuelto, también lo está el de la información, porque las personas siempre pueden hacer que la economía sea al menos tan eficaz como con el mercado o el plan, transmitiéndose voluntariamente la información económica que circula en estos sistemas.

La tecnología de la información ha revolucionado verdaderamente este ámbito, ahora que todos estamos conectados y, gracias al desarrollo de aplicaciones adecuadas, la transmisión de información es más fácil que nunca.

Transmitir y procesar información forma parte del trabajo de la sociedad, junto con otras tareas. A cambio, la motivación nos impulsa a hacer lo mejor para la comunidad. Por tanto, este sistema puede hacer todo lo que el mercado o el plan.

Además, la reciprocidad reduce considerablemente la cantidad de información necesaria. Mediante la reducción del consumo competitivo o conspicuo, la reducción de las necesidades extrínsecas y la reducción de la producción de los bienes o servicios correspondientes, la reducción de la necesidad de renovar los productos debido a su diseño basado en la sostenibilidad y la reducción de muchos servicios que ya no serán necesarios para el funcionamiento de la economía.

## El problema de la motivación

Cuando hablamos de "naturaleza humana" podemos oír "el hombre nunca cambiará, siempre ha sido egoísta, etc." y luego vienen las ideas sobre la "verdadera" naturaleza humana que deberían prevalecer en última instancia. Los conocimientos históricos, antropológicos y psicológicos, el análisis, e incluso la simple lógica, deben contraponerse absolutamente a estas convicciones ingenuas, y la respuesta a "el hombre nunca cambiará" bien podría ser que sólo ha cambiado[^15][^16][^17].

A "el hombre siempre ha sido y es egoísta en todas partes" podemos replicar con la misma facilidad preguntándonos qué sabemos acerca de lo que "siempre ha sido".

La humanidad está embarcada en una transformación sin precedentes en su existencia por su profundidad y, por supuesto, por su escala.

Como hemos visto, la naturaleza humana no existe, y los seres humanos pueden convertirse en lo que decidan convertirse.

He aquí una serie de puntos que responden a la cuestión de la motivación:

- Las personas tienen preferencia por las relaciones altruistas y generosas[^18].
- Todas las grandes éticas religiosas y laicas abogan firmemente por el altruismo y la generosidad[^20]. [^20]
- La principal motivación humana es recibir la aprobación de otros seres humanos: en un sistema recíproco, la aprobación es naturalmente universal y recíproca[^19].
- Los análisis empíricos del "comportamiento de ayuda" demuestran que es mucho más probable que las personas ayuden a otras si ellas mismas han recibido ayuda.
- Los sociólogos han defendido la idea de que la principal característica del ser humano es la imitación[^22].
- El hábito, esa imitación de uno mismo, es otro fenómeno humano de primera importancia[^23].
- Los estudios sobre el desarrollo de los niños demuestran que inicialmente somos altruistas, luego la sociedad nos hace egoístas[^21].
- Educación: como hemos visto, el ser humano no es más que una página en la que escribe la sociedad[^24].
- Refuerzo social: cuanto más espíritu de reciprocidad tienen las personas, más demuestran con sus actos y expresan con sus palabras que es normal y bueno tenerlo, y esto anima a los demás en esta dirección. Así pues, la educación y las relaciones sociales en su conjunto desempeñan un papel fundamental. Si el hombre es ante todo cultural, y por tanto maleable, la sociedad puede hacer de él tanto la persona de la reciprocidad como el egoísta del mercado, o el sumiso o el jerarca del plan[^25].
- Cuanta más reciprocidad haya, más veremos que es viable y más nos daremos cuenta de sus ventajas, y por tanto más la aceptaremos, más "creeremos en ella", más la querremos, más la impulsaremos. Y así, más tiende a extenderse.

- En el caso de que algunas personas capacitadas para ello no deseen participar, no es grave, porque si se dieran esos casos, no se les despreciaría, ya que tal sentimiento significaría una falta de bienestar en la sociedad: Pero incluso si la persona no desea participar, no es un asunto grave, porque no sólo descubriremos que la carga de trabajo necesaria por habitante se reducirá considerablemente, sino que además esto puede verse contrarrestado por el hecho de que otras personas estarán encantadas de dar "más" de lo que reciben, por altruismo, como hemos visto, pero también por su propio interés en el trabajo.

Al fin y al cabo, en la sociedad actual, ¿para qué trabajar?

- Coacción directa: trabajo forzado
- Intercambio: vender el propio trabajo a cambio de un salario
- Por el deseo de ayudar a los demás o a la comunidad, para "estar al servicio".
- Por sentido del deber, posiblemente para devolver a los demás lo que recibimos de ellos.
- Por interés en el propio trabajo.
- Para encontrar contactos sociales, relaciones, integración en la sociedad, contribuyendo al entorno laboral.
- Para demostrar a los demás que trabajas para ellos, que haces cosas bonitas o que eres hábil.
- Por el estatus social que conlleva el trabajo (contribución, responsabilidad, etc.).
- Para mantenerse ocupado, para tener "algo que hacer", para evitar aburrirse o estar ocioso.
- Para jugar
- Como hábito

Podríamos añadir otros, pero aquí se ve claramente la importancia de la ocupación en sí y de las relaciones sociales que el trabajo nos permite crear, como puede verse en los sentimientos que siguen a la "jubilación". Por regla general, las personas obtienen más significado de su actividad productiva que de su actividad consuntiva. Y todas estas motivaciones, a excepción de las dos primeras, pueden incitar a la gente a trabajar en un sistema de reciprocidad, por lo que sólo hay que jugar con ellas aumentando el sentido y el interés del trabajo.

El trabajo no está condenado a ser la tortura de su discutido origen etimológico, de la palabra travalhum: tripalium, instrumento de tortura con trípode...[^26].

Por tanto, hay que borrar o reducir todas las dimensiones de la monotonía de la tarea. El trabajo física o psicológicamente agotador, embrutecedor o anestesiante, demasiado duro, mecánico, monótono, demasiado sucio o desagradable, demasiado carente de sentido, despreciable, etc., debe suprimirse, sustituirse o hacerse menos agotador. Debe eliminarse, sustituirse, restringirse, automatizarse, mejorarse, enriquecerse, compensarse, recompensarse, valorarse o compartirse más ampliamente.

La idea misma es transformar la naturaleza del trabajo para que se convierta en una actividad que sea algo así como la autorrealización, la manifestación de la propia humanidad profunda a través de la transformación de la naturaleza y su preservación. De este modo, el trabajo se convertiría en la "primera necesidad de la vida". Así, la expresión "trabajar para vivir" ya no significaría sufrir el dolor del trabajo para sobrevivir y mantenerse, sino ganarse realmente la vida a través de una actividad rica en significados y experiencias.

## El lugar del dinero en la reciprocidad

Estamos tan acostumbrados a él que todos creemos que el dinero es una condición necesaria para la vida en sociedad. Sin embargo, el dinero no es un hecho de la naturaleza, sino una invención imaginaria de los seres humanos que puede ser criticada[^30].

El funcionamiento del dinero no explica su origen, no se explica a sí mismo, y por eso los economistas no explican nada. Tampoco se explican por razones existenciales.

Se supone que el dinero se inventó para resolver el problema de la doble coincidencia de necesidades que planteaba el trueque, porque sin dinero sería demasiado difícil completar los intercambios. ¿Cómo se comparan los bienes, y si yo necesito tus zapatos pero tú no necesitas mis manzanas?

A pesar de las numerosas investigaciones antropológicas en sentido contrario, este mito es uno de los más arraigados en la percepción que la gente tiene de la economía.

En las economías primitivas, la gente simplemente cogía lo que necesitaba y, desde un punto de vista social, compartía sus recursos. Ignorando la obsesión por la escasez que caracteriza a nuestras economías de mercado, las economías primitivas podían centrarse sistemáticamente en la abundancia[^7][^8].

Algunas investigaciones antropológicas basadas más estrechamente en elementos históricos y descubrimientos empíricos nos llevan a pensar que el dinero no procede ni del trabajo ni del intercambio, sino que es ante todo, histórica y conceptualmente, una forma de crear un vínculo social entre los individuos, ya que representa la medida de las deudas contraídas entre ellos[^12].

Algunas civilizaciones, como los incas, no conocían ni el dinero, ni el mercado, ni el comercio. [^10]

El problema del dinero es que, mientras sea necesario disponer de él para acceder al intercambio de bienes y servicios, el capital puede jugar con nosotros.

Por tanto, no es teniendo la mano sobre el trabajo como el capitalismo asegura su dominación, sino dominando las condiciones de socialidad que hacen necesario el trabajo y que incluyen, entre otras cosas, la necesidad del dinero[^3].

El dinero es la marca de la confianza entre los hombres. Se necesita una confianza inquebrantable para aceptar un signo monetario de otra persona a cambio de un servicio prestado o de un bien suministrado: confianza en el hecho de que no se trata de un billete falso o de dinero falsificado, confianza en el futuro que este signo monetario representa en la medida en que hace posibles los proyectos futuros. En definitiva, el dinero es el horizonte de la reciprocidad en el tiempo, no necesariamente hacia la persona que me ha dado el dinero, sino la confianza en el hecho de que en el futuro habrá alguien que podrá devolverme en forma de bienes y servicios el valor de los bienes y servicios que vendí temporalmente a cambio de dinero.

Porque ¿para qué utilizar un signo monetario si realmente confío en la otra persona? El propio objeto, el propio concepto de dinero, refleja en realidad una falta de confianza en la otra persona, mientras que si esa confianza se establece su utilidad desaparece.

Si la otra persona confía realmente en mí y en la sociedad, puede darme algo sin pedirme dinero a cambio, porque sabe que mañana seré yo u otra persona quien le dé algo.

La relación de intercambio, mediada por el dinero, es una relación basada en la idea de que los participantes no pueden confiar en la duración de su relación: utilizan el dinero para completar el intercambio, para saldar la deuda, en definitiva para no tener nada más que ver el uno con el otro una vez finalizada la transacción.

El debate político actual en nuestras economías gira en torno a cómo utilizar este dinero, para que funcione correctamente, pero ¿quizás el dinero simplemente no puede funcionar correctamente, quizás está destinado a pervertir las relaciones sociales?

Sin embargo, si tuviéramos que darle una naturaleza, sería mejor decir que el dinero está destinado a pervertir las relaciones sociales en su forma actual como medio de intercambio, más que como "facilitador de la circularización de bienes y servicios".

El dinero se engulle en todos los vacíos dejados por la ausencia de sentido, nos permite mantener el control para calmar nuestro córtex cingulado, y el miedo a la muerte se convierte en el motor de la avaricia[^11].

Muchos análisis señalan los efectos negativos de la influencia del dinero en la sociedad, pero no proponen una respuesta adecuada al problema de la monetización de la realidad.

En una sociedad monetizada, el valor de las cosas pasa a ser el valor que les da el dinero.

Abolir el dinero parece imposible, tan natural se nos ha hecho la idea del dinero. La monetización de la sociedad parece cada vez más un hecho fuera del cual ni siquiera es posible pensar.

El problema es que ya no es el individuo el que determina su sistema de valores y, por tanto, a qué aspira, sino que este sistema de valores es objetivado por el dinero.

Sin embargo, al distanciarnos del dinero, podemos recuperar la sensación de libertad frente a los valores del mercado, reivindicando nuestros propios valores.

Pero si seguimos pensando que el dinero en su forma actual es la única manera de resolver la cuestión del intercambio de bienes y, por tanto, la cuestión de los encuentros entre individuos, en otras palabras, si pensamos que el dinero es un requisito previo de la socialidad, nos resultará muy difícil distinguir el valor económico de los valores éticos y políticos.

Son nuestras sociedades occidentales las que, muy recientemente, han convertido al ser humano en un "animal económico". Pero aún no todos somos animales económicos. El Homo oeconomicus no está detrás de nosotros, está delante; como el hombre de la moral y del deber; como el hombre de la ciencia y de la razón. Durante mucho tiempo, el hombre fue otra cosa; y no hace mucho que se convirtió en una complicada máquina dotada de una máquina calculadora.

Tratamos aquí de dar valor a otras facetas de la humanidad distintas de la relación económica de intercambio de bienes.

Un valor que permita a cada uno florecer en un encuentro permanente con los demás dentro de una socialidad basada en los principios del compartir y de la ayuda mutua.

> "Algunos hombres ven las cosas como son y se preguntan '¿Por qué? Yo sueño con cosas que nunca fueron y me pregunto '¿Por qué no? George Benard Shaw

## Comparación con las ideologías actuales

Esta presentación conduce inevitablemente a la creación de una nueva ideología. Creemos que es esencial establecer paralelismos con las ideologías existentes y explicar por qué son diferentes, y por qué esas otras ideologías no nos parecen sostenibles ni eficaces.

Pero no se trata de rechazar esas ideologías, porque los valores que hay detrás de la izquierda, la igualdad, y de la derecha, la libertad, son hermosos y necesarios para una Eutopía, pero ahora se trata de reunir lo bueno. Para que podamos unirnos.

Para que no nos limitemos a decir "Todos estáis equivocados", sino "Todos tenéis parte de razón".

A partir de ahora, no trataremos de entrar en confrontación con movimientos preexistentes. Al contrario, preferimos mantener relaciones amistosas, crear apertura y apoyar a otros en proyectos que estén en línea con los valores que defendemos. La idea es respetar a los demás por lo que creen, para que ellos también puedan respetarnos por lo que creemos. Creemos que las verdades acabarán saliendo por sí solas, y que no debemos ser perturbados en nuestras reflexiones, a riesgo de cerrarnos a los demás. Creemos que sólo así podremos salir del abismo en el que nos encontramos, permaneciendo juntos a pesar de nuestras opiniones diversas, unidos en la búsqueda común de la plenitud.

### Comparación con la izquierda

Los sistemas ideológicos que abogan por una mayor planificación, o incluso por la planificación total, son los que más podrían parecerse al de nuestra Eutopía, pero bastan unas pocas palabras para cambiar por completo el sistema práctico que emerge.

Socialismo = "De cada uno según sus capacidades y a cada uno según su trabajo".
Comunismo = "De cada uno según sus capacidades y a cada uno según sus necesidades"[^27].
Reciprocidad = "De cada uno, voluntariamente, según las necesidades de los demás"[^3].

La insuficiencia y las carencias de estas otras ideologías son en parte las causas esenciales de lo que es el mundo hoy. Han dado lugar a desviaciones, a revoluciones anticapitalistas, pero con ellas han fracasado más o menos en la creación de sociedades de nuevo tipo en gran parte del globo. Y como el resultado no es obviamente mejor que el capitalismo desarrollado, lo han protegido. Ahora son utilizados por todos los bandos para constituir el engaño y la hipocresía más gigantescos de la historia.

La igualdad entre las personas, como quiera que se defina, puede ser un límite a la libertad de algunas personas. Y la libertad por sí sola no suele garantizar la igualdad.

La mejor de las sociedades es probablemente muy diferente de las que conocemos, como nuestra sociedad de clases sociales dominantes y dominadas. Pero quienes han intentado reinventarla han fracasado, en primer lugar porque no especifican suficientemente las sociedades que proponen.

Si este enfoque analítico era necesario, ahora debemos ir más allá si queremos continuar la obra de desnaturalización genética emprendida, en particular, por el célebre Karl Marx [^28].

Además, el sistema marxiano no puede pensar el dinero ni antes ni después del capitalismo. Además de arrojar a las sombras dos mil años de historia humana, no puede proponer ninguna salida.

Las dificultades del pensamiento de Marx residen en la posibilidad de pensar una forma de organización del trabajo y de la socialidad económica distinta de la del modo de producción capitalista.

Así, en la era de la noosfera, los movimientos marxistas que arrastran consigo teorías de hace 150 años tienen grandes dificultades para reimaginar organizaciones de trabajo horizontales autogestionadas, para reinventar nuestros procesos democráticos, para imaginar un mundo distinto al del productivismo, para comprender el ser profundo que somos y los medios para transformarlo. Con demasiada frecuencia, luchan CONTRA el capitalismo, y les cuesta encontrar soluciones alternativas genuinas por las que podamos luchar.

No criticamos aquí la magistral obra de Marx, muy necesaria para la reflexión, pero sí pensamos que si hubiera admitido sinceramente que lo suyo también era la ética y hubiera puesto mucho más de ella en sus obras, si hubiera hablado más de la sociedad final o buena y de los medios para conseguirla, si hubiera puesto en sus características tanto altruismo como en sí mismo, quizá hubiera pasado menos de lo peor[^3].

### Comparación con el derecho

Las políticas de derechas, es decir, las que promueven el mercado, o incluso el libre mercado total, son el movimiento ideológico dominante en los países occidentales, al menos el que está en el poder, e incluso es el movimiento ideológico dominante en el mundo, y el que tiene mayor impacto.

Todo el mundo puede ver los efectos negativos, no sólo sobre el planeta y nuestros ecosistemas, sino también sobre nuestro bienestar general. En resumen, en este sistema económico, nos pasamos el tiempo malgastándolo, y malgastamos nuestras vidas "ganándolas". Y los que no pierden el tiempo perdiéndolo, lo hagan o no, siguen cosechando las desastrosas consecuencias, y si no a corto plazo, sí a largo plazo.

En el mercado, la gente siempre tiene cadenas. Pero, en su mayoría, ya no son toscas cadenas de acero oxidado; están "finamente niqueladas", son mucho más "resistentes" y, en definitiva, aún más difíciles de romper, sobre todo porque algunas personas introducen en ellas eslabones de oro. Entonces nos sentimos como si tuviéramos algo precioso que perder.

Nuestro sistema atrapa a cada individuo por todos los demás, en el sentido de que una persona simplemente no puede, sola y libremente, manifestar sus mejores cualidades. En economía, en un sistema competitivo, una empresa no puede, so pena de quiebra, no tratar de maximizar su beneficio si los demás tratan de maximizar el suyo. Del mismo modo, una persona que da no puede sobrevivir en una sociedad en la que otros toman.

Hay cosas buenas en este sistema, como la libertad de empresa y la "libertad" general que impera, y no se puede negar la eficacia que ha tenido en muchos avances. Pero tampoco podemos negar las consecuencias que ha tenido, porque donde unos se encontraron, otros no... los que fueron desatendidos, explotados, olvidados.... Y ya es hora de seguir adelante, e incluir por fin a todos.

Aquí termina esta primera versión de nuestro análisis de la comparación con la Derecha. No es que no haya mucho que decir, al contrario, hay una enorme cantidad de literatura sobre el capitalismo que detalla extensamente sus orígenes, causas y consecuencias. Pero la forma en que queremos ver esas ideologías, las relaciones que queremos mantener con ellas y, por último, todas las cuestiones vinculadas al compromiso de nuestro movimiento con una política concreta, aún están por definir colectivamente. No obstante, si crees que no sabes lo suficiente al respecto, te sugerimos que consultes los numerosos recursos disponibles en la literatura y en Internet:[^29][^31].

## Fuentes (lista no exhaustiva)

[^1]: El Día del Sobregiro de la Tierra 2017 cayó el 2 de agosto, Día del Sobregiro de la Tierra. https://overshootday.org
[^2]: [Wikipedia : Economía](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: El fin del crecimiento. Richard Heinberg. 2013
[^5]: Sébastien Bohler. El insecto humano. 2020
[^6]: Karl Polanyi. "La gran transformación" y "La economía como proceso instituido" en Comercio y mercado en los primeros imperios: economías en la historia y la teoría.
[^7]: Marshall Sahlins - Economía de la Edad de Piedra
[^8]: Marcel Mauss- Ensayo sobre el Don
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Organización social de los incas](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. ¿Dónde está el sentido? 2020
[^12]: David Graeber. Deuda: 5000 años de historia. 2011
[^13]: Lee Alan Dugatkin. La ecuación del altruismo: siete científicos buscan los orígenes de la bondad. 2006
[^14]: [Número de Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Breve historia de la humanidad
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[^17]: Daniel Kahneman. Pensar, rápido y despacio
[^18]: [Ernst Fehr, La naturaleza del altruismo humano](https://www.nature.com/articles/nature02043)
[^19]: [La necesidad de aprobación social](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religión y moral](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., El altruismo en los niños y la conducta percibida de los demás en Journal of Abnormal and social Psychology, 1942, 37, pp. 2018-233
[^22]: Gabriel Tarde. Las leyes de la imitación, 1890
[^23]: Charles Duhigg. El poder del hábito. 2014
[^24]: Émile Durkheim. Educación y Sociología; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORY OF NORMATIVE CONDUCT: A THEORETICAL REFINEMENT AND REEVALUATION OF THE ROLE OF NORMS IN HUMAN BEHAVIOR
[^26]: [¿Es la etimología de la palabra trabajo tripalium?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Crítica del Programa de Gotha (1875). 1891
[^28]: Karl Marx. El Capital. 1867
[^29]: [Conferencia: Comunismo y Capitalismo: la historia detrás de estas ideologías](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. El orden del dinero - Crítica de la economía. 2019
[^31]: [Documental Arte 2014 - Ilan Ziv - Capitalismo](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
