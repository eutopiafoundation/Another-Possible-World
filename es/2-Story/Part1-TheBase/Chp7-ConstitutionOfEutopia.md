---
title: Constitución de Eutopía
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

> Contenido en fase experimental y en proceso de redacción.
> {.is-danger}

La constitución de Eutopia se divide en 3 enunciados:

- Declaración Universal de los Derechos Humanos, que incorpora muchos de los principios fundamentales de la actual Declaración Universal de los Derechos Humanos[^1], pero que hemos adaptado para añadir importantes matices y ampliaciones que reflejen mejor nuestros valores como la sostenibilidad, la diversidad, la interdependencia con la naturaleza, la libertad, la solidaridad y la equidad.
- Declaración Universal de los Derechos de la Naturaleza / Madre Tierra, es una carta que fue establecida por iniciativa de los pueblos amerindios y formulada en la Conferencia Mundial de los Pueblos contra el Cambio Climático en 2010[^2].
- Declaración Universal de los Derechos Humanos en Relación con las Especies Animales No Humanas, es una carta provisional y experimental, cuyo objetivo es establecer con mayor precisión nuestras relaciones con otras especies animales vivas.

Para no hacerlas más farragosas, las justificaciones de los distintos artículos se han separado y colocado al final de esta misma página. Estas declaraciones son libres de ser modificadas por los ciudadanos de Eutopía mediante el mismo proceso democrático descrito al final del capítulo anterior sobre cómo decidir juntos.

## Declaración Universal de los Derechos Humanos

### Preámbulo

Guiados por los valores de igualdad, libertad, solidaridad y sostenibilidad, reconocemos la dignidad inherente a cada individuo, faro que ilumina el camino hacia un mundo en el que todos nazcamos iguales en derechos y libertades, y hacia nuestro destino colectivo.

Celebramos la infinita diversidad que teje la riqueza de nuestro tapiz humano, y nos une la profunda convicción de que en esta diversidad reside nuestra fuerza colectiva. En este espíritu de unidad y armonía enunciamos esta Declaración, afirmando que toda persona tiene derecho a expresar libremente su pensamiento, perseguir su felicidad y contribuir a nuestra prosperidad común.

Nuestra búsqueda común está marcada por el respeto absoluto a la integridad de los demás, por la búsqueda incesante de la equidad y la justicia, y por un compromiso sagrado con la preservación de nuestro hogar común, la Madre Tierra.

Al poner por escrito estos derechos universales, estamos erigiendo un poderoso escudo contra la opresión, la injusticia y el olvido. Declaramos que en cada rincón de Eutopía, la luz de la igualdad ilumina los oscuros rincones de la injusticia, estableciendo un mundo donde la libertad, el amor y la compasión son derechos inalienables.

Que esta Declaración Universal de los Derechos Humanos inspire la acción, guíe nuestras decisiones y una nuestros corazones en la construcción perpetua de una sociedad en la que todos, de la mano, marchemos hacia el amanecer de un mundo en el que la dignidad humana sea la brújula que guíe nuestro viaje.

### Propuestas y resúmenes

Artículo 1. Igualdad Igualdad :

- Los seres humanos nacen y permanecen libres e iguales en derechos.
- Cada ciudadano de Eutopía tiene el derecho inalienable a participar en el proceso de toma de decisiones en igualdad de condiciones con todos los demás. Ninguna forma de discriminación o privilegio puede impedir el igual poder de decisión de cada individuo.

Artículo 2. Libertad Libertad :

- Los seres humanos tienen plena libertad para expresarse y pensar; nadie puede impedir la diversidad de opiniones y la libre circulación de ideas.
- La libertad de creencias también es sagrada. Toda persona tiene derecho a elegir, practicar y expresar sus creencias espirituales, religiosas o filosóficas sin temor a ser perseguida o discriminada.
- Los seres humanos tienen derecho a elegir libremente su vestimenta, a expresarse personalmente a través de la ropa, sin estar sujetos a juicios o restricciones impuestas por terceros.
- Los seres humanos tienen el derecho fundamental a circular libremente por todo el mundo, salvo por razones de preservación de la Naturaleza o por razones de seguridad de la integridad humana o material, ninguna frontera puede limitar esta libertad de circulación.
- Cada individuo es libre de emprender, de realizarse, de crear y de aportar al mundo sus creaciones e innovaciones. Debe fomentarse la creatividad, la innovación y la contribución individual al bienestar común de Eutopía.

Artículo 3. Solidaridad (Fraternidad) :

- La solidaridad debe guiar las decisiones tomadas por los miembros de Eutopía.

Artículo 3. Equidad :

- Toda decisión debe ser concebida para que todos puedan beneficiarse plenamente de ella, eliminando barreras y desigualdades.

Artículo 4. Sostenibilidad:

- La preservación del medio ambiente es primordial y una responsabilidad colectiva. Las decisiones deben tener en cuenta el impacto sobre la naturaleza y garantizar un equilibrio sostenible entre la humanidad y el planeta.

Artículo 5. Integridad :

- En ningún caso una decisión puede comprometer la integridad de otra persona, ni física ni mentalmente. Sin embargo, cada individuo sigue siendo soberano y es el único titular de la facultad de comprometer su propia integridad.

Artículo 7. Seguridad Seguridad :

- Cada individuo tiene derecho a la seguridad, a la protección contra cualquier violencia física o mental y a un entorno seguro en el que desarrollarse.
- No se podrá poseer ni fabricar ningún arma letal fuera de los centros y personas de investigación autorizados.

Artículo 8. Confianza Confianza :

- La socialidad se basa en la confianza colectiva, la ayuda mutua altruista y el reconocimiento de las necesidades de los demás. Ningún servicio prestado puede ser pedido al destinatario a cambio de dinero, ni puede ser medido y almacenado en un valor imaginario con vistas a un intercambio futuro por el mismo u otro servicio. Todo el que se da a todos no se da a nadie en particular.

Artículo 9. Diversidad Diversidad :

- Eutopia celebra la diversidad en todas sus formas y se compromete a crear una comunidad inclusiva, donde cada individuo sea respetado y valorado por su singularidad.

## Declaración Universal de los Derechos de la Naturaleza / Madre Tierra

### Preámbulo

Nosotros, los Eutópicos de la Tierra :

- Considerando que todos somos parte de la Madre Tierra, una comunidad indivisible de vida compuesta por seres interdependientes íntimamente ligados por un destino común;
- Reconociendo con gratitud que la Madre Tierra es fuente de vida, sustento y educación, y que nos proporciona todo lo que necesitamos para vivir bien;
- Convencidos de que, en una comunidad de vida que implica relaciones interdependientes, es imposible conceder derechos únicamente a los seres humanos sin causar desequilibrios en la Madre Tierra;
- Afirmando que para garantizar los derechos humanos es necesario reconocer y defender los derechos de la Madre Tierra y de todos los seres vivos que viven en ella, y que existen culturas, prácticas y leyes que reconocen y defienden estos derechos;
- Proclaman la presente Declaración Universal de los Derechos de la Madre Tierra, para que cada persona y cada institución asuma la responsabilidad de promover, a través de la enseñanza, la educación y el despertar de las conciencias, el respeto a los derechos reconocidos en la Declaración, y para que, a través de medidas y disposiciones diligentes y progresivas a escala local y global, sean universal y efectivamente reconocidos y aplicados por todos los eutopistas del mundo.

### Propuestas y resúmenes

Artículo 1. La Madre Tierra :

1. La Madre Tierra es un ser vivo.
2. La Madre Tierra es una comunidad única, indivisible y autorregulada de seres interrelacionados que nutre, contiene y renueva a todos los seres.
3. Cada ser se define por sus relaciones como elemento constitutivo de la Madre Tierra.
4. Los derechos intrínsecos de la Madre Tierra son inalienables ya que derivan de la misma fuente que la existencia misma.
5. La Madre Tierra y todos los seres poseen todos los derechos intrínsecos reconocidos en esta Declaración, sin distinción alguna entre seres biológicos y no biológicos ni distinción alguna basada en la especie, el origen, la utilidad para los seres humanos o cualquier otra característica.
6. Así como los seres humanos gozan de derechos humanos, todos los demás seres tienen derechos que son específicos de su especie o tipo y apropiados a su papel y función dentro de las comunidades en las que existen.
7. Los derechos de cada ser están limitados por los de los demás seres, y cualquier conflicto entre sus respectivos derechos debe resolverse de forma que se preserve la integridad, el equilibrio y la salud de la Madre Tierra.

Artículo 2. Derechos inherentes de la Madre Tierra Los Derechos Inherentes de la Madre Tierra :

1. La Madre Tierra y todos los seres que la componen poseen los siguientes derechos intrínsecos
   - el derecho a vivir y a existir ;
   - el derecho al respeto ;
   - el derecho a la regeneración de su biocapacidad y a la continuidad de sus ciclos y procesos vitales, libres de perturbaciones humanas
   - el derecho a mantener su identidad e integridad como seres distintos, autorregulados e interconectados;
   - el derecho al agua como fuente de vida
   - el derecho a un aire limpio
   - el derecho a una salud plena;
   - el derecho a no sufrir contaminación, polución ni residuos tóxicos o radiactivos;
   - el derecho a no ser modificados genéticamente o transformados de forma que se perjudique su integridad o su funcionamiento vital y saludable;
   - el derecho a una reparación plena y rápida por cualquier violación de los derechos reconocidos en la presente Declaración resultante de actividades humanas.
2. Cada ser tiene derecho a ocupar un lugar y a desempeñar su papel dentro de la Madre Tierra para que ella pueda funcionar armoniosamente.
3. 3. Todos los seres tienen derecho al bienestar y a estar libres de tortura o tratos crueles a manos de los seres humanos.

Artículo 3. Obligaciones de los seres humanos hacia la Madre Tierra Obligaciones de los seres humanos para con la Madre Tierra :

1. Todo ser humano tiene el deber de respetar a la Madre Tierra y de vivir en armonía con ella.
2. Los seres humanos tienen el deber de :
   - a) actuar de conformidad con los derechos y obligaciones reconocidos en esta Declaración ;
   - b) reconocer y promover la plena realización de los derechos y obligaciones establecidos en esta Declaración
   - c) promover y participar en el aprendizaje, el análisis, la interpretación y la comunicación de formas de vida en armonía con la Madre Tierra, de conformidad con esta Declaración;
   - d) garantizar que la búsqueda del bienestar humano contribuya al bienestar de la Madre Tierra, ahora y en el futuro;
   - e) establecer y aplicar normas y leyes eficaces para la defensa, protección y preservación de los derechos de la Madre Tierra
   - f) respetar, proteger y preservar los ciclos, procesos y equilibrios ecológicos vitales de la Madre Tierra y, cuando sea necesario, restaurar su integridad
   - g) garantizar la reparación de los daños resultantes de las violaciones humanas de los derechos intrínsecos reconocidos en esta Declaración y que los responsables estén obligados a restablecer la integridad y la salud de la Madre Tierra
   - h) empoderar a los seres humanos y a las instituciones para que defiendan los derechos de la Madre Tierra y de todos los seres; y
   - i) establezcan medidas cautelares y restrictivas para evitar que las actividades humanas provoquen la extinción de especies, la destrucción de ecosistemas o la alteración de los ciclos ecológicos;
   - j) garantizar la paz y eliminar las armas nucleares, químicas y biológicas;
   - k) promover y fomentar prácticas que respeten a la Madre Tierra y a todos los seres, de acuerdo con sus propias culturas, tradiciones y costumbres
   - l) promover sistemas económicos que estén en armonía con la Madre Tierra y sean coherentes con los derechos reconocidos en esta Declaración.

## Declaración Universal de los Derechos Humanos en relación con las Especies Animales No Humanas

### Preámbulo

En esta Declaración Universal de Eutopía sobre los Derechos Humanos en las Relaciones con las Especies Animales No Humanas, sentamos las bases para una coexistencia armoniosa basada en la reciprocidad y el mutualismo. Es crucial enfatizar que los principios aquí expuestos se aplican a cualquier animal con el que decidamos cooperar y compartir interacciones simbióticas dentro de Eutopia.

Hacemos esta distinción deliberada para preservar la libertad de los animales como parte integrante del reino animal y de sus hábitats naturales, donde pueden evolucionar sin interferencias humanas. En estas zonas preservadas, reconocemos el valor inestimable de la naturaleza salvaje y nos comprometemos a dejar que estos ecosistemas prosperen con total autonomía.

Sin embargo, allí donde residimos los humanos y elegimos construir comunidades, aspiramos a vivir en armonía con las especies animales seleccionadas, estableciendo relaciones basadas en la comprensión mutua, el respeto y la cooperación beneficiosa. Así pues, esta declaración se aplica a los animales que integramos en nuestro ecosistema, de los que nos hacemos automáticamente responsables, demostrando nuestro compromiso con una simbiosis equilibrada y respetuosa.

### Derechos de las especies bajo responsabilidad humana

Artículo 1. Respeto de la dignidad de los animales :

- Todos los animales bajo la responsabilidad de los habitantes de Eutopía tienen derecho a una vida digna y sin crueldad.
- La interacción humana con los animales debe respetar su naturaleza intrínseca y su comportamiento natural.

Artículo 2. Condiciones de vida dignas Condiciones de vida dignas :

- Los animales deben ser alojados en condiciones que respeten su bienestar físico y psicológico.
- Las instalaciones deben ofrecer un espacio adecuado, acceso a alimentos apropiados, agua limpia y condiciones sanitarias óptimas.

Artículo 3. Acceso a los cuidados Acceso a los cuidados :

- Todo animal bajo la responsabilidad de los habitantes de Eutopia tiene derecho a cuidados veterinarios regulares para garantizar su salud y bienestar.
- El tratamiento médico debe proporcionarse de forma humana, respetuosa y ética.

Artículo 4. Fin de la vida Fin de la vida :

- Los animales destinados al consumo humano deben tener un final de vida respetuoso y sin sufrimientos innecesarios.

## Justificación

### Declaración Universal de los Derechos Humanos

En curso...

### Declaración Universal de los Derechos de la Naturaleza / Madre Tierra

En proceso de redacción...

### Declaración Universal de los Derechos Humanos en relación con las especies animales no humanas

En proceso de redacción...

## Fuentes (Lista no exhaustiva)

[^1]: [Declaración Universal de los Derechos Humanos. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Declaración Universal de los Derechos de la Madre Tierra. 2012 ](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
