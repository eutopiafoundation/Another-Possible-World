---
title: Decidir juntos
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

> Esta página aún se está escribiendo, por lo que pueden faltar algunas partes.
> {.is-info}

## ¿Cómo podemos decidir todos juntos?

Necesitamos entender cómo podemos tomar decisiones y asegurarnos de que éstas son las correctas para alcanzar nuestro objetivo común.

Existen muchas herramientas de toma de decisiones y aún quedan muchas más por inventar. Aquí dejaremos de lado las herramientas de precisión a menor escala y nos centraremos en la global.

En nuestra sociedad occidental, la herramienta utilizada es la democracia. Si analizamos y reinventamos la democracia, podremos desarrollar herramientas a menor escala basadas en ella, que luego podrán adaptarse a otras formas de organización, como las empresas, las escuelas, etc.

## ¿Qué es la democracia?

Democracia es la combinación griega de demos "el pueblo" y kratos "poder". Hoy en día, el término se utiliza para describir cualquier sistema político en el que el pueblo es soberano[^1].

Muchos países han conseguido la igualdad del poder político mediante el sufragio universal. Esta victoria está ahora consagrada en sus constituciones y se considera una de las joyas institucionales más brillantes de sus sociedades.

Pero "democracia" sigue siendo una palabra muy denostada y mal empleada.

Proclamamos la democracia como si realmente tuviéramos el poder.

Pero nuestra democracia no es más que una abdicación recurrente de nuestro poder legítimo en favor de los políticos que se apoderan de él.

Y una vez que lo tienen, se sienten legítimos por tenerlo, pero incluso ellos están bajo el control de un poder mucho mayor.

Pensamos que el sistema político domina y que puede cambiar todo lo demás, incluida la economía y su forma. Pensamos que nuestro sistema es perfectamente democrático por la competencia entre los candidatos a los que votamos.

Pero veremos que la economía interviene en esta elección competitiva. Así pues, por una parte, el sistema electoral ya no tiene la cualidad de ofrecer una buena competencia entre candidatos y, por otra, no domina la economía porque la economía influye en él[^2].

Así pues, es la economía la que domina la política.

En cuestiones climáticas, por ejemplo, los estudios demuestran que no hay correlación entre lo que quiere la mayoría de la gente y lo que obtiene, excepto cuando tiene las mismas preferencias que el 10% más rico de la población[^3].

Nuestra democracia necesita una revisión completa, y aquí estamos hablando de muchos defectos: abuso de poder, corrupción, influencia de los grupos de presión, decisiones descoordinadas y no compartidas, defectos de representatividad, ilusiones de elección, desigualdad de oportunidades.

Y aquí es donde descubrimos que el resultado real de nuestro sistema democrático es lo contrario de su aspiración ideológica.

A pesar de todos estos defectos, la democracia es un riesgo que merece la pena correr[^4] si queremos preservar nuestra libertad.

Así que empecemos por analizar nuestra democracia, comprender lo que falla en ella y, a continuación, reinventemos un sistema democrático completamente distinto.

## El problema de nuestras democracias

### El empresario político[^5][^6][^7]

El objetivo de todo político es estar y permanecer en el poder. Hace carrera, lo que le permite ganarse la vida y encontrar su lugar en la sociedad.

Compiten entre sí, pero también en connivencia: quieren destruir a su adversario político, pero no el cargo político.

Todos los políticos, cualquiera que sea su oposición, tienen un interés común en reforzar, no en disminuir, el poder de los políticos. Y si uno de ellos sugiere la idea, recibirá la desaprobación de todos los demás.

La inmensa mayoría de los empresarios políticos proceden de la misma clase social, y generalmente también de la misma clase social que los dirigentes del sector privado y de las mismas familias burguesas.

Los candidatos no pretenden presentar el programa que constituye su ideal de la buena sociedad.

Sean cuales sean sus fines personales, eligen el programa que mejor conviene al mayor número. Llegan a sacrificar la defensa de sus propias opiniones, ideales y preferencias personales, si las tienen, a las de lo que quiere la masa de la población.

Sin embargo, este conocimiento de los deseos de los electores por parte de los candidatos no se utiliza en su mayor parte para servir al mayor número, sino que, como veremos, se utiliza para explotarlos en beneficio de los más pequeños.

Los políticos simplemente reaccionan a los incentivos que les permiten adquirir y permanecer en el poder.

No tiene sentido odiarlos, simplemente siguen las reglas de un juego. Así que es el juego lo que hay que odiar, no a los jugadores[^8].

Pero el problema de los empresarios políticos es que su objetivo no es tener razón, sino que la gente y las empresas les den la razón.

Los empresarios políticos no son estúpidos, son finos tácticos que reaccionan a los incentivos del juego y están muy bien asesorados por la experiencia de su equipo en la materia, por lo que utilizan diversas estratagemas y alianzas para lograr sus fines.

Para presentar una buena imagen, los candidatos ocultan su personalidad, se disfrazan y se vuelven hipócritas.

Los votantes creen saber a quién votan, porque a menudo se hace hincapié en la personalidad del candidato, destacando su seducción, su carácter tranquilizador o su carisma, creando un efecto de halo. Y estas impresiones son mucho más fácilmente manipulables por los medios de comunicación que el contenido de un programa.

### Programas políticos[^2]

Los candidatos construyen su "programa" no de una vez, sino mediante adiciones o modificaciones sucesivas de declaraciones y acciones, sin poder por ello volver mucho atrás, borrar o contradecir palabras o acciones anteriores. Esta limitación puede explicarse por el tiempo que se tarda en dar a conocer un programa, mientras que aclararlo sobre la marcha, añadirle cosas, reforzarlo, llega más eficazmente a los electores.

También se explica por el hecho de que estas contradicciones o desmentidos dañarían la fiabilidad y credibilidad del candidato, creando una estructura evolutiva e iterativa de conocimiento y construcción del programa.

Esto conduce a menudo a diferencias en sus programas y a una imperfección que obstaculiza la calidad democrática del resultado del proceso.

En segundo lugar, los programas son globales, abarcan todos los temas a la vez, y hay que elegir entre ellos, aunque es muy posible que se prefiera uno en un punto y otro en otro.

En este caso, los programas dividen a la población, y uno de ellos podría ser elegido por mayoría, mientras que esa misma mayoría prefiere un punto de todos los demás programas al del ganador.

Esto es algo que permiten los referendos específicos, en concreto, expresar la opinión sobre cada cuestión por separado.

El efecto de las contribuciones financieras interesadas es uniformizar los programas, mientras que el efecto del apoyo idealista/activista es diferenciarlos.

### Honestidad y transparencia

Todo sistema manipulable lo será en el peor momento posible[^9].

Cuando se diseña un sistema, hay que preguntarse absolutamente cómo van a poder atacarlo los interesados en el sistema y aprovecharse deshonestamente de él.

La honradez y la transparencia son problemas persistentes en los sistemas democráticos de todo el mundo. A menudo se acusa a los políticos y representantes electos de falta de transparencia, de ser corruptos y de verse envueltos en numerosos conflictos de intereses al favorecer intereses particulares en detrimento del interés general.

En nuestro sistema, ciertas normas no fomentan la honradez y la ética.

Por lo tanto, es crucial diseñar sistemas que animen a las personas a actuar de forma ética y moral.

Tenemos que desarrollar el principio de divulgación cuando los participantes tengan interés en ser honestos en sus divulgaciones y comportarse de forma ética y moral, ya que esto redundaría en su propio beneficio y les beneficiaría a largo plazo. Deben incluirse mayores medidas de transparencia.

### La elección de alternativas

Aparte de que los programas son globales, la elección de programas o candidatos que se ofrece al votante suele ser reducida.

La financiación de las campañas electorales es uno de los factores que limitan la elección de alternativas. Los candidatos son los que tienen acceso a fuentes de financiación y pueden desplegar suficiente información y propaganda sobre sí mismos para dar visibilidad a su partido y tener posibilidades de ganar[^10].

Los partidos políticos pueden tener estructuras internas que favorezcan a determinados candidatos o tendencias políticas frente a otros. Esto puede limitar las opciones de los votantes al darles sólo unas pocas opciones que hayan sido aprobadas por los partidos políticos.

### Delegación de poder

Unas elecciones son dos cosas: un voto y una delegación de poder.

Como ya se ha mencionado, nuestra democracia es la abdicación recurrente del poder legítimo del pueblo a un grupo de personas.

La delegación de poder también puede conducir a una pérdida de control sobre las decisiones. Si una persona o grupo delega su poder en otra persona o grupo, puede perder el control sobre qué decisiones se toman y cómo se aplican.

Con el pretexto de la delegación, que da plena legitimidad a los representantes elegidos, puede haber apropiación indebida, enajenación, expoliación o usurpación del poder.

Uno de los principales problemas de la delegación de poder es el riesgo de desempoderamiento. Si una persona o grupo delega su poder de decisión en otra persona o grupo, puede eximirse de responsabilidad en caso de fallo o error por parte de la persona o grupo en quien ha delegado su poder.

Otro problema de la delegación de poder es el riesgo de conflicto de intereses.
Algunas operaciones requieren tiempo, energía, conocimientos previos y capacidad intelectual. En estos casos, puede ser rentable, o incluso necesario, recurrir a la división del trabajo y a la especialización delegando parte del trabajo de toma de decisiones. Pero esta delegación es poder. Por ejemplo, un gobierno puede delegar su poder en una empresa para que gestione un proyecto. Pero como el delegatario no sabe exactamente lo que delega, ya que por definición no dispone de toda la información sobre las posibilidades. Esto deja la puerta abierta a todo tipo de abusos[^2].

### Centralización del poder

El poder está centralizado cuando unos pocos deciden mucho y muchos deciden poco.

La centralización aumenta la delegación de poder, lo que a su vez reduce la democracia.

Cuando un pequeño grupo de personas ostenta el poder absoluto, puede dar lugar a decisiones unilaterales y políticas injustas que ya no tienen en cuenta los intereses y opiniones de los ciudadanos.

La centralización del poder puede hacer que los gobiernos sean más vulnerables al abuso de poder, la tiranía y la dictadura, ya que hay menos controles y equilibrios para limitar la autoridad de un pequeño grupo de personas.

### Sistemas de votación

Es importante tener en cuenta que un candidato elegido lo es por votación y que cambiar la votación cambiará quién es elegido.

El proceso electoral afecta en gran medida al resultado de una votación. Por lo tanto, la elección de la papeleta afecta al futuro de nuestras sociedades. Por lo tanto, es importante centrarse en la elección de la papeleta que queremos establecer.

Las elecciones a una o dos vueltas tienen un problema fundamental: no permiten expresar con claridad y precisión las preferencias de los votantes[^11].

En un sistema de una sola vuelta, el votante sólo puede votar a un candidato, lo que puede ser problemático si varios candidatos cuentan con un apoyo significativo. Por ejemplo, en unas elecciones en las que hay tres candidatos principales, el votante debe elegir entre tres opciones diferentes, pero sólo dispone de un voto para expresar su elección. Esto puede llevar a situaciones en las que el candidato elegido no tenga el apoyo de la mayoría de los votantes, sino simplemente el mayor número de votos.

El problema de las elecciones a dos vueltas es que a veces pueden dar lugar a tácticas de voto estratégicas y a alianzas entre candidatos. En un sistema de dos vueltas, los dos candidatos con más votos en la primera vuelta se enfrentan en una segunda vuelta. Esto puede llevar a situaciones en las que los votantes voten de forma diferente en la primera vuelta para asegurarse de que su candidato preferido se clasifica para la segunda vuelta, aunque no estén realmente convencidos por ese candidato. Además, puede darse el caso de que los candidatos unan sus fuerzas para expulsar a otro candidato de la carrera, en lugar de intentar ganar las elecciones de forma independiente.

En general, el voto por mayoría tiene el grave inconveniente de no tener en cuenta la intensidad de las preferencias de la gente. Los que son casi indiferentes entre dos alternativas, y los que prefieren una mucho más que la otra, tienen el mismo peso en la elección entre ellas. Por ejemplo, 1001 ciudadanos que apenas prefieren una alternativa pesan más que otros 1000 para los que es la peor catástrofe.

Estos sondeos fomentan el voto útil, lo que incita a los políticos a organizarse en partidos políticos y a colocar a los electores en casillas predefinidas, reforzando el efecto de bipolarización.

La bipolarización del electorado en política se refiere a la tendencia de los votantes a agruparse en torno a dos partidos políticos principales, creando una clara división política entre estos dos partidos. Esto deja poco espacio a los partidos o candidatos independientes, o a los partidos políticos más pequeños. Esto también tiene la consecuencia negativa de conducir a una mayor polarización de la opinión, una reducción de la diversidad de puntos de vista

Con estas papeletas, acabamos votando en contra de un candidato y no a su favor.

Pero el voto útil no legitima a la persona elegida.

La representación proporcional hace que la política no esté demasiado polarizada, pero puede conducir a una fragmentación del poder y dificultar la formación de gobiernos estables. Es difícil que un partido consiga la mayoría absoluta de los escaños, lo que puede dar lugar a coaliciones de gobierno inestables o a gobiernos minoritarios que tienen dificultades para sacar adelante su programa.

Unas buenas elecciones deben ser independientes de alternativas irrelevantes y cerrar la puerta al dilema del voto útil.

Ejemplo de un posible voto alternativo:

Voto de aprobación: cada votante puede aprobar tantos candidatos como desee, sin orden de preferencia. El candidato con más aprobaciones es elegido.

El voto por puntos (o voto ponderado) es un sistema electoral alternativo en el que cada votante dispone de un determinado número de puntos que puede repartir entre los candidatos.

Voto por mayoría: los votantes dan a cada candidato una puntuación, por ejemplo de "muy bueno" a "rechazado". El candidato con la puntuación media más alta es elegido.

El voto Condorcet es un sistema de votación en el que los votantes votan comparando a todos los candidatos de dos en dos[^12].

Voto de Condorcet aleatorio: en este caso, cada votante compara pares de candidatos como en el voto de Condorcet, pero en lugar de tener en cuenta todos los votos de todos los votantes, se selecciona un número de comparaciones al azar para determinar el ganador.

Votación Mehestan (Girasol): es un sistema de votación que combina la votación por puntos y la votación Condorcet. Consiste en comparar a cada candidato uno por uno y decir cuál preferiría para cada criterio, pero definiendo también el nivel de preferencia para ese criterio. Este método de votación pretende ser a la vez justo y representativo, permitiendo a los votantes votar por su candidato preferido sin riesgo de dividir el voto y teniendo en cuenta las preferencias de todos los votantes a la hora de determinar el ganador.

### Partidos políticos

Los sistemas de votación descritos anteriormente incitan a los políticos a organizarse en partidos, debido a su dependencia de alternativas irrelevantes.

### Lentitud institucional

En muchos casos, los problemas políticos requieren soluciones rápidas y eficaces para minimizar los daños y las pérdidas. Sin embargo, la lentitud de los procesos políticos, como las negociaciones, los debates, las votaciones y la burocracia, pueden retrasar la toma de decisiones e impedir que los gobiernos actúen con rapidez.

### Mandatos a corto plazo

Los representantes electos en cuestión son elegidos por un determinado periodo de tiempo, normalmente unos pocos años. Durante este tiempo, tienen una libertad personal sustancial para elegir sus acciones. Los votantes ya no les limitan en modo alguno. Muy poco obliga a los representantes electos a cumplir sus promesas electorales, aparte de su honor, pero pueden muy bien decir que las cosas han cambiado.

Los representantes electos suelen estar más inclinados a centrarse en los resultados a corto plazo que en los retos a largo plazo.

Esto se debe a que estos mandatos animan a los representantes electos a centrarse en cuestiones políticas que tienen más probabilidades de atraer el apoyo rápido de los votantes, en lugar de en cuestiones más complejas que requieren un compromiso mayor y a más largo plazo.

Los políticos pueden verse tentados a tomar decisiones que sean populares a corto plazo para maximizar sus posibilidades de reelección. Esto puede conducir a políticas que no son viables o sostenibles a largo plazo y pueden causar mayores problemas en el futuro. Esto conduce a un enfoque político de la gobernanza en lugar de un enfoque más pragmático y ponderado.

### Bluffing to expertise

El "farol de la pericia" en política se produce cuando los políticos utilizan su posición con un título en ciencias o administración para dar la impresión de que tienen pericia o conocimientos especiales sobre temas, cuando en realidad no es así. Pueden utilizar esta estrategia para manipular o persuadir a los votantes, pero puede conducir a decisiones políticas equivocadas.

El énfasis en las carreras de ciencias y administración conduce a una concentración de poder e influencia en manos de unas pocas élites que han tenido la suerte de acceder a estos cursos. Esto conduce a una pérdida de diversidad e inclusión en los círculos de toma de decisiones, ya que las personas procedentes de entornos menos privilegiados suelen tener menos oportunidades de obtener estos títulos.

Sin embargo, la posesión de un título no garantiza necesariamente aptitudes o conocimientos especializados en materia de gobernanza o política. El farol de la pericia, del que hemos hablado antes, puede ser utilizado por personas que tienen títulos pero carecen de los conocimientos o las aptitudes reales para tomar decisiones con conocimiento de causa[^2].

### La ciencia despreciada

Las decisiones políticas deben tomarse a menudo sobre la base de datos científicos complejos y pruebas empíricas, sobre todo en ámbitos como la salud pública, el cambio climático, la seguridad alimentaria, etcétera. Cuando los responsables políticos ignoran las pruebas científicas, toman decisiones que no se basan en hechos y datos verificables, sino en creencias u opiniones. Esto puede tener graves consecuencias para la sociedad, sobre todo en términos de salud, medio ambiente y seguridad.

### Propaganda

Los votantes no votan a un candidato del que no saben nada. Sin información sobre el candidato que se transmite a los votantes, un candidato no tiene ninguna posibilidad. Sólo empieza a tener posibilidades cuando la información que recibe supera un determinado nivel: sus posibilidades de ser elegido aumentan cuando aumenta la información que recibe.

La inversión política es una inversión como cualquier otra, la búsqueda es siempre la misma: el beneficio.

La historia de la instauración de la "democracia electoral" es paralela a las luchas y victorias formales por la libertad de expresión, de prensa, de reunión y de asociación. Pero estas libertades, debido a su necesaria base material y económica, han sido acaparadas en su mayor parte por quienes detentan estos recursos económicos. Y así utilizarlas para la propaganda electoral[^24].

La propaganda se utiliza para manipular a la opinión pública e influir en los resultados de elecciones o referendos a favor de un partido o ideología determinados. Puede ser engañosa, partidista y basarse en mentiras, medias verdades u omisiones de hechos importantes. También puede jugar con los miedos, prejuicios y emociones de la gente en lugar de con la razón y la lógica.

### El hooligan político

Podemos distinguir 3 tipos caricaturescos de ciudadano durante las elecciones:[^13][^14]

- Los hobbits: aquellos que no tienen ningún interés en la política y son muy ignorantes en este ámbito, no tienen demasiados sesgos.
- Los hooligans: son parciales, pero muy parciales, suelen ser víctimas del sesgo de confirmación y defienden valores de forma irracional.
- Vulcanos: son personas capaces de pensar racionalmente.

Nuestros modelos asumen que todos los ciudadanos son o pueden llegar a ser vulcanos, pero en la práctica la mayoría de la gente son hobbits o hooligans.

La intuición nos obliga a formarnos una opinión, y luego se utiliza la razón para apoyar estas opiniones. Racionalizamos nuestras convicciones.

No utilizamos la razón para averiguar la verdad, sino para justificar las posiciones ideológicas que ya hemos adoptado.

Por ejemplo, si decimos la palabra "Democracia", nuestra intuición nos dice que es algo bueno, y entonces nuestra razón manipula la definición de la palabra para justificar nuestra intuición.

Hoy en día, es fácil confirmar nuestra intuición gracias a Internet, que puede proporcionarnos todos los argumentos que necesitamos, y si estos argumentos o cifras no van en nuestra dirección, siempre podemos cuestionar los motivos del informador de los hechos o interpretarlos de otra manera.

Además, al rodearnos de personas afines, nuestro entorno refuerza constantemente nuestra intuición y tiende a polarizarla.

Los activistas políticos se vuelven demasiado confiados y resulta imposible hacerles cambiar de opinión, incluso con datos y cifras.

Y cuando la intuición predomina de esta manera, cuanto más te informas, más buenas razones encuentras para creer lo que crees, independientemente de la información que leas.

El compromiso político sólo refuerza el hooliganismo político.

Cuanto más nos interesemos por un partido político con precio, más radical será nuestra inclinación inicial.

Los vídeos y artículos científicos engañosos pero bien explicados refuerzan la convicción científica de quienes los ven y leen.

Dado que la selección grupal es más eficaz que la individual, las tribus que han sobrevivido son aquellas en las que los individuos han cooperado lo suficiente como para garantizar la supervivencia del grupo.

Y, por tanto, aquellas que están dispuestas a sacrificar sus intereses individuales por el bien del grupo. La selección natural ha seleccionado el gamberrismo político.

Un voto basado en el gamberrismo político proporciona placer, mientras que un voto racional requiere esfuerzo mental.

### La ignorancia y la irracionalidad de los ciudadanos

La gente es ignorante, casi todos los votantes no tienen nociones de ciencia ni entienden el sistema político, están mal informados y no conocen las cifras[^16].

Algunos de ellos no son conscientes de que están participando en gamberradas políticas.

La mayoría ignora los diferentes sesgos cognitivos y los efectos a los que nos enfrentamos. Repasamos varios de ellos en el capítulo 3 sobre HomoSapiens. He aquí algunos de ellos a modo de recordatorio:

- Sesgo de confirmación[^17]
- El efecto halo[^18]
- Disonancia cognitiva[^19]
- Presión de pareja
- Riesgo de disponibilidad[^20]

La escuela y los graduados pueden hacer creer a la gente que son eruditos, hacerles creer que son más listos que los demás y crearles un exceso de confianza en ámbitos que no les son propios.

Todo el mundo se vuelve muy seguro de sí mismo. Y el problema es que la incertidumbre se ve como un signo de debilidad, cuando en realidad es una cualidad fundamental.

El efecto Dunning-Kruger demuestra que las personas con pocos conocimientos o habilidades en un campo tienden a sobrestimar su propio nivel de dominio, mientras que las personas con mayores habilidades tienden a subestimar su nivel de dominio[^16].

Pero la ignorancia de los ciudadanos no es el mayor problema, porque ni siquiera informando a los ciudadanos se evita que voten irracionalmente.

La gente se comporta como gamberros, no como científicos.

### Tiempo y energía para informarse

Informarse, aprender, conocer, comprender, para luego elegir y decidir, y votar, requiere tiempo, energía y conocimientos previos por parte del votante, lo que a su vez requiere tiempo y esfuerzo. Estos costes explican una cierta delegación de poder.

Las campañas electorales suelen ser cortas e intensas, lo que deja a los votantes poco tiempo para informarse y evaluar las distintas opciones. Además, los medios de comunicación tienden a menudo a centrarse en los aspectos más espectaculares o controvertidos de los acontecimientos políticos, en lugar de ofrecer un análisis profundo y matizado.

Esto puede conducir a una polarización de la política y a una reducción de la calidad del debate público. Los votantes pueden verse tentados a recurrir a fuentes de información simplistas o partidistas que no reflejan necesariamente la complejidad de los temas.

### Comunitarismo

El comunitarismo puede definirse como una tendencia a conceder más importancia a la pertenencia a un grupo o comunidad que al individuo como tal. El principal problema del comunitarismo es que puede conducir a una fragmentación de la sociedad, en la que cada grupo se centra en sus propios intereses en lugar de en el interés común de la sociedad en su conjunto[^21].

El comunitarismo solía ser geográfico, pero ahora se está extendiendo a todo Internet.

A nadie le gusta estar en minoría.

Cuando los internautas dejan de participar en un debate porque se sienten demasiado minoritarios, se produce una segregación ideológica.

Cambiar de vecindario ideológico en Internet es mucho más fácil que desplazarse geográficamente.

Las personas pueden abandonar comunidades en las que se sienten en minoría para unirse a otras en las que son mayoría.

Las burbujas de filtros son lo que llamamos aislamientos ideológicos en la red.

Nos encerramos en un mundo donde todo lo que leemos y oímos confirma lo que ya pensamos, lo que dificulta cuestionar nuestras convicciones y entender por qué otros piensan lo que piensan.

Estas burbujas de filtros se amplifican con los contenidos personalizados que ofrecen los gigantes de la red: Facebook, YouTube, etc. Nos encierran en la comunidad con la que compartimos nuestras ideas, ofreciéndonos contenidos personalizados. Cada vez que hacemos clic en un enlace o nos gusta un post, nos encierran aún más en una comunidad con la que compartimos nuestras ideas[^22].

### Convicciones

Las convicciones pueden definirse como creencias arraigadas u opiniones firmemente arraigadas sobre un tema. Aunque las convicciones pueden ser beneficiosas, suelen ser más problemáticas que otra cosa, sobre todo en el ámbito de la política.

Uno de los principales problemas de las creencias es que pueden provocar polarización y división. Si las personas tienen creencias muy arraigadas sobre un tema, pueden estar menos inclinadas a considerar otros puntos de vista o a trabajar con personas que tienen creencias diferentes. Esto puede provocar polarización y división en la sociedad[^23].

Pueden llevar a una mentalidad cerrada. Si las personas están demasiado apegadas a sus creencias, pueden estar menos dispuestas a escuchar opiniones o hechos que las contradigan. Esto puede llevar a tomar decisiones basadas en prejuicios y no en hechos,

Las creencias pueden provocar resistencia al cambio. Si la gente está firmemente apegada a sus creencias, puede estar menos dispuesta a aceptar nuevas ideas o cambios en la sociedad.

## Soluciones a los problemas de nuestras democracias

### El empresario político

La solución al empresario político es suprimir esta función o al menos limitarla aumentando la participación ciudadana.

Que el empresario político deje de ser alguien en quien se delega el poder y que, desde el momento en que lo adquiere, puede hacer con él lo que quiera. Sino que debe tener una función representativa, y que en cuanto las personas que le delegan el poder sientan que ya no les representa, deben poder destituirle.

Hay que instaurar una transparencia total para que los ciudadanos sepan exactamente a quién delegan su poder y qué hacen con él.

Y para que haya una buena diversidad, debemos asegurarnos de que los candidatos proceden de distintos entornos y tienen las mismas oportunidades que los demás para acceder a estos puestos.

### Programas políticos

Una solución al problema de los programas políticos podría ser hacerlos más modulares y más específicos para cada tema, en lugar de presentarlos como un paquete global. Esto permitiría a los votantes elegir las propuestas que prefieren en cada tema por separado, en lugar de tener que aceptar o rechazar todo el programa.

### Honestidad y transparencia

En una democracia, es importante que la toma de decisiones sea transparente y se rindan cuentas, y que los gobernantes sean directamente responsables de sus actos. La delegación de poder debe utilizarse con cuidado y deben tenerse en cuenta los riesgos asociados para garantizar la protección de los intereses de los ciudadanos.

### La elección de alternativas

Es importante dar al votante una posibilidad completa y amplia de elegir alternativas, y la misma visibilidad e información sobre ellas. Los programas globales limitan de nuevo esta posibilidad, y los temas podrían separarse para permitir decisiones más específicas.

### Delegación de poder

Los votantes deben poder elegir si delegan o no su poder. También deben poder retractarse y retirar su delegación de poder en cualquier momento.

### Centralización del poder

En una democracia, la distribución del poder es esencial para garantizar que los intereses y opiniones de los ciudadanos estén representados y se tengan en cuenta en la toma de decisiones. Esto implica la participación activa de los ciudadanos, la descentralización del poder y la creación de mecanismos de rendición de cuentas y transparencia para limitar el poder de los dirigentes elegidos.

### Sistemas de votación

Hay que favorecer los sistemas de votación que tengan en cuenta la intensidad de las preferencias de los ciudadanos y evitar los que fomentan el voto útil.

### Lentitud institucional

Deberían introducirse referendos rápidos para casos de emergencia. De forma más general, los ciudadanos deberían poder expresar sus opiniones con mayor regularidad.

### Mandatos a corto plazo

Existen varias posibilidades, entre ellas la introducción de mandatos más largos con la introducción de un procedimiento que otorgue a los votantes el derecho permanente a revocar a sus representantes electos. También podemos hacer que los ciudadanos sean más conscientes del largo plazo y motivar a los políticos para que planifiquen a largo plazo. También podríamos limitar los mandatos, o incluso abolir el papel del empresario político.

### Bluffs to expertise

Es importante reconocer que las competencias y la pericia no son exclusivas de las cualificaciones académicas y que la inclusión de personas de orígenes diversos puede aportar una riqueza de perspectivas que puede ser beneficiosa para la gobernanza y la política.

### Science scorned

Implicar a los científicos en los procesos de toma de decisiones: Los científicos pueden contribuir a fundamentar las decisiones políticas aportando datos, análisis y evaluaciones independientes y basados en pruebas.

Aumentar la transparencia y la responsabilidad de los datos y pruebas científicas que utilizan para tomar decisiones políticas, y rendir cuentas de sus decisiones y sus consecuencias. Fomentar una comunicación eficaz entre los científicos y los responsables políticos, comunicando sus conclusiones de forma clara y concisa, evitando la jerga técnica. Fomentar una cultura de respeto por la ciencia. Educar a los responsables políticos sobre la ciencia: Los políticos deben recibir formación sobre el método científico y los conceptos científicos clave para comprender mejor los datos y las pruebas científicas.

Establecer en una constitución los principios que deben tenerse en cuenta para que las decisiones respeten los principios inalienables sobre los que existe consenso, por ejemplo garantizar la felicidad humana y el equilibrio de nuestros ecosistemas en la Tierra.

### Propaganda

Para reducir el impacto de la propaganda en la política, es importante fomentar una información libre y transparente, basada en hechos y datos verificables. Los medios de comunicación deben desempeñar un papel clave en este proceso, proporcionando una cobertura equilibrada e imparcial de los acontecimientos políticos, comprobando las afirmaciones políticas y promoviendo la comprobación independiente de los hechos. También es importante concienciar a la opinión pública sobre las técnicas de propaganda y manipulación de la opinión, educándola sobre cómo reconocer y resistirse a estas tácticas.

### Ignorancia ciudadana

Una educación de calidad puede ayudar a los ciudadanos a ser votantes más informados. Es importante enseñar a los estudiantes las habilidades del pensamiento crítico, el razonamiento lógico, la búsqueda de información fiable y la comprensión de las cuestiones políticas.

Transparencia y accesibilidad: las políticas y decisiones públicas deben ser transparentes y fácilmente accesibles para que los ciudadanos puedan comprender mejor cómo funciona su gobierno y qué decisiones se toman.

Comunicación: es importante que los líderes políticos se comuniquen de forma clara y regular con los ciudadanos, utilizando un lenguaje sencillo y evitando la jerga técnica.

Participación: fomentar la participación ciudadana en los procesos políticos, como elecciones, peticiones, debates públicos, consultas, encuestas de opinión, etc., puede contribuir a aumentar la implicación de los ciudadanos en la vida política y a reforzar su comprensión de las cuestiones políticas.

### Falta de tiempo

El problema del tiempo puede resolverse dando más tiempo a los ciudadanos reduciendo su horario de trabajo. Otra posibilidad es disponer de un algoritmo que seleccione cada vez con precisión una muestra representativa de la sociedad para evitar que todo el mundo tenga que votar.

### El hooligan político

Tenga cuidado al leer los programas sin hooliganismo político, y contrástelos con investigaciones económicas y sociológicas. Pregúntate si estamos siguiendo nuestra intuición, que luego nuestra razón habría respaldado con argumentos.

Tomar conciencia de este hooliganismo nos llevará a mejores debates, y nos proporcionará una forma de pensar que no se quede estancada en la intuición y que nos permita salir de nuestro hooliganismo político. Tenemos que apreciar todo lo que es contraintuitivo, apreciar el hecho de convencer a nuestra intuición de que nuestra intuición está equivocada.

Necesitamos apreciar el hecho de que estamos equivocados, de que podemos cuestionarnos, y comprender que es desbloqueando nuestra intuición como podemos avanzar más.

### Comunitarismo

Tenemos que luchar contra el deseo de todos de formar parte de la mayoría.
Luego debemos luchar para que formar parte de la minoría no sea una experiencia embarazosa.

Debemos tener cuidado de no dar ventaja a la mayoría simplemente porque es la mayoría.

Hay que hacer que la experiencia de pertenecer a la minoría sea agradable.

Hacer que la gente que está en la mayoría quiera estar en la minoría.

Que todos intenten entenderse y mezclarse.

### Convicciones

En una democracia, es importante que la gente tenga una mentalidad abierta y esté dispuesta a escuchar las opiniones y puntos de vista de los demás. Hay que concienciar y educar a los ciudadanos al respecto.

### Conclusión de las soluciones

Hay muchos problemas inherentes a la democracia.

Para que funcione bien, necesita unas condiciones ideales.

Buenos medios de comunicación para estar bien informados y que los políticos en el poder actúen realmente en interés del pueblo.

Pero el problema es que nuestra democracia está creando un contexto en el que ocurrirá justo lo contrario.

La gente o bien es un hobbit, no le interesa la política, es muy ignorante en este terreno, no tiene demasiados prejuicios, o bien es un hooligan, tiene un prejuicio, pero muy asumido, y a menudo víctima del sesgo de confirmación, llega a defender valores de forma irracional.

Lo que necesitamos son vulcanos, gente que sepa pensar racionalmente.

Y en este momento no se pueden tener discusiones racionales entre personas políticas en una democracia con representantes elegidos que quieren ser elegidos.

Si queremos un cambio radical en la democracia, tenemos que tener absolutamente en cuenta todos estos elementos racionales, como el principio de los vulcanos políticos, la segregación comunitaria, etc.

Pero la democracia es una apuesta que merece la pena, porque con ella todos nos sentimos iguales, ofrece capital moral y nos federa para cooperar.

## Idea experimental para una nueva democracia profunda (V1)

Nombre: E-Democracia participativa compartida por referéndum espontáneo apoyada en la experiencia y la sabiduría.

Para permitir una participación ciudadana más amplia y directa, estamos utilizando las tecnologías de la información y la comunicación para crear una herramienta digital que facilite y mejore los procesos democráticos.

Esta herramienta es una plataforma web, también disponible como aplicación móvil. Se puede pensar en ella como en una wiki, en la que se puede acceder a información relacionada con los distintos estratos de la sociedad en la que se participa, como leyes, modos de organización, decisiones actuales, etc. Estos estratos están divididos y estructurados de forma que se pueda acceder a ellos fácilmente. Estos estratos se dividen y estructuran en categorías y subcategorías según sea necesario en función de la naturaleza del estrato.

Cualquiera puede sugerir cambios en la información introducida. Como es imposible que cada ser humano lea todas las propuestas que podrían hacer los 7.000 millones de seres humanos. Utilizamos un algoritmo que toma una muestra representativa de todas las personas vinculadas a la propuesta. La propuesta se envía entonces a esta muestra representativa. La gente puede votar a favor, en contra o abstenerse, y si hay más de un 50% a favor, la propuesta pasa al siguiente nivel.

A continuación, la propuesta se envía a un consorcio de expertos y sabios que la estudiarán para dar su opinión en profundidad, comprobar el aspecto técnico y científico, estimar los recursos y el tiempo que hay que dedicar, pero también y sobre todo comprobar que se ajusta a las constituciones establecidas.

La constitución de Eutopía sentará las bases de la sociedad, que habrá que estudiar y justificar por permitirnos alcanzar nuestro objetivo común de vivir una vida plena y sostenible en armonía con la Tierra. Luego habrá que enseñarla para despertar a la gente a ella, para que guíe sus propuestas y decisiones.

Una vez concluido el trabajo de los expertos y sabios, sus opiniones se adjuntarán a la propuesta final, para que los ciudadanos tengan todas las claves que necesitan para hacer su elección. Las personas designadas como expertos o sabios serán elegidas por los ciudadanos en sus respectivas zonas. Habrá que garantizar la transparencia y veracidad de la información facilitada en estos dictámenes, así como la sencillez de la información, todo ello para que los ciudadanos tengan todas las cartas en la mano para hacer su elección y ejercer su poder con la conciencia tranquila.

A continuación, la propuesta se enviará de nuevo a un grupo de muestra con la información y las opiniones emitidas por los expertos y sabios. Si la propuesta vuelve a ser aprobada, se envía entonces a toda la población para su adopción definitiva. El resultado debe ser entonces el mismo que el de la muestra. Sin embargo, cuando los ciudadanos reciban esta propuesta para la votación final, sabrán de antemano que más de la mayoría tiene intención de aceptarla, lo que tiene la ventaja de darles una última oportunidad de retirarse.

Así pues, el ciudadano no recibe todas las propuestas, sino sólo la que le concierne, según que la decisión se aplique más local o globalmente.

Antes de que envíe su propuesta, también ponemos en marcha un sistema colaborativo que le aconsejamos encarecidamente que utilice. Su propuesta es entonces, ante todo, una sugerencia, y estas sugerencias son accesibles a cualquiera que desee hacerlas, y cualquiera puede participar en ellas.

También puede delegar su poder en otra persona, que votará la propuesta por usted. No obstante, tendrá acceso al historial de las posiciones votadas para comprobar que está de acuerdo con ellas, y podrá retirar su delegación de poder en cualquier momento. También puedes elegir el nivel de poder que deseas delegar en relación con las distintas capas de la sociedad.

Por último, es este sistema democrático el que utilizaremos también para la edición colaborativa de historias. O antes, a medida que probemos nuestro sistema podremos iterar sobre él y modificarlo hasta que estemos plenamente satisfechos con él, y será en última instancia esta versión final la que constituirá la democracia de Eutopía.

> "La democracia en este horizonte parece requerir tiempo, coraje, perseverancia... y quizás un toque de locura..." Datageueule[^4]

## Fuentes (Lista no exhaustiva)

[^1]: [Wikipedia : Democracia ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. ¿Son democráticas las elecciones? 1977
[^3]: David Shearman. El desafío del cambio climático y el fracaso de la democracia (Politics and the Environment). 2007.
[^4]: [Datageule. ¿Democracia(s)?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All : El principio fundamental de la política](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory. 1656
[^8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Foster honestidad | Democracia 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja y Brian F. Schaffner. Financiación de campañas y polarización política: cuando prevalecen los puristas. 2015
[^11]: [Science4All. Our Democracies Divide | Democracy 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. The Randomized Condorcet Ballot | Democracy 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Ciencia4Todos. ¿Es usted un hooligan político? Democracia 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. La mente justiciera: Por qué las buenas personas están divididas por la política y la religión. 2013
[^15]: [Science4All. Rationally Irrational | Democracy 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. El mito del votante racional: por qué las democracias eligen malas políticas. 2008
[^17]: [El trabajo de los sesgos #5: Sesgo de confirmación](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [AmazingScience - The Halo Effect - Stupid Brain #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [The Bias Bracket #3- Disonancia Cognitiva](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Academia Franklin Templeton - Sesgo de disponibilidad](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. El pequeño comunitarismo se hará grande | Democracia 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [Conferencia TED. Eli Pariser advierte contra las "burbujas de filtros" en línea](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Querida convicción, ¡¡¡muta en una infección VIRAL!!! Democracia 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Concentraciones mediáticas: los multimillonarios te informan. 2016
