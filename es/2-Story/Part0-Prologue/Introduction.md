---
title: Introducción
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

## ¿Quién eres?

Puede que no seas un gran sociólogo, o un gran filósofo, o un gran economista, o un gran cualquier otra cosa, de hecho quizás no fuiste a la universidad, quizás fuiste malo en la escuela. Ese es el caso de muchos de nosotros también, pero a pesar de estos antecedentes educativos tan diferentes, tenemos la confianza de proponerte aquí otro mundo posible, un mundo que aspira al ideal, y te invitamos a unirte a nosotros.

Quizá te preguntes por qué estarías bien situado para hacerlo realidad. Y quizá una parte de ti, nada más leer este párrafo, ya sienta el impulso de cerrar este libro para no perder más tiempo. Pero permítanos unos párrafos más para intentar convencerle.

## Imaginación

Aunque no eres un gran experto en ninguna de las grandes áreas de este mundo, hay algo para lo que estás especialmente dotado, y es para soñar. Esta capacidad de imaginar es el mayor don del ser humano. Te invitamos aquí a soñar, pero sobre todo a ayudar a los demás a soñar.

Cuando empezamos, éramos una especie insignificante con un poder de acción muy limitado. Lo que marcó la diferencia entre nuestra especie, el Homo Sapiens, y las demás fue nuestra capacidad para cooperar en gran número. [^1]

Y esto se lo debemos a nuestra imaginación, a nuestra capacidad de inventar historias y contárnoslas a nosotros mismos.

Gracias a los mitos colectivos, que también podemos llamar "ficciones", hemos sido capaces de unirnos y cooperar.

Tenemos la capacidad de creer en cosas que no existen, hemos inventado muchas leyendas, mitos e historias, hemos trazado líneas imaginarias en el mundo, creado naciones y costumbres, hemos inventado el dinero y le hemos dado el mayor valor en nuestra imaginación.

Este don de la imaginación procede de nuestra necesidad de sentido, de nuestra necesidad de predecir y comprender nuestro entorno para sobrevivir mejor en él[^2].

Dejamos que esta imaginación colectiva rija toda nuestra vida, incluso dejamos que rija el destino de la humanidad en su conjunto. Su poder es tal que nos lleva a destruir nuestro propio entorno, a provocar nuestra propia destrucción.

Pero todo esto no es más que un imaginario colectivo y deja de existir en el momento en que todos decidimos dejar de creer en él.

En realidad, lo que más comúnmente llamamos razón es más bien el hecho de seguir creyendo en las reglas de nuestro imaginario colectivo y ajustarnos a ellas.

## Razón

Una vez que decidimos dejar atrás este mundo imaginario, es cuando por fin podemos encontrar la verdadera razón.

Puede que sientas que no sabes mucho al respecto, pero descubrirás que no necesitas saber mucho para encontrar las soluciones y tomar las decisiones que nos llevarán a todos a una vida plena y con sentido.

Porque las respuestas a nuestras preguntas ya están dentro de cada uno de nosotros. Para encontrarlas, lo único que tenemos que hacer es dejar atrás nuestra imaginación colectiva, abrir el corazón y escuchar. Puede parecer una tontería, pero estamos convencidos de que ahí reside la verdadera razón.

Y como ya hemos iniciado este proceso de deconstrucción, eso es lo que te proponemos descubrir aquí, un mundo verdaderamente basado en la razón.

Un mundo cuya sencillez, sentido y lógica también te parecerán obvios.

Entonces descubrirás que tú también, seas quien seas, tengas los conocimientos que tengas, no necesitas saber mucho para juzgar lo que es bueno y lo que no lo es, que no necesitas mucho para contribuir a dar forma a este mundo.

> "Pero entonces", dice Alicia, "si el mundo no tiene ningún sentido, ¿qué nos impide inventar uno?". Inspirado por Lewis Carrol

<!-- > "Toda innovación pasa por tres fases en la opinión pública: 1) Es ridícula 2) Es peligrosa 3) Es obvia". Cita apócrifa de Arthur Schopenhauer -->

## Eutopia

Puede que estés pensando que lo que proponemos aquí es una utopía.

Utopía es una palabra construida a partir del griego antiguo, derivada de "tópos" ("lugar"), con el prefijo "ou" ("no"), literalmente "ningún lugar"[^3].

Utopía es una palabra de la que a menudo se abusa. Se esgrime a cada paso para ahogar la esperanza, denigrar la imaginación, reprimir el progreso, es una llamada a la resignación, una palabra clave en el sistema de explotación, alienación y dominación para aceptar el statu quo[^4].

Pero un "no-lugar" es una sociedad que no existe, no una sociedad imposible. Y como podemos ver a lo largo de la historia, el futuro de la sociedad del mañana es siempre diferente y nuevo. Al final, la sociedad de mañana es siempre la utopía de hoy.

A veces se contrapone utopía a "científico". Pero esto demuestra ignorancia no sólo del griego, sino sobre todo de la propia naturaleza de la ciencia.

La búsqueda de lo inexistente, la búsqueda de lo realizable, el análisis de utopías realistas es, en efecto, una actividad científica. Para saber lo que es y lo que no es posible entre lo inobservable, hay que tener un conocimiento y una conciencia profundos de la naturaleza y la estructura del mundo.

Es una actitud científica la que adoptamos aquí, que consiste en cuestionar y poner en duda todo, en querer explicar las cosas en profundidad, en buscar las verdaderas causas, en practicar la duda y la crítica sistemática de las justificaciones recibidas, y en no aceptar nada como dado, evidente o inmutable. Esta forma de escribir nos llevará a cuestionar radicalmente nuestra sociedad. La ciencia consiste en abrir los ojos.

Así pues, el estudio de las sociedades posibles en relación con el estudio de las sociedades reales, presentes o pasadas, no está muy extendido. Algunas disciplinas y autores ni siquiera imaginan que pueda tener una dimensión profesional, o incluso rechazan fervientemente la idea. Tal vez estos intelectuales intenten justificar la limitación de su imaginación.

Nuestro sistema puede ser terriblemente malo, pero todos sabemos que no es el peor posible. Un cambio radical podría conducir a una sociedad aún más desagradable, y así, con este temor justificado, la masa de la población es conservadora o cautelosamente reformista. Sólo puede ser de otro modo si la gente puede formarse una idea bastante precisa y fiable de lo que podría ser el nuevo y mejor sistema.

Por esta razón, el análisis de otra sociedad posible debe ser suficientemente serio, reflexivo, preciso y completo. Y lo mismo cabe decir de las etapas de su aplicación, las etapas de transición de lo existente a lo nuevo.

Porque lo que creemos posible depende del análisis, la gente tiene que estar convencida del proyecto. Sólo entonces lo asumirán como el fin, haciendo de la marcha hacia una sociedad mejor una necesidad para la evolución de la suya propia. [^4]

> "Por supuesto, es analizando las sociedades que hemos construido como encontramos el conocimiento que necesitamos para construir otras; pero entonces tenemos que dejar de acumular ingredientes sin llegar a cocinar." Christophe Kolm

Así pues, en su definición actual tal y como la percibe nuestra sociedad, la utopía es un ideal, un concepto que no tiene en cuenta la realidad y que, por tanto, es inalcanzable.

Así que sí, en ese caso no crearemos una utopía aquí.

La realidad no es nuestra imaginación colectiva. La realidad consiste en umbrales que no podemos sobrepasar para no comprometer las condiciones favorables a nuestra supervivencia. Es la concentración máxima de CO2 en nuestra atmósfera, es el límite de perturbación de los ciclos bioquímicos del fósforo y del nitrógeno indispensables para el buen estado de los suelos y de las aguas, es la erosión máxima de la biodiversidad, es decir, la extinción de especies y de su papel esencial en los ecosistemas, es el uso intensivo de los suelos, el uso abusivo del agua dulce, la acidificación de los océanos, etcétera...[^5][^6][^7][^8][^9]

Esto forma parte de lo que constituye la realidad, lo que no es el caso, por ejemplo, del valor anunciado por las pequeñas cifras de la página de nuestra cuenta bancaria.

De hecho, estamos en plena utopía, creyendo que nuestra sociedad puede seguir viviendo así indefinidamente a largo plazo[^11].

Para diseñar el mundo ideal, tenemos que dejar atrás nuestro mundo imaginario, empezar desde abajo, para poder resolver los problemas en su origen. Porque así es como realmente podemos construir otro mundo que tenga sentido, otro mundo que sea realmente posible.

Lo que intentamos crear aquí es una Eutopía, un concepto similar al de utopía, pero que se considera alcanzable en la realidad. El prefijo "u" se convierte en "eu", que en griego significa "bueno" y, por tanto, literalmente "el lugar correcto"[^12].

Lo que guía a los seres humanos libres es su imaginación del futuro, lo posible, lo deseable, su utopía, Eutopía. Y como los seres humanos avanzan mucho más a menudo atraídos por la esperanza que impulsados por la desesperación, los sueños acaban creando la realidad. Nuestra esperanza es que la realidad se convierta en Eutopía.

> La utopía ha cambiado de bando; el utópico es la persona que cree que todo puede seguir como antes. Pablo Servigne y Raphaël Stevens[^11]

## Dar un paso atrás

Debemos cuestionar nuestra percepción del corto y el largo plazo. ¿Debemos definir 100 años como corto o largo plazo? Todo es cuestión de puntos de vista, pero necesitamos dar un paso atrás y alejarnos de la escala de nuestras cortas vidas.

La Tierra se formó hace 4.000 millones de años, la vida apareció hace 500 millones de años, el Hominia, el linaje de los seres humanos, se separó del linaje de los chimpancés hace 7.000 millones de años, pero de este linaje sólo hace 300.000 años que nació nuestra especie: el Homo Sapiens[^13].

Dentro de 5.000 a 7.000 millones de años, el Sol habrá agotado su helio y se transformará en una gigante roja, multiplicando su radio por 1.000 y atrayendo poco a poco a la Tierra hacia su halo ardiente. La temperatura de la Tierra será entonces tan elevada que los océanos se evaporarán. Pero la vida habrá desaparecido mucho antes por falta de oxígeno[^14].

En realidad, a los seres humanos aún les quedan 2.000 millones de años para vivir en un planeta habitable.

Es importante visualizar estas cifras en una escala para entenderlas con claridad.

2.000 millones de años son visualmente 2.000.000.000 de años.

Si consideramos una generación por cada 25 años, son otras 80.000.000 generaciones. Y actualmente sólo somos la 12.000ª.... Sólo ha transcurrido el 0,015% de nuestra vida en la Tierra.

Sin embargo, el fin de la Tierra no significa el fin de la especie humana, el universo es inmenso... Pero esta parte dejará espacio para otras historias imaginarias... Porque primero habría que aprender a sobrevivir en nuestro propio planeta antes de esperar poder sobrevivir en otros. Lo que no es el caso actualmente.

Así que sólo estamos al principio de nuestra historia, y si tenemos en cuenta el ritmo al que estamos consumiendo los recursos de nuestro planeta y la relación que tenemos con él y con todos sus ecosistemas, no podemos esperar aguantar mucho tiempo...

Al ritmo actual de producción mundial, habremos agotado todo el petróleo en 51 años, todo el gas en 53 años, todo el carbón en 114 años, todo el uranio en 100 años, y no podemos dar mucho más de sí para todas las tierras raras que son esenciales para las tecnologías actuales. [^11]¿Y esperamos durar 2.000.000.000?

De ahí la importancia de dar un paso atrás, mirar más allá de nuestras cortas vidas humanas y pensar a largo plazo. Volveremos sobre esto más adelante, pero es importante darse cuenta de que si estamos tan faltos de esta capacidad, es porque el cerebro del Homo Sapiens no está diseñado para pensar a largo plazo. [^15]

También te darás cuenta de que no es la Tierra lo que estamos tratando de salvar aquí, porque todavía tiene un futuro brillante por delante, con o sin nosotros, lo que estamos tratando de salvar aquí es, de hecho, la existencia humana, y con ella las diversas especies que componen nuestro ecosistema.

## Cuando los sueños se hacen realidad

Tal vez se pregunte cómo imaginar otro mundo podría ayudar a cambiar el actual.

Después de todo, podríamos decir que sería mejor dedicar nuestro tiempo y energía a cambiar lo que ya existe en lugar de imaginar uno nuevo.

Pero lo que ocurre no es independiente de lo que intentamos. Lo que intentamos depende de lo que creemos que es posible. Y lo que creemos que es posible depende de nuestro análisis.

Los principios y valores en los que se basan los fundamentos del mundo que te proponemos descubrir han sido muy poco desarrollados y analizados por los intelectuales. Este análisis te permitirá creer que esta visión es posible, y luego intentarlo. Y por tanto, en última instancia, a cambiar lo que ya existe. Este análisis debe ser lo más coherente posible para ganarse el apoyo del mayor número de personas y conducir a una acción colectiva, sensata y de largo alcance.

Al imaginar un mundo nuevo, descubrimos por comparación lo absurdo del actual, lo que nos lleva a cuestionar profundamente el mundo que nos rodea, pero también a nosotros mismos.

Al imaginar un mundo nuevo, creamos material, un espacio de reflexión y debate en la búsqueda de un ideal común.

Al imaginar un mundo nuevo, reunimos a la gente en torno a un ideal común, lo que les permite unirse para pasar a la acción y cambiar lo que ya existe.

Al imaginar un mundo nuevo, podemos inspirarnos en él para producir herramientas y principios, aplicarlos en la vida real, experimentar con ellos, iterar sobre ellos y mejorarlos.

Por último, al imaginar un mundo diferente, inspiramos sueños y esperanza, una emoción necesaria en la raíz de cualquier gran revolución.

Ya les hemos hablado de este don del ser humano, la capacidad de imaginar y contarnos historias, una facultad que nos ha permitido evolucionar exponencialmente desde nuestra revolución cognitiva hace 90.000 años.

Pues bien, ha llegado el momento de volver a cambiar nuestro relato para cambiar la historia. Necesitamos dejarnos llevar por un ideal común, cambiar nuestra narrativa, modificar nuestras creencias para poder movilizarnos y colaborar para cambiar nuestro futuro.

> "A lo largo de la historia, las historias han sido los vehículos más poderosos para el cambio filosófico, ético y político... Así pues, es a través de las historias como podemos provocar una verdadera 'revolución'". Manual de resistencia contemporánea[^24]

Cuando leas esto, no debes decirte a ti mismo "Eso es imposible" - probablemente tengas razón, lo es en las condiciones actuales, y no nos proponemos aplicar esta historia ahora mismo. Pero debes decirte a ti mismo "Podría llegar a ser posible".

Lo que tienes que hacer entonces es preguntarte "¿Qué es posible ahora mismo para avanzar hacia este ideal?", y de este modo construir las etapas de tu transición lo mejor que puedas. Pero para ello, primero tienes que saber cuál es ese ideal y hacia dónde te diriges.

Luego sólo tienes que ver qué es posible hacer ahora mismo y qué nuevas posibilidades se abrirán a medida que trabajes para alcanzar otras.

Sin embargo, el objetivo aquí no es decirte qué hacer o cómo hacerlo, sino desarrollar una visión que te permita averiguarlo por ti mismo y te ayude a hacerlo con las herramientas adecuadas.

También es importante darse cuenta de que esta visión no está grabada en piedra; es algo que tú mismo construyes y que está diseñado para evolucionar con el tiempo a medida que progresas y descubres cosas nuevas.

Así que es posible que nuestro sistema esté condenado al colapso hagamos lo que hagamos. Parece atrapado en su inercia, destruyendo su entorno y fuera de control.

Hay muchos libros sobre teorías del colapso, basados en estudios muy serios, pero si tal colapso ocurriera, no pasaríamos de 7.000 millones a 1 ser humano, y aún habría suficiente para reconstruir una sociedad. El colapso no se producirá de la noche a la mañana, será gradual con ondulaciones, y siempre será posible cambiar de dirección durante el mismo. [^16]

En cualquiera de los dos casos, el trabajo realizado aquí es igual de interesante: o bien gracias a él conseguimos cambiar la sociedad a tiempo para evitar el colapso, o bien, mientras nos preparamos para resistir y poder seguir viviendo durante este colapso, preparamos los cimientos sólidos para iniciar una nueva sociedad.

Dado que el colapso no dejará espacio para una sociedad completamente nueva y lista para ser utilizada, serán los individuos en ese momento los que tendrán que construir los caminos y elaborar las direcciones a compartir juntos.

A partir de entonces, nuestra falta de preparación podría dejar el campo libre a asambleas verborreicas e impotentes. En la misma línea, los sistemas de autogestión no pueden establecerse sin haberlos probado antes, ya que ello conducirá casi con toda seguridad a numerosos fracasos. Si no se establece una toma de decisiones colaborativa y democrática en todos los niveles de la sociedad, la situación de emergencia podría dar paso a otros sistemas de dominación que no son deseables. Por eso es necesario explorar hoy estas nuevas vías. [^4]

A pesar de todo, seguimos convencidos de que podemos evitar el colapso. Crearía un desastre humanitario y ecológico aún más difícil de reparar, por lo que no podemos calificarlo de deseable, aunque dejara espacio para algo nuevo. Por tanto, haremos todo lo posible por encontrar y desarrollar soluciones para evitarlo. No olvidemos nunca que, aunque el sistema sea poderoso, nuestra imaginación colectiva es tan poderosa como él, porque es sobre esta misma imaginación sobre la que el sistema existe y resiste.

Por último, la construcción de esta visión, aunque no sea aceptada ni deseada, revela cómo podría ser el mundo. Puede hacer que la gente se dé cuenta de que simplemente están jugando a un juego gigantesco con reglas que crean incentivos, y que sus vidas, oportunidades y privilegios serían muy diferentes si cambiaran las reglas.

> "Si quieres construir un barco, no reúnas a tus hombres y mujeres para darles órdenes, explicarles cada detalle, decirles dónde encontrar cada cosa. Si quieres construir un barco, haz que tus hombres y mujeres anhelen el mar. Antoine de Saint Exupéry

## La lucha de las historias de transición

Hemos visto más arriba que es lo imaginario lo que crea lo real, y desde tiempos inmemoriales han sido las historias las que han hecho evolucionar nuestras sociedades. Nuestra época no es una excepción, y las personas en el poder no conciben el fin de nuestras sociedades, creen en una narrativa hipermoderna y en la construcción de la narrativa de la transición.

Imaginan un futuro en el que viviremos en ciudades artificiales atiborradas de tecnologías que, entre otras cosas, permitirán contrarrestar los estragos del cambio climático. Nuestra alimentación será cada vez más industrial, artificial y sintética. Estaremos inmersos en el consumo instantáneo apoyado por una economía de la información, o más bien de la vigilancia. Seríamos inquilinos de todo, ya no pediríamos nada, pero seríamos "felices".

Pero el relato hipermoderno y la construcción del relato de la transición se han roto. Esta modernidad basada en el ideal del progreso material infinito, correlativo al progreso social para todos, se ha hundido en una crisis multidimensional y se ha vuelto completamente disonante. La narrativa de la hipermodernidad ha explotado el ideal de progreso al tiempo que lo ha vaciado de su esencia social emancipadora. Es manipuladora y cuenta con el apoyo de los medios de comunicación, especialmente a través de la aparición de una novolengua. Las palabras son secuestradas de su significado original, vaciadas de su sustancia, orientadas según intereses ocultos. La hipermodernidad es hiperacumulación, hiperconsumo nacido del deseo insaciable por la publicidad invasiva. El hombre moderno, que lo tiene todo, es paradójicamente el más insatisfecho de los seres humanos. [^17]

La narrativa hipermoderna está sufriendo tres grandes fracasos que todo el mundo empieza a ver con claridad. El primero es su fracaso ecológico, y la promesa del capitalismo verde, es decir, el productivismo respetuoso con la vida, parece cada vez más inverosímil. El segundo fracaso es de naturaleza económica, con el acaparamiento y la desigualdad a todas las escalas, de la local a la mundial, aumentando exponencialmente, a pesar de una abundancia material excepcional. El tercer fracaso es ético: nuestras relaciones sociales se están transformando en relaciones comerciales, de las que todos esperan salir ganando, y todo se compra y se vende. Estos desequilibrios generan violencia y ya no son sostenibles.

Una narrativa de transición está emergiendo como alternativa a la narrativa hipermoderna, una narrativa que nos sumergirá en una nueva era, la de la noosfera. Pero aún está lejos de construirse y de ser deseable. Ese es el objetivo del proyecto Eutopía, construir esta nueva narrativa...

Entonces, ¿hay que entender con qué sueña realmente la gente? No hay que descuidar estos sueños. La imaginación es poderosa, y será el primer paso en el camino hacia la realización. Por ejemplo, ¿soñamos con sentirnos de vacaciones todo el tiempo, en todas partes? ¿Cuál es nuestro ideal colectivo? Tenemos que responder urgentemente a esta pregunta, para poder dar sentido a este movimiento y diseñar y aplicar nuevas políticas ahora.

> Si queremos construir un mañana mejor, las murallas de la fraternidad y la imaginación creativa tendrán que interponerse entre nosotros y el colapso. Cita modificada de Valérie Jousseaume[^17]

## Inteligencia colectiva

Nadie posee la verdad, pero juntos podemos al menos acercarnos a ella.

Desde luego, no se te pide que creas nada de lo que leas aquí. Al contrario, se te anima a que no creas nada, a que nunca pienses que posees la verdad y a que siempre mantengas alguna duda en tu interior.

Porque creer que tienes la verdad es impedirte buscarla y, por tanto, impedirte encontrarla.

Por eso no podemos imaginar este mundo nosotros solos: necesitamos el mayor número posible de personas. Para lograrlo, necesitamos utilizar la inteligencia colectiva y desarrollar sus herramientas.

Así que, si estás leyendo esto, en estos momentos estamos editando este texto mediante una herramienta llamada Git, que te permite versionar el progreso de esta historia. También te permite participar en la edición de partes de esta historia, enviándolas para que todos los colaboradores puedan dar su opinión, debatir y finalmente votar si se incorpora o no la modificación. Una vez aprobada la modificación, se muestra en línea justo donde la estás leyendo.

La idea es diseñar una plataforma enteramente dedicada a la edición colaborativa y democrática de esta historia. La herramienta será lo suficientemente modular como para poder utilizarse en muchos otros proyectos.

La aplicación se llama Ekklésiapp, una mezcla entre aplicación y Ekklesia, de la antigua palabra griega "asamblea", que viene directamente de las asambleas de ciudadanos de las antiguas ciudades griegas, entre ellas Atenas, cuna de la democracia. Aunque en aquella época la democracia sólo era accesible a la clase dirigente, veremos en el capítulo 5 "Cómo decidir juntos" lo imperfecta que sigue siendo y lo mucho que queda por hacer. [^18]

Ecclesiapp es una aplicación de toma de decisiones que puede utilizarse desde unas pocas personas hasta toda la población, y es lo suficientemente modular como para poder crear un número infinito de métodos diferentes de toma de decisiones, para adaptarla fácilmente al trabajo a realizar. Un método elegido puede modificarse fácilmente, de modo que podemos buscar continuamente un método que ofrezca los mejores resultados.

Este método es de libre acceso. Puede permitir a otros crear su propia utopía, distopía o eutopía. Y todo ello para desarrollar la investigación en este campo, que es necesaria y útil por las muchas razones antes mencionadas.

También permite a otras personas realizar otro tipo de trabajos de escritura colaborativa. Puede utilizarse para gestionar una organización, una empresa o cualquier otro proyecto y, por último, ofrece una verdadera herramienta democrática, devolviendo el poder a las bases, lo suficientemente eficaz como para acabar con los sistemas de dominación y jerarquía.

Pero, ¿cómo podemos ponernos de acuerdo sobre las reglas para publicar esta historia?

Ecclesia se inspira directamente en el proceso democrático de toma de decisiones que se pondrá en marcha en este mundo ideal. De hecho, el proceso democrático de toma de decisiones en este mundo ideal se inspirará en esta plataforma. Porque esta plataforma nos permitirá directamente poner a prueba nuestras ideas sobre el proceso de toma de decisiones colectivo y democrático adecuado, iterar sobre él hasta que encontremos el que todo el mundo esté de acuerdo en que conducirá a los mejores resultados. Así, no sólo darás tu opinión sobre una decisión, sino también sobre el proceso de toma de decisiones. A continuación se desvelará el actual proceso de toma de decisiones.

> "Solos vamos más rápido, juntos llegamos más lejos" Proverbio africano

## El proceso de redacción

En cuanto a las competencias necesarias para juzgar esta historia o participar en ella, no hay ninguna.

Si crees que el mundo es complicado, es complicado porque lo hemos hecho complicado, pero en realidad, cuando se parte de una buena base, todo resulta ser más sencillo y lógico de lo que parece.

Todo lo que nos parece complicado es en realidad una serie de elementos sencillos, sólo que algunas cosas necesitan más tiempo que otras para ser revisadas y asimiladas.

Al final, los grandes científicos son los que han sabido vislumbrar esta simplicidad del mundo.

No dudes de ti mismo: si no entiendes algo, es simplemente porque alguien no se ha tomado el tiempo de explicártelo a partir de las bases que conoces.

De hecho, es esencial empezar siempre desde cero. Es uno de los principios fundamentales de esta historia, y por eso nos tomaremos el tiempo de crear una base sólida en la parte 2, antes de describir Eutopía en detalle en la parte 3.

Este libro está pensado para que lo lea y lo entienda todo el mundo. Los intelectuales pueden encontrar este libro demasiado simplista, con un vocabulario poco rico, pero ése es el objetivo.

Porque todos tenemos que ser agentes del cambio, ¿cómo vamos a unirnos si lo que debería unirnos es ininteligible e incomprensible para alguien que no conozca a fondo tal o cual campo?

Aquí no pretendemos utilizar términos complicados ni giros largos y complejos, ni empleamos estos métodos para parecer más científicos, más inteligentes, y así crear potencialmente una falsa sensación de verdad en usted de esta manera.

Esta historia tiene que ser accesible, comprensible y directa. Pretende ser honesta con usted, y desprovista de cualquier uso de efectos psicológicos, le dejaremos plena libertad para creer o no en su contenido.

En la historia de la humanidad, cada producción de un autor no es más que la producción indirecta de una inteligencia colectiva.

Esta historia surge naturalmente de la búsqueda constante de la "verdad" que han llevado a cabo a lo largo de la humanidad los innumerables humanos que han contribuido a la evolución del pensamiento global. Cada uno de estos humanos nació en una época, en un momento del tiempo, en el que recibió conocimientos colectivos del pasado y se benefició de interacciones que le llevaron, con la ayuda de las habilidades, el estatus y el tiempo de que disfrutaba, a reunir y publicar nuevos pensamientos y conocimientos. Las referencias directas a estos individuos en nuestra narración estarán anotadas, pero evitaremos describir extensamente el proceso del que surgieron sus pensamientos, para aligerar esta narración, para hacerla accesible pero también para evitar individualizar demasiado estos descubrimientos, y para promover un nuevo paradigma: entender que toda creación de conocimiento y pensamientos en la historia de la humanidad ha sido, aunque sea indirectamente, sólo colectiva. Ahora queremos llevar esta inteligencia colectiva aún más lejos, con la ayuda de herramientas que faciliten estos mecanismos, como la utilizada en esta historia. A lo largo de esta historia, también notarás un cierto lenguaje que intenta salir de esta individualidad.

En nuestra opinión, Eutopía es el fruto de una inteligencia colectiva producida a lo largo de miles de años y no el fruto de individuos aislados a lo largo de estas eras, incluida la nuestra. Una imagen mental es la de ver a la humanidad como un único organismo que evoluciona con el tiempo, lleno de millones y miles de millones de células, que vive, muere y renace, en constante evolución y readaptación para encontrar un equilibrio en su entorno.

No obstante, las referencias a los autores originales aparecerán siempre anotadas en la parte inferior de la página, para que pueda obtener más información sobre estas personas y su obra. Pero también para poner a su disposición todas nuestras fuentes, de modo que las pruebas que presentamos puedan ser verificadas.

También se elaborará una segunda versión de esta historia, más rica y científica, que estará directamente vinculada a ésta para apoyar la argumentación de su contenido ante las instituciones académicas. Podrá acceder fácilmente a esta versión con sólo pulsar un botón cada vez que quiera saber más sobre la historia, las ideas y los argumentos de un tema concreto, y podrá profundizar en cualquier tema con sólo pulsar un botón.

A lo largo de su lectura se citarán muchos estudios, pero como norma general le recomendamos que sea prudente con las conclusiones de los estudios que lea, ya que es fácil influir en la ciencia. Algunos estudios pueden estar financiados por empresas o grupos de interés con motivaciones e intereses específicos que influirán en el estudio.

Los estudios pueden estar sujetos a errores de diseño, como sesgos de selección, errores de medición o errores estadísticos, que pueden dar lugar a resultados no representativos o incorrectos. La interpretación de los resultados puede ser subjetiva y depender de las creencias y prejuicios del autor del estudio. Es importante considerar los resultados en su contexto y examinar las pruebas en su conjunto. En general, las cifras pueden presentarse de diferentes maneras para apoyar un argumento concreto y, por tanto, llevar a conclusiones erróneas. [^19][^20][^21]

No se trata de rehuir la ciencia, sino al contrario, de confiar en ella, pero con ojo avizor, reconociendo que el conocimiento que se produce es cambiante y evolutivo, que nuevos estudios contradicen a los anteriores y que en el estado actual está influido en gran medida por el sistema económico que financia la investigación. [^22]

Además, sin entrar aquí en detalles, la metafísica cuestiona supuestos básicos sobre lo que existe y sobre cómo pueden categorizarse y comprenderse las entidades. En concreto, explora conceptos como la existencia del libre albedrío, la naturaleza de la mente en relación con el cuerpo y los fundamentos de la propia realidad. Aunque no cuestiona directamente los estudios científicos, la reflexión metafísica pone en tela de juicio los marcos conceptuales y los supuestos subyacentes a la investigación científica. Esto puede incluir reflexiones sobre lo que significa "replicar" un estudio, cómo entendemos la causalidad y qué significa que algo sea "real" o "verdadero" en un contexto científico, como el hecho de que el acto de observación no es neutral, sino que afecta al estado del objeto observado. En resumen, la metafísica cuestiona los fundamentos filosóficos de lo que damos por sentado en nuestra búsqueda del conocimiento, incluidas las ciencias[^23].

Por ello, esta obra no está, ni estará nunca, grabada en piedra; su vocación es ser llevada a cabo por la humanidad en su conjunto y actualizarse a lo largo de su evolución, por y para sí misma. Por tanto, siempre estará abierta a nuevas evidencias que puedan contradecir su contenido, incluidos sus fundamentos más profundos.

> La duda es el principio de la sabiduría. Aristóteles[^25]

## Conclusión de la introducción

Como habrás deducido, estás a punto de leer Eutopía, una historia más que necesaria para la humanidad. Es hora de que salgamos de nuestro estado primitivo, de que nuestra sociedad evolucione hacia una verdadera civilización, para evitar el desastre ecológico y humano que se avecina. El objetivo de esta historia es ser realizada por la humanidad en su conjunto, por sí misma y para sí misma, para devolverle por fin el sentido y la dirección, para asegurarle un futuro garantizado de libertad y felicidad.

> "Las tensiones y contradicciones del alma humana sólo desaparecerán cuando desaparezcan las tensiones entre las personas, las contradicciones estructurales del entramado humano. Entonces ya no será la excepción, sino la regla, que el individuo encuentre ese equilibrio físico óptimo que las sublimes palabras "Felicidad" y "Libertad" pretenden designar: a saber, el equilibrio duradero o incluso la armonía perfecta entre sus tareas sociales, la totalidad de las exigencias de su existencia social, por una parte, y sus inclinaciones y necesidades personales, por otra. Sólo cuando la estructura de las interrelaciones humanas se inspire en este principio, sólo cuando la cooperación entre las personas, la base misma de la existencia de cada individuo, tenga lugar de tal manera que todos los que trabajan codo con codo en la compleja cadena de tareas comunes tengan al menos la posibilidad de encontrar este equilibrio, sólo entonces podrán los hombres afirmar con un poco más de razón que son "civilizados". Hasta entonces, como mucho están inmersos en el proceso de civilización. Hasta entonces, tendrán que seguir repitiendo: "La civilización aún no se ha completado. Se está gestando". Nober Elias, La dinámica de Occidente".

## Fuentes (lista no exhaustiva)

[^1]: Yuval Noah Harari, Sapiens : Breve historia de la humanidad. 2011
[^2]: Sébastien Bohler, ¿Dónde está el sentido?/ 2020
[^3]: [Etimología de la palabra utopía](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, La buena economía: reciprocidad general. 1984
[^5]: [Concentración máxima de CO2 en la atmósfera](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Límite de perturbación de los ciclos bioquímicos del fósforo y el nitrógeno](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Erosión máxima de la biodiversidad](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Uso intensivo de la tierra](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Uso abusivo del agua dulce](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Acidificación de los océanos](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Cómo todo puede colapsar, Pablo Servigne & Raphael Stevens
[^12]: [Etimología de la palabra eutopía](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, La historia de la Tierra
[^14]: El futuro de la vida en la Tierra" de Peter D. Ward y Donald Brownlee
[^15]: Sébastien Bohler, El bicho humano. 2019
[^16]: Jared Diamond, Colapso. 2004
[^17]: Valérie Jousseaume, Plouc Pride: A new narrative for the countryside. 2021
[^18]: Serge Christhonne, ¿Son democráticas las elecciones? 1977
[^19]: Ben Goldacre, Mala ciencia. 2008
[^20]: [Por qué la mayoría de los resultados de investigación publicados son falsos](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [La crisis de la replicación](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documental : La Fábrica de la Ignorancia. 2020
[^23]: Jaegwon Kim, Fundamentos de la metafísica.
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[^25]: Aristóteles, Ética a Eudemo
