---
title: Dedicatorias
description:
published: true
date: 2024-03-07T21:15:26.470Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:09:19.311Z
---

Para el ideal que hay en cada uno de nosotros que sufre por un mundo que no es.

Por el sufrimiento de los que amamos, por nuestro propio sufrimiento, por el sufrimiento de los que no conocemos y por todos los que vendrán...

Esta obra está destinada a ser creada por la humanidad en su conjunto y a ser actualizada a lo largo de su evolución, por y para ella misma.
