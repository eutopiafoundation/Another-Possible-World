---
title: Home
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z
---

> Haga clic en el globo en la parte superior izquierda para cambiar el idioma.
> {.is-info}

Bienvenido a la historia colaborativa y democrática promovida por la fundación Eutopia.

Le aconsejamos que empiece a leer por la página de dedicatoria y siga después el orden de las páginas.

Puede que tengas la tentación de pasar directamente a la parte 2 para saber más sobre el mundo que te proponemos, pero las bases de la parte 1 son esenciales, porque son las que justifican lo posible, y sin ellas corres el riesgo de no ver más que lo imposible...

Por eso nos hemos concentrado principalmente en la parte 1, y por eso gran parte de la parte 2 aún no está terminada.

¿Cómo hacer posibles todas las ideas de la parte 2? Uno de los elementos clave es comprender al ser humano, lo que hicimos en el capítulo titulado "Homo Sapiens", pero también elegir el sistema económico adecuado, lo que hicimos en el capítulo titulado "Trabajar juntos", porque este sistema no sólo produce bienes y servicios, sino sobre todo seres humanos y las relaciones entre ellos.

Conscientes del tiempo y la motivación de que podemos disponer en nuestras vidas, le recomendamos en primer lugar estos capítulos, y también hemos elaborado resúmenes para todos los capítulos de nuestra historia. [Esto está disponible en los Apéndices](/es/4-Appendices/1Summaries)
