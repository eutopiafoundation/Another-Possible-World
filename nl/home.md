---
title: Home
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z
---

> Klik op de wereldbol linksboven om de taal te wijzigen.
> {.is-info}

Welkom bij het samenwerkende en democratische verhaal dat wordt gepromoot door de stichting Eutopia.

We raden je aan om te beginnen met lezen op de inwijdingspagina en daarna de volgorde van de pagina's te volgen.

Je kunt in de verleiding komen om meteen door te gaan naar deel 2 om meer te weten te komen over de wereld die we voorstellen, maar de basisprincipes in deel 1 zijn essentieel, omdat ze rechtvaardigen wat er mogelijk is en zonder deze loop je het risico dat je alleen het onmogelijke ziet...

Daarom hebben we ons voornamelijk geconcentreerd op deel 1 en is een groot deel van deel 2 nog niet af.

Hoe maken we alle ideeën in deel 2 mogelijk? Een van de sleutelelementen is het begrijpen van de mens, wat we hebben gedaan in het hoofdstuk "Homo Sapiens", maar ook het kiezen van het juiste economische systeem, wat we hebben gedaan in het hoofdstuk "Samenwerken", omdat dit systeem niet alleen goederen en diensten voortbrengt, maar vooral mensen en de relaties tussen hen.

Omdat we ons bewust zijn van de tijd en motivatie die we in ons leven kunnen hebben, raden we je deze hoofdstukken als eerste aan, en we hebben ook samenvattingen gemaakt voor alle hoofdstukken in ons verhaal. [Deze zijn beschikbaar in de Bijlagen (/nl/4-Appendices/1Summaries)
