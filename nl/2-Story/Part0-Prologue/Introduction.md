---
title: Inleiding
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Wie ben jij?

Je bent misschien geen groot socioloog, of een groot filosoof, of een groot econoom, of een groot iets anders, misschien ben je niet naar de universiteit geweest, misschien was je zelf slecht op school. Dat geldt voor velen van ons ook, maar ondanks onze zeer verschillende educatieve achtergronden, hebben we het vertrouwen om je hier een andere mogelijke wereld voor te stellen, een wereld die streeft naar het ideaal, en we nodigen je uit om met ons mee te doen.

Misschien vraag je je af waarom jij in een goede positie verkeert om dit te realiseren. En misschien voelt een deel van je bij het lezen van deze paragraaf al de drang om dit boek dicht te slaan om niet nog meer tijd te verspillen. Maar geef ons nog een paar alinea's om je te overtuigen.

## Verbeelding

Hoewel je geen groot expert bent op een van de belangrijkste gebieden van deze wereld, is er één ding waar je bijzonder begaafd in bent en dat is dromen. Dit vermogen om je voor te stellen is de grootste gave van de mens. We nodigen je hier uit om te dromen, maar vooral om anderen te helpen dromen.

Toen we begonnen, waren we een onbeduidende soort met zeer beperkte macht om te handelen. Wat het verschil maakte tussen onze soort, Homo Sapiens, en de anderen was ons vermogen om in grote aantallen samen te werken. [^1]

En dat hebben we te danken aan onze verbeeldingskracht, ons vermogen om verhalen te verzinnen en die aan onszelf te vertellen.

Dankzij collectieve mythen, die we ook "ficties" kunnen noemen, zijn we in staat geweest om samen te komen en samen te werken.

We hebben het vermogen om te geloven in dingen die niet bestaan, we hebben vele legenden, mythen en verhalen verzonnen, we hebben denkbeeldige lijnen getrokken op de wereld, naties en gebruiken gecreëerd, we hebben geld uitgevonden en het de grootste waarde gegeven in onze verbeelding.

Deze gave van verbeelding komt voort uit onze behoefte aan betekenis, onze behoefte om onze omgeving te voorspellen en te begrijpen om er beter in te kunnen overleven[^2].

We laten deze collectieve verbeelding ons hele leven beheersen, we laten het zelfs het lot van de mensheid als geheel beheersen. De kracht ervan is zodanig dat het ons ertoe brengt onze eigen omgeving te vernietigen, onze eigen ondergang te bewerkstelligen.

Maar dit alles is in feite slechts een collectieve verbeelding en houdt op te bestaan op het moment dat we allemaal besluiten er niet meer in te geloven.

In werkelijkheid is wat we gewoonlijk rede noemen vaker het feit dat we blijven geloven in en ons conformeren aan de regels van onze collectieve verbeelding.

## Reden

Zodra we besluiten om deze denkbeeldige wereld achter ons te laten, kunnen we eindelijk echte rede vinden.

Je hebt misschien het gevoel dat je er niet veel over weet, maar je zult ontdekken dat je niet veel hoeft te weten om de oplossingen te vinden en de keuzes te maken die ons allemaal naar een vervuld en zinvol leven zullen leiden.

Want de antwoorden op onze vragen zitten al in ieder van ons. Om ze te vinden hoeven we alleen maar onze collectieve verbeelding achter ons te laten, ons hart te openen en te luisteren. Het klinkt misschien gek, maar we zijn ervan overtuigd dat hier de echte reden ligt.

En omdat we al begonnen zijn met dit proces van deconstructie, is dat wat we je voorstellen om hier te ontdekken, een wereld die echt gebaseerd is op rede.

Een wereld waarvan de eenvoud, betekenis en logica ook voor jou vanzelfsprekend zullen lijken.

Je zult dan ontdekken dat ook jij, wie je ook bent, wat je kennis ook is, niet veel hoeft te weten om te beoordelen wat goed is en wat niet, dat je niet veel nodig hebt om bij te dragen aan de vormgeving van deze wereld.

> "Maar dan," zegt Alice, "als de wereld absoluut geen zin heeft, wat houdt ons dan tegen om er een uit te vinden?" Geïnspireerd door Lewis Carrol

<!-- > "Elke innovatie doorloopt drie fasen in de publieke opinie: 1) Het is belachelijk 2) Het is gevaarlijk 3) Het is duidelijk." Apocrief citaat van Arthur Schopenhauer -->

## Eutopie

Je denkt misschien dat wat we hier voorstellen een utopie is.

Utopia is een woord uit het Oudgrieks, afgeleid van "tópos" ("plaats"), met het voorvoegsel "ou" ("niet"), letterlijk "geen plaats"[^3].

Utopia is een woord dat vaak verkeerd wordt gebruikt en misbruikt. Het wordt te pas en te onpas gebruikt om hoop te smoren, verbeelding te kleineren, vooruitgang te onderdrukken, het is een oproep tot berusting, een sleutelwoord in het systeem van uitbuiting, vervreemding en overheersing om de status quo te accepteren.[^4].

Maar een "non-plaats" is een samenleving die niet bestaat, geen onmogelijke samenleving. En zoals we door de geschiedenis heen kunnen zien, is de toekomst van de samenleving van morgen altijd anders en nieuw. Uiteindelijk is de samenleving van morgen altijd de utopie van vandaag.

Utopia wordt soms vergeleken met "wetenschappelijk". Maar dit getuigt niet alleen van onwetendheid over het Grieks, maar vooral over de aard van wetenschap.

De zoektocht naar het niet-bestaande, de zoektocht naar het haalbare, de analyse van realistische utopieën is wel degelijk een wetenschappelijke activiteit. Om te weten wat er wel en niet mogelijk is onder het onwaarneembare, moet je een diepgaande kennis en bewustzijn hebben van de aard en structuur van de wereld.

Het is een wetenschappelijke houding die we hier aannemen, een houding die erin bestaat alles in vraag te stellen en in twijfel te trekken, de dingen grondig te willen verklaren, op zoek te gaan naar de echte oorzaken, te twijfelen en systematische kritiek uit te oefenen op ontvangen rechtvaardigingen, en niets als gegeven, vanzelfsprekend of onveranderlijk te aanvaarden. Het is in deze manier van schrijven dat we ertoe gebracht worden onze samenleving radicaal in vraag te stellen. Wetenschap gaat over het openen van onze ogen.

De studie van mogelijke samenlevingen in relatie tot de studie van feitelijke samenlevingen, heden of verleden, is dus niet erg wijdverspreid. Sommige disciplines en auteurs stellen zich niet eens voor dat dit een professionele dimensie zou kunnen hebben, of verwerpen het idee zelfs vurig. Misschien proberen deze intellectuelen het beperken van hun verbeelding te rechtvaardigen.

Ons systeem mag dan verschrikkelijk slecht zijn, maar we weten allemaal dat het niet het slechtst mogelijke is. Radicale verandering zou kunnen leiden tot een nog onaangenamere maatschappij, en dus is met deze gerechtvaardigde angst de massa van de bevolking conservatief of voorzichtig hervormingsgezind. Dat kan alleen anders als mensen zich een redelijk precies en betrouwbaar beeld kunnen vormen van wat het nieuwe en betere systeem zou kunnen zijn.

Daarom moet de analyse van een andere mogelijke samenleving voldoende serieus, doordacht, precies en volledig zijn. En hetzelfde geldt voor de fasen van de implementatie, de fasen van de overgang van het bestaande naar het nieuwe.

Omdat wat we voor mogelijk houden afhangt van de analyse, moeten mensen overtuigd worden door het project. Alleen dan zullen ze het als doel nemen en de mars naar een betere samenleving tot een noodzaak maken voor de evolutie van hun eigen samenleving. [^4]

> "Natuurlijk vinden we in de analyse van de samenlevingen die we hebben gebouwd de kennis die we nodig hebben om andere samenlevingen te bouwen; maar dan moeten we stoppen met het verzamelen van ingrediënten zonder ooit te koken." Christophe Kolm

Dus in de huidige definitie zoals die door onze samenleving wordt waargenomen, is utopie een ideaal, een concept dat geen rekening houdt met de werkelijkheid en daarom onbereikbaar is.

Dus ja, in dat geval zullen we hier geen utopie creëren.

De werkelijkheid is niet onze collectieve verbeelding. De realiteit bestaat uit drempels die we niet kunnen overschrijden om de omstandigheden die gunstig zijn voor onze overleving niet in gevaar te brengen. Het is de maximale concentratie van CO2 in onze atmosfeer, het is de limiet voor het verstoren van de biochemische cycli van fosfor en stikstof die essentieel zijn voor de goede conditie van bodems en wateren, het is de maximale erosie van biodiversiteit, dat wil zeggen het uitsterven van soorten en hun essentiële rol in ecosystemen, het is het intensieve gebruik van bodems, het misbruik van zoet water, de verzuring van oceanen, enzovoort...[^5][^6][^7][^8][^9]

Dit is een deel van wat de werkelijkheid uitmaakt, wat bijvoorbeeld niet het geval is met de waarde die wordt aangekondigd door de kleine cijfertjes op de pagina van onze bankrekening.

In feite zitten we midden in de utopie, in de overtuiging dat onze samenleving op de lange termijn voor onbepaalde tijd zo kan blijven leven[^11].

Om de ideale wereld te ontwerpen, moeten we onze denkbeeldige wereld achter ons laten, vanaf de grond beginnen, zodat we de problemen bij de bron kunnen oplossen. Want zo kunnen we echt een andere wereld bouwen die zinvol is, een andere wereld die echt mogelijk is.

Wat we hier proberen te creëren is een Eutopia, een concept dat lijkt op utopia, maar dat in de realiteit haalbaar wordt geacht. Het voorvoegsel 'u' wordt 'eu', wat in het Grieks 'goed' betekent, en dus letterlijk de 'juiste plaats'[^12].

Wat vrije mensen leidt is hun verbeelding van de toekomst, het mogelijke, het wenselijke, hun utopie, Eutopie. En omdat mensen veel vaker vooruit worden getrokken door hoop dan gedreven door wanhoop, creëren dromen uiteindelijk de werkelijkheid. Onze hoop is dat de werkelijkheid Eutopia wordt.

> De utopist is de persoon die gelooft dat alles bij het oude kan blijven. Pablo Servigne en Raphaël Stevens[^11]

## Een stap terug doen

We moeten onze perceptie van de korte en lange termijn in vraag stellen. Moeten we 100 jaar definiëren als korte termijn of lange termijn? Het is allemaal een kwestie van standpunt, maar we moeten een stap terug doen en de schaal van ons korte leven achter ons laten.

De aarde werd 4 miljard jaar geleden gevormd, het leven verscheen 500 miljoen jaar geleden, de Hominia, het geslacht van de mens, scheidde zich 7 miljard jaar geleden af van het chimpansee geslacht, maar uit dit geslacht werd pas 300.000 jaar geleden onze soort geboren: Homo Sapiens[^13].

Over 5 tot 7 miljard jaar zal de zon haar helium opgebruikt hebben en veranderen in een rode reus, waardoor haar straal 1000 keer zo groot wordt en de aarde geleidelijk in haar brandende halo wordt getrokken. De temperatuur van de aarde zal dan zo hoog zijn dat de oceanen zullen verdampen. Maar het leven zal al veel eerder verdwenen zijn door een gebrek aan zuurstof[^14].

In werkelijkheid heeft de mens nog 2 miljard jaar om op een bewoonbare planeet te leven.

Het is belangrijk om deze cijfers op een schaal te visualiseren om ze duidelijk te begrijpen.

2 miljard jaar is visueel 2.000.000.000 jaar.

Als we uitgaan van één generatie voor elke 25 jaar, dan zijn dat nog eens 80.000.000 generaties. En wij zijn momenteel slechts de 12.000ste.... Slechts 0,015% van ons leven op aarde is voorbij.

Het einde van de Aarde betekent echter niet het einde van de menselijke soort, het universum is uitgestrekt... Maar dit deel laat ruimte voor andere denkbeeldige verhalen... Want het zou eerst tijd zijn om te leren overleven op onze eigen planeet voordat we hopen te kunnen overleven op anderen. Wat op dit moment niet het geval is.

Dus we staan nog maar aan het begin van onze geschiedenis, en als je bedenkt hoe snel we de grondstoffen van onze planeet verbruiken en de relatie die we hebben met onze planeet en al haar ecosystemen, kunnen we niet hopen dat we het nog lang volhouden...

In het huidige tempo van de wereldproductie zullen we alle olie in 51 jaar hebben opgebruikt, al het gas in 53 jaar, alle kolen in 114 jaar, al het uranium in 100 jaar, en we hebben niet veel tijd meer voor alle zeldzame aardmetalen die essentieel zijn voor de technologieën van vandaag. [^11]En we hopen 2.000.000.000 jaar mee te gaan?

Vandaar het belang om een stap terug te doen, verder te kijken dan onze korte mensenlevens en op lange termijn te denken. We komen hier later nog op terug, maar het is belangrijk om te beseffen dat als we zo'n gebrek hebben aan dit vermogen, dat komt omdat het brein van de Homo Sapiens niet ontworpen is om op lange termijn te denken. [^15]

Je zult ook merken dat het niet de Aarde is die we hier proberen te redden, want die heeft nog een mooie toekomst voor zich, met of zonder ons, wat we hier proberen te redden is inderdaad het menselijk bestaan, en daarmee de verschillende soorten die deel uitmaken van ons ecosysteem.

## Wanneer dromen werkelijkheid worden

Misschien vraag je je af hoe het verbeelden van een andere wereld kan helpen om de bestaande wereld te veranderen?

We zouden immers kunnen zeggen dat het beter zou zijn om onze tijd en energie te steken in het veranderen van wat er al is dan in het bedenken van een nieuwe.

Maar wat er gebeurt staat niet los van wat we proberen. Wat we proberen hangt af van wat we denken dat mogelijk is. En wat we denken dat mogelijk is, hangt af van onze analyse.

De principes en waarden waarop de fundamenten van de wereld die wij jullie voorstellen te ontdekken gebaseerd zijn, zijn heel weinig ontwikkeld en geanalyseerd door intellectuelen. Deze analyse zal jullie in staat stellen om te geloven dat deze visie mogelijk is en om het vervolgens te proberen. En dus, uiteindelijk, te veranderen wat al bestaat. Deze analyse moet zo coherent mogelijk zijn om de steun van zoveel mogelijk mensen te winnen en te leiden tot collectieve, verstandige en verreikende actie.

Door ons een nieuwe wereld voor te stellen, ontdekken we door vergelijking de absurditeit van de huidige, wat ons ertoe brengt de wereld om ons heen, maar ook onszelf, diepgaand in vraag te stellen.

Door ons een nieuwe wereld voor te stellen, creëren we materiaal, ruimte voor reflectie en debat op zoek naar een gemeenschappelijk ideaal.

Door ons een nieuwe wereld voor te stellen, brengen we mensen samen rond een gemeenschappelijk ideaal, zodat ze samen actie kunnen ondernemen en het bestaande kunnen veranderen.

Door ons een nieuwe wereld voor te stellen, kunnen we er inspiratie uit putten om tools en principes te produceren, ze in het echte leven toe te passen, ermee te experimenteren, ze te herhalen en ze te verbeteren.

Tot slot, door ons een andere wereld voor te stellen, inspireren we dromen en hoop, een noodzakelijke emotie die aan de basis ligt van elke grote revolutie.

We hebben met jullie gesproken over deze gave van de mens, het vermogen om zich voor te stellen en onszelf verhalen te vertellen, een vermogen dat ons in staat heeft gesteld om exponentieel te evolueren sinds onze cognitieve revolutie 90.000 jaar geleden.

Nu is het tijd om ons verhaal opnieuw te veranderen om de geschiedenis te veranderen. We moeten gedreven worden door een gemeenschappelijk ideaal, ons verhaal veranderen, onze overtuigingen aanpassen zodat we ons kunnen mobiliseren en samenwerken om onze toekomst te veranderen.

> Door de geschiedenis heen zijn verhalen de krachtigste middelen geweest voor filosofische, ethische en politieke verandering... Het is dus via verhalen dat we een echte 'revolutie' teweeg kunnen brengen". Een handboek voor hedendaags verzet[^24]

Als je dit leest, moet je niet tegen jezelf zeggen "Dat is onmogelijk" - je hebt waarschijnlijk gelijk, het is onder de huidige omstandigheden, en we stellen niet voor om dit verhaal nu toe te passen. Maar je moet tegen jezelf zeggen "Het zou mogelijk kunnen worden".

Wat je dan moet doen is jezelf afvragen "Wat is er op dit moment mogelijk om naar dit ideaal toe te werken?", en op die manier de fasen van je transitie zo goed mogelijk opbouwen. Maar om dat te doen, moet je eerst weten wat dat ideaal is en waar je naartoe gaat.

Dan hoef je alleen maar te kijken naar wat er nu al mogelijk is en welke nieuwe mogelijkheden zich zullen aandienen als je naar het bereiken van andere toewerkt.

Het doel hier is echter niet om je te vertellen wat je moet doen of hoe je het moet doen, maar om een visie te ontwikkelen die je in staat stelt om het zelf uit te vinden en je helpt om het te doen met de juiste hulpmiddelen.

Het is ook belangrijk om te beseffen dat deze visie niet in steen gebeiteld is. Je bouwt hem zelf op en hij is ontworpen om na verloop van tijd te evolueren naarmate je vooruitgang boekt en nieuwe dingen ontdekt.

Het is dus nog steeds mogelijk dat ons systeem gedoemd is om in te storten, wat we ook doen. Het lijkt vast te zitten in zijn inertie, zijn omgeving te vernietigen en geen controle meer te hebben.

Er zijn veel boeken over instortingstheorieën, gebaseerd op zeer serieuze studies, maar als zo'n instorting zou plaatsvinden, zouden we niet van 7 miljard naar 1 mens gaan en zou er nog steeds genoeg zijn om een samenleving opnieuw op te bouwen. De ineenstorting zal niet van de ene op de andere dag plaatsvinden, het zal geleidelijk gaan met golvingen, en het zal altijd mogelijk zijn om tijdens de ineenstorting van richting te veranderen. [^16]

In beide gevallen is het werk dat hier gedaan wordt net zo interessant: of we slagen er dankzij dit werk in om de samenleving op tijd te veranderen om ineenstorting te voorkomen, of we bereiden ons voor op veerkracht zodat we kunnen blijven leven tijdens deze ineenstorting en we bereiden de solide fundamenten voor om een nieuwe samenleving te beginnen.

Omdat de ineenstorting geen ruimte zal laten voor een gloednieuwe samenleving die klaar is voor gebruik, zullen het de individuen zijn die op dat moment de paden moeten bouwen en de richtingen moeten uitwerken die we samen moeten delen.

Vanaf dat moment zou onze onvoorbereidheid het veld wel eens open kunnen laten liggen voor breedsprakige en machteloze vergaderingen. In dezelfde geest kunnen zelfbeheersystemen niet worden opgezet zonder ze eerst uit te proberen, omdat dit vrijwel zeker zal leiden tot talloze mislukkingen. Als het niet lukt om op alle niveaus van de samenleving samenwerkende en democratische besluitvorming tot stand te brengen, kan dit vanwege de noodsituatie leiden tot andere systemen van overheersing die niet wenselijk zijn. Daarom moeten deze nieuwe wegen nu verkend worden. [^4]

Ondanks alles blijven we ervan overtuigd dat we instorting kunnen voorkomen. Het zou een humanitaire en ecologische ramp veroorzaken die des te moeilijker te herstellen zou zijn, daarom kunnen we het niet als wenselijk omschrijven, zelfs als het ruimte zou laten voor iets nieuws. Daarom zullen we er alles aan doen om oplossingen te vinden en te ontwikkelen om dit te voorkomen. Laten we nooit vergeten dat zelfs als het systeem machtig is, onze collectieve verbeelding net zo machtig is, want het is juist op deze verbeelding dat het systeem bestaat en zich verzet.

Tot slot onthult de constructie van deze visie, ook al wordt ze niet aanvaard en gewenst, hoe de wereld zou kunnen zijn. Het kan mensen doen beseffen dat ze gewoon een gigantisch spel spelen met regels die prikkels creëren, en dat hun leven, kansen en privileges er heel anders uit zouden zien als de regels veranderd zouden worden.

> "Als je een boot wilt bouwen, roep dan niet je mannen en vrouwen bij elkaar om ze orders te geven, om elk detail uit te leggen, om ze te vertellen waar ze alles kunnen vinden. Als je een boot wilt bouwen, laat je mannen en vrouwen dan verlangen naar de zee. Antoine de Saint Exupéry

## De strijd van overgangsverhalen

We zagen hierboven dat het de verbeelding is die de werkelijkheid creëert, en sinds mensenheugenis zijn het verhalen die onze samenlevingen hebben doen evolueren. Ons tijdperk vormt daarop geen uitzondering en de machthebbers denken niet aan het einde van onze samenlevingen, ze geloven in een hypermodern verhaal en in de constructie van het verhaal van de overgang.

Ze stellen zich een toekomst voor waarin we leven in kunstmatige steden volgestouwd met technologieën die het onder andere mogelijk zullen maken om de verwoestingen van de klimaatverandering tegen te gaan. Ons voedsel zou steeds industriëler, kunstmatiger en synthetischer worden. We zullen worden ondergedompeld in instant consumptie ondersteund door een informatie-economie, of beter gezegd een surveillance-economie. We zouden huurders van alles zijn, we zouden nergens meer om vragen, maar we zouden wel 'gelukkig' zijn.

Maar het hypermoderne narratief en de constructie van het narratief van de transitie zijn afgebroken. Deze moderniteit gebaseerd op het ideaal van oneindige materiële vooruitgang, gecorreleerd met sociale vooruitgang voor iedereen, is in een multidimensionale crisis verzonken en volledig dissonant geworden. Het verhaal van de hypermoderniteit heeft het ideaal van de vooruitgang uitgebuit en het tegelijkertijd ontdaan van zijn emancipatoire sociale essentie. Het is manipulatief en wordt ondersteund door de media, met name door de opkomst van een nieuwe taal. Woorden worden ontdaan van hun oorspronkelijke betekenis, ontdaan van hun substantie, georiënteerd volgens verborgen belangen. Hypermoderniteit is hyperaccumulatie, hyperconsumptie geboren uit verlangen dat onverzadigbaar wordt gemaakt door opdringerige reclame. De moderne mens, die alles heeft, is paradoxaal genoeg de meest ontevreden mens. [^17]

Het hypermoderne verhaal ondergaat drie grote mislukkingen die iedereen duidelijk begint te zien. De eerste is de ecologische mislukking en de belofte van een groen kapitalisme, d.w.z. een productivisme dat het leven respecteert, lijkt steeds ongeloofwaardiger. Het tweede falen is economisch van aard, met hamsteren en ongelijkheid op elke schaal, van lokaal tot mondiaal, die exponentieel toenemen, ondanks de uitzonderlijke materiële overvloed. De derde mislukking is van ethische aard: onze sociale relaties worden omgevormd tot commerciële relaties waaruit iedereen als winnaar tevoorschijn hoopt te komen, waarbij alles wordt gekocht en verkocht. Dit gebrek aan evenwicht leidt tot geweld en is niet langer houdbaar.

Als alternatief voor het hypermoderne verhaal ontstaat er een overgangsverhaal dat ons in een nieuw tijdperk zal storten, dat van de noosphere. Maar het is nog lang niet gebouwd, en het is nog lang niet wenselijk. Dat is het doel van het Eutopia-project, om dit nieuwe verhaal te bouwen...

Dus we moeten begrijpen waar mensen echt van dromen? Deze dromen mogen niet worden verwaarloosd. Verbeelding is krachtig en zal de eerste stap zijn op weg naar realisatie. Dromen we er bijvoorbeeld van om altijd en overal het gevoel te hebben dat we op vakantie zijn? Wat is dan ons collectieve ideaal? We moeten deze vraag dringend beantwoorden zodat we betekenis kunnen geven aan deze beweging en nu nieuw beleid kunnen ontwerpen en implementeren.

> Als we een betere toekomst willen opbouwen, zullen de wallen van broederschap en creatieve verbeelding tussen ons en de ondergang moeten staan. Gewijzigd citaat van Valérie Jousseaume[^17]

## Collectieve intelligentie

Niemand heeft de waarheid in pacht, maar samen kunnen we er tenminste dichter bij komen.

Er wordt je zeker niet gevraagd om alles wat je hier leest te geloven. Integendeel, je wordt aangemoedigd om niets te geloven, nooit te denken dat je de waarheid in pacht hebt en altijd enige twijfel in jezelf te houden.

Want geloven dat je de waarheid in pacht hebt, is jezelf ervan weerhouden ernaar op zoek te gaan, en daarmee jezelf ervan weerhouden haar te vinden.

Daarom kunnen we ons deze wereld niet alleen voorstellen - we hebben zoveel mogelijk mensen nodig. Om dat te bereiken, moeten we collectieve intelligentie gebruiken en haar instrumenten ontwikkelen.

Dus, als je dit leest, zijn we op dit moment deze tekst aan het bewerken met behulp van een tool genaamd Git, waarmee je de voortgang van dit verhaal in versie kunt bekijken. Het stelt je ook in staat om mee te werken aan het bewerken van delen van dit verhaal, ze in te sturen zodat alle bijdragers hun mening kunnen geven, kunnen debatteren en uiteindelijk stemmen over het al dan niet opnemen van de wijziging. Zodra de wijziging is goedgekeurd, wordt deze online weergegeven op de plek waar je het nu leest.

Het idee is om een platform te ontwerpen dat volledig gewijd is aan het gezamenlijk en democratisch bewerken van dit verhaal. De tool zal modulair genoeg zijn om in vele andere projecten gebruikt te worden.

De applicatie heet Ekklésiapp, een mix tussen applicatie en Ekklesia van het oude Griekse woord voor 'vergadering', dat rechtstreeks afkomstig is van de volksvergaderingen van oude Griekse steden, waaronder Athene, de bakermat van de democratie. Hoewel democratie in die tijd alleen toegankelijk was voor de heersende klasse, zullen we in hoofdstuk 5 "Hoe samen beslissen" zien hoe onvolmaakt het nog steeds is en hoeveel er nog moet gebeuren. [^18]

Ecclesiapp is een besluitvormingsapplicatie die gebruikt kan worden op de schaal van een paar mensen tot de hele bevolking, en is modulair genoeg om een oneindig aantal verschillende besluitvormingsmethoden te kunnen maken, om het gemakkelijk aan te passen aan het werk dat gedaan moet worden. Een gekozen methode kan gemakkelijk worden aangepast, zodat we voortdurend kunnen zoeken naar een methode die de beste resultaten oplevert.

Deze methode is vrij beschikbaar. Het kan anderen in staat stellen om hun eigen utopie, dystopie of eutopie te creëren. En dit alles om onderzoek op dit gebied te ontwikkelen, wat nodig en nuttig is om de vele redenen die hierboven zijn genoemd.

Het stelt andere mensen ook in staat om andere vormen van collaboratief schrijfwerk uit te voeren. Het kan gebruikt worden om een organisatie, een bedrijf of een ander project te beheren, en biedt uiteindelijk een echt democratisch hulpmiddel, dat de macht teruggeeft aan de basis, effectief genoeg om systemen van overheersing en hiërarchie af te schaffen.

Maar hoe kunnen we het eens worden over de regels voor het publiceren van dit verhaal?

Ecclesia is direct geïnspireerd door het democratische besluitvormingsproces dat in deze ideale wereld zal worden ingevoerd. Sterker nog, het democratische besluitvormingsproces in deze ideale wereld zal worden geïnspireerd door dit platform. Want dit platform zal ons direct in staat stellen om onze ideeën over het juiste collectieve en democratische besluitvormingsproces te testen, om het te herhalen totdat we het proces vinden waarvan iedereen het erover eens is dat het tot de beste resultaten zal leiden. Je geeft dus niet alleen je mening over een beslissing, maar ook over het besluitvormingsproces. Het huidige besluitvormingsproces wordt hieronder onthuld.

> "Alleen gaan we sneller, samen gaan we verder" Afrikaans spreekwoord

## Het schrijfproces

Er zijn geen vaardigheden nodig om dit verhaal te beoordelen of om eraan deel te nemen.

Als je denkt dat de wereld ingewikkeld is, dan is dat omdat wij hem ingewikkeld hebben gemaakt, maar in werkelijkheid blijkt alles eenvoudiger en logischer te zijn dan het lijkt als je uitgaat van een goede basis.

Alles wat voor ons ingewikkeld lijkt, is eigenlijk gewoon een reeks eenvoudige elementen. Sommige dingen hebben gewoon meer tijd nodig dan andere om te worden bekeken en geassimileerd.

Uiteindelijk zijn de grote wetenschappers degenen die een glimp hebben kunnen opvangen van deze eenvoud van de wereld.

Twijfel niet aan jezelf: als je iets niet begrijpt, komt dat gewoon omdat iemand niet de tijd heeft genomen om het je uit te leggen vanuit de basis die je kent.

In feite is het essentieel om altijd vanaf de basis te beginnen. Het is een van de basisprincipes van dit verhaal en daarom nemen we de tijd om een solide basis te leggen in deel 2, voordat we Eutopia in detail beschrijven in deel 3.

Dit boek is bedoeld om door iedereen gelezen en begrepen te worden. Intellectuelen zullen dit boek misschien te simplistisch vinden, met een woordenschat die niet rijk genoeg is, maar dat is juist de bedoeling.

Omdat we allemaal agenten van verandering moeten zijn, hoe kunnen we samenkomen als wat ons zou moeten verenigen onbegrijpelijk en onbegrijpelijk is voor iemand die geen diepgaande kennis heeft van dit of dat gebied.

We proberen hier geen ingewikkelde termen of lange, ingewikkelde zinswendingen te gebruiken, noch gebruiken we deze methoden om wetenschappelijker of intelligenter over te komen en op die manier mogelijk een vals gevoel van waarheid bij je te creëren.

Dit verhaal moet toegankelijk, begrijpelijk en to the point zijn. Het is bedoeld om eerlijk tegen je te zijn, en verstoken van elk gebruik van psychologische effecten, laten we je volledig vrij om de inhoud al dan niet te geloven.

In de geschiedenis van de mensheid is elke productie van een auteur slechts de indirecte productie van een collectieve intelligentie.

Dit verhaal komt natuurlijk voort uit de constante zoektocht naar de "waarheid" die door de hele mensheid is uitgevoerd door de ontelbare mensen die hebben bijgedragen aan de evolutie van het wereldwijde denken. Deze mensen werden elk geboren op een moment, op een moment in de tijd, waarop ze collectieve kennis uit het verleden ontvingen en profiteerden van interacties die hen ertoe brachten om, met behulp van de vaardigheden, status en tijd die ze genoten, nieuwe gedachten en kennis te verzamelen en te publiceren. De directe verwijzingen naar deze individuen in ons verhaal zullen worden geannoteerd, maar we zullen het proces waaruit hun gedachten voortkwamen niet uitvoerig beschrijven, om dit verhaal lichter en toegankelijker te maken, maar ook om deze ontdekkingen niet te veel te individualiseren, en om een nieuw paradigma te bevorderen: begrijpen dat alle creatie van kennis en gedachten in de geschiedenis van de mensheid, zelfs indirect, alleen maar collectief is geweest. We willen deze collectieve intelligentie nu nog verder brengen, met behulp van hulpmiddelen om deze mechanismen te vergemakkelijken, zoals het mechanisme dat in dit verhaal wordt gebruikt. Door dit verhaal heen zul je ook een bepaalde taal opmerken die deze individualiteit probeert te doorbreken.

In onze visie is Eutopia de vrucht van een collectieve intelligentie die in de loop van duizenden jaren tot stand is gekomen en niet de vrucht van geïsoleerde individuen in de loop van deze tijdperken, waaronder het onze. Eén mentaal beeld is dat van de mensheid als een enkel organisme dat in de loop der tijd evolueert, gevuld met miljoenen en miljarden cellen, die leven, sterven en herboren worden, zich voortdurend ontwikkelen en aanpassen om een balans te vinden in hun omgeving.

Verwijzingen naar de oorspronkelijke auteurs worden echter altijd onderaan de pagina vermeld, zodat je meer te weten kunt komen over deze personen en hun werk. Maar ook om al onze bronnen beschikbaar te maken, zodat het bewijs dat we naar voren brengen kan worden geverifieerd.

Er zal ook een tweede, rijkere en meer wetenschappelijke versie van dit verhaal worden ontwikkeld, die direct aan dit verhaal zal worden gekoppeld om de argumentatie van de inhoud ervan met academische instellingen te ondersteunen. Deze versie zal gemakkelijk toegankelijk zijn met een klik op een knop wanneer je meer wilt weten over de geschiedenis, gedachten en argumenten over een bepaald onderwerp, en je zult in staat zijn om dieper in te gaan op een bepaald onderwerp terwijl je doorklikt.

Veel studies zullen worden geciteerd tijdens het lezen, maar als algemene regel raden we je aan om voorzichtig te zijn met de conclusies van de studies die je leest, omdat het gemakkelijk is om de wetenschap te beïnvloeden. Sommige onderzoeken kunnen worden gefinancierd door bedrijven of belangengroepen met specifieke motivaties en belangen die het onderzoek beïnvloeden.

Studies kunnen onderhevig zijn aan ontwerpfouten, zoals selectiebias, meetfouten of statistische fouten, die kunnen leiden tot niet-representatieve of onjuiste resultaten. De interpretatie van resultaten kan subjectief zijn en afhangen van de overtuigingen en vooroordelen van de auteur van het onderzoek. Het is belangrijk om de resultaten in hun context te beschouwen en het bewijsmateriaal in zijn geheel te bekijken. In het algemeen kunnen cijfers op verschillende manieren worden gepresenteerd om een specifiek argument te ondersteunen, en daardoor tot verkeerde conclusies leiden. [^19][^20][^21]

Het idee is niet om de wetenschap te schuwen, maar juist om erop te vertrouwen, maar met een waakzaam oog, in het besef dat de geproduceerde kennis in beweging is en evolueert, dat nieuwe studies eerdere tegenspreken, en dat het in de huidige staat grotendeels wordt beïnvloed door het economische systeem dat onderzoek financiert. [^22]

Bovendien, zonder hier in detail te treden, stelt de metafysica basisaannames in vraag over wat er bestaat en hoe entiteiten kunnen worden gecategoriseerd en begrepen. In het bijzonder onderzoekt het concepten zoals het bestaan van de vrije wil, de aard van de geest in relatie tot het lichaam en de fundamenten van de werkelijkheid zelf. Hoewel het wetenschappelijke studies niet direct uitdaagt, stelt metafysische reflectie de conceptuele kaders en aannames die ten grondslag liggen aan wetenschappelijk onderzoek in vraag. Dit kan reflecties omvatten over wat het betekent om een onderzoek te 'repliceren', hoe we causaliteit begrijpen en wat het betekent voor iets om 'echt' of 'waar' te zijn in een wetenschappelijke context, zoals het feit dat de handeling van het observeren niet neutraal is, maar de toestand van het geobserveerde object beïnvloedt. Kortom, metafysica stelt de filosofische grondslagen in vraag van wat we als vanzelfsprekend beschouwen in onze zoektocht naar kennis, ook in de wetenschappen[^23].

Daarom is dit werk niet in steen gebeiteld en zal het dat ook nooit worden; zijn roeping is om uitgevoerd te worden door de mensheid als geheel en om bijgewerkt te worden tijdens zijn evolutie, door en voor zichzelf. Het zal daarom altijd openstaan voor nieuw bewijs dat de inhoud ervan kan tegenspreken, inclusief de diepste fundamenten.

> Twijfel is het begin van wijsheid. Aristoteles[^25]

## Conclusie van de inleiding

Zoals je hebt begrepen, sta je op het punt om Eutopia te lezen, een verhaal dat nu meer dan noodzakelijk is voor de mensheid. Het is tijd voor ons om onze primitieve staat achter ons te laten, om onze samenleving te laten evolueren naar echte beschaving, om de ecologische en menselijke ramp die dreigt te voorkomen. Het doel van dit verhaal is om gerealiseerd te worden door de mensheid als geheel, door zichzelf en voor zichzelf, om het eindelijk weer zin en richting te geven, om een gegarandeerde toekomst van vrijheid en geluk te waarborgen.

> De spanningen en tegenstrijdigheden van de menselijke ziel zullen pas verdwijnen als de spanningen tussen mensen, de structurele tegenstrijdigheden van het menselijk netwerk, verdwijnen. Dan zal het individu niet langer de uitzondering, maar de regel zijn om dat optimale fysieke evenwicht te vinden waar de sublieme woorden "Geluk" en "Vrijheid" op doelen: namelijk het blijvende evenwicht of zelfs de perfecte harmonie tussen zijn sociale taken, het geheel van de eisen van zijn sociale bestaan enerzijds, en zijn persoonlijke neigingen en behoeften anderzijds. Pas wanneer de structuur van menselijke interrelaties geïnspireerd is door dit principe, pas wanneer samenwerking tussen mensen, de basis van ieders bestaan, zo plaatsvindt dat allen die hand in hand werken aan de complexe keten van gemeenschappelijke taken tenminste de mogelijkheid hebben om dit evenwicht te vinden, pas dan zullen mensen met iets meer reden kunnen beweren dat ze "beschaafd" zijn. Tot die tijd zijn ze hoogstens bezig met het beschavingsproces. Tot die tijd zullen ze moeten blijven herhalen: "De beschaving is nog niet voltooid. Ze is in de maak! Nober Elias, De dynamiek van het Westen".

## Bronnen (niet-limitatieve lijst)

[^1]: Yuval Noah Harari, Sapiens : Een korte geschiedenis van de mensheid. 2011
[^2]: Sébastien Bohler, Waar is de zin?/ 2020
[^3]: [Etymologie van het woord utopie](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, De goede economie: algemene wederkerigheid. 1984
[^5]: [Maximale concentratie CO2 in de atmosfeer](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Grens van verstoring van de biochemische cycli van fosfor en stikstof](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Maximale erosie van biodiversiteit](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Intensief landgebruik](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Misbruik van zoet water](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Verzuring van de oceanen](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Hoe alles kan instorten, Pablo Servigne & Raphael Stevens.
[^12]: [Etymologie van het woord eutopie](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, De geschiedenis van de aarde.
[^14]: De toekomst van het leven op aarde" door Peter D. Ward en Donald Brownlee
[^15]: Sébastien Bohler, De menselijke bug. 2019
[^16]: Jared Diamond, Collapse. 2004
[^17]: Valérie Jousseaume, Plouc Pride: een nieuw verhaal voor het platteland. 2021
[^18]: Serge Christhonne, Zijn verkiezingen democratie? 1977
[^19]: Ben Goldacre, Slechte wetenschap. 2008
[^20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [The Replication Crisis](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documentaire : De Fabriek van Onwetendheid. 2020
[^23]: Jaegwon Kim, Grondslagen van de metafysica.
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018.
[^25]: Aristoteles, Ethica aan Eudemus
