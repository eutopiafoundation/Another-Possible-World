---
title: Inwijdingen
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Voor het ideaal in ieder van ons dat lijdt onder een wereld die dat niet is.

Voor het lijden van hen die we liefhebben, ons eigen lijden, het lijden van hen die we niet kennen en al diegenen die nog komen...

Dit werk is bedoeld om gecreëerd te worden door de mensheid als geheel en om bijgewerkt te worden tijdens haar evolutie, door en voor haarzelf.
