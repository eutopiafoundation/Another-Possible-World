---
title: Verbeelding van een andere wereld
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Hoe stel je je een nieuwe wereld voor, een ideaal, Eutopia?

## Het systeem, zijn beperkingen en stimulansen

Als we ons momenteel in een impasse lijken te bevinden met betrekking tot klimaatverandering en de andere uitdagingen van onze tijd, dan komt dat omdat de fundamenten van ons systeem beperkingen en prikkels genereren die uiteindelijk elke effectieve verandering onmogelijk maken zonder de gevestigde samenhang te verstoren, waardoor onze samenleving vervolgens wordt meegesleurd in een destructieve inertie die niemand lijkt te kunnen stoppen.

Bij elke verkiezing proberen onze politici ons te overtuigen van hun voorstellen en op het eerste gezicht denken ze allemaal dat ze het juiste doen, ze denken dat ze de oplossing hebben die het dagelijkse leven van de mensen die ze verdedigen kan verbeteren. Maar ze vechten onderling met deze beperkingen en prikkels, en die kunnen niet leiden tot een duurzame en wenselijke oplossing. Dus, machteloos tegenover het systeem, hebben ze geen andere keuze dan zich erdoor te laten regeren.

Voor zover we die ooit echt hebben gehad, zijn wij mensen onze macht kwijtgeraakt, maar erger nog, degenen die we kiezen om ons te vertegenwoordigen zijn die macht ook kwijtgeraakt.

We denken dat het politieke systeem domineert en dat het al het andere kan veranderen, inclusief de economie en de vorm daarvan. Dat we in een democratie leven vanwege de competitie tussen de kandidaten waarop we stemmen.

Maar een economisch systeem produceert niet alleen goederen en diensten, het bepaalt de voorwaarden van onze socialiteit, produceert mensen en de relaties tussen hen. [^1]

We zullen zien dat de economie zelfs ingrijpt in deze concurrerende democratische keuze. Dat ze die beïnvloedt en dat, als gevolg daarvan, het electorale systeem niet langer de kwaliteit heeft om goede concurrentie tussen kandidaten te bieden. Uiteindelijk is het dus niet de politiek die de economie domineert, maar de economie die de politiek domineert. [^2]

Wat we hier voorstellen is een schoonmaak van ons systeem, een volledig afscheid van alles wat onze huidige collectieve verbeelding uitmaakt. We nodigen jullie uit om vanaf een nieuwe basis te beginnen, om een Nieuwe Wereld te ontdekken die heel anders is dan de wereld die jullie kennen, een wereld die gebaseerd is op betekenis en logica, en om vervolgens samen de paden en oplossingen uit te werken voor een overgang daar naartoe.

## Beginnen vanaf de grond

Dus wat is de basis?

De basis is de werkelijkheid, het is wat echt bestond aan het begin van het bestaan van de mensheid toen we onszelf nog geen denkbeeldige verhalen konden vertellen. De basis is alles wat overblijft zodra we deze collectieve menselijke verbeelding hebben uitgewist.

We hebben dus de aarde en alles wat daar deel van uitmaakt. In het bijzonder haar verschillende ecosystemen: een ecosysteem is een groep die gevormd wordt door een gemeenschap van levende wezens die in wisselwerking staan met hun omgeving, en deze levende wezens kunnen planten of dieren zijn. De planeet is een groot systeem waarin een oneindig aantal subsystemen op elkaar inwerken en elkaar dynamisch in evenwicht houden. Onder alle levende wezens op onze planeet zijn wij, Homo Sapiens, beter bekend als Mensen.

Wij zijn met 7,9 miljard mensen en er wordt voorspeld dat we met 11 miljard zullen zijn in 2100[^3]. We zijn nu bijna overal op onze planeet, en zelfs in dunbevolkte gebieden hebben mensen nog steeds een impact op het milieu en op ecosystemen[^4]. Het probleem is echter niet de grootte van de bevolking, maar de interacties die we hebben binnen de ecosystemen.

Beginnen bij de basis betekent dus allereerst dat we de basis moeten begrijpen. We moeten de wereld begrijpen en hoe die werkt. En omdat wij mensen daarin leven, moeten we ook begrijpen hoe wij werken. Een deel zal uitsluitend gewijd zijn aan de aarde en een ander deel zal gewijd zijn aan een diepgaand begrip van de mens.

Deze 2 onderzoeksvragen zijn erg breed en vereisen veel kennis, maar we hoeven niet op alle details in te gaan. We gaan ons concentreren op de essentie, deze verzamelen en samenvatten om de volgende vragen te beantwoorden.

## Onderzoeksvragen

Als de vaardigheden die nodig zijn om de wereld en de mensen te begrijpen eenmaal verworven zijn, hebben we een doel nodig in de vorm van een onderzoeksvraag die onze wens vastlegt naar wat we zoeken in een ideale wereld.

Deze onderzoeksvraag is erg belangrijk, omdat het een leidraad zal zijn voor het onderzoek waaruit onze oplossingen zullen voortvloeien. Verschillende onderzoeksvragen zullen tot verschillende resultaten leiden. De vragen moeten zo eenvoudig mogelijk, duidelijk en precies zijn, maar ze moeten wel de essentie van wat we willen vastleggen.

We zullen ons richten op onze soort, maar zoals hierboven vermeld, leven we in ecosystemen, en we zullen ontdekken wanneer we de wereld bestuderen dat we er direct mee verbonden zijn en dat we er dus afhankelijk van zijn om te overleven.

We zoeken het ideaal. Natuurlijk hangt het ideaal af van de waarden, overtuigingen, aspiraties en ervaringen van elk individu. We zouden daarom kunnen hopen dat het ideaal voor elk individu is om een bevredigend en evenwichtig leven te leiden dat voldoet aan hun behoeften en aspiraties.

Dat is waar we naar streven, dat ieder mens een bevredigend leven heeft gedurende zijn tijd op aarde. Maar vergeet niet dat er nog 80 miljoen generaties moeten komen, dus dit onderzoek is van toepassing op de huidige generaties, maar houdt ook rekening met al die komende generaties.

Met deze verschillende elementen in gedachten is hier de formulering van onze onderzoeksvraag:

> Hoe kunnen we alle huidige mensen en de 80 miljoen generaties die nog komen in staat stellen om van een bevredigend leven op Aarde te genieten?
> {.is-succes}

Ons grote collectieve project is daarom ervoor te zorgen dat alle mensen een bevredigend leven op Aarde hebben dat voldoet aan hun behoeften en aspiraties, zowel voor de huidige generaties als voor de 80 miljoen generaties die nog komen.

Als we eenmaal een goed begrip hebben van hoe de wereld en mensen werken, zullen we ontdekt hebben hoe mensen vervuld kunnen worden, hoe we zin kunnen geven aan hun bestaan en hoe ze op lange termijn kunnen leven in hun omgeving op Aarde.

Om dit onderzoek te voltooien, zullen we snel de verschillende tijdperken van de mensheid doorlopen, omdat het kennen van het verleden de basis en inspiratie biedt voor het interpreteren van het heden [^5] En we moeten het heden kennen om een mogelijke toekomstige transitie te bepalen. Want de best mogelijke samenleving is de best mogelijke evolutie van samenlevingen vanaf nu. [^1]

Om dit gemeenschappelijke doel te bereiken, zullen onze 7,7 miljard mensen 2 essentiële acties moeten uitvoeren: samenwerken en beslissen.

## Subvraag: Samenwerken

Zoals in elke samenleving zullen we moeten samenwerken om de goederen en diensten te leveren die we nodig hebben om te floreren. Onze eerste grote deelvraag zal daarom zijn :

> Hoe kunnen we mensen laten samenwerken om elkaar te voorzien van de goederen en diensten die we nodig hebben om dit gemeenschappelijke doel te bereiken?
> {.is-succes}

Dit is het gebied van de economie. Het is een van de grote problemen van ons systeem en waar de meeste beperkingen en prikkels vandaan komen. Door vanaf nul te beginnen en op zoek te gaan naar de best mogelijke economie, kunnen we de noodzakelijke fundamenten voor een ideale samenleving definiëren en de beperkingen en stimulansen vermijden die de realisatie ervan in de weg staan. Zoals we zullen ontdekken, produceert een economisch systeem niet alleen goederen en diensten, het produceert ook mensen en de relaties tussen hen, die net zo essentieel zijn voor hun vervulling.

## Subvraag: Beslissingen

Tenslotte zullen onze 7,7 miljard mensen, gedurende hun hele leven en de ontwikkeling van hun samenlevingen, keuzes moeten maken, overeenkomsten moeten sluiten en beslissingen moeten nemen op zowel kleine als grote schaal. Dit is onze tweede grote deelvraag:

> Hoe kunnen we samen beslissingen nemen en ervoor zorgen dat deze beslissingen ons in staat stellen om ons gemeenschappelijke doel te bereiken?
> {.is-succes}

Zoals je uit de vraag kunt opmaken, is het idee niet alleen om overeenstemming te bereiken, want wat heeft het voor zin om alle of een deel van de bevolking het eens te laten worden over een beslissing, en deze te laten valideren, als het ons uiteindelijk niet in staat stelt om ons gemeenschappelijke doel te bereiken.

Dit veld is de wetenschap van het nemen van beslissingen en het is van toepassing op vele praktijken, waaronder de politiek, waar we momenteel gebruik maken van representatieve democratie.

We zullen dieper ingaan op de verschillende mogelijkheden, rekening houdend met wat we geleerd hebben over hoe mensen en de wereld werken.

## Conclusie

Samenvattend beginnen we met het begrijpen van onze habitat, de aarde, hoe ze werkt, haar ecosystemen en onze plaats daarin. Daarna gaan we ons verdiepen in de mens, hoe hij functioneert en wat hij nodig heeft om tijdens zijn verblijf op aarde te floreren.

Deze kennis zal ons in staat stellen om effectief te reageren op de vraag hoe we mensen in staat kunnen stellen om beslissingen te nemen en samen te werken om het gemeenschappelijke doel van de duurzame ontwikkeling van ieder van hen te bereiken.

Het functioneren van de aarde in hoofdstuk 2, het begrip van de mens in hoofdstuk 3 en het overzicht van de tijdperken van de mensheid in hoofdstuk 4 kunnen saai lijken om te lezen als je al bekend bent met deze gebieden of als je in het begin niet te veel de diepte in wilt gaan. In dat geval raden we je aan om meteen door te gaan naar hoofdstuk 5 en 6, waar je een nieuwe manier van samenwerken en beslissingen nemen zult ontdekken. Deze 2 delen zijn essentieel omdat ze uitleggen hoe de samenleving die we ons gaan voorstellen mogelijk is door de fundamenten te leggen die haar coherent zullen maken. De hoofdstukken 2, 3 en 4 zijn echter niet minder belangrijk en hebben geleid tot de realisatie en rechtvaardiging van de oplossingen in de hoofdstukken 5 en 6. Als je wilt, zijn er samenvattingen gemaakt en [zijn hier beschikbaar](/en/4-Appendices/1Summaries)

> Kennis is macht. Hoe meer we de wereld en onszelf begrijpen, hoe meer we in staat zijn om de wereld ten goede te veranderen. Malala Yousafzai

## Bronnen (Niet-uitputtende lijst)

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, Zijn verkiezingen democratie?/ 1977
[^3]: [World Population Prospects 2022" gepubliceerd door de Verenigde Naties](https://desapublications.un.org/file/989/download)
[^4]: [IPCC-rapporten 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Plouc Pride: Een nieuw verhaal voor het platteland. 2021
