---
title: Planeet Aarde
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Deze pagina wordt nog geschreven, dus sommige delen kunnen ontbreken.
> {.is-info}

## Korte beschrijving van de aarde

### De juiste wereldkaart

[Gall-Peters projectie](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

De Peters projectie (of Gall-Peters projectie naar James Gall (1808-1895) en Arno Peters (1916-2002)) is een cartografische projectie die, in tegenstelling tot de Mercator projectie, toelaat rekening te houden met de werkelijke oppervlakte van de continenten. Maar plaatselijk behoudt deze projectie de hoeken niet, wat resulteert in de vervorming van de continenten, in tegenstelling tot de Mercator-kaart. [^1]

### Korte beschrijving van de verschillende ecosystemen

## De watercyclus

### Presentatie van de verschillende vormen van water op aarde

### Beschrijving van de waterkringloop en zijn interacties met de omgeving

### Gevolgen van menselijke activiteiten op de watercyclus

## Fotosynthese en zuurstofproductie

### Beschrijving van fotosynthese en de rol ervan bij de zuurstofproductie.

Sinds haar ontstaan heeft de aarde voortdurend informatie gecreëerd. Dankzij deze informatie is de geordende beweging van materie ontstaan, waardoor een uitzonderlijke diversiteit aan vormen, kleuren en beweging is ontstaan: het leven zoals wij dat kennen.

Van al deze beschikbare informatie is er één meer dan alle andere de drijvende kracht geweest: het is de informatie die gecodeerd is in de genen van planten die de mechanismen van fotosynthese dragen. Dit proces, dat specifiek is voor het plantenrijk, maakt het levende systeem bijzonder efficiënt en kent geen equivalent. Fotosynthese heeft zowel energetische als chemische krachten. Het vangt ruwe, immateriële energie op - lichtenergie - slaat deze op en herverdeelt deze uiterst gedetailleerd, op een moleculaire schaal van minder dan een nanojoule. Het maakt het ook mogelijk om verspreide minerale elementen te combineren tot materialen en moleculen, die zelf informatie dragen in hun interacties en energie in hun samenstelling.

Dit is een wonderbaarlijk fenomeen in termen van de natuurkundige wetten van energie. Terwijl de wetten van de fysica voorschrijven dat energie die door een bron wordt uitgezonden zich moet verspreiden en afbreken en steeds minder bruikbaar wordt, zijn planten in staat om deze energie te kanaliseren. Dankzij de informatie in haar genetische bibliotheek gaat de plant dus in tegen de natuurkundige wetten van energie, bekend als entropie. In plaats daarvan vormt ze een negatieve entropielus in het universum: materie wordt geordend en georganiseerd en energie wordt gekanaliseerd. Voordat het weer uiteenvalt op zijn reis door het leven. Vergeleken met de oneindig grote hoeveelheid materie in het universum is organische materie als kant: het kan elke vorm aannemen. Het is deze verzameling moleculen in de vorm van haken, katrollen, wielen, kabels, deuren, punten en kettingen die levende materie vormt[^2].

### Gevolgen van menselijke activiteiten op de fotosynthese en zuurstofproductie

## De koolstofcyclus

### Presentatie van de koolstofcyclus en de interactie met het milieu.

### Gevolgen van menselijke activiteiten op de koolstofcyclus en broeikasgassen

Bovendien veranderen de materialen en energieën die ons in staat stellen om zulke prestaties te leveren de globale thermostaat, die voornamelijk uit koolstof bestaat, en voegen ze broeikasgassen toe aan de atmosfeer wanneer ze worden gebruikt. En de atmosfeer warmt op. De kalksteen die de basis vormt van cement, pleister en kalk is een koolstofhoudend gesteente; olie, kolen en gas, die 80% van de energiebronnen vormen die we gebruiken of 100% van het bitumen waarop we ons voortbewegen, zijn koolstofhoudende gesteenten. Deze koolstof was niet aanwezig in de oorspronkelijke aardkorst, maar was geconcentreerd in de atmosfeer. Het werd overgedragen van de atmosfeer naar de aardkorst door levende organismen, die het absorbeerden en afzetten op de bodem van de oceanen of in de geologische lagen van land en lagunes. In slechts een paar decennia geven we koolstof vrij dat levende wezens honderden miljoenen jaren nodig hadden om te begraven. We destabiliseren een systeem waarvan het evenwicht aan de oorsprong ligt van de meeste van de huidige geëvolueerde levensvormen, waaronder het onze.[^3] [^4] We destabiliseren een systeem waarvan het evenwicht aan de oorsprong ligt van de meeste van de huidige geëvolueerde levensvormen, waaronder het onze.

## Biodiversiteit

### Presentatie van het belang van biodiversiteit voor het leven op aarde

### Gevolgen van menselijke activiteiten voor de biodiversiteit

Meer dan de helft van de bossen en wetlands op aarde is in een eeuw verdwenen [^2].

## Bodem

### Presentatie van het belang van de bodem voor het leven op aarde

### Gevolgen van menselijke activiteiten voor de bodem

Een derde van de bodems op aarde is aangetast. [^4]

## Natuurlijke hulpbronnen

### Presentatie van de verschillende natuurlijke hulpbronnen (water, lucht, bodem, enz.).

### Duurzaam beheer van natuurlijke hulpbronnen

### Duurzame alternatieven voor natuurlijke hulpbronnen

## Terrestrische en mariene ecosystemen

### Presentatie van de verschillende terrestrische en mariene ecosystemen en hun belang

### De invloed van menselijke activiteiten op ecosystemen

## Bronnen

[^1]: [Wikipedia : Peters projectie](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, De symbiotische economie, Actes sud | Colibri, 2017.
[^3]: [Voedsel- en Landbouworganisatie van de Verenigde Naties (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Voedsel- en Landbouworganisatie van de Verenigde Naties (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
