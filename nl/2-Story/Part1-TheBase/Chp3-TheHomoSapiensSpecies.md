---
title: De soort Homo Sapiens
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Homo Sapiens

De menselijke soort mag dan begiftigd zijn met een formidabele intelligentie en een steeds groeiend bewustzijn en technologische kennis, maar het is duidelijk dat wanneer we geconfronteerd worden met de huidige en toekomstige gevolgen van klimaatverandering, met cijfers om deze te onderbouwen, we niet in staat zijn om onze koers te wijzigen[^1].

Vandaar de noodzaak om de mens en zijn functioneren te begrijpen, want alleen op die manier kunnen we ervoor zorgen dat de omgeving die we in ons Eutopia creëren mogelijk is en dat we ons gedrag daarin kunnen voorspellen.

Inzicht in de menselijke 'zwakheden' stelt ons in staat om ons aan te passen en dienovereenkomstig hulpmiddelen te ontwikkelen, zodat we ze kunnen uitbuiten om een kracht te creëren.

We gaan de mechanismen op individueel niveau begrijpen, de verschillende fysieke en psychologische aspecten... Maar deze benadering is onvoldoende en gaat gepaard met een hele reeks globale en sociologische overwegingen.

<!-- ## Een korte geschiedenis van Homo sapiens -->

## De behoeften van Homo Sapiens

Laten we teruggaan naar onze onderzoeksvraag:

"Hoe kunnen we alle mensen in staat stellen om een bevredigend leven op aarde te hebben voor de volgende 80 miljoen generaties?"

We zijn uitgegaan van het principe dat een bevredigend leven voor ieder mens een bevredigend leven is dat voldoet aan zijn behoeften en aspiraties. Een behoefte is een waargenomen noodzaak, of die nu fysiek, sociaal of mentaal is.

### Classificatie van behoeften

Om ons te helpen bij ons onderzoek, beginnen we met de piramide van Abraham Maslow, geformuleerd in 1943. Deze classificeert menselijke behoeften in 5 stadia en biedt een interessante basis voor een overzicht van de belangrijkste behoeften van mensen[^2].

Merk op dat dit model vandaag in vraag wordt gesteld, omdat we volgens Maslow pas een hogere behoefte voelen als de vorige is bereikt. Maar deze behoeften zijn zeker niet piramidaal, en veel vooruitgang heeft nu aangetoond dat sociale behoeften net zo belangrijk zijn als fysiologische behoeften, wat niet betekent dat ze aan de basis van de piramide moeten staan, maar eerder dat ze alomtegenwoordig zijn op elk niveau, en dat iedereen ze allemaal tegelijk voelt[^3].

Maar in elk geval hoeven we niet precies te begrijpen welke behoefte belangrijker is dan een andere en in welke omstandigheden, omdat het doel hier is om ze allemaal zonder uitzondering te vervullen.

Hieronder zullen we de piramide voltooien door de theorie te actualiseren in het licht van ontwikkelingen in psychologische en neurowetenschappelijke kennis.

Pyramide van Maslow](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

Op het eerste niveau hebben we fysiologische behoeften, dat zijn de basisbehoeften die nodig zijn om het leven en het goed functioneren van het organisme in stand te houden, zoals eten, drinken, seksualiteit, slapen, ademen...

Het is de moeite waard om op te merken dat ons huidige systeem er totaal niet in slaagt om aan de basisbehoeften van de hele bevolking te voldoen. Vandaag de dag zijn er 25.0000 mensen gestorven van de honger, waaronder 15.000 kinderen jonger dan 5 jaar[^4]. Op dezelfde manier is seksualiteit vaak taboe, gestigmatiseerd, gediscrimineerd en ongeschoold, wat leidt tot tal van stoornissen, overdracht van ziekten, daden van vrijwilligheid, trauma's, geestelijke en lichamelijke gezondheidsproblemen, gebrek aan of verslaving aan pleziertjes, gebrek aan een gevoel van vrijheid en vertrouwen, die allemaal zorgen voor een algeheel gebrek aan welzijn met betrekking tot iemands seksualiteit voor een zeer groot deel van de bevolking.

Op de tweede verdieping hebben we de behoefte aan veiligheid. De veiligheid van het hebben van een schuilplaats, zoals een plek om te wonen, de veiligheid van het hebben van toegang tot gewenste middelen, zoals apparatuur, gereedschap, spelletjes... Fysieke veiligheid tegen geweld, agressie, enz. Morele en psychologische veiligheid, emotionele veiligheid en stabiliteit, medische/sociale veiligheid en gezondheidsveiligheid.

Ook hier merken we de verschillende tekortkomingen van onze samenleving op, zelfs in westerse landen, waar huisvestingszekerheid een echte uitdaging is voor veel mensen, vooral daklozen, die vaak te maken hebben met precaire en onmenselijke omstandigheden[^5]. De toegang tot middelen kan voor sommige mensen ook beperkt zijn, afhankelijk van hun sociaaleconomische status, wat hun welzijn en levenskwaliteit kan beïnvloeden. Op het gebied van medische en psychologische veiligheid is er een gebrek aan professionals in de geestelijke gezondheidszorg, waardoor het moeilijk is om toegang te krijgen tot zorg. Zelfs met sociale zekerheid in landen als Frankrijk kunnen de kosten van de zorg voor sommige mensen onbetaalbaar zijn, waardoor ze niet de zorg krijgen die ze nodig hebben[^6].

Het 3e niveau zijn de behoeften om erbij te horen, de behoefte om lief te hebben en geliefd te worden, om intieme relaties te hebben, om vrienden te hebben, om deel uit te maken van een samenhangende groep, om zich geaccepteerd te voelen, om zich niet alleen of afgewezen te voelen.

We leven in een maatschappij waarin individualisme hoogtij viert en/of menselijke relaties vaak worden verwaarloosd ten gunste van persoonlijk succes. Een groot deel van de bevolking voelt zich steeds eenzamer en verliest sociale banden. Sommigen worden afgewezen en geïsoleerd, wat leidt tot geestelijke gezondheidsproblemen zoals depressie en angst. [7] Ons sociaal model bevordert veralgemeende concurrentie en op eigenbelang gerichte of commerciële relaties als het enige relationele model, wat een ware cultuur van egoïsme bevordert.

Het vierde niveau vertegenwoordigt de behoefte aan waardering. Mensen moeten zich gewaardeerd en gerespecteerd voelen door anderen, zich erkend voelen voor hun prestaties en zich in staat voelen om te slagen. Deze behoeften worden vervuld door dingen te doen die door de maatschappij worden gewaardeerd, door doelen te stellen en ernaar toe te werken of door activiteiten te ondernemen die persoonlijke ontwikkeling mogelijk maken.

Het vijfde niveau is de behoefte aan zelfontplooiing. Dit is de behoefte om zichzelf te vervullen, om je persoonlijke potentieel te benutten en er het beste uit te halen. Het uit zich in het verlangen om je talenten en vaardigheden te ontwikkelen, om activiteiten te ondernemen die betekenis en voldoening geven en om jezelf te overtreffen.

In onze samenleving is het moeilijk om aan deze behoefte aan zelfontplooiing te voldoen, omdat de sociale druk en de hoge verwachtingen die aan de meeste individuen worden opgelegd het moeilijk kunnen maken om je passies en persoonlijke interesses na te streven. Onze zeer competitieve samenleving en haar vervreemding van het individu laat weinig tijd over om talenten en vaardigheden te verkennen en te ontwikkelen. Veel mensen worden ook uitgesloten van het systeem, vooral mensen met een handicap die er niet in passen omdat het niet aan hen is aangepast. [^9]

Deze behoefte aan zelfontplooiing werd in 2010 echter enigszins uitgedaagd door het werk van 4 psychologen, die een update van de evolutietheorie voorstelden. [^3] Dit verwijdert de behoefte aan zelfontplooiing om ruimte te maken voor drie behoeften die verband houden met de voortplantingsdrift: ontmoeting, continuïteit en nageslacht.

|     |     |
| --- | --- |

| Behoefte aan nageslacht: stelt de onvermijdelijke dood uit.
Behoefte aan continuïteit: Behoud van partner, status, etc. | Afweren van verlies | Behoefte aan ontmoeting: Behoud van partner, status, etc. | Afweren van verlies
| Behoefte aan ontmoeting: Afweren van concurrenten en sociaal isolement.
| Behoefte aan aanzien en status: erkend worden als uniek wezen.
| Behoefte aan verbondenheid: geliefd en geaccepteerd worden om wie je bent.
| Behoefte aan bescherming: onderdak, veiligheid, fysiek, familiaal, psychologisch, financieel, enz.
| Fysiologische behoeften: Ademen, drinken, eten, jezelf ontlasten, slapen, warm blijven.

\*Lijst van de zeven fundamentele onbewuste en gelijktijdige menselijke behoeften uit D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Deze behoeften zijn onbewuste, evoluerende behoeften die op duizend verschillende manieren tot uiting komen in het leven van elke persoon. Zo kan artistieke creativiteit net zo goed gebaseerd zijn op de behoefte om status te verwerven en sociaal erkend te worden als een uniek wezen als op de behoefte om partners te verleiden of om in de loop der tijd te overleven door een kunstwerk voor het nageslacht na te laten.

We hebben zojuist de 5/7 soorten behoeften besproken, maar we moeten opmerken dat tegen het einde van zijn leven, Abarham Maslow een 6e behoefte toevoegde, "zichzelf overtreffen". Deze bevindt zich bovenaan de piramide:

> "De volledig ontwikkelde mens die in de beste omstandigheden werkt, is geneigd gemotiveerd te worden door waarden die zijn 'zelf' overstijgen"[^9].

Het is interessant om te beseffen dat een vervuld mens die verder gaat dan zelfvervulling, volledig open zou staan om verder te gaan dan zijn individualiteit om zich in te zetten voor anderen.

Tot slot kunnen deze behoeften ook worden aangevuld met de reflecties van Hannah Arendt.

In onze menselijke conditie worden we gedreven tot een zoektocht naar eeuwigheid: de vita contemplativa. Een zoektocht die steeds moeilijker te bevredigen is in onze moderne samenlevingen, die niet meer in God geloven. We worden ook gedreven tot een zoektocht naar onsterfelijkheid, de vita activa. [^10]

De zoektocht naar onsterfelijkheid is wat een sterfelijk mens drijft om creatieve dingen te zeggen en te doen die verder gaan dan zijn eigen vitale behoefte, zijn eigen bestaan, en hem projecteren in de toekomst voorbij zijn eigen dood.

Oorspronkelijk werd in het tijdperk van de moderniteit de behoefte aan onsterfelijkheid sociaal erkend, bijvoorbeeld door het heldendom van soldaten, door titels en motto's, of door de moed en authenticiteit van compromisloos spreken: "Men denkt alleen, men spreekt alleen krachtig vanuit de diepte van zijn graf: dat is waar men zichzelf moet plaatsen, dat is waar men mensen moet aanspreken" (Diderot, 1782). Vandaag de dag vinden al deze titels, al deze woorden, al deze handelingen geen weerklank meer in onze samenleving. Hannah Arendt stelt dat de sociale onderdrukking van de zoektocht naar eeuwigheid buiten het publieke domein, gekoppeld aan de sociale onderdrukking van de zoektocht naar onsterfelijkheid, verklaart hoe "het moderne tijdperk - dat begon met een explosie van menselijke activiteit zo nieuw, zo rijk aan belofte - eindigde in de meest inerte, steriele passiviteit die de geschiedenis ooit heeft gekend. [^10][^11]

Emmanuel Todd maakt een vergelijkbaar punt. Hij stelt dat de overgang van "de mens die gelooft in de mens" naar "de mens die gelooft in God" een significante collectieve energie heeft gegenereerd. Het weggaan van de zoektocht naar eeuwigheid heeft de energie versterkt in de richting van de zoektocht naar onsterfelijkheid. Toen de kerk als sociaal kader instortte, ontwikkelde zich een nieuw seculier humanistisch ideaal. Dit was de immense hoop in de Nieuwe Mens die gedragen werd door de revolutionaire bewegingen van de negentiende eeuw. Aan de andere kant markeert de ineenstorting van het communistische sociale ideaal en het revolutionaire republikeinse ideaal het sociale einde van de zoektocht naar onsterfelijkheid. "De Mens die in de Mens gelooft verdwijnt op zijn beurt en wordt de Mens die helemaal nergens meer in gelooft, een nihilistisch, apathisch mens, zonder hoop of doel, die zijn existentiële leegte vult door middel van consumptie[^12].

Wat bevredigt onze behoefte aan nageslacht? Volgens Hannah Harendt is dat het werk. Een werk is datgene dat in staat is om een nageslacht achter te laten, met andere woorden een kind. Het betekent ook het planten van bomen waaronder we door het verstrijken van de tijd niet zelf schaduw kunnen komen halen. Het betekent iets doorgeven aan kinderen. Het betekent gedichten schrijven of stukken hout snijden. Het betekent je leven heldhaftig opofferen. Het betekent werk doen dat betekenis heeft. Het biedt innerlijke zekerheid en zelfbeheersing. Door hun acties tonen mensen hun unieke werk aan de wereld en geven ze een glimp van hun onsterfelijkheid. Het werk van elke persoon, privé gecreëerd en publiekelijk aangeboden, is wat van de menselijke wereld een wereld maakt. [^10]

Overlevingswerk is louter overleven. Volgens de filosoof is werk een vitale noodzaak die ons fysiologische bestaan in stand houdt. Werk is een enge dimensie van het menselijk leven waarover we geen echte macht hebben. Hannah Arendt betreurt het feit dat het collectieve levensproject is afgeremd door de ongelooflijke plaats die in moderne samenlevingen wordt ingenomen door werk voor levensonderhoud. Volgens haar moet werk naar de privésfeer worden verbannen en van secundair belang zijn. Werk zou centraal moeten staan in het openbare sociale leven.

Hannah Arendt's filosofische reflectie over het buitensporige belang van het werk van levensbehoeften en het verdwijnen van het werk van het nageslacht biedt veel verklarende verbanden met de hedendaagse situatie. Het helpt ons de verdwijning van burgers, vervangen door consumenten, te begrijpen. Het helpt ons om de absolute existentiële verwarring te begrijpen die werkloosheid kan veroorzaken in een maatschappij die het werk vergeten is en die publiekelijk enkel georganiseerd is rond het levensonderhoud. Het helpt ons om de houding van de jongere generaties ten opzichte van zelfvoorzienend werk en hun zoektocht naar betekenis in een vak te begrijpen. Tot slot stelt het ons in staat om de behoeftepiramide in de facetten van de mens te onderzoeken.

Vanuit fysiek oogpunt betekent overleven dat je je voortplant en dat de verschillende mensen kinderen krijgen. Vanuit spiritueel oogpunt betekent het eeuwig zijn. Vanuit sociaal oogpunt betekent onsterfelijk zijn dat je een spoor achterlaat van je tijd op aarde. Het streven naar onsterfelijkheid, zoals Hannah Arendt het begrijpt, is de sociale uitdrukking van de behoefte aan nageslacht die elk aspect van ons fysieke, psychologische, sociale en spirituele leven doordringt. In deze zin ontmoet afkomst transcendentie. We mogen niet vergeten dat voor Hannah Arendt, net als voor Kenrick Douglas en zijn collega's, de behoefte aan nageslacht twee noodzakelijkheden combineert: de intieme productie van een volstrekt uniek en persoonlijk werk, gecombineerd met een relatie tot de wereld die het kan manifesteren.

Deze multidimensionale behoefte aan nageslacht wordt volledig overschaduwd door onze functionalistische en utilitaire maatschappij. En toch is het hier, in deze handelingen die geen duidelijke economische of rationele betekenis hebben voor het dagelijks leven of de ontwikkeling van onmiddellijke voordelen, dat we betekenis, grootsheid vinden. Door een leven voor te stellen dat beperkt is tot vitale overleving en materiële ontwikkeling, heeft de huidige moderne samenleving geleidelijk de toegang afgesneden tot de bevrediging van de behoefte aan nageslacht. Het resultaat is een knagend gevoel van ontevredenheid en onvoldaanheid, in een maatschappij waar paradoxaal genoeg overvloed en plezier de boventoon voeren. Dit is waar de zoektocht naar betekenis om de hoek komt kijken. Deze zoektocht naar betekenis lijkt geen erkenning te vinden in de hypermoderne samenleving. [^11]

Hier zijn dus de fundamentele menselijke behoeften aangevuld met de reflecties van Hannah Arendt. De pijlen staan voor de verschillende dimensies van het menselijk lichaam volgens de oosterse geneeskunde. Deze voorstelling werd eerder gekozen dan de klassieke onderscheidingen van de moderne westerse geneeskunde, omdat ze het spectrum verbreedt en Hannah Arendt's vita comtemplativa omvat. De pijlen zijn gestippeld om aan te geven dat ze buiten het bereik van de huidige wetenschap vallen.

[Menselijke basisbehoeften aangevuld met reflecties van Hannah Arendt](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)].

### Intrinsieke en extrinsieke behoeften of verlangens

Er is een verschil tussen intrinsieke en extrinsieke behoeften of verlangens.

Intrinsieke behoeften zijn interne en persoonlijke motivaties die van binnenuit het individu komen.

Extrinsieke behoeften zijn externe motivaties die voortkomen uit de omgeving en het individu. Ze hebben te maken met het beeld dat het individu wil uitstralen naar anderen en zelfs naar zichzelf. Consumptie kan behoeften van beide soorten tegelijkertijd bevredigen en de meeste consumptie die intrinsieke behoeften bevredigt, heeft ook een extrinsieke dimensie.

Extrinsieke behoeften zijn nauw verbonden met verschillende sociale normen, status, ethisch gedrag en de collectieve verbeelding. Soms heeft de bevrediging van extrinsieke behoeften een nadeel ten opzichte van intrinsieke behoeften[^13].

Sociaal onderscheid en imitatie creëren extrinsieke verlangens.

Elke samenleving creëert extrinsieke wensen en behoeften.

In onze maatschappij worden deze verlangens voortdurend en krachtig gemanipuleerd: het onderwijs en de sociale omgeving brengen levensstijlmodellen voort, of heel vaak het gevoel dat meer beter is, en reclame creëert het ene verlangen na het andere[^14].

Ontevredenheid komt voort uit de kloof tussen verlangens en de vervulling ervan, of met andere woorden tussen aspiraties en prestaties.

Om deze ontevredenheid te beperken, zijn er twee mogelijke benaderingen: het verlangen verminderen of het bevredigen. De nadruk wordt vaak gelegd op het tweede middel, het bevredigen van het verlangen, maar het bestaan van het eerste middel wordt bijna volledig vergeten of verwaarloosd.

Wat we dus eerst moeten doen, is begrijpen hoe onze verlangens worden gevormd en hoe we ze kunnen beheersen. En dat is wat we gaan doen.

## Menselijke natuur

Maar eerst is het essentieel om de balans op te maken van de menselijke natuur, want dat is een punt dat regelmatig terugkomt in het debat. De sociale wetenschappen en neurowetenschappen proberen al lang onderscheid te maken tussen wat aangeboren en wat aangeleerd is, maar er is geen consensus over de menselijke natuur.

Terwijl de natuurwetenschappen meer dan een halve eeuw het utilitarisme leken te ondersteunen, is het nu mogelijk om deze hypothese tegen te spreken: de mens is niet alleen egoïstisch, maar ook niet perfect rationeel. Dit niet-utilitarisme van de mens is van toepassing op de meest primitieve levensvormen: samenwerking is vanaf de oorsprong ingeschreven en zou, samen met de survival of the fittest, een van de essentiële motoren van de evolutie zijn[^15].

Om te geloven dat onze evolutie uitsluitend gebaseerd is op inter-individuele competitie is een bevooroordeelde kijk op evolutie, terwijl de rol van samenwerking al aanwezig was in de Darwinistische evolutietheorie. [^16]

Maar wat de menselijke natuur ook mag zijn, de ontelbare voorbeelden uit de geschiedenis bewijzen dat ze alles kan worden: zowel de slechtste als de beste.[^17][^18] De menselijke natuur zou niets moeten rechtvaardigen over wat we willen worden. We kunnen zeker beslissen wat we willen worden in onze samenleving, omdat we eenvoudigweg de mogelijkheid hebben om dat te doen.[^19]

Wat wel zeker is, is dat mensen geen kluizenaars zijn; ze leven in de maatschappij en zijn sociaal. Mensen zijn in de eerste plaats homo sociabilis, met de neiging om dicht bij hun medeschepselen te komen en met hen te communiceren. Ze hebben behoefte om te praten, om informatie uit te wisselen, om kennis en emoties te delen, maar ze hebben ook een fundamentele behoefte om in de ogen van anderen te bestaan. Ze zoeken menselijke interactie voor de gezelligheid die het biedt en voor de uitwisseling van positieve en bevredigende beelden waarmee ze zich voeden. [^20][^21]

## Vormgeven aan onze verlangens

We zeiden eerder dat we eerst moeten begrijpen hoe onze verlangens gevormd worden en hoe we ze kunnen beheersen.

Al meer dan anderhalf miljard jaar hebben we een klein orgaan in onze schedel dat het striatum heet.

Het striatum geeft dopamine af, een stof die ons een gevoel van genot geeft en gedrag versterkt dat als een succes wordt beschouwd. [^22]

Het maakt deel uit van een heel mechanisme van samenwerking en dialoog tussen verschillende structuren en neemt deel aan de beslissing, ontvangt informatie en vergelijkt de verschillende opties, mogelijke winsten, vereiste inspanning, onmiddellijkheid van beloning.

Dit systeem wordt vaak het beloningscircuit genoemd, maar dopamine is slechts een hormoon dat de uitkomst van een actie voorspelt. [^23]

Deze dopamineontladingen geven aanleiding tot prikkels die ons tot actie aanzetten, en zullen dat gedrag of die activiteit in de toekomst versterken.

Deze bekrachtigers, die we ook diepe motivaties van onze hersenen kunnen noemen, zijn : [^22]

- voedsel
- geslacht
- Sociale positie
- Behoefte aan informatie
- Minste inspanning
- Intellectuele stimulatie

Deze motivaties waren essentieel voor onze overleving en de ontwikkeling van onze soort in primitieve tijden en zijn vandaag de dag nog steeds erg gevoelig. Neuronen in het striatum en in het beloningscircuit trekken ons aan tot wat onze bekrachtigers bevredigt, d.w.z. alles wat voedsel, seks, status, gemak en informatie oplevert.
Ze zijn niet intrinsiek schadelijk, maar zoals we vandaag de dag kunnen zien, kunnen ze worden uitgebuit om economieën te ontwikkelen die productivistisch, consumentistisch, individualistisch en destructief voor het milieu zijn.

Het bijzondere aan dit mechanisme, dat onze hersenen niet alleen conditioneert om voedsel, geld, seks en sociale status te zoeken, is dat het streeft naar steeds meer geld, steeds meer voedsel, steeds meer seks of steeds meer status.

Wanneer een rat in de buurt van een gang wordt geplaatst met een stuk kaas aan het einde, geven zijn neuronen dopamine af zodra hij de gang in gaat. De eerste keer dat hij een stuk kaas vindt aan het einde van de gang, geven zijn neuronen dopamine af; de tweede keer, als hij de kaas vindt, doen zijn dopamine-neuronen niets. Maar als hij twee stukken kaas vindt in plaats van één, zullen zijn dopamineneuronen weer ontladen. [^22]

Het striatum doet dit alles alleen voor zover het meer en meer kan doen.

Dus we kunnen onze genotscircuits alleen stimuleren door de doses te verhogen.

Een ander mechanisme is dat het plezier en gemak dat we ons nu kunnen veroorloven honderd keer meer gewicht heeft in onze beslissingen dan de overweging van een verre toekomst. Hoe verder in de toekomst een voordeel ligt, hoe minder waarde het voor onze hersenen heeft. Dit staat bekend als temporele devaluatie.

We geven de voorkeur aan onmiddellijk plezier boven toekomstig plezier, of aan het maken van een keuze die ons nu voordeel oplevert maar op de lange termijn schadelijke gevolgen zal hebben.

Hoe langer de vertraging, hoe zwakker de anticiperende respons[^24].

Er zijn zeker genetische verschillen tussen mensen, en sommigen zijn gevoeliger voor hun striatum, maar het zijn vooral elementen die verband houden met opvoeding en de sociaal-culturele omgeving die doorslaggevend blijken te zijn. [^25][^26]

Deze neuronen liggen eigenlijk aan de basis van onze leerprocessen

Ze conditioneren ons: als we eenmaal geleerd hebben dat eten volgt op een gebaar, wordt het gebaar zelf aantrekkelijk. Dit is het principe van sociaal leren: we worden geconditioneerd om dit of dat gedrag leuk te vinden. Het principe van operante conditionering heerst op een leerplek bij uitstek, de school.

Egoïsme en altruïsme worden beide evenzeer gemotiveerd door plezier.

Wanneer we iemand die we liefhebben een geschenk geven, komt het niet bewust in ons op om dit te doen om ons goed te voelen, en toch voelen we ons goed.

We kunnen ons dus afvragen, als altruïsme ons persoonlijk een goed gevoel geeft en we ons er bovendien bewust van zijn, is dit dan nog steeds altruïsme, of is het niet eerder een soort welgeplaatst eigenbelang?

Er is een dunne lijn tussen altruïsme en egoïsme, een lijn die op biologisch niveau niet eens bestaat. Er is geen altruïsmehormoon of egoïsmehormoon. Er is een handeling waarvan onze hersenen de waarschijnlijkheid berekenen om te weten of die nuttig voor ons zal zijn, en vervolgens ontlenen we er plezier aan. De betrokken gebieden in de hersenen zijn gemeenschappelijk, maar wat we zien is dat er een grotere activering is van de dorsolaterale prefrontale cortex bij egoïsme vanwege de meer uitgesproken evaluatie van kosten-batenberekeningen. [27] Maar over het algemeen zijn wij het die handelingen classificeren als altruïstisch of egoïstisch, en deze definitie kan dus variëren afhankelijk van de manier van denken.

We zullen zeggen dat in de egoïstische handeling er alleen het doel is om persoonlijk voordeel te behalen.

Bij de altruïstische handeling is het de bedoeling om voordelen aan anderen te geven en tegelijkertijd een voordeel te ontvangen dat onbewust wordt nagestreefd. Maar er kan ook een intentie zijn om voordeel aan een ander te geven, terwijl je een bewust gezocht voordeel ontvangt. [^28]

De altruïstische handeling is daarom een wederzijdse uitwisseling die beide kanten op gaat, terwijl de egoïstische handeling maar één kant op gaat.

Zowel egoïsme als altruïsme kunnen dus leiden tot individueel plezier, en we zullen ontdekken welke van deze 2 motivaties het meest effectief is in het bereiken van ons gemeenschappelijke doel.

Dus als sommige mensen vrijgeviger zijn, komt dat waarschijnlijk omdat hun hersenen van jongs af aan zo zijn ingesteld. Zoals Moeder Teresa, die bekend stond om haar niet aflatende toewijding om anderen te helpen, haar medeleven en haar onvoorwaardelijke liefde voor iedereen die ze ontmoette. Ze wijdde haar leven aan het goede doen voor anderen, zonder zich zorgen te maken over haar eigen veiligheid of comfort, en werd een van de belangrijkste symbolen van liefdadigheid en altruïsme over de hele wereld. Toen ze nog maar 6 jaar oud was, nam haar moeder haar mee om de allerarmsten, alcoholisten en wezen te bezoeken. En ze gaf haar een onveranderlijk en hardnekkig advies: "Mijn dochter neemt nooit een mondvol voedsel aan dat niet met anderen wordt gedeeld". Delen, regelmatig aangeleerd in de wieg en met een constante zorg voor toepassing en voorbeeldigheid, werd zo'n krachtige vorm van conditionering dat het striatum van Moeder Teresa nooit wankelde in de tachtig jaar van haar roeping. Haar omgeving en haar opvoeding hielpen dus om in haar waarden en motivaties te ontwikkelen zoals vrijgevigheid, mededogen en het verlangen om anderen te dienen[^29][^30] Natuurlijk speelt religie, door deze morele waarden van vrijgevigheid en altruïsme te bepleiten, een belangrijke rol in de configuratie van Moeder Teresa's hersenen, en dit komt voort uit onze zoektocht naar betekenis, die we later zullen bespreken.

Studies hebben ook het bestaan van een bonus voor mentale behendigheid en probleemoplossend vermogen aangetoond, wat leidt tot de activering van geheugenzones en een betere memorisatie van informatie die door de leraar wordt gecommuniceerd. Zo zal de dopamine die een leerling ontvangt wanneer hij een goed punt krijgt, de synaptische plasticiteit moduleren en herinneringen consolideren. [^31]

Natuurlijk is het striatum niet het enige deel van onze hersenen dat beslissingen neemt. De frontale cortex is het voorste deel van het menselijk brein en is in de loop van de evolutie steeds dikker geworden, wat onze toenemende cognitieve vaardigheden weerspiegelt. Het is de zetel van wilskracht en planning.

Toen deelnemers bijvoorbeeld werd gevraagd om te kiezen tussen onmiddellijk twintig euro of 30 euro over 2 weken, zagen degenen die het geld onmiddellijk aannamen het striatum oplichten, terwijl degenen die dat niet deden de frontale cortex zagen oplichten[^32].

Dus als we een menselijke gemeenschap willen vormen die in staat is om de uitdagingen van de toekomst aan te gaan, dan zijn dit de verbindingen die we moeten ontwikkelen door middel van hard werken, bewustzijn en doorzettingsvermogen.

## Onze verlangens beheersen

Om de schadelijke effecten van ons striatum te beperken, zijn er 3 mogelijke oplossingen,

- Ons striatum onderdrukken: Historisch gezien werd de activiteit van het striatum geblokkeerd door morele bevelen en door de inspanning van de wil tegen verleiding. Maar dit is geen oplossing: als we ons striatum onderdrukken, kunnen we ons vermogen om motivatie of plezier te voelen aantasten, wat kan leiden tot problemen zoals depressie en verschillende stoornissen. Repressie heeft negatieve gevolgen voor het individuele welzijn en functioneren en is daarom noch een effectieve noch een duurzame oplossing. [^33]

- Als we het striatum op zijn beloop laten, kunnen we inderdaad primaire bekrachtigers bespelen om een ideale samenleving te bereiken, bijvoorbeeld door het de sociale norm te maken om er een gezonde en duurzame levensstijl op na te houden. Stimuleer de ontwikkeling van de bekrachtigers die essentieel zijn voor een goede samenleving.

- Maar een effectievere en duurzamere oplossing is een beroep doen op het unieke vermogen van de mens, het bewustzijn. Want de kracht van het striatum komt voort uit het feit dat zijn commando's onbewust zijn.

Wanneer de neuronen van het striatum gewend zijn geraakt aan een bepaald sociaal niveau, worden ze afgestompt en voelen we niets meer. Dit proces van toename geeft geen blijvende voldoening, het kan geen geluk brengen.

De meeste van onze acties worden ondernomen met een zeer laag bewustzijnsniveau. Vaker wel dan niet handelen we mechanisch. Zelfs als we ons bezighouden met intellectuele activiteiten, zou het verkeerd zijn om te zeggen dat we dat bewust doen. [^34]

We hebben een hersenschors met een enorme rekenkracht, die we voornamelijk gebruiken voor utilitaire, prestatiegerichte en technische doeleinden. Al duizenden jaren wordt ons vermogen tot abstractie, conceptualisering en planning voornamelijk gebruikt om gereedschappen te ontwerpen die ons striatum bevredigen.

We zijn wezens met een hoog intelligentieniveau maar een laag bewustzijnsniveau. [^22]

Intelligentie ontwikkelt oplossingen, genereert berekeningen, voert doelstellingen en programma's uit. Maar het kan dit allemaal doen zonder bewustzijn. Het meest voor de hand liggende voorbeeld is kunstmatige intelligentie, die extreem complexe taken kan uitvoeren zonder enig bewustzijn.

De grote kunstmatige intelligentiesystemen die je vandaag de dag kent, werken ook met een notie van waarschijnlijkheid en een systeem van versterking, waarbij handelingen worden beloond of bestraft om het aanleren van gewenst gedrag aan te moedigen. Met andere woorden, deze systemen werken op een vergelijkbare manier als dopamine bij mensen.

Dus als we een bewustzijn in AI willen creëren, hoeven we alleen maar te begrijpen hoe ons eigen bewustzijn wordt gecreëerd. Dit mysterie
wordt nog niet volledig begrepen door de neurowetenschappen en daar komen we later op terug. Maar als dat wel zo is en het ons lukt om dit bewustzijn te creëren, wordt het verschil tussen ons en een machine wel heel erg klein. Deze mogelijke vooruitgang roept een groot aantal ethische vragen op en zet ons aan het denken over de perceptie van ons eigen bewustzijn.

Hoe dan ook, een deel van de oplossing voor de uitdagingen waar onze soort voor staat, is het toevoegen van meer bewustzijn aan onze dagelijkse handelingen.

Bewustzijn is ook een klankbord voor onze waarnemingen.

Door onze sensorische resonantiekamer te ontwikkelen, kunnen we ons striatum laten geloven dat het meer plezier krijgt, terwijl we het kwantitatief minder geven.

Een heel praktisch hulpmiddel om ons bewustzijn te ontwikkelen is bemiddeling. [^35]

Ons bewustzijnsniveau op een niveau brengen dat vergelijkbaar is met ons intelligentieniveau zal ongetwijfeld een grote uitdaging zijn om een goede samenleving te ontwikkelen en de toekomst van onze soort veilig te stellen. De evolutie naar een bewustzijnssamenleving en naar een economie van mentale groei.

## De behoefte aan betekenis en zekerheid

Zodra het individu keuze- en geloofsautonomie heeft, met andere woorden het recht om zijn leven de richting te geven die hij wil, is het bepalen van die richting een heel moeilijke taak geworden. Nu de richting niet langer wordt bepaald door religie of een totalitair politiek regime, moet ieder van ons zijn eigen betekenis creëren. Maar wanneer we ons ervan bewust worden dat ons bestaan kort is en gedoemd tot niets, worden we geconfronteerd met het vinden van een rechtvaardiging voor ons bestaan en onze daden, en dit besef kan ondraaglijk worden. [^36]

Omdat zingeving belangrijker is voor het leven dan al het andere, heeft het overlevingswaarde voor de mens. Dus om ons gebrek aan zingeving te compenseren gaan we op zoek naar geld, status enzovoort.

Het genotssignaal dat wordt gegenereerd stroomopwaarts van waar het dier naar op zoek is, is een voorspelling van onze hersenen over wat er gaat gebeuren. Deze productie vindt plaats omdat het een evolutionair voordeel is, een mechanisme dat de overlevingskansen van het dier vergroot.

Door te kunnen produceren wat er gaat gebeuren op basis van wat het om zich heen waarneemt, vertienvoudigt het zijn controle en beslissingsvermogen. Het kan voordelige situaties opzoeken en potentieel gevaarlijke situaties ontvluchten. Ze zijn de realiteit een stap voor.

De hersenen van gewervelde dieren hebben een manier uitgevonden om voorspellingen te doen en de werkelijkheid een stap voor te blijven. Dit vermogen om verbanden te leggen tussen de toestand van de omgeving op een bepaald moment en de toekomstige toestand ervan is de basis van wat we, in een zeer gevierde soort als Homo Sapiens, zintuig noemen.

We hebben een gevoel voor de menselijke samenleving gecreëerd en we weten dat we daarin geaccepteerd kunnen worden en onze weg kunnen vinden, zolang we de gevestigde regels en codes respecteren. Deze samenleving is geen chaos, maar heeft een orde, en deze begrijpelijke orde is fundamenteel geruststellend voor ons.

Onze neiging om betekenisvolle verbanden te ontdekken in onze omgeving is zo ontwikkeld en zo onbedwingbaar dat het ons soms leidt naar verbanden die er niet noodzakelijkerwijs zijn.

Als een krijger bijvoorbeeld op jacht gaat naar een prooi, een halsketting omdoet en daarmee op de prooi jaagt, zal hij de volgende keer dat hij dezelfde halsketting omdoet een kleine stoot dopamine ontvangen over de mogelijkheid om een andere prooi te vangen. De krijger zal zich zekerder voelen over zijn toekomstige succes.

Dit systeem van anticiperen helpt om het gevoel van onzekerheid over de toekomst te verminderen. Waarnemen, voorspellen en anticiperen op toekomstige gebeurtenissen en het verminderen van angst maken allemaal deel uit van het begrip zingeving. Dit voordeel is zo doorslaggevend dat er alle reden is om aan te nemen dat het door de evolutie is geselecteerd.

Het waarnemen van betekenis om ons heen is zo cruciaal voor onze overleving dat situaties waarin deze betekenis ons ontglipt acute fysiologische angst oproepen. Deze reactie wordt door ons organisme uitgelokt als een overlevingsinstinct[^37].

Met behulp van beeldvormende instrumenten voor de hersenen kunnen we achterhalen wat er in de hersenen gebeurt. Een kleine strook hersenschors, die zich een paar centimeter boven het striatum bevindt, komt in actie. Dit is een plooi van hersenschors die zich bevindt op het grensvlak tussen twee hersenhelften, die zelf verbonden zijn met het striatum, en die de anterieure cingulate cortex wordt genoemd. Deze innerlijke cingulate cortex licht op zodra de gemaakte voorspellingen niet langer worden bevestigd door wat er werkelijk gebeurt.

Dit alarmsignaal betekent een schending van de voorspelling. Als er te veel voorspellingen ongeldig worden verklaard, wordt het moeilijk om ons te organiseren en krijgen we de indruk in chaos te vervallen. Overbelast raakt dit foutsignaal schadelijk voor de lichamelijke en geestelijke gezondheid van het individu. Het veroorzaakt een krachtige stressreactie in het lichaam, waarbij de cingulate cortex een meervoudig gekoppeld zenuwcircuit activeert dat leidt naar een hersencentrum dat betrokken is bij angst en bezorgdheid, de amygdala, Vervolgens naar de adrenocorticale klieren in de nieren en naar neuronale kernen in de hersenstam, die hormonen als cortisol en noradrenaline vrijgeven, waardoor het lichaam in een vluchthouding terechtkomt, zelfs verlamd raakt en een angst opwekt die existentieel kan worden.

Onze cingulate cortex speelt daarom de rol van een alarmsignaal dat ons waarschuwt wanneer onze wereld niet langer waarneembaar zinvol is. De gevolgen van deze reactie variëren van slaapstoornissen tot depressie, angst, geheugenverlies, hart- en vaatziekten en diabetes.

Als het niveau van orde en organisatie in onze omgeving begint af te nemen, wordt dit centrale deel van onze hersenen geactiveerd en waarschuwt het ons voor de aanwezigheid van een potentieel gevaar voor onze overleving. Als de samenleving relatief stabiel is, waar de structuren van werk, gezin en menselijke relaties niet al te onvoorspelbaar en willekeurig veranderen, is de cingulate cortex een factor van aanpassing en aanpassing, maar als de referentiepunten te snel en voortdurend veranderen zonder het individu enig respijt te geven, kan dit extreem gevaarlijk zijn voor zichzelf en voor anderen.

Maar hoe reageert de menselijke geest wanneer hij wordt geconfronteerd met een leegte van betekenis? Hij construeert representatiesystemen die doordrongen zijn van betekenis, orde en samenhang. Zolang de mens bestaat, heeft hij de werkelijkheid alleen maar betekenis gegeven.

De eerste pogingen daartoe namen de vorm aan van mythische verslagen over de schepping en de natuur. De behoefte aan betekenis komt voort uit de behoefte aan controle en is een uitloper van ons verlangen om te overleven. En ons de wereld voorstellen als een plaats die bewoond wordt door betekenis, kalmeert het interne waarschuwingssysteem in het geval van een eenmalige voorspellingsfout in de concrete wereld.

Als gevolg daarvan wordt de waarschuwingsreactie die door de cingulate cortex in gang wordt gezet van nature verzwakt bij gelovigen. Maar vandaag de dag geloven de meeste mensen in de westerse wereld niet meer en zijn we dit zintuig kwijtgeraakt.

De grote systemen van religieuze, maar ook ideologische, democratische of filosofische betekenis zijn, om zo te zeggen, niet meer dan gedevalueerde referentiepunten, verzwakt door wetenschappelijke kennis en door het naast elkaar bestaan van meerdere spirituele of ideologische boodschappen die we proberen te tolereren, maar waarvan alleen al de veelheid genoeg is om de hoop dat een van hen als enige een absolute waarheid kan bevatten, tot niets te reduceren.

Door de geschiedenis heen zijn rituelen systematisch vóór morele systemen verschenen. De synchronisatie en mimicry die in collectieve rituelen aan het werk zijn, maken mensen gevoeliger voor de gevoelens, verlangens en emoties van hun medemensen. Dit vermogen om in de huid van een ander te kruipen wordt cognitieve empathie genoemd en stelt ons in staat om 'in de huid van een ander te kruipen', te voelen wat zij voelen en te denken wat zij denken. En dit vermogen wordt vertienvoudigd door imitatie: gesynchroniseerde partners voelen compassie voor elkaar.

Rituelen kalmeren onze cingulate cortex doordat ze ons in staat stellen betrouwbaarder te voorspellen wat onze medemensen waarschijnlijk wel of niet zullen doen door het simpele feit dat we hun bewegingen tijdens het ritueel al kunnen voorspellen.

Maar het is moeilijker om de gedachten van mensen te voorspellen dan hun bewegingen. Om dit te bereiken is het niet langer een kwestie van simpelweg gebaren delen, maar van het delen van mentale representaties. Met andere woorden, waarden en visies op de wereld.

Er kunnen verschillende visies op de wereld zijn, waarvan sommige zogenaamd normatief zijn, in de zin dat ze bepaald gedrag voorschrijven, en andere die enkel positief (of feitelijk) zijn en omgekeerd geen enkel gedrag voorschrijven. Wanneer deze visie voorschrijft wat goed is om wel of niet te doen, en mensen zich hieraan houden en de wereld op dezelfde manier zien als wij, vermindert dit de onzekerheid en de facto de activiteit van de cingulate cortex aanzienlijk.

Maar de schending ervan activeert het daarentegen sterk (net als andere hersenstructuren). Voor onze anterieure cingulate cortex is het niet respecteren van morele normen een fundamentele voorspellingsfout. Dit is op zich al een krachtige kalmerende factor voor de cingulate cortex, die zijn oriëntatie zoekt bij zijn medemensen.

Zodra je er zeker van bent dat anderen in dezelfde heilige morele waarden geloven als jij, kun je met grotere betrouwbaarheid voorspellen wat ze waarschijnlijk wel of niet zullen doen.

In de term religie betekent het Latijnse werkwoord religere samenbinden, dus we begrijpen dat religie in de eerste plaats dient om individuen samen te binden.

Met het verstrijken van de tijd heeft de mensheid haar sociale en morele focus verloren ten gunste van voorspellingen over de materiële wereld. Zoals de beroemde filosoof Friedrich Nietzsche het formuleerde: "God is dood, alles kan".

Voor onze cingulate cortex is dit het begin van angst. Omdat de betekenis die de wetenschap geeft alleen een positieve (of feitelijke) visie biedt, heeft deze niet dezelfde geruststellende kracht als die van religie en moraliteit.

De wetenschap introduceert een feitelijke betekenis, maar niet, tenminste in eerste instantie, een morele.

Toegegeven, de feitelijke betekenis die door de wetenschap wordt verkregen is kolossaal. Vandaag de dag weten mensen tot op zekere hoogte hoe de aarde is gevormd en hoe ze erop terecht zijn gekomen, en ze kunnen vrij nauwkeurig de opeenvolging van gebeurtenissen reconstrueren die hebben geleid tot de evolutie van soorten, van bacteriën tot de eerste gewervelde dieren, zoogdieren, apen en Homo Sapiens. De vraag naar de oorzaak van zijn bestaan, zijn aard en zijn fysieke en mentale kenmerken is beantwoord. Op dezelfde manier stelt kennis van de wetten van de fysica en van levende organismen ons in staat om een duizelingwekkend aantal concrete feiten te voorspellen, zoals het weer, verduisteringen van de maan en de zon, het vermogen van een verbrandingsmotor afhankelijk van de mate van verfijning van de gebruikte benzine en de cilinderinhoud, enz.

Maar we weten niet meer wat zinvol is in menselijke en existentiële termen. Want hoewel we zingeving hebben uitgebannen, hebben we de behoefte aan zingeving niet uitgebannen. Het deel van onze hersenen dat honderdduizenden jaren nodig had om zich aan te passen om betekenis te vinden in de wereld en in sociale structuren is er nog steeds. [^36]

In onze samenleving leven we de mythe van Sisyphus, een Grieks mythologisch personage dat veroordeeld is om een rotsblok eeuwig naar de top van een heuvel te rollen en elke keer weer naar beneden te rollen voordat hij de top bereikt. [39] In onze maatschappij heeft de manier waarop we produceren geen zin meer. We leven nu in een wegwerpwereld. En als niets blijvend is, wordt het leven een verspilling. We worden allemaal in meer of mindere mate met dit probleem geconfronteerd.

Bovendien is door de klimaatverandering niets meer stabiel, niet de cyclus van de seizoenen, niet de zeespiegel, niet het optreden van natuurrampen. De wereld is nu in beweging en deze beweging gaat onze schuldcapaciteiten te boven.

Het woord is hard, maar onzekerheid is dodelijk, het ondermijnt onze hersenen, het vernietigt het fundamentele menselijke streven naar betekenis.

## Onze reacties op onzekerheid

Het gevoel van onbepaaldheid activeert de cingulate cortex, die pijnlijk de onmogelijkheid opmerkt om voorspellingen te doen over de toekomst, om zichzelf te definiëren en om een duidelijk levenspad te zien.

Eco-angst, bijvoorbeeld, is een nieuwe vorm van angst die voortkomt uit het verlammende besef van de vernietiging van onze planeet. [^38]

In een wereld waar een maximum aan onzekerheid heerst, zal de cingulate cortex erin slagen zekerheid te herstellen op kleinere schalen, omdat het dat niet kan op de meer globale schaal van zijn bestaan. Dit wordt microzekerheid genoemd.

Drugs die onze neuronen beïnvloeden kunnen een vervanging zijn voor zingeving. Cocaïne vernietigt bijvoorbeeld onzekerheid.

Eigenwaarde is de egocentrische uitvinding van een individualistische wereld en een manier om het verlies aan zingeving te compenseren.

Geld is het ultieme paspoort om vrij te zijn van de angst van onzekerheid.

Maar in het spel van competitie groeit de ongelijkheid. De verliezers in deze wedloop, ontmenselijkt door het doorsnijden van de navelstreng, zullen zich in het ergste geval wenden tot een gedegradeerde vorm van menselijkheid. Dit is het soort menselijkheid dat de menselijkheid van anderen ontkent om zijn eigen exclusiviteit te garanderen. We hebben het hier over identiteit.

Want deze spanning leidt automatisch tot een dringende behoefte: voor eens en altijd weten wie je bent. En de eenvoudige manier om je identiteit te verduidelijken is door jezelf te definiëren aan de hand van een groep waartoe je behoort. Jezelf definiëren door middel van je relatie tot een groep vervult een fundamentele behoefte: een manier vinden om je te conformeren aan de geldende regels, die de voorbarige reacties van de cingulate cortex zal beperken.

In feite is een van de belangrijkste bronnen van zingeving die regelmatig in enquêtes wordt genoemd het gevoel bij een sociale groep te horen[^40].

Ongelijkheden duwen samenlevingen naar een terugtrekking in identiteit. Waar het enige dat telt het vermogen is om de productiemachine voor eigen doeleinden te gebruiken. Het neoliberale economische model, gebaseerd op concurrentie, de mobiliteit van individuen, de versnelling van het werktempo en de vermindering van overheidsuitgaven voor de bescherming en rechtvaardige verdeling van rijkdom, lokt een autoritaire en op identiteit gebaseerde impuls uit die bedoeld is om de reacties van het individu op onzekerheid en het gebrek aan een gevoel van sociale verbondenheid te kalmeren.

Anomie verwijst naar het gevoel van een verlies aan orde en logica in de samenleving.

De mensen die het vaakst nostalgische gedachten hebben, zijn ook degenen die het duidelijkst de indruk hebben dat het bestaan zin heeft[^41].

Onze hersenen zijn extreem goed in ontkenning.

Wanneer onze handelingen niet in overeenstemming zijn met onze mentale voorstellingen, creëert dit cognitieve dissonantie. Tegenstrijdigheid activeert ook cognitieve dissonantie.

Dit komt omdat het menselijk brein in alle omstandigheden samenhang en redelijkheid zoekt. Het zal daarom proberen om de dissonantie die onzekerheid vertegenwoordigt op te lossen, en om dit te doen zal het ofwel zijn acties aanpassen zodat ze in lijn zijn met zijn denken, of zijn denken aanpassen zodat het in fase is met zijn acties.

Onderzoek naar cognitieve dissonantie toont aan dat het meestal gedachten zijn die zich aanpassen aan acties en niet andersom. [^42][^43]

We verliezen het contact met de werkelijkheid omdat we niet meer weten hoe we die moeten interpreteren. Betekenis beschermt ons omdat het manieren voorstelt om de wereld te interpreteren en effectiever te handelen om deze te beheersen en ons te beschermen tegen de bedreigingen die erin schuilen.

Nu het schrikbeeld van grote rampen steeds dichterbij komt, zal het terugtrekken van gemeenschappen alleen maar meer uitgesproken worden, net als het compensatiegedrag van hyperconsumptie, ego-inflatie en ontkenning.

Het gebrek aan zingevingssystemen drijft ons tot het consumeren van materiële goederen, vooral in situaties van grote onzekerheid, versnelling en concurrentie. Hypermaterialisme is het lapmiddel voor onzekerheid en concurrentie[^36].

## Oplossingen voor het gebrek aan betekenis

Het brein probeert de wereld te voorspellen om hem beter te kunnen beheersen, en het bereikt dit ofwel door zingeving ofwel door technologie. Als technologie vooruitgang boekt, heeft het geen betekenis meer nodig. En wanneer technologie faalt, heeft het betekenis nodig.

Vandaag de dag hebben we technologie, maar het heeft gefaald, want het tekent ons uitsterven. [^44]

Met het oog op de ramp die zich nu ontvouwt en nog gaat voltrekken, moeten we zo snel mogelijk stoppen met de consumptiemachine die de mensheid is geworden, terwijl we actief zoeken naar manieren om de aarde te herstellen.

Hoe meer betekenis afneemt hoe meer consumptie toeneemt, en omgekeerd, hoe meer betekenis toeneemt hoe meer consumptie afneemt.

We hebben 8 miljard cingulate cortexes die bezig zijn met het misleiden van hun angst voor de dood en het onvoorspelbare, nu moeten we ze bezig houden met het misleiden van deze angst door ze te verenigen rond een gedeelde betekenis[^36].

Deze betekenis moet miljarden mensen van verschillende culturele achtergronden, met verspreide beschavingserfenissen en heterogene intellectuele achtergronden, samenbrengen om hen hun obsessie met bezitten en uitbuiten te laten vergeten.

Er zijn 2 betekenissen: de kosmische en de sociale.

De eerste is gebaseerd op ons begrip van de wetten van de natuur, de materie en het universum. De tweede heeft te maken met ons vermogen om in een sociale context te handelen in overeenstemming met wat we goed en fout vinden.

Ze creëren een moreel systeem, waarin een goed en een kwaad worden gedefinieerd die het sociale en individuele handelen reguleren.

Het heilige is de spil van waaruit betekenis wordt opgebouwd in menselijke bijeenkomsten. Vandaag de dag heeft de moderne mensheid het heilige verloren door de werkelijkheid te deconstrueren en te ontdekken dat alles, van de beweging van atomen in een gesmolten ster tot de stroom van ionen in het membraan van je neuronen terwijl je deze regels leest, gehoorzaamt aan mechanistische wetten waarin goed en kwaad geen plaats hebben.

Het gevolg van deze demoralisatie van de wereld is dat elk individu op het aardoppervlak kan beslissen wat hij of zij goed of slecht vindt.

Maar er kan geen collectief gevoel bestaan zonder de notie van het heilige. De oplossing is om het heilige opnieuw te scheppen, en dat heilige kan onze aarde zijn.

Het heilige kan zich alleen manifesteren door middel van rituelen, en de mensheid zal nieuwe rituelen moeten uitvinden om de impasse te doorbreken die ze heeft bereikt. Zoals we gezien hebben, helpen synchrone praktijken om de cingulate cortex te kalmeren. Zulke rituelen zullen het behoud van de planeet heilig moeten maken.

Door op deze manier te werk te gaan, kunnen mensen zichzelf uitrusten met effectieve middelen om de mentale last van angst die gekoppeld is aan individualisme te verminderen. De wetenschap dat de burgers van mijn land, van het hele continent en zo mogelijk van de hele planeet, heilig houden wat ikzelf als een onovertrefbare waarde beschouw, schept de basis voor een nieuw pact van saamhorigheid en vertrouwen.

De mens is een voorwaardelijke samenwerker; hij is in staat tot immense offers op voorwaarde dat hij de zekerheid heeft dat de andere leden van zijn gemeenschap hetzelfde zullen doen. [^45]

Door van koolstofzuiverheid en het behoud van biologisch en geologisch evenwicht heilige waarden te maken, kunnen de miljarden mensen de activiteit van hun cingulate cortex terugbrengen naar een aanvaardbaar niveau en niet langer afhankelijk zijn van materiële goederen, drugs, geld en ego-vervangers om hun eigen bestaan te ondersteunen.

Ecologie kan een visie op de wereld bieden die gemeenschappelijk is voor alle mensen.

Met kennis is de lijst van handelingen die verenigbaar zijn met heilige waarde een kwestie van wetenschap, van metingen en berekeningen door het menselijk collectief.

De uitdaging is om een gevoel van globale identiteit te creëren dat iedereen het gevoel geeft geaccepteerd te worden in een groep, zonder de noodzaak om hun waarde en geschiktheid te bewijzen door materiële goederen of egosteun te vergaren.

Vandaag de dag moeten we accepteren dat de wetenschap ons niet alleen kan vertellen wat waar en onwaar is, maar ook wat goed en fout is.

De derde betekenis zal daarom ecologisch zijn.

Om deze te ontwikkelen moeten we kennis ontwikkelen door middel van onderwijs, om kennis door te geven over levende systemen, biodiversiteit, klimatologie, ecologie en de evolutie van soorten.

Dit zal een gevoel van verwondering creëren, want verwondering is een krachtig tegengif voor de existentiële angst die ons in zijn greep houdt, door de ontdekking van de oneindige rijkdom van levende dingen, de schoonheid van landschappen, flora en fauna, omdat we daarin een bron van betekenis vinden.

Omdat we allemaal dezelfde aarde bewonen, ernaar handelen en lijden onder de gevolgen van haar achteruitgang, worden we allemaal aangezet tot het erkennen van dezelfde heilige waarde.

Op deze manier zullen burgers in staat zijn om zich in te zetten voor acties die in overeenstemming zijn met hun waarden en overtuigingen en zullen ze een gevoel van betekenis ervaren.

Een speler worden in deze verandering is op zichzelf al een bron van betekenis en van afstemming tussen onze meningen en onze daden.

We moeten de fundamenten van onze beschaving herzien. De logica van just-in-time productie en consumptie die ten grondslag ligt aan onze economieën is onhoudbaar. In het volgende hoofdstuk ontdek je onze voorstellen in termen van de 'economie'.

Diep van binnen hebben we allemaal een immense aspiratie, we zijn op zoek naar betekenis en we weten niet hoe we die kunnen vervullen. Het doel van dit werk is om dat verlangen te bevredigen zonder te wachten tot het werkelijkheid wordt, want het is er al vanaf het allereerste begin.

## Behoefte aan fictie - Het verhalende brein

Het menselijk brein, die buitengewone vertelmachine, is de kern van ons vermogen om verhalen te creëren, in goede en slechte tijden. Het smeedt de verhalen die ons in staat stellen om onze acties te rechtvaardigen en er betekenis aan te geven. Dit narratieve vermogen is geen toeval, maar het product van een complexe evolutie, die de diepte van onze behoefte aan betekenis en orde in een chaotische wereld onthult. [^46]

Dus wat onze soort bijzonder maakt is bijvoorbeeld niet dat ze al oorlog voert sinds het begin der tijden - chimpansees en mieren doen het net zo goed - maar dat ze er een hele geschiedenis van heeft gemaakt... en miljoenen verhalen.

Andere dieren doen geen kwaad omwille van het kwaad, noch doen ze iets anders omwille van het goede; ze handelen voornamelijk op impuls van onmiddellijke behoeften of instincten. Mensen daarentegen zoeken betekenis in hun handelingen en die van anderen, vaak door het prisma van moraliteit, goed en kwaad. Deze zoektocht naar betekenis is dus een manifestatie van onze hogere hersenfuncties, die ons ertoe aanzetten om verklaringen te zoeken en ons gedrag in een bredere context te rechtvaardigen.

Wat we beschouwen als het 'zelf' is geen geïsoleerde of autonome entiteit, maar eerder een mozaïek van eigenschappen, overtuigingen en waarden die we lenen, delen en soms betwisten van de mensen om ons heen. Elke interactie, elke culturele of sociale uitwisseling helpt onze identiteit vorm te geven en maakt haar dynamisch en evoluerend. Ons zelf is geweven van anderen. We definiëren onszelf in de spiegel van onze relaties, waarbij elke persoon een reflectie is die ons helpt ons eigen beeld te vormen[^49].

Dit proces van narratieve constructie resoneert met de boeddhistische leer over de illusie van het zelf. Volgens het boeddhisme is het 'zelf' dat we waarnemen als een stabiele en continue entiteit een illusie, een constructie van de geest. [^47] We zullen hier dieper op ingaan in het gedeelte over religie van deel 2. Op dezelfde manier onthult de neurowetenschap dat onze identiteit een verhaal is dat we onszelf vertellen, een samenhangende opeenvolging die vanuit het niets door onze hersenen wordt gemaakt om een illusie van continuïteit in stand te houden[^48].

Ons identificeren met onze verhalen en ons geconstrueerde 'zelf' kan ons vastzetten in beperkende denkpatronen. Erkennen dat deze narratieve constructie een illusie is, kan bevrijdend werken. Het nodigt ons uit om traditionele verhalen in twijfel te trekken en ons open te stellen voor nieuwe mogelijkheden om de wereld waar te nemen en te ervaren.

De interactie tussen het 'echte leven' en fictie krijgt een nieuwe dimensie. In plaats van deze sferen als gescheiden te zien, kunnen we begrijpen dat ze intrinsiek met elkaar verbonden zijn en elkaar voeden. Onze persoonlijke en collectieve verhalen zijn niet slechts afleidingen of ontsnappingen, maar krachtige hulpmiddelen voor transformatie en begrip.

Het herkennen van de narratieve aard van onze hersenen en het erkennen van de illusie van het zelf betekent niet dat we onze identiteit of onze verhalen verwerpen, maar dat we ze zien voor wat ze zijn: flexibele, aanpasbare en diep menselijk constructies. Het moedigt ons aan om niet langer traditionele verhalen te volgen en tegen onszelf te zeggen "Zo is de wereld altijd geweest en zal hij altijd zijn", maar om te begrijpen en tegen onszelf te zeggen "We zien alleen wat we zien".

Het biedt ons een pad naar een rijker en genuanceerder begrip van onszelf en de wereld, waarbij we ons niet langer vastklampen aan onze identiteit als een onwrikbare realiteit en ons er exclusief mee identificeren, maar waarbij we in ons leven de mogelijkheid zien om de wereld en onszelf opnieuw te verbeelden.

## Effecten en cognitieve vooringenomenheid

Hier gaan we de verschillende effecten en vooroordelen van onze hersenen bekijken, omdat ze onze perceptie, geheugen, oordelen, beslissingen en gedrag op verschillende manieren kunnen beïnvloeden en de manier waarop we informatie verwerken en ermee omgaan kunnen beïnvloeden. Dit begrip van onszelf zal ons in staat stellen om effectieve hulpmiddelen te ontwikkelen voor onze samenleving, in het bijzonder voor het vormgeven van ons besluitvormingsproces en het vermijden van de gevolgen van deze vooroordelen die ons zouden verhinderen om ons gemeenschappelijke doel te bereiken.

Een gedetailleerde lezing van de cognitieve effecten en vooroordelen is weliswaar aan te raden, maar geenszins noodzakelijk en kan voor uw leesgemak worden overgeslagen. Ze zijn samengevat in het volgende gedeelte.

### Samenvatting van cognitieve effecten en vertekeningen

Hier volgt een niet-uitputtende lijst van cognitieve vertekeningen:

- Selectiebias: We zijn eerder geneigd om informatie te selecteren die past bij onze vooropgezette overtuigingen en meningen, in plaats van een breder scala aan informatie in overweging te nemen. [^50]

- Geheugenillusie: We hebben de neiging om de betrouwbaarheid van onze herinneringen te overschatten en ze te kleuren met onze huidige perceptie[^51].

- Rationalisatie van overtuigingen: We hebben de neiging om onze beslissingen en gedragingen te rechtvaardigen, zelfs als ze verkeerd of schadelijk zijn, door ze te interpreteren op een manier die gunstig is voor onszelf[^53].

- Overschattingsbias: We hebben de neiging om ons vermogen om toekomstige gebeurtenissen te voorspellen en situaties te beheersen te overschatten[^52].

- Optimism bias: We hebben de neiging om de waarschijnlijkheid van toekomstige voordelen te overschatten en potentiële risico's te onderschatten. [^54]

- Complexiteitsreductie bias: We hebben de neiging om de complexiteit van situaties te vereenvoudigen om ze beter beheersbaar te maken[^55].

- Causal inference bias: We hebben de neiging om een oorzaak toe te schrijven aan een gebeurtenis, zelfs wanneer dit niet noodzakelijk gerechtvaardigd of redelijk is.[^56].

- Reduction effect bias: We hebben de neiging om de positieve effecten van een actie te overschatten en de negatieve effecten te onderschatten.[^57]

- Primacy effect bias: We worden beïnvloed door de eerste informatie die we ontvangen en zijn geneigd deze zwaarder te wegen dan latere informatie.[^58]

- Similarity effect bias: We zijn eerder geneigd om mensen aardig te vinden en te vertrouwen die op ons lijken[^59].
- Beschikbaarheidsheuristiek: We gebruiken vaak een snelle, intuïtieve schatting om de waarschijnlijkheid of frequentie van een gebeurtenis in te schatten, gebaseerd op de informatie die het gemakkelijkst beschikbaar is in ons geheugen.[^60]

- Peer pressure: We worden beïnvloed door de perceptie van wat anderen van ons denken en worden onder druk gezet om ons te conformeren aan hun meningen en gedragingen.[^61]

- Halo bias: We hebben de neiging om een positieve of negatieve mening over een persoon te generaliseren op basis van een enkel kenmerk.[^62]

### Diepgaande uitleg over effecten en cognitieve bias

- Bevestigingsvooroordeel

Confirmation bias is een cognitieve bias, waarbij we de voorkeur geven aan informatie die onze meningen en overtuigingen ondersteunt. Omdat we op zoek zijn naar betekenis, selecteren we informatie die in onze richting gaat. De oplossing hiervoor is om onze eigen tegenspreker te zijn, om onze gedachten op de proef te stellen en te proberen te bewijzen dat ze niet kloppen.

De tegenstrijdigheden in onze oordelen moeten ons dus niet kwetsen, maar ons wakker schudden en aanzetten tot actie. We houden niet van de correctie van onze meningen, maar integendeel, we moeten ervoor openstaan en onszelf eraan aanbieden, en dat moet gebeuren in de vorm van een gesprek en niet in de vorm van een preek.

We moeten niet, terecht of onterecht, zoeken naar manieren om van elke oppositie af te komen, maar juist diepgaand kijken of het wel juist kan zijn.

- Vooroordeel over beschikbaarheid

Beschikbaarheidsbias is een andere cognitieve bias die de besluitvorming kan beïnvloeden. Het treedt op wanneer we de waarschijnlijkheid van een gebeurtenis of de frequentie van het voorkomen ervan inschatten op basis van voorbeelden die ons snel te binnen schieten. Deze bias kan ertoe leiden dat we de waarschijnlijkheid van zeldzame gebeurtenissen onderschatten en de waarschijnlijkheid van veel voorkomende of recente gebeurtenissen overschatten. We overschatten onze redenering door de voorkeur te geven aan informatie die direct toegankelijk is voor ons geheugen.

Om deze bias te vermijden, is het belangrijk om rekening te houden met een representatieve steekproef van informatie en om te proberen objectief te zijn bij het inschatten van waarschijnlijkheden. Het is ook belangrijk om niet alleen te vertrouwen op informatie die direct beschikbaar is, maar om op zoek te gaan naar aanvullende informatie die ons een completer en evenwichtiger beeld kan geven.

- Halo-effect

Het halo-effect is het mechanisme dat ervoor zorgt dat we iemand kwaliteiten toeschrijven op basis van zijn of haar fysieke verschijning.

Een datingsite genaamd cupidexe voerde bijvoorbeeld een experiment uit waarbij de profielen van mensen werden beoordeeld op persoonlijkheid en uiterlijk. Degenen met een lage fysieke score hebben een lage persoonlijkheidsscore en omgekeerd betekent een hoge fysieke score een goede persoonlijkheid. Je zou denken dat omdat ze een goede persoonlijkheid lijken te hebben, ze ook een goede lichaamsbouw zouden hebben, maar in de top 1% zijn sommige persoonlijkheidsprofielen leeg...
Ze deden nog een experiment om dit te controleren. Hier kwamen gebruikers profielen tegen met tekst en soms niet, en of je nu tekst had of niet je kreeg dezelfde score.

Er zijn andere studies en statistieken die dit effect bevestigen. Alle Amerikaanse presidenten zijn bijvoorbeeld lang en hoe langer je bent, hoe meer geld je verdient.

- Barnum effect

Het Barnum-effect verwijst naar de neiging van mensen om te geloven dat vage of algemene opmerkingen accurate beschrijvingen zijn van hun persoonlijkheid of leven. Dit fenomeen is vernoemd naar de beroemde showman P.T. Barnum, die vage persoonlijkheidsbeschrijvingen gebruikte om mensen naar zijn shows te lokken.

Bijvoorbeeld, een persoonlijkheidstest die zegt "u bent een zorgzaam en liefhebbend persoon voor uw familie en vrienden" is vaag genoeg om als waar beschouwd te worden voor de meeste mensen. Het geeft echter geen specifieke details over de persoon, wat kan leiden tot een gevoel van identificatie met de algemene beschrijving, zelfs als het niet erg persoonlijk is.

Het Barnum-effect kan worden versterkt door de behoefte van mensen aan erkenning en hun hang naar zelfbeschikking. Mensen voelen zich graag begrepen en gewaardeerd, wat ertoe kan leiden dat ze algemene beschrijvingen als waar voor hen accepteren.

Het is belangrijk om rekening te houden met het Barnum-effect wanneer je persoonlijkheidstests of opmerkingen over je leven of persoonlijkheid analyseert. Het is altijd beter om te zoeken naar meer specifieke en gedetailleerde informatie om een nauwkeuriger begrip van jezelf te krijgen.

- Verankeringseffect

Het verankeringseffect verwijst naar de neiging van mensen om hun inschattingen of beslissingen te baseren op een eerste stukje informatie, dat een "anker" wordt genoemd. Dit eerste stukje informatie kan een grote invloed hebben op latere inschattingen, zelfs als het irrelevant of zelfs fout is.

Als je bijvoorbeeld gevraagd wordt om het aantal inwoners van je stad te schatten en je krijgt als eerste informatie een hoog cijfer, dan zul je geneigd zijn om een hogere schatting te maken dan wanneer je een lager cijfer had gekregen. Op dezelfde manier, als je onderhandelt over de prijs van een object, zal het eerste voorstel vaak als het anker worden beschouwd en invloed hebben op latere voorstellen.

Het verankeringseffect kan bewust of onbewust worden gebruikt in onderhandelings-, verkoop- of overtuigingssituaties, wat kan leiden tot slecht rationele en bevooroordeelde beslissingen.

- Druk van gelijken

Collegiale pressie is een sociaal fenomeen dat de neiging beschrijft van individuen om zich te conformeren aan de meningen, houdingen en gedragingen van een groep waartoe ze behoren of waarmee ze zich verbonden voelen. Het kan zich voordoen wanneer individuen zich willen conformeren aan sociale normen en afkeuring of isolatie van de groep willen vermijden.

Wanneer iemand zich bijvoorbeeld in een groep bevindt met mensen die een bepaalde mening of houding hebben, kan hij beïnvloed worden door deze meningen en houdingen, zelfs als deze niet overeenkomen met zijn eigen meningen of houdingen. Dit kan vooral het geval zijn in situaties waarin de persoon geaccepteerd wil worden door de groep en afkeuring of uitsluiting wil vermijden.

Groepsdruk kan ook ontstaan wanneer iemand zich verplicht voelt zich te conformeren aan het gedrag van een groep om zich niet anders of ongemakkelijk te voelen. Iemand in een groep die alcohol drinkt, kan bijvoorbeeld beïnvloed worden om zelf ook alcohol te drinken, zelfs als hij of zij dat niet wil.

Het is belangrijk om je bewust te zijn van groepsdruk en niet toe te geven aan de meningen of het gedrag van een groep alleen maar om afkeuring te voorkomen of om je aan te passen aan sociale normen. Het is altijd beter om je eigen mening en overtuigingen te volgen dan toe te geven aan groepsdruk.

- Beschikbaarheidsheuristiek

De beschikbaarheidsheuristiek is een mentale snelkoppeling die we vaak gebruiken om de frequentie of waarschijnlijkheid van een gebeurtenis te beoordelen op basis van de meest recente en direct beschikbare voorbeelden in ons geheugen. Dit kan leiden tot beoordelingsfouten, omdat er vooroordelen kunnen zitten in de voorbeelden die we in gedachten hebben.

Als iemand bijvoorbeeld net op televisie heeft gehoord over een vliegtuigongeluk, kan hij of zij zich meer zorgen maken over de veiligheid op de volgende vlucht, omdat deze gebeurtenis vers in het geheugen ligt en direct beschikbaar is. Deze persoon kan echter de veiligheid van de luchtvaart in het algemeen onderschatten, omdat vliegtuigongelukken eigenlijk heel zeldzaam zijn.

Het is belangrijk om te onthouden dat de beschikbaarheidsheuristiek tot beoordelingsfouten kan leiden en om meer volledige en betrouwbare informatie te zoeken om de frequentie of waarschijnlijkheid van een gebeurtenis te beoordelen. Het is ook nuttig om te onthouden dat de media onze perceptie van gebeurtenissen kunnen beïnvloeden door bepaalde verhalen meer te benadrukken dan andere. Dit kan leiden tot een oververtegenwoordiging van zeldzame of dramatische gebeurtenissen en een verkeerd beeld geven van de werkelijke frequentie ervan. Het is dus belangrijk om te onthouden dat we ons niet alleen laten beïnvloeden door wat we zien of horen in de media.

Concluderend kan de beschikbaarheidsheuristiek nuttig zijn voor het nemen van snelle beslissingen in alledaagse situaties, maar het is belangrijk om er voorzichtig mee om te gaan als het gaat om het nemen van belangrijkere beslissingen of beslissingen met gevolgen op de lange termijn. Het is beter om te zoeken naar volledige en betrouwbare informatie bij het inschatten van waarschijnlijkheden en risico's dan alleen te vertrouwen op wat direct beschikbaar is in ons geheugen.

## Conclusie

Dit hoofdstuk over Homo Sapiens heeft al een aantal wegen geopend. Sommige daarvan zullen we in deze conclusie verder uitwerken, maar deze lijst is niet uitputtend. Bovenal zullen we vele andere ontwikkelen in de volgende hoofdstukken, voortbouwend op de hier ontwikkelde basis.

We kennen nu de menselijke behoeften die bevredigd moeten worden als we totale vervulling willen bereiken. We hebben gezien dat het mogelijk is om onze extrinsieke behoeften te transformeren en dat het nodig zal zijn om dat te doen om ze compatibel te maken met een gemeenschappelijk project dat ons allemaal in staat zal stellen om onze behoefte aan zingeving te vervullen.

Deze collectieve betekenis moet bestaan via de notie van het heilige. En niets is heiliger dan wat we allemaal gemeen hebben en waarvan het voortbestaan direct met ons verbonden is, namelijk onze aarde en haar ecosystemen. Wetenschap en ecologie zijn dus niet langer alleen in staat om ons te vertellen wat waar of onwaar is, maar zijn ook in staat om onze waarden te dicteren, wat goed of fout is.

We hebben gezien dat er geen menselijke natuur is die ons verhindert in dit project te slagen, dat er geen onwrikbaar zelf is, en in deze taak zal de ontwikkeling van ons bewustzijn onze beste bondgenoot zijn. Net zo belangrijk zal de opvoeding van nieuwe generaties zijn, hen aanmoedigen altruïstisch te zijn en zich te verwonderen over de natuur om haar te beschermen.

Door de studie van verschillende cognitieve effecten en vooroordelen hebben we steeds meer informatie verzameld die nodig is om onszelf te begrijpen en dus ook om hulpmiddelen te ontwikkelen die rekening houden met deze parameters zodat ze ons effectief kunnen helpen in ons gemeenschappelijke project.

## Bronnen (niet-limitatieve lijst)

[^1]: [IPCC-rapport](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia : Pyramide van behoeften](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectieven op de psychologische wetenschap
[^4]: [Voedsel- en Landbouworganisatie van de Verenigde Naties (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Cijfers en analyses over slechte huisvesting in Frankrijk, waaronder het aantal daklozen en precaire leefomstandigheden](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Afzien van gezondheidszorg om financiële redenen in de agglomeratie Parijs:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [De Fondation de France publiceert de 13e editie van haar studie over eenzaamheid](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap International: Gehandicapten blijven behoren tot de meest uitgeslotenen ter wereld](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivatie en Persoonlijkheid, 1970 (tweede editie)
[^10]: Arendt Hannah, De toestand van de moderne mens, 1961
[^11]: Valérie Jousseaume, Plouc Pride: een nieuw verhaal voor het platteland.
[^12]: Emmanuel Todd, The Origin of Family Systems, 2011 - Waar staan we? Een schets van de menselijke geschiedenis, 2017 - Klassenstrijd in Frankrijk in de 21e eeuw, 2020.
[^13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997.
[^14]: Jean Baudrillard, De consumptiemaatschappij, 1970.
[^15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994.
[^16]: Charles Darwin, The Origin of Species: Hoofdstuk IV / III, 1859 & The Descent of Man and Sexual Selection: Hoofdstuk VII, 1871
[^17]: Hannah Arendt, De oorsprong van het totalitarisme, 1951
[^18]: Albert Camus, De pest, 1947
[^19]: Jean-Paul Sartre, Existentialisme is een humanisme.
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, Homo Sociabilis: Wederkerigheid.
[^21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, De menselijke bug.
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, Het oerbrein
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior.
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, Altruïsme: een enigma?
[^29]: Matthieu Ricard, "Altruïsme: het gen of de opvoeding?"
[^30]: Kathryn Spink, "Moeder Teresa: een leven in dienst van anderen".
[^31]: Wolfram Schultz, The Role of Dopamine in Learning and Memory, 2007.
[^32]: Read, D., & Northoff, G, Neurale correlaten van impulsieve en reflectieve beslissingen. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, Het brein en plezier.
[^34]: David Eagleman, Het onbewuste brein.
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 and Gloria Y. Yeh,3 Een systematisch overzicht en meta-analyse van de effecten van meditatie op empathie, compassie en prosociaal gedrag, 2018
[^36]: Sébastien Bohler, Waar is de betekenis, 2020.
[^37]: [Roy F. Baumeister, De behoefte aan zingeving: een psychologisch perspectief](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Eco-angst: analyse van een hedendaagse angst.
[^39]: Albert Camus, De mythe van Sisyphus, 1942.
[^40]: [Enquête "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, De irrationele mens 2001.
[^43]: Elliot Aronson, De theorie van cognitieve dissonantie.
[^44]: Elizabeth Kolbert, The 6th Extinction, 2015.
[^45]: Robert Axelrod, Het dilemma van de gevangene
[^46]: Nancy Huston, The fabulist species
[^47]: Serge-Chritophe Kolm, Geluk en vrijheid
[^48]: David M. Eagleman, Incognito, 2015.
[^49]: William James, De beginselen van de psychologie, 1980
[^50]: [Wikipedia: Selection bias](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, De illusie van het geheugen.
[^52]: David Dunning and Justin Kruger, Unskilled and unaware of it: How difficulties in recognising one's own incompetence lead to inflated self-assessments, 1997.
[^53]: [Festinger en Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Onrealistisch optimisme over toekomstige levensgebeurtenissen, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones en Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Thinking, Fast and Slow, 2011.
[^58]: [Asch, S. E., Forming impressions of personality, 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Beschikbaarheid: een heuristiek voor het beoordelen van frequentie en waarschijnlijkheid, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. Een constante fout in psychologische beoordelingen, 1920.
