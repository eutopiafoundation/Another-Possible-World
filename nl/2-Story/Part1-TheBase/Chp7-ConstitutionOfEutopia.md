---
title: Grondwet van Eutopia
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Inhoud in het experimentele stadium en in het proces van schrijven.
> {.is-gevaar}

De grondwet van Eutopia is verdeeld in 3 verklaringen:

- De Universele Verklaring van de Rechten van de Mens, waarin veel van de fundamentele principes van de huidige Universele Verklaring van de Rechten van de Mens[^1] zijn opgenomen, maar die we hebben aangepast om belangrijke nuances en uitbreidingen toe te voegen om onze waarden beter te weerspiegelen, zoals duurzaamheid, diversiteit, onderlinge afhankelijkheid met de natuur, vrijheid, solidariteit en gelijkheid.
- Universele Verklaring van de Rechten van de Natuur / Moeder Aarde, is een handvest dat werd opgesteld op initiatief van de Amerindiaanse volkeren en geformuleerd op de Wereldconferentie van Volkeren tegen Klimaatverandering in 2010[^2].
- Universele Verklaring van de Rechten van de Mens met betrekking tot niet-menselijke diersoorten, is een voorlopig en experimenteel handvest dat tot doel heeft onze relaties met andere levende diersoorten preciezer vast te leggen.

Om deze niet omslachtiger te maken, zijn de rechtvaardigingen voor de verschillende artikelen gescheiden en aan het einde van deze pagina geplaatst. Deze verklaringen kunnen vrij worden gewijzigd door de burgers van Eutopia door middel van hetzelfde democratische proces dat beschreven is aan het einde van het vorige hoofdstuk over hoe samen te beslissen.

## Universele Verklaring van de Rechten van de Mens

### Preambule

Geleid door de waarden van gelijkheid, vrijheid, solidariteit en duurzaamheid, erkennen wij de inherente waardigheid van elk individu, een baken dat de weg verlicht naar een wereld waarin allen gelijk geboren worden in rechten en vrijheden, en naar onze collectieve bestemming.

We vieren de oneindige diversiteit die de rijkdom van ons menselijk tapijt weeft, en we zijn verenigd door de diepe overtuiging dat in deze diversiteit onze collectieve kracht ligt. Het is in deze geest van eenheid en harmonie dat we deze Verklaring afleggen, waarin we bevestigen dat ieder mens het recht heeft om vrijelijk zijn gedachten te uiten, zijn geluk na te streven en bij te dragen aan onze gemeenschappelijke welvaart.

Onze gemeenschappelijke zoektocht wordt vormgegeven door een absoluut respect voor elkaars integriteit, door het niet aflatende streven naar eerlijkheid en rechtvaardigheid, en door een heilige toewijding aan het behoud van ons gedeelde thuis, Moeder Aarde.

Door deze universele rechten op te schrijven, werpen we een krachtig schild op tegen onderdrukking, onrecht en vergetelheid. We verklaren dat in elke hoek van Eutopia het licht van gelijkheid de donkere hoeken van onrecht verlicht en een wereld creëert waar vrijheid, liefde en mededogen onvervreemdbare rechten zijn.

Moge deze Universele Verklaring van de Rechten van de Mens ons inspireren tot actie, onze beslissingen leiden en onze harten verenigen in de voortdurende opbouw van een samenleving waarin allen, hand in hand, marcheren naar de dageraad van een wereld waarin menselijke waardigheid het kompas is dat onze reis leidt.

### Voorstellen en samenvattingen

Artikel 1. Gelijkheid :

- Mensen worden geboren en blijven vrij en hebben gelijke rechten.
- Elke burger van Eutopia heeft het onvervreemdbare recht om op gelijke voet met alle anderen deel te nemen aan het besluitvormingsproces. Geen enkele vorm van discriminatie of voorrecht mag de gelijke beslissingsbevoegdheid van elk individu belemmeren.

Artikel 2. Vrijheid :

- De mens heeft de volledige vrijheid om zich te uiten en te denken; niemand mag de verscheidenheid van meningen en het vrije verkeer van ideeën verhinderen.
- Ook de vrijheid van geloof is heilig. Iedereen heeft het recht om zijn spirituele, religieuze of filosofische overtuigingen te kiezen, te beoefenen en te uiten zonder angst voor vervolging of discriminatie.
- Mensen hebben het recht om vrij hun kleding te kiezen, om zichzelf persoonlijk uit te drukken door middel van kleding, zonder onderworpen te worden aan oordelen of beperkingen opgelegd door derden.
- Mensen hebben het fundamentele recht om zich vrij over de hele wereld te bewegen, behalve om redenen van natuurbehoud of om redenen van veiligheid van menselijke of materiële integriteit, geen enkele grens mag deze bewegingsvrijheid beperken.
- Elk individu is vrij om te ondernemen, zichzelf te ontplooien, te creëren en zijn creaties en innovaties in de wereld te brengen. Creativiteit, innovatie en individuele bijdrage aan het algemeen welzijn van Eutopia moeten worden aangemoedigd.

Artikel 3. Solidariteit (Broederschap) :

- Solidariteit moet de leidraad zijn voor de beslissingen die door de leden van Eutopia worden genomen.

Artikel 3. Billijkheid :

- Elke beslissing moet zo worden ontworpen dat iedereen er ten volle van kan profiteren, waarbij barrières en ongelijkheden worden geëlimineerd.

Artikel 4. Duurzaamheid:

- Het behoud van het milieu is van het grootste belang en een collectieve verantwoordelijkheid. Bij beslissingen moet rekening worden gehouden met de gevolgen voor de natuur en moet worden gezorgd voor een duurzaam evenwicht tussen de mensheid en de planeet.

Artikel 5. Integriteit :

- In geen geval mag een beslissing de integriteit van een andere persoon in gevaar brengen, noch fysiek, noch mentaal. Elk individu blijft echter soeverein en is de enige houder van de macht om zijn of haar eigen integriteit aan te tasten.

Artikel 7. Veiligheid :

- Elk individu heeft recht op veiligheid, op bescherming tegen fysiek of mentaal geweld en op een veilige omgeving om zich te ontwikkelen.
- Er mogen geen dodelijke wapens worden bewaard of geproduceerd buiten de geautoriseerde onderzoekscentra en personen.

Artikel 8. Vertrouwen :

- Socialiteit is gebaseerd op collectief vertrouwen, altruïstische wederzijdse hulp en erkenning van elkaars behoeften. Geen enkele verleende dienst mag van de ontvanger worden gevraagd voor geld, noch mag deze worden gemeten en opgeslagen in een denkbeeldige waarde met het oog op toekomstige ruil voor dezelfde of een andere dienst. Iedereen die van zichzelf aan allen geeft, geeft van zichzelf aan niemand in het bijzonder.

Artikel 9. Verscheidenheid :

- Eutopia viert diversiteit in al haar vormen en zet zich in voor het creëren van een inclusieve gemeenschap, waar elk individu wordt gerespecteerd en gewaardeerd om zijn of haar uniekheid.

## Universele Verklaring van de Rechten van de Natuur / Moeder Aarde

### Preambule

Wij, de Eutopianen van de Aarde :

- Overwegende dat wij allen deel uitmaken van Moeder Aarde, een ondeelbare levensgemeenschap bestaande uit onderling afhankelijke wezens die nauw met elkaar verbonden zijn door een gemeenschappelijke bestemming;
- Dankbaar erkennend dat Moeder Aarde de bron is van leven, voeding en onderwijs, en dat zij ons voorziet van alles wat we nodig hebben om goed te leven;
- Overtuigd dat het in een levensgemeenschap met onderling afhankelijke relaties onmogelijk is om alleen aan mensen rechten toe te kennen zonder het evenwicht binnen Moeder Aarde te verstoren;
- Bevestigend dat om mensenrechten te garanderen het noodzakelijk is om de rechten van Moeder Aarde en van alle levende wezens in haar te erkennen en te verdedigen, en dat er culturen, praktijken en wetten bestaan die deze rechten erkennen en verdedigen;
- Deze Universele Verklaring van de Rechten van Moeder Aarde af te kondigen, zodat elke persoon en elke instelling de verantwoordelijkheid op zich neemt om, door onderwijs, opvoeding en het ontwaken van het geweten, de eerbied voor de in de Verklaring erkende rechten te bevorderen, en door ijverige en progressieve maatregelen en bepalingen op lokale en mondiale schaal te verzekeren dat ze universeel en effectief erkend en toegepast worden door alle Eutopiërs van de wereld.

### Voorstellen en samenvattingen

Artikel 1. Moeder Aarde :

1. Moeder Aarde is een levend wezen.
2. Moeder Aarde is een unieke, ondeelbare en zelfregulerende gemeenschap van onderling verbonden wezens die alle wezens voedt, bevat en vernieuwt.
3. Elk wezen wordt gedefinieerd door zijn relaties als een constituerend element van Moeder Aarde.
4. De intrinsieke rechten van Moeder Aarde zijn onvervreemdbaar omdat ze voortkomen uit dezelfde bron als het bestaan zelf.
5. Moeder Aarde en alle wezens bezitten alle intrinsieke rechten die in deze Verklaring worden erkend, zonder onderscheid tussen biologische en niet-biologische wezens of onderscheid op basis van soort, oorsprong, nut voor de mens of enig ander kenmerk.
6. Net zoals mensen mensenrechten genieten, hebben alle andere wezens rechten die specifiek zijn voor hun soort of type en passend bij hun rol en functie binnen de gemeenschappen waarin zij bestaan.
7. De rechten van elk wezen worden beperkt door die van andere wezens, en elk conflict tussen hun respectieve rechten moet worden opgelost op een manier die de integriteit, het evenwicht en de gezondheid van Moeder Aarde bewaart.

Artikel 2. De inherente rechten van Moeder Aarde :

1. Moeder Aarde en alle wezens waaruit zij bestaat bezitten de volgende intrinsieke rechten:
   - het recht om te leven en te bestaan ;
   - het recht op respect ;
   - het recht op regeneratie van hun biocapaciteit en op de continuïteit van hun vitale cycli en processen, vrij van menselijke verstoring;
   - het recht op het behoud van hun identiteit en integriteit als afzonderlijke, zelfregulerende en onderling verbonden wezens;
   - het recht op water als bron van leven;
   - het recht op schone lucht
   - het recht op volledige gezondheid;
   - het recht om vrij te zijn van vervuiling, verontreiniging en giftig of radioactief afval;
   - het recht om niet genetisch gemodificeerd of getransformeerd te worden op een manier die hun integriteit of vitaal en gezond functioneren aantast;
   - het recht op volledige en onmiddellijke genoegdoening voor elke schending van de rechten erkend in deze Verklaring als gevolg van menselijke activiteiten.
2. Elk wezen heeft het recht om een plaats in te nemen en zijn rol te spelen binnen Moeder Aarde zodat zij harmonieus kan functioneren.
3. 3. Alle wezens hebben het recht op welzijn en om vrij te zijn van marteling of wrede behandeling door toedoen van mensen.

Artikel 3. Verplichtingen van de mens jegens Moeder Aarde :

1. Ieder mens heeft de plicht Moeder Aarde te respecteren en in harmonie met haar te leven.
2. Mensen hebben de plicht
   - a) te handelen in overeenstemming met de rechten en plichten die in deze Verklaring worden erkend ;
   - b) de volledige verwezenlijking van de in deze Verklaring erkende rechten en plichten te erkennen en te bevorderen
   - c) het bevorderen van en deelnemen aan het leren over, analyseren, interpreteren van en communiceren over manieren van leven in harmonie met Moeder Aarde in overeenstemming met deze Verklaring;
   - d) te verzekeren dat het nastreven van menselijk welzijn bijdraagt aan het welzijn van Moeder Aarde, nu en in de toekomst;
   - e) doeltreffende normen en wetten vast te stellen en te handhaven voor de verdediging, bescherming en het behoud van de rechten van Moeder Aarde;
   - f) om de vitale ecologische cycli, processen en evenwichten van Moeder Aarde te respecteren, te beschermen en te behouden en, waar nodig, hun integriteit te herstellen;
   - g) te verzekeren dat schade die het gevolg is van menselijke schendingen van de intrinsieke rechten die in deze Verklaring worden erkend, wordt hersteld en dat de verantwoordelijken aansprakelijk worden gesteld voor het herstel van de integriteit en de gezondheid van Moeder Aarde;
   - h) mensen en instellingen in staat te stellen de rechten van Moeder Aarde en alle wezens te verdedigen; en
   - i) voorzorgs- en beperkende maatregelen te nemen om te voorkomen dat menselijke activiteiten leiden tot het uitsterven van soorten, de vernietiging van ecosystemen of de verstoring van ecologische cycli;
   - j) de vrede te waarborgen en nucleaire, chemische en biologische wapens uit te bannen
   - k) praktijken te bevorderen en aan te moedigen die Moeder Aarde en alle wezens respecteren, in overeenstemming met hun eigen culturen, tradities en gewoonten;
   - l) economische systemen te bevorderen die in harmonie zijn met Moeder Aarde en in overeenstemming met de rechten die in deze Verklaring worden erkend.

## Universele Verklaring van de Rechten van de Mens met betrekking tot niet-menselijke diersoorten

### Preambule

In deze Eutopia Universele Verklaring van de Rechten van de Mens in Relaties met Niet-Menselijke Diersoorten leggen we de basis voor een harmonieus samenleven gebaseerd op wederkerigheid en mutualisme. Het is cruciaal om te benadrukken dat de principes die hier uiteengezet worden van toepassing zijn op elk dier waarmee we kiezen samen te werken en symbiotische interacties te delen binnen Eutopia.

We maken dit bewuste onderscheid om de vrijheid van dieren te behouden als een integraal onderdeel van het dierenrijk en hun natuurlijke habitats, waar ze zich kunnen ontwikkelen zonder menselijke inmenging. In deze beschermde gebieden erkennen we de onschatbare waarde van de wilde natuur en zetten we ons in om deze ecosystemen in volledige autonomie te laten gedijen.

Echter, waar mensen wonen en ervoor kiezen om gemeenschappen te bouwen, streven we ernaar om in harmonie te leven met de geselecteerde diersoorten en relaties op te bouwen gebaseerd op wederzijds begrip, respect en voordelige samenwerking. Deze verklaring is dus van toepassing op de dieren die we opnemen in ons ecosysteem, voor wie we automatisch verantwoordelijk worden en waaruit onze inzet voor een evenwichtige en respectvolle symbiose blijkt.

### Rechten van soorten onder menselijke verantwoordelijkheid

Artikel 1. Respect voor de waardigheid van dieren :

- Alle dieren onder de verantwoordelijkheid van de inwoners van Eutopia hebben recht op een waardig en wreed leven.
- Menselijke interactie met dieren moet hun intrinsieke aard en natuurlijke gedrag respecteren.

Artikel 2. Fatsoenlijke levensomstandigheden :

- Dieren moeten worden gehuisvest in omstandigheden die hun fysieke en psychologische welzijn respecteren.
- De faciliteiten moeten voldoende ruimte, toegang tot geschikt voedsel, schoon water en optimale sanitaire omstandigheden bieden.

Artikel 3. Toegang tot verzorging :

- Elk dier onder de verantwoordelijkheid van de inwoners van Eutopia heeft recht op regelmatige diergeneeskundige zorg om zijn gezondheid en welzijn te waarborgen.
- Medische behandeling moet worden gegeven op een humane, respectvolle en ethische manier.

Artikel 4. Einde van het leven :

- Dieren die bestemd zijn voor menselijke consumptie moeten een respectvol levenseinde krijgen zonder onnodig lijden.

## Rechtvaardiging

### Universele Verklaring van de Rechten van de Mens

Wordt aan gewerkt...

### Universele Verklaring van de Rechten van de Natuur / Moeder Aarde

Wordt momenteel opgesteld...

### Universele Verklaring van de Rechten van de Mens met betrekking tot niet-menselijke diersoorten

Wordt momenteel opgesteld...

## Bronnen (Niet-uitputtende lijst)

[^1]: [Universele Verklaring van de Rechten van de Mens. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Universele Verklaring van de Rechten van Moeder Aarde. 2012 ](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
