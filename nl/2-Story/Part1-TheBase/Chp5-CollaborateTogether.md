---
title: Samenwerken
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Wat is de economie?

De economie is de menselijke activiteit die bestaat uit de productie, distributie, uitwisseling en consumptie van goederen en diensten[^2].

Maar een economisch systeem produceert niet alleen goederen en diensten, het produceert ook mensen en de relaties tussen hen. De manier waarop de maatschappij produceert en consumeert heeft een grote invloed op persoonlijkheden, karakters, kennis, verlangens, geluk en soorten interpersoonlijke relaties.

We hebben soms de neiging om de samenleving alleen te beoordelen op de hoeveelheden van de verschillende goederen en diensten die de leden ervan hebben. Maar mensen waarderen niet alleen deze goederen en diensten, maar ook de gevoelens, houdingen en interpersoonlijke relaties die ermee gepaard gaan, wat deze overbrengen over wat het is om mens te zijn. Deze aspecten worden maar al te vaak verwaarloosd in de economie[^3].

Een economisch systeem vormt en vormt mensen en hun relaties door hun productie- en consumptieactiviteiten. De maatschappij houdt het economische systeem in stand, dat op zijn beurt de aard en de kwaliteit van de relaties tussen mensen en hun psyche diepgaand beïnvloedt.

In een ruil ontvangen de gever en ontvanger altijd meer dan ze eigenlijk aan elkaar geven, omdat ze elkaar ook de kans geven om een menselijke relatie aan te gaan.

We zijn eraan gewend geraakt om de economische wetenschap te volgen alsof het een eenvoudig natuurgegeven is dat dicteert hoe de wereld zou moeten zijn, zonder het echt in vraag te stellen. We zijn doordrenkt van de wereld waarin we leven, maar als we eenmaal begrijpen wat een economisch systeem is, zijn we in staat om ons voor te stellen hoe onze wereld werkt met andere regels.

Er gebeurt iets nieuws deze dagen, want bovenop de ellende van de wereld en de overheersing die we over elkaar hebben, hebben we de klimaatverandering, die ons naar het einde van onze economische groei zal brengen. [^4]

Elk jaar geven de media en klimaatonderzoeksorganisaties ons een datum. Het is de datum waarop wetenschappers schatten dat de mensheid meer grondstoffen heeft verbruikt dan de aarde in dezelfde periode kan produceren.

Dit cijfer is het resultaat van experts op verschillende gebieden die de balans opmaken van de hulpbronnen die we aan de planeet onttrekken en de snelheid waarmee deze hulpbronnen worden aangevuld.

Deze datum staat bekend als de "dag van de overschrijding". Voor een duurzame economie zou deze datum op 31 december moeten vallen en niet eerder. In dat geval kunnen hulpbronnen in hetzelfde tempo worden vernieuwd als ze worden verbruikt.

In 2022 wordt deze datum geschat op 22 juli[^1].

Deze datum zou ons moeten helpen om beter naar ons economisch systeem te kijken, om ons bewust te worden van de menselijke fundamenten en de ondeugden ervan.

Met name het blijven bevorderen van een economisch systeem dat onze grote primaire versterkers (= verlangen naar seks, voedsel, informatie) aanmoedigt, is waarschijnlijk het slechtste wat we kunnen doen[^5].

Economische theorieën slagen er niet in om argumenten naar voren te brengen die echt intellectuele steun winnen, wat hun zwakte aantoont. Dit komt omdat ze vaak gebaseerd zijn op onze collectieve verbeelding en geen echte basis hebben.

Dus misschien is economie meer filosofisch dan het wil laten blijken, en minder wetenschappelijk dan het wil laten blijken.

Zijn er dan andere mogelijkheden voor een economisch systeem?

## Het plan en de markt

Er zijn momenteel 2 economische systemen die de moderne wereld domineren, de markt en planning[^6].

Elke partij verdedigt haar systeem met een sociale ethiek en valt de andere aan voor de realisatie van een andere.

Voor de markt is deze waarde vrijheid en voor planning is deze waarde gelijkheid.

Maar in de markt zijn we nog lang niet echt vrij, hoewel we vrijer zijn dan in het plan, maar we zijn slechter af als het gaat om gelijkheid.

En in het plan zijn we nog lang niet echt gelijk, maar wel meer dan in de markt, aan de andere kant zijn we het slechtst af als het gaat om vrijheid.

In feite is de huidige realiteit dat elk van deze systemen slecht presteert op het gebied van de waarde die ze verdedigen, maar het is waar, beter dan het andere voor de verdedigde waarde en zelfs slechter volgens het andere ethische criterium.

Deze 2 economische systemen zijn in hun meest extreme vorm de kapitalistische markt en het totalitaire communisme geworden, die beide misselijkmakende en vernederende menselijke eigenschappen en relaties voortbrengen en versterken, zoals egoïsme, anderen als dingen behandelen, vijandigheid, conflicten, concurrentie tussen mensen, overheersing, uitbuiting, vervreemding...

Geen van deze 2 economische systemen is erin geslaagd om de beste kwaliteiten van de mens te verheerlijken. Sommige ervan worden zelfs sterk onderdrukt. In beide systemen is er een bepaalde vorm van overheersing.

## De 3de economie: Wederkerigheid

Is er dan een ander alternatief?

Er bestaat literatuur die begint met "Er zijn drie soorten economische systemen". Maar deze literatuur moet gezocht worden in de historische of disciplinaire marges van het economisch denken, ofwel bij economen van enkele generaties geleden, ofwel in de economische antropologie.

Dit is het geval bij de econoom Karl Polanyi (1886-1964), die in zijn werk de economische systemen van alle samenlevingen, met inbegrip van voorbije, primitieve en traditionele samenlevingen, in drie categorieën indeelt. En hij wijst erop dat ze naast elkaar kunnen bestaan binnen dezelfde samenleving, hoewel elk van hen een bevoorrechte/dominante/principale plaats toekent. Hij onderscheidt eerst één economisch systeem, de markt. Zijn tweede systeem noemt hij het systeem van herverdeling, dat in zijn uitgebreide en moderne vorm planning is, waarbij een politiek centrum beslist over de economische toewijzing. Het derde systeem noemt Polanyi "Wederkerigheid", volgens een antropologische traditie die al goed gevestigd is, en die zelfs vrij duidelijk en nauwkeurig is. [^6]

We zijn ons niet bewust van het bestaan van dit type economie, maar het bestaat wel binnen onze samenlevingen. Vrijwel alle samenlevingen bevatten alle drie de economische systemen, maar in alle samenlevingen is één van de systemen belangrijker dan de andere twee.

Hier is een driehoek die overeenkomt met verschillende belangrijke systemen in 1984:
[![Diagram van de 3 soorten economie](./../../../GlobalProject/Image/Marché.png)]

Economen bouwden de economie en haar denkwijze op in de veronderstelling dat de mens was en altijd is geweest wat we "homo oeconomicus" zouden noemen, en ze creëerden een karikatuur van hem als een egoïstisch, berekenend en asociaal wezen. Maar menselijke wezens waren bovenal wat we zouden noemen "homo sociabilis", universeel beoefenaars van wederkerigheid en universeel behoefte hebbend aan een kring van goede vrienden of een gemeenschap[^9].

Wederkerigheid is het mechanisme van de gemeenschapseconomie die het begin van de mensheid voedde, als jager-verzamelaars en vervolgens als sedentaire boeren.

Wederkerigheid is nog steeds onder ons en het economische leven binnen de familie is voornamelijk gebaseerd op deze economie. Je biedt elkaar diensten aan zonder er iets voor terug te verwachten, je betaalt bijvoorbeeld een familielid niet om je van school te halen, noch wordt die persoon daartoe gedwongen door een externe entiteit. Wederkerigheid in onze samenlevingen is de laatste decennia drastisch afgenomen als gevolg van de markt, die sociale interacties steeds meer wil monetariseren, juist om nieuwe markten te creëren. Naast het gezin vinden we echter nog steeds veel wederkerigheid in vriendschappelijke/emotionele/liefdesrelaties, via vrijwilligerswerk, gemeenschappen, maar ook op verschillende gebieden, zoals de wereld van IT met opensource en via talloze platforms voor het delen van informatie, prestaties, objecten, enz.

Een lange reeks van wederkerige, regelmatige, voortdurende en vaste geschenken tussen twee mensen noemen we wederkerigheid. Algemene wederkerigheid is het equivalent, maar dan van iedereen. Iedereen geeft aan de maatschappij en de maatschappij ontvangt van iedereen.

Altruïsme en geven kunnen door hun aard niet worden verkregen door dwang of door uitwisseling in strikte zin.

Dit is het verschil met het plan: deze overdrachten worden niet afgedwongen en opgelegd aan de gever door een sociale entiteit buiten hem. En het verschil met de markt is dat het ook geen uitwisselingen zijn in de strikte zin van het woord, waar elke transactiepartner uitsluitend geïnteresseerd is in het hebben van meer of het niet hebben van minder van de goederen of diensten die ze overdragen.

In dit sociale systeem geeft niemand specifiek aan een ander, dus is het onmogelijk om te domineren, te dwingen, te vernederen of een tegengift af te dwingen, en om op die manier economisch of ethisch uit te buiten. Iedereen die aan iedereen geeft, geeft aan niemand.

In dit systeem komt het werk van iedereen ten goede aan de consumptie van een groot aantal anderen, vaak veel later. En iedereen profiteert van het werk van vele anderen, vaak al veel eerder. Elke persoon ontvangt zelfs een deel van het werk van generaties die verdwenen zijn en werkt voor generaties die nog geboren moeten worden.

De sociaal-ethische waarde van wederkerigheid zou je "broederschap" kunnen noemen. Hier hebben we de beroemde republikeinse trilogie: broederschap wordt gedefinieerd als "Doe anderen niet aan wat je niet wilt dat jou wordt aangedaan; doe anderen steeds het goede dat je van hen zou willen ontvangen".

Maar daar houdt de vergelijking met de trilogie op, omdat wederkerigheid het verlangen en de betekenis van de andere twee waarden uitwist. Want in dit systeem, waarin iedereen voor elkaar zorgt alsof hij voor zichzelf zorgt, botsen de wilsbeschikkingen, en dus de persoonlijke vrijheden, niet langer en hebben eisen voor gelijkheid geen enkele basis meer.

## Voordeel van wederkerigheid

Wederkerigheid is gebaseerd op vriendelijkheid.

Vriendelijkheid betekent de nadruk leggen op altruïsme, liefde, vrijwillige solidariteit, wederzijds geven, vrijgevigheid, delen, vrije gemeenschap, naastenliefde en liefdadigheid, welwillendheid en vriendschap, sympathie en mededogen. We missen ze allemaal en hebben het gevoel dat we er meer van nodig hebben. Al onze grote geestes-, denk- en gevoelsfamilies eisen ze op. Maar ze worden ontkend in ons economische leven. Ze domineren het bestaan van onze samenlevingen, maar we kunnen zien dat de meest wijdverspreide aspiraties de zeldzaamste prestaties zijn.

Algemene wederkerigheid is de verzoening tussen onze altruïstische ethiek en onze wetenschappelijke techniek, tussen ons streven naar een warme gemeenschap, individuele vrijheid en materiële vooruitgang; het maakt de duur van culturele betekenissen en de schoonheden van de wereld mogelijk in de uitbreiding van onze kennis, capaciteiten en gewetens.

Bovendien brengt wederkerigheid veel voordelen met zich mee voor de economische productie. Want het biedt een samenhangend, efficiënt systeem dat de totale werktijd drastisch zal verminderen.

## Is wederkerigheid mogelijk?

Hoewel we markten en planning uitgebreid hebben bestudeerd, hebben we bijna niets gezegd over de derde manier om goederen en diensten over te dragen, namelijk geschenken, en helemaal niets over de groep geschenken die bekend staat als wederkerigheid.

Door de geschiedenis heen zijn er nieuwe economische systemen verschenen en elke keer zijn er mensen, misschien zelfs de meerderheid, die denken en beweren dat de mogelijke systemen die systemen zijn die op dat moment kunnen worden waargenomen of die in het verleden hebben bestaan. Maar deze overtuigingen worden voortdurend tegengesproken door de geschiedenis, door het bestaan van nieuwe overtuigingen. En dit is zeker nog steeds het geval, want er is geen bewijs dat een van de systemen die bestaan of hebben bestaan het best mogelijke is. Economische systemen die nog nooit zijn waargenomen, zijn inderdaad mogelijk.

De fout in ons systeem is dat het individuen verhindert om individueel te kiezen om te geven: een bedrijf dat te gunstige prijzen aanbiedt gaat failliet; een individu kan niet weggeven wat hij produceert in een maatschappij waar niemand anders hem iets geeft.

Psychosociologen hebben lang onderzoek gedaan naar "helpend gedrag", met een aantal zeer interessante resultaten: we zijn niet alleen geneigd om diegenen te helpen die ons geholpen hebben, maar we zijn ook geneigd om meer te helpen als we zelf geholpen zijn, dat wil zeggen we zijn geneigd om vreemden te helpen als we zelf geholpen zijn door vreemden. Dat is de essentie van wederkerigheid. [^13]

En dit is wat we zagen in hoofdstuk 3 over Homo Sapiens, dat het mogelijk was om altruïstische wezens te creëren, omdat altruïsme ook gemotiveerd wordt door plezier, en dat het vooral de omgeving en opvoeding zijn die bijdragen aan het ontwikkelen van waarden en motivaties zoals vrijgevigheid, mededogen en het verlangen om anderen te dienen.

Maar is het eigenlijk wel mogelijk voor een samenleving om een economie te hebben die gebaseerd is op wederkerigheid, en in het bijzonder algemene wederkerigheid?

Er zijn hele samenlevingen waarin alle economische relaties in essentie of zelfs exclusief gebaseerd zijn op wederkerigheid. Maar deze sub-samenlevingen of samenlevingen zijn wat we "face-to-face groepen" noemen, waarvan de leden elkaar kennen, families, dorpen, bendes, etc., en wederkerigheid is niet in essentie algemeen. Het was nog steeds gemakkelijk om het hier toe te passen, maar één persoon kan niet meer dan honderd anderen kennen, dus we kunnen ons terecht afvragen of het echt mogelijk is op grote schaal[^14].

Want hoewel economie het probleem van de productie en distributie van goederen en diensten oplost, heeft elk economisch systeem (inclusief wederkerigheid) te maken met twee fundamentele problemen voor productie: het probleem van informatie en het probleem van motivatie.

## Het informatieprobleem

Informatie is de kennis van de arbeider of de leverancier van middelen of diensten over wat hij moet doen om zijn behoeften en verlangens zo goed mogelijk te bevredigen.

En motivatie is de prikkel om het werk te doen of de middelen op deze manier te gebruiken.

Het plan en de markt lossen deze twee problemen min of meer goed op.

De markt lost het informatieprobleem op via de gedecentraliseerde mechanismen van het prijssysteem, de "indicatoren van schaarste en nut": vraag en aanbod.

Planning lost het probleem op door deze informatie te organiseren, d.w.z. door instructies of voorspellingen te verzamelen, te berekenen en uit te geven, op een relatief gecentraliseerde manier.

De markt lost het motivatieprobleem op door egoïsme te verminderen via de stimulans van uitwisseling.

Het plan lost het probleem op door dwang, dreiging of straf, beloning of promotie, maar meer in het algemeen door autoriteit en haar verschillende middelen van dwang of beloning, en de min of meer sterke internalisering van de noodzaak om te gehoorzamen uit gewoonte of plichtsgevoel.

Dezelfde problemen gelden voor wederkerigheid: hoe zullen mensen weten wat anderen van hen nodig hebben en zullen ze genoeg aan anderen willen geven?

Deze twee problemen versterken elkaar in die zin dat het oplossen van het ene probleem het andere helpt en vice versa.

Hoe beter het motivatieprobleem is opgelost, hoe beter het informatieprobleem kan worden opgelost, omdat mensen dan gemakkelijker de nodige informatie aan elkaar kunnen doorgeven.

Hoe meer het informatieprobleem is opgelost, hoe productiever het economisch systeem is, en dus hoe minder verspilling er is, hoe minder werk er nodig is voor een bepaalde einddienst, en dus hoe minder het motivatiesysteem nodig is om werk te verschaffen, of hoe minder ernstig de imperfectie ervan is.

Wederkerig gezien vindt iedereen de behoeften of verlangens van anderen even belangrijk als die van zichzelf, en dit bepaalt wat ze geven van hun werk (tijd, moeite, moeite) en hun bezittingen, ongeacht hun herkomst.

Als het motivatieprobleem is opgelost, dan is ook het informatieprobleem opgelost, omdat mensen de economie altijd minstens zo efficiënt kunnen maken als met de markt of het plan, door vrijwillig de economische informatie die in deze systemen circuleert aan elkaar door te geven.

Informatietechnologie heeft op dit gebied een ware revolutie teweeggebracht, nu we allemaal met elkaar verbonden zijn en dankzij de ontwikkeling van geschikte toepassingen wordt de overdracht van informatie gemakkelijker dan ooit tevoren.

Het overbrengen en verwerken van informatie maakt deel uit van het werk van de maatschappij, naast ander werk. In ruil daarvoor drijft motivatie ons om te doen wat het beste is voor de gemeenschap. Dit systeem kan dus alles wat de markt of het plan kan doen.

Bovendien vermindert wederkerigheid de hoeveelheid benodigde informatie aanzienlijk. Door de vermindering van concurrerende of opzichtige consumptie, de vermindering van extrinsieke behoeften en de vermindering van de productie van de overeenkomstige goederen of diensten, de vermindering van de noodzaak om producten te vernieuwen vanwege hun ontwerp op basis van duurzaamheid, en de vermindering van veel diensten die niet langer nodig zullen zijn voor het functioneren van de economie.

## Het motivatieprobleem

Als we het over de "menselijke natuur" hebben, kunnen we horen "de mens zal nooit veranderen, hij is altijd egoïstisch geweest, enzovoort" en dan komen de ideeën over de "ware" menselijke natuur die uiteindelijk de overhand zouden moeten hebben. Historische, antropologische en psychologische kennis, analyse en zelfs eenvoudige logica moeten absoluut tegenover deze naïeve overtuigingen worden gesteld en het antwoord op "de mens zal nooit veranderen" kan net zo goed zijn dat hij alleen maar veranderd is[^15][^16][^17].

Op "de mens is altijd egoïstisch geweest en is dat overal" kunnen we net zo gemakkelijk antwoorden door ons af te vragen wat we weten over wat hij "altijd is geweest".

De mensheid staat aan het begin van een transformatie die ongekend is in haar bestaan wat betreft de diepte en, natuurlijk, de schaal.

Zoals we hebben gezien, bestaat er niet zoiets als de menselijke natuur en kunnen mensen worden wat ze maar willen.

Hier zijn een aantal punten die de motivatievraag beantwoorden:

- Mensen hebben een voorkeur voor altruïstische en gevende relaties[^18].
- Alle grote religieuze en seculiere ethieken pleiten sterk voor altruïsme en geven. [^20]
- De belangrijkste menselijke motivatie is het ontvangen van goedkeuring van andere mensen: in een wederkerig systeem is goedkeuring van nature universeel en wederkerig[^19].
- Empirische analyses van "helpend gedrag" bewijzen dat mensen veel eerder geneigd zijn anderen te helpen als ze zelf geholpen zijn.
- Sociologen hebben het idee verdedigd dat het belangrijkste kenmerk van mensen imitatie is[^22].
- Gewoonte, deze imitatie van zichzelf, is een ander menselijk fenomeen van primordiaal belang[^23].
- Studies naar de ontwikkeling van kinderen tonen aan dat we aanvankelijk altruïstisch zijn, maar dat de maatschappij ons vervolgens egoïstisch maakt.[^21].
- Opvoeding: zoals we gezien hebben, is de mens slechts een bladzijde waarop de maatschappij schrijft[^24].
- Sociale versterking: hoe meer mensen een geest van wederkerigheid hebben, hoe meer ze door hun daden laten zien en door hun woorden uitdrukken dat het normaal en goed is om dit te hebben, en dit moedigt anderen aan in deze richting. Onderwijs en sociale relaties als geheel spelen dus een belangrijke rol. Als de mens in de eerste plaats cultureel is, en dus kneedbaar, kan de maatschappij van hem net zo'n persoon van wederkerigheid maken als de egoïst van de markt, of de onderdanige of de hiërarch van het plan[^25].
- Hoe meer wederkerigheid er is, hoe meer we zien dat het levensvatbaar is en hoe meer we ons de voordelen ervan realiseren, en dus hoe meer we het accepteren, hoe meer we erin "geloven", hoe meer we het willen, hoe meer we ons ervoor inzetten. En dus hoe meer het zich verspreidt.

- In het geval dat sommige mensen die daartoe in staat zijn niet mee willen doen, is dat geen ernstige zaak, want als zulke gevallen zouden bestaan, zou het geen minachting voor hen zijn, omdat een dergelijk gevoel een gebrek aan welzijn in de samenleving zou betekenen: Maar zelfs als de persoon niet wenst deel te nemen, is dat geen ernstige zaak, omdat we niet alleen zullen ontdekken dat de werklast die per inwoner nodig is aanzienlijk zal verminderen, maar ook dat dit kan worden gecompenseerd door het feit dat anderen graag "meer" zullen geven dan ze ontvangen, uit altruïsme zoals we hebben gezien, maar ook uit hun eigen interesse in het werk.

Waarom zou je in de huidige maatschappij immers nog werken?

- Directe dwang: dwangarbeid
- Uitruil: je werk verkopen voor een loon
- Uit een verlangen om anderen of de gemeenschap te helpen, om "van dienst te zijn".
- Uit plichtsgevoel, mogelijk om anderen iets terug te geven voor wat we van hen ontvangen.
- Uit interesse in het werk zelf.
- Om sociale contacten, relaties, integratie in de maatschappij te vinden door bij te dragen aan de werkomgeving.
- Om anderen te laten zien dat je voor hen werkt, of dat je mooie dingen doet of dat je vaardig bent.
- Voor de sociale status die aan het werk verbonden is (bijdrage, verantwoordelijkheid, enz.).
- Om bezig te blijven, om "iets te doen" te hebben, om te voorkomen dat je je verveelt of niets doet.
- Voor spel
- Als gewoonte

We zouden er nog andere kunnen aan toevoegen, maar we zien hier duidelijk het belang van het beroep zelf en van de sociale relaties die het werk ons in staat stelt te creëren, zoals blijkt uit de gevoelens die volgen op 'pensionering'. In het algemeen ontlenen mensen meer betekenis aan hun productieve activiteit dan aan hun consumptieve activiteit. En al deze motivaties, met uitzondering van de eerste twee, kunnen mensen aanmoedigen om te werken in een systeem van wederkerigheid, dus we hoeven er alleen maar op in te spelen door de betekenis en het belang van werk te vergroten.

Werk is niet veroordeeld tot de marteling van zijn betwiste etymologische oorsprong, van het woord travalhum: tripalium, een martelwerktuig met een drievoet...[^26].

Alle dimensies van de sleur van de taak moeten daarom worden uitgewist of verminderd. Werk dat lichamelijk of psychisch uitputtend, saai of geestdodend is, te zwaar, mechanisch, monotoon, te vuil of onaangenaam, te zinloos, verachtelijk, enzovoort, moet worden afgeschaft, vervangen of minder vermoeiend worden gemaakt. Moet worden verwijderd, vervangen, beperkt, geautomatiseerd, verbeterd, verrijkt, gecompenseerd, beloond, gewaardeerd of breder gedeeld.

Het idee zelf is om de aard van werk te veranderen zodat het een activiteit wordt die zoiets is als zelfrealisatie, de manifestatie van iemands diepe menselijkheid door de transformatie van de natuur en het behoud ervan. Op deze manier zou werk de "eerste levensbehoefte" worden. De uitdrukking "werken om in je levensonderhoud te voorzien" zou dus niet langer betekenen dat je de pijn van het werk moet ondergaan om te overleven en jezelf te onderhouden, maar dat je daadwerkelijk in je levensonderhoud moet voorzien door middel van een activiteit die rijk is aan betekenis en ervaring.

## De plaats van geld in wederkerigheid

We zijn er zo aan gewend dat we allemaal geloven dat geld een noodzakelijke voorwaarde is voor het leven in de maatschappij. Toch is geld geen natuurgegeven, maar een denkbeeldige uitvinding van mensen die bekritiseerd kan worden[^30].

De manier waarop geld werkt verklaart niet zijn oorsprong, het verklaart zichzelf niet, en daarom verklaren economen niets. Ze verklaren zichzelf ook niet om existentiële redenen.

Geld is zogenaamd uitgevonden om het probleem van het dubbele samenvallen van behoeften op te lossen dat ruilhandel met zich meebrengt, omdat het zonder geld te moeilijk zou zijn om uitwisselingen te voltooien. Hoe vergelijk je goederen, en wat als ik jouw schoenen nodig heb maar jij mijn appels niet?

Ondanks veel antropologisch onderzoek dat het tegendeel bewijst, is deze mythe een van de diepst gewortelde mythes in het beeld dat mensen van de economie hebben.

In primitieve economieën namen mensen gewoon wat ze nodig hadden en vanuit sociaal oogpunt deelden ze hun middelen. Door de obsessie met schaarste die onze markteconomieën kenmerkt te negeren, konden primitieve economieën zich systematisch richten op overvloed[^7][^8].

Sommige antropologische onderzoeken, die meer gebaseerd zijn op historische elementen en empirische ontdekkingen, doen ons geloven dat geld niet voortkomt uit werk of uit ruil, maar dat het eerst en vooral historisch en conceptueel een manier is om een sociale band te creëren tussen individuen, omdat het de maatstaf is voor schulden die tussen hen zijn aangegaan.[^12].

Sommige beschavingen, zoals de Inca's, kenden noch geld, noch de markt, noch handel. [^10]

Het probleem met geld is dat zolang je het moet hebben om toegang te krijgen tot de uitwisseling van goederen en diensten, het kapitaal met ons kan spelen.

Het is dus niet door de hand op de arbeid te leggen dat het kapitalisme zijn overheersing verzekert, maar door de voorwaarden van socialiteit te beheersen die arbeid noodzakelijk maken en die, onder andere, de noodzaak van geld omvatten[^3].

Geld is het teken van vertrouwen tussen mensen. Er is namelijk een rotsvast vertrouwen nodig om een monetair teken van iemand anders te accepteren in ruil voor een verleende dienst of een geleverd goed: vertrouwen in het feit dat het geen vals biljet of vals geld is, vertrouwen in de toekomst die dit monetaire teken vertegenwoordigt voor zover het toekomstige projecten mogelijk maakt. Uiteindelijk is geld de horizon van wederkerigheid in de tijd, niet noodzakelijk ten opzichte van de persoon die mij het geld gaf, maar vertrouwen in het feit dat er in de toekomst iemand zal zijn die mij in de vorm van goederen en diensten de waarde zal kunnen teruggeven van de goederen en diensten die ik tijdelijk voor geld heb verkocht.

Want waarom zou ik een monetair teken gebruiken als ik de ander echt vertrouw? Het object zelf, het concept zelf van geld, weerspiegelt eigenlijk een gebrek aan vertrouwen in de ander, terwijl als dat vertrouwen er wel is, het nut ervan verdwijnt.

Als de ander mij en de maatschappij echt vertrouwt, kunnen ze me iets geven zonder er geld voor terug te vragen, omdat ze weten dat morgen ik of iemand anders hen iets zal geven.

De ruilrelatie, bemiddeld door geld, is een relatie die gebaseerd is op het idee dat de deelnemers geen vertrouwen kunnen hebben in de duur van hun relatie: ze gebruiken geld om de ruil af te ronden, om de schuld te vereffenen, kortom om niets meer met elkaar te maken te hebben als de transactie is afgerond.

De huidige politieke discussie in onze economieën gaat over hoe dit geld te gebruiken, zodat het goed functioneert, maar misschien kan geld gewoon niet goed functioneren, misschien is het voorbestemd om sociale relaties te perverteren?

Toch, als we het een aard moeten geven, zou het beter zijn om te zeggen dat geld voorbestemd is om sociale relaties te verstoren in zijn huidige vorm als ruilmiddel, in plaats van als "facilitator van de circularisering van goederen en diensten".

Geld zuigt zich op in alle leegtes die worden achtergelaten door de afwezigheid van betekenis, het stelt ons in staat controle te houden om onze cingulate cortex te kalmeren, en de angst voor de dood wordt de drijvende kracht achter gierigheid[^11].

Veel analyses wijzen wel op de negatieve effecten van de invloed van geld in de samenleving, maar ze stellen geen adequaat antwoord voor op het probleem van de monetarisering van de werkelijkheid.

In een gemonetariseerde samenleving wordt de waarde van dingen de waarde die geld eraan geeft.

Geld afschaffen lijkt onmogelijk, zo vanzelfsprekend is het idee van geld voor ons geworden. De monetarisering van de samenleving lijkt meer en meer een feit te worden waar we niet buiten kunnen denken.

Het probleem is dat het niet langer het individu is dat zijn of haar waardesysteem bepaalt, en dus waar hij of zij naar streeft, maar dat dit waardesysteem objectief wordt gemaakt door geld.

Maar door afstand te nemen van geld kunnen we ons weer vrij voelen van marktwaarden, door onze eigen waarden terug te winnen.

Maar als we blijven denken dat geld in zijn huidige vorm de enige manier is om de kwestie van de uitwisseling van goederen op te lossen, en dus de kwestie van ontmoetingen tussen individuen, met andere woorden als we denken dat geld een voorwaarde is voor socialiteit, dan zullen we het heel moeilijk vinden om economische waarde te onderscheiden van ethische en politieke waarden.

Het zijn onze westerse samenlevingen die zeer recent van de mens een 'economisch dier' hebben gemaakt. Maar we zijn nog niet allemaal economische dieren. Homo oeconomicus ligt niet achter ons, maar voor ons; net als de mens van moraliteit en plicht; net als de mens van wetenschap en rede. Heel lang was de mens iets anders; en het is nog niet zo lang geleden dat hij een ingewikkelde machine werd, uitgerust met een rekenmachine.

We proberen hier waarde te geven aan andere facetten van menselijkheid dan de economische relatie van het uitwisselen van goederen.

Waarde die iedereen in staat stelt zich te ontplooien in een permanente ontmoeting met anderen binnen een socialiteit die gebaseerd is op de principes van delen en wederzijdse hulp.

> Sommige mensen zien de dingen zoals ze zijn en vragen zich af 'Waarom? Ik droom van dingen die er nooit zijn geweest en vraag 'Waarom niet? George Benard Shaw

## Vergelijking met huidige ideologieën

Deze presentatie leidt onvermijdelijk tot de creatie van een nieuwe ideologie. We geloven dat het essentieel is om parallellen te trekken met bestaande ideologieën en uit te leggen waarom ze anders zijn en waarom deze andere ideologieën ons niet duurzaam of effectief lijken.

Maar het idee is niet om deze ideologieën te verwerpen, want de waarden achter links, gelijkheid, en rechts, vrijheid, zijn mooi en noodzakelijk voor een Eutopia, maar nu gaat het erom het goede te verzamelen. Zodat we samen kunnen komen.

Zodat we niet alleen maar zeggen "Jullie hebben het allemaal mis", maar "Jullie hebben allemaal gedeeltelijk gelijk".

Vanaf nu gaan we niet de confrontatie aan met bestaande bewegingen. Integendeel, we geven er de voorkeur aan vriendschappelijke relaties te onderhouden, openheid te creëren en anderen te steunen in projecten die in lijn zijn met de waarden die wij verdedigen. Het idee is om anderen te respecteren om wat ze geloven, zodat ze ons ook kunnen respecteren om wat we geloven. Wij geloven dat waarheden uiteindelijk vanzelf naar boven komen en dat we ons niet moeten laten storen in onze overpeinzingen, met het risico dat we ons afsluiten van anderen. Wij geloven dat dit de enige manier is waarop we uit de afgrond kunnen komen waarin we ons bevinden, door ondanks onze verschillende meningen samen te blijven, verenigd in de gemeenschappelijke zoektocht naar vervulling.

### Vergelijking met links

De ideologische systemen die pleiten voor meer planning, of zelfs totale planning, zijn degenen die het meest zouden kunnen lijken op dat van ons Eutopia, maar een paar woorden zijn genoeg om het praktische systeem dat ontstaat volledig te veranderen.

Socialisme = "Van ieder naar zijn vermogen en aan ieder naar zijn werk".
Communisme = "Van ieder naar vermogen en aan ieder naar behoefte"[^27].
Wederkerigheid = "Van ieder, vrijwillig, naar de behoeften van anderen"[^3].

De ontoereikendheid en tekortkomingen van deze andere ideologieën zijn deels de essentiële oorzaken van wat de wereld vandaag de dag is. Ze hebben aanleiding gegeven tot afwijkingen, tot antikapitalistische revoluties, maar daardoor zijn ze er min of meer niet in geslaagd om samenlevingen van een nieuw type over een groot deel van de wereld te creëren. En omdat het resultaat duidelijk niet beter is dan het ontwikkelde kapitalisme, hebben ze het beschermd. Ze worden nu door alle partijen gebruikt om de meest gigantische misleiding en hypocrisie in de geschiedenis te vormen.

Gelijkheid tussen mensen, hoe ook gedefinieerd, kan een grens zijn voor de vrijheid van sommige mensen. En vrijheid alleen is over het algemeen geen garantie voor gelijkheid.

De beste samenleving is waarschijnlijk heel anders dan de samenlevingen die we kennen, zoals onze samenleving van dominante en gedomineerde sociale klassen. Maar degenen die hebben geprobeerd het opnieuw uit te vinden hebben gefaald, in de eerste plaats omdat ze de samenlevingen die ze voorstellen niet genoeg specificeren.

Als deze analytische benadering nodig was, dan moeten we nu verder gaan als we het werk van genetische denaturalisatie willen voortzetten dat met name door de beroemde Karl Marx is ondernomen [^28].

Bovendien kan het Marxiaanse systeem niet nadenken over geld, noch voor noch na het kapitalisme. Naast het feit dat het tweeduizend jaar menselijke geschiedenis in de schaduw stelt, kan het ook geen enkele uitweg voorstellen.

De moeilijkheden van Marx' denken liggen in de mogelijkheid om na te denken over een andere vorm van organisatie van arbeid en economische socialiteit dan die van de kapitalistische productiewijze.

Dus in het tijdperk van de noossfeer hebben marxistische bewegingen die 150 jaar oude theorieën met zich meeslepen grote moeite met het opnieuw bedenken van horizontale arbeidsorganisaties met zelfbeheer, met het opnieuw uitvinden van onze democratische processen, met het bedenken van een andere wereld dan die van het productivisme, met het begrijpen van het diepe wezen dat we zijn en de middelen om dat te transformeren. Ze vechten maar al te vaak TEGEN het kapitalisme en vinden het moeilijk om met echte alternatieve oplossingen te komen waar we vóór kunnen vechten.

We hebben hier geen kritiek op het meesterlijke werk van Marx, dat zeer noodzakelijk was voor reflectie, maar we denken wel dat als hij oprecht had toegegeven dat hij ook met ethiek bezig was en daar veel meer van in zijn werk had gestopt, als hij meer had gezegd over de uiteindelijke of goede samenleving en de middelen om die te bereiken, als hij net zoveel altruïsme in zijn karakteristieken had gestopt als hij in zichzelf had, er misschien minder van het ergste zou zijn gebeurd[^3].

### Vergelijking met rechts

Rechts beleid, d.w.z. beleid dat de markt bevordert, of zelfs de totale vrije markt, is de dominante ideologische stroming in westerse landen, althans degene die aan de macht is, en het is zelfs de dominante ideologische stroming in de wereld, en degene die de grootste impact heeft.

Iedereen kan de negatieve effecten zien, niet alleen op de planeet en onze ecosystemen, maar ook op ons algehele welzijn. Kortom, in dit economische systeem verspillen we onze tijd en verspillen we ons leven met 'verdienen'. En degenen die hun tijd niet verspillen, of ze dat nu doen of niet, plukken nog steeds de rampzalige gevolgen, en zo niet op de korte termijn, dan wel op de lange termijn.

In de markt hebben mensen altijd kettingen. Maar meestal zijn het geen ruwe kettingen van roestig staal meer; ze zijn 'fijn vernikkeld', veel 'steviger' en uiteindelijk nog moeilijker te breken, vooral omdat sommige mensen er gouden schakels in stoppen. We hebben dan het gevoel dat we iets kostbaars te verliezen hebben.

Ons systeem sluit elk individu in door alle anderen, in die zin dat een persoon simpelweg niet, alleen en vrij, zijn of haar beste kwaliteiten kan manifesteren. In de economie, in een concurrerend systeem, kan een bedrijf, op straffe van faillissement, niet proberen zijn winst te maximaliseren als anderen proberen hun winst te maximaliseren. Op dezelfde manier kan iemand die geeft niet overleven in een maatschappij waar anderen nemen.

Er zit wat goeds in dit systeem, zoals de vrijheid van ondernemerschap en de algemene 'vrijheid' die heerst, en het valt niet te ontkennen dat dit effect heeft gehad op veel ontwikkelingen. Maar we kunnen ook de gevolgen niet ontkennen, want waar sommigen zich bevonden, deden anderen dat niet... zij die werden verwaarloosd, uitgebuit, vergeten... En het is hoog tijd om verder te gaan en eindelijk iedereen erbij te betrekken.

Dit is waar deze eerste versie van onze analyse van de vergelijking met rechts eindigt. Niet dat er niet veel te zeggen valt - integendeel, er is een enorme hoeveelheid literatuur over kapitalisme waarin de oorsprong, oorzaken en gevolgen ervan uitvoerig worden beschreven. Maar de manier waarop we naar deze ideologieën willen kijken, de relaties die we met hen willen onderhouden en tot slot alle vragen die verband houden met de toewijding van onze beweging aan concrete politiek, moeten nog collectief worden gedefinieerd. Als je echter het gevoel hebt dat je hier niet genoeg over weet, stellen we voor dat je de vele bronnen raadpleegt die beschikbaar zijn in de literatuur en op het internet:[^29][^31].

## Bronnen (Niet-uitputtende lijst)

[^1]: Earth Overshoot day 2017 viel op 2 augustus, Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipedia : Economie](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: Het einde van de groei. Richard Heinberg. 2013
[^5]: Sébastien Bohler. De menselijke insect. 2020
[^6]: Karl Polanyi. "The great transformation" en "The economy as Instituted Process" in Trade and Market in the Early Empires: Economies in History and Theory.
[^7]: Marshall Sahlins - Economie uit de Steentijd.
[^8]: Marcel Mauss- Essay on the Gift
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Sociale organisatie van de Inca's](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Waar is de betekenis? 2020
[^12]: David Graeber. Schuld: 5000 jaar geschiedenis. 2011
[^13]: Lee Alan Dugatkin. The Altruism Equation: Zeven wetenschappers op zoek naar de oorsprong van goedheid. 2006
[^14]: [Dunbar's Number](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Een korte geschiedenis van de mensheid
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion.
[^17]: Daniel Kahneman. Snel en traag denken
[^18]: [Ernst Fehr, De aard van het menselijk altruïsme](https://www.nature.com/articles/nature02043)
[^19]: [De behoefte aan sociale goedkeuring](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religie en moraliteit](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruism in children and the perceived conduct of others in Journal of Abnormal and social Psychology, 1942, 37, pp. 2018-233.
[^22]: Gabriel Tarde. De wetten van imitatie, 1890
[^23]: Charles Duhigg. De kracht van gewoonte. 2014
[^24]: Émile Durkheim. Onderwijs en sociologie; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.FOCUS THEORIE VAN NORMATIEF CONDUCT: EEN THEORETISCHE VERFIJNING EN HERVALUATIE VAN DE ROL VAN NORMEN IN MENSELIJK GEDRAG
[^26]: [Is de etymologie van het woord werk tripalium?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Kritiek van het Gotha Programma (1875). 1891
[^28]: Karl Marx. Kapitaal. 1867
[^29]: [Conferentie: Communisme & Kapitalisme: de geschiedenis achter deze ideologieën](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. De orde van het geld - Kritiek op de economie. 2019
[^31]: [Arte 2014 Documentaire - Ilan Ziv - Kapitalisme](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
