---
title: Samen beslissen
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Deze pagina wordt nog geschreven, dus sommige delen kunnen ontbreken.
> {.is-info}

## Hoe kunnen we allemaal samen beslissen?

We moeten begrijpen hoe we beslissingen kunnen nemen en ervoor zorgen dat dit de juiste beslissingen zijn om ons gemeenschappelijke doel te bereiken.

Er bestaan veel besluitvormingshulpmiddelen en er moeten er nog veel meer worden uitgevonden. Hier zullen we de kleinschaligere precisie-instrumenten buiten beschouwing laten en ons concentreren op de globale.

In onze westerse samenleving is democratie het gebruikte instrument. Door democratie te analyseren en opnieuw uit te vinden, kunnen we vervolgens kleinschaligere hulpmiddelen ontwikkelen die erop gebaseerd zijn en die vervolgens aangepast kunnen worden aan andere organisatievormen, zoals bedrijven, scholen, enz.

## Wat is democratie?

Democratie is de Griekse combinatie van demos "het volk" en kratos "macht". Tegenwoordig wordt de term gebruikt om elk politiek systeem te beschrijven waarin het volk soeverein is[^1].

Veel landen hebben gelijkheid van politieke macht bereikt door middel van algemeen kiesrecht. Deze overwinning is nu verankerd in hun grondwetten en wordt beschouwd als een van de meest briljante institutionele juwelen van hun samenlevingen.

Maar "Democratie" is nog steeds een veel verguisd en misbruikt woord.

We verkondigen democratie alsof we echt de macht hebben.

Maar onze democratie is niets meer dan het steeds weer afstaan van onze legitieme macht aan de politici die er beslag op leggen.

En als ze die eenmaal hebben, voelen ze zich legitiem omdat ze die hebben, maar zelfs zij staan onder controle van een veel grotere macht.

We denken dat het politieke systeem domineert en dat het al het andere kan veranderen, inclusief de economie en haar vorm. We denken dat ons systeem perfect democratisch is vanwege de competitie tussen de kandidaten waarop we stemmen.

Maar we zullen zien dat de economie een rol speelt in deze competitieve keuze. Dus aan de ene kant heeft het kiesstelsel niet langer de kwaliteit van het bieden van goede concurrentie tussen kandidaten, en aan de andere kant domineert het de economie niet omdat de economie het beïnvloedt[^2].

Het is dus de economie die de politiek domineert.

Over klimaatkwesties bijvoorbeeld, tonen studies aan dat er geen correlatie is tussen wat de meerderheid van de mensen wil en wat ze krijgen, behalve wanneer ze dezelfde voorkeuren hebben als de rijkste 10% van de bevolking[^3].

Onze democratie moet volledig op de schop, en dan hebben we het over vele gebreken: machtsmisbruik, corruptie, de invloed van lobby's, ongecoördineerde, niet gedeelde beslissingen, gebreken in representativiteit, illusies van keuze, ongelijkheid van kansen.

En hier ontdekken we dat het echte resultaat van ons democratisch systeem het tegenovergestelde is van zijn ideologische aspiratie.

Ondanks al deze gebreken is democratie een risico dat we moeten nemen[^4] als we onze vrijheid willen behouden.

Dus laten we beginnen met het analyseren van onze democratie, begrijpen wat er mis mee is en dan een heel ander democratisch systeem opnieuw uitvinden.

## Het probleem met onze democratieën

### De politieke ondernemer[^5][^6][^7]

Het doel van elke politicus is om aan de macht te komen en te blijven. Hij maakt er carrière mee, waardoor hij in zijn levensonderhoud kan voorzien en zijn plaats in de maatschappij kan vinden.

Ze concurreren met elkaar, maar werken ook samen: ze willen hun politieke tegenstander vernietigen, maar niet het politieke ambt.

Alle politici, ongeacht hun oppositie, hebben een gemeenschappelijk belang bij het versterken, niet het verminderen, van de macht van politici. En als een van hen het idee oppert, zal hij de afkeuring van alle anderen krijgen.

De overgrote meerderheid van de politieke ondernemers komt uit dezelfde sociale klasse, en meestal ook uit dezelfde sociale klasse als de leiders van de particuliere sector en dezelfde burgerlijke families.

Kandidaten proberen niet het programma te presenteren dat hun ideaal van de goede samenleving vormt.

Wat hun persoonlijke doelen ook zijn, ze kiezen het programma dat het grootste aantal het beste uitkomt. Ze offeren de verdediging van hun eigen meningen, idealen en persoonlijke voorkeuren, als ze die hebben, op aan wat de massa van de bevolking wil.

Toch wordt deze kennis van de wensen van de kiezers door de kandidaten meestal niet gebruikt om het grootste aantal te dienen, maar, zoals we zullen zien, om ze uit te buiten ten voordele van de kleinsten.

Politici reageren gewoon op de prikkels die hen in staat stellen om aan de macht te komen en te blijven.

Het heeft geen zin om ze te haten, ze volgen gewoon de regels van een spel. Het is dus het spel dat gehaat moet worden, niet de spelers[^8].

Maar het probleem met politieke ondernemers is dat hun doel niet is om gelijk te hebben, maar dat de mensen en bedrijven hun gelijk moeten bewijzen.

Politieke ondernemers zijn niet dom, het zijn goede tactici die reageren op de prikkels van het spel en zeer goed geadviseerd worden door de expertise van hun team in het veld, dus gebruiken ze verschillende strategieën en allianties om hun doel te bereiken.

Om een goed imago neer te zetten, verbergen de kandidaten hun persoonlijkheid, vermommen ze zichzelf en worden ze hypocriet.

Kiezers denken dat ze weten op wie ze stemmen, omdat de nadruk vaak ligt op de persoonlijkheid van de kandidaat, waarbij zijn of haar verleidelijkheid, geruststellende karakter of charisma wordt benadrukt, wat een halo-effect creëert. En deze indrukken zijn veel gemakkelijker te manipuleren door de media dan de inhoud van een programma.

### Politieke programma's[^2]

Kandidaten bouwen hun "programma" niet in één keer op, maar door opeenvolgende toevoegingen of wijzigingen van uitspraken en daden, zonder echter veel terug te kunnen draaien, eerdere woorden of daden uit te wissen of tegen te spreken. Deze beperking kan worden verklaard door de tijd die het kost om een programma bekend te maken, terwijl het effectiever is om het gaandeweg te verduidelijken, aan te vullen, te versterken.

Het wordt ook verklaard door het feit dat deze tegenspraken of ontkenningen de betrouwbaarheid en geloofwaardigheid van de kandidaat zouden schaden, waardoor een evoluerende, iteratieve structuur van kennis en programmaconstructie ontstaat.

Dit leidt vaak tot verschillen in hun programma's en een onvolkomenheid die het democratische gehalte van de uitkomst van het proces belemmert.

Ten tweede zijn de programma's globaal, ze bestrijken alle onderwerpen tegelijk, en je moet tussen ze kiezen, hoewel je misschien op één punt de voorkeur geeft aan de ene en op een ander punt aan de andere.

In dit geval verdelen de programma's de bevolking, en één van hen zou door een meerderheid gekozen kunnen worden, terwijl diezelfde meerderheid één punt van alle andere programma's verkiest boven dat van de winnaar.

Dit is iets wat specifieke referenda mogelijk maken, in het bijzonder om je mening te geven over elke vraag apart.

Het effect van financiële bijdragen uit eigenbelang is om programma's te standaardiseren, terwijl het effect van idealistische/activistische steun is om ze te differentiëren.

### Eerlijkheid en transparantie

Elk systeem dat gemanipuleerd kan worden, zal op het slechtst mogelijke moment gemanipuleerd worden[^9].

Wanneer je een systeem ontwerpt, moet je je absoluut afvragen hoe de belanghebbenden in het systeem in staat zullen zijn om het aan te vallen en oneerlijk voordeel uit het systeem te halen.

Eerlijkheid en transparantie zijn hardnekkige problemen in democratische systemen over de hele wereld. Politici en gekozen vertegenwoordigers worden er vaak van beschuldigd niet transparant te zijn, corrupt te zijn en verwikkeld te zijn in talloze belangenconflicten door bepaalde belangen te bevoordelen boven het algemeen belang.

In ons systeem zijn bepaalde regels niet bevorderlijk voor eerlijkheid en ethiek.

Het is daarom cruciaal om systemen te ontwerpen die mensen aanmoedigen om ethisch en moreel te handelen.

We moeten het principe van openbaarmaking ontwikkelen waarbij deelnemers er belang bij hebben om eerlijk te zijn in hun openbaarmakingen en zich ethisch en moreel te gedragen, omdat dit in hun eigen belang zou zijn en ze er op de lange termijn baat bij zouden hebben. Er moeten meer transparantiemaatregelen komen.

### De keuze van alternatieven

Afgezien van het feit dat de programma's globaal zijn, is de keuze van programma's of kandidaten die aan de kiezer worden aangeboden over het algemeen klein.

De financiering van verkiezingscampagnes is een van de factoren die de keuze van alternatieven beperkt. Kandidaten zijn degenen die toegang hebben tot financieringsbronnen en die genoeg informatie en propaganda over zichzelf kunnen verspreiden om hun partij zichtbaar te maken en een kans te maken om te winnen[^10].

Politieke partijen kunnen interne structuren hebben die bepaalde kandidaten of politieke stromingen bevoordelen boven andere. Dit kan de keuzes van kiezers beperken door hen slechts een paar opties te geven die door de politieke partijen zijn goedgekeurd.

### Delegatie van macht

Een verkiezing bestaat uit twee dingen: een stemming en een delegatie van macht.

Zoals hierboven vermeld, is onze democratie het steeds weer afstaan van legitieme macht van het volk aan een groep mensen.

Het delegeren van macht kan ook leiden tot een verlies van controle over beslissingen. Als een persoon of groep zijn macht delegeert aan een andere persoon of groep, kan hij de controle verliezen over welke beslissingen worden genomen en hoe ze worden uitgevoerd.

Onder het voorwendsel van delegatie, waardoor gekozen vertegenwoordigers volledige legitimiteit krijgen, kan er sprake zijn van verduistering, vervreemding, verkwisting of usurpatie van macht.

Een van de grootste problemen met het delegeren van macht is het risico op machteloosheid. Als een persoon of groep zijn of haar beslissingsbevoegdheid delegeert aan een andere persoon of groep, kan hij of zij zichzelf ontslaan van verantwoordelijkheid in geval van falen of fouten van de persoon of groep aan wie hij of zij zijn of haar macht heeft gedelegeerd.

Een ander probleem met het delegeren van macht is het risico op belangenverstrengeling.
Sommige handelingen vereisen tijd, energie, voorkennis en intellectuele capaciteit. In dergelijke gevallen kan het voordelig of zelfs noodzakelijk zijn om gebruik te maken van arbeidsverdeling en specialisatie door een deel van de besluitvorming te delegeren. Maar deze delegatie is macht. Een overheid kan bijvoorbeeld haar macht delegeren aan een bedrijf om een project te beheren. Maar aangezien de gedelegeerde niet precies weet wat hij delegeert, beschikt hij per definitie niet over alle informatie over de mogelijkheden. Dit zet de deur open voor allerlei vormen van misbruik[^2].

### Centralisatie van macht

Macht wordt gecentraliseerd als enkelen veel beslissen en velen weinig.

Centralisatie verhoogt het delegeren van macht, wat op zijn beurt de democratie vermindert.

Wanneer een kleine groep mensen de absolute macht heeft, kan dit leiden tot eenzijdige besluitvorming en oneerlijk beleid dat geen rekening meer houdt met de belangen en meningen van burgers.

De centralisatie van macht kan regeringen kwetsbaarder maken voor machtsmisbruik, tirannie en dictatuur, omdat er minder checks and balances zijn om de autoriteit van een kleine groep mensen te beperken.

### Stemsystemen

Het is belangrijk om te beseffen dat een verkozen kandidaat wordt verkozen door middel van een stembiljet en dat het wijzigen van het stembiljet zal veranderen wie wordt verkozen.

Het verkiezingsproces heeft een grote invloed op de uitkomst van een stemming. De keuze van het stembiljet beïnvloedt dus de toekomst van onze samenlevingen. Het is daarom belangrijk om ons te concentreren op de keuze van de stemming die we willen invoeren.

Verkiezingen met één of twee stemrondes hebben een fundamenteel probleem: ze laten niet toe om de voorkeuren van de kiezers duidelijk en precies uit te drukken[^11].

In een één-ronde-systeem kan de kiezer slechts op één kandidaat stemmen, wat problematisch kan zijn als meerdere kandidaten aanzienlijke steun hebben. Bijvoorbeeld, in een verkiezing waar er drie belangrijke kandidaten zijn, moet de kiezer kiezen tussen drie verschillende opties, maar heeft slechts één stem om zijn of haar keuze uit te drukken. Dit kan leiden tot situaties waarin de gekozen kandidaat niet de steun heeft van de meerderheid van de kiezers, maar gewoon het hoogste aantal stemmen.

Het probleem met verkiezingen met twee ronden is dat ze soms kunnen leiden tot strategische stemtactieken en allianties tussen kandidaten. In een tweerondesysteem staan de twee kandidaten met de meeste stemmen in de eerste ronde tegenover elkaar in een tweede ronde. Dit kan leiden tot situaties waarin kiezers anders stemmen in de eerste ronde om ervoor te zorgen dat hun voorkeurskandidaat zich kwalificeert voor de tweede ronde, zelfs als ze niet echt overtuigd zijn van die kandidaat. Daarnaast kunnen er gevallen zijn waarin kandidaten hun krachten bundelen om een andere kandidaat uit de race te drukken, in plaats van te proberen de verkiezing onafhankelijk te winnen.

Over het algemeen heeft het stemmen bij meerderheid het grote nadeel dat het geen rekening houdt met de intensiteit van de voorkeuren van mensen. Degenen die bijna onverschillig zijn tussen twee alternatieven en degenen die de ene veel meer verkiezen dan de andere, hebben hetzelfde gewicht in de keuze tussen hen. Bijvoorbeeld, 1001 burgers die nauwelijks voorkeur hebben voor één alternatief wegen zwaarder dan de 1000 anderen voor wie het de ergste ramp is.

Deze peilingen moedigen nuttig stemmen aan, wat politici aanmoedigt om zich in politieke partijen te organiseren en kiezers in vooraf bepaalde hokjes te plaatsen, wat het bipolarisatie-effect versterkt.

Bipolarisatie van het electoraat in de politiek verwijst naar de neiging van kiezers om zich te groeperen rond twee belangrijke politieke partijen, waardoor er een duidelijke politieke kloof ontstaat tussen deze twee partijen. Dit laat weinig ruimte over voor onafhankelijke partijen of kandidaten, of voor kleinere politieke partijen. Dit heeft ook als negatief gevolg dat de polarisatie van de opinie toeneemt en de diversiteit van standpunten afneemt.

Met deze stembiljetten stemmen we uiteindelijk tegen een kandidaat en niet voor hem of haar.

Maar een nuttige stem maakt de gekozen persoon nog niet legitiem.

Evenredige vertegenwoordiging betekent dat de politiek niet te veel gepolariseerd is, maar het kan leiden tot een versnippering van de macht en het moeilijk maken om stabiele regeringen te vormen. Het wordt moeilijk voor één partij om een absolute meerderheid van zetels te behalen, wat kan leiden tot onstabiele regeringscoalities of minderheidsregeringen die het moeilijk vinden om hun agenda vooruit te helpen.

Een goede verkiezing moet onafhankelijk zijn van irrelevante alternatieven en de deur sluiten voor het dilemma van de nuttige stem.

Voorbeeld van een mogelijke alternatieve stem:

Approval voting: elke kiezer kan zoveel kandidaten goedkeuren als hij of zij wil, zonder volgorde van voorkeur. De kandidaat met de meeste goedkeuringen wordt verkozen.

Stemming op basis van punten (of gewogen stemming) is een alternatief kiessysteem waarbij elke kiezer een bepaald aantal punten heeft die hij of zij kan verdelen tussen de kandidaten.

Meerderheidsstemming: kiezers geven elke kandidaat een score, bijvoorbeeld van "zeer goed" tot "afkeur". De kandidaat met de hoogste gemiddelde score wordt verkozen.

Condorcet voting is een stemsysteem waarbij kiezers stemmen door alle kandidaten twee aan twee te vergelijken[^12].

Randomised Condorcet voting: hier vergelijkt elke kiezer paren van kandidaten zoals bij Condorcet voting, maar in plaats van rekening te houden met alle stemmen van alle kiezers, wordt een aantal vergelijkingen willekeurig gekozen om de winnaar te bepalen.

Mehestan (Zonnebloem) stemming: Dit is een stemsysteem dat het stemmen op punten en de Condorcet stemming combineert. Het houdt in dat je elke kandidaat één voor één vergelijkt en zegt welke kandidaat je zou verkiezen voor elk criterium, maar ook het niveau van voorkeur voor dat criterium bepaalt. Deze stemmethode is bedoeld om zowel eerlijk als representatief te zijn, zodat kiezers op hun voorkeurskandidaat kunnen stemmen zonder het risico te lopen de stemmen te splitsen en rekening te houden met de voorkeuren van alle kiezers bij het bepalen van de winnaar.

### Politieke partijen

De hierboven beschreven stemsystemen moedigen politici aan om zich in partijen te organiseren, omdat ze afhankelijk zijn van irrelevante alternatieven.

### Institutionele traagheid

In veel gevallen vereisen politieke problemen snelle en effectieve oplossingen om schade en verlies tot een minimum te beperken. Maar trage politieke processen, zoals onderhandelingen, debatten, stemmingen en bureaucratie, kunnen de besluitvorming vertragen en verhinderen dat regeringen snel handelen.

### Korte termijn mandaten

De gekozen vertegenwoordigers in kwestie worden gekozen voor een bepaalde periode, meestal een paar jaar. Gedurende deze periode hebben ze een aanzienlijke persoonlijke vrijheid om hun acties te kiezen. Kiezers dwingen hen op geen enkele manier meer. Afgezien van hun eer is er weinig dat de verkozenen dwingt om hun verkiezingsbeloften na te komen.

Gekozen vertegenwoordigers zijn vaak meer geneigd om zich te richten op kortetermijnresultaten dan op langetermijnuitdagingen.

Dit komt omdat deze mandaten de verkozenen aanmoedigen om zich te concentreren op beleidskwesties die meer kans maken om snel de steun van de kiezers te krijgen, in plaats van op complexere kwesties die een groter engagement op langere termijn vereisen.

Politici kunnen in de verleiding komen om beslissingen te nemen die op korte termijn populair zijn om hun kansen op herverkiezing te maximaliseren. Dit kan leiden tot beleid dat op de lange termijn niet levensvatbaar of duurzaam is en in de toekomst grotere problemen kan veroorzaken. Dit leidt tot een politieke benadering van bestuur in plaats van een meer pragmatische en weloverwogen benadering.

### Bluffen om deskundigheid

De "expertisebluf" in de politiek doet zich voor wanneer politici hun positie met een graad in wetenschap of bestuur gebruiken om de indruk te wekken dat ze expertise of speciale kennis over onderwerpen hebben, terwijl dat in werkelijkheid niet zo is. Ze kunnen deze strategie gebruiken om kiezers te manipuleren of te overtuigen, maar het kan leiden tot slechte politieke beslissingen.

De nadruk op wetenschappelijke en administratieve diploma's leidt tot een concentratie van macht en invloed in de handen van een paar elites die het geluk hebben gehad om toegang te krijgen tot deze opleidingen. Dit leidt tot een verlies aan diversiteit en inclusie in besluitvormingskringen, omdat mensen met een minder bevoorrechte achtergrond vaak minder kansen hebben om deze diploma's te behalen.

Toch garandeert het bezit van een diploma niet noodzakelijk vaardigheden of expertise in bestuur of politiek. De expertisebluf, die we eerder bespraken, kan gebruikt worden door mensen die een diploma hebben maar niet over de echte kennis of vaardigheden beschikken om weloverwogen beslissingen te nemen[^2].

### Wetenschap geminacht

Politieke beslissingen moeten vaak worden genomen op basis van complexe wetenschappelijke gegevens en empirisch bewijs, vooral op gebieden zoals volksgezondheid, klimaatverandering, voedselveiligheid enzovoort. Wanneer beleidsmakers wetenschappelijk bewijs negeren, nemen ze beslissingen die niet gebaseerd zijn op feiten en verifieerbare gegevens, maar eerder op overtuigingen of meningen. Dit kan ernstige gevolgen hebben voor de samenleving, vooral op het gebied van gezondheid, milieu en veiligheid.

### Propaganda

Kiezers stemmen niet op een kandidaat waar ze niets over weten. Zonder informatie over de kandidaat die aan de kiezers wordt doorgegeven, heeft een kandidaat geen kans. Ze beginnen pas een kans te maken als de informatie die ze ontvangen een bepaalde hoeveelheid overschrijdt: hun kansen om gekozen te worden nemen toe als de informatie die ze ontvangen toeneemt.

Politieke investering is een investering zoals elke andere, het doel is altijd hetzelfde: winst.

De geschiedenis van de vestiging van de "verkiezingsdemocratie" loopt parallel met de strijd en de formele overwinningen voor de vrijheid van meningsuiting, pers, vergadering en vereniging. Maar deze vrijheden zijn, vanwege hun noodzakelijke materiële en economische basis, voor het grootste deel overgenomen door degenen die deze economische middelen bezitten. Om ze vervolgens te gebruiken voor verkiezingspropaganda[^24].

Propaganda wordt gebruikt om de publieke opinie te manipuleren en de resultaten van verkiezingen of referenda te beïnvloeden ten gunste van een bepaalde partij of ideologie. Het kan misleidend en partijdig zijn en gebaseerd op leugens, halve waarheden of het weglaten van belangrijke feiten. Het kan ook inspelen op de angsten, vooroordelen en emoties van mensen in plaats van op rede en logica.

### De politieke hooligan

We kunnen 3 karikaturale types burgers onderscheiden tijdens verkiezingen:[^13][^14]

- De hobbits: degenen die geen interesse hebben in politiek en erg onwetend zijn op dit gebied, ze hebben niet al te veel vooroordelen.
- Hooligans: zijn bevooroordeeld, maar erg bevooroordeeld, vallen vaak ten prooi aan confirmation bias, en verdedigen waarden irrationeel.
- Vulcans: mensen die in staat zijn tot rationeel denken.

Onze modellen gaan ervan uit dat alle burgers Vulcans zijn of kunnen worden, maar in de praktijk zijn de meeste mensen ofwel hobbits ofwel hooligans.

Intuïtie dwingt ons om een mening te vormen en de rede wordt dan gebruikt om deze meningen te ondersteunen. We rationaliseren onze overtuigingen.

We gebruiken onze rede niet om de waarheid te doorgronden, maar om de ideologische standpunten die we al hebben ingenomen te rechtvaardigen.

Als we bijvoorbeeld het woord "democratie" uitspreken, zegt onze intuïtie ons dat dit een goede zaak is en dan manipuleert onze rede de definitie van het woord om onze intuïtie te rechtvaardigen.

Tegenwoordig is het gemakkelijk om onze intuïtie te bevestigen dankzij het internet, dat ons kan voorzien van alle argumenten die we nodig hebben, en als deze argumenten of cijfers niet onze kant op gaan, kunnen we altijd de motieven van de verslaggever van de feiten in twijfel trekken of ze op een andere manier interpreteren.

Bovendien, door ons te omringen met gelijkgestemde mensen, versterkt onze omgeving voortdurend onze intuïtie en heeft ze de neiging om te polariseren.

Politieke activisten worden overmoedig en het wordt onmogelijk om hen op andere gedachten te brengen, zelfs met feiten en cijfers.

En als intuïtie op deze manier overheerst, hoe meer je jezelf informeert, hoe meer goede redenen je vindt om te geloven wat je gelooft, welke informatie je ook leest.

Politieke betrokkenheid versterkt alleen maar het politieke hooliganisme.

Hoe meer we geïnteresseerd raken in een politieke partij met een prijskaartje, hoe radicaler onze aanvankelijke neiging zal worden.

Misleidende maar goed uitgelegde wetenschappelijke video's en artikelen versterken de wetenschappelijke overtuiging van degenen die ze bekijken en lezen.

Omdat groepsselectie effectiever is dan individuele selectie, zijn de stammen die hebben overleefd die waar individuen voldoende hebben samengewerkt om het voortbestaan van de groep te garanderen.

En dus degenen die bereid zijn hun individuele belangen op te offeren voor het welzijn van de groep. Natuurlijke selectie heeft geselecteerd op politiek hooliganisme.

Een stem gebaseerd op politiek hooliganisme levert plezier op, terwijl een rationele stem mentale inspanning vereist.

### Onwetendheid en irrationaliteit van burgers

Mensen zijn onwetend, bijna alle kiezers hebben geen notie van wetenschap of begrip van het politieke systeem, zijn slecht geïnformeerd en kennen de cijfers niet[^16].

Sommigen zijn zich er niet van bewust dat ze aan politiek hooliganisme doen.

De meerderheid van ons is zich niet bewust van de verschillende cognitieve vooroordelen en effecten waarmee we te maken hebben. We hebben er een aantal besproken in hoofdstuk 3 over HomoSapiens. Hier zijn er een paar ter herinnering:

- Confirmation bias[^17]
- Het halo-effect[^18]
- Cognitieve dissonantie[^19]
- Paarddruk
- Beschikbaarheidsrisico[^20]

School en afgestudeerden kunnen mensen laten geloven dat ze geleerden zijn, hen laten denken dat ze slimmer zijn dan anderen en overmoed creëren op gebieden die niet hun eigen zijn.

Iedereen wordt zo zeker van zichzelf. En het probleem is dat onzekerheid wordt gezien als een teken van zwakte, terwijl het in feite een fundamentele kwaliteit is.

Het Dunning-Kruger effect laat zien dat mensen met weinig kennis of vaardigheden op een bepaald gebied de neiging hebben om hun eigen niveau van beheersing te overschatten, terwijl mensen met hogere vaardigheden de neiging hebben om hun niveau van beheersing te onderschatten[^16].

Maar de onwetendheid van burgers is niet het grootste probleem, want zelfs het informeren van burgers voorkomt niet dat ze irrationeel stemmen.

Mensen gedragen zich als hooligans, niet als wetenschappers.

### Tijd en energie om geïnformeerd te worden

Geïnformeerd zijn, leren, weten, begrijpen, vervolgens kiezen en beslissen, en stemmen, vereist tijd, energie en voorkennis van de kiezer, wat op zijn beurt tijd en moeite kost. Deze kosten verklaren een zekere machtsdelegatie.

Verkiezingscampagnes zijn vaak kort en intensief, waardoor kiezers weinig tijd hebben om zichzelf te informeren en de verschillende opties te beoordelen. Daarnaast hebben de media vaak de neiging om zich te richten op de meest spectaculaire of controversiële aspecten van politieke gebeurtenissen, in plaats van een diepgaande, genuanceerde analyse te geven.

Dit kan leiden tot een polarisatie van de politiek en een vermindering van de kwaliteit van het publieke debat. Kiezers kunnen in de verleiding komen zich te wenden tot simplistische of partijgebonden informatiebronnen die niet noodzakelijkerwijs de complexiteit van de kwesties weerspiegelen.

### Communitarisme

Communitarisme kan gedefinieerd worden als de neiging om meer belang te hechten aan het behoren tot een groep of gemeenschap dan aan het individu als zodanig. Het grootste probleem met communitarisme is dat het kan leiden tot een versplintering van de samenleving, waar elke groep zich richt op zijn eigen belangen in plaats van het gemeenschappelijke belang van de samenleving als geheel[^21].

Communitarisme was vroeger geografisch, maar verspreidt zich nu over het hele internet.

Niemand is graag in de minderheid.

Wanneer internetgebruikers niet meer deelnemen aan een discussie omdat ze het gevoel hebben dat ze te veel in de minderheid zijn, leidt dit tot ideologische segregatie.

Het veranderen van ideologische buurten op het internet is veel gemakkelijker dan geografisch verhuizen.

Mensen kunnen gemeenschappen waar ze zich in de minderheid voelen verlaten om zich aan te sluiten bij gemeenschappen waar ze in de meerderheid zijn.

Filterbubbels zijn wat we ideologische isolaties op het web noemen.

We sluiten onszelf op in een wereld waarin alles wat we lezen en horen bevestigt wat we al denken, wat het moeilijk maakt om onze overtuigingen in twijfel te trekken en te begrijpen waarom anderen denken wat ze denken.

Deze filterbubbels worden versterkt door de gepersonaliseerde inhoud die wordt aangeboden door de webgiganten: Facebook, YouTube, enz. Ze sluiten ons op in de gemeenschap waarmee we onze ideeën delen en leveren inhoud op maat. Elke keer dat we op een link klikken of een bericht leuk vinden, sluiten ze ons verder op in een gemeenschap met wie we onze ideeën delen[^22].

### Overtuigingen

Overtuigingen kunnen gedefinieerd worden als diepgewortelde overtuigingen of vastgeroeste meningen over een onderwerp. Hoewel overtuigingen nuttig kunnen zijn, zijn ze over het algemeen problematischer dan wat dan ook, vooral op het gebied van politiek.

Een van de grootste problemen met overtuigingen is dat ze kunnen leiden tot polarisatie en verdeeldheid. Als mensen sterke overtuigingen hebben over een onderwerp, zijn ze misschien minder geneigd om andere standpunten in overweging te nemen of om samen te werken met mensen met andere overtuigingen. Dit kan leiden tot polarisatie en verdeeldheid in de samenleving[^23].

Ze kunnen leiden tot geslotenheid. Als mensen te zeer gehecht zijn aan hun overtuigingen, kunnen ze minder geneigd zijn om te luisteren naar meningen of feiten die hun overtuigingen tegenspreken. Dit kan leiden tot besluitvorming op basis van vooroordelen in plaats van feiten,

Overtuigingen kunnen leiden tot weerstand tegen verandering. Als mensen sterk gehecht zijn aan hun overtuigingen, kunnen ze minder geneigd zijn om nieuwe ideeën of veranderingen in de samenleving te accepteren.

## Oplossingen voor de problemen van onze democratieën

### De politieke ondernemer

De oplossing voor de politieke ondernemer is om deze functie af te schaffen of op zijn minst te beperken door de burgerparticipatie te vergroten.

Niet langer zou de politieke ondernemer iemand moeten zijn aan wie macht wordt gedelegeerd en die, vanaf het moment dat hij deze verwerft, ermee kan doen wat hij wil. Maar dat hij een vertegenwoordigende functie heeft, en dat zodra de mensen die macht aan hem delegeren vinden dat hij hen niet meer vertegenwoordigt, ze hem moeten kunnen ontslaan.

Er moet volledige transparantie komen, zodat mensen precies weten aan wie ze hun macht delegeren en wat ze ermee doen.

En voor een goede diversiteit moeten we ervoor zorgen dat kandidaten uit verschillende achtergronden komen en dezelfde kansen hebben als anderen om toegang te krijgen tot deze posities.

### Politieke programma's

Een oplossing voor het probleem van politieke programma's zou kunnen zijn om ze meer modulair en specifieker voor elk onderwerp te maken, in plaats van ze als een totaalpakket te presenteren. Dit zou kiezers in staat stellen om voor elk onderwerp afzonderlijk de voorstellen te kiezen waaraan ze de voorkeur geven, in plaats van het hele programma te moeten accepteren of verwerpen.

### Eerlijkheid en transparantie

In een democratie is het belangrijk dat de besluitvorming transparant en controleerbaar is en dat de machthebbers rechtstreeks verantwoordelijk worden gehouden voor hun daden. Het delegeren van macht moet zorgvuldig gebeuren en er moet rekening worden gehouden met de bijbehorende risico's om ervoor te zorgen dat de belangen van de burgers worden beschermd.

### De keuze van alternatieven

Het is belangrijk om de kiezer een volledige en ruime keuze aan alternatieven te geven, en evenveel zichtbaarheid en informatie over die alternatieven. Uitgebreide programma's beperken deze mogelijkheid weer, en onderwerpen zouden kunnen worden gescheiden om meer specifieke beslissingen mogelijk te maken.

### Delegatie van macht

Kiezers moeten kunnen kiezen of ze hun macht willen delegeren of niet. Ze moeten hun bevoegdheidsdelegatie ook op elk moment kunnen intrekken.

### Centralisatie van macht

In een democratie is machtsverdeling essentieel om ervoor te zorgen dat de belangen en meningen van burgers worden vertegenwoordigd en in de besluitvorming worden meegenomen. Dit impliceert de actieve participatie van burgers, de decentralisatie van macht en het creëren van verantwoordings- en transparantiemechanismen om de macht van gekozen leiders te beperken.

### Stemsystemen

We moeten de voorkeur geven aan stemsystemen die rekening houden met de intensiteit van de voorkeuren van burgers en stemsystemen vermijden die nuttig stemmen aanmoedigen.

### Institutionele traagheid

Er moeten snelle referenda worden ingevoerd voor noodsituaties. Meer in het algemeen zouden burgers regelmatiger hun mening moeten kunnen geven.

### Korte termijn mandaten

Er zijn een aantal mogelijkheden, waaronder de invoering van langere ambtstermijnen met de invoering van een procedure die kiezers het permanente recht geeft om hun gekozen vertegenwoordigers terug te roepen. We kunnen burgers ook meer bewust maken van de lange termijn en politici motiveren om te plannen voor de lange termijn. We kunnen ook de ambtstermijnen beperken of zelfs de rol van de politieke ondernemer afschaffen.

### Bluffs naar expertise

Het is belangrijk om te erkennen dat vaardigheden en expertise niet exclusief zijn voor academische kwalificaties en dat de inclusie van mensen met verschillende achtergronden een rijkdom aan perspectieven met zich mee kan brengen die gunstig kunnen zijn voor bestuur en politiek.

### Wetenschap geminacht

Betrek wetenschappers bij besluitvormingsprocessen: Wetenschappers kunnen helpen bij het nemen van beleidsbeslissingen door onafhankelijke, op bewijs gebaseerde gegevens, analyses en beoordelingen te leveren.

Vergroot de transparantie en verantwoordingsplicht van de wetenschappelijke gegevens en bewijzen die ze gebruiken om beleidsbeslissingen te nemen, en leg verantwoording af voor hun beslissingen en de gevolgen ervan. Effectieve communicatie tussen wetenschappers en beleidsmakers aanmoedigen, door hun bevindingen duidelijk en beknopt te communiceren en technisch jargon te vermijden. Een cultuur van respect voor wetenschap bevorderen. Beleidsmakers opleiden over wetenschap: Politici moeten worden opgeleid in de wetenschappelijke methode en de belangrijkste wetenschappelijke concepten om wetenschappelijke gegevens en bewijzen beter te begrijpen.

Vastleggen in een grondwet van principes waarmee rekening moet worden gehouden, zodat beslissingen onvervreemdbare principes respecteren waarover consensus bestaat, bijvoorbeeld om menselijk geluk en een evenwicht binnen onze ecosystemen op aarde te garanderen.

### Propaganda

Om de invloed van propaganda in de politiek te verminderen, is het belangrijk om vrije en transparante informatie aan te moedigen, gebaseerd op feiten en verifieerbare gegevens. De media moeten een sleutelrol spelen in dit proces door evenwichtig en onpartijdig verslag te doen van politieke gebeurtenissen, politieke beweringen op feiten te controleren en onafhankelijke feitencontrole te bevorderen. Het is ook belangrijk om het publiek bewust te maken van propaganda- en opiniemanipulatietechnieken en hen te leren hoe ze deze tactieken kunnen herkennen en weerstaan.

### Burger onwetendheid

Kwaliteitsonderwijs kan burgers helpen om beter geïnformeerde kiezers te worden. Het is belangrijk om studenten de vaardigheden van kritisch denken, logisch redeneren, het vinden van betrouwbare informatie en het begrijpen van politieke kwesties bij te brengen.

Transparantie en toegankelijkheid: overheidsbeleid en -beslissingen moeten transparant en gemakkelijk toegankelijk zijn, zodat burgers beter kunnen begrijpen hoe hun overheid werkt en welke beslissingen worden genomen.

Communicatie: het is belangrijk dat politieke leiders duidelijk en regelmatig met burgers communiceren, in eenvoudige taal en zonder vakjargon.

Participatie: het aanmoedigen van burgerparticipatie in politieke processen, zoals verkiezingen, petities, publieke debatten, raadplegingen, opiniepeilingen, enz. kan helpen om de betrokkenheid van burgers bij het politieke leven te vergroten en hun begrip van politieke kwesties te versterken.

### Tijdgebrek

Het tijdsprobleem kan worden opgelost door burgers meer tijd te geven door hun werktijden te verkorten. Een andere mogelijkheid is een algoritme dat elke keer nauwkeurig een representatieve steekproef uit de samenleving selecteert, zodat niet iedereen hoeft te stemmen.

### De politieke hooligan

Lees de programma's zonder politiek vandalisme en vergelijk ze met economisch en sociologisch onderzoek. Vraag jezelf af of we onze intuïtie volgen, die onze rede dan met argumenten zou hebben gestaafd.

Je bewust worden van dit hooliganisme zal leiden tot betere debatten en een manier van denken bieden die niet vastzit aan intuïtie en die ons in staat stelt om uit onze politieke hooligan manier van denken te stappen. We moeten alles waarderen wat tegenintuïtief is, het feit waarderen dat we onze intuïtie kunnen overtuigen dat onze intuïtie fout is.

We moeten het feit waarderen dat we het mis hebben, dat we onszelf in twijfel kunnen trekken en begrijpen dat we de grootste vooruitgang kunnen boeken door onze intuïtie te deblokkeren.

### Communitarisme

We moeten vechten tegen het verlangen van iedereen om deel uit te maken van de meerderheid.
We moeten dan vechten om ervoor te zorgen dat deel uitmaken van de minderheid geen pijnlijke ervaring is.

We moeten oppassen dat we de meerderheid niet de overhand geven, gewoon omdat het de meerderheid is.

Je moet de ervaring om tot de minderheid te behoren aangenaam maken.

Zorg ervoor dat mensen die in de meerderheid zijn, in de minderheid willen zijn.

Laat iedereen proberen elkaar te begrijpen en zich te mengen.

### Overtuigingen

In een democratie is het belangrijk dat mensen ruimdenkend zijn en bereid om naar de meningen en standpunten van anderen te luisteren. Burgers moeten zich hiervan bewust zijn en hierover worden voorgelicht.

### Conclusie van oplossingen

Er zijn veel problemen die inherent zijn aan democratie.

Om goed te kunnen werken, zijn ideale omstandigheden nodig.

Goede media om goed geïnformeerd te zijn en voor de politici die aan de macht zijn om echt in het belang van het volk te handelen.

Maar het probleem is dat onze democratie een context creëert waarin precies het tegenovergestelde gebeurt.

Mensen zijn ofwel hobbits, niet geïnteresseerd in politiek, zeer onwetend op dit gebied, ze hebben niet al te veel vooroordelen, of ze zijn hooligans, hebben een vooroordeel, maar zeer genomen, en vaak slachtoffer van confirmation bias, ze gaan zo ver dat ze waarden irrationeel verdedigen.

Wat we nodig hebben zijn vulcans, mensen die rationeel kunnen denken.

En op dit moment kun je geen rationele discussies voeren tussen politieke mensen in een democratie met gekozen vertegenwoordigers die gekozen willen worden.

Als we een radicale verandering in de democratie willen, moeten we absoluut rekening houden met al deze rationele elementen, zoals het principe van politieke hooligans, gemeenschapsscheiding, enz.

Maar democratie is een gok die de moeite waard is, omdat we ons er allemaal gelijk door voelen, het moreel kapitaal biedt en ons aanzet tot samenwerking.

## Experimenteel idee voor een nieuwe diepe democratie (V1)

Naam: Gedeelde participatieve E-Democratie door spontaan referendum ondersteund door expertise en wijsheid.

Om een bredere en directere burgerparticipatie mogelijk te maken, gebruiken we informatie- en communicatietechnologieën om een digitaal hulpmiddel te creëren dat democratische processen zal vergemakkelijken en verbeteren.

Deze tool is een webplatform dat ook beschikbaar is als mobiele applicatie. Je kunt het zien als een wiki, waarop je toegang hebt tot informatie over de verschillende lagen van de samenleving waarin je betrokken bent, zoals wetten, organisatiewijzen, actuele beslissingen, enz. Deze lagen zijn zo ingedeeld en gestructureerd dat ze gemakkelijk toegankelijk zijn. Deze lagen zijn zo nodig onderverdeeld en gestructureerd in categorieën en subcategorieën, afhankelijk van de aard van de laag.

Iedereen kan wijzigingen in de ingevoerde informatie voorstellen. Het is onmogelijk voor elk mens om alle voorstellen te lezen die door de 7 miljard mensen kunnen worden gedaan. We gebruiken een algoritme dat een steekproef neemt die alle mensen vertegenwoordigt die gekoppeld zijn aan het voorstel. Het voorstel wordt dan naar deze representatieve steekproef gestuurd. Mensen kunnen voor, tegen of onthouding stemmen en als er meer dan 50% voor is, gaat het voorstel door naar het volgende niveau.

Het voorstel wordt dan naar een consortium van experts en wijzen gestuurd die het voorstel zullen bestuderen om hun diepgaande mening te geven, het technische en wetenschappelijke aspect te controleren, een schatting te maken van de toe te wijzen middelen en tijd, maar ook en vooral om te controleren of het in overeenstemming is met de vastgestelde grondwetten.

De grondwet van Eutopia zal de fundamenten leggen van de samenleving, die bestudeerd en gerechtvaardigd zal moeten worden als zijnde in staat om ons gemeenschappelijke doel te bereiken, namelijk een bevredigend en duurzaam leven leiden in harmonie met de Aarde. De grondwet zal dan onderwezen moeten worden om de mensen er bewust van te maken, om er zeker van te zijn dat het hun voorstellen en beslissingen leidt.

Zodra het werk van de experts en wijzen is afgerond, zullen hun meningen worden toegevoegd aan het uiteindelijke voorstel, zodat de burgers alle sleutels hebben die ze nodig hebben om hun keuze te maken. De mensen die worden aangewezen als deskundigen of wijzen worden gekozen door de burgers in hun respectieve gebieden. De transparantie en waarheidsgetrouwheid van de informatie die in deze adviezen wordt verstrekt, moet worden gewaarborgd, evenals de eenvoud van de informatie, zodat de burgers alle kaarten in handen hebben om hun keuze te maken en hun macht met een zuiver geweten uit te oefenen.

Het voorstel zal dan opnieuw naar een steekproefgroep worden gestuurd met de informatie en meningen van de experts en wijzen. Als het voorstel opnieuw wordt goedgekeurd, wordt het naar de hele bevolking gestuurd voor definitieve goedkeuring. Het resultaat zou dan hetzelfde moeten zijn als dat van de steekproef. Wanneer burgers dit voorstel voor de eindstemming ontvangen, zullen ze echter van tevoren weten dat meer dan een meerderheid van plan is om het te accepteren, wat het voordeel heeft dat ze een laatste kans hebben om zich terug te trekken.

De burger ontvangt dus niet alle voorstellen, maar alleen het voorstel dat hem of haar aanbelangt, afhankelijk van of de beslissing meer lokaal of globaal wordt toegepast.

Voordat je je voorstel instuurt, hebben we ook een samenwerkingssysteem opgezet dat we je sterk aanraden te gebruiken. Je voorstel is dan in de eerste plaats een suggestie, en deze suggesties zijn toegankelijk voor iedereen die ze wil doen, en iedereen kan eraan deelnemen.

Je kunt je macht ook delegeren aan iemand anders, die dan in jouw plaats over het voorstel stemt. Je hebt dan wel toegang tot de geschiedenis van de standpunten waarover gestemd is om te controleren of je het ermee eens bent, en je kunt je machtsdelegatie op elk moment weer intrekken. Je kunt ook kiezen hoeveel macht je wilt delegeren aan de verschillende lagen van de samenleving.

Tot slot zullen we dit democratische systeem ook gebruiken voor het gezamenlijk bewerken van verhalen. Of eerder, terwijl we ons systeem testen kunnen we het itereren en aanpassen tot we er volledig tevreden over zijn, en het zal uiteindelijk deze definitieve versie zijn die de democratie van Eutopia zal vormen.

> "Democratie aan deze horizon lijkt tijd, moed, doorzettingsvermogen... en misschien een vleugje waanzin te vereisen..." Datageueule[^4]

## Bronnen (Niet-uitputtende lijst)

[^1]: [Wikipedia : Democratie ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. Zijn verkiezingen democratie? 1977
[^3]: David Shearman. De uitdaging van de klimaatverandering en het falen van de democratie (Politics and the Environment). 2007.
[^4]: [Datageule. Democratie(s) ?](https://www.youtube.com/watch?v=RAvW7LIML60).
[^5]: [Science4All : Het grondbeginsel van de politiek](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. De logica van politiek overleven (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Voorspelling: Hoe de toekomst te zien en te vormen met speltheorie. 1656
[^8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Foster eerlijkheid | Democratie 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja en Brian F. Schaffner. Campagnefinanciering en politieke polarisatie: wanneer puristen de overhand krijgen. 2015
[^11]: [Science4All. Our Democracies Divide | Democracy 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. The Randomized Condorcet Ballot | Democracy 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. Ben jij een politieke hooligan? Democratie 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[^15]: [Science4All. Rationally Irrational | Democracy 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies. 2008
[^17]: [The Bias Work #5: Confirmation Bias](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [AmazingScience - The Halo Effect - Stupid Brain #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [The Bias Bracket #3- Cognitive Dissonance](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Franklin Templeton Academy - Availability bias](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. Klein communitarisme wordt groot | Democracy 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [TED Conference. Eli Pariser waarschuwt voor online "filter bubbles"](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Beste overtuiging, muteer in een VIRALE infectie !!! Democratie 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Mediaconcentraties: de miljardairs informeren u. 2016
