---
title: Werktijd
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Inhoud in het experimentele stadium en in het proces van schrijven.
> {.is-gevaar}

Werk, van het woord travalhum, komt van tripalium, een drievoet gebruikt als martelwerktuig. Maar werk is niet veroordeeld tot de marteling van zijn etymologische oorsprong.

Voor velen van ons zijn de aard en de duur van het werk onvermijdelijk repetitief, saai en levensvernietigend.

Het leven lijkt leeg en we zijn teleurgesteld als we worden opgeslokt door werk dat geen betekenis geeft.

Maar de maatschappij staat ons niet toe om onze ware verlangens te vervullen. Want de ontevredenheid die ons ertoe zou kunnen brengen om dit leven in twijfel te trekken en een ander leven te eisen, wordt gekanaliseerd door het systeem dat onze intrinsieke behoeften vervult en de extrinsieke behoeften die ze opwekken, en dat ons tegelijkertijd verhindert om te bereiken wat we missen.

Waar werk ons in staat zou moeten stellen om een creatie op de wereld te zetten die onze stempel draagt, om onszelf uit te drukken en collectief betekenis te geven aan ons leven.

Alle dimensies van de sleur van de taak kunnen en moeten worden uitgewist of verminderd. Werk dat lichamelijk of psychisch uitputtend, saai of geestdodend is, te zwaar, mechanisch, monotoon, te vuil of onaangenaam, te betekenisloos, verachtelijk, enzovoort. Moet worden verwijderd, vervangen, beperkt, geautomatiseerd, verbeterd, verrijkt, gecompenseerd, beloond, gewaardeerd of beter gedeeld.

Het idee zelf is om de aard van werk te transformeren zodat het een activiteit wordt die zoiets is als zelfrealisatie, de manifestatie van iemands diepste menselijkheid in harmonie met de natuur. Op deze manier zou werk de "eerste levensbehoefte" worden. Op deze manier zou de uitdrukking "werken om in je levensonderhoud te voorzien" niet langer betekenen dat je moeite moet doen om te overleven en jezelf te onderhouden, maar dat je daadwerkelijk in je levensonderhoud moet voorzien. ^1]

## Vermindering van de werktijd

We gaan je laten zien dat het economische model van wederkerigheid het mogelijk maakt om de werktijd die nodig is voor het goed functioneren van de maatschappij aanzienlijk te verminderen, tot een quotum van 2 tot 3 uur per persoon[^2][^8].

Productiviteit is de verhouding tussen productie en het aantal werkuren dat nodig is om dit te bereiken.

De productiviteit is de afgelopen eeuwen aanzienlijk toegenomen.

Ten eerste met de komst van de industriële revolutie in de 18e en 19e eeuw, waarbij productieprocessen gemechaniseerd werden en nieuwe technologieën zoals stoom en elektriciteit werden geïntroduceerd.

Sindsdien is de productiviteit in een wisselend tempo blijven stijgen, maar wel gestaag naarmate er nieuwe technologieën en productiesystemen werden ontwikkeld.

Het is soms moeilijk om betrouwbare gegevens te vinden over de productiviteitsgroei in Frankrijk over een periode van meer dan 100 jaar. De methoden om productiviteit te meten zijn in de loop der tijd geëvolueerd, waardoor het moeilijk kan zijn om gegevens over lange perioden te vergelijken.

Maar tussen 1950 en 2020 steeg de arbeidsproductiviteit in Frankrijk bijvoorbeeld gemiddeld met ongeveer 2% per jaar. Dit betekent dat een gemiddelde werknemer in Frankrijk ongeveer twee keer zoveel goederen en diensten kan produceren in één werkdag als in 1950[^3].

Toch is de gemiddelde werkweek in dezelfde periode niet met 50% gedaald, maar met 12,5%, van 40 uur in 1950 tot ongeveer 35 uur in 2020.

Er is hier een beschavingskeuze gemaakt om de mogelijkheden die de wonderbaarlijke ontwikkeling van de technologie in de laatste paar eeuwen biedt, niet te gebruiken om de arbeidstijd te verkorten, maar om de productie en de bijbehorende winsten te verhogen.

Dit komt voort uit de basis van het kapitalisme, de groei waarvan het voortbestaan afhangt, het winstbejag van de ondernemer en het accumulatiebejag van de consument, dat wordt gecombineerd met steeds meer.

Het productieapparaat moet daarom op volle toeren blijven draaien. Het doel is om steeds meer te verkopen en de mogelijkheden voor winst te vermenigvuldigen.

Dit leidt tot onzin in de manier waarop goederen en diensten worden geproduceerd en geconsumeerd.

Maar arbeidstijdverkorting is de vijand van onze samenleving, omdat vrije tijd gebruikt kan worden voor discussie, voor organisatie aan de basis en voor het uitvinden van vormen van collectief leven. De macht zou dan niet langer geconfronteerd worden met anonieme, geïsoleerde individuen die bereid zijn te accepteren, maar met bewuste, sterke groepen die moeilijker te manipuleren zouden zijn.

Betekent een vermindering van de arbeidstijd dan een vermindering van de productie? Met catastrofale gevolgen voor onze levenskwaliteit?

Nee, het idee is geenszins om onze levenskwaliteit te veranderen; integendeel, het is om deze voortdurend te verbeteren voor de bevolking als geheel. Hoewel levenskwaliteit een concept blijft dat volledig afhankelijk is van de individuele en maatschappelijke mores van een tijdperk. We gaan aantonen dat het mogelijk is om aanzienlijke besparingen te realiseren die, zonder onze levensstandaard aan te tasten, ons in staat stellen om de productie en dus de arbeidstijd aanzienlijk te verminderen.

Hier ontdekken we de kracht van wederkerigheid. Als we eenmaal uit de markt zijn, kunnen we eindelijk effectief oplossingen bedenken om de arbeidstijd te verkorten zonder dat dit ten koste gaat van onze economie.

Want de absurditeiten en onrechtvaardigheden die we aan het licht zullen brengen zijn niet het resultaat van fouten of onhandigheid, integendeel, ze zijn noodzakelijk voor het overleven van het kapitalisme.

Hier, in de volgende punten, zijn de verschillende oplossingen om de totale arbeidstijd te verminderen:

## Productie/consumptie verminderen

### Onze extrinsieke behoeften veranderen

Het is moeilijk voor te stellen hoe we, buiten dit consumptiesysteem dat ons altijd meer belooft, dat beetje extra zouden kunnen krijgen dat het leven kleur en smaak geeft, net als de voorwerpen die onze identiteit verzekeren. Want het is door de dingen die we bezitten, de kleren die we dragen, de plaatsen die ons vertrouwd zijn, dat we ons zowel gelijkaardig genoeg voelen aan anderen zodat ze ons niet afwijzen, als verschillend genoeg zodat we er zeker van kunnen zijn onszelf te zijn.

Maar door onze consumptie te beperken, kunnen we ons verlangen om lief te hebben en te creëren onthullen, om de tijd vrij te maken om te leven die vandaag de dag zo ontbreekt. Dan zullen we de vreugde kennen van het laten stromen van het leven, waarvan het ruisen ons vertelt wat we zijn, het plezier van liefhebben als er geen haast bij is, het plezier van het beheersen van een techniek die helemaal van onszelf is, van het zelf creëren van objecten, of we ze nu willen gebruiken om onze ruimte te tekenen of om het geschenk te zijn dat we reserveren voor degenen van wie we houden.

Deze visie helpt ons te begrijpen waarom het idee van arbeidstijdverkorting zoveel weerstand oproept in onze maatschappij, omdat het vrijmaken van de tijd die werk in beslag neemt, een breuk betekent in het keurslijf waarin de kapitalistische maatschappij ons leven heeft geperst.

Groei wordt in stand gehouden omdat het nooit kan voldoen aan "behoeften" die voortdurend opnieuw worden gecreëerd op basis van ongelijkheid die niet wordt gecorrigeerd.

Deze behoeften zijn verlangens naar status, erbij horen, eigenwaarde, macht of sociale identificatie. Ze worden gecreëerd door reclame, marketing en sociale druk.

Reclame wil consumenten ervan overtuigen dat ze bepaalde producten of diensten nodig hebben om gelukkig, tevreden of gerespecteerd te zijn in de maatschappij. Ze gebruiken beelden of boodschappen die onze emoties, verlangens en aspiraties aanspreken.

### De levensduur van producten verlengen

Een auto, bijvoorbeeld, vervult een behoefte aan persoonlijke autonomie, maar ook een extrinsieke behoefte om het prestige te verwerven dat verbonden is aan het bezitten van het nieuwste model of het topmodel.

Het autobedrijf verzekert zijn winstgevendheid en dus zijn voortbestaan door zijn product voortdurend te vernieuwen, door het verlangen van de consument naar steeds geavanceerdere en duurdere machines te ontwikkelen, maar ook door zijn kwetsbaarheid en dus zijn behoefte aan vernieuwing.

Als we de uren die een auto ons bespaart vergelijken met de uren die we werken om hem te betalen, dan besteedt de gemiddelde werknemer het equivalent van 375 uur per jaar, of 2 maanden werk, aan het kopen, onderhouden, laten rijden en verzekeren van een auto.

Met de eerste auto's kon 200.000 tot 300.000 km worden gereden, maar de 'vooruitgang' heeft zich gericht op het maken van kwetsbaardere auto's. Dit is de prijs van groei, maar het is een kwestie van tijd en geld. Dit is de prijs van groei, maar het gaat ten koste van de consument, terwijl de fabrikant bespaart door te bezuinigen op classificaties en de kwaliteit van materialen, grote winsten maakt op reserveonderdelen en apparatuur steeds sneller vernieuwt.

De auto moet ontworpen zijn om 20 jaar of langer mee te gaan, misschien zelfs tot hij afgedankt wordt, maar dit punt wordt verder behandeld in het hoofdstuk over transport.

De monumenten uit het verleden, kathedralen en kastelen, tonen ons de sociale verhoudingen en ideologie van hun tijd. Onze eigen monumenten zijn deze machines, waarvan de fantastische complexiteit het hoogtepunt van onze wetenschappelijke kennis en technologische knowhow weerspiegelt, maar die verouderd zijn zodra ze geboren zijn en na een paar decennia op de schroothoop belanden. Goed geconstrueerde gebouwen en huizen kunnen gemakkelijk meer dan een eeuw meegaan.

Hoeveel apparaten zijn er niet gerepareerd, vaak binnen een korte tijd na aankoop, waarbij de reparatie maar al te vaak te moeilijk of te duur was, wat ertoe leidde dat het apparaat werd weggegooid en vervangen door een ander.

De eerste kousen waren onverwoestbaar, maar nu worden ze na de productie in een zuurbad gedompeld om hun sterkte te verminderen. Het bedrijf dat de tl-buizen uitvond, bracht ze pas op de markt nadat ze ontdekt hadden hoe ze hun werkingsduur konden terugbrengen tot 1000 uur. Scheermesjes zijn ontworpen om bot te worden. Gloeilampen zijn momenteel ontworpen om slechts 1000 uur mee te gaan. Dezelfde lampen kunnen 3000 uur meegaan.

De meeste huishoudelijke apparaten zouden 30 jaar mee kunnen gaan.

De verslechtering van de kwaliteit van objecten is duidelijk, maar het is een geheim dat door de industrie nauwlettend wordt bewaakt en moeilijk te bewijzen is. Alleen de fabrikanten weten hoeveel meer het hen zou kosten om voorwerpen te maken die 2 of 3 keer langer meegaan.

Dit is de paradox: een product dat te lang meegaat kan op lange termijn niet winstgevend zijn in een marktlogica die gericht is op monetaire winst, maar is wel winstgevend in een logica van wederkerigheid die gericht is op minder werk.

### Afval verminderen

In onze maatschappij produceren we veel meer dan we echt nodig hebben, wat leidt tot inefficiënt gebruik van natuurlijke hulpbronnen en een opeenhoping van afval.

Voedselverspilling: Volgens de FAO gaat elk jaar ongeveer een derde van het voedsel dat wereldwijd wordt geproduceerd verloren of wordt verspild. Dit kan te wijten zijn aan overproductie, slecht beheer van de toeleveringsketen, strenge esthetische normen of consumentengedrag zoals impulsaankopen of overconsumptie.

Energieverspilling: We hebben de neiging om meer energie te verbruiken dan we nodig hebben voor onze dagelijkse behoeften. Bijvoorbeeld door elektronische apparaten op stand-by te laten staan, inefficiënte vervoersmiddelen te gebruiken of lichten onnodig aan te laten.

Verspilling van materialen: Onze samenleving produceert vaak goederen voor eenmalig gebruik of van lage kwaliteit die snel worden weggegooid of vervangen, in plaats van de voorkeur te geven aan duurzame en gemakkelijk te repareren goederen. Dit leidt tot inefficiënt gebruik van natuurlijke hulpbronnen en een opeenhoping van afval.

### Goederen delen

Door goederen te delen, kunnen mensen hun consumptie en dus hun productie verminderen, omdat ze niet zoveel spullen hoeven te kopen om aan hun behoeften te voldoen. Bijvoorbeeld, verschillende mensen kunnen een boor delen in plaats van er elk een te kopen voor occasioneel gebruik. Dit vermindert de productie van boren.

90% van de auto's staat stil, dus we moeten onze inspanningen bundelen om minder te produceren.

Bovendien, wanneer goederen gedeeld worden, worden ze meestal beter gebruikt en beter onderhouden, wat de levensduur van objecten verlengt en dus ook van nieuwe productie. Wanneer mensen mede-eigenaar zijn, is er 30% minder schade.

## Productiviteit verhogen

### Gebruik technische vooruitgang

Technische vooruitgang wordt vaak geassocieerd met een hogere productiviteit, omdat het ons in staat stelt meer goederen of diensten te produceren met minder middelen, minder tijd en minder inspanning. Technologische vooruitgang heeft de efficiëntie en kwaliteit van onze productieprocessen al verbeterd en de productiekosten helpen verlagen.

Er is een 4e industriële revolutie aan de gang die kunstmatige intelligentie, het internet van dingen, geavanceerde robotica, augmented reality, blockchain, nanotechnologie en biotechnologie omvat. En deze technologieën zullen de manier waarop we produceren echt veranderen.

We moeten echter oppassen dat het gebruik van technologische vooruitgang geen gevolgen heeft voor de Human Environmental Impact en het Bruto Binnenlands Geluk, die altijd moeten bijdragen aan duurzame en rechtvaardige ontwikkeling.

### Optimaal profiteren van kortere werktijden

Elke baan, zelfs de eenvoudigste, vereist een minimum aan opleiding, oefening en vaardigheid. Als de arbeidstijd afneemt, kan de productiviteit slechts tot een bepaald punt stijgen, waarna deze weer daalt.

Hoe meer je beheerst, hoe productiever je bent, maar hoe langer je werkt, hoe vermoeider je wordt en hoe minder productief je bent.

En het is mogelijk dat voor veel beroepen die kennis vereisen dit punt wordt overschreden met de "2 tot 3 uur per dag", daarom moeten deze 2 tot 3 uur per dag niet letterlijk worden genomen. Iedereen kan zijn werk indelen in periodes die drukker zijn en andere die minder druk zijn of die helemaal vrij zijn.

Bijvoorbeeld, 3 dagen van 8 uur per week gedurende 2 jaar kan dan plaats maken voor 3 jaar vrije tijd.

## Werk verwijderen

### Verander gebonden werk in vrij werk

In de huidige maatschappij, zoals in elke maatschappij, zijn er onaangename jobs en andere die aangenaam zijn. Over het algemeen worden de vervelende en saaie taken slecht betaald en zijn ze niet erg prestigieus, en de meest lonende activiteiten zijn over het algemeen de meest sociaal gewaardeerde en interessante voor degenen die ze doen.

Er zijn 2 soorten werk.

Hard, saai werk dat nog steeds nodig is om de maatschappij als geheel draaiende te houden, wat we "gebonden werk" zullen noemen.

En vrij werk, dat betekenis heeft voor de persoon die het doet, voortkomend uit een creatieve activiteit die zijn rechtvaardiging in zichzelf vindt. Natuurlijk kan dit subjectief zijn en specifiek voor elk individu.

In werkelijkheid ontsnapt 3/4 van het werk dat in onze kapitalistische maatschappij wordt gedaan aan de markteconomie.

Met meer tijd en een betere samenleving kunnen we een beroep doen op een zelfhulpnetwerk om onze alledaagse problemen te bespreken en op te lossen, onze diëten, ons seksleven, onze zorgen over het leven te bespreken, net zoals nu al op grote schaal begint te gebeuren in grote Facebookgroepen.

Apparaten zouden zo ontworpen kunnen worden dat ze hanteerbaar zijn voor gebruikers, gemakkelijk te monteren en demonteren zijn met gestandaardiseerd gereedschap, van model veranderen wanneer echte technische vooruitgang dat nodig maakt, en tegelijkertijd garanderen dat reserveonderdelen tegen redelijke prijzen gedurende zeer lange perioden geleverd kunnen worden, klanten duidelijke, gedetailleerde instructies geven, lokale montage- en reparatiewerkplaatsen opzetten waar het nodige gereedschap te vinden is en hulp en ervaring uitgewisseld kunnen worden.

Verbeteringen aan het huis kunnen door de bewoners worden uitgevoerd. Wat vandaag de dag een ondraaglijke last is, je eigen huis bouwen met zo'n zwaar werkschema. Met de hulp van de gemeenschap kan deze activiteit echter een creatieve en plezierige activiteit worden.

Lesgeven zou niet het monopolie van een professionele instantie kunnen blijven, maar integendeel flexibeler worden en de functie van leraar zou breder gedeeld kunnen worden, en iedereen die kennis te bieden heeft zou aangemoedigd kunnen worden om te helpen bij het lesgeven.

In feite zou een groot deel van het werk dat gedaan wordt door het personeel van bepaalde beroepen door anderen gedaan kunnen worden en vrij werk worden in ons Eutopia, waardoor deze beroepen ontlast worden.

### Profiteren van de reorganisatie van het werk

Het volgende hoofdstuk gaat over de reorganisatie van het werk, d.w.z. de manieren waarop bedrijven in ons Eutopia georganiseerd zijn.

De horizontale aard van deze organisatie zal een bepaalde hiërarchie elimineren die management- en besluitvormingstaken uitvoert. Dit werk zal niet langer nodig zijn, omdat het nu door alle medewerkers van een bedrijf gezamenlijk wordt uitgevoerd.

### Bullshit job en meer noodzakelijke job

In ons Eutopia zouden veel banen worden afgeschaft omdat ze in werkelijkheid het product zijn van disfuncties in de huidige maatschappij en daarom niet langer nodig zouden zijn.

- Reclame is goed voor 1% van het BBP[^4].
- Het leger 3% van het BBP.
- Verloren werk in verzekeringsmaatschappijen.

Een volledige lijst van banen die verloren zijn gegaan in Eutopia is beschikbaar aan het einde van dit verhaal.

## Het aantal werknemers verhogen

### Werkloosheid verminderen

In een land als België bedraagt het werkloosheidspercentage momenteel 5,5%. Rekening houdend met de totale actieve bevolking in België van 5,8 miljoen mensen volgens de gegevens van Eurostat voor 2021, vertegenwoordigt dit ongeveer 319.000 werklozen[^7].

Aangezien er geen banen als zodanig zijn, gaat het effect van het verdwijnen van de werkgelegenheid in loondienst gepaard met het verdwijnen van de werkloosheid.

Het zorgt voor sociale inclusie en een open samenleving, waar elk nieuw lid welkom is om zijn of haar vaardigheden en kennis, wat die ook mogen zijn, bij te dragen aan de samenleving en de totale werktijd te verminderen.

### Het aantal mensen dat deelneemt aan gekoppeld werk verhogen

De maatschappij sluit een hele reeks mensen uit van het werkende leven, vooral mensen met een handicap.

Dit kan het gevolg zijn van discriminatie, stigmatisering, gebrek aan toegankelijkheid of gebrek aan ondersteuning.

Door het systeem voor wie het vaak alles of niets is, ook al is het beter voor hen om hun ondersteuning te blijven ontvangen dan te werken en deze ingetrokken te zien worden.

De algemene kwalificaties van de bevolking zouden aanzienlijk verhoogd kunnen worden. Op dit moment worden vrouwelijke experts meestal gerekruteerd uit de meest bevoorrechte sociale klassen. Maar als iedereen de kans zou krijgen om te worden wat ze willen worden en als ze de middelen zouden krijgen om dat te doen, zou het aantal experts veel hoger liggen.

Oudere mensen die na hun pensionering worden uitgesloten van de markt, die worden gediscrimineerd, enz.

### Gezondheidsproblemen verminderen

Volgens een onderzoek in 2020 onder Europese werknemers door de managementadviesgroep Mercer zei ongeveer 33% van de Europese werknemers dat ze ziekteverlof hadden genomen om redenen die verband houden met stress of geestelijke gezondheid.[^5] Daarnaast bleek uit een onderzoek van de Europese Commissie uit 2018 dat burn-out bij 28% van de EU-werknemers voorkwam.[^6]

## Bronnen (niet-limitatieve lijst)

[^1]: Serge-Christophe Kolms. La bonne économie. 1984
[^2]: Adret. Twee uur per dag werken. 1977
[^3]: [Insee. Croissance - Productivité](https://www.insee.fr/fr/statistiques/4277770?sommaire=4318291)
[^4]: Bullshit Job. David Graeber. 2018
[^5]: [Mercer Marsh Benefits Study - Absenteeism Barometer](https://www.mercer.com/fr-fr/insights/total-rewards/employee-benefits-strategy/barometre-de-labsenteisme-2023/)
[^6]: [MEDEDELING VAN DE COMMISSIE AAN HET EUROPEES PARLEMENT, DE RAAD, HET EUROPEES ECONOMISCH EN SOCIAAL COMITÉ EN HET COMITÉ VAN DE REGIO'S over een alomvattende aanpak van de geestelijke gezondheid](https://health.ec.europa.eu/system/files/2023-06/com_2023_298_1_act_fr.pdf)
[^7]: [STATBEL. Werkgelegenheid en werkloosheid ](https://statbel.fgov.be/fr/themes/emploi-formation/marche-du-travail/emploi-et-chomage)
[^8]: Keynes. J. Brief aan onze kleinkinderen. Essay over de toekomst van het kapitalisme. 1930.
