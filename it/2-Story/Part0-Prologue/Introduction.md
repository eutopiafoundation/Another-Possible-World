---
title: Introduzione
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Chi sei?

Forse non sei un grande sociologo, o un grande filosofo, o un grande economista, o un grande altro, anzi forse non hai frequentato l'università, o forse non sei andato bene a scuola. È così anche per molti di noi, ma nonostante i nostri percorsi formativi molto diversi, abbiamo la fiducia di proporvi qui un altro mondo possibile, un mondo che punta all'ideale, e vi invitiamo a unirvi a noi.

Forse vi state chiedendo perché siete nella posizione giusta per realizzarlo. E forse una parte di voi, leggendo questo paragrafo, sente già l'impulso di chiudere questo libro per non perdere altro tempo. Ma lasciateci ancora qualche paragrafo per cercare di convincervi.

## Immaginazione

Sebbene non siate grandi esperti in nessuna delle principali aree di questo mondo, c'è una cosa in cui siete particolarmente dotati: sognare. Questa capacità di immaginare è il più grande dono degli esseri umani. Vi invitiamo qui a sognare, ma soprattutto ad aiutare gli altri a sognare.

Quando abbiamo iniziato, eravamo una specie insignificante con un potere d'azione molto limitato. Ciò che ha fatto la differenza tra la nostra specie, l'Homo Sapiens, e le altre è stata la nostra capacità di cooperare in grandi numeri. [^1]

E questo lo dobbiamo alla nostra immaginazione, alla nostra capacità di inventare storie e di raccontarle a noi stessi.

Grazie ai miti collettivi, che possiamo anche chiamare "finzioni", siamo stati in grado di riunirci e cooperare.

Abbiamo la capacità di credere in cose che non esistono, abbiamo inventato molte leggende, miti e storie, abbiamo tracciato linee immaginarie sul mondo, creato nazioni e costumi, abbiamo inventato il denaro e gli abbiamo dato il massimo valore nella nostra immaginazione.

Questo dono dell'immaginazione deriva dal nostro bisogno di significato, dal nostro bisogno di prevedere e comprendere il nostro ambiente per sopravvivere meglio in esso[^2].

Lasciamo che questa immaginazione collettiva governi tutta la nostra vita, lasciamo che governi anche il destino dell'intera umanità. Il suo potere è tale che ci porta a distruggere il nostro ambiente, a provocare la nostra stessa distruzione.

Ma tutto questo è fondamentalmente solo un immaginario collettivo e cessa di esistere nel momento in cui tutti noi decidiamo di smettere di crederci.

In realtà, ciò che più comunemente chiamiamo ragione è più spesso il fatto di continuare a credere e a conformarsi alle regole del nostro immaginario collettivo.

## Ragione

Quando decidiamo di lasciarci alle spalle questo mondo immaginario, è allora che possiamo finalmente trovare la vera ragione.

Forse vi sembrerà di non saperne molto, ma scoprirete che non è necessario saperne molto per trovare le soluzioni e fare le scelte che ci condurranno tutti a una vita soddisfacente e significativa.

Perché le risposte alle nostre domande sono già dentro ognuno di noi. Per trovarle, non dobbiamo fare altro che abbandonare l'immaginazione collettiva, aprire il cuore e ascoltare. Può sembrare sciocco, ma siamo convinti che la vera ragione sia proprio questa.

E poiché abbiamo già iniziato questo processo di decostruzione, è questo che vi proponiamo di scoprire qui, un mondo veramente basato sulla ragione.

Un mondo la cui semplicità, significato e logica sembreranno evidenti anche a voi.

Scoprirete allora che anche voi, chiunque siate, qualunque sia la vostra conoscenza, non avete bisogno di sapere molto per giudicare ciò che è buono e ciò che non lo è, che non avete bisogno di molto per contribuire alla formazione di questo mondo.

> "Ma allora", dice Alice, "se il mondo non ha assolutamente senso, cosa ci impedisce di inventarne uno?". Ispirato a Lewis Carrol

<!-- > "Ogni innovazione attraversa tre fasi nell'opinione pubblica: 1) è ridicola 2) è pericolosa 3) è ovvia". Citazione apocrifa di Arthur Schopenhauer -->.

## Eutopia

Potreste pensare che quello che stiamo proponendo sia un'utopia.

Utopia è una parola costruita dal greco antico, derivata da "tópos" ("luogo"), con il prefisso "ou" ("non"), letteralmente "nessun luogo"[^3].

Utopia è una parola spesso usata in modo improprio e abusato. Viene brandita in ogni occasione per soffocare la speranza, denigrare l'immaginazione, reprimere il progresso, è un invito alla rassegnazione, una parola chiave nel sistema di sfruttamento, alienazione e dominio per accettare lo status quo[^4].

Ma un "non luogo" è una società che non esiste, non una società impossibile. E come possiamo vedere nel corso della storia, il futuro della società di domani è sempre diverso e nuovo. Alla fine, la società di domani è sempre l'utopia di oggi.

L'utopia viene talvolta contrapposta alla "scientificità". Ma questo dimostra un'ignoranza non solo del greco, ma soprattutto della natura stessa della scienza.

La ricerca dell'inesistente, la ricerca del realizzabile, l'analisi delle utopie realistiche è infatti un'attività scientifica. Per sapere che cosa è o non è possibile tra le cose non osservate, è necessario avere una profonda conoscenza e consapevolezza della natura e della struttura del mondo.

È un atteggiamento scientifico quello che adottiamo qui, che consiste nell'interrogare e mettere in discussione tutto, nel voler spiegare le cose in profondità, nel cercare le vere cause, nel praticare il dubbio e la critica sistematica delle giustificazioni ricevute, nell'accettare nulla come dato, evidente o immutabile. È in questo modo di scrivere che saremo portati a mettere radicalmente in discussione la nostra società. La scienza è aprire gli occhi.

Lo studio delle società possibili in relazione allo studio delle società attuali, presenti o passate, non è molto diffuso. Alcune discipline e alcuni autori non immaginano nemmeno che possa avere una dimensione professionale, o addirittura rifiutano con forza l'idea. Forse questi intellettuali cercano di giustificare la limitazione della loro immaginazione.

Il nostro sistema può essere terribilmente cattivo, ma sappiamo tutti che non è il peggiore possibile. Un cambiamento radicale potrebbe portare a una società ancora più sgradevole, e quindi, con questa paura giustificata, la massa della popolazione è conservatrice o cautamente riformista. Non può essere altrimenti solo se le persone possono farsi un'idea abbastanza precisa e affidabile di quello che potrebbe essere il nuovo e migliore sistema.

Per questo motivo l'analisi di un'altra società possibile deve essere sufficientemente seria, ponderata, precisa e completa. E lo stesso vale per le fasi della sua attuazione, le fasi di transizione dall'esistente al nuovo.

Poiché ciò che riteniamo possibile dipende dall'analisi, le persone devono essere convinte del progetto. Solo allora lo prenderanno come fine, facendo della marcia verso una società migliore una necessità per l'evoluzione della propria. [^4]

> "Certo, è analizzando le società che abbiamo costruito che troviamo le conoscenze necessarie per costruirne altre; ma poi dobbiamo smettere di accumulare ingredienti senza mai cucinare". Christophe Kolm

Nella sua definizione attuale, così come viene percepita dalla nostra società, l'utopia è un ideale, un concetto che non tiene conto della realtà ed è quindi irraggiungibile.

Quindi sì, in questo caso non creeremo un'utopia.

La realtà non è la nostra immaginazione collettiva. La realtà è costituita da soglie che non possiamo superare per non compromettere le condizioni favorevoli alla nostra sopravvivenza. È la concentrazione massima di CO2 nella nostra atmosfera, è il limite per l'interruzione dei cicli biochimici del fosforo e dell'azoto, essenziali per le buone condizioni dei suoli e delle acque, è l'erosione massima della biodiversità, cioè l'estinzione delle specie e del loro ruolo essenziale negli ecosistemi, è l'uso intensivo dei suoli, l'uso abusivo dell'acqua dolce, l'acidificazione degli oceani, e così via...[^5][^6][^7][^8][^9]

Questo fa parte di ciò che costituisce la realtà, che non è il caso, ad esempio, del valore annunciato dalle piccole cifre sulla pagina del nostro conto in banca.

In realtà, siamo nel bel mezzo dell'utopia, credendo che la nostra società possa continuare a vivere così a lungo termine[^11].

Per progettare il mondo ideale, dobbiamo lasciarci alle spalle il nostro mondo immaginario, partire dalle fondamenta, in modo da risolvere i problemi alla fonte. Perché è così che possiamo davvero costruire un altro mondo che abbia senso, un altro mondo che sia davvero possibile.

Quello che stiamo cercando di creare è un'Eutopia, un concetto simile all'utopia, ma che è considerato realizzabile nella realtà. Il prefisso "u" diventa "eu", che in greco significa "buono", e quindi letteralmente il "posto giusto"[^12].

Ciò che guida gli esseri umani liberi è la loro immaginazione del futuro, del possibile, del desiderabile, della loro utopia, Eutopia. E poiché gli esseri umani avanzano molto più spesso attratti dalla speranza che spinti dalla disperazione, i sogni finiscono per creare la realtà. La nostra speranza è che la realtà diventi Eutopia.

> L'utopia ha cambiato faccia; l'utopista è la persona che crede che tutto possa continuare come prima. Pablo Servigne e Raphaël Stevens[^11]

## Fare un passo indietro

Dobbiamo mettere in discussione la nostra percezione del breve e del lungo termine. Dobbiamo definire 100 anni come breve o lungo termine? È una questione di punti di vista, ma dobbiamo fare un passo indietro e lasciarci alle spalle la scala delle nostre brevi vite.

La Terra si è formata 4 miliardi di anni fa, la vita è comparsa 500 milioni di anni fa, l'Hominia, la stirpe degli esseri umani, si è separata dalla stirpe degli scimpanzé 7 miliardi di anni fa, ma da questa stirpe è nata solo 300.000 anni fa la nostra specie: l'Homo Sapiens[^13].

Tra 5-7 miliardi di anni, il Sole avrà esaurito il suo elio e si trasformerà in una gigante rossa, moltiplicando il suo raggio per 1.000 e attirando gradualmente la Terra nel suo alone ardente. La temperatura della Terra sarà allora così alta che gli oceani evaporeranno. Ma la vita sarà scomparsa molto prima a causa della mancanza di ossigeno[^14].

In realtà, gli esseri umani hanno ancora 2 miliardi di anni per vivere su un pianeta abitabile.

È importante visualizzare queste cifre su una scala per comprenderle chiaramente.

2 miliardi di anni sono visivamente 2.000.000.000 di anni.

Se consideriamo una generazione ogni 25 anni, si tratta di altre 80.000.000 di generazioni. E noi siamo attualmente solo la 12.000esima.... È passato solo lo 0,015% della nostra vita sulla Terra.

Tuttavia, la fine della Terra non significa la fine della specie umana, l'universo è vasto... Ma questa parte lascerà spazio ad altre storie immaginarie... Perché sarebbe prima il momento di imparare a sopravvivere sul nostro pianeta prima di sperare di poter sopravvivere su altri. Cosa che attualmente non avviene.

Quindi siamo solo all'inizio della nostra storia, e se si considera la velocità con cui stiamo consumando le risorse del nostro pianeta e il rapporto che abbiamo con esso e con tutti i suoi ecosistemi, non possiamo sperare di resistere a lungo...

Al ritmo attuale di produzione mondiale, avremo esaurito tutto il petrolio in 51 anni, tutto il gas in 53 anni, tutto il carbone in 114 anni, tutto l'uranio in 100 anni, e non potremo dare molto di più per tutte le terre rare che sono essenziali per le tecnologie di oggi. [^11]E speriamo di durare 2.000.000.000?

Da qui l'importanza di fare un passo indietro, guardare oltre le nostre brevi vite umane e pensare a lungo termine. Torneremo su questo punto più avanti, ma è importante rendersi conto che se siamo così carenti in questa capacità, è perché il cervello dell'Homo Sapiens non è progettato per pensare a lungo termine. [^15]

Noterete anche che non è la Terra che stiamo cercando di salvare, perché ha ancora un futuro brillante davanti a sé, con o senza di noi; ciò che stiamo cercando di salvare è l'esistenza umana, e con essa le varie specie che compongono il nostro ecosistema.

## Quando i sogni diventano realtà

Forse vi state chiedendo come immaginare un altro mondo possa aiutare a cambiare quello esistente?

Dopo tutto, potremmo dire che sarebbe meglio dedicare il nostro tempo e le nostre energie a cambiare ciò che già esiste, piuttosto che immaginarne uno nuovo.

Ma ciò che accade non è indipendente da ciò che proviamo. Ciò che proviamo dipende da ciò che crediamo sia possibile. E ciò che pensiamo sia possibile dipende dalla nostra analisi.

I principi e i valori su cui si basano le fondamenta del mondo che vi proponiamo di scoprire sono stati molto poco sviluppati e analizzati dagli intellettuali. Questa analisi vi permetterà di credere che questa visione sia possibile, e quindi di tentarla. E quindi, in ultima analisi, di cambiare ciò che già esiste. Questa analisi deve essere il più coerente possibile per ottenere il sostegno del maggior numero possibile di persone e portare a un'azione collettiva, sensata e di ampio respiro.

Immaginando un mondo nuovo, scopriamo al confronto l'assurdità di quello attuale, che ci porta a mettere profondamente in discussione il mondo che ci circonda, ma anche noi stessi.

Immaginando un nuovo mondo, creiamo materiale, uno spazio di riflessione e di dibattito alla ricerca di un ideale comune.

Immaginando un nuovo mondo, riuniamo le persone attorno a un ideale comune, consentendo loro di unirsi per agire e cambiare ciò che già esiste.

Immaginando un nuovo mondo, possiamo trarne ispirazione per produrre strumenti e principi, applicarli nella vita reale, sperimentarli, iterarli e migliorarli.

Infine, immaginando un mondo diverso, ispiriamo sogni e speranza, un'emozione necessaria alla base di ogni grande rivoluzione.

Vi abbiamo parlato di questo dono dell'essere umano, la capacità di immaginare e raccontarsi storie, una facoltà che ci ha permesso di evolvere in modo esponenziale dalla nostra rivoluzione cognitiva di 90.000 anni fa.

È giunto il momento di cambiare ancora una volta la nostra storia per cambiare la storia. Dobbiamo essere guidati da un ideale comune, cambiare la nostra narrazione, modificare le nostre convinzioni in modo da poterci mobilitare e collaborare per cambiare il nostro futuro.

> "Nel corso della storia, le storie sono state i veicoli più potenti per il cambiamento filosofico, etico e politico... Quindi è attraverso le storie che possiamo realizzare una vera e propria 'rivoluzione'". Un manuale di resistenza contemporanea[^24]

Quando leggete queste righe, non dovete dire a voi stessi "È impossibile": probabilmente avete ragione, lo è nelle condizioni attuali, e non stiamo proponendo di applicare questa storia in questo momento. Ma dovete dire a voi stessi "Potrebbe diventare possibile".

Poi dovete chiedervi: "Cosa è possibile fare in questo momento per avvicinarsi a questo ideale?", e in questo modo costruire al meglio le tappe della vostra transizione. Ma per farlo, dovete prima sapere qual è l'ideale e dove state andando.

Poi basta guardare a ciò che è possibile fare in questo momento e a quali nuove possibilità si apriranno man mano che si lavora per raggiungerle.

Tuttavia, l'obiettivo non è quello di dirvi cosa fare o come farlo, ma di sviluppare una visione che vi permetta di scoprirlo da soli e vi aiuti a farlo con gli strumenti giusti.

È inoltre importante rendersi conto che questa visione non è fissa: la state costruendo voi stessi ed è destinata a evolversi nel tempo, man mano che fate progressi e scoprite cose nuove.

Quindi è ancora possibile che il nostro sistema sia destinato a crollare, qualunque cosa facciamo. Sembra essere bloccato nella sua inerzia, distruggendo il suo ambiente e fuori controllo.

Ci sono molti libri sulle teorie del collasso, basati su studi molto seri, ma se un tale collasso dovesse accadere, non passeremmo da 7 miliardi a 1 essere umano, e ci sarebbe ancora abbastanza per ricostruire una società. Il crollo non avverrà da un giorno all'altro, sarà graduale con ondulazioni, e sarà sempre possibile cambiare direzione durante il suo svolgimento. [^16]

In entrambi i casi, il lavoro svolto qui è altrettanto interessante: o grazie ad esso riusciamo a cambiare la società in tempo per evitare il collasso, o mentre ci prepariamo alla resilienza per poter continuare a vivere durante il collasso, prepariamo le solide fondamenta per iniziare una nuova società.

Poiché il crollo non lascerà spazio a una società nuova di zecca pronta all'uso, saranno gli individui in quel momento a dover costruire i percorsi e a elaborare le direzioni da condividere insieme.

Da quel momento in poi, la nostra impreparazione potrebbe lasciare campo libero ad assemblee verbose e impotenti. Allo stesso modo, non si possono mettere in piedi sistemi di autogestione senza averli prima sperimentati, perché questo porterà quasi certamente a numerosi fallimenti. L'incapacità di stabilire un processo decisionale collaborativo e democratico a tutti i livelli della società potrebbe, a causa della situazione di emergenza, lasciare spazio ad altri sistemi di dominio che non sono auspicabili. Ecco perché oggi è necessario esplorare queste nuove strade. [^4]

Nonostante tutto, restiamo convinti che possiamo evitare il collasso. Il crollo sarebbe una catastrofe umanitaria ed ecologica, tanto più difficile da riparare, e per questo non possiamo definirlo auspicabile, anche se lascerebbe spazio a qualcosa di nuovo. Faremo quindi tutto il possibile per trovare e sviluppare soluzioni per evitarlo. Non dimentichiamo mai che anche se il sistema è potente, la nostra immaginazione collettiva lo è altrettanto, perché è proprio su questa immaginazione che il sistema esiste e resiste.

Infine, la costruzione di questa visione, anche se non viene accettata e desiderata, rivela come potrebbe essere il mondo. Può far capire alle persone che stanno semplicemente giocando a un gioco gigantesco con regole che creano incentivi e che la loro vita, le loro opportunità e i loro privilegi sarebbero molto diversi se le regole fossero cambiate.

> Se volete costruire una barca, non riunite i vostri uomini e le vostre donne per dare loro ordini, per spiegare ogni dettaglio, per dire loro dove trovare tutto. Se volete costruire una barca, fate in modo che i vostri uomini e le vostre donne desiderino il mare". Antoine de Saint Exupéry

## La lotta delle storie di transizione

Abbiamo visto sopra che è l'immaginario a creare il reale, e da sempre sono state le storie a far evolvere le nostre società. La nostra epoca non fa eccezione e le persone al potere non concepiscono la fine delle nostre società, ma credono in una narrazione ipermoderna e nella costruzione di una narrazione di transizione.

Immaginano un futuro in cui vivremo in città artificiali piene di tecnologie che permetteranno, tra l'altro, di contrastare le devastazioni del cambiamento climatico. Il nostro cibo sarà sempre più industriale, artificiale e sintetico. Saremo immersi in un consumo istantaneo sostenuto da un'economia dell'informazione, o meglio da un'economia della sorveglianza. Saremo inquilini di tutto, non chiederemo più nulla, ma saremo "felici".

Ma la narrazione ipermoderna e la costruzione della narrazione della transizione si sono spezzate. Questa modernità basata sull'ideale di un progresso materiale infinito, correlato a un progresso sociale per tutti, è sprofondata in una crisi multidimensionale ed è diventata completamente dissonante. La narrazione dell'ipermodernità ha sfruttato l'ideale di progresso e allo stesso tempo lo ha svuotato della sua essenza sociale emancipatrice. È manipolativa e sostenuta dai media, in particolare attraverso l'emergere di una nuova lingua. Le parole vengono dirottate dal loro significato originario, svuotate della loro sostanza, orientate secondo interessi nascosti. L'ipermodernità è iperaccumulazione, iperconsumo che nasce dal desiderio reso insaziabile dalla pubblicità invasiva. L'uomo moderno, che ha tutto, è paradossalmente il più insoddisfatto degli esseri umani. [^17]

La narrazione ipermoderna sta subendo tre grandi fallimenti che tutti cominciano a vedere chiaramente. Il primo è il suo fallimento ecologico e la promessa di un capitalismo verde, cioè di un produttivismo rispettoso della vita, sembra sempre più implausibile. Il secondo fallimento è di natura economica: l'accaparramento e la disuguaglianza su ogni scala, da quella locale a quella globale, aumentano esponenzialmente, nonostante l'eccezionale abbondanza materiale. Il terzo fallimento è di natura etica: le nostre relazioni sociali si stanno trasformando in relazioni commerciali, da cui tutti sperano di uscire vincitori, con tutto che viene comprato e venduto. Questi squilibri generano violenza e non sono più sostenibili.

In alternativa alla narrazione ipermoderna sta emergendo una narrazione di transizione che ci proietta in una nuova era, quella della noosfera. Ma è ancora lontana dall'essere costruita e dall'essere desiderabile. Questo è l'obiettivo del progetto Eutopia, costruire questa nuova narrazione...

Quindi dobbiamo capire cosa sogna davvero la gente? Questi sogni non devono essere trascurati: l'immaginazione è potente e sarà il primo passo sulla strada della realizzazione. Ad esempio, sogniamo di sentirci sempre e ovunque in vacanza? Qual è il nostro ideale collettivo? È urgente rispondere a questa domanda per poter dare un senso a questo movimento e progettare e attuare nuove politiche ora.

> Se vogliamo costruire un domani più luminoso, i bastioni della fraternità e dell'immaginazione creativa dovranno frapporsi tra noi e il crollo. Citazione modificata da Valérie Jousseaume[^17]

## Intelligenza collettiva

Nessuno possiede la verità, ma insieme possiamo almeno avvicinarci ad essa.

Non vi viene chiesto di credere a nulla di ciò che leggete qui, anzi, vi si incoraggia a non credere a nulla, a non pensare mai di possedere la verità e a mantenere sempre un po' di dubbio dentro di voi.

Perché credere di possedere la verità significa impedirsi di cercarla e quindi di trovarla.

Ecco perché non possiamo immaginare questo mondo da soli: abbiamo bisogno del maggior numero di persone possibile. Per realizzarlo, dobbiamo usare l'intelligenza collettiva e sviluppare i suoi strumenti.

Quindi, se state leggendo, stiamo modificando questo testo utilizzando uno strumento chiamato Git, che vi permette di visualizzare i progressi di questa storia. Inoltre, vi permette di partecipare alla modifica di parti di questa storia, inviandole in modo che tutti i collaboratori possano esprimere la loro opinione, discutere e infine votare se incorporare o meno la modifica. Una volta approvata, la modifica viene visualizzata online proprio dove la state leggendo.

L'idea è di progettare una piattaforma interamente dedicata all'editing collaborativo e democratico di questa storia. Lo strumento sarà abbastanza modulare da poter essere utilizzato in molti altri progetti.

L'applicazione si chiama Ekklésiapp, un mix tra applicazione ed Ekklesia, dall'antica parola greca per "assemblea", che deriva direttamente dalle assemblee dei cittadini delle antiche città greche, tra cui Atene, la culla della democrazia. Sebbene all'epoca la democrazia fosse accessibile solo alla classe dirigente, vedremo nel capitolo 5 "Come decidere insieme" quanto sia ancora imperfetta e quanto resti da fare. [^18]

Ecclesiapp è un'applicazione decisionale che può essere utilizzata da poche persone fino all'intera popolazione, ed è abbastanza modulare da poter creare un numero infinito di metodi decisionali diversi, per adattarla facilmente al lavoro da svolgere. Un metodo scelto può essere facilmente modificato, in modo da poter ricercare continuamente un metodo che offra i migliori risultati.

Questo metodo è liberamente disponibile. Può permettere ad altri di creare la propria utopia, distopia o eutopia. E tutto questo per sviluppare la ricerca in questo campo, che è necessaria e utile per le molte ragioni sopra citate.

Inoltre, consente ad altre persone di svolgere altri tipi di lavoro di scrittura collaborativa. Può essere utilizzato per gestire un'organizzazione, un'impresa o qualsiasi altro progetto, e infine offre un vero e proprio strumento democratico, che restituisce il potere alla base e che è abbastanza efficace da eliminare i sistemi di dominio e gerarchia.

Ma come possiamo concordare le regole di pubblicazione di questa storia?

Ecclesia si ispira direttamente al processo decisionale democratico che sarà messo in atto in questo mondo ideale. Infatti, il processo decisionale democratico in questo mondo ideale sarà ispirato da questa piattaforma. Perché questa piattaforma ci permetterà direttamente di testare le nostre idee sul giusto processo decisionale collettivo e democratico, di iterare su di esso fino a trovare quello che tutti concordano porterà ai migliori risultati. Quindi non solo darete la vostra opinione su una decisione, ma anche sul processo decisionale. L'attuale processo decisionale sarà svelato di seguito.

> "Da soli si va più veloci, insieme si va più lontano" Proverbio africano

## Il processo di redazione

Per quanto riguarda le competenze necessarie per giudicare questa storia o per parteciparvi, non ce ne sono.

Se pensate che il mondo sia complicato, lo è perché lo abbiamo reso complicato, ma in realtà, quando si parte da una buona base, tutto si rivela più semplice e logico di quanto sembri.

Tutto ciò che ci sembra complicato è in realtà solo una serie di elementi semplici, alcune cose hanno solo bisogno di più tempo di altre per essere riviste e assimilate.

Alla fine, i grandi scienziati sono quelli che hanno saputo intravedere questa semplicità del mondo.

Non dubitate di voi stessi: se non capite qualcosa, è semplicemente perché qualcuno non si è preso il tempo di spiegarvelo partendo dalle basi che conoscete.

Infatti, è fondamentale partire sempre dalle basi. È uno dei principi fondamentali di questa storia, ed è per questo che ci prenderemo il tempo di creare una solida base nella seconda parte, prima di descrivere Eutopia nel dettaglio nella terza parte.

Questo libro è destinato a essere letto e compreso da tutti. Gli intellettuali potrebbero trovare questo libro troppo semplicistico, con un vocabolario non abbastanza ricco, ma questo è l'obiettivo.

Poiché tutti dobbiamo essere agenti di cambiamento, come possiamo unirci se ciò che ci dovrebbe unire è incomprensibile e inintelligibile per chi non ha una conoscenza approfondita di questo o quel campo.

Qui non stiamo cercando di usare termini complicati o giri di parole lunghi e complessi, né stiamo usando questi metodi per apparire più scientifici, più intelligenti, e quindi potenzialmente creare in voi un falso senso di verità in questo modo.

Questa storia deve essere accessibile, comprensibile e diretta al punto. Il nostro intento è quello di essere onesti con voi e, senza ricorrere a effetti psicologici, vi lasceremo la piena libertà di credere o meno al suo contenuto.

Nella storia dell'umanità, ogni produzione di un autore è solo la produzione indiretta di un'intelligenza collettiva.

Questa storia nasce naturalmente dalla costante ricerca della "verità" che è stata portata avanti in tutta l'umanità dagli innumerevoli esseri umani che hanno contribuito all'evoluzione del pensiero globale. Ognuno di loro è nato in un momento, in un'epoca in cui ha ricevuto la conoscenza collettiva del passato e ha beneficiato di interazioni che li hanno portati, con l'aiuto delle capacità, dello status e del tempo di cui godevano, a raccogliere e pubblicare nuovi pensieri e conoscenze. I riferimenti diretti a questi individui nella nostra narrazione saranno annotati, ma eviteremo di descrivere a lungo il processo da cui sono nati i loro pensieri, per alleggerire questa narrazione, renderla accessibile ma anche per evitare di individualizzare troppo queste scoperte, e per promuovere un nuovo paradigma: comprendere che tutta la creazione di conoscenza e di pensieri nella storia dell'umanità è stata, anche indirettamente, solo collettiva. Ora vogliamo portare questa intelligenza collettiva ancora più avanti, con l'aiuto di strumenti che facilitino questi meccanismi, come quello utilizzato in questa storia. Nel corso di questa storia, noterete anche un certo linguaggio che cerca di uscire da questa individualità.

A nostro avviso, Eutopia è il frutto di un'intelligenza collettiva prodotta in migliaia di anni e non il frutto di individui isolati nel corso di queste epoche, compresa la nostra. Un'immagine mentale è quella di vedere l'umanità come un unico organismo che si evolve nel tempo, pieno di milioni e miliardi di cellule, che vive, muore e rinasce, evolvendosi e riadattandosi costantemente per trovare un equilibrio nel suo ambiente.

I riferimenti agli autori originali saranno comunque sempre annotati in fondo alla pagina, in modo che possiate saperne di più su queste persone e sul loro lavoro. Ma anche per rendere disponibili tutte le nostre fonti, in modo che le prove che presentiamo possano essere verificate.

Verrà sviluppata anche una seconda versione più ricca e scientifica di questa storia, che sarà direttamente collegata a questa per sostenere l'argomentazione dei suoi contenuti con le istituzioni accademiche. Questa versione sarà facilmente accessibile con un semplice clic ogni volta che si vorrà saperne di più sulla storia, le riflessioni e le argomentazioni su un determinato argomento, e sarà possibile approfondire ogni singolo argomento con un semplice clic.

Molti studi saranno citati nel corso della lettura, ma come regola generale vi invitiamo a essere cauti sulle conclusioni degli studi che leggete, perché è facile influenzare la scienza. Alcuni studi possono essere finanziati da aziende o gruppi di interesse con motivazioni e interessi specifici che influenzano lo studio.

Gli studi possono essere soggetti a errori di progettazione, come pregiudizi di selezione, errori di misurazione o errori statistici, che possono portare a risultati non rappresentativi o errati. L'interpretazione dei risultati può essere soggettiva e dipendere dalle convinzioni e dai pregiudizi dell'autore dello studio. È importante considerare i risultati nel loro contesto e considerare l'evidenza nel suo complesso. In generale, i dati possono essere presentati in modi diversi per sostenere un'argomentazione specifica e quindi portare a conclusioni errate. [^19][^20][^21]

L'idea non è quella di rifuggire dalla scienza, ma al contrario di affidarsi ad essa, ma con un occhio vigile, riconoscendo che la conoscenza prodotta è in continua evoluzione, che nuovi studi contraddicono quelli precedenti e che allo stato attuale è ampiamente influenzata dal sistema economico che finanzia la ricerca. [^22]

Inoltre, senza entrare nei dettagli, la metafisica mette in discussione gli assunti di base su ciò che esiste e su come le entità possono essere categorizzate e comprese. In particolare, esplora concetti come l'esistenza del libero arbitrio, la natura della mente in relazione al corpo e i fondamenti della realtà stessa. Pur non sfidando direttamente gli studi scientifici, la riflessione metafisica mette in discussione i quadri concettuali e gli assunti alla base della ricerca scientifica. Ciò può includere riflessioni su cosa significhi "replicare" uno studio, su come intendiamo la causalità e su cosa significhi che qualcosa sia "reale" o "vero" in un contesto scientifico, come ad esempio il fatto che l'atto di osservazione non è neutro ma influisce sullo stato dell'oggetto osservato. In breve, la metafisica mette in discussione i fondamenti filosofici di ciò che diamo per scontato nella nostra ricerca della conoscenza, anche nelle scienze[^23].

Ecco perché questo lavoro non è, e non sarà mai, incastonato nella pietra; la sua vocazione è quella di essere portato avanti dall'umanità nel suo complesso e di essere aggiornato nel corso della sua evoluzione, da e per se stesso. Pertanto, sarà sempre aperta a nuove prove che possono contraddire il suo contenuto, compresi i suoi fondamenti più profondi.

> Il dubbio è l'inizio della saggezza. Aristotele[^25]

## Conclusione dell'introduzione

Come avrete capito, state per leggere Eutopia, una storia che è oggi più che necessaria per l'umanità. È giunto il momento di abbandonare il nostro stato primitivo, di permettere alla nostra società di evolvere verso una vera civiltà, di evitare il disastro ecologico e umano che incombe. L'obiettivo di questa storia deve essere realizzato dall'umanità intera, da sé e per sé, per ridarle finalmente un senso e una direzione, per assicurarle un futuro garantito di libertà e felicità.

> Le tensioni e le contraddizioni dell'anima umana scompariranno solo quando scompariranno le tensioni tra le persone, le contraddizioni strutturali della rete umana. Allora non sarà più l'eccezione, ma la regola, per l'individuo trovare quell'equilibrio fisico ottimale che le sublimi parole "Felicità" e "Libertà" intendono designare: vale a dire l'equilibrio duraturo o addirittura l'armonia perfetta tra i suoi compiti sociali, la totalità delle esigenze della sua esistenza sociale, da un lato, e le sue inclinazioni ed esigenze personali, dall'altro. Solo quando la struttura delle interrelazioni umane si ispirerà a questo principio, solo quando la cooperazione tra le persone, base stessa dell'esistenza di ogni individuo, avverrà in modo tale che tutti coloro che lavorano fianco a fianco nella complessa catena dei compiti comuni avranno almeno la possibilità di trovare questo equilibrio, solo allora le persone potranno affermare con un po' più di ragione di essere "civilizzate". Fino ad allora, al massimo sono impegnati nel processo di civilizzazione. Fino ad allora, dovranno continuare a ripetere: "La civiltà non è ancora completa. È in divenire! Nober Elias, La dinamica dell'Occidente".

## Fonti (elenco non esaustivo)

[^1]: Yuval Noah Harari, Sapiens : Breve storia dell'umanità. 2011
[^2]: Sébastien Bohler, Dov'è il senso? / 2020
[^3]: [Etimologia della parola utopia](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, La buona economia: la reciprocità generale. 1984
[^5]: [Massima concentrazione di CO2 nell'atmosfera](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Limite di disturbo dei cicli biochimici del fosforo e dell'azoto](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Massima erosione della biodiversità](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Uso intensivo del suolo](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Uso abusivo dell'acqua dolce](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Acidificazione degli oceani](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Come tutto può crollare, Pablo Servigne & Raphael Stevens
[^12]: [Etimologia della parola eutopia](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, La storia della Terra
[^14]: Il futuro della vita sulla Terra" di Peter D. Ward e Donald Brownlee
[^15]: Sébastien Bohler, L'insetto umano. 2019
[^16]: Jared Diamond, Collasso. 2004
[^17]: Valérie Jousseaume, Plouc Pride: una nuova narrazione per la campagna. 2021
[^18]: Serge Christhonne, Le elezioni sono democrazia? 1977
[^19]: Ben Goldacre, Bad Science. 2008
[^20]: [Perché la maggior parte dei risultati delle ricerche pubblicate sono falsi](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [La crisi della replicazione](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documentario: La fabbrica dell'ignoranza. 2020
[^23]: Jaegwon Kim, Fondamenti di metafisica
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[^25]: Aristotele, Etica a Eudemo
