---
title: Dediche
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Per l'ideale che c'è in ognuno di noi e che soffre per un mondo che non c'è.

Per la sofferenza di coloro che amiamo, per la nostra sofferenza, per la sofferenza di coloro che non conosciamo e per tutti quelli che verranno...

Quest'opera è destinata a essere creata dall'umanità nel suo insieme e ad essere aggiornata nel corso della sua evoluzione, da e per se stessa.
