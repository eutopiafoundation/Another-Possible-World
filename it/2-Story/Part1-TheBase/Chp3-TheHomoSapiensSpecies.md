---
title: La specie Homo Sapiens
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Homo Sapiens

La specie umana può essere dotata di un'intelligenza formidabile e di una consapevolezza e di una conoscenza tecnologica sempre maggiori, ma è chiaro che di fronte alle conseguenze presenti e future del cambiamento climatico, con tanto di cifre a sostegno, non siamo in grado di cambiare rotta[^1].

Da qui la necessità di comprendere gli esseri umani e il loro funzionamento, perché solo così possiamo garantire che l'ambiente creato nella nostra Eutopia sia possibile e che possiamo prevedere il nostro comportamento al suo interno.

Comprendere le "debolezze" umane ci consentirà di adattarci e di sviluppare strumenti di conseguenza, in modo da poterle sfruttare per creare un punto di forza.

Comprenderemo i meccanismi a livello individuale, i suoi diversi aspetti fisici e psicologici... Ma questo approccio sarebbe insufficiente e si accompagna a tutta una serie di considerazioni globali e sociologiche.

<!-- ## Breve storia dell'Homo sapiens -->

## I bisogni dell'Homo Sapiens

Torniamo alla nostra domanda di ricerca:

"Come possiamo permettere a tutti gli esseri umani di avere una vita soddisfacente sulla Terra per i prossimi 80 milioni di generazioni?".

Siamo partiti dal principio che una vita appagante per ogni essere umano è una vita appagante che soddisfa i suoi bisogni e le sue aspirazioni. Un bisogno è una necessità percepita, sia essa fisica, sociale o mentale.

### Classificazione dei bisogni

Per aiutarci nella nostra ricerca, inizieremo con la piramide di Abraham Maslow, formulata nel 1943. Questa classifica i bisogni umani in 5 stadi e fornisce una base interessante per una panoramica dei principali bisogni degli esseri umani[^2].

Si noti che questo modello è oggi messo in discussione, perché secondo Maslow sentiamo un bisogno superiore solo quando il precedente è stato raggiunto. Ma questi bisogni non sono affatto piramidali e molti progressi hanno dimostrato che i bisogni sociali sono altrettanto importanti di quelli fisiologici, il che non significa che debbano trovarsi alla base della piramide, ma piuttosto che sono onnipresenti a ogni livello e tutti li sentono allo stesso tempo[^3].

In ogni caso, non è necessario capire esattamente quale bisogno sia più importante di un altro e in quali circostanze, perché l'obiettivo è soddisfarli tutti senza eccezioni.

Di seguito, completeremo la piramide aggiornando la teoria alla luce degli sviluppi delle conoscenze psicologiche e neuroscientifiche.

Piramide di Maslow](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

Al primo livello abbiamo i bisogni fisiologici, che sono i bisogni fondamentali necessari per mantenere la vita e il corretto funzionamento dell'organismo, come mangiare, bere, sessualità, dormire, respirare...

Vale la pena notare il totale fallimento del nostro sistema attuale nel soddisfare i bisogni primari dell'intera popolazione. Oggi, 25.000 persone sono morte di fame, tra cui 15.000 bambini sotto i 5 anni[^4]. Allo stesso modo, quando si parla di sessualità, questa è spesso tabù, stigmatizzata, discriminata e non educata, il che porta a numerosi disturbi, trasmissione di malattie, atti di non consenso, traumi, problemi di salute mentale e fisica, mancanza o dipendenza dai piaceri, mancanza di un senso di libertà e di fiducia, il che crea una mancanza complessiva di benessere con la propria sessualità per una parte molto ampia della popolazione.

Al secondo piano c'è il bisogno di sicurezza. La sicurezza di avere un rifugio, come un posto dove vivere, la sicurezza di avere accesso alle risorse desiderate, come attrezzature, strumenti, giochi... La sicurezza fisica dalla violenza, dalle aggressioni, ecc. La sicurezza morale e psicologica, la sicurezza emotiva e la stabilità, la sicurezza medica/sociale e la sicurezza sanitaria.

Anche in questo caso, notiamo le varie carenze della nostra società, anche nei Paesi occidentali, dove la sicurezza abitativa è una vera e propria sfida per molte persone, in particolare per i senzatetto, che spesso si trovano ad affrontare condizioni precarie e disumane[^5]. Anche l'accesso alle risorse può essere limitato per alcune persone a seconda del loro status socio-economico, il che può influire sul loro benessere e sulla qualità della vita. In termini di sicurezza medica e psicologica, c'è una mancanza di professionisti della salute mentale, che rende difficile l'accesso alle cure. Anche con la sicurezza sociale in paesi come la Francia, il costo delle cure può essere proibitivo per alcune persone, impedendo loro di ricevere le cure di cui hanno bisogno[^6].

Il terzo livello è costituito dai bisogni di appartenenza, dal bisogno di amare ed essere amati, di avere relazioni intime, di avere amici, di far parte di un gruppo coeso, di sentirsi accettati, di non sentirsi soli o rifiutati.

Viviamo in una società in cui l'individualismo è diffuso e/o le relazioni umane sono spesso trascurate a favore del successo personale. Gran parte della popolazione si sente sempre più sola e perde i legami sociali. Alcuni sono rifiutati e isolati, con conseguenti problemi di salute mentale come la depressione e l'ansia. [Il nostro modello sociale favorisce la competizione generalizzata e i rapporti di interesse personale o commerciale come unico modello relazionale, promuovendo una vera e propria cultura dell'egoismo.

Il quarto livello rappresenta i bisogni di stima: gli esseri umani hanno bisogno di sentirsi considerati, di sentirsi apprezzati e rispettati dagli altri, di sentirsi riconosciuti per i loro risultati e di sentirsi capaci di avere successo. Questi bisogni vengono soddisfatti facendo cose che sono apprezzate dalla società, fissando obiettivi e lavorando per raggiungerli, o impegnandosi in attività che consentono lo sviluppo personale.

Il quinto livello è il bisogno di auto-realizzazione. Si tratta del bisogno di realizzarsi, di sfruttare e sfruttare al meglio il proprio potenziale personale. Si manifesta nel desiderio di sviluppare i propri talenti e le proprie capacità, di impegnarsi in attività che diano significato e soddisfazione e di superare se stessi.

Nella nostra società è difficile soddisfare questo bisogno di auto-realizzazione, poiché le pressioni sociali e le elevate aspettative imposte alla maggior parte degli individui possono rendere difficile perseguire le proprie passioni e i propri interessi personali. La nostra società altamente competitiva e la sua alienazione dell'individuo lasciano poco tempo per esplorare e sviluppare talenti e capacità. Molte persone sono anche escluse dal sistema, in particolare le persone con disabilità che non possono inserirsi in esso perché non è adattato a loro. [^9]

Tuttavia, questo bisogno di auto-realizzazione è stato in qualche modo messo in discussione nel 2010 dal lavoro di 4 psicologi, che hanno proposto un aggiornamento della teoria dell'evoluzione. [^3] Questa elimina il bisogno di auto-realizzazione per fare spazio a tre bisogni legati alla pulsione riproduttiva: l'incontro, la continuità e la posterità.

|     |     |
| --- | --- |

| Bisogno di posterità: rimanda l'inevitabile morte.
Bisogno di continuità: mantenere il proprio partner, il proprio status, ecc.
| Bisogno di incontro: Respingere i concorrenti e l'isolamento sociale.
| Bisogno di stima e di status: essere riconosciuti come un essere unico.
| Bisogno di appartenenza o di affiliazione: essere amati e accettati per quello che si è.
| Bisogno di protezione: riparo, sicurezza +fisica, familiare, psicologica, finanziaria, ecc.
| Bisogni fisiologici: respirare, bere, mangiare, darsi sollievo, dormire, tenersi al caldo.

\*Elenco dei sette bisogni umani fondamentali, inconsci e concomitanti, tratto da D. Kenrick, V. Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Si tratta di bisogni inconsci, in evoluzione, che si esprimono in mille modi diversi nella vita di ogni persona. Per esempio, la creatività artistica può basarsi tanto sul bisogno di acquisire uno status e di essere riconosciuti socialmente come un essere unico, quanto sul bisogno di sedurre il partner o di durare nel tempo lasciando un'opera ai posteri.

Abbiamo appena passato in rassegna i 5/7 tipi di bisogni, ma va notato che verso la fine della sua vita Abarham Maslow ne aggiunse un sesto, il "superamento di sé". Questo si trova al vertice della piramide:

> "L'essere umano pienamente sviluppato che lavora nelle migliori condizioni tende a essere motivato da valori che trascendono il suo 'sé'"[^9].

È interessante rendersi conto che un essere umano realizzato, che va oltre la realizzazione di sé, sarebbe pienamente aperto ad andare oltre la propria individualità per impegnarsi al servizio degli altri.

Per concludere, queste esigenze possono essere integrate dalle riflessioni di Hannah Arendt.

Nella nostra condizione umana, siamo spinti verso una ricerca di eternità: la vita contemplativa. Una ricerca che è sempre più difficile da soddisfare nelle nostre società moderne, che non credono più in Dio. Siamo anche spinti alla ricerca dell'immortalità, la vita activa. [^10]

La ricerca dell'immortalità è ciò che spinge un essere umano mortale a dire e fare cose creative che vanno al di là del proprio bisogno vitale, della propria esistenza, e lo proiettano nel futuro oltre la propria morte.

In origine, l'età della modernità riconosceva socialmente il bisogno di immortalità, ad esempio attraverso l'eroismo dei soldati, attraverso i titoli e i motti, o attraverso il coraggio e l'autenticità di una parola senza compromessi: "Si pensa, si parla con forza solo dal fondo della propria tomba: è lì che ci si deve collocare, è lì che ci si deve rivolgere agli esseri umani" (Diderot, 1782). Oggi, tutti questi titoli, tutte queste parole, tutte queste azioni, non risuonano più nella nostra società. Hannah Arendt sostiene che la repressione sociale della ricerca dell'eternità al di fuori del dominio pubblico, unita alla repressione sociale della ricerca dell'immortalità, spiega come "l'epoca moderna - iniziata con un'esplosione di attività umane così nuove, così ricche di promesse - si sia conclusa con la più inerte, sterile passività che la storia abbia mai conosciuto". [^10][^11]

Emmanuel Todd fa un ragionamento simile. Egli sostiene che il passaggio dall'"uomo che crede nell'uomo" all'"uomo che crede in Dio" ha generato una notevole energia collettiva. L'allontanamento dalla ricerca dell'eternità ha rafforzato le energie verso la ricerca dell'immortalità. Quando la Chiesa come struttura sociale è crollata, si è sviluppato un nuovo ideale umanista secolare. Si trattava dell'immensa speranza nell'Uomo Nuovo portata dai movimenti rivoluzionari del XIX secolo. D'altra parte, il crollo dell'ideale sociale comunista e dell'ideale rivoluzionario repubblicano segna la fine sociale della ricerca dell'immortalità. L'uomo che crede nell'uomo scompare a sua volta e diventa l'uomo che non crede più in nulla, un essere umano nichilista e apatico, senza speranza né scopo, che riempie il suo vuoto esistenziale attraverso il consumo" [^12].

Cosa soddisfa il nostro bisogno di posterità? Secondo Hannah Harendt, è l'opera. Un lavoro è ciò che è in grado di lasciare una posterità, in altre parole un figlio. Significa anche piantare alberi sotto i quali il passare del tempo non ci permetterà di venire a fare ombra. Significa trasmettere qualcosa ai bambini. Significa scrivere poesie o intagliare pezzi di legno. Significa sacrificare eroicamente la propria vita. Significa fare un lavoro che ha un significato. Offre sicurezza interiore e autocontrollo. Attraverso le loro azioni, le persone mostrano al mondo il loro lavoro unico e danno un assaggio della loro immortalità. Il lavoro di ciascuno, creato in privato e offerto in pubblico, è ciò che rende il mondo umano un mondo. [^10]

Il lavoro di sussistenza è mera sopravvivenza. Secondo il filosofo, il lavoro è una necessità vitale che sostiene la nostra esistenza fisiologica. Il lavoro è una dimensione ristretta della vita umana sulla quale non abbiamo alcun potere reale. Hannah Arendt deplora il fatto che il progetto di vita proposto collettivamente sia stato bloccato dall'incredibile posto occupato, nelle società moderne, dal lavoro di sussistenza. Secondo lei, il lavoro dovrebbe essere relegato nella sfera privata e avere un'importanza secondaria. Il lavoro dovrebbe essere al centro della vita sociale pubblica.

La riflessione filosofica di Hannah Arendt sull'eccessiva importanza del lavoro di necessità vitale e sulla scomparsa del lavoro della posterità fornisce molti collegamenti esplicativi alla situazione contemporanea. Ci aiuta a comprendere la scomparsa dei cittadini, sostituiti dai consumatori. Ci permette di comprendere l'assoluto disordine esistenziale che la disoccupazione può produrre in una società che ha dimenticato il lavoro e che è organizzata pubblicamente solo intorno al lavoro di sussistenza. Ci aiuta a capire l'atteggiamento delle giovani generazioni nei confronti del lavoro di sussistenza e la loro ricerca di un senso in un mestiere. Infine, ci permette di esaminare la piramide dei bisogni nelle sfaccettature dell'essere umano.

Da un punto di vista fisico, sopravvivere significa riprodursi e i diversi hanno figli. Da un punto di vista spirituale, significa essere eterni. Da un punto di vista sociale, essere immortali significa lasciare una traccia del proprio tempo sulla terra. La ricerca dell'immortalità, come la intende Hannah Arendt, è l'espressione sociale del bisogno di posterità che permea ogni aspetto della nostra vita fisica, psicologica, sociale e spirituale. In questo senso, la discendenza incontra la trascendenza. Va ricordato che, per Hannah Arendt come per Kenrick Douglas e i suoi colleghi, il bisogno di posterità combina due necessità: la produzione intima di un'opera del tutto unica e personale, unita a una relazione con il mondo che possa manifestarla.

Questo bisogno multidimensionale di posterità è completamente oscurato dalla nostra società funzionalista e utilitarista. Eppure è proprio qui, in questi atti che non hanno un apparente significato economico o razionale per la vita quotidiana o per lo sviluppo di benefici immediati, che troviamo il senso, la grandezza. La società moderna, proponendo una vita limitata alla sopravvivenza vitale e allo sviluppo materiale, ha gradualmente tagliato l'accesso alla soddisfazione del bisogno di posterità. Il risultato è un fastidioso senso di insoddisfazione e di inappagamento, in una società in cui, paradossalmente, l'abbondanza e il piacere regnano sovrani. È qui che entra in gioco la ricerca di senso. Questa ricerca di senso sembra non trovare riconoscimento nella società ipermoderna. [^11]

Ecco dunque i bisogni umani fondamentali integrati dalle riflessioni di Hannah Arendt. Le frecce rappresentano le diverse dimensioni del corpo umano secondo la medicina orientale. Questa rappresentazione è stata scelta prima delle distinzioni classiche della moderna medicina occidentale, perché amplia lo spettro e include la vita comtemplativa di Hannah Arendt. Le frecce sono tratteggiate per indicare che sono al di fuori della portata della scienza attuale.

[Bisogni umani fondamentali integrati dalle riflessioni di Hannah Arendt](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)]

### Bisogni o desideri intrinseci ed estrinseci

Esiste una differenza tra bisogni o desideri intrinseci ed estrinseci.

I bisogni intrinseci sono motivazioni interne e personali che provengono dall'interno dell'individuo.

I bisogni estrinseci sono motivazioni esterne che provengono dall'ambiente e dall'individuo. Si riferiscono all'immagine che l'individuo vuole proiettare agli altri e anche a se stesso. Il consumo può soddisfare contemporaneamente bisogni di entrambi i tipi e la maggior parte dei consumi che soddisfano bisogni intrinseci hanno anche una dimensione estrinseca.

I bisogni estrinseci sono strettamente legati a varie norme sociali, allo status, al comportamento etico e all'immaginario collettivo. A volte la soddisfazione dei bisogni estrinseci comporta uno svantaggio rispetto a quelli intrinseci[^13].

La distinzione sociale e l'imitazione creano desideri estrinseci.

Ogni società crea desideri e bisogni estrinseci.

Nella nostra società, questi desideri sono costantemente e fortemente manipolati: l'educazione e l'ambiente sociale inculcano modelli di stile di vita, o molto spesso la sensazione che più è meglio, e la pubblicità crea un desiderio dopo l'altro[^14].

L'insoddisfazione nasce dal divario tra i desideri e la loro realizzazione, o in altre parole tra le aspirazioni e le conquiste.

Per limitare questa insoddisfazione, ci sono due approcci possibili: ridurre il desiderio o soddisfarlo. Spesso si pone l'accento sul secondo mezzo, la soddisfazione del desiderio, ma l'esistenza del primo è quasi del tutto dimenticata o trascurata.

Perciò dobbiamo innanzitutto capire come si formano i nostri desideri e come possiamo controllarli. Ed è quello che faremo.

## Natura umana

Ma prima di tutto è essenziale fare il punto sulla natura umana, perché è un punto che ritorna regolarmente nel dibattito. Le scienze sociali e le neuroscienze cercano da tempo di distinguere tra ciò che è innato e ciò che è acquisito, ma non c'è consenso sulla natura umana.

Mentre le scienze naturali sembravano sostenere l'utilitarismo per più di mezzo secolo, oggi è possibile contraddire questa ipotesi: l'uomo non solo è egoista, ma anche non perfettamente razionale. Questo non utilitarismo dell'essere umano vale per le forme di vita più primitive: la cooperazione è inscritta fin dalle origini e sarebbe, con la sopravvivenza del più adatto, uno dei motori essenziali dell'evoluzione[^15].

Credere che la nostra evoluzione si basi esclusivamente sulla competizione interindividuale significa avere una visione distorta dell'evoluzione, mentre il ruolo della cooperazione era già presente nella teoria evolutiva darwiniana. [^16]

Ma qualunque sia la natura umana, gli innumerevoli esempi della storia dimostrano che essa può diventare qualsiasi cosa: la peggiore come la migliore.[^17][^18] La natura umana non dovrebbe giustificare nulla di ciò che desideriamo diventare. Possiamo certamente decidere cosa vogliamo diventare nella nostra società, perché abbiamo semplicemente l'opportunità di farlo.[^19]

Ciò che è certo, tuttavia, è che gli esseri umani non sono eremiti; vivono in società e sono socievoli. Gli esseri umani sono innanzitutto homo sociabilis, con una propensione ad avvicinarsi ai propri simili e a interagire con loro. Hanno bisogno di parlare, di scambiare informazioni, di condividere conoscenze ed emozioni, ma hanno anche un bisogno fondamentale di esistere agli occhi degli altri. Cercano l'interazione umana per la socievolezza che offre e per lo scambio di immagini positive e gratificanti di cui si nutrono. [^20][^21]

## Dare forma ai nostri desideri

Abbiamo detto in precedenza che dobbiamo prima capire come si formano i nostri desideri e come possiamo controllarli.

Da oltre un miliardo e mezzo di anni abbiamo un piccolo organo nel cranio chiamato striato.

Lo striato rilascia la dopamina, una sostanza che ci dà una sensazione di piacere e rafforza un comportamento identificato come un successo. [^22]

Fa parte di un intero meccanismo di cooperazione e dialogo tra diverse strutture e partecipa alla decisione, riceve informazioni e confronta le diverse opzioni, i potenziali guadagni, lo sforzo richiesto, l'immediatezza della ricompensa.

Questo sistema è spesso chiamato circuito della ricompensa, ma la dopamina è solo un ormone che prevede l'esito di un'azione. [^23]

Queste scariche di dopamina danno origine a incentivi che ci spingono all'azione e che rafforzeranno quel comportamento o quell'attività in futuro.

Questi rinforzi, che possiamo anche chiamare motivazioni profonde del nostro cervello, sono : [^22]

- cibo
- il sesso
- Posizione sociale
- Necessità di informazioni
- Minimo sforzo
- Stimolazione intellettuale

Queste motivazioni erano essenziali per la nostra sopravvivenza e lo sviluppo della nostra specie in tempi primitivi e sono ancora oggi molto sensibili. I neuroni dello striato e di tutto il circuito della ricompensa ci attraggono verso ciò che soddisfa i nostri rinforzatori, cioè tutto ciò che fornisce cibo, sesso, status, facilità e informazioni.
Non sono intrinsecamente dannosi, ma come possiamo vedere oggi, possono essere sfruttati per sviluppare economie produttive, consumistiche, individualistiche e distruttive dell'ambiente.

La particolarità di questo meccanismo, che condiziona il nostro cervello non solo a cercare cibo, denaro, sesso e status sociale, è che cerca di ottenere sempre più denaro, sempre più cibo, sempre più sesso o sempre più status.

Quando un topo viene messo vicino a un corridoio con un pezzo di formaggio alla fine, i suoi neuroni rilasciano dopamina non appena entra nel corridoio. La prima volta che trova un pezzo di formaggio alla fine del corridoio, i suoi neuroni scaricano dopamina; la seconda volta, se trova il formaggio, i suoi neuroni dopaminergici non fanno nulla. Ma se trova due pezzi di formaggio invece di uno, i suoi neuroni della dopamina si scaricano di nuovo. [^22]

Lo striato fa tutto questo solo nella misura in cui può fare sempre di più.

Quindi riusciamo a stimolare i nostri circuiti del piacere solo aumentando le dosi.

Un altro dei nostri meccanismi è che il piacere e la facilità che possiamo permetterci ora hanno un peso cento volte maggiore nelle nostre decisioni rispetto alla considerazione di un futuro lontano. Più un beneficio è lontano nel tempo, meno valore ha per il nostro cervello. Questo fenomeno è noto come svalutazione temporale.

Privilegiamo il piacere immediato rispetto a quello futuro, oppure facciamo una scelta che ci avvantaggia ora ma che avrà conseguenze dannose a lungo termine.

Più lungo è il ritardo, più debole è la risposta anticipatoria[^24].

Esistono certamente differenze genetiche tra le persone, e alcune sono più sensibili al loro striato, ma sono soprattutto gli elementi legati all'educazione e all'ambiente socio-culturale a rivelarsi decisivi. [^25][^26]

Questi neuroni sono alla base dei nostri processi di apprendimento.

Ci condizionano: una volta appreso che il cibo segue un gesto, il gesto stesso diventa attraente. Questo è il principio dell'apprendimento sociale: siamo condizionati ad apprezzare questo o quel comportamento. Il principio del condizionamento operante regna sovrano in un luogo di apprendimento per eccellenza, la scuola.

Egoismo e altruismo sono entrambi ugualmente motivati dal piacere.

Quando facciamo un regalo a qualcuno che amiamo, non ci viene in mente di farlo per sentirci bene, eppure ci sentiamo bene.

Potremmo quindi chiederci: se l'altruismo ci fa sentire bene personalmente, e per di più ne siamo consapevoli, è ancora altruismo o non è piuttosto una sorta di interesse personale ben riposto?

C'è una linea sottile tra altruismo ed egoismo, una linea che non esiste nemmeno a livello biologico. Non esiste un ormone dell'altruismo o un ormone dell'egoismo. Esiste un atto per il quale il nostro cervello calcola la probabilità di sapere se sarà vantaggioso per noi, e da cui traiamo piacere. Le aree cerebrali coinvolte sono comuni, ma quello che notiamo è che nell'egoismo c'è una maggiore attivazione della corteccia prefrontale dorsolaterale a causa della valutazione più accentuata del calcolo costi-benefici. [Nel complesso, però, siamo noi a classificare gli atti come altruistici o egoistici, e questa definizione può quindi variare a seconda delle modalità di pensiero.

Diremo che nell'atto egoistico c'è solo lo scopo di ottenere benefici personali.

Nell'atto altruistico, l'intenzione è quella di dare benefici agli altri, ricevendo un beneficio che è inconsciamente ricercato. Ma ci può anche essere l'intenzione di dare un beneficio a un altro, ricevendo un beneficio cercato consapevolmente. [^28]

L'atto altruistico è quindi uno scambio reciproco che va in entrambe le direzioni, mentre l'atto egoistico va solo in una direzione.

Sia l'egoismo che l'altruismo possono quindi portare al piacere individuale, e scopriremo quale di queste due motivazioni è la più efficace per raggiungere il nostro obiettivo comune.

Quindi, se alcune persone sono più generose, probabilmente è perché il loro cervello è stato configurato in questo modo fin dalla più tenera età. Come Madre Teresa, nota per la sua incrollabile dedizione all'aiuto degli altri, la sua compassione e il suo amore incondizionato per tutti coloro che incontrava, ha dedicato la sua vita a fare del bene agli altri, senza preoccuparsi della sua sicurezza personale o del suo benessere, ed è diventata uno dei simboli più importanti della carità e dell'altruismo in tutto il mondo. Quando aveva solo 6 anni, sua madre la portava a visitare i più poveri tra i poveri, gli alcolisti e gli orfani. E le dava una raccomandazione invariabile e ostinata: "Mia figlia non accetta mai un boccone di cibo che non sia condiviso con altri". La condivisione, insegnata fin dalla culla con regolarità e con una costante attenzione all'applicazione e all'esemplarità, divenne una forma di condizionamento così potente che lo striato di Madre Teresa non vacillò mai negli ottant'anni della sua vocazione. Il suo ambiente e la sua educazione hanno quindi contribuito a sviluppare in lei valori e motivazioni come la generosità, la compassione e il desiderio di servire gli altri[^29][^30] Naturalmente, la religione, sostenendo questi valori morali di generosità e altruismo, svolge un ruolo importante nella configurazione del cervello di Madre Teresa, e questo deriva dalla nostra ricerca di significato, di cui parleremo più avanti.

Gli studi hanno anche rivelato l'esistenza di un bonus per l'agilità mentale e la capacità di risolvere i problemi, che porta all'attivazione delle zone di memoria e a una migliore memorizzazione delle informazioni comunicate dall'insegnante. La dopamina ricevuta da un allievo quando riceve un buon punto modula la plasticità sinaptica e consolida i ricordi. [^31]

Naturalmente, lo striato non è l'unica parte del nostro cervello che prende decisioni. La corteccia frontale è la parte più anteriore del cervello umano ed è diventata sempre più spessa nel corso dell'evoluzione, riflettendo le nostre crescenti capacità cognitive. È la sede della forza di volontà e della pianificazione.

Ad esempio, quando ai partecipanti è stato chiesto di scegliere tra venti euro subito o 30 euro tra due settimane, quelli che hanno preso i soldi subito hanno visto accendersi lo striato, mentre quelli che non lo hanno fatto hanno visto accendersi la corteccia frontale[^32].

Quindi, se vogliamo formare una comunità umana capace di cogliere le sfide del futuro, queste sono le connessioni che dobbiamo sviluppare attraverso il duro lavoro, la consapevolezza e la perseveranza.

## Controllare i nostri desideri

Per limitare gli effetti nocivi del nostro striato, ci sono 3 possibili soluzioni,

- Sopprimere lo striato: storicamente, l'attività dello striato è stata bloccata dai comandi morali e dallo sforzo della volontà contro le tentazioni. Ma questa non è una soluzione: se reprimiamo il nostro striato, possiamo compromettere la nostra capacità di provare motivazione o piacere, il che può portare a problemi come la depressione e vari disturbi. La repressione ha conseguenze negative sul benessere e sul funzionamento dell'individuo e non è quindi una soluzione efficace né sostenibile. [^33]

- Prendendo lo striato al suo stesso gioco, possiamo effettivamente giocare sui rinforzi primari per raggiungere una società ideale, ad esempio rendendo la norma sociale uno stile di vita sano e sostenibile. Spingere lo sviluppo dei rinforzi essenziali per una buona società.

- Ma una soluzione più efficace e sostenibile è quella di fare appello alla capacità unica dell'essere umano, la coscienza. Perché la forza dello striato deriva dal fatto che i suoi comandi non sono coscienti.

Quando i neuroni dello striato si sono abituati a un certo livello sociale, diventano ottusi e non sentiamo più nulla, quindi diventa indispensabile salire di una tacca per stimolarli. Questo processo di incremento non produce alcuna soddisfazione duratura, non può portare alla felicità.

La maggior parte delle nostre azioni sono intraprese con un livello di coscienza molto basso. Il più delle volte agiamo in modo meccanico. Anche quando ci dedichiamo ad attività intellettuali, sarebbe sbagliato dire che lo facciamo consapevolmente. [^34]

Abbiamo una corteccia cerebrale con un'enorme potenza di calcolo, che usiamo principalmente per scopi utilitaristici, prestazionali e tecnici. Per diversi millenni, il nostro potere di astrazione, concettualizzazione e pianificazione è stato utilizzato principalmente per progettare strumenti che soddisfano il nostro striato.

Siamo esseri con un alto livello di intelligenza ma un basso livello di coscienza. [^22]

L'intelligenza sviluppa soluzioni, genera calcoli, realizza obiettivi e programmi. Ma può fare tutto questo senza coscienza. L'esempio più evidente è l'intelligenza artificiale, che può svolgere compiti estremamente complessi senza alcuna coscienza.

I grandi sistemi di intelligenza artificiale che conosciamo oggi funzionano anche con una nozione di probabilità e con un sistema di rinforzo, che prevede di premiare o punire le azioni per favorire l'apprendimento di comportamenti desiderabili. In altre parole, questi sistemi funzionano in modo simile alla dopamina negli esseri umani.

Quindi, se vogliamo creare una coscienza nell'IA, dobbiamo solo capire come viene creata la nostra. Questo mistero
non è ancora del tutto compreso dalle neuroscienze, e ci torneremo più avanti. Ma se lo sarà e riusciremo a creare questa coscienza, la differenza tra noi e una macchina diventerà molto sottile. Questo possibile progresso solleva un gran numero di questioni etiche e ci porta a riflettere sulla percezione della nostra stessa coscienza.

In ogni caso, una parte della soluzione alle sfide che la nostra specie deve affrontare consiste nell'aggiungere maggiore consapevolezza alle nostre azioni quotidiane.

La coscienza è anche una cassa di risonanza per le nostre percezioni.

Sviluppando la nostra camera di risonanza sensoriale, possiamo far credere al nostro striato che sta ricevendo più piacere, mentre noi gliene stiamo dando meno in termini quantitativi.

Uno strumento molto pratico per sviluppare la nostra consapevolezza è la mediazione. [^35]

Portare il nostro livello di coscienza a un livello paragonabile a quello della nostra intelligenza sarà senza dubbio una sfida importante per sviluppare una buona società e garantire il futuro della nostra specie. L'evoluzione verso una società della coscienza e verso un'economia della crescita mentale.

## Il bisogno di significato e di certezza

Una volta che l'individuo ha l'autonomia di scelta e di credo, in altre parole il diritto di dare alla propria vita la direzione che desidera, definire tale direzione è diventato un compito molto difficile. Non essendo più la direzione stabilita dalla religione o da un regime politico totalitario, ognuno di noi deve creare il proprio significato. Tuttavia, quando ci rendiamo conto che la nostra esistenza è breve e destinata al nulla, ci troviamo a dover trovare una giustificazione per la nostra esistenza e le nostre azioni, e questa consapevolezza può diventare insopportabile. [^36]

Poiché il significato è più importante di qualsiasi altra cosa per la vita, ha un valore di sopravvivenza per l'essere umano. Quindi, per compensare la nostra mancanza di significato, andiamo alla ricerca di denaro, status, ecc...

Il segnale di piacere che si genera a monte di ciò che l'animale sta cercando è una previsione fatta dal nostro cervello su ciò che accadrà. Questa produzione avviene perché è un vantaggio evolutivo, un meccanismo che aumenta le possibilità di sopravvivenza dell'animale.

Essendo in grado di produrre ciò che accadrà in base a ciò che osserva intorno a sé, decuplica il suo controllo e il suo potere decisionale. Può cercare situazioni vantaggiose e fuggire da quelle potenzialmente pericolose. Sono un passo avanti rispetto alla realtà.

Il cervello degli animali vertebrati ha inventato un modo per fare previsioni e stare un passo avanti rispetto alla realtà. Questa capacità di stabilire collegamenti tra lo stato dell'ambiente in un dato momento e il suo stato futuro è alla base di ciò che chiamiamo, in una specie molto celebrata come l'Homo Sapiens, senso.

Abbiamo creato un senso della società umana e sappiamo che possiamo essere accettati e trovare la nostra strada in essa, a patto di rispettare le regole e i codici stabiliti. Questa società non è nel caos, ha un ordine e questo ordine intelligibile è fondamentalmente rassicurante per noi.

La nostra tendenza a individuare legami di significato all'interno del nostro ambiente è così sviluppata e insopprimibile che a volte ci porta a discernere legami che non esistono necessariamente.

Per esempio, quando un guerriero va a caccia di prede, indossa una collana e caccia la preda con essa, la volta successiva che indosserà la stessa collana, il guerriero riceverà una piccola scossa di dopamina per la possibilità di catturare un'altra preda. Il guerriero si sentirà più sicuro del suo futuro successo.

Questo sistema di anticipazione aiuta a ridurre la sensazione di incertezza sul futuro. Osservare, prevedere e anticipare gli eventi futuri e ridurre l'ansia fanno parte della nozione di significato. Questo vantaggio è così decisivo che c'è ragione di credere che sia stato selezionato dall'evoluzione.

Individuare il significato intorno a noi è talmente cruciale per la nostra sopravvivenza che le situazioni in cui questo significato ci sfugge scatenano un'ansia fisiologica acuta. Questa reazione è provocata dal nostro organismo come istinto di sopravvivenza[^37].

Utilizzando strumenti di imaging cerebrale, possiamo scoprire cosa succede nel cervello. Entra in azione una piccola striscia di corteccia cerebrale, situata pochi centimetri sopra lo striato. Si tratta di una piega della corteccia cerebrale situata all'interfaccia tra due emisferi cerebrali, a loro volta collegati allo striato, ed è chiamata corteccia cingolata anteriore. Questa corteccia cingolata interna si accende non appena le previsioni fatte non sono più confermate da ciò che accade realmente.

Questo segnale di allarme indica una violazione della previsione. Se troppe previsioni vengono invalidate, diventa difficile organizzarsi e si ha l'impressione di cadere nel caos. Sovraccaricato, questo segnale di errore diventa dannoso per la salute fisica e mentale dell'individuo. Innesca una potente reazione di stress nell'organismo, con la corteccia cingolata che attiva un circuito nervoso multi-linked che porta fino a un centro cerebrale coinvolto nella paura e nell'ansia, l'amigdala, poi alle ghiandole adrenocorticali situate sui reni e ai nuclei neuronali del tronco encefalico, che rilasciano ormoni come il cortisolo e la noradrenalina, con l'effetto di porre il corpo in una posizione di fuga, fino alla paralisi, e di provocare un'ansia che può diventare esistenziale.

La corteccia cingolata svolge quindi il ruolo di segnale d'allarme che ci avverte quando il nostro mondo non ha più un senso individuabile. Le conseguenze di questa reazione vanno dai disturbi del sonno alla depressione, all'ansia, al declino della memoria, alle malattie cardiovascolari e al diabete.

Se il livello di ordine e organizzazione del nostro ambiente comincia a diminuire, questa parte centrale del nostro cervello si attiva e ci avverte della presenza di un potenziale pericolo per la nostra sopravvivenza. Se la società è relativamente stabile, dove le strutture del lavoro, della famiglia e delle relazioni umane non cambiano in modo troppo imprevedibile e arbitrario, la corteccia cingolata è un fattore di adattamento e di adeguamento, ma se i punti di riferimento cambiano troppo rapidamente e costantemente senza dare tregua all'individuo, può essere estremamente pericoloso per sé e per gli altri.

Ma di fronte al vuoto di significato, come reagisce la mente umana? Costruisce sistemi di rappresentazione intrisi di significato, ordine e coerenza. Da quando esiste l'uomo, non ha fatto altro che dare un senso alla realtà.

I primi tentativi hanno assunto la forma di racconti mitici della creazione e della natura. Il bisogno di significato deriva dalla necessità di controllo ed è una conseguenza del nostro desiderio di sopravvivenza. Immaginare il mondo come un luogo abitato da significati tranquillizza il sistema di allarme interno in caso di errore di previsione una tantum nel mondo concreto.

Di conseguenza, la reazione di allarme innescata dalla corteccia cingolata è naturalmente attenuata nei credenti. Ma oggi, per la maggior parte delle persone nel mondo occidentale, non crediamo più e abbiamo perso questo senso.

I grandi sistemi di significato religioso, ma anche ideologico, democratico o filosofico, non sono altro che referenti svalutati, indeboliti dalla conoscenza scientifica e dalla coesistenza di molteplici messaggi spirituali o ideologici che cerchiamo di tollerare, ma la cui molteplicità è sufficiente a ridurre a zero la speranza che uno solo di essi possa contenere una verità assoluta.

Nel corso della storia, i rituali sono apparsi sistematicamente prima dei sistemi morali. La sincronizzazione e la mimica dei rituali collettivi rendono gli esseri umani più sensibili ai sentimenti, ai desideri e alle emozioni dei loro simili. Questa capacità di mettersi nei panni degli altri si chiama empatia cognitiva e ci permette di "metterci nei panni degli altri", di sentire quello che sentono e di pensare quello che pensano. Questa capacità è decuplicata dall'imitazione: i partner sincronizzati provano compassione l'uno per l'altro.

I rituali calmano la nostra corteccia cingolata perché ci permettono di prevedere in modo più affidabile ciò che i nostri simili faranno o non faranno per il semplice fatto che possiamo già prevedere i loro movimenti durante il rituale.

Ma è più difficile prevedere i pensieri delle persone che i loro movimenti. A tal fine, non si tratta più di condividere semplicemente i gesti, ma di condividere le rappresentazioni mentali. In altre parole, valori e visioni del mondo.

Ci possono essere diverse visioni del mondo, alcune delle quali sono dette normative, nel senso che prescrivono determinati comportamenti, e altre che sono solo positive (o fattuali), e viceversa non prescrivono alcun comportamento. Quando questa visione prescrive ciò che è bene fare o meno, e le persone vi aderiscono e vedono il mondo nello stesso modo in cui lo vediamo noi, questo riduce notevolmente l'incertezza e di fatto l'attività della corteccia cingolata.

Ma la loro violazione, invece, la attiva fortemente (così come altre strutture cerebrali). Per la nostra corteccia cingolata anteriore, il mancato rispetto delle norme morali è un errore fondamentale di previsione. Questo è di per sé un potente fattore calmante per la corteccia cingolata, che cerca di orientarsi tra i suoi simili.

Non appena si è certi che gli altri credono nei nostri stessi sacri valori morali, si può iniziare a prevedere con maggiore affidabilità ciò che probabilmente faranno o non faranno.

Nel termine religione, il verbo latino religere significa legare insieme, quindi capiamo che la religione serve principalmente a legare gli individui.

Con il passare del tempo, l'umanità ha perso la sua attenzione sociale e morale a favore delle previsioni sul mondo materiale. Come disse il famoso filosofo Friedrich Nietzsche, "Dio è morto, tutto è permesso".

Per la nostra corteccia cingolata, questo è l'inizio dell'angoscia. Poiché il significato fornito dalla scienza offre solo una visione positiva (o fattuale), non ha lo stesso potere rassicurante di quello fornito dalla religione e dalla morale.

La scienza introduce un significato fattuale, ma non, almeno inizialmente, uno morale.

Certo, il significato fattuale ottenuto dalla scienza è colossale. Oggi gli esseri umani sanno in una certa misura come si è formata la Terra e come sono arrivati su di essa, e possono ripercorrere con una certa precisione la sequenza di eventi che ha portato all'evoluzione delle specie dai batteri ai primi vertebrati, ai mammiferi, alle scimmie e all'Homo Sapiens. La domanda sulla causa della sua esistenza, sulla sua natura e sulle sue caratteristiche fisiche e mentali ha trovato risposta. Allo stesso modo, la conoscenza delle leggi della fisica e degli organismi viventi ci permette di prevedere un numero impressionante di fatti concreti, come il tempo atmosferico, le eclissi di luna e di sole, la potenza di un motore a combustione in funzione del grado di raffinatezza della benzina utilizzata e della cilindrata del motore, ecc.

Ma non sappiamo più cosa abbia senso in termini umani ed esistenziali. Perché se è vero che abbiamo eliminato il senso, non abbiamo eliminato il bisogno di senso. La parte del nostro cervello che ha impiegato centinaia di migliaia di anni per conformarsi al fine di trovare un significato nel mondo e nelle strutture sociali è ancora molto presente. [^36]

Nella nostra società stiamo vivendo il mito di Sisifo, un personaggio della mitologia greca condannato a far rotolare eternamente un masso fino alla cima di una collina e a ridiscenderlo ogni volta prima di raggiungere la cima. [Nella nostra società, il modo di produrre non ha più senso. Siamo ormai in un mondo usa e getta. E quando nulla dura, la vita diventa uno spreco. Tutti noi affrontiamo questo problema in misura maggiore o minore.

Inoltre, con il cambiamento climatico, nulla è più stabile, né il ciclo delle stagioni, né il livello dei mari, né il verificarsi di disastri naturali. Il mondo è ormai in movimento e questo movimento supererà le nostre capacità di indebitamento.

La parola è dura, ma l'incertezza uccide, mina i nostri cervelli, distrugge la fondamentale aspirazione umana al significato.

## Le nostre reazioni all'incertezza

La sensazione di indeterminatezza attiva la corteccia cingolata, che constata dolorosamente l'impossibilità di fare previsioni sul futuro, di definire se stessi e di vedere un percorso di vita chiaro.

L'eco-ansia, ad esempio, è una nuova forma di ansia derivante dalla consapevolezza paralizzante della distruzione del nostro pianeta. [^38]

In un mondo in cui esiste un livello massimo di incertezza, la corteccia cingolata riuscirà a ripristinare la certezza su scale più piccole, poiché non può farlo sulla scala più globale della sua esistenza. Questa è la cosiddetta microcertezza.

Le droghe che influenzano i nostri neuroni possono essere un sostituto del significato. La cocaina, ad esempio, distrugge l'incertezza.

L'autostima è l'invenzione egocentrica di un mondo individualista e un modo per compensare la perdita di significato.

Il denaro è il passaporto definitivo per la libertà dall'angoscia dell'incertezza.

Ma nel gioco della competizione, le disuguaglianze aumentano. I perdenti in questa gara, disumanizzati dalla recisione del cordone ombelicale, nel peggiore dei casi si rivolgeranno a una forma degradata di umanità. Questo è il tipo di umanità che nega l'umanità degli altri per garantire la propria esclusività. Stiamo parlando di identità.

Perché da questa tensione nasce automaticamente un bisogno urgente: sapere una volta per tutte chi si è. E il modo più semplice per chiarire la propria identità è definirsi in riferimento a un gruppo a cui si appartiene. Definirsi attraverso il rapporto con un gruppo soddisfa un bisogno fondamentale: trovare un modo per conformarsi alle regole vigenti, che limiti le reazioni intempestive della corteccia cingolata.

Infatti, una delle principali fonti di significato regolarmente citate nei sondaggi è il senso di appartenenza a un gruppo sociale[^40].

Le disuguaglianze spingono le società a ritirarsi nell'identità. Dove l'unica cosa che conta è la capacità di utilizzare la macchina produttiva per i propri fini. Il modello economico neoliberista, basato sulla competizione, sulla mobilità degli individui, sull'accelerazione dei ritmi di lavoro e sulla riduzione della spesa pubblica destinata alla protezione e alla distribuzione equa della ricchezza, provoca un impulso autoritario e identitario volto a placare le reazioni individuali all'incertezza e alla mancanza di senso di appartenenza sociale.

L'anomia si riferisce alla sensazione di perdita di ordine e logica nella società.

Le persone che più spesso hanno pensieri nostalgici sono anche quelle che hanno la più chiara impressione che l'esistenza abbia un senso[^41].

Il nostro cervello è estremamente bravo a negare.

Quando le nostre azioni non sono in linea con le nostre rappresentazioni mentali, si crea dissonanza cognitiva. Anche la contraddizione attiva la dissonanza cognitiva.

Questo perché il cervello umano cerca la coerenza e la ragione in ogni circostanza. Cercherà quindi di risolvere la dissonanza che rappresenta l'incertezza e per farlo modificherà le sue azioni in modo che siano in linea con il suo pensiero, oppure modificherà il suo pensiero in modo che sia in fase con le sue azioni.

Le ricerche sulla dissonanza cognitiva dimostrano che il più delle volte sono i pensieri ad adattarsi alle azioni e non il contrario. [^42][^43]

Perdiamo il contatto con la realtà perché non sappiamo più come interpretarla. Il significato ci protegge perché ci suggerisce modi di interpretare il mondo e di agire più efficacemente per dominarlo e proteggerci dalle minacce che contiene.

Con l'avvicinarsi dello spettro delle grandi catastrofi, il ritiro delle comunità non potrà che accentuarsi, così come i comportamenti compensativi di iperconsumo, inflazione dell'ego e negazione.

La mancanza di sistemi di significato ci spinge a consumare beni materiali, soprattutto in situazioni di grande incertezza, accelerazione e competizione. L'ipermaterialismo è il palliativo all'incertezza e alla competizione[^36].

## Soluzioni alla mancanza di significato

Il cervello cerca di prevedere il mondo per controllarlo meglio, e lo fa o attraverso il significato o attraverso la tecnologia. Quando la tecnologia progredisce, non ha più bisogno di significato. E quando la tecnologia fallisce, ha bisogno del significato.

Oggi abbiamo la tecnologia, ma è fallita, perché firma la nostra estinzione. [^44]

In vista del disastro che si sta verificando e che si verificherà, dobbiamo fermare al più presto la macchina del consumo che l'umanità è diventata, cercando attivamente di riparare la Terra.

Se più il significato diminuisce più il consumo aumenta, viceversa più il significato aumenta più il consumo diminuisce.

Abbiamo 8 miliardi di cortecce cingolate impegnate a ingannare la loro paura della morte e dell'imprevedibile, ora dobbiamo tenerle impegnate a ingannare questa paura unendole intorno a un significato condiviso[^36].

Questo significato deve riunire miliardi di persone provenienti da contesti culturali diversi, con patrimoni civili sparsi e background intellettuali eterogenei, per far loro dimenticare l'ossessione del possesso e dello sfruttamento.

Ci sono due significati: quello cosmico e quello sociale.

Il primo si basa sulla comprensione delle leggi della natura, della materia e dell'universo. Il secondo riguarda la nostra capacità di agire, in un contesto sociale, in conformità con ciò che riteniamo giusto e sbagliato.

Esse creano un sistema morale, definendo un bene e un male che regolano l'azione sociale e individuale.

Il sacro è il perno da cui si costruisce il significato nelle assemblee umane. Oggi l'umanità moderna ha perso il sacro decostruendo la realtà e scoprendo che tutto, dal movimento degli atomi in una stella fusa al flusso di ioni nella membrana dei vostri neuroni mentre leggete queste righe, obbedisce a leggi meccanicistiche in cui il bene e il male non trovano posto.

La conseguenza di questa demoralizzazione del mondo è che ogni individuo sulla superficie della Terra può decidere cosa trovare buono o cattivo.

Ma nessun senso collettivo può esistere senza la nozione di sacro. La soluzione è ricreare il sacro, e questo sacro può essere la nostra Terra.

Il sacro può manifestarsi solo attraverso il rituale e l'umanità dovrà inventare nuovi rituali per uscire dall'impasse in cui si trova. Come abbiamo visto, le pratiche sincrone aiutano a calmare la corteccia cingolata. Tali rituali dovranno rendere sacra la conservazione del pianeta.

Procedendo in questo modo, gli esseri umani possono dotarsi di mezzi efficaci per ridurre il carico mentale di ansia legato all'individualismo. Sapere che i cittadini del mio Paese, dell'intero continente, e se possibile dell'intero pianeta, ritengono sacro ciò che io stesso ritengo un valore insuperabile, crea le basi per un nuovo patto di appartenenza e di fiducia.

L'uomo è un cooperatore condizionato; è capace di immensi sacrifici purché abbia la certezza che gli altri membri della sua comunità faranno lo stesso. [^45]

Rendendo la purezza del carbonio e la conservazione dell'equilibrio biologico e geologico valori sacri, i miliardi di esseri umani possono riportare l'attività delle loro cortecce cingolate a livelli accettabili e cessare di dipendere da beni materiali, droghe, denaro e sostituti dell'ego per sostenere la propria esistenza.

L'ecologia può offrire una visione del mondo comune a tutti gli esseri umani.

Con la conoscenza, l'elenco degli atti compatibili con il valore sacro è una questione di scienza, di misurazioni e calcoli effettuati dal collettivo umano.

La sfida è creare un senso di identità globale che dia a tutti la sensazione di essere accettati in un gruppo, senza la necessità di dimostrare il proprio valore e la propria idoneità accumulando beni materiali o sostegni dell'ego.

Oggi dobbiamo accettare che la scienza può dirci non solo cosa è vero e cosa è falso, ma anche cosa è giusto e cosa è sbagliato.

Il terzo significato che verrà sarà quindi ecologico.

Per svilupparlo, dobbiamo sviluppare la conoscenza attraverso l'educazione, trasmettere la conoscenza dei sistemi viventi, della biodiversità, della climatologia, dell'ecologia e dell'evoluzione delle specie.

Questo creerà un senso di meraviglia, perché la meraviglia è un potente antidoto all'angoscia esistenziale che ci attanaglia, attraverso la scoperta dell'infinita ricchezza degli esseri viventi, della bellezza dei paesaggi, della flora e della fauna, perché in essi troviamo una fonte di significato.

Abitando tutti la stessa Terra, agendo su di essa e subendo le conseguenze del suo degrado, siamo tutti portati a riconoscere lo stesso valore sacro.

In questo modo, i cittadini potranno impegnarsi in azioni coerenti con i loro valori e le loro convinzioni e sperimenteranno un senso di significato.

Diventare protagonisti di questo cambiamento è già di per sé una fonte di significato e di allineamento tra le nostre opinioni e le nostre azioni.

Dobbiamo rivedere le fondamenta stesse della nostra civiltà. La logica della produzione e del consumo "just-in-time" che sta alla base delle nostre economie è insostenibile. Nel prossimo capitolo scoprirete le nostre proposte in termini di "economia".

Nel profondo abbiamo tutti un'immensa aspirazione, siamo alla ricerca di un senso e non sappiamo come realizzarlo. L'obiettivo di questo lavoro è soddisfare questo desiderio senza nemmeno aspettare che diventi realtà, perché è già presente all'inizio.

## Bisogno di finzione - Il cervello narratore

Il cervello umano, quella straordinaria macchina narrativa, è il cuore della nostra capacità di creare storie, nel bene e nel male. Crea le storie che ci permettono di giustificare le nostre azioni e di dar loro un senso. Questa capacità narrativa non è un caso, ma il prodotto di un'evoluzione complessa, che rivela la profondità del nostro bisogno di significato e di ordine in un mondo caotico. [^46]

Quindi, ciò che rende speciale la nostra specie non è, ad esempio, il fatto che faccia la guerra dall'alba dei tempi - gli scimpanzé e le formiche lo fanno altrettanto - ma che ne abbia fatto un'intera storia... e milioni di storie.

Gli altri animali non fanno il male per amore del male, né fanno altro per amore del bene; agiscono principalmente sulla spinta di bisogni immediati o istinti. Gli esseri umani, invece, cercano un significato nelle loro azioni e in quelle degli altri, spesso attraverso il prisma della morale, del bene e del male. Questa ricerca di significato è quindi una manifestazione delle nostre funzioni cerebrali superiori, che ci spingono a cercare spiegazioni e a giustificare il nostro comportamento in un contesto più ampio.

Quello che consideriamo il "sé" non è un'entità isolata o autonoma, ma piuttosto un mosaico di tratti, credenze e valori presi in prestito, condivisi e talvolta contestati da chi ci circonda. Ogni interazione, ogni scambio culturale o sociale contribuisce a plasmare la nostra identità, rendendola dinamica e in evoluzione. Il nostro io è intessuto dagli altri. Ci definiamo nello specchio delle nostre relazioni, ogni persona è un riflesso che ci aiuta a formare la nostra immagine[^49].

Questo processo di costruzione narrativa risuona con gli insegnamenti buddisti sull'illusione del sé. Secondo il buddismo, il "sé" che percepiamo come un'entità stabile e continua è un'illusione, una costruzione della mente. [Torneremo su questo punto in modo più approfondito nella sezione Religione della seconda parte. Allo stesso modo, le neuroscienze rivelano che la nostra identità è una storia che raccontiamo a noi stessi, una sequenza coerente fabbricata ex novo dal nostro cervello per mantenere un'illusione di continuità[^48].

L'identificazione con le nostre storie e con il nostro "io" costruito può bloccarci in schemi di pensiero limitanti. Riconoscere questa costruzione narrativa come un'illusione può essere liberatorio. Ci invita a mettere in discussione le narrazioni tradizionali e ad aprirci a nuove possibilità di percepire e vivere il mondo.

L'interazione tra "vita reale" e finzione assume una nuova dimensione. Piuttosto che vedere queste sfere come distinte, possiamo capire che sono intrinsecamente collegate, alimentandosi a vicenda. Le nostre narrazioni personali e collettive non sono semplici distrazioni o evasioni, ma potenti strumenti di trasformazione e comprensione.

Riconoscere la natura narrativa del nostro cervello e riconoscere l'illusione del sé non significa rifiutare la nostra identità o le nostre storie, ma vederle per quello che sono: costrutti flessibili, modificabili e profondamente umani. Ci incoraggia a smettere di seguire le narrazioni tradizionali e a dire a noi stessi "il mondo è sempre stato così e sarà sempre così", ma a capire e a dire a noi stessi "vediamo solo ciò che vediamo".

Ci offre un percorso verso una comprensione più ricca e sfumata di noi stessi e del mondo, senza più aggrapparci alla nostra identità come realtà inamovibile e identificarci esclusivamente con essa, ma lasciandoci intravedere nella nostra vita la possibilità di ripensare il mondo e noi stessi.

## Effetti e pregiudizi cognitivi

In questa sede passeremo in rassegna i diversi effetti e bias del nostro cervello, in quanto possono influenzare la nostra percezione, la memoria, i giudizi, le decisioni e il comportamento in una varietà di modi, influenzando il modo in cui elaboriamo e interagiamo con le informazioni. Questa comprensione di noi stessi ci permetterà di sviluppare strumenti efficaci per la nostra società, in particolare per modellare il nostro processo decisionale ed evitare le conseguenze di questi pregiudizi che ci impedirebbero di raggiungere il nostro obiettivo comune.

Una lettura dettagliata degli effetti e dei pregiudizi cognitivi, benché consigliabile, non è affatto necessaria e può essere saltata per comodità di lettura. Sono stati riassunti nella sezione seguente.

### Sintesi degli effetti e dei pregiudizi cognitivi

Ecco un elenco non esaustivo dei bias cognitivi:

- Bias di selezione: siamo più propensi a selezionare le informazioni che si adattano alle nostre convinzioni e opinioni preconcette, piuttosto che considerare una gamma più ampia di informazioni. [^50]

- Illusione della memoria: tendiamo a sopravvalutare l'affidabilità dei nostri ricordi e a colorarli con la nostra percezione attuale[^51].

- Razionalizzazione delle credenze: tendiamo a giustificare le nostre decisioni e i nostri comportamenti, anche se sbagliati o dannosi, interpretandoli in modo favorevole a noi stessi[^53].

- Pregiudizio di sopravvalutazione: tendiamo a sopravvalutare la nostra capacità di prevedere gli eventi futuri e di controllare le situazioni[^52].

- Pregiudizio dell'ottimismo: tendiamo a sovrastimare la probabilità di benefici futuri e a sottovalutare i rischi potenziali. [^54]

- Bias di riduzione della complessità: tendiamo a semplificare la complessità delle situazioni per renderle più gestibili[^55].

- Pregiudizio di inferenza causale: tendiamo ad attribuire una causa a un evento anche quando ciò non è necessariamente giustificato o ragionevole[^56].

- Pregiudizio di riduzione dell'effetto: tendiamo a sovrastimare gli effetti positivi di un'azione e a sottovalutare quelli negativi[^57].

- Pregiudizio da effetto primacy: siamo influenzati dalle prime informazioni che riceviamo e tendiamo a soppesarle più pesantemente di quelle successive.[^58]

- Pregiudizio dell'effetto somiglianza: è più probabile che ci piacciano e ci fidiamo di persone simili a noi[^59].
- Euristica della disponibilità: spesso usiamo una stima rapida e intuitiva per valutare la probabilità o la frequenza di un evento, basandoci sulle informazioni più facilmente disponibili nella nostra memoria[^60].

- Pressione dei pari: siamo influenzati dalla percezione di ciò che gli altri pensano di noi e siamo spinti a conformarci alle loro opinioni e comportamenti[^61].

- Halo bias: tendiamo a generalizzare un'opinione positiva o negativa di una persona sulla base di una singola caratteristica.[^62]

### Spiegazione approfondita degli effetti e dei bias cognitivi

- Pregiudizio di conferma

Il bias di conferma è un bias cognitivo che si verifica quando favoriamo le informazioni che supportano le nostre opinioni e convinzioni. Poiché siamo alla ricerca di un significato, selezioniamo le informazioni che vanno nella nostra direzione. La soluzione a questo problema è essere il nostro stesso contraddittore, mettere alla prova i nostri pensieri e cercare di dimostrare che sono sbagliati.

Le contraddizioni nei nostri giudizi non devono quindi ferirci, ma risvegliarci e spronarci all'azione. Non ci piace la correzione delle nostre opinioni, ma al contrario dobbiamo essere aperti e offrirci ad essa, e che avvenga sotto forma di conversazione e non di lezione.

Non dobbiamo, a torto o a ragione, cercare modi per sbarazzarci di ogni opposizione, ma piuttosto guardare in profondità per vedere se può essere giusta.

- Pregiudizio della disponibilità

Il pregiudizio di disponibilità è un altro pregiudizio cognitivo che può influenzare il processo decisionale. Si verifica quando stimiamo la probabilità di un evento o la frequenza del suo verificarsi sulla base di esempi che ci vengono rapidamente in mente. Questo bias può portarci a sottostimare la probabilità di eventi rari e a sovrastimare la probabilità di eventi comuni o recenti. Sovrastimiamo il nostro ragionamento privilegiando le informazioni direttamente accessibili alla nostra memoria.

Per evitare questo pregiudizio, è importante prendere in considerazione un campione rappresentativo di informazioni e cercare di essere obiettivi nella valutazione delle probabilità. È anche importante non basarsi solo sulle informazioni immediatamente disponibili, ma cercare altre informazioni che possano darci una visione più completa ed equilibrata.

- Effetto alone

L'effetto alone è il meccanismo che ci porta ad attribuire qualità a qualcuno in base al suo aspetto fisico.

Ad esempio, un sito di incontri chiamato cupidexe ha condotto un esperimento in cui i profili delle persone sono stati valutati in base alla personalità e all'aspetto. Chi ha un basso punteggio fisico ha un basso punteggio di personalità e viceversa un alto punteggio fisico significa una buona personalità. Si potrebbe pensare che, dato che sembrano avere una buona personalità, abbiano anche un buon fisico, ma nel top 1% alcuni profili di personalità sono vuoti...
Per verificarlo è stato condotto un altro esperimento in cui gli utenti si sono imbattuti in profili con o senza testo e, a prescindere dalla presenza o meno di testo, è stato assegnato lo stesso punteggio.

Esistono altri studi e statistiche che confermano questo effetto. Ad esempio, tutti i presidenti americani sono alti e più si è alti, più si guadagna.

- Effetto Barnum

L'effetto Barnum si riferisce alla tendenza delle persone a credere che commenti vaghi o generici siano descrizioni accurate della loro personalità o della loro vita. Questo fenomeno prende il nome dal famoso showman P.T. Barnum, che utilizzava letture vaghe della personalità per attirare le persone ai suoi spettacoli.

Per esempio, un test della personalità che afferma "sei una persona premurosa e affettuosa nei confronti della famiglia e degli amici" è abbastanza vago da essere considerato vero per la maggior parte delle persone. Tuttavia, non fornisce dettagli specifici sulla persona, il che può portare a un sentimento di identificazione con la descrizione generale, anche se non è molto personale.

L'effetto Barnum può essere rafforzato dal bisogno di riconoscimento e dalla sete di autodeterminazione delle persone. Le persone amano sentirsi comprese e valorizzate, il che può portarle ad accettare le descrizioni generali come vere per loro.

È importante tenere conto dell'effetto Barnum quando si analizzano i test di personalità o i commenti sulla propria vita o personalità. È sempre meglio cercare informazioni più specifiche e dettagliate per ottenere una comprensione più accurata di se stessi.

- Effetto ancoraggio

L'effetto ancoraggio si riferisce alla tendenza delle persone a basare le proprie stime o decisioni su un'informazione iniziale, chiamata "ancora". Questa prima informazione può avere un grande impatto sulle stime successive, anche se è irrilevante o addirittura sbagliata.

Ad esempio, se vi viene chiesto di indovinare il numero di persone che vivono nella vostra città e vi viene data una cifra alta come prima informazione, tenderete a fare una stima più alta rispetto a quella che avreste fatto se vi fosse stata data una cifra più bassa. Allo stesso modo, se si sta negoziando il prezzo di un oggetto, la prima proposta sarà spesso considerata l'ancora e influenzerà le proposte successive.

L'effetto di ancoraggio può essere utilizzato consapevolmente o inconsapevolmente in situazioni di negoziazione, vendita o persuasione, e può portare a decisioni poco razionali e distorte.

- Pressione dei pari

La pressione dei pari è un fenomeno sociale che descrive la tendenza degli individui a conformarsi alle opinioni, agli atteggiamenti e ai comportamenti di un gruppo a cui appartengono o con cui si sentono affiliati. Può verificarsi quando gli individui cercano di conformarsi alle norme sociali e di evitare la disapprovazione o l'isolamento dal gruppo.

Ad esempio, quando una persona si trova in un gruppo di persone che hanno una particolare opinione o atteggiamento, può essere influenzata da queste opinioni e atteggiamenti, anche se non corrispondono alle proprie opinioni o atteggiamenti. Questo può essere particolarmente vero in situazioni in cui la persona vuole essere accettata dal gruppo ed evitare la disapprovazione o l'esclusione.

La pressione di gruppo può anche verificarsi quando una persona si sente obbligata a conformarsi al comportamento di un gruppo per evitare di sentirsi diversa o a disagio. Ad esempio, chi fa parte di un gruppo che beve alcolici può essere influenzato a bere alcolici a sua volta, anche se non lo vuole.

È importante essere consapevoli della pressione del gruppo e non cedere alle opinioni o al comportamento di un gruppo solo per evitare la disapprovazione o per conformarsi alle norme sociali. È sempre meglio seguire le proprie opinioni e convinzioni piuttosto che cedere alle pressioni del gruppo.

- Euristica della disponibilità

L'euristica della disponibilità è una scorciatoia mentale che spesso utilizziamo per valutare la frequenza o la probabilità di un evento basandoci sugli esempi più recenti e prontamente disponibili nella nostra memoria. Questo può portare a errori di valutazione, poiché gli esempi che abbiamo in mente possono essere distorti.

Ad esempio, se una persona ha appena sentito parlare di un incidente aereo in televisione, potrebbe sentirsi più preoccupata per la sua sicurezza sul prossimo volo, poiché l'evento è fresco nella sua memoria e prontamente disponibile. Tuttavia, questa persona potrebbe sottovalutare la sicurezza dell'aviazione in generale, poiché gli incidenti aerei sono in realtà molto rari.

È importante ricordare che l'euristica della disponibilità può portare a errori di valutazione e cercare informazioni più complete e affidabili per valutare la frequenza o la probabilità di un evento. È anche utile ricordare che i media possono influenzare la nostra percezione degli eventi, dando risalto a certe storie piuttosto che ad altre. Questo può portare a una sovrarappresentazione di eventi rari o drammatici e dare una falsa impressione della loro reale frequenza. È quindi importante ricordare di non lasciarsi influenzare solo da ciò che si vede o si sente nei media.

In conclusione, l'euristica della disponibilità può essere utile per prendere decisioni rapide nelle situazioni quotidiane, ma è importante considerarla con cautela quando si tratta di esprimere giudizi più importanti o di prendere decisioni con conseguenze a lungo termine. È preferibile cercare informazioni complete e affidabili quando si valutano probabilità e rischi, piuttosto che affidarsi esclusivamente a ciò che è prontamente disponibile nella nostra memoria.

## Conclusione

Questa sezione sull'Homo Sapiens ha già aperto una serie di strade. Ne svilupperemo alcune in questa conclusione, ma l'elenco non è esaustivo. Soprattutto, ne svilupperemo molte altre nei capitoli successivi, partendo dalle basi sviluppate qui.

Ora conosciamo i bisogni umani che devono essere soddisfatti se vogliamo ottenere una realizzazione totale. Abbiamo visto che è possibile trasformare i nostri bisogni estrinseci e che sarà necessario farlo per renderli compatibili con un progetto comune che permetta a tutti noi di soddisfare il nostro bisogno di significato.

Questo significato collettivo deve esistere attraverso la nozione di sacro. E non c'è niente di più sacro di ciò che abbiamo in comune e la cui sopravvivenza è direttamente legata a noi, cioè la nostra terra e i suoi ecosistemi. Così la scienza e l'ecologia non sono più solo in grado di dirci cosa è vero o falso, ma sono anche in grado di dettare i nostri valori, cosa è giusto o sbagliato.

Abbiamo visto che non c'è natura umana che ci impedisca di riuscire in questo progetto, che non c'è un io inamovibile, e in questo compito lo sviluppo della nostra coscienza sarà il nostro migliore alleato. Altrettanto importante sarà l'educazione delle nuove generazioni, incoraggiandole ad essere altruiste e a meravigliarsi della natura per proteggerla.

Attraverso lo studio dei diversi effetti e pregiudizi cognitivi, abbiamo raccolto sempre più informazioni necessarie per comprendere noi stessi e quindi anche per sviluppare strumenti che tengano conto di questi parametri per renderli efficaci nell'aiutarci nel nostro progetto comune.

## Fonti (elenco non esaustivo)

[^1]: [Rapporto IPCC](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia : Piramide dei bisogni](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Prospettive della scienza psicologica
[^4]: [Organizzazione delle Nazioni Unite per l'alimentazione e l'agricoltura (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Cifre e analisi sulla povertà abitativa in Francia, compreso il numero di senzatetto e le condizioni di vita precarie](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Rinuncia all'assistenza sanitaria per motivi economici nell'agglomerato di Parigi:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [La Fondation de France pubblica la 13a edizione del suo studio sulla solitudine](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap International: i disabili restano tra i più esclusi al mondo](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivazione e personalità, 1970 (seconda edizione)
[^10]: Arendt Hannah, La condizione dell'uomo moderno, 1961
[^11]: Valérie Jousseaume, Plouc Pride: una nuova narrazione per la campagna
[^12]: Emmanuel Todd, L'origine dei sistemi familiari, 2011 - A che punto siamo? Uno schizzo di storia umana, 2017 - La lotta di classe in Francia nel XXI secolo, 2020.
[^13]: Robert J. Vallerand, Verso un modello gerarchico di motivazione intrinseca ed estrinseca, 1997.
[^14]: Jean Baudrillard, La società dei consumi, 1970
[^15]: Robert Wright, L'animale morale: perché siamo come siamo, 1994
[^16]: Charles Darwin, L'origine delle specie: capitolo IV / III, 1859 e L'ascendenza dell'uomo e la selezione sessuale: capitolo VII, 1871.
[^17]: Hannah Arendt, Le origini del totalitarismo, 1951
[^18]: Albert Camus, La peste, 1947
[^19]: Jean-Paul Sartre, L'esistenzialismo è un umanesimo
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, Homo Sociabilis: Reciprocità
[^21]: [RIM Dunbar, L'ipotesi del cervello sociale e le sue implicazioni per l'evoluzione sociale](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, L'insetto umano
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, Il cervello primordiale
[^26]: Joseph Henrich, Il ruolo della cultura nel modellare il comportamento legato alla ricompensa
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, Altruismo: un enigma?
[^29]: Matthieu Ricard, "Altruismo: il gene o l'educazione?".
[^30]: Kathryn Spink, "Madre Teresa: una vita al servizio degli altri".
[^31]: Wolfram Schultz, Il ruolo della dopamina nell'apprendimento e nella memoria, 2007
[^32]: Read, D., & Northoff, G, Correlati neurali delle decisioni impulsive e riflessive. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, Il cervello e il piacere
[^34]: David Eagleman, Il cervello inconscio
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 e Gloria Y. Yeh,3 Una revisione sistematica e una meta-analisi degli effetti della meditazione sull'empatia, la compassione e i comportamenti prosociali, 2018
[^36]: Sébastien Bohler, Dov'è il significato?, 2020
[^37]: [Roy F. Baumeister, Il bisogno di significato: una prospettiva psicologica](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Eco-ansietà: analisi di un'angoscia contemporanea
[^39]: Albert Camus, Il mito di Sisifo, 1942
[^40]: [Sondaggio "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides and Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, L'uomo irrazionale 2001
[^43]: Elliot Aronson, La teoria della dissonanza cognitiva
[^44]: Elizabeth Kolbert, La sesta estinzione, 2015
[^45]: Robert Axelrod, Il dilemma del prigioniero
[^46]: Nancy Huston, La specie favolistica
[^47]: Serge-Chritophe Kolm, Felicità e libertà
[^48]: David M. Eagleman, Incognito, 2015
[^49]: William James, I principi della psicologia, 1980
[^50]: [Wikipedia: Bias di selezione](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, L'illusione della memoria
[^52]: David Dunning e Justin Kruger, Unskilled and unaware of it: How difficulties in recognising one's own incompetence lead to inflated self-assessments, 1997
[^53]: [Festinger e Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones e Victor H. Harris, "L'attribuzione degli atteggiamenti, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, Pensare velocemente e lentamente, 2011
[^58]: [Asch, S. E., Forming impressions of personality, 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. Il paradigma dell'attrazione, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinioni e pressione sociale, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. Un errore costante nelle valutazioni psicologiche, 1920
