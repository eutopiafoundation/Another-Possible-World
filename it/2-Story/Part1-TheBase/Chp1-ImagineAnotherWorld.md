---
title: Immaginare un altro mondo
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Come si fa a immaginare un nuovo mondo, un ideale, un'Eutopia?

## Il sistema, i suoi vincoli e incentivi

Se attualmente sembriamo in una situazione di stallo di fronte al cambiamento climatico e alle altre sfide del nostro tempo, è perché le fondamenta del nostro sistema generano vincoli e incentivi che finiscono per impedire qualsiasi cambiamento efficace senza interrompere la coerenza stabilita, che poi trascina la nostra società in un'inerzia distruttiva che nessuno sembra in grado di fermare.

A ogni elezione, i nostri politici cercano di convincerci delle loro proposte, e in apparenza tutti pensano di fare la cosa giusta, di avere la soluzione che potrebbe migliorare la vita quotidiana complessiva delle persone che difendono. Ma stanno combattendo tra di loro con questi vincoli e incentivi, che non possono portare a una soluzione duratura e auspicabile. Quindi, impotenti di fronte al sistema, non hanno altra scelta che lasciarsi governare da esso.

Nella misura in cui l'abbiamo mai avuto, noi esseri umani abbiamo perso il nostro potere, ma peggio ancora lo hanno perso coloro che eleggiamo a rappresentarci.

Pensiamo che il sistema politico domini e che possa cambiare tutto il resto, compresa l'economia e la sua forma. Che siamo in una democrazia grazie alla competizione tra i candidati che votiamo.

Ma un sistema economico non si limita a produrre beni e servizi, ma definisce le condizioni della nostra socialità, produce esseri umani e le relazioni tra di essi. [^1]

Vedremo che l'economia interviene persino in questa scelta democratica competitiva. Che la influenza e che, di conseguenza, il sistema elettorale non ha più la qualità di offrire una buona competizione tra i candidati. Alla fine, quindi, non è la politica a dominare l'economia, ma l'economia a dominare la politica. [^2]

Quello che proponiamo qui è un colpo di spugna sul nostro sistema, un allontanamento completo da tutto ciò che costituisce il nostro attuale immaginario collettivo. Vi invitiamo a partire da una nuova base, a scoprire un Mondo Nuovo molto diverso da quello che conoscete, un mondo basato sul senso e sulla logica, e poi a elaborare insieme i percorsi e le soluzioni per una transizione verso di esso.

## Partire da zero

Qual è la base?

La base è la realtà, è ciò che esisteva realmente all'inizio dell'esistenza dell'umanità, quando non potevamo ancora raccontarci storie immaginarie. La base è tutto ciò che rimane una volta cancellata l'immaginazione umana collettiva.

Abbiamo quindi la Terra e tutto ciò che la compone. In particolare, i suoi diversi ecosistemi: un ecosistema è un insieme formato da una comunità di esseri viventi che interagiscono con il loro ambiente, e questi esseri viventi possono essere piante o animali. Il pianeta è un grande sistema in cui un numero infinito di sottosistemi interagisce e si equilibra dinamicamente. Tra tutti gli esseri viventi sul nostro pianeta, ci siamo noi, l'Homo Sapiens, più comunemente conosciuto come Uomo.

Gli esseri umani sono 7,9 miliardi e si prevede che saranno 11 miliardi entro il 2100[^3]. Siamo ormai quasi ovunque sul nostro pianeta e, anche nelle aree scarsamente popolate, gli esseri umani hanno ancora un impatto sull'ambiente e sugli ecosistemi[^4]. Tuttavia, il problema non è la dimensione della popolazione, ma le interazioni che abbiamo all'interno dei suoi ecosistemi.

Quindi, partire dalla base significa innanzitutto capire la base. Dobbiamo capire il mondo e come funziona. E poiché noi esseri umani viviamo al suo interno, dobbiamo anche capire come funzioniamo. Una parte sarà dedicata esclusivamente alla terra e un'altra sarà dedicata alla comprensione approfondita dell'essere umano.

Queste due domande di ricerca sono molto ampie e richiedono una grande quantità di conoscenze, ma non è necessario entrare nei dettagli: ci concentreremo sull'essenziale, lo raccoglieremo e lo riassumeremo per rispondere alle domande che seguono.

## Domande di ricerca

Una volta acquisite le competenze necessarie per comprendere il mondo e gli esseri umani, avremo bisogno di un obiettivo sotto forma di domanda di ricerca che stabilisca il nostro desiderio su ciò che stiamo cercando in un mondo ideale.

Questa domanda di ricerca è molto importante, perché guiderà la ricerca da cui scaturiranno le nostre soluzioni. Domande di ricerca diverse porteranno a risultati diversi. Le domande devono essere il più possibile semplici, chiare e precise, ma devono cogliere l'essenza di ciò che vogliamo.

Ci concentreremo sulla nostra specie, ma, come già detto, viviamo in ecosistemi e, studiando il mondo, scopriremo che siamo direttamente legati ad essi e che quindi dipendiamo da essi per la nostra sopravvivenza.

Cerchiamo l'ideale. Naturalmente, l'ideale dipende dai valori, dalle credenze, dalle aspirazioni e dalle esperienze di ciascun individuo. Potremmo quindi sperare che l'ideale per ogni individuo sia vivere una vita appagante ed equilibrata che soddisfi i suoi bisogni e le sue aspirazioni.

È questo il nostro obiettivo: che ogni essere umano abbia una vita soddisfacente durante il suo periodo di permanenza sulla Terra. Ma ricordiamo che ci sono ancora 80 milioni di generazioni a venire, quindi questa ricerca si applica alle generazioni attuali, ma tiene conto anche di tutte quelle future.

Tenendo conto di questi diversi elementi, ecco la formulazione della nostra domanda di ricerca:

> Come possiamo permettere a tutti gli esseri umani attuali e agli 80 milioni di generazioni a venire di godere di una vita soddisfacente sulla Terra?
> {.is-success}

Il nostro grande progetto collettivo è quindi quello di garantire a tutti gli esseri umani una vita appagante sulla Terra, che soddisfi i loro bisogni e le loro aspirazioni, sia per le generazioni attuali che per gli altri 80 milioni che verranno.

Quando avremo capito bene come funzionano il mondo e gli esseri umani, avremo scoperto come gli esseri umani possono essere soddisfatti, come possiamo dare un senso alla loro esistenza e come possono vivere a lungo termine nel loro ambiente sulla Terra.

Per completare questa ricerca, ripercorreremo rapidamente le diverse epoche dell'umanità, perché conoscere il passato fornisce le basi e l'ispirazione per interpretare il presente [^5] e dobbiamo conoscere il presente per determinare una possibile transizione futura. Perché la migliore società possibile è la migliore evoluzione possibile delle società da oggi in poi. [^1]

Per raggiungere questo obiettivo comune, i nostri 7,7 miliardi di esseri umani dovranno compiere due azioni essenziali: collaborare e decidere.

## Sottodomanda: Collaborazione

Come in ogni società, dovremo collaborare per fornire i beni e i servizi di cui abbiamo bisogno per prosperare. La nostra prima grande sotto-domanda sarà quindi :

> Come possiamo permettere alle persone di collaborare per fornirsi reciprocamente i beni e i servizi di cui abbiamo bisogno per raggiungere questo obiettivo comune?
> {.is-success}

Questo è il campo dell'economia. È uno dei grandi problemi del nostro sistema, da cui provengono la maggior parte dei vincoli e degli incentivi. Partire da zero e cercare la migliore economia possibile ci permetterà di definire le basi necessarie per una società ideale possibile, evitando i vincoli e gli incentivi che contrastano con la sua realizzazione. Come scopriremo, un sistema economico non si limita a produrre beni e servizi, ma produce anche esseri umani e le relazioni tra di essi, che sono altrettanto essenziali per la loro realizzazione.

## Sottodomanda: Decisioni

Infine, i nostri 7,7 miliardi di esseri umani, nel corso della loro vita e dello sviluppo delle loro società, dovranno fare delle scelte, raggiungere accordi e prendere decisioni sia su piccola che su grande scala. Questa è la nostra seconda grande sotto-domanda:

> Come possiamo prendere decisioni insieme e fare in modo che queste decisioni ci permettano di raggiungere il nostro obiettivo comune?
> {.is-success}

Come si può notare dalla domanda, l'idea non è solo quella di raggiungere un accordo, perché a cosa serve che tutta o parte della popolazione sia d'accordo su una decisione, e che questa venga convalidata, se alla fine non ci permette di raggiungere il nostro obiettivo comune.

Questo campo è la scienza del processo decisionale e si applica a molte pratiche, compresa la politica, dove attualmente si usa la democrazia rappresentativa.

Approfondiremo le diverse possibilità, tenendo conto di ciò che abbiamo imparato sul funzionamento dell'uomo e del mondo.

## Conclusione

Per riassumere, inizieremo a capire il nostro habitat, la Terra, il suo funzionamento, i suoi ecosistemi e il nostro posto al suo interno. Poi capiremo a fondo gli esseri umani, come funzionano e di cosa hanno bisogno per prosperare durante la loro permanenza sulla Terra.

Questa conoscenza ci permetterà di rispondere efficacemente alla domanda su come consentire agli esseri umani di prendere decisioni e cooperare per raggiungere l'obiettivo comune dello sviluppo sostenibile di ognuno di loro.

Il funzionamento della Terra nel capitolo 2, la comprensione degli esseri umani nel capitolo 3 e la rassegna delle epoche dell'umanità nel capitolo 4 possono sembrare noiosi da leggere se si ha già familiarità con queste aree o se non si vuole approfondire troppo all'inizio. In questo caso, vi consigliamo di passare direttamente ai capitoli 5 e 6, dove scoprirete un nuovo modo di lavorare insieme e di prendere decisioni. Queste due parti sono essenziali perché spiegano come la società che stiamo per immaginare sia possibile creando le basi che la renderanno coerente. Tuttavia, i capitoli 2, 3 e 4 non sono meno importanti e hanno portato alla realizzazione e alla giustificazione delle soluzioni dei capitoli 5 e 6. Se lo desiderate, sono stati prodotti dei riassunti che sono disponibili qui (/it/4-Appendices/1Summaries).

> La conoscenza è potere. Più comprendiamo il mondo e noi stessi, più abbiamo la possibilità di cambiarlo in meglio. Malala Yousafzai

## Fonti (elenco non esaustivo)

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, Le elezioni sono democrazia? / 1977
[^3]: [Prospettive della popolazione mondiale 2022", pubblicato dalle Nazioni Unite] (https://desapublications.un.org/file/989/download).
[^4]: [Rapporti IPCC 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, Plouc Pride: una nuova narrazione per la campagna. 2021
