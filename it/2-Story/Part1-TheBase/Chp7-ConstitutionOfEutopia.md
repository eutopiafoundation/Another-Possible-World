---
title: Costituzione di Eutopia
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Contenuto in fase sperimentale e in corso di scrittura.
> {.is-danger}

La costituzione di Eutopia è divisa in 3 affermazioni:

- La Dichiarazione Universale dei Diritti Umani, che incorpora molti dei principi fondamentali dell'attuale Dichiarazione Universale dei Diritti Umani[^1], ma che abbiamo adattato per aggiungere importanti sfumature ed estensioni per riflettere meglio i nostri valori come la sostenibilità, la diversità, l'interdipendenza con la natura, la libertà, la solidarietà e l'equità.
- La Dichiarazione universale dei diritti della natura / Madre Terra è una carta nata su iniziativa dei popoli amerindi e formulata in occasione della Conferenza mondiale dei popoli contro il cambiamento climatico del 2010[^2].
- Dichiarazione universale dei diritti umani in relazione alle specie animali non umane, è una carta provvisoria e sperimentale che mira a stabilire con maggiore precisione le nostre relazioni con le altre specie animali viventi.

Per non appesantire il testo, le motivazioni dei vari articoli sono state separate e poste alla fine di questa stessa pagina. Queste dichiarazioni sono liberamente modificabili dai cittadini di Eutopia attraverso lo stesso processo democratico descritto alla fine del capitolo precedente su come decidere insieme.

## Dichiarazione universale dei diritti umani

### Preambolo

Guidati dai valori dell'uguaglianza, della libertà, della solidarietà e della sostenibilità, riconosciamo la dignità intrinseca di ogni individuo, un faro che illumina la strada verso un mondo in cui tutti nascono uguali nei diritti e nelle libertà, e verso il nostro destino collettivo.

Celebriamo l'infinita diversità che tesse la ricchezza del nostro arazzo umano e siamo uniti dalla profonda convinzione che in questa diversità risiede la nostra forza collettiva. È in questo spirito di unità e armonia che enunciamo la presente Dichiarazione, affermando che ogni persona ha il diritto di esprimere liberamente il proprio pensiero, perseguire la propria felicità e contribuire alla nostra prosperità comune.

La nostra ricerca comune è caratterizzata dal rispetto assoluto per l'integrità dell'altro, dall'incessante ricerca dell'equità e della giustizia e da un sacro impegno per la conservazione della nostra casa comune, la Madre Terra.

Scrivendo questi diritti universali, erigiamo un potente scudo contro l'oppressione, l'ingiustizia e l'oblio. Dichiariamo che in ogni angolo di Eutopia, la luce dell'uguaglianza illumina gli angoli bui dell'ingiustizia, stabilendo un mondo in cui libertà, amore e compassione sono diritti inalienabili.

Possa questa Dichiarazione universale dei diritti umani ispirare l'azione, guidare le nostre decisioni e unire i nostri cuori nella costruzione perpetua di una società in cui tutti, mano nella mano, marciano verso l'alba di un mondo in cui la dignità umana è la bussola che guida il nostro cammino.

### Proposte e sintesi

Articolo 1. Uguaglianza :

- Gli esseri umani nascono e rimangono liberi e uguali nei diritti.
- Ogni cittadino di Eutopia ha il diritto inalienabile di partecipare al processo decisionale su un piano di parità con tutti gli altri. Nessuna forma di discriminazione o privilegio può impedire l'uguale potere decisionale di ciascun individuo.

Articolo 2. Libertà :

- Gli esseri umani hanno piena libertà di esprimersi e di pensare; nessuno può impedire la diversità delle opinioni e la libera circolazione delle idee.
- Anche la libertà di credo è sacra. Ogni individuo ha il diritto di scegliere, praticare ed esprimere le proprie convinzioni spirituali, religiose o filosofiche senza temere persecuzioni o discriminazioni.
- Gli esseri umani hanno il diritto di scegliere liberamente il proprio abbigliamento, di esprimersi personalmente attraverso l'abbigliamento, senza essere soggetti a giudizi o restrizioni imposte da terzi.
- Gli esseri umani hanno il diritto fondamentale di muoversi liberamente in tutto il mondo, tranne che per ragioni di conservazione della natura o per motivi di sicurezza dell'integrità umana o materiale, nessuna frontiera può limitare questa libertà di movimento.
- Ogni individuo è libero di intraprendere, di realizzarsi, di creare e di portare le proprie creazioni e innovazioni nel mondo. La creatività, l'innovazione e il contributo individuale al benessere comune di Eutopia devono essere incoraggiati.

Articolo 3. Solidarietà (Fraternità) :

- La solidarietà deve guidare le decisioni prese dai membri di Eutopia.

Articolo 3. Equità :

- Ogni decisione deve essere concepita in modo che tutti possano trarne pieno beneficio, eliminando barriere e disuguaglianze.

Articolo 4. Sostenibilità:

- La conservazione dell'ambiente è fondamentale ed è una responsabilità collettiva. Le decisioni devono tenere conto dell'impatto sulla natura e garantire un equilibrio sostenibile tra l'umanità e il pianeta.

Articolo 5. Integrità :

- In nessun caso una decisione può compromettere l'integrità di un'altra persona, sia fisica che mentale. Tuttavia, ogni individuo rimane sovrano ed è l'unico detentore del potere di compromettere la propria integrità.

Articolo 7. Sicurezza :

- Ogni individuo ha diritto alla sicurezza, alla protezione contro qualsiasi violenza fisica o mentale e a un ambiente sicuro in cui svilupparsi.
- Nessuna arma letale può essere detenuta o prodotta al di fuori dei centri di ricerca e delle persone autorizzate.

Articolo 8. Fiducia :

- La socialità si basa sulla fiducia collettiva, sull'aiuto reciproco altruistico e sul riconoscimento dei bisogni dell'altro. Nessun servizio reso può essere richiesto al destinatario in cambio di denaro, né può essere misurato e accumulato in un valore immaginario in vista di un futuro scambio per lo stesso servizio o per un altro. Chiunque dia se stesso a tutti non dà se stesso a nessuno in particolare.

Articolo 9. Diversità :

- Eutopia celebra la diversità in tutte le sue forme e si impegna a creare una comunità inclusiva, dove ogni individuo è rispettato e valorizzato per la sua unicità.

## Dichiarazione Universale dei Diritti della Natura / Madre Terra

### Preambolo

Noi, Eutopisti della Terra :

- Considerando che siamo tutti parte della Madre Terra, una comunità di vita indivisibile composta da esseri interdipendenti intimamente legati da un destino comune;
- Riconoscendo con gratitudine che la Madre Terra è la fonte della vita, del sostentamento e dell'educazione e che ci fornisce tutto ciò di cui abbiamo bisogno per vivere bene;
- Convinti che, in una comunità di vita che implica relazioni interdipendenti, è impossibile concedere diritti ai soli esseri umani senza causare uno squilibrio all'interno della Madre Terra;
- Affermando che per garantire i diritti umani è necessario riconoscere e difendere i diritti della Madre Terra e di tutti gli esseri viventi al suo interno, e che esistono culture, pratiche e leggi che riconoscono e difendono questi diritti;
- Proclamare questa Dichiarazione Universale dei Diritti della Madre Terra, affinché ogni persona e ogni istituzione si assuma la responsabilità di promuovere, attraverso l'insegnamento, l'educazione e il risveglio delle coscienze, il rispetto dei diritti riconosciuti nella Dichiarazione e di assicurare, attraverso misure e disposizioni diligenti e progressive su scala locale e globale, che essi siano universalmente ed effettivamente riconosciuti e applicati da tutti gli Eutopiani del mondo.

### Proposte e sintesi

Articolo 1. Madre Terra :

1. La Madre Terra è un essere vivente.
2. La Madre Terra è una comunità unica, indivisibile e autoregolata di esseri interconnessi che nutre, contiene e rinnova tutti gli esseri.
3. Ogni essere è definito dalle sue relazioni come elemento costitutivo della Madre Terra.
4. I diritti intrinseci della Madre Terra sono inalienabili poiché derivano dalla stessa fonte dell'esistenza.
5. La Madre Terra e tutti gli esseri possiedono tutti i diritti intrinseci riconosciuti in questa Dichiarazione, senza alcuna distinzione tra esseri biologici e non biologici o alcuna distinzione basata sulla specie, l'origine, l'utilità per gli esseri umani o qualsiasi altra caratteristica.
6. Così come gli esseri umani godono di diritti umani, tutti gli altri esseri hanno diritti specifici per la loro specie o tipo e appropriati al loro ruolo e funzione all'interno delle comunità in cui esistono.
7. I diritti di ciascun essere sono limitati da quelli degli altri esseri e qualsiasi conflitto tra i rispettivi diritti deve essere risolto in modo da preservare l'integrità, l'equilibrio e la salute della Madre Terra.

Articolo 2. I diritti intrinseci della Madre Terra :

1. La Madre Terra e tutti gli esseri che la compongono possiedono i seguenti diritti intrinseci:
   - il diritto di vivere e di esistere;
   - il diritto al rispetto;
   - il diritto alla rigenerazione della propria biocapacità e alla continuità dei propri cicli e processi vitali, senza alcun disturbo da parte dell'uomo;
   - il diritto a mantenere la propria identità e integrità come esseri distinti, autoregolati e interconnessi;
   - il diritto all'acqua come fonte di vita;
   - il diritto all'aria pulita;
   - il diritto alla piena salute;
   - il diritto di essere liberi da contaminazioni, inquinamento e rifiuti tossici o radioattivi;
   - il diritto a non essere geneticamente modificati o trasformati in modo da danneggiare la loro integrità o il loro funzionamento sano e vitale;
   - il diritto a una piena e pronta riparazione per qualsiasi violazione dei diritti riconosciuti nella presente Dichiarazione derivante dalle attività umane.
2. Ogni essere ha il diritto di occupare un posto e di svolgere il proprio ruolo all'interno della Madre Terra, in modo che essa possa funzionare armoniosamente.
3. Tutti gli esseri hanno il diritto al benessere e ad essere liberi da torture o trattamenti crudeli da parte degli esseri umani.

Articolo 3. Obblighi degli esseri umani nei confronti della Madre Terra:

1. Ogni essere umano ha il dovere di rispettare la Madre Terra e di vivere in armonia con lei.
2. Gli esseri umani hanno il dovere di
   - a) agire in conformità con i diritti e gli obblighi riconosciuti in questa Dichiarazione ;
   - (b) riconoscere e promuovere la piena realizzazione dei diritti e dei doveri enunciati nella presente Dichiarazione
   - (c) promuovere e partecipare all'apprendimento, all'analisi, all'interpretazione e alla comunicazione di modi di vivere in armonia con la Madre Terra in conformità con la presente Dichiarazione;
   - d) garantire che il perseguimento del benessere umano contribuisca al benessere della Madre Terra, ora e in futuro;
   - e) stabilire e applicare norme e leggi efficaci per la difesa, la protezione e la conservazione dei diritti della Madre Terra;
   - f) rispettare, proteggere e preservare i cicli, i processi e gli equilibri ecologici vitali della Madre Terra e, ove necessario, ripristinarne l'integrità;
   - g) assicurare che venga fornito un risarcimento per i danni derivanti dalle violazioni umane dei diritti intrinseci riconosciuti in questa Dichiarazione e che i responsabili siano chiamati a rispondere del ripristino dell'integrità e della salute della Madre Terra;
   - h) conferire agli esseri umani e alle istituzioni il potere di difendere i diritti della Madre Terra e di tutti gli esseri; e
   - i) mettere in atto misure precauzionali e restrittive per evitare che le attività umane portino all'estinzione delle specie, alla distruzione degli ecosistemi o all'interruzione dei cicli ecologici;
   - j) garantire la pace ed eliminare le armi nucleari, chimiche e biologiche;
   - k) promuovere e incoraggiare pratiche che rispettino la Madre Terra e tutti gli esseri, in conformità con le loro culture, tradizioni e costumi;
   - l) promuovere sistemi economici che siano in armonia con la Madre Terra e coerenti con i diritti riconosciuti in questa Dichiarazione.

## Dichiarazione universale dei diritti umani in relazione alle specie animali non umane.

### Preambolo

In questa Dichiarazione universale dei diritti umani nelle relazioni con le specie animali non umane di Eutopia, poniamo le basi per una coesistenza armoniosa basata sulla reciprocità e sul mutualismo. È fondamentale sottolineare che i principi qui esposti si applicano a qualsiasi animale con cui decidiamo di cooperare e condividere interazioni simbiotiche all'interno di Eutopia.

Facciamo questa distinzione deliberata per preservare la libertà degli animali come parte integrante del regno animale e dei loro habitat naturali, dove possono evolversi senza interferenze umane. In queste aree preservate, riconosciamo il valore inestimabile della natura selvaggia e ci impegniamo a lasciare che questi ecosistemi prosperino in completa autonomia.

Tuttavia, dove gli esseri umani risiedono e scelgono di costruire comunità, aspiriamo a vivere in armonia con le specie animali selezionate, stabilendo relazioni basate sulla comprensione reciproca, sul rispetto e sulla cooperazione vantaggiosa. Questa dichiarazione si applica quindi agli animali che integriamo nel nostro ecosistema, di cui diventiamo automaticamente responsabili, dimostrando il nostro impegno per una simbiosi equilibrata e rispettosa.

### Diritti delle specie sotto la responsabilità umana

Articolo 1. Rispetto della dignità degli animali:

- Tutti gli animali sotto la responsabilità degli abitanti di Eutopia hanno diritto a una vita dignitosa e priva di crudeltà.
- L'interazione umana con gli animali deve rispettare la loro natura intrinseca e il loro comportamento naturale.

Articolo 2. Condizioni di vita dignitose :

- Gli animali devono essere ospitati in condizioni che rispettino il loro benessere fisico e psicologico.
- Le strutture devono offrire spazio adeguato, accesso a cibo adeguato, acqua pulita e condizioni sanitarie ottimali.

Articolo 3. Accesso alle cure:

- Ogni animale sotto la responsabilità degli abitanti di Eutopia ha diritto a cure veterinarie regolari per garantire la sua salute e il suo benessere.
- Le cure mediche devono essere fornite in modo umano, rispettoso ed etico.

Articolo 4. Fine della vita:

- Gli animali destinati al consumo umano devono avere una fine della vita rispettosa e priva di inutili sofferenze.

## Giustificazione

### Dichiarazione universale dei diritti umani

In corso...

### Dichiarazione universale dei diritti della natura / Madre Terra

Attualmente in fase di elaborazione...

### Dichiarazione universale dei diritti dell'uomo in relazione alle specie animali non umane

Attualmente in fase di stesura...

## Fonti (elenco non esaustivo)

[^1]: [Dichiarazione universale dei diritti umani. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Dichiarazione universale dei diritti della Madre Terra. 2012](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
