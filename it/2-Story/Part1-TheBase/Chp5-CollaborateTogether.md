---
title: Lavorare insieme
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Che cos'è l'economia?

L'economia è l'attività umana che consiste nella produzione, distribuzione, scambio e consumo di beni e servizi[^2].

Ma un sistema economico non produce solo beni e servizi, produce anche esseri umani e le relazioni tra di loro. Il modo in cui la società produce e consuma ha una grande influenza su personalità, caratteri, conoscenze, desideri, felicità e tipi di relazioni interpersonali.

A volte tendiamo a giudicare la società solo in base alle quantità dei vari beni e servizi di cui i suoi membri dispongono. Ma le persone non danno valore solo a questi beni e servizi, ma anche ai sentimenti, agli atteggiamenti e alle relazioni interpersonali che li accompagnano, a ciò che questi trasmettono su ciò che significa essere umani. Questi aspetti sono troppo spesso trascurati in economia[^3].

Un sistema economico forma e modella gli esseri umani e le loro relazioni attraverso le loro attività di produzione e consumo. La società secerne il sistema economico, che a sua volta influenza profondamente la natura e la qualità delle relazioni tra le persone e la loro psiche.

In uno scambio, chi dà e chi riceve ricevono sempre più di quanto in realtà danno all'altro, perché si danno anche l'opportunità di entrare in una relazione umana.

Ci siamo abituati a seguire la scienza economica come se fosse un semplice dato di natura che detta come dovrebbe essere il mondo, senza interrogarci a fondo. Siamo impregnati del mondo in cui viviamo, ma una volta capito cos'è un sistema economico, diventiamo capaci di immaginare il nostro mondo che funziona con altre regole.

In questi giorni sta accadendo qualcosa di nuovo, perché alla miseria del mondo e al dominio che abbiamo gli uni sugli altri si aggiunge il cambiamento climatico, che ci porterà alla fine della nostra crescita economica. [^4]

Ogni anno i media e le organizzazioni di ricerca sul clima ci danno una data. È la data in cui gli scienziati stimano che l'umanità abbia consumato più risorse di quelle che la Terra può produrre nello stesso periodo.

Questa cifra è il risultato del lavoro di esperti in vari settori che fanno il punto sulle risorse che stiamo sottraendo al pianeta e sulla velocità con cui queste risorse vengono reintegrate.

Questa data è nota come "giorno del superamento". Affinché un'economia sia sostenibile, questa data dovrebbe essere il 31 dicembre e non prima. In questo caso, le risorse possono essere rinnovate alla stessa velocità con cui vengono consumate.

Nel 2022, questa data è stimata al 22 luglio[^1].

Questa data dovrebbe aiutarci a guardare con più chiarezza al nostro sistema economico, a prendere coscienza dei suoi fondamenti umani e dei suoi vizi.

In particolare, continuare a promuovere un sistema economico che incoraggia i nostri grandi rinforzi primari (= desiderio di sesso, cibo, informazione) è probabilmente la cosa peggiore che possiamo fare[^5].

Le teorie economiche non riescono a proporre argomentazioni che abbiano un reale sostegno intellettuale, il che dimostra la loro debolezza. Questo perché spesso si basano sulla nostra immaginazione collettiva e non hanno alcun fondamento reale.

Forse l'economia è più filosofica di quanto non voglia far credere e meno scientifica di quanto non voglia far credere.

Esistono quindi altre possibilità per un sistema economico?

## Il piano e il mercato

Attualmente esistono due sistemi economici che dominano il mondo moderno: il mercato e la pianificazione[^6].

Ciascuna parte difende il proprio sistema con un'etica sociale e attacca l'altra per la realizzazione di un'altra.

Per il mercato questo valore è la libertà, mentre per la pianificazione è l'uguaglianza.

Ma nel mercato siamo ben lontani dall'essere veramente liberi, anche se siamo più liberi che nel piano, ma siamo peggio quando si tratta di uguaglianza.

E nel piano siamo molto lontani dall'essere veramente uguali, ma lo siamo più che nel mercato, d'altra parte siamo i peggiori quando si tratta di libertà.

In realtà, la realtà attuale è che ognuno di questi sistemi si comporta male nell'ambito del valore che difende, ma è vero, meglio dell'altro per il valore difeso e ancora peggio secondo l'altro criterio etico.

Questi due sistemi economici, al loro estremo, sono diventati il mercato capitalista e il comunismo totalitario, entrambi i quali generano e rafforzano tratti e relazioni umane nauseanti e degradanti, come l'egoismo, il trattare gli altri come cose, l'ostilità, il conflitto, la competizione tra le persone, il dominio, lo sfruttamento, l'alienazione...

Nessuno di questi due sistemi economici è riuscito a esaltare le migliori qualità dell'uomo. Anzi, ne reprimono fortemente alcune. In entrambi i sistemi è presente una certa forma di dominio.

## La terza economia: la reciprocità

Esiste dunque un'altra alternativa?

Esiste una letteratura che inizia con "Ci sono tre tipi di sistemi economici". Ma questa letteratura va cercata ai margini storici o disciplinari del pensiero economico, tra gli economisti di diverse generazioni fa o nell'antropologia economica.

È il caso dell'economista Karl Polanyi (1886-1964), le cui opere classificano i sistemi economici di tutte le società, comprese quelle del passato, primitive e tradizionali, in tre categorie. E sottolinea che possono coesistere all'interno della stessa società, anche se ognuna assegna un posto privilegiato/dominante/principale a una di esse. Innanzitutto distingue un sistema economico, il mercato. Il secondo sistema lo chiama sistema di ridistribuzione, che nella sua forma elaborata e moderna è la pianificazione, in cui un centro politico decide l'allocazione economica. Il terzo sistema, Polanyi lo chiama "Reciprocità", secondo una tradizione antropologica già ben consolidata e anche abbastanza chiara e precisa. [^6]

Non conosciamo l'esistenza di questo tipo di economia, ma esiste nelle nostre società. Praticamente tutte le società contengono tutti e tre i sistemi economici, ma in tutte uno di essi è più importante degli altri due.

Ecco un triangolo corrispondente ai diversi sistemi principali nel 1984:
[![Diagramma dei 3 tipi di economia](./../../../GlobalProject/Image/Marché.png)]

Gli economisti hanno costruito l'economia e il suo pensiero sul presupposto che l'uomo fosse e sia sempre stato quello che chiameremmo "homo oeconomicus", e ne hanno creato una caricatura di essere egoista, calcolatore e asociale. Ma gli esseri umani erano soprattutto ciò che chiameremmo "homo sociabilis", praticando universalmente la reciprocità e necessitando universalmente di una cerchia di amici intimi o di una comunità[^9].

La reciprocità è il meccanismo dell'economia comunitaria che ha alimentato le origini dell'umanità, come cacciatori-raccoglitori e poi come agricoltori sedentari.

La reciprocità è ancora presente e la vita economica della famiglia si basa principalmente su questa economia. Ci si offre reciprocamente dei servizi senza aspettarsi nulla in cambio, ad esempio non si paga un membro della famiglia per andare a prendervi a scuola, né si è costretti a farlo da un'entità esterna. La reciprocità nelle nostre società è diminuita drasticamente negli ultimi decenni, a causa del mercato, che cerca di monetizzare sempre più le interazioni sociali, proprio per creare nuovi mercati. A parte la famiglia, però, possiamo trovare ancora molta reciprocità nelle relazioni amicali/affettive/amorose, nel volontariato, nelle comunità, ma anche in vari ambiti, come il mondo dell'informatica con l'opensource e le numerose piattaforme di condivisione di informazioni, realizzazioni, oggetti, ecc.

Una lunga serie di doni reciproci, regolari, continui e consolidati tra due persone è ciò che chiamiamo reciprocità. La reciprocità generale è l'equivalente, ma da parte di tutti. Tutti danno alla società e la società riceve da tutti.

L'altruismo e il dono, per loro natura, non possono essere ottenuti né con la coercizione né con lo scambio in senso stretto.

Questa è la differenza con il piano: questi trasferimenti non sono costretti e imposti al donatore da un'entità sociale esterna a lui. E la differenza con il mercato è che non si tratta nemmeno di scambi in senso stretto, in cui ogni transatore è esclusivamente interessato ad avere di più, o a non avere di meno, dei beni o servizi che sta trasferendo.

In questo sistema sociale, nessuno dà a un altro in modo specifico, quindi è impossibile dominare, costringere, umiliare o forzare un contro-dono, e sfruttare economicamente o eticamente con questo mezzo. Chi dà a tutti non dà a nessuno.

In questo sistema, il lavoro di ciascuno va a beneficio del consumo di un gran numero di altri, spesso molto più tardi. E tutti beneficiano del lavoro di molti altri, spesso molto prima. Ognuno riceve persino parte del lavoro di generazioni scomparse e lavora per generazioni che devono ancora nascere.

Il valore etico sociale della reciprocità potrebbe essere chiamato "fraternità". Qui abbiamo la famosa trilogia repubblicana: la fraternità è definita come "Non fare agli altri ciò che non vorresti fosse fatto a te; fai costantemente agli altri il bene che vorresti ricevere da loro".

Ma il paragone con la trilogia finisce qui, perché la reciprocità cancella il desiderio e il significato stesso degli altri due valori. Perché in questo sistema, dove ognuno si prende cura dell'altro come se si prendesse cura di se stesso, le volontà, e quindi le libertà personali, non si scontrano più, e le richieste di uguaglianza non hanno più alcun fondamento.

## Vantaggi della reciprocità

La reciprocità si basa sulla gentilezza.

Gentilezza significa enfatizzare l'altruismo, l'amore, la solidarietà volontaria, il dono reciproco, la generosità, la condivisione, la libera comunità, l'amore per il prossimo e la carità, la benevolenza e l'amicizia, la simpatia e la compassione. Tutti noi ne siamo carenti e sentiamo di averne bisogno di più. Tutte le nostre grandi famiglie dello spirito, del pensiero e del sentimento li rivendicano. Ma sono negati nella nostra vita economica. Dominano l'esistenza delle nostre società, ma possiamo vedere che la più diffusa delle aspirazioni è la più rara delle realizzazioni.

La reciprocità generale è la riconciliazione tra la nostra etica altruistica e la nostra tecnica scientifica, tra le nostre aspirazioni a una comunità calorosa, alla libertà individuale e al progresso materiale; permette la durata dei significati culturali e delle bellezze del mondo nell'espansione delle nostre conoscenze, capacità e coscienze.

Inoltre, la reciprocità porta con sé molti vantaggi alla produzione economica. Perché offre un sistema coerente ed efficiente che riduce drasticamente il tempo di lavoro complessivo.

## La reciprocità è possibile?

Mentre abbiamo studiato ampiamente i mercati e la pianificazione, non abbiamo detto quasi nulla sulla terza modalità di trasferimento di beni e servizi, ossia i doni, e non abbiamo detto nulla sul gruppo di doni noto come reciprocità.

Nel corso della storia sono apparsi nuovi sistemi economici e ogni volta ci sono state persone, forse anche la maggioranza, che pensano e sostengono che i sistemi possibili sono quelli che si possono osservare in quel momento o che sono esistiti in passato. Ma queste convinzioni sono costantemente contraddette dalla storia, dall'esistenza di nuove convinzioni. E questo è certamente vero anche oggi, perché non c'è alcuna prova che uno dei sistemi che esistono o sono esistiti sia il migliore possibile. In effetti, sì, sono possibili sistemi economici che non sono mai stati osservati.

Il difetto del nostro sistema è che impedisce agli individui di scegliere individualmente di dare: un'impresa che offre prezzi troppo favorevoli fallisce; un individuo non può dare ciò che produce in una società in cui nessun altro gli dà nulla.

Gli psicosociologi hanno studiato a lungo il "comportamento di aiuto", con risultati molto interessanti: non solo tendiamo ad aiutare chi ci ha aiutato, ma tendiamo anche ad aiutare di più se siamo stati aiutati noi stessi, cioè tendiamo ad aiutare gli estranei se siamo stati aiutati da estranei. Questa è l'essenza della reciprocità. [^13]

E questo è ciò che abbiamo visto nel capitolo 3 sull'Homo Sapiens, ovvero che è stato possibile creare esseri altruisti, perché l'altruismo è anche motivato dal piacere, e che sono soprattutto l'ambiente e l'educazione a contribuire allo sviluppo di valori e motivazioni come la generosità, la compassione e il desiderio di servire gli altri.

Ma è possibile che una società abbia un'economia basata sulla reciprocità, e in particolare sulla reciprocità generale?

Esistono intere società in cui tutte le relazioni economiche sono basate essenzialmente o addirittura esclusivamente sulla reciprocità. Ma queste sottosocietà o società sono quelle che chiamiamo "gruppi faccia a faccia", i cui membri si conoscono, famiglie, villaggi, bande, ecc. e la reciprocità non è essenzialmente generale. È stato comunque facile applicarla qui, ma una persona non può conoscere più di cento altre, quindi possiamo legittimamente chiederci se sia davvero possibile su larga scala[^14].

Infatti, sebbene l'economia risolva il problema della produzione e della distribuzione di beni e servizi, qualsiasi sistema economico (compresa la reciprocità) deve affrontare due problemi fondamentali per la produzione: il problema dell'informazione e il problema della motivazione.

## Il problema dell'informazione

L'informazione è la conoscenza da parte del lavoratore o del fornitore di risorse o servizi di ciò che deve fare per soddisfare i suoi bisogni e desideri nel miglior modo possibile.

La motivazione è l'incentivo a svolgere il lavoro o a utilizzare le risorse in questo modo.

Il piano e il mercato risolvono questi due problemi più o meno bene.

Il mercato risolve il problema dell'informazione attraverso i meccanismi decentralizzati del sistema dei prezzi, gli "indicatori di scarsità e utilità": domanda e offerta.

La pianificazione risolve il problema organizzando queste informazioni, cioè raccogliendo, calcolando ed emettendo istruzioni o previsioni, in modo relativamente centralizzato.

Il mercato risolve il problema della motivazione riducendo l'egoismo attraverso l'incentivo dello scambio.

Il piano risolve il problema attraverso la costrizione, la minaccia o la punizione, la ricompensa o la promozione, ma più in generale attraverso l'autorità e i suoi vari mezzi di costrizione o ricompensa, e l'interiorizzazione più o meno forte della necessità di obbedire per abitudine o senso del dovere.

Gli stessi problemi si applicano alla reciprocità: come si fa a sapere di cosa hanno bisogno gli altri e a dare abbastanza agli altri?

Questi due problemi si rafforzano a vicenda, in quanto la soluzione di uno aiuta l'altro e viceversa.

Quanto meglio si risolve il problema della motivazione, tanto meglio si risolve il problema dell'informazione, perché le persone possono trasmettersi più facilmente le informazioni necessarie.

Più il problema dell'informazione è risolto, più il sistema economico è produttivo, e quindi meno sprechi ci sono, meno lavoro è necessario per un determinato servizio finale, e quindi meno lavoro deve fornire il sistema motivazionale, o meno grave è la sua imperfezione.

Reciprocamente, ognuno considera i bisogni o i desideri degli altri importanti quanto i propri, e questo determina ciò che dà del suo lavoro (tempo, sforzo, fatica) e dei suoi beni, qualunque sia la loro origine.

Se si risolve il problema della motivazione, si risolve anche il problema dell'informazione, perché le persone possono sempre rendere l'economia efficiente almeno quanto il mercato o il piano, trasmettendosi volontariamente le informazioni economiche che circolano in questi sistemi.

La tecnologia dell'informazione ha veramente rivoluzionato questo settore, ora che siamo tutti connessi e grazie allo sviluppo di applicazioni appropriate il trasferimento di informazioni sta diventando più facile che mai.

Trasmettere ed elaborare informazioni fa parte del lavoro della società, insieme ad altri lavori. In cambio, la motivazione ci spinge a fare ciò che è meglio per la comunità. Questo sistema può quindi fare tutto ciò che il mercato o il piano possono fare.

Inoltre, la reciprocità riduce notevolmente la quantità di informazioni necessarie. Attraverso la riduzione dei consumi competitivi o vistosi, la riduzione dei bisogni estrinseci e la riduzione della produzione dei beni o servizi corrispondenti, la riduzione della necessità di rinnovare i prodotti grazie alla loro progettazione basata sulla sostenibilità e la riduzione di molti servizi che non saranno più necessari per il funzionamento dell'economia.

## Il problema della motivazione

Quando si parla di "natura umana" si sente dire "l'uomo non cambierà mai, è sempre stato egoista, ecc." e poi vengono le idee sulla "vera" natura umana che dovrebbero alla fine prevalere. La conoscenza storica, antropologica e psicologica, l'analisi e anche la semplice logica devono assolutamente essere contrapposte a queste convinzioni ingenue, e la risposta a "l'uomo non cambierà mai" potrebbe anche essere che è solo cambiato[^15][^16][^17].

A "l'uomo è sempre stato ed è ovunque egoista" possiamo altrettanto facilmente replicare chiedendoci cosa sappiamo di ciò che "è sempre stato".

L'umanità si sta imbarcando in una trasformazione senza precedenti nella sua esistenza in termini di profondità e, naturalmente, di scala.

Come abbiamo visto, la natura umana non esiste e l'uomo può diventare qualsiasi cosa scelga di diventare.

Ecco una serie di punti che rispondono alla domanda sulla motivazione:

- Le persone hanno una preferenza per le relazioni altruistiche e di donazione[^18].
- Tutte le principali etiche religiose e laiche sostengono con forza l'altruismo e la donazione. [^20]
- La principale motivazione umana è quella di ricevere approvazione da altri esseri umani: in un sistema reciproco l'approvazione è naturalmente universale e reciproca[^19].
- Le analisi empiriche del "comportamento di aiuto" dimostrano che le persone sono molto più propense ad aiutare gli altri se sono state aiutate a loro volta.
- I sociologi hanno difeso l'idea che la caratteristica principale degli esseri umani sia l'imitazione[^22].
- L'abitudine, questa imitazione di se stessi, è un altro fenomeno umano di primaria importanza[^23].
- Gli studi sullo sviluppo dei bambini dimostrano che inizialmente siamo altruisti, poi la società ci rende egoisti[^21].
- Educazione: come abbiamo visto, l'essere umano è solo una pagina su cui la società scrive[^24].
- Rafforzamento sociale: più le persone hanno uno spirito di reciprocità, più dimostrano con le loro azioni ed esprimono con le loro parole che è normale e buono averlo, e questo incoraggia gli altri in questa direzione. L'educazione e le relazioni sociali nel loro complesso giocano quindi un ruolo fondamentale. Se l'uomo è prima di tutto culturale, e quindi malleabile, la società può renderlo una persona di reciprocità tanto quanto l'egoista del mercato, o il sottomesso o il gerarca del piano[^25].
- Più c'è reciprocità, più vediamo che è praticabile e più ci rendiamo conto dei suoi vantaggi, e quindi più la accettiamo, più "ci crediamo", più la vogliamo, più la spingiamo. E quindi più tende a diffondersi.

- Nel caso in cui alcune persone in grado di farlo non vogliano partecipare, non è una cosa grave, perché se questi casi esistessero, non si tratterebbe di disprezzo per loro, perché un tale sentimento significherebbe un malessere nella società: Ma anche se la persona non vuole partecipare, non è una cosa grave, perché non solo scopriremo che il carico di lavoro richiesto per abitante sarà notevolmente ridotto, ma anche che questo può essere controbilanciato dal fatto che gli altri saranno felici di dare "più" di quanto ricevono, per altruismo come abbiamo visto, ma anche per il proprio interesse nel lavoro.

Dopo tutto, nella società di oggi, perché lavorare?

- Coercizione diretta: lavoro forzato
- Scambio: vendita del proprio lavoro in cambio di un salario
- Per il desiderio di aiutare gli altri o la comunità, per "essere al servizio".
- Per senso del dovere, possibilmente per restituire agli altri ciò che riceviamo da loro.
- Per interesse verso il lavoro stesso.
- Per trovare contatti sociali, relazioni, integrazione nella società, contribuendo all'ambiente di lavoro.
- Per dimostrare agli altri che si lavora per loro, che si fanno cose belle o che si è abili.
- Per lo status sociale legato al lavoro (contributo, responsabilità, ecc.).
- Per tenersi occupati, per avere "qualcosa da fare", per evitare di annoiarsi o di rimanere inattivi.
- Per il gioco
- Come abitudine

Potremmo aggiungerne altri, ma è evidente l'importanza dell'occupazione in sé e delle relazioni sociali che il lavoro ci permette di creare, come si evince dai sentimenti che seguono il "pensionamento". In generale, le persone traggono più significato dall'attività produttiva che da quella di consumo. Tutte queste motivazioni, ad eccezione delle prime due, possono spingere le persone a lavorare in un sistema di reciprocità, quindi non dobbiamo far altro che giocare su di esse aumentando il significato e l'interesse del lavoro.

Il lavoro non è condannato a essere la tortura della sua discussa origine etimologica, dalla parola travalhum: tripalium, strumento di tortura con un treppiede...[^26].

Tutte le dimensioni della pesantezza del compito devono quindi essere cancellate o ridotte. Il lavoro che è fisicamente o psicologicamente estenuante, stucchevole o deprimente, troppo duro, meccanico, monotono, troppo sporco o sgradevole, troppo privo di significato, spregevole, ecc. deve essere abolito, sostituito o reso meno faticoso. Dovrebbe essere eliminato, sostituito, limitato, automatizzato, migliorato, arricchito, compensato, ricompensato, valorizzato o condiviso più ampiamente.

L'idea è quella di trasformare la natura del lavoro in un'attività che sia qualcosa di simile all'autorealizzazione, alla manifestazione della propria profonda umanità attraverso la trasformazione della natura e la sua conservazione. In questo modo, il lavoro diventerebbe il "primo bisogno della vita". Così l'espressione "lavorare per vivere" non significherebbe più soffrire il dolore del lavoro per sopravvivere e mantenersi, ma guadagnarsi effettivamente da vivere attraverso un'attività ricca di significato e di esperienza.

## Il posto del denaro nella reciprocità

Siamo talmente abituati che tutti crediamo che il denaro sia una condizione necessaria per la vita in società, eppure il denaro non è un dato di natura, ma un'invenzione immaginaria degli esseri umani che può essere criticata[^30].

Il funzionamento del denaro non spiega la sua origine, non spiega se stesso, ed è per questo che gli economisti non spiegano nulla. Né si spiegano per motivi esistenziali.

Il denaro sarebbe stato inventato per risolvere il problema della doppia coincidenza dei bisogni posto dal baratto, perché senza denaro sarebbe troppo difficile portare a termine gli scambi. Come si fa a confrontare i beni, e cosa succede se io ho bisogno delle tue scarpe ma tu non hai bisogno delle mie mele?

Nonostante le numerose ricerche antropologiche abbiano dimostrato il contrario, questo mito è uno dei più radicati nella percezione che le persone hanno dell'economia.

Nelle economie primitive, le persone prendevano semplicemente ciò di cui avevano bisogno e, da un punto di vista sociale, condividevano le risorse. Ignorando l'ossessione per la scarsità che caratterizza le nostre economie di mercato, le economie primitive erano in grado di concentrarsi sistematicamente sull'abbondanza[^7][^8].

Alcune ricerche antropologiche basate più strettamente su elementi storici e scoperte empiriche ci portano a credere che il denaro non derivi né dal lavoro né dallo scambio, ma che sia innanzitutto storicamente e concettualmente un modo per creare un legame sociale tra gli individui, in quanto rappresenta la misura dei debiti contratti tra di loro.[^12]

Alcune civiltà, come gli Inca, non conoscevano né il denaro, né il mercato, né il commercio. [^10]

Il problema del denaro è che, finché è necessario averlo per accedere allo scambio di beni e servizi, il capitale può giocare con noi.

Non è quindi mettendo le mani sul lavoro che il capitalismo si assicura il dominio, ma padroneggiando le condizioni della socialità che rendono necessario il lavoro e che includono, tra le altre cose, la necessità del denaro [^3].

Il denaro è il segno della fiducia tra gli uomini. Ci vuole infatti una fiducia incrollabile per accettare un segno monetario da qualcun altro in cambio di un servizio reso o di un bene fornito: fiducia nel fatto che non si tratti di una banconota falsa o di denaro contraffatto, fiducia nel futuro che questo segno monetario rappresenta nella misura in cui rende possibili progetti futuri. In ultima analisi, il denaro è l'orizzonte della reciprocità nel tempo, non necessariamente nei confronti di chi mi ha dato il denaro, ma la fiducia nel fatto che in futuro ci sarà qualcuno in grado di restituirmi sotto forma di beni e servizi il valore dei beni e servizi che ho temporaneamente venduto per denaro.

Perché usare un segno monetario se mi fido davvero dell'altra persona? L'oggetto stesso, il concetto stesso di denaro, riflette in realtà una mancanza di fiducia nell'altra persona, mentre se questa fiducia si stabilisce la sua utilità scompare.

Se l'altra persona si fida davvero di me e della società, può darmi qualcosa senza chiedermi denaro in cambio, perché sa che domani sarò io o qualcun altro a darle qualcosa.

La relazione di scambio, mediata dal denaro, è una relazione basata sull'idea che i partecipanti non possono avere fiducia nella durata della loro relazione: usano il denaro per completare lo scambio, per finalizzare il debito, insomma per non avere più nulla a che fare l'uno con l'altro una volta conclusa la transazione.

L'attuale discussione politica nelle nostre economie verte su come utilizzare questo denaro, in modo che funzioni correttamente, ma forse il denaro non può funzionare correttamente, forse è destinato a pervertire le relazioni sociali?

Tuttavia, se dovessimo dargli una natura, sarebbe meglio dire che il denaro è destinato a pervertire le relazioni sociali nella sua forma attuale di mezzo di scambio, piuttosto che come "facilitatore della circolarizzazione di beni e servizi".

Il denaro si insinua in tutti i vuoti lasciati dall'assenza di senso, ci permette di mantenere il controllo per calmare la nostra corteccia cingolata, e la paura della morte diventa il motore dell'avarizia[^11].

Molte analisi rilevano gli effetti negativi dell'influenza del denaro nella società, ma non riescono a proporre una risposta adeguata al problema della monetizzazione della realtà.

In una società monetizzata, il valore delle cose diventa quello attribuito dal denaro.

Abolire il denaro sembra impossibile, tanto naturale è diventata per noi l'idea del denaro. La monetizzazione della società sembra sempre più un dato di fatto al di fuori del quale non è possibile nemmeno pensare.

Il problema è che non è più l'individuo a determinare il suo sistema di valori, e quindi ciò a cui aspira, ma che questo sistema di valori è reso oggettivo dal denaro.

Tuttavia, prendendo le distanze dal denaro, possiamo recuperare un senso di libertà dai valori del mercato, riappropriandoci dei nostri valori.

Ma se continuiamo a pensare che il denaro nella sua forma attuale sia l'unico modo per risolvere la questione dello scambio di beni, e quindi la questione dell'incontro tra gli individui, in altre parole se pensiamo che il denaro sia un prerequisito della socialità, ci sarà molto difficile distinguere il valore economico dai valori etici e politici.

Sono le nostre società occidentali che, molto recentemente, hanno trasformato l'essere umano in un "animale economico". Ma non siamo ancora tutti animali economici. L'homo oeconomicus non è dietro di noi, ma davanti a noi, come l'uomo della morale e del dovere, come l'uomo della scienza e della ragione. Per molto tempo, l'uomo è stato qualcos'altro; e non è passato molto tempo da quando è diventato una macchina complicata dotata di una macchina calcolatrice.

Stiamo cercando di dare valore ad altre sfaccettature dell'umanità oltre al rapporto economico di scambio di beni.

Un valore che permetta a ciascuno di prosperare in un incontro permanente con gli altri all'interno di una socialità basata sui principi della condivisione e dell'aiuto reciproco.

> "Alcuni uomini vedono le cose come sono e si chiedono: "Perché? Io sogno cose che non sono mai state e chiedo 'Perché no? George Benard Shaw

## Confronto con le ideologie attuali

Questa presentazione porta inevitabilmente alla creazione di una nuova ideologia. Riteniamo che sia essenziale fare dei paralleli con le ideologie esistenti e spiegare perché sono diverse e perché queste altre ideologie non ci sembrano sostenibili o efficaci.

Ma l'idea non è quella di rifiutare queste ideologie, perché i valori alla base della sinistra, l'uguaglianza, e della destra, la libertà, sono belli e necessari per un'Eutopia, ma ora si tratta di raccogliere il buono. In modo da poterci unire.

Così non diremo solo "Avete tutti torto", ma "Avete tutti in parte ragione".

D'ora in poi non cercheremo di entrare in confronto con i movimenti preesistenti. Al contrario, preferiamo mantenere relazioni amichevoli, creare apertura e sostenere altri progetti in linea con i valori che difendiamo. L'idea è quella di rispettare gli altri per quello che credono, in modo che anche loro possano rispettare noi per quello che crediamo. Crediamo che le verità alla fine vengano fuori da sole e che non dobbiamo essere disturbati nelle nostre riflessioni, con il rischio di chiuderci agli altri. Crediamo che questo sia l'unico modo per uscire dall'abisso in cui ci troviamo, rimanendo uniti nonostante le diverse opinioni, uniti nella comune ricerca della realizzazione.

### Confronto con la sinistra

I sistemi ideologici che sostengono una maggiore pianificazione, o addirittura una pianificazione totale, sono quelli che più potrebbero assomigliare alla nostra Eutopia, eppure bastano poche parole per cambiare completamente il sistema pratico che ne emerge.

Socialismo = "Da ciascuno secondo le sue capacità e a ciascuno secondo il suo lavoro".
Comunismo = "Da ciascuno secondo le sue capacità e a ciascuno secondo i suoi bisogni"[^27].
Reciprocità = "Da ciascuno, volontariamente, secondo i bisogni degli altri"[^3].

L'inadeguatezza e le carenze di queste altre ideologie sono in parte le cause essenziali di ciò che il mondo è oggi. Hanno dato origine a deviazioni, a rivoluzioni anticapitaliste, ma attraverso queste non sono riuscite a creare società di tipo nuovo su gran parte del globo. E poiché il risultato non è ovviamente migliore del capitalismo sviluppato, lo hanno protetto. Ora sono usati da tutte le parti per costituire il più gigantesco inganno e ipocrisia della storia.

L'uguaglianza tra le persone, comunque definita, può essere un limite alla libertà di alcune persone. E la libertà da sola non garantisce in genere l'uguaglianza.

La migliore delle società è probabilmente molto diversa da quelle che conosciamo, come la nostra società di classi sociali dominanti e dominate. Ma coloro che hanno cercato di reinventarla hanno fallito, in primo luogo perché non specificano abbastanza le società che propongono.

Se questo approccio analitico era necessario, ora dobbiamo andare oltre se vogliamo continuare l'opera di denaturalizzazione genetica intrapresa in particolare dal famoso Karl Marx [^28].

Inoltre, il sistema marxiano non può pensare al denaro né prima né dopo il capitalismo: oltre a gettare nell'ombra duemila anni di storia umana, non può proporre alcuna via d'uscita.

Le difficoltà del pensiero di Marx risiedono nella possibilità di pensare a una forma di organizzazione del lavoro e della socialità economica diversa da quella del modo di produzione capitalistico.

Così, nell'era della noosfera, i movimenti marxisti che trascinano con sé teorie vecchie di 150 anni hanno grandi difficoltà a reimmaginare organizzazioni del lavoro orizzontali autogestite, a reinventare i nostri processi democratici, a immaginare un mondo diverso da quello del produttivismo, a comprendere l'essere profondo che siamo e i mezzi per trasformarlo. Troppo spesso lottano contro il capitalismo e hanno difficoltà a proporre soluzioni alternative autentiche per le quali lottare.

Non stiamo criticando l'opera magistrale di Marx, che era molto necessaria per la riflessione, ma pensiamo che se avesse ammesso sinceramente di essere anche etico e ne avesse parlato molto di più nelle sue opere, se avesse parlato di più della società finale o buona e dei mezzi per raggiungerla, se avesse messo nelle sue caratteristiche tanto altruismo quanto ne aveva in se stesso, forse sarebbe successo meno del peggio[^3].

### Confronto con il diritto

Le politiche di destra, cioè quelle che promuovono il mercato, o addirittura il libero mercato totale, sono il movimento ideologico dominante nei Paesi occidentali, almeno quello al potere, ed è addirittura il movimento ideologico dominante nel mondo, e quello che ha il maggiore impatto.

Tutti possono vedere gli effetti negativi, non solo sul pianeta e sui nostri ecosistemi, ma anche sul nostro benessere generale. In breve, in questo sistema economico, passiamo il nostro tempo a sprecarlo e sprechiamo la nostra vita a "guadagnarlo". E chi non spreca il proprio tempo, che lo faccia o meno, ne raccoglie comunque le disastrose conseguenze, se non a breve termine, a lungo termine.

Nel mercato, le persone hanno sempre delle catene. Ma per la maggior parte non si tratta più di catene rozze di acciaio arrugginito, bensì di catene "finemente nichelate", molto più "resistenti" e in definitiva anche più difficili da rompere, soprattutto perché alcuni vi infilano maglie d'oro. In questo modo abbiamo la sensazione di avere qualcosa di prezioso da perdere.

Il nostro sistema intrappola ogni individuo da tutti gli altri, nel senso che una persona non può semplicemente, da sola e liberamente, manifestare le sue qualità migliori. In economia, in un sistema competitivo, un'azienda non può, pena il fallimento, non cercare di massimizzare il proprio profitto se altri cercano di massimizzare il proprio. Allo stesso modo, una persona che dà non può sopravvivere in una società in cui gli altri prendono.

C'è del buono in questo sistema, come la libertà d'impresa e la "libertà" generale che prevale, e non si può negare l'efficacia che ha avuto su molti progressi. Ma non possiamo nemmeno negare le conseguenze che ha avuto, perché dove alcuni si sono trovati, altri non si sono trovati... coloro che sono stati trascurati, sfruttati, dimenticati... È giunto il momento di andare avanti e di includere finalmente tutti.

Qui finisce questa prima versione della nostra analisi del confronto con la destra. Non che non ci sia molto da dire - al contrario, esiste un'enorme letteratura sul capitalismo che ne descrive a lungo le origini, le cause e le conseguenze. Ma il modo in cui vogliamo guardare a queste ideologie, i rapporti che vogliamo intrattenere con esse, e infine tutte le questioni legate all'impegno del nostro movimento in una politica concreta, sono ancora da definire collettivamente. Tuttavia, se ritenete di non saperne abbastanza, vi suggeriamo di consultare le numerose risorse disponibili in letteratura e su Internet:[^29][^31].

## Fonti (elenco non esaustivo)

[^1]: L'Earth Overshoot day 2017 è caduto il 2 agosto, l'Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipedia : Economia](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: La fine della crescita. Richard Heinberg. 2013
[^5]: Sébastien Bohler. L'insetto umano. 2020
[^6]: Karl Polanyi. "La grande trasformazione" e "L'economia come processo istituito" in Commercio e mercato nei primi imperi: economie nella storia e nella teoria.
[^7]: Marshall Sahlins - Economia dell'età della pietra.
[^8]: Marcel Mauss - Saggio sul dono
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Organizzazione sociale degli Inca](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Dov'è il significato? 2020
[^12]: David Graeber. Debito: 5000 anni di storia. 2011
[^13]: Lee Alan Dugatkin. L'equazione dell'altruismo: sette scienziati cercano le origini della bontà. 2006
[^14]: [Numero di Dunbar](https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Una breve storia dell'umanità
[^16]: Jonathan Haidt. La mente giusta: perché le persone buone sono divise dalla politica e dalla religione.
[^17]: Daniel Kahneman. Il pensiero veloce e lento
[^18]: [Ernst Fehr, La natura dell'altruismo umano](https://www.nature.com/articles/nature02043)
[^19]: [Il bisogno di approvazione sociale](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religione e moralità](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruismo nei bambini e comportamento percepito dagli altri in Journal of Abnormal and social Psychology, 1942, 37, pp. 2018-233
[^22]: Gabriel Tarde. Le leggi dell'imitazione, 1890
[^23]: Charles Duhigg. Il potere dell'abitudine. 2014
[^24]: Émile Durkheim. Educazione e sociologia; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. LA TEORIA DELLA CONDOTTA NORMATIVA: UN PERFEZIONAMENTO TEORICO E UNA RIVALUTAZIONE DEL RUOLO DELLE NORME NEL COMPORTAMENTO UMANO.
[^26]: [L'etimologia della parola "lavoro" è tripolare?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Critica del programma di Gotha (1875). 1891
[^28]: Karl Marx. Il Capitale. 1867
[^29]: [Conferenza: Comunismo e capitalismo: la storia di queste ideologie](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. L'ordine del denaro - Critica dell'economia. 2019
[^31]: [Arte 2014 Documentario - Ilan Ziv - Capitalismo](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
