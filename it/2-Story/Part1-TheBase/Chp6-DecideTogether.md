---
title: Decidere insieme
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Questa pagina è ancora in fase di scrittura, quindi alcune parti potrebbero essere mancanti.
> {.is-info}

## Come possiamo decidere tutti insieme?

Dobbiamo capire come prendere decisioni e assicurarci che siano quelle giuste per raggiungere il nostro obiettivo comune.

Esistono molti strumenti decisionali e molti altri devono ancora essere inventati. Qui lasceremo da parte gli strumenti di precisione su piccola scala e ci concentreremo su quello globale.

Nella nostra società occidentale, lo strumento utilizzato è la democrazia. Analizzando e reinventando la democrazia, possiamo sviluppare strumenti su scala ridotta basati su di essa, che possono poi essere adattati ad altre forme di organizzazione, come le imprese, le scuole, ecc.

## Cos'è la democrazia?

Democrazia è la combinazione greca di demos "popolo" e kratos "potere". Oggi il termine viene utilizzato per descrivere qualsiasi sistema politico in cui il popolo è sovrano[^1].

Molti Paesi hanno ottenuto la parità di potere politico attraverso il suffragio universale. Questa vittoria è ora sancita nelle loro costituzioni ed è considerata uno dei gioielli istituzionali più brillanti delle loro società.

Ma "democrazia" è ancora una parola molto bistrattata e abusata.

Proclamiamo la democrazia, come se ne avessimo davvero il potere.

Ma la nostra democrazia non è altro che una ricorrente abdicazione del nostro legittimo potere ai politici che se ne appropriano.

E una volta ottenuto, si sentono legittimati ad averlo, ma anche loro sono sotto il controllo di un potere molto più grande.

Pensiamo che il sistema politico domini e che possa cambiare tutto il resto, compresa l'economia e la sua forma. Pensiamo che il nostro sistema sia perfettamente democratico grazie alla competizione tra i candidati che votiamo.

Ma vedremo che l'economia gioca un ruolo in questa scelta competitiva. Quindi, da un lato il sistema elettorale non ha più la qualità di offrire una buona competizione tra i candidati, dall'altro non domina l'economia perché l'economia lo influenza[^2].

Quindi è l'economia a dominare la politica.

Sulle questioni climatiche, ad esempio, gli studi dimostrano che non c'è correlazione tra ciò che la maggioranza delle persone vuole e ciò che ottiene, tranne quando ha le stesse preferenze del 10% più ricco della popolazione[^3].

La nostra democrazia deve essere completamente rivista, e qui stiamo parlando di molti difetti: abuso di potere, corruzione, influenza delle lobby, decisioni non coordinate e non condivise, difetti di rappresentatività, illusioni di scelta, disuguaglianza di opportunità.

Ed è qui che scopriamo che il risultato reale del nostro sistema democratico è l'opposto della sua aspirazione ideologica.

Nonostante tutti questi difetti, la democrazia è un rischio che vale la pena di correre[^4] se vogliamo preservare la nostra libertà.

Cominciamo quindi ad analizzare la nostra democrazia, a capire cosa non va e a reinventare un sistema democratico completamente diverso.

## Il problema delle nostre democrazie

### L'imprenditore politico[^5][^6][^7]

L'obiettivo di ogni politico è essere e rimanere al potere. Fa carriera, il che gli permette di guadagnarsi da vivere e di trovare il suo posto nella società.

Sono in competizione tra loro, ma anche in collusione: vogliono distruggere l'avversario politico, ma non la carica politica.

Tutti i politici, a prescindere dalla loro opposizione, hanno un interesse comune a rafforzare, non a diminuire, il potere dei politici. E se uno di loro suggerisce l'idea, riceverà la disapprovazione di tutti gli altri.

La stragrande maggioranza degli imprenditori politici proviene dalla stessa classe sociale, e generalmente anche dalla stessa classe sociale dei leader del settore privato e delle stesse famiglie borghesi.

I candidati non cercano di presentare il programma che costituisce il loro ideale di buona società.

Qualunque siano i loro fini personali, scelgono il programma che meglio si adatta al maggior numero di persone. Arrivano a sacrificare la difesa delle proprie opinioni, ideali e preferenze personali, se ne hanno, a quelle di ciò che vuole la massa della popolazione.

Tuttavia, questa conoscenza dei desideri degli elettori da parte dei candidati, per la maggior parte, non viene utilizzata per servire il maggior numero, ma, come vedremo, per sfruttarlo a vantaggio del più piccolo.

I politici reagiscono semplicemente agli incentivi che consentono loro di acquisire e mantenere il potere.

Non ha senso odiarli, stanno semplicemente seguendo le regole di un gioco. Quindi è il gioco che va odiato, non i giocatori[^8].

Ma il problema degli imprenditori politici è che il loro obiettivo non è avere ragione, ma che le persone e le imprese gli diano ragione.

Gli imprenditori politici non sono stupidi, sono ottimi tattici che reagiscono agli incentivi del gioco e sono molto ben consigliati dall'esperienza sul campo della loro squadra, per cui utilizzano vari stratagemmi e alleanze per raggiungere i loro scopi.

Per dare una buona immagine, i candidati nascondono la loro personalità, si camuffano e diventano ipocriti.

Gli elettori pensano di sapere per chi stanno votando, perché spesso l'enfasi viene posta sulla personalità del candidato, evidenziandone la seduttività, il carattere rassicurante o il carisma, creando un effetto alone. E queste impressioni sono molto più facilmente manipolabili dai media rispetto al contenuto di un programma.

### Programmi politici[^2]

I candidati costruiscono il loro "programma" non tutto in una volta, ma con aggiunte o modifiche successive di dichiarazioni e azioni, senza tuttavia poter tornare indietro, cancellare o contraddire le parole o le azioni precedenti. Questo vincolo si spiega con il tempo necessario per rendere noto un programma, mentre chiarirlo man mano, aggiungerlo, rafforzarlo, raggiunge più efficacemente gli elettori.

Si spiega anche con il fatto che queste contraddizioni o smentite danneggerebbero l'affidabilità e la credibilità del candidato, creando una struttura evolutiva e iterativa della conoscenza e della costruzione del programma.

Questo porta spesso a differenze nei loro programmi e a un'imperfezione che ostacola la qualità democratica del risultato del processo.

In secondo luogo, i programmi sono globali, coprono tutte le materie contemporaneamente, e si deve scegliere tra di essi, anche se si può preferire uno su un punto e l'altro su un altro.

In questo caso, i programmi dividono la popolazione e uno di essi potrebbe essere eletto dalla maggioranza, mentre la stessa maggioranza preferisce un punto di tutti gli altri programmi a quello del vincitore.

Questo è un aspetto che i referendum specifici consentono, in particolare, di esprimere la propria opinione su ciascun quesito separatamente.

L'effetto dei contributi finanziari di interesse personale è quello di standardizzare i programmi, mentre l'effetto del sostegno idealista/attivista è quello di differenziarli.

### Onestà e trasparenza

Ogni sistema che può essere manipolato sarà manipolato nel momento peggiore[^9].

Quando si progetta un sistema, bisogna assolutamente chiedersi come le parti interessate al sistema saranno in grado di attaccarlo e di trarne un vantaggio disonesto.

L'onestà e la trasparenza sono problemi persistenti nei sistemi democratici di tutto il mondo. I politici e i rappresentanti eletti sono spesso accusati di scarsa trasparenza, di essere corrotti e di essere coinvolti in numerosi conflitti di interesse, favorendo interessi particolari rispetto all'interesse generale.

Nel nostro sistema, alcune regole non promuovono l'onestà e l'etica.

È quindi fondamentale progettare sistemi che incoraggino le persone ad agire in modo etico e morale.

Dobbiamo sviluppare il principio della disclosure, in cui i partecipanti hanno interesse ad essere onesti nelle loro dichiarazioni e a comportarsi in modo etico e morale, in quanto ciò sarebbe nel loro migliore interesse e li avvantaggerebbe a lungo termine. È necessario prevedere maggiori misure di trasparenza.

### La scelta delle alternative

A parte il fatto che i programmi sono globali, la scelta di programmi o candidati offerta all'elettore è generalmente ridotta.

Il finanziamento delle campagne elettorali è uno dei fattori che limitano la scelta delle alternative. I candidati sono quelli che hanno accesso a fonti di finanziamento e possono distribuire informazioni e propaganda sufficienti a rendere visibile il loro partito e ad avere una possibilità di vittoria[^10].

I partiti politici possono avere strutture interne che favoriscono alcuni candidati o tendenze politiche rispetto ad altre. Questo può limitare le scelte degli elettori, offrendo loro solo alcune opzioni approvate dai partiti politici.

### Delega di potere

Le elezioni sono due cose: un voto e una delega di potere.

Come già detto, la nostra democrazia è la ricorrente abdicazione del potere legittimo dal popolo a un gruppo di persone.

La delega di potere può anche portare a una perdita di controllo sulle decisioni. Se una persona o un gruppo delega il proprio potere a un'altra persona o gruppo, può perdere il controllo su quali decisioni vengono prese e come vengono attuate.

Con il pretesto della delega, che conferisce piena legittimità ai rappresentanti eletti, possono verificarsi appropriazione indebita, alienazione, spoliazione o usurpazione del potere.

Uno dei problemi principali della delega di potere è il rischio di esautorazione. Se una persona o un gruppo delega il proprio potere decisionale a un'altra persona o gruppo, può esimersi dalla responsabilità in caso di fallimento o errore da parte della persona o del gruppo a cui ha delegato il proprio potere.

Un altro problema della delega di potere è il rischio di conflitto di interessi.
Alcune operazioni richiedono tempo, energia, conoscenze pregresse e capacità intellettuali. In questi casi, può essere vantaggioso, o addirittura necessario, utilizzare la divisione del lavoro e la specializzazione delegando parte del lavoro decisionale. Ad esempio, un governo può delegare il proprio potere a un'azienda per la gestione di un progetto. Ma poiché il delegato non sa esattamente cosa sta delegando, per definizione non dispone di tutte le informazioni sulle possibilità. Questo lascia la porta aperta a ogni tipo di abuso[^2].

### Centralizzazione del potere

Il potere è centralizzato quando pochi decidono molto e molti decidono poco.

La centralizzazione aumenta la delega di potere, che a sua volta riduce la democrazia.

Quando un piccolo gruppo di persone detiene il potere assoluto, questo può portare a decisioni unilaterali e a politiche inique che non tengono più conto degli interessi e delle opinioni dei cittadini.

La centralizzazione del potere può rendere i governi più vulnerabili all'abuso di potere, alla tirannia e alla dittatura, poiché ci sono meno controlli e contrappesi per limitare l'autorità di un piccolo gruppo di persone.

### Sistemi di voto

È importante capire che un candidato eletto viene eletto tramite una votazione e che cambiando la votazione cambierà l'eletto.

Il processo elettorale influisce notevolmente sull'esito del voto. Pertanto, la scelta della scheda elettorale influisce sul futuro delle nostre società. È quindi importante concentrarsi sulla scelta della scheda elettorale che vogliamo mettere in atto.

Le elezioni a turno unico e a due turni hanno un problema fondamentale: non consentono di esprimere le preferenze degli elettori in modo chiaro e preciso[^11].

In un sistema a turno unico, l'elettore può votare solo per un candidato, il che può essere problematico se diversi candidati hanno un sostegno significativo. Ad esempio, in un'elezione in cui ci sono tre candidati principali, l'elettore deve scegliere tra tre diverse opzioni, ma ha a disposizione un solo voto per esprimere la sua scelta. Questo può portare a situazioni in cui il candidato eletto non ha il sostegno della maggioranza degli elettori, ma semplicemente il maggior numero di voti.

Il problema delle elezioni a doppio turno è che a volte possono portare a tattiche di voto strategiche e alleanze tra i candidati. In un sistema a doppio turno, i due candidati che hanno ottenuto il maggior numero di voti al primo turno si affrontano in un secondo turno. Questo può portare a situazioni in cui gli elettori votano in modo diverso al primo turno per assicurarsi che il loro candidato preferito si qualifichi per il secondo turno, anche se non sono realmente convinti di quel candidato. Inoltre, possono verificarsi casi in cui i candidati uniscono le forze per spingere un altro candidato fuori dalla corsa, piuttosto che cercare di vincere le elezioni in modo indipendente.

In generale, il voto a maggioranza ha il grave inconveniente di non tenere conto dell'intensità delle preferenze delle persone. Coloro che sono quasi indifferenti tra due alternative e coloro che ne preferiscono una molto più dell'altra hanno lo stesso peso nella scelta tra di esse. Ad esempio, 1001 cittadini che preferiscono a malapena un'alternativa superano gli altri 1000 per i quali si tratta della peggiore catastrofe.

Questi sondaggi incoraggiano il voto utile, che spinge i politici a organizzarsi in partiti politici e a collocare gli elettori in caselle predefinite, rafforzando l'effetto di bipolarizzazione.

La bipolarizzazione dell'elettorato in politica si riferisce alla tendenza degli elettori a raggrupparsi intorno a due partiti politici principali, creando una netta divisione politica tra questi due partiti. Ciò lascia poco spazio ai partiti o ai candidati indipendenti o ai partiti politici più piccoli. Ciò ha anche la conseguenza negativa di portare a una maggiore polarizzazione dell'opinione, a una riduzione della diversità dei punti di vista.

Con queste schede si finisce per votare contro un candidato e non per lui.

Ma un voto utile non rende legittima la persona eletta.

La rappresentanza proporzionale fa sì che la politica non sia troppo polarizzata, ma può portare a una frammentazione del potere e rendere difficile la formazione di governi stabili. Diventa difficile per un partito conquistare la maggioranza assoluta dei seggi, il che può portare a coalizioni di governo instabili o a governi di minoranza che hanno difficoltà a portare avanti il loro programma.

Una buona elezione deve essere indipendente da alternative irrilevanti e chiudere la porta al dilemma del voto utile.

Esempio di un possibile voto alternativo:

Voto di approvazione: ogni elettore può approvare tutti i candidati che desidera, senza ordine di preferenza. Il candidato con il maggior numero di approvazioni viene eletto.

Il voto a punti (o voto ponderato) è un sistema elettorale alternativo in cui ogni elettore dispone di un certo numero di punti che può distribuire tra i candidati.

Voto a maggioranza: gli elettori assegnano a ciascun candidato un punteggio, ad esempio da "molto buono" a "bocciato". Viene eletto il candidato con il punteggio medio più alto.

Il voto Condorcet è un sistema di voto in cui gli elettori votano confrontando tutti i candidati a due a due[^12].

Voto Condorcet randomizzato: in questo caso ogni elettore confronta coppie di candidati come nel voto Condorcet, ma invece di prendere in considerazione tutti i voti di tutti gli elettori, un numero di confronti viene selezionato a caso per determinare il vincitore.

Voto Mehestan (Girasole): è un sistema di voto che combina il voto a punti e il voto Condorcet. Consiste nel confrontare ogni candidato uno per uno e dire quale si preferisce per ogni criterio, ma anche definire il livello di preferenza per quel criterio. Questo metodo di voto mira a essere equo e rappresentativo, consentendo agli elettori di votare per il loro candidato preferito senza il rischio di dividere il voto e tenendo conto delle preferenze di tutti gli elettori nel determinare il vincitore.

### Partiti politici

I sistemi di voto sopra descritti incoraggiano i politici a organizzarsi in partiti, a causa della loro dipendenza da alternative irrilevanti.

### Lentezza istituzionale

In molti casi, i problemi politici richiedono soluzioni rapide ed efficaci per minimizzare danni e perdite. Tuttavia, la lentezza dei processi politici, come negoziati, dibattiti, votazioni e burocrazia, può ritardare il processo decisionale e impedire ai governi di agire rapidamente.

### Mandati a breve termine

I rappresentanti eletti in questione vengono eletti per un certo periodo di tempo, di solito qualche anno. Durante questo periodo, hanno una sostanziale libertà personale di scegliere le proprie azioni. Gli elettori non li vincolano più in alcun modo. Ben poco obbliga gli eletti a mantenere le promesse elettorali, a parte l'onore, ma possono benissimo dire che le cose sono cambiate.

I rappresentanti eletti sono spesso più inclini a concentrarsi sui risultati a breve termine piuttosto che sulle sfide a lungo termine.

Questo perché i mandati incoraggiano i rappresentanti eletti a concentrarsi su questioni politiche che hanno maggiori probabilità di attirare il rapido sostegno degli elettori, piuttosto che su questioni più complesse che richiedono un impegno maggiore e a lungo termine.

I politici possono essere tentati di prendere decisioni popolari nel breve periodo per massimizzare le loro possibilità di rielezione. Questo può portare a politiche che non sono praticabili o sostenibili a lungo termine e che possono causare maggiori problemi in futuro. Questo porta a un approccio alla governance di tipo politico piuttosto che a un approccio più pragmatico e ponderato.

### Bluffare per ottenere competenze

Il "bluff della competenza" in politica si verifica quando i politici sfruttano la loro posizione di laureati in materie scientifiche o amministrative per dare l'impressione di avere competenze o conoscenze speciali su argomenti che in realtà non hanno. Possono usare questa strategia per manipolare o persuadere gli elettori, ma può portare a decisioni politiche sbagliate.

L'enfasi sulle lauree scientifiche e amministrative porta a una concentrazione di potere e influenza nelle mani di poche élite che hanno avuto la fortuna di accedere a questi corsi. Ciò comporta una perdita di diversità e di inclusione nei circoli decisionali, poiché le persone provenienti da contesti meno privilegiati hanno spesso minori opportunità di ottenere queste lauree.

Tuttavia, il possesso di una laurea non garantisce necessariamente capacità o competenze nella governance o nella politica. Il bluff della competenza, di cui abbiamo parlato in precedenza, può essere usato da persone che hanno una laurea ma non hanno le reali conoscenze o competenze per prendere decisioni informate[^2].

### La scienza disprezzata

Le decisioni politiche devono spesso essere prese sulla base di dati scientifici complessi e di prove empiriche, in particolare in settori come la salute pubblica, il cambiamento climatico, la sicurezza alimentare e così via. Quando i politici ignorano le prove scientifiche, prendono decisioni che non si basano su fatti e dati verificabili, ma piuttosto su convinzioni o opinioni. Questo può avere gravi conseguenze per la società, soprattutto in termini di salute, ambiente e sicurezza.

### Propaganda

Gli elettori non votano per un candidato di cui non sanno nulla. Senza informazioni sul candidato trasmesse agli elettori, un candidato non ha alcuna possibilità. Il candidato inizia ad avere una possibilità solo quando le informazioni ricevute superano una certa quantità: le sue possibilità di essere eletto aumentano quando aumentano le informazioni ricevute.

L'investimento politico è un investimento come un altro, la ricerca è sempre la stessa: il profitto.

La storia dell'affermazione della "democrazia elettorale" è parallela alle lotte e alle vittorie formali per la libertà di espressione, di stampa, di riunione e di associazione. Ma queste libertà, a causa della loro necessaria base materiale ed economica, sono state per la maggior parte acquisite da coloro che detengono queste risorse economiche. E quindi di usarle per la propaganda elettorale[^24].

La propaganda viene utilizzata per manipolare l'opinione pubblica e influenzare i risultati di elezioni o referendum a favore di un particolare partito o ideologia. Può essere fuorviante, di parte e basata su bugie, mezze verità o omissioni di fatti importanti. Può anche fare leva sulle paure, sui pregiudizi e sulle emozioni delle persone piuttosto che sulla ragione e sulla logica.

### Il teppista politico

Possiamo distinguere 3 tipi caricaturali di cittadini durante le elezioni:[^13][^14]

- Gli hobbit: coloro che non hanno interesse per la politica e sono molto ignoranti in questo campo, non hanno troppi pregiudizi.
- Gli hobbit: sono prevenuti, ma molto prevenuti, spesso sono vittime di bias di conferma e difendono i valori in modo irrazionale.
- Vulcaniani: persone capaci di pensiero razionale.

I nostri modelli presuppongono che tutti i cittadini siano o possano diventare vulcaniani, ma in pratica la maggior parte delle persone sono hobbit o hooligan.

L'intuizione ci costringe a formarci un'opinione e la ragione viene usata per sostenerla. Razionalizziamo le nostre convinzioni.

Non usiamo la ragione per scoprire la verità, ma per giustificare le posizioni ideologiche che abbiamo già adottato.

Per esempio, se pronunciamo la parola "democrazia", la nostra intuizione ci dice che è una cosa buona, e poi la nostra ragione manipola la definizione della parola per giustificare la nostra intuizione.

Al giorno d'oggi, è facile confermare la nostra intuizione grazie a Internet, che può fornirci tutti gli argomenti di cui abbiamo bisogno, e se questi argomenti o dati non vanno nella nostra direzione, possiamo sempre mettere in dubbio le motivazioni di chi ha riportato i fatti o interpretarli in modo diverso.

Inoltre, circondandoci di persone che la pensano come noi, il nostro ambiente rafforza costantemente le nostre intuizioni e tende a polarizzarle.

Gli attivisti politici diventano troppo sicuri di sé e diventa impossibile far loro cambiare idea anche con fatti e cifre.

E quando l'intuizione predomina in questo modo, più ci si informa, più si trovano buone ragioni per credere in ciò che si crede, indipendentemente dalle informazioni che si leggono.

L'impegno politico non fa che rafforzare il teppismo politico.

Più ci interessiamo a un partito politico con un prezzo, più la nostra inclinazione iniziale diventerà radicale.

Video e articoli scientifici fuorvianti ma ben spiegati rafforzano la convinzione scientifica di chi li guarda e li legge.

Poiché la selezione di gruppo è più efficace di quella individuale, le tribù che sono sopravvissute sono quelle in cui gli individui hanno cooperato a sufficienza per garantire la sopravvivenza del gruppo.

E quindi coloro che sono disposti a sacrificare i propri interessi individuali per il bene del gruppo. La selezione naturale ha selezionato il teppismo politico.

Un voto basato sul teppismo politico offre divertimento, mentre un voto razionale richiede uno sforzo mentale.

### Ignoranza e irrazionalità dei cittadini

I cittadini sono ignoranti, quasi tutti gli elettori non hanno alcuna nozione di scienza o comprensione del sistema politico, sono male informati e non conoscono le cifre[^16].

Alcuni di loro non sanno di essere coinvolti in atti di teppismo politico.

La maggior parte di noi non è consapevole dei diversi pregiudizi cognitivi e dei loro effetti. Ne abbiamo analizzati alcuni nel Capitolo 3 sull'HomoSapiens. Eccone alcuni come promemoria:

- Pregiudizio di conferma[^17]
- L'effetto alone[^18]
- Dissonanza cognitiva[^19]
- Pressione di coppia
- Rischio di disponibilità [^20]

La scuola e i laureati possono far credere di essere degli studiosi, far credere di essere più intelligenti degli altri e creare un'eccessiva fiducia in ambiti che non sono i propri.

Tutti diventano così sicuri di sé. Il problema è che l'incertezza viene vista come un segno di debolezza, mentre in realtà è una qualità fondamentale.

L'effetto Dunning-Kruger dimostra che le persone con scarse conoscenze o competenze in un campo tendono a sopravvalutare il proprio livello di padronanza, mentre le persone con competenze più elevate tendono a sottovalutare il proprio livello di padronanza[^16].

Ma l'ignoranza dei cittadini non è il problema principale, perché anche informare i cittadini non impedisce loro di votare in modo irrazionale.

Le persone si comportano come teppisti, non come scienziati.

### Tempo ed energia per informarsi

Essere informati, imparare, conoscere, capire, poi scegliere e decidere, e votare, richiede tempo, energia e conoscenze pregresse da parte dell'elettore, che a sua volta richiede tempo e fatica. Questi costi spiegano una certa delega di potere.

Le campagne elettorali sono spesso brevi e intense, lasciando agli elettori poco tempo per informarsi e valutare le varie opzioni. Inoltre, i media tendono spesso a concentrarsi sugli aspetti più spettacolari o controversi degli eventi politici, piuttosto che fornire analisi approfondite e sfumate.

Questo può portare a una polarizzazione della politica e a una riduzione della qualità del dibattito pubblico. Gli elettori possono essere tentati di rivolgersi a fonti di informazione semplicistiche o di parte che non riflettono necessariamente la complessità delle questioni.

### Comunitarismo

Il comunitarismo può essere definito come la tendenza ad attribuire maggiore importanza all'appartenenza a un gruppo o a una comunità piuttosto che all'individuo in quanto tale. Il problema principale del comunitarismo è che può portare a una frammentazione della società, dove ogni gruppo si concentra sui propri interessi piuttosto che sull'interesse comune della società nel suo complesso[^21].

Un tempo il comunitarismo era geografico, ma ora si sta diffondendo in tutta Internet.

A nessuno piace essere in minoranza.

Quando gli utenti di Internet smettono di partecipare a una discussione perché si sentono troppo minoranza, questo porta alla segregazione ideologica.

Cambiare quartiere ideologico su Internet è molto più facile che spostarsi geograficamente.

Le persone possono lasciare le comunità in cui si sentono in minoranza per entrare in quelle in cui sono in maggioranza.

Le filter bubbels sono ciò che chiamiamo isolamenti ideologici sul web.

Ci chiudiamo in un mondo in cui tutto ciò che leggiamo e sentiamo conferma ciò che già pensiamo, il che rende difficile mettere in discussione le nostre convinzioni e capire perché gli altri pensano ciò che pensano.

Queste bolle di filtraggio sono amplificate dai contenuti personalizzati offerti dai giganti del web: Facebook, YouTube, ecc. Ci chiudono nella comunità con cui condividiamo le nostre idee, offrendoci contenuti personalizzati. Ogni volta che clicchiamo su un link o mettiamo "mi piace" a un post, ci chiudono ulteriormente in una comunità con cui condividiamo le nostre idee[^22].

### Convinzioni

Le convinzioni possono essere definite come convinzioni radicate o opinioni ferme su un argomento. Sebbene le convinzioni possano essere utili, in genere sono più problematiche che altro, soprattutto nel campo della politica.

Uno dei problemi principali delle convinzioni è che possono portare alla polarizzazione e alla divisione. Se le persone hanno convinzioni forti su un argomento, possono essere meno inclini a considerare altri punti di vista o a lavorare con persone che hanno convinzioni diverse. Questo può portare alla polarizzazione e alla divisione della società[^23].

Possono portare alla chiusura mentale. Se le persone sono troppo attaccate alle proprie convinzioni, possono essere meno inclini ad ascoltare opinioni o fatti che le contraddicono. Questo può portare a prendere decisioni basate sui pregiudizi piuttosto che sui fatti,

Le convinzioni possono portare alla resistenza al cambiamento. Se le persone sono fermamente attaccate alle loro convinzioni, possono essere meno inclini ad accettare nuove idee o cambiamenti nella società.

## Soluzioni ai problemi delle nostre democrazie

### L'imprenditore politico

La soluzione all'imprenditore politico è abolire questa funzione o almeno limitarla aumentando la partecipazione dei cittadini.

L'imprenditore politico non dovrebbe più essere qualcuno a cui viene delegato il potere e che, dal momento in cui lo acquisisce, può farne ciò che vuole. Ma dovrebbe avere una funzione rappresentativa e, non appena i cittadini che gli hanno delegato il potere sentono che non li rappresenta più, dovrebbero poterlo licenziare.

È necessario garantire una trasparenza totale, in modo che i cittadini sappiano esattamente a chi stanno delegando il loro potere e cosa ne fanno.

E per una buona diversità, dobbiamo garantire che i candidati provengano da ambienti diversi e abbiano le stesse opportunità degli altri di accedere a queste posizioni.

### Programmi politici

Una soluzione al problema dei programmi politici potrebbe essere quella di renderli più modulari e più specifici per ogni argomento, invece di presentarli come un pacchetto complessivo. Ciò consentirebbe agli elettori di scegliere separatamente le proposte che preferiscono su ciascun tema, anziché dover accettare o rifiutare l'intero programma.

### Onestà e trasparenza

In una democrazia, è importante che il processo decisionale sia trasparente e responsabile, e che chi detiene il potere sia ritenuto direttamente responsabile delle proprie azioni. La delega di potere deve essere usata con cura e i rischi associati devono essere presi in considerazione per garantire la tutela degli interessi dei cittadini.

### La scelta delle alternative

È importante offrire all'elettore una scelta ampia e completa di alternative e una pari quantità di visibilità e di informazioni su di esse. I programmi completi limitano ancora una volta questa possibilità, mentre gli argomenti potrebbero essere separati per consentire decisioni più specifiche.

### Delega di potere

Gli elettori devono poter scegliere se delegare o meno il proprio potere. Dovrebbero anche poter ritirare e revocare la delega di potere in qualsiasi momento.

### Centralizzazione del potere

In una democrazia, la distribuzione del potere è essenziale per garantire che gli interessi e le opinioni dei cittadini siano rappresentati e presi in considerazione nel processo decisionale. Ciò implica la partecipazione attiva dei cittadini, il decentramento del potere e la creazione di meccanismi di responsabilità e trasparenza per limitare il potere dei leader eletti.

### Sistemi di voto

Dobbiamo favorire sistemi di voto che tengano conto dell'intensità delle preferenze dei cittadini ed evitare quelli che incoraggiano il voto utile.

### Lentezza istituzionale

I referendum rapidi dovrebbero essere introdotti per le emergenze. Più in generale, i cittadini dovrebbero poter esprimere le proprie opinioni con maggiore regolarità.

### Mandati a breve termine

Esistono diverse possibilità, tra cui l'introduzione di mandati più lunghi con l'introduzione di una procedura che dia agli elettori il diritto permanente di richiamare i propri rappresentanti eletti. Possiamo anche rendere i cittadini più consapevoli del lungo periodo e motivare i politici a pianificare per il lungo periodo. Potremmo anche limitare i mandati o addirittura abolire il ruolo dell'imprenditore politico.

### Bluffs to expertise

È importante riconoscere che le competenze e l'esperienza non sono esclusive delle qualifiche accademiche e che l'inclusione di persone provenienti da contesti diversi può apportare una ricchezza di prospettive che può essere utile alla governance e alla politica.

### La scienza disprezzata

Coinvolgere gli scienziati nei processi decisionali: gli scienziati possono contribuire a informare le decisioni politiche fornendo dati, analisi e valutazioni indipendenti e basati su prove.

Aumentare la trasparenza e la responsabilità dei dati e delle prove scientifiche che utilizzano per prendere decisioni politiche, e rendere conto delle loro decisioni e delle loro conseguenze. Incoraggiare una comunicazione efficace tra scienziati e responsabili politici, comunicando le loro scoperte in modo chiaro e conciso, evitando il gergo tecnico. Promuovere una cultura di rispetto per la scienza. Educare i politici alla scienza: i politici dovrebbero essere formati sul metodo scientifico e sui concetti scientifici chiave, per comprendere meglio i dati e le prove scientifiche.

Stabilire in una costituzione i principi da tenere in considerazione, in modo che le decisioni rispettino i principi inalienabili su cui c'è consenso, ad esempio per garantire la felicità umana e l'equilibrio dei nostri ecosistemi sulla Terra.

### Propaganda

Per ridurre l'impatto della propaganda in politica, è importante incoraggiare un'informazione libera e trasparente, basata su fatti e dati verificabili. I media devono svolgere un ruolo chiave in questo processo, fornendo una copertura equilibrata e imparziale degli eventi politici, verificando le affermazioni politiche e promuovendo un fact-checking indipendente. È inoltre importante sensibilizzare l'opinione pubblica sulle tecniche di propaganda e di manipolazione dell'opinione, educandola a riconoscere e a resistere a queste tattiche.

### Cittadino ignorante

Un'istruzione di qualità può aiutare i cittadini a diventare elettori più informati. È importante insegnare agli studenti le capacità di pensiero critico, ragionamento logico, ricerca di informazioni affidabili e comprensione delle questioni politiche.

Trasparenza e accessibilità: le politiche e le decisioni pubbliche devono essere trasparenti e facilmente accessibili, in modo che i cittadini possano capire meglio come funziona il loro governo e quali decisioni vengono prese.

Comunicazione: è importante che i leader politici comunichino in modo chiaro e regolare con i cittadini, utilizzando un linguaggio semplice ed evitando il gergo tecnico.

Partecipazione: incoraggiare la partecipazione dei cittadini ai processi politici, come elezioni, petizioni, dibattiti pubblici, consultazioni, sondaggi d'opinione, ecc. può contribuire ad aumentare il coinvolgimento dei cittadini nella vita politica e a rafforzare la loro comprensione delle questioni politiche.

### Mancanza di tempo

Il problema del tempo può essere risolto dando ai cittadini più tempo riducendo il loro orario di lavoro. Un'altra possibilità è quella di avere un algoritmo che ogni volta selezioni accuratamente un campione rappresentativo della società per evitare che tutti debbano votare.

### Il teppista politico

Fate attenzione a leggere i programmi senza teppismo politico e incrociateli con le ricerche economiche e sociologiche. Chiedetevi se stiamo seguendo la nostra intuizione, che la nostra ragione avrebbe poi supportato con argomenti.

La presa di coscienza di questo teppismo porterà a dibattiti migliori e fornirà un modo di pensare che non è bloccato dall'intuizione e che ci permette di uscire dal nostro modo di pensare da teppisti politici. Dobbiamo apprezzare tutto ciò che è controintuitivo, apprezzare il fatto di convincere la nostra intuizione che la nostra intuizione è sbagliata.

Dobbiamo apprezzare il fatto che ci sbagliamo, che possiamo metterci in discussione e capire che è sbloccando la nostra intuizione che possiamo fare i maggiori progressi.

### Comunitarismo

Dobbiamo lottare contro il desiderio di ognuno di far parte della maggioranza.
Dobbiamo poi lottare affinché far parte della minoranza non sia un'esperienza imbarazzante.

Dobbiamo fare attenzione a non dare alla maggioranza il sopravvento solo perché è la maggioranza.

Bisogna rendere piacevole l'esperienza di essere in minoranza.

Fare in modo che le persone che sono in maggioranza vogliano essere in minoranza.

Fate in modo che tutti cerchino di capire e di mescolarsi.

### Convinzioni

In una democrazia, è importante che le persone siano aperte e disposte ad ascoltare le opinioni e i punti di vista degli altri. I cittadini devono essere sensibilizzati ed educati al riguardo.

### Conclusione delle soluzioni

La democrazia presenta molti problemi.

Per funzionare bene, ha bisogno di condizioni ideali.

Buoni mezzi di comunicazione per essere ben informati e perché i politici al potere agiscano davvero nell'interesse del popolo.

Ma il problema è che la nostra democrazia sta creando un contesto in cui accade esattamente il contrario.

La gente o è un hobbit, non si interessa di politica, è molto ignorante in questo campo, non ha troppi pregiudizi, oppure è un hooligan, ha un pregiudizio, ma molto preso, e spesso è vittima del bias di conferma, arriva a difendere i valori in modo irrazionale.

Abbiamo bisogno di vulcani, di persone che sappiano pensare razionalmente.

E al momento non è possibile avere discussioni razionali tra persone politiche in una democrazia con rappresentanti eletti che vogliono essere eletti.

Se vogliamo un cambiamento radicale della democrazia, dobbiamo assolutamente prendere in considerazione tutti questi elementi razionali, come il principio degli hooligans politici, la segregazione comunitaria, ecc.

Ma la democrazia è una scommessa che vale la pena di fare, perché con essa ci sentiamo tutti uguali, offre capitale morale e ci federa per cooperare.

## Idea sperimentale per una nuova democrazia profonda (V1)

Nome: E-Democrazia partecipativa condivisa tramite referendum spontanei supportati da competenze e saggezza.

Per consentire una partecipazione più ampia e diretta dei cittadini, stiamo utilizzando le tecnologie dell'informazione e della comunicazione per creare uno strumento digitale che faciliterà e migliorerà i processi democratici.

Questo strumento è una piattaforma web, disponibile anche come applicazione mobile. Si può pensare a un wiki, sul quale è possibile accedere alle informazioni relative ai diversi livelli della società in cui si è coinvolti, come le leggi, le modalità di organizzazione, le decisioni attuali, ecc. Questi livelli sono suddivisi e strutturati in categorie e sottocategorie, a seconda della natura del livello.

Chiunque può suggerire modifiche alle informazioni inserite. Poiché è impossibile per ogni essere umano leggere tutte le proposte che potrebbero essere fatte dai 7 miliardi di esseri umani. Utilizziamo un algoritmo che prende un campione che rappresenta tutte le persone collegate alla proposta. La proposta viene quindi inviata a questo campione rappresentativo. Le persone possono votare a favore, contro o astenersi e se i voti favorevoli sono più del 50%, la proposta passa al livello successivo.

La proposta viene poi inviata a un consorzio di esperti e saggi che la studiano per dare un parere approfondito, verificarne l'aspetto tecnico e scientifico, stimare le risorse e il tempo da destinare, ma anche e soprattutto controllare che sia in linea con le costituzioni stabilite.

La costituzione di Eutopia getterà le basi della società, che dovrà essere studiata e giustificata in quanto ci permetterà di raggiungere il nostro obiettivo comune di vivere una vita appagante e sostenibile in armonia con la Terra. Dovrà poi essere insegnata per risvegliare le persone, per far sì che guidi le loro proposte e decisioni.

Una volta completato il lavoro degli esperti e dei saggi, i loro pareri saranno allegati alla proposta finale, in modo che i cittadini abbiano tutte le chiavi di lettura necessarie per fare la loro scelta. Le persone nominate come esperti o saggi saranno elette dai cittadini delle rispettive aree. Dovranno essere garantite la trasparenza e la veridicità delle informazioni fornite in questi pareri, nonché la semplicità delle informazioni, il tutto per consentire ai cittadini di avere tutte le carte in regola per fare la loro scelta ed esercitare il loro potere con la coscienza pulita.

La proposta sarà poi inviata nuovamente a un gruppo campione con le informazioni e i pareri degli esperti e dei saggi. Se la proposta viene nuovamente approvata, viene inviata all'intera popolazione per l'adozione definitiva. Il risultato dovrebbe essere lo stesso del campione. Tuttavia, quando i cittadini riceveranno la proposta per il voto finale, sapranno in anticipo che più della maggioranza intende accettarla, il che ha il vantaggio di dare loro un'ultima possibilità di ritirarsi.

Quindi il cittadino non riceve tutte le proposte, ma solo quella che lo riguarda, a seconda che la decisione sia applicata a livello locale o globale.

Prima di inviare la vostra proposta, abbiamo anche istituito un sistema di collaborazione che vi consigliamo vivamente di utilizzare: la vostra proposta è quindi prima di tutto un suggerimento, accessibile a chiunque lo desideri e a chiunque possa parteciparvi.

Potete anche delegare il vostro potere a qualcuno, che voterà la proposta al posto vostro; avrete comunque accesso alla cronologia delle posizioni votate per verificare che le condividiate e potrete ritirare la vostra delega in qualsiasi momento. Potete anche scegliere il livello di potere che desiderate delegare in relazione ai diversi livelli della società.

Infine, è questo sistema democratico che useremo anche per l'editing collaborativo delle storie. O prima, mentre testiamo il nostro sistema, possiamo iterarlo e modificarlo fino a quando non ne saremo pienamente soddisfatti, e alla fine sarà questa versione finale a costituire la democrazia di Eutopia.

> "La democrazia in questo orizzonte sembra richiedere tempo, coraggio, perseveranza... e forse un pizzico di follia..." Datageueule[^4]

## Fonti (elenco non esaustivo)

[^1]: [Wikipedia : Democrazia ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. Le elezioni sono democrazia? 1977
[^3]: David Shearman. La sfida del cambiamento climatico e il fallimento della democrazia (Politics and the Environment). 2007.
[^4]: [Datageule. Democracy(s) ?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All : Il principio fondamentale della politica](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno De Mesquita, Alastair Smith, Randolph M. Siverson. La logica della sopravvivenza politica (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Previsione: come vedere e modellare il futuro con la teoria dei giochi. 1656
[^8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Foster onestà | Democrazia 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja e Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail. 2015
[^11]: [Science4All. Our Democracies Divide | Democracy 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. Il voto a Condorcet randomizzato | Democrazia 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. Sei un teppista politico? Democrazia 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion. 2013
[^15]: [Science4All. Rationally Irrational | Democracy 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. Il mito dell'elettore razionale: perché le democrazie scelgono politiche sbagliate. 2008
[^17]: [The Bias Work #5: Confirmation Bias](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [AmazingScience - L'effetto alone - Cervello stupido #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [The Bias Bracket #3- Cognitive Dissonance](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Accademia Franklin Templeton - Pregiudizi sulla disponibilità](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. Il piccolo comunitarismo diventerà grande | Democrazia 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [Conferenza TED. Eli Pariser mette in guardia dalle "bolle di filtraggio" online](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Cara convinzione, muta in un'infezione VIRALE!!! Democrazia 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Concentrazioni mediatiche: i miliardari vi informano. 2016
