---
title: Tempo di lavoro
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Contenuto in fase sperimentale e in corso di scrittura.
> {.is-danger}

Lavoro, dalla parola travalhum, deriva da tripalium, un treppiede usato come strumento di tortura. Ma il lavoro non è condannato a essere la tortura della sua origine etimologica.

Per molti di noi, la natura e la durata del lavoro sono inevitabilmente ripetitive, noiose e rovinano la vita.

La vita sembra vuota e siamo delusi quando siamo consumati da un lavoro che non le dà alcun significato.

Ma la società non ci permette di realizzare i nostri veri desideri. Perché l'insoddisfazione che potrebbe portarci a mettere in discussione questa vita e a chiederne un'altra è incanalata dal sistema che soddisfa i nostri bisogni intrinseci e i bisogni estrinseci che essi generano, impedendoci allo stesso tempo di realizzare ciò che ci manca.

Dove il lavoro dovrebbe permetterci di mettere al mondo una creazione che porti la nostra impronta, di esprimere noi stessi e di dare un senso collettivo alla nostra vita.

Tutte le dimensioni del lavoro faticoso possono e devono essere cancellate o ridotte. Il lavoro fisicamente o psicologicamente estenuante, ottundente o scemante, troppo duro, meccanico, monotono, troppo sporco o sgradevole, troppo privo di significato, sprezzante, ecc. Deve essere rimosso, sostituito, limitato, automatizzato, migliorato, arricchito, compensato, premiato, valorizzato o meglio condiviso.

L'idea è quella di trasformare la natura del lavoro in un'attività che sia qualcosa di simile all'autorealizzazione, alla manifestazione della propria umanità più profonda in armonia con la natura. In questo modo, il lavoro diventerebbe il "primo bisogno della vita". In questo modo, l'espressione "lavorare per vivere" non significherebbe più affrontare la fatica di lavorare per sopravvivere e mantenersi, ma guadagnarsi effettivamente da vivere. ^1]

## Riduzione dell'orario di lavoro

Dimostreremo che il modello economico della reciprocità consente di ridurre notevolmente il tempo di lavoro necessario al buon funzionamento della società, fino a una quota di 2 o 3 ore per persona[^2][^8].

La produttività è il rapporto tra la produzione e il numero di ore di lavoro necessarie per ottenerla.

La produttività è aumentata in modo significativo negli ultimi secoli.

In primo luogo, con l'avvento della rivoluzione industriale nel XVIII e XIX secolo, che ha visto la meccanizzazione dei processi produttivi e l'introduzione di nuove tecnologie come il vapore e l'elettricità.

Da allora, la produttività ha continuato ad aumentare a un ritmo variabile, ma costante, grazie allo sviluppo di nuove tecnologie e sistemi di produzione.

Può essere difficile trovare dati affidabili sulla crescita della produttività in Francia per più di 100 anni. I metodi di misurazione della produttività si sono evoluti nel tempo, il che può rendere difficile il confronto dei dati su lunghi periodi.

Ad esempio, tra il 1950 e il 2020, la produttività del lavoro in Francia è aumentata in media di circa il 2% all'anno. Ciò significa che un lavoratore medio in Francia può produrre circa il doppio di beni e servizi in una giornata lavorativa rispetto al 1950[^3].

Tuttavia, nello stesso periodo la settimana lavorativa media non è diminuita del 50%, ma del 12,5%, passando da 40 ore nel 1950 a circa 35 ore nel 2020.

In questo caso è stata fatta una scelta di civiltà: utilizzare le possibilità aperte dal prodigioso sviluppo della tecnologia negli ultimi secoli non per ridurre le ore di lavoro, ma per aumentare la produzione e i profitti che ne derivano.

Ciò deriva dalla base stessa del capitalismo, dalla crescita da cui dipende la sua sopravvivenza, dal desiderio di profitto dell'imprenditore e dal desiderio di accumulo del consumatore, che si combina con un sempre maggior numero di prodotti.

L'apparato produttivo deve quindi essere mantenuto a pieno regime. L'obiettivo è vendere sempre di più e moltiplicare le opportunità di profitto.

Questo porta a un'assurdità nel modo di produrre e consumare beni e servizi.

Ma la riduzione dell'orario di lavoro è nemica della nostra società, perché il tempo libero può essere utilizzato per la discussione, per l'organizzazione di base e per inventare forme di vita collettiva. Il potere non si troverebbe più di fronte a individui anonimi e isolati pronti ad accettare, ma a gruppi consapevoli e forti, più difficili da manipolare.

La riduzione dell'orario di lavoro significa quindi una riduzione della produzione? Con conseguenze catastrofiche per la nostra qualità di vita?

No, l'idea non è affatto quella di cambiare la nostra qualità di vita; al contrario, è quella di migliorarla continuamente per l'intera popolazione. Anche se la qualità della vita rimane un concetto del tutto relativo ai costumi individuali e sociali di un'epoca. Dimostreremo che è possibile realizzare risparmi significativi che, senza intaccare il nostro tenore di vita, ci permetteranno di ridurre notevolmente la produzione e quindi il tempo di lavoro.

È qui che scopriamo il potere della reciprocità: una volta usciti dal mercato, possiamo finalmente elaborare soluzioni efficaci per ridurre l'orario di lavoro senza che questo entri in conflitto con la nostra economia.

Perché le assurdità e le ingiustizie che riveleremo non sono frutto di errori o di maldestrezza; al contrario, sono necessarie per la sopravvivenza del capitalismo.

Ecco, nei punti seguenti, le varie soluzioni per ridurre l'orario di lavoro complessivo:

## Ridurre la produzione/il consumo

### Cambiare i nostri bisogni estrinseci

È difficile immaginare come, al di fuori di questo sistema di consumo che ci promette sempre di più, potremmo ottenere quel qualcosa in più che dà alla vita il suo colore e il suo sapore, proprio come gli oggetti che garantiscono la nostra identità. È infatti attraverso le cose che possediamo, i vestiti che indossiamo, i luoghi che ci sono familiari, che ci sentiamo abbastanza simili agli altri perché non ci rifiutino e abbastanza diversi perché possiamo essere sicuri di essere noi stessi.

Ma limitare i consumi ci permette di rivelare il nostro desiderio di amare e creare, di liberare il tempo per vivere che oggi manca tanto. Allora conosceremo la gioia di lasciar scorrere la vita, il cui mormorio ci dice cosa siamo, il piacere di amare quando non c'è fretta, il piacere di padroneggiare una tecnica tutta nostra, di creare oggetti noi stessi, sia che li intendiamo per firmare il nostro spazio, sia che li intendiamo come dono da riservare a chi amiamo.

Questa visione ci aiuta a capire perché l'idea di ridurre l'orario di lavoro nella nostra società incontra tanta opposizione, perché liberare il tempo che il lavoro occupa significa aprire una breccia nella camicia di forza in cui la società capitalista ha stretto le nostre vite.

La crescita viene mantenuta perché non potrà mai soddisfare i "bisogni" che vengono costantemente ricreati sulla base di una disuguaglianza che non viene corretta.

Questi bisogni sono desideri di status, appartenenza, autostima, potere o identificazione sociale. Sono creati dalla pubblicità, dal marketing e dalle pressioni sociali.

La pubblicità mira a convincere i consumatori che hanno bisogno di determinati prodotti o servizi per essere felici, soddisfatti o rispettati nella società. Utilizza immagini o messaggi che fanno leva sulle nostre emozioni, desideri e aspirazioni.

### Aumentare la durata di vita dei prodotti

Un'automobile, ad esempio, soddisfa un bisogno di autonomia personale, ma anche un bisogno estrinseco di acquisire il prestigio conferito dal possesso dell'ultimo modello o del modello top di gamma.

L'azienda automobilistica si assicura la redditività e quindi la sopravvivenza rinnovando costantemente il proprio prodotto, sviluppando il desiderio del consumatore di macchine sempre più sofisticate e costose, ma anche la sua fragilità e quindi la necessità di rinnovarsi.

Se confrontiamo le ore che un'auto ci fa risparmiare con le ore di lavoro che impieghiamo per pagarla, un lavoratore medio spende l'equivalente di 375 ore all'anno, ovvero 2 mesi di lavoro, per acquistare, mantenere, gestire e assicurare un'auto.

Le prime auto potevano essere guidate per 200.000-300.000 km, ma il "progresso" si è concentrato sulla produzione di auto più fragili. Questo è il prezzo della crescita, ma a spese del consumatore, mentre il produttore risparmia riducendo le caratteristiche e la qualità dei materiali, facendo grandi profitti sui pezzi di ricambio e rinnovando sempre più rapidamente le attrezzature.

L'auto deve essere progettata per durare 20 anni o più, forse anche fino alla rottamazione, ma questo punto sarà trattato più approfonditamente nel capitolo sui trasporti.

I monumenti del passato, cattedrali e castelli, ci mostrano le relazioni sociali e l'ideologia del loro tempo. I nostri monumenti sono queste macchine, la cui fantastica complessità riflette l'altezza delle nostre conoscenze scientifiche e del nostro know-how tecnologico, ma che sono obsolete appena nate e che saranno consegnate al macero dopo pochi decenni. Edifici e case costruiti in modo adeguato potrebbero durare più di un secolo.

Quanti elettrodomestici hanno dovuto essere riparati, spesso a breve distanza di tempo dall'acquisto, e troppo spesso la riparazione è troppo difficile o troppo costosa, tanto che l'elettrodomestico viene buttato via e sostituito con un altro.

Le prime calze erano indistruttibili, ma ora vengono immerse in un bagno acido dopo la produzione per ridurne la resistenza. L'azienda che ha inventato i tubi fluorescenti li ha immessi sul mercato solo dopo aver scoperto come ridurre il loro tempo di funzionamento a 1.000 ore. Le lame dei rasoi sono progettate per diventare smussate. Le lampadine sono attualmente progettate per durare solo 1.000 ore. Le stesse lampadine potrebbero durare 3.000 ore.

La maggior parte degli elettrodomestici potrebbe durare 30 anni.

Il deterioramento della qualità degli oggetti è evidente, ma è un segreto strettamente custodito dall'industria e difficile da dimostrare. Solo i produttori sanno quanto costerebbe loro produrre oggetti che durino 2 o 3 volte di più.

Questo è il paradosso: un prodotto troppo duraturo non può essere redditizio a lungo termine in una logica di mercato che punta al profitto monetario, ma lo è in una logica di reciprocità che punta a un minor lavoro.

### Ridurre gli sprechi

Nella nostra società produciamo molto di più di quanto abbiamo realmente bisogno, con un conseguente uso inefficiente delle risorse naturali e un accumulo di rifiuti.

Spreco alimentare: secondo la FAO, ogni anno circa un terzo del cibo prodotto nel mondo va perso o sprecato. Ciò può essere dovuto alla sovrapproduzione, a una cattiva gestione della catena di approvvigionamento, a rigidi standard estetici o a comportamenti dei consumatori come l'acquisto d'impulso o il consumo eccessivo.

Spreco di energia: tendiamo a consumare più energia di quella necessaria per le nostre esigenze quotidiane. Ad esempio, lasciando i dispositivi elettronici in stand-by, utilizzando mezzi di trasporto inefficienti o lasciando le luci accese inutilmente.

Spreco di materiali: la nostra società produce spesso beni monouso o di bassa qualità che vengono rapidamente gettati o sostituiti, anziché privilegiare beni durevoli e facilmente riparabili. Ciò comporta un uso inefficiente delle risorse naturali e un accumulo di rifiuti.

### Condividere i beni

Condividendo i beni, le persone possono ridurre i loro consumi e quindi la loro produzione, perché non hanno bisogno di acquistare tanti oggetti per soddisfare i loro bisogni. Ad esempio, più persone possono condividere un trapano piuttosto che acquistarne uno per uso occasionale. In questo modo si riduce la produzione di trapani.

Il 90% delle automobili è fermo, quindi dobbiamo unire i nostri sforzi per produrne di meno.

Inoltre, quando i beni sono condivisi, tendono a essere utilizzati e mantenuti meglio, il che allunga la durata di vita degli oggetti e quindi della nuova produzione. Quando le persone sono comproprietarie, i danni sono ridotti del 30%.

## Aumenta la produttività

### Utilizzare il progresso tecnico

Il progresso tecnico è spesso associato all'aumento della produttività, in quanto ci permette di produrre più beni o servizi con meno risorse, meno tempo e meno fatica. I progressi tecnologici hanno già migliorato l'efficienza e la qualità dei nostri processi produttivi e hanno contribuito a ridurre i costi di produzione.

È in corso una quarta rivoluzione industriale che coinvolge l'intelligenza artificiale, l'Internet degli oggetti, la robotica avanzata, la realtà aumentata, la blockchain, le nanotecnologie e le biotecnologie. Queste tecnologie trasformeranno davvero il nostro modo di produrre.

Tuttavia, dobbiamo fare attenzione che l'uso del progresso tecnologico non abbia conseguenze sull'impatto ambientale umano e sulla felicità interna lorda, che devono sempre contribuire a uno sviluppo sostenibile ed equo.

### Sfruttare al meglio gli orari di lavoro ridotti

Ogni lavoro, anche il più semplice, richiede un minimo di formazione, pratica e abilità. Quando le ore di lavoro diminuiscono, la produttività può aumentare solo fino a un certo punto, dopodiché diminuisce di nuovo.

Più si padroneggia, più si è produttivi, ma più si lavora a lungo, più ci si stanca e meno si è produttivi.

È possibile che per molte professioni che richiedono know-how questo punto venga superato con le "2 o 3 ore al giorno", motivo per cui queste 2 o 3 ore al giorno non devono essere prese alla lettera. Il lavoro di ognuno può essere organizzato in periodi più impegnativi e altri meno o totalmente liberi.

Ad esempio, 3 giorni di 8 ore alla settimana per 2 anni possono lasciare il posto a 3 anni di tempo libero.

## Rimuovere il lavoro

### Trasformare il lavoro vincolato in lavoro libero

Nella società odierna, come in tutte le società, ci sono lavori sgradevoli e altri piacevoli. In generale, i lavori noiosi e tediosi sono mal pagati e poco prestigiosi, mentre le attività più remunerative sono in genere quelle socialmente più apprezzate e interessanti per chi le svolge.

Esistono 2 tipi di lavoro.

Un lavoro duro e noioso, ma necessario per far funzionare la società nel suo complesso, che chiameremo "lavoro vincolato".

E il lavoro libero, che ha un significato per la persona che lo svolge, derivante da un'attività creativa che trova la sua giustificazione in se stessa. Naturalmente, questo può essere soggettivo e specifico per ogni individuo.

In realtà, 3/4 del lavoro svolto nella nostra società capitalista sfugge all'economia di mercato.

Con più tempo e una società migliore, possiamo affidarci a una rete di auto-aiuto per discutere e risolvere i nostri problemi quotidiani, discutere delle nostre diete, della nostra vita sessuale, delle nostre ansie nei confronti della vita, proprio come sta già iniziando a succedere su larga scala nei grandi gruppi di Facebook.

Gli elettrodomestici potrebbero essere progettati in modo da essere maneggevoli per gli utenti, facili da montare e smontare con strumenti standardizzati, cambiando modello quando il reale progresso tecnico lo rende necessario, ma garantendo la fornitura di pezzi di ricambio a prezzi ragionevoli per periodi molto lunghi, fornendo ai clienti istruzioni chiare e dettagliate, creando laboratori locali di assemblaggio e riparazione dove trovare gli strumenti necessari e scambiare aiuto ed esperienza.

Le migliorie alla casa possono essere eseguite dagli abitanti. Quello che oggi è un onere insopportabile, costruire la propria casa con un programma di lavoro così pesante. Tuttavia, con l'aiuto della comunità, questa attività potrebbe diventare un'attività creativa e divertente.

L'insegnamento non potrebbe rimanere monopolio di un corpo professionale, ma al contrario essere più flessibile e la funzione di insegnante potrebbe essere più ampiamente condivisa, e chiunque abbia conoscenze da offrire potrebbe essere incoraggiato a contribuire all'insegnamento.

In effetti, gran parte del lavoro svolto dal personale di alcune professioni potrebbe essere svolto da altri e diventare lavoro gratuito nella nostra Eutopia, alleggerendo così queste professioni.

### Sfruttare la riorganizzazione del lavoro

Il prossimo capitolo tratterà della riorganizzazione del lavoro, cioè delle modalità di organizzazione delle aziende nella nostra Eutopia.

La natura orizzontale di queste ultime eliminerà una certa gerarchia che svolge compiti di gestione e di decisione. Questo lavoro non sarà più necessario, poiché ora sarà svolto in modo collaborativo da tutti i collaboratori di un'azienda.

### Lavoro di merda e lavoro più necessario

Nella nostra Eutopia, molti lavori verrebbero aboliti, perché sono in realtà il prodotto delle disfunzioni della società odierna e quindi non sarebbero più necessari.

- La pubblicità rappresenta l'1% del PIL[^4].
- L'esercito il 3% del PIL.
- Il lavoro perso nelle compagnie di assicurazione.

L'elenco completo dei posti di lavoro persi in Eutopia è disponibile alla fine di questa storia.

## Aumentare il numero di lavoratori

### Smantellare la disoccupazione

In un Paese come il Belgio, il tasso di disoccupazione è attualmente del 5,5%. Tenendo conto della popolazione attiva totale del Belgio, pari a 5,8 milioni di persone secondo i dati Eurostat per il 2021, si tratta di circa 319.000 persone senza lavoro[^7].

Poiché non ci sono posti di lavoro in quanto tali, l'effetto della scomparsa del lavoro dipendente è accompagnato dalla scomparsa della disoccupazione.

Il lavoro salariato è un'opzione che favorisce l'inclusione sociale e una società aperta, in cui ogni nuovo membro è benvenuto a contribuire alla società con le proprie competenze e conoscenze, qualunque esse siano, e riduce le ore di lavoro complessive.

### Aumentare il numero di persone che partecipano al lavoro collegato

La società esclude dalla vita lavorativa tutta una serie di persone, in particolare quelle con disabilità.

Ciò può essere dovuto a discriminazione, stigmatizzazione, mancanza di accessibilità o di supporto.

Dal sistema, per il quale spesso è tutto o niente, anche se per loro è meglio continuare a ricevere il sostegno piuttosto che lavorare e vederselo revocare.

Le qualifiche generali della popolazione potrebbero essere notevolmente aumentate: attualmente le esperte vengono reclutate di solito dalle classi sociali più privilegiate. Ma se tutti avessero l'opportunità di diventare ciò che desiderano, e se avessero i mezzi per farlo, il numero di esperti sarebbe molto più alto.

Gli anziani che vengono esclusi dal mercato una volta andati in pensione, che vengono discriminati, ecc.

### Riduzione dei problemi di salute

Secondo un'indagine del 2020 sui dipendenti europei condotta dal gruppo di consulenza gestionale Mercer, circa il 33% dei lavoratori europei ha dichiarato di essersi messo in malattia per motivi legati allo stress o alla salute mentale.[^5] Inoltre, un'indagine del 2018 della Commissione europea ha rilevato che il burnout è presente nel 28% dei lavoratori dell'UE.[^6]

## Fonti (elenco non esaustivo)

[^1]: Serge-Christophe Kolms. La bonne économie. 1984
[^2]: Adret. Lavorare due ore al giorno. 1977
[^3]: [Insee. Croissance - Productivité](https://www.insee.fr/fr/statistiques/4277770?sommaire=4318291)
[^4]: Bullshit Job. David Graeber. 2018
[^5]: [Mercer Marsh Benefits Study - Barometro dell'assenteismo](https://www.mercer.com/fr-fr/insights/total-rewards/employee-benefits-strategy/barometre-de-labsenteisme-2023/)
[^6]: [COMUNICAZIONE DELLA COMMISSIONE AL PARLAMENTO EUROPEO, AL CONSIGLIO, AL COMITATO ECONOMICO E SOCIALE EUROPEO E AL COMITATO DELLE REGIONI su un approccio globale alla salute mentale](https://health.ec.europa.eu/system/files/2023-06/com_2023_298_1_act_fr.pdf)
[^7]: [ STATBEL. Occupazione e disoccupazione ](https://statbel.fgov.be/fr/themes/emploi-formation/marche-du-travail/emploi-et-chomage)
[^8]: Keynes. J. Lettera ai nostri nipoti. Saggio sul futuro del capitalismo. 1930.
