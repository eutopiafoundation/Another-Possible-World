> Fare clic sul mappamondo in alto a sinistra per cambiare lingua.
> {.is-info}

Benvenuti nella storia collaborativa e democratica promossa dalla Fondazione Eutopia.

Vi consigliamo di iniziare la lettura dalla pagina della dedica e di seguire l'ordine delle pagine.

Potreste essere tentati di passare direttamente alla parte 2 per scoprire di più sul mondo che vi proponiamo, ma le basi della parte 1 sono essenziali, perché sono quelle che giustificano il possibile, e senza di esse rischiate di vedere solo l'impossibile...

Ecco perché ci siamo concentrati sulla prima parte e perché gran parte della seconda è ancora incompiuta.

Come rendere possibili tutte le idee della parte 2? Uno degli elementi chiave è comprendere l'essere umano, come abbiamo fatto nel capitolo intitolato "Homo Sapiens", ma anche scegliere il sistema economico giusto, come abbiamo fatto nel capitolo intitolato "Lavorare insieme", perché questo sistema non produce solo beni e servizi, ma soprattutto esseri umani e le relazioni tra di loro.

Consapevoli del tempo e della motivazione che possiamo avere nella nostra vita, vi raccomandiamo questi capitoli per primi, e abbiamo anche prodotto dei riassunti per tutti i capitoli della nostra storia. [Questo è disponibile nelle Appendici](/it/4-Appendices/1Summaries)
