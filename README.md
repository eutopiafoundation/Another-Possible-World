# Another Possible World - Collaborative and democratic story by Eutopia Foundation

Online version on https://anotherpossible.world/
Supported by the Eutopia Foundation https://www.fondationeutopia.org/fr

## Eutopia- From the Greek eu=Good + topos=place

Convinced that the imaginary creates the real, the Eutopia Foundation promotes the creation of an evolving, collaborative and democratic ideal world based on shared values. Positioning itself as a third alternative to planning and market economies, it aspires to reconcile the principles of freedom and equality through the promotion of an economy based on reciprocity. It participates in the battle of imaginations and supports the realization of a genuine ecological and social transition. It supports projects in line with its values, by developing and providing the necessary tools for their creation, networking and self-management.

## Creation of a collaborative and democratic story

The basis of this project is the collaborative and democratic drafting of a shared vision of the ideal world. Each new member who joins the project can express his or her power on an equal footing with the others by editing the story, thus formulating a proposal, and can also propose changes to the democratic decision-making process through which these proposals pass. The aim is to rally around a common vision, transcending borders and linking up internationally. This phase also aims to develop and experiment with an effective democratic tool to enable the population to regain its power.

![Creation of a collaborative and democratic story](/0-GlobalProject/Image/presentation/1.webp)

## Implementation and support of projects

Thanks to this rallying around a shared vision, we'll be able to undertake and support existing projects that are in line with the values and aspirations of this collaborative narrative. To break out of hierarchical vertical organization based on domination, we are providing tools to encourage horizontal self-management supported by collaborative and democratic processes, as well as collective intelligence tools. Our aim is to beat the system at its own game of competition and monopoly by creating large-scale human cooperatives and open source projects that will compete with and bankrupt companies that disagree with the values we defend. Our alternative will ultimately bankrupt the system itself.

![Implementation and support of projects](/0-GlobalProject/Image/presentation/2.webp)

## Preparing the world of tomorrow

As it stands, our system can't go on indefinitely, and even if we don't manage to achieve a truly 'smooth' ecological and social transition, it will always be possible to change course as we gradually collapse - indeed, we'll end up being forced to do so. So the projects we are developing and experimenting with are preparing for the world after, by providing the tools and solutions we need to avoid falling back into a system of domination and environmental destruction. The aim is also to enable the population, in the event of any overthrow, to come together with the help of a deep and effective new democracy, ready for use to prevent slow and fruitless assemblies from taking place and legitimizing a further loss of power.

![Preparing the world of tomorrow](/0-GlobalProject/Image/presentation/3.webp)
