## Ajouter une proposition
---
### Accéder à l'outil d'édition du récit

Pour modifier collaborativement et démocratiquement notre récit, nous utilisons git avec Gitlab. Gitlab est open source et soutiens cette philosophie.

Vous avez dû recevoir un mail qui a ajouté votre adresse email au projet Eutopia. Via le lien dans cette email ou via https://gitlab.com/ créez-vous un compte avec votre email ou connectez-vous si vous avez déjà un compte.

Accédez au projet Eutopia qui se trouve dans vos projets ou accédez-y via ce lien : https://gitlab.com/FondationEutopia/WikiJS-Un-autre-monde-possible/

Sur cette page, cliquez sur WebIDE. Vous avez maintenant accès à l'outil d'édition du récit, mais attention **n'effectuez aucune modification avant d'avoir terminer la lecture de cette page !**

### Système des branches

En bas à gauche vous pouvez voir que vous vous situez sur la branche **main**, celle-ci correspond aux contenues public afficher le site wki.fondation-eutopia.org.

**Cette branche ne doit jamais être éditée !**

Les propositions de modification se réalisent dans la branche **dev**, celle-ci correspond au contenu en cours de développement par la communauté. 

Dans les X temps, la branche dev après vérification et approbations de la communauté sera placée dans la branche main pour mettre à jour le site.

### Créer une proposition

Cliquer sur main et sélectionner la branche **dev**.

De cette branche vous pouvez créer une proposition de modification.

Cliquez sur **dev** puis en haut sur **"+ create new branche ..."**.

Le nom de votre proposition doit être au format suivant
- Proposition-AnnéeMoisJoureHeureMinute-PrenomNom

Exemple :
- Propostion-2303180745-ManuTention

Vous êtes désormais dans votre branche qui est une copie de la branche dev, toutes vos modifications ne s'effectueront que sur votre branche.

Une fois édité, pour enregistrer les modifications sur votre branche vous devez clique sur "Source controle" au milieu à gauche "Icône avec 3 ronds reliés entre eux" et cliquer sur **"Commit & Push"**, puis **No use the current branch***

Sur cette même page si vous le souhaitez vous pouvez voir quelles sont les modifications que vous venez d'apporter.

### Format et type de proposition

Voici les différents types de propositions 
- Proposition de modification
- Proposition d'ajout
- Proposition de suppression
- Proposition de corrections d'orthographe
- Proposition de correction de chiffres

Les propositions de correction orthographique peuvent être larges et modifier l'ensemble des fichiers, mis à part celles-ci, les autres doivent se concentrer sur un sujet.

Réaliser une proposition séparée pour chaque sujet facilite le vote, le débat et l'approbation de ceux-ci par la communauté.

### Envoyer sa proposition

Pour envoyer votre proposition retourner la page gitlab du projet : https://gitlab.com/FondationEutopia/WikiJS-Un-autre-monde-possible

1) À gauche cliquer sur **Merge requests** vous pouvez retrouver ici toutes les propositions de la communauté et participer à leur approbation.

2) Pour envoyer la votre cliquer **New merge requests**

3) Sélectionnez la source qui correspond à votre proposition et envoyez-la sur la branche **dev**

4) Définiser un titre et une description résumant votre proposition et en l'argumentant si besoin.

5) Assigner vous la proposition en cliquant sur **Assign to me**

6) Dans reviewer ajouter tous les membres de la communauté

7) Sélectionner le label correspondant à votre proposition

8) Vérifier que **Delete source branch when merge request is accepted.** est bien sélectionné

9) Vérifier vos changements en cliquant sur **changes**

10) Envoyer votre proposition en cliquant sur **Create merge request**

## Bonne pratique de rédactions et d'approbations

Nous tirons une partie de nos bonnes pratiques de Tim Peters un grand ingénieur logiciel qui a défini une guide à suivre pour une bonne programmation dans le langage python. Nous avons retires celles spécifiques à l'informatique et adapter l'explication des autres à notre cas utilisation. Nous avons également développé nos propres bonnes pratiques et d'autres sont encore à découvrir.

Ces pratiques sont autant valables pour les solutions à concevoir que leurs explications.

Celles-ci ont pour vocation de vous aider à rédiger et à approuver des propositions pour permettre un meilleur résultat global.

1) Le beau est mieux que le laid.
    - La présentation de l'écrit, le graphisme du site à une 
    importance pour le lecteur

2) L'explicite est préférable à l'implicite.
    - Il faut aller droit à but sur ce que l'on souhaite dire et ne pas laisser des suppositions ou sous-entendu.

3) La simplicité est préférable à la complexité.
    - L'explication doit être simple et compréhensible par tous, il ne sert à rien de donner un air intelligent.

4) Complexe est mieux que compliqué.
    - Néanmoins certaines situations requièrent de la complexité, ce qui est bien différent de compliquer.

5) Un langage clair est préférable à un langage dense.
    - Un langage clair consiste à utiliser des mots simples et des phrases courtes pour exprimer des idées complexes de manière concise et compréhensible. 

6) La lisibilité compte.
    - Pour une meilleure lisibilité, il faut séparer les points de votre sujet en petit paragraphe autant que possible.

7) Les cas spéciaux ne sont pas assez spéciaux pour enfreindre les règles
    -  Il est préférable de suivre les meilleures pratiques plutôt que de le faire à votre façon, car cela conduit souvent à un code incohérent et illisible.

8) Bien que l'aspect pratique l'emporte sur la pureté 
    - Oui, il est préférable de suivre les meilleures pratiques, mais faire tout son possible pour respecter les règles peut également entraîner un code illisible. Ainsi, chaque règle peut avoir une exception. Si votre façon de résoudre un problème est plus pratique, lisible et plus facile à comprendre, alors il vaut mieux s'écarter des bonnes pratiques établies. 

9) Il devrait y avoir une - et de préférence une seule - manière évidente de le faire.
    - Garder juste en tête qu'il surement toujours une meilleure façon de faire et d'expliquer les choses

10) Même si cette façon n'est pas évidente au premier abord.
    - Garder juste à l'idée que les bonnes idées ne nous paraissent pas forcément évidentes en premier lieu à cause de nos habitudes et de nos croyances.

11) Mieux vaut maintenant que jamais.
    - Cela signifie qu'il ne faut pas passer notre temps à tout planifier et préoptimiser, il vaut mieux avoir une base et itérez dessus pour l'améliorer. Écrivez les propositions qui vous semblent bonnes même si incomplète, surtout si c'est sur des sujets encore inexploré.

12) Bien que jamais soit souvent mieux que *maintenant*.
    - Cependant il faut tout de même un minimum réfléchir et placer de bonnes bases, que de s'engager sur un chemin qui signifie plus tard qu'il n'y a pas de chemin de retour possible à moins d'établir des changements conséquents dans le récit. 

13) Si la mise en œuvre est difficile à expliquer, c'est probablement une mauvaise idée.

14) Si la mise en œuvre est facile à expliquer, c'est peut-être une bonne idée.

15) L'ortographe ne doit pas être un frein à la participation
    - Lors de la soummison de votre proposition d'autre pourront facilement corriger votre ortographe et votre syntaxe

## Syntaxe à respecter
---
Le wiki utilise Markdown qui est un langage de balisage léger avec une syntaxe de formatage de texte brut.

Vous pouvez directement visualiser le rendu en cliquant "Open Preview to the side" en haut à droite (Logo avec une petite loupe). Le rendu ne sera pas le même que celui affiché sur le site, mais cela permet de vous de voir si la syntaxe est bien respecter.

### Gras et italique
    *Texte en italique*
    **Texte en gras**
    ***Texte en italique et en gras***

### Barré:
    ~~Ce texte est barré.~~, mais pas celui-là.

### Les titres :
    #  Titre 1
    ## Titre 2
    ###  Titre 3
    #### Titre 4
    #####  Titre 5
    ###### Titre 6

### Les citations
    >Ceci est une **zone en retrait**.
    >La zone continue ici

    >Ceci est une autre **zone de retrait**.
    Cette zone continue également dans la ligne suivante.
    Cependant, cette ligne n’est plus en retrait

### Les titres :
    Liste tiret :

    - Liste1
    - Liste 2
    - Liste 3

    Liste numérotée :
    1. Liste 1
    2. Liste 2
    3. Liste 3

    Liste à cocher :
    [ ] A
    [x] B
    [ ] C

### Le code :
    C’est le `code`.

    ```html
    <html>
    <head>
    </head>
    </html>
    ```

### Images et hyperliens :
    Ici ce qui suit [Lien](https://example.com/ "titre de lien optionnel").

    [![Ceci est un exemple d’image](https://example.com/bild.jpg)](https://example.com)

### Les tableaux :
    |cellule 1|cellule 2|
    |--------|--------|
    |    A    |    B    |
    |    C    |    D    |