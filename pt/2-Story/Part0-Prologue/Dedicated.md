---
title: Dedications
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Para o ideal em cada um de nós que sofre com um mundo que não é.

Pelo sofrimento daqueles que amamos, pelo nosso próprio sofrimento, pelo sofrimento daqueles que não conhecemos e por todos aqueles que virão...

Esta obra destina-se a ser criada pela humanidade no seu conjunto e a ser actualizada ao longo da sua evolução, por e para si própria.
