---
title: Introduction
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Quem és tu?

Pode não ser um grande sociólogo, ou um grande filósofo, ou um grande economista, ou qualquer outra coisa, de facto, talvez não tenha ido à universidade, talvez tenha sido mau na escola. É o caso de muitos de nós também, mas apesar das nossas origens educativas muito diferentes, temos a confiança de vos propor aqui um outro mundo possível, um mundo que visa o ideal, e convidamos-vos a juntarem-se a nós.

Por isso, talvez se esteja a perguntar por que razão estaria bem colocado para o fazer acontecer. E talvez parte de si, só de ler este parágrafo, já sinta vontade de fechar este livro para não perder mais tempo. Mas, por favor, permita-nos mais alguns parágrafos para o tentar convencer.

## Imaginação

Apesar de não ser um grande especialista em nenhuma das grandes áreas deste mundo, há uma coisa em que é particularmente dotado, que é sonhar. Esta capacidade de imaginar é a maior dádiva dos seres humanos. Convidamo-lo aqui a sonhar, mas sobretudo a ajudar os outros a sonhar.

Quando começámos, éramos uma espécie insignificante com um poder de ação muito limitado. O que fez a diferença entre a nossa espécie, Homo Sapiens, e as outras foi a nossa capacidade de cooperar em grande número. [^1]

E devemos isso à nossa imaginação, à nossa capacidade de inventar histórias e de as contar a nós próprios.

Graças aos mitos colectivos, a que também podemos chamar "ficções", fomos capazes de nos unir e cooperar.

Temos a capacidade de acreditar em coisas que não existem, inventámos muitas lendas, mitos e histórias, traçámos linhas imaginárias no mundo, criámos nações e costumes, inventámos o dinheiro e demos-lhe o maior valor na nossa imaginação.

Este dom da imaginação vem da nossa necessidade de sentido, da nossa necessidade de prever e compreender o nosso ambiente para melhor sobreviver nele[^2].

Deixamos que esta imaginação colectiva governe toda a nossa vida, deixamos mesmo que governe o destino de toda a humanidade. O seu poder é tal que nos leva a destruir o nosso próprio ambiente, a provocar a nossa própria destruição.

Mas tudo isto não passa, no fundo, de uma imaginação colectiva e deixa de existir no momento em que todos decidimos deixar de acreditar nela.

Na realidade, aquilo a que chamamos mais vulgarmente razão é mais frequentemente o facto de continuarmos a acreditar e a conformarmo-nos com as regras do nosso imaginário coletivo.

## Razão

Quando decidimos deixar este mundo imaginário para trás, é aí que podemos finalmente encontrar a verdadeira razão.

Pode achar que não sabe muito sobre ela, mas descobrirá que não precisa de saber muito para encontrar as soluções e fazer as escolhas que nos levarão a uma vida plena e com significado.

Porque as respostas às nossas perguntas já estão dentro de cada um de nós. Para as encontrar, tudo o que temos de fazer é deixar a nossa imaginação colectiva para trás, abrir os nossos corações e ouvir. Pode parecer disparatado, mas estamos convencidos de que é aqui que reside a verdadeira razão.

E como já iniciámos este processo de desconstrução, é isso que vos propomos descobrir aqui, um mundo verdadeiramente baseado na razão.

Um mundo cuja simplicidade, sentido e lógica vos parecerão óbvios.

Descobrirá então que também você, seja quem for, seja qual for o seu conhecimento, não precisa de saber muito para julgar o que é bom e o que não é, que não precisa de muito para contribuir para a formação deste mundo.

> "Mas então", diz Alice, "se o mundo não faz sentido nenhum, o que é que nos impede de inventar um?" Inspirado por Lewis Carrol

<!-- > "Toda a inovação passa por três fases na opinião pública: 1) É ridícula 2) É perigosa 3) É óbvia." Citação apócrifa de Arthur Schopenhauer -->

## Eutopia

Podem estar a pensar que o que estamos a propor aqui é uma utopia.

Utopia é uma palavra construída a partir do grego antigo, derivada de "tópos" ("lugar"), com o prefixo "ou" ("não"), literalmente "nenhum lugar"[^3].

Utopia é uma palavra frequentemente mal utilizada e abusada. É brandida a todo o momento para sufocar a esperança, denegrir a imaginação, reprimir o progresso, é um apelo à resignação, uma palavra-chave no sistema de exploração, alienação e dominação para aceitar o status quo[^4].

Mas um "não-lugar" é uma sociedade que não existe, não uma sociedade impossível. E como podemos ver ao longo da história, o futuro da sociedade de amanhã é sempre diferente e novo. No final, a sociedade de amanhã é sempre a utopia de hoje.

A utopia é por vezes contraposta ao "científico". Mas isso revela ignorância não só do grego, mas sobretudo da própria natureza da ciência.

A busca do inexistente, a procura do realizável, a análise das utopias realistas é, de facto, uma atividade científica. Para saber o que é e o que não é possível entre o não observado, é preciso ter um conhecimento e uma consciência profundos da natureza e da estrutura do mundo.

É uma atitude científica que aqui adoptamos, que consiste em questionar e pôr em causa tudo, em querer explicar as coisas em profundidade, em procurar as verdadeiras causas, em praticar a dúvida e a crítica sistemática das justificações recebidas e em não aceitar nada como dado, evidente ou imutável. É nesta forma de escrever que seremos levados a questionar radicalmente a nossa sociedade. A ciência consiste em abrir os olhos.

Assim, o estudo das sociedades possíveis em relação ao estudo das sociedades actuais, presentes ou passadas, não está muito difundido. Algumas disciplinas e autores nem sequer imaginam que possa ter uma dimensão profissional, ou mesmo rejeitam fervorosamente a ideia. Talvez estes intelectuais estejam a tentar justificar a limitação da sua imaginação.

O nosso sistema pode ser terrivelmente mau, mas todos sabemos que não é o pior possível. Uma mudança radical poderia conduzir a uma sociedade ainda mais desagradável e, por isso, com este receio justificado, a massa da população é conservadora ou cautelosamente reformista. Só pode ser de outra forma se as pessoas conseguirem formar uma ideia bastante precisa e fiável do que poderá ser o novo e melhor sistema.

É por esta razão que a análise de uma outra sociedade possível deve ser suficientemente séria, reflectida, precisa e completa. E o mesmo se aplica às fases da sua aplicação, às fases de transição do sistema existente para o novo.

Porque o que acreditamos ser possível depende da análise, as pessoas precisam de ser convencidas pelo projeto. Só então o tomarão como fim, fazendo da marcha para uma sociedade melhor uma necessidade para a evolução da sua própria sociedade. [^4]

> "É claro que é na análise das sociedades que construímos que encontramos o conhecimento necessário para construir outras; mas depois temos de parar de acumular ingredientes sem nunca cozinhar." Christophe Kolm

Assim, na sua definição atual, tal como é entendida pela nossa sociedade, a utopia é um ideal, um conceito que não tem em conta a realidade e é, portanto, inatingível.

Portanto, sim, nesse caso, não estaremos a criar uma utopia aqui.

A realidade não é a nossa imaginação colectiva. A realidade é constituída por limiares que não podemos ultrapassar, para não comprometer as condições favoráveis à nossa sobrevivência. É a concentração máxima de CO2 na nossa atmosfera, é o limite para perturbar os ciclos bioquímicos do fósforo e do azoto, essenciais para o bom estado dos solos e das águas, é a erosão máxima da biodiversidade, ou seja, a extinção de espécies e do seu papel essencial nos ecossistemas, é a utilização intensiva dos solos, a utilização abusiva da água doce, a acidificação dos oceanos, etc...[^5][^6][^7][^8][^9]

Isto faz parte da realidade, o que não é o caso, por exemplo, do valor anunciado pelos pequenos números na página da nossa conta bancária.

De facto, estamos em plena utopia, acreditando que a nossa sociedade pode continuar a viver assim indefinidamente a longo prazo[^11].

Para conceber o mundo ideal, temos de deixar o nosso mundo imaginário para trás, começar do zero, para podermos resolver os problemas na origem. Porque é assim que podemos realmente construir um outro mundo que faça sentido, um outro mundo que seja realmente possível.

O que estamos a tentar criar aqui é uma Eutopia, um conceito semelhante ao de utopia, mas que é considerado realizável na realidade. O prefixo "u" torna-se "eu", que em grego significa "bom" e, portanto, literalmente o "sítio certo"[^12].

O que guia o ser humano livre é a sua imaginação do futuro, do possível, do desejável, da sua utopia, Eutopia. E como os seres humanos avançam muito mais frequentemente atraídos pela esperança do que movidos pelo desespero, os sonhos acabam por criar a realidade. A nossa esperança é que a realidade se transforme em Eutopia.

> A utopia mudou de lado; o utópico é a pessoa que acredita que tudo pode continuar como dantes. Pablo Servigne e Raphaël Stevens[^11]

## Dando um passo atrás

Temos de questionar a nossa perceção do curto e do longo prazo. Devemos definir 100 anos como curto ou longo prazo? É tudo uma questão de ponto de vista, mas precisamos de dar um passo atrás e deixar para trás a escala das nossas curtas vidas.

A Terra formou-se há 4 mil milhões de anos, a vida surgiu há 500 milhões de anos, a Hominia, a linhagem dos seres humanos, separou-se da linhagem dos chimpanzés há 7 mil milhões de anos, mas foi apenas há 300 000 anos que nasceu a nossa espécie: Homo Sapiens[^13].

Dentro de 5 a 7 mil milhões de anos, o Sol terá esgotado o seu hélio e transformar-se-á numa gigante vermelha, multiplicando o seu raio por 1000 e atraindo gradualmente a Terra para a sua auréola ardente. Nessa altura, a temperatura da Terra será tão elevada que os oceanos se evaporarão. Mas a vida terá desaparecido muito antes disso, devido à falta de oxigénio[^14].

Na realidade, os seres humanos ainda têm 2 mil milhões de anos para viver num planeta habitável.

É importante visualizar estes números numa escala para os compreender claramente.

2 mil milhões de anos são visualmente 2.000.000.000 de anos.

Se considerarmos uma geração por cada 25 anos, são mais 80.000.000.000 de gerações. E nós somos atualmente apenas a 12.000ª.... Apenas 0,015% da nossa vida na Terra já passou.

No entanto, o fim da Terra não significa o fim da espécie humana, o universo é vasto... Mas esta parte deixará espaço para outras histórias imaginárias... Porque primeiro seria tempo de aprender a sobreviver no nosso próprio planeta antes de esperar poder sobreviver noutros. O que não é o caso atualmente.

Portanto, estamos apenas no início da nossa história e, se considerarmos o ritmo a que estamos a consumir os recursos do nosso planeta e a relação que temos com ele e com todos os seus ecossistemas, não podemos esperar aguentar muito tempo...

Ao ritmo atual da produção mundial, teremos esgotado todo o petróleo em 51 anos, todo o gás em 53 anos, todo o carvão em 114 anos, todo o urânio em 100 anos, e não podemos dar muito mais tempo a todas as terras raras que são essenciais para as tecnologias actuais. [E esperamos durar 2.000.000.000?

Daí a importância de dar um passo atrás, olhar para além das nossas curtas vidas humanas e pensar a longo prazo. Voltaremos a este assunto mais tarde, mas é importante perceber que, se somos tão deficientes nesta capacidade, é porque o cérebro do Homo Sapiens não foi concebido para pensar a longo prazo. [^15]

Repare também que não é a Terra que estamos a tentar salvar, porque ela ainda tem um futuro brilhante pela frente, com ou sem nós, o que estamos a tentar salvar aqui é de facto a existência humana, e com ela as várias espécies que compõem o nosso ecossistema.

## Quando os sonhos se tornam realidade

Talvez se esteja a perguntar como é que imaginar um outro mundo pode ajudar a mudar o atual?

Afinal de contas, poderíamos dizer que seria melhor dedicar o nosso tempo e energia a mudar o que já existe do que a imaginar um novo.

Mas o que acontece não é independente do que tentamos. O que tentamos depende do que acreditamos ser possível. E o que pensamos ser possível depende da nossa análise.

Os princípios e valores em que se baseiam os fundamentos do mundo que vos propomos descobrir foram muito pouco desenvolvidos e analisados pelos intelectuais. Esta análise permitir-vos-á acreditar que esta visão é possível, e depois tentá-la. E portanto, em última análise, mudar o que já existe. Esta análise deve ser tão coerente quanto possível, para que possa ganhar o apoio do maior número possível de pessoas e conduzir a uma ação colectiva, sensata e de grande alcance.

Ao imaginar um mundo novo, descobrimos, por comparação, o absurdo do mundo atual, o que nos leva a questionar profundamente o mundo que nos rodeia, mas também a nós próprios.

Ao imaginar um mundo novo, criamos material, um espaço de reflexão e de debate na procura de um ideal comum.

Ao imaginar um mundo novo, reunimos as pessoas em torno de um ideal comum, permitindo-lhes juntar-se para agir e mudar o que já existe.

Ao imaginarmos um novo mundo, podemos inspirar-nos nele para produzir ferramentas e princípios, aplicá-los na vida real, experimentá-los, repeti-los e melhorá-los.

Finalmente, ao imaginar um mundo diferente, inspiramos sonhos e esperança, uma emoção necessária que está na base de qualquer grande revolução.

Falámos-vos deste dom do ser humano, a capacidade de imaginar e de contar histórias, uma faculdade que nos permitiu evoluir exponencialmente desde a nossa revolução cognitiva há 90 000 anos.

Pois bem, chegou o momento de mudarmos de novo a nossa história para mudarmos a história. Precisamos de ser movidos por um ideal comum, mudar a nossa narrativa, modificar as nossas crenças para que possamos mobilizar-nos e colaborar para mudar o nosso futuro.

> "Ao longo da história, as histórias têm sido os veículos mais poderosos para a mudança filosófica, ética e política... Por isso, é através das histórias que podemos provocar uma verdadeira 'revolução'". Um manual da resistência contemporânea[^24]

Quando estiver a ler isto, não deve dizer a si próprio "É impossível" - provavelmente tem razão, é-o nas condições actuais, e não estamos a propor a aplicação desta história neste momento. Mas deve dizer a si próprio "Pode vir a ser possível".

O que deve então fazer é perguntar a si próprio "O que é possível fazer neste momento para avançar em direção a este ideal?" e, desta forma, construir as etapas da sua transição da melhor forma possível. Mas, para isso, é preciso saber primeiro qual é esse ideal e para onde se está a ir.

Depois, basta olhar para o que é possível fazer agora e para as novas possibilidades que se abrem à medida que se trabalha para as alcançar.

No entanto, o objetivo aqui não é dizer-lhe o que fazer ou como fazê-lo, mas desenvolver uma visão que lhe permita descobrir por si próprio e ajudá-lo a fazê-lo com as ferramentas certas.

É também importante compreender que esta visão não é fixa, que é construída por si e que deve evoluir ao longo do tempo, à medida que faz progressos e descobre coisas novas.

Por isso, é ainda possível que o nosso sistema esteja condenado ao colapso, independentemente do que fizermos. Parece estar preso na sua inércia, a destruir o seu ambiente e fora de controlo.

Existem muitos livros sobre teorias de colapso, baseados em estudos muito sérios, mas se tal colapso acontecesse, não passaríamos de 7 mil milhões para 1 ser humano, e ainda haveria o suficiente para reconstruir uma sociedade. O colapso não acontecerá de um dia para o outro, será gradual, com ondulações, e será sempre possível mudar de direção durante o mesmo. [^16]

Em qualquer dos casos, o trabalho aqui realizado é igualmente interessante: ou graças a ele conseguimos mudar a sociedade a tempo de evitar o colapso, ou ao prepararmo-nos para a resiliência, de modo a podermos continuar a viver durante esse colapso, preparamos as bases sólidas para iniciar uma nova sociedade.

Uma vez que o colapso não deixará espaço para uma sociedade totalmente nova e pronta a ser utilizada, serão os indivíduos, nesse momento, que terão de construir os caminhos e elaborar as direcções a partilhar em conjunto.

A partir daí, a nossa impreparação poderá deixar o campo aberto a assembleias palavrosas e impotentes. Da mesma forma, os sistemas de autogestão não podem ser implantados sem antes serem experimentados, pois é quase certo que isso levará a inúmeros fracassos. A não instauração de uma tomada de decisão colaborativa e democrática a todos os níveis da sociedade pode, devido à situação de emergência, dar lugar a outros sistemas de dominação que não são desejáveis. É por isso que estas novas vias devem ser exploradas atualmente. [^4]

Apesar de tudo, continuamos convencidos de que podemos evitar o colapso. Este criaria uma catástrofe humanitária e ecológica que seria ainda mais difícil de reparar, razão pela qual não podemos qualificá-lo de desejável, mesmo que deixasse espaço para algo novo. Por conseguinte, faremos tudo o que estiver ao nosso alcance para encontrar e desenvolver soluções para o evitar. Não esqueçamos nunca que, mesmo que o sistema seja poderoso, a nossa imaginação colectiva é tão poderosa como ele, porque é nessa mesma imaginação que o sistema existe e resiste.

Por fim, a construção desta visão, mesmo que não seja aceite e desejada, revela como o mundo poderia ser. Pode fazer com que as pessoas se apercebam de que são elas próprias. Pode fazer as pessoas perceberem que estão simplesmente a jogar um jogo gigante com regras que criam incentivos, e que as suas vidas, oportunidades e privilégios seriam muito diferentes se as regras fossem alteradas.

> "Se queres construir um barco, não reúnas os teus homens e mulheres para lhes dares ordens, para lhes explicares todos os pormenores, para lhes dizeres onde encontrar tudo. Se queres construir um barco, faz com que os teus homens e mulheres desejem o mar. Antoine de Saint Exupéry

## A luta das histórias de transição

Vimos acima que é o imaginário que cria o real e, desde tempos imemoriais, foram as histórias que fizeram evoluir as nossas sociedades. A nossa época não é exceção, e os detentores do poder não concebem o fim das nossas sociedades, acreditam numa narrativa hiper-moderna e na construção da narrativa de transição.

Imaginam um futuro em que viveremos em cidades artificiais repletas de tecnologias que permitirão, entre outras coisas, contrariar a devastação das alterações climáticas. A nossa alimentação seria cada vez mais industrial, artificial e sintética. Estaremos imersos num consumo instantâneo apoiado por uma economia da informação, ou melhor, uma economia da vigilância. Seríamos rendeiros de tudo, já não pediríamos nada, mas seríamos "felizes".

Mas a narrativa hipermoderna e a construção da narrativa da transição quebraram-se. Esta modernidade baseada no ideal de um progresso material infinito, correlacionado com o progresso social para todos, afundou-se numa crise multidimensional e tornou-se completamente dissonante. A narrativa da hipermodernidade explorou o ideal do progresso, esvaziando-o ao mesmo tempo da sua essência social emancipadora. É manipuladora e apoiada pelos media, nomeadamente através da emergência de uma nova linguagem. As palavras são desviadas do seu significado original, esvaziadas da sua substância, orientadas em função de interesses ocultos. A hipermodernidade é a hiperacumulação, o hiperconsumo nascido do desejo tornado insaciável pela publicidade invasiva. O homem moderno, que tem tudo, é paradoxalmente o mais insatisfeito dos seres humanos. [^17]

A narrativa hipermoderna está a sofrer três grandes fracassos que todos começam a ver claramente. O primeiro é o seu fracasso ecológico, e a promessa de um capitalismo verde, ou seja, de um produtivismo que respeita a vida, parece cada vez mais implausível. O segundo fracasso é de natureza económica, com a acumulação e a desigualdade a todas as escalas, do local ao global, a aumentar exponencialmente, apesar de uma abundância material excecional. O terceiro fracasso é ético: as nossas relações sociais estão a ser transformadas em relações comerciais, das quais todos esperam sair vencedores, com tudo a ser comprado e vendido. Estes desequilíbrios geram violência e já não são sustentáveis.

Uma narrativa de transição está a surgir como alternativa à narrativa hipermoderna, que nos fará mergulhar numa nova era, a da noosfera. Mas está longe de estar construída e está longe de ser desejável. É esse o objetivo do projeto Eutopia, construir esta nova narrativa...

Então é preciso perceber com o que é que as pessoas sonham realmente? A imaginação é poderosa e será o primeiro passo no caminho para a realização. Por exemplo, será que sonhamos em sentir que estamos sempre de férias, em todo o lado? Então, qual é o nosso ideal coletivo? Precisamos urgentemente de responder a esta pergunta para podermos dar sentido a este movimento e conceber e implementar novas políticas agora.

> Se quisermos construir um amanhã melhor, as muralhas da fraternidade e da imaginação criativa terão de se interpor entre nós e o colapso. Citação modificada de Valérie Jousseaume[^17]

## Inteligência colectiva

Nenhuma pessoa detém a verdade, mas juntos podemos, pelo menos, aproximarmo-nos dela.

Não estamos a pedir-lhe que acredite em nada do que lê aqui, pelo contrário, estamos a encorajá-lo a não acreditar em nada, a nunca pensar que detém a verdade e a manter sempre alguma dúvida dentro de si.

Porque acreditar que se tem a verdade é impedir-se de a procurar e, portanto, de a encontrar.

É por isso que não podemos imaginar este mundo sozinhos - precisamos do maior número possível de pessoas. Para o conseguir, precisamos de utilizar a inteligência colectiva e desenvolver as suas ferramentas.

Por isso, se está a ler isto, estamos a editar este texto utilizando uma ferramenta chamada Git, que lhe permite fazer uma versão do progresso desta história. Também lhe permite participar na edição de partes desta história, submetendo-as para que todos os colaboradores possam dar a sua opinião, debater e, finalmente, votar na incorporação ou não da modificação. Uma vez aprovada a alteração, esta é apresentada online no local onde a está a ler.

A ideia é conceber uma plataforma inteiramente dedicada à edição colaborativa e democrática desta história. A ferramenta será suficientemente modular para poder ser utilizada em muitos outros projectos.

A aplicação chama-se Ekklésiapp, uma mistura de aplicação e Ekklesia, da palavra grega antiga para "assembleia", que vem diretamente das assembleias de cidadãos das antigas cidades gregas, incluindo Atenas, o berço da democracia. Embora nessa altura a democracia só fosse acessível à classe dirigente, veremos no capítulo 5 "Como decidir em conjunto" como continua a ser imperfeita e como ainda há muito a fazer. [^18]

Ecclesiapp é uma aplicação de tomada de decisões que pode ser utilizada à escala de algumas pessoas até toda a população, e é suficientemente modular para poder criar um número infinito de métodos de tomada de decisões diferentes, para o adaptar facilmente ao trabalho a efetuar. Um método escolhido pode ser facilmente modificado para que se possa procurar continuamente um método que ofereça os melhores resultados.

Este método está disponível gratuitamente. Pode permitir que outros criem a sua própria utopia, distopia ou eutopia. E tudo isto para desenvolver a investigação neste domínio, que é necessária e útil pelas muitas razões acima mencionadas.

Também permite que outras pessoas realizem outros tipos de trabalho de escrita em colaboração. Pode ser utilizado para gerir uma organização, uma empresa ou qualquer outro projeto e, finalmente, oferece uma verdadeira ferramenta democrática, devolvendo o poder às bases, suficientemente eficaz para acabar com os sistemas de dominação e hierarquia.

Mas como é que podemos chegar a acordo sobre as regras de publicação desta história?

A Ecclesia inspira-se diretamente no processo de decisão democrático que será posto em prática neste mundo ideal. De facto, o processo democrático de tomada de decisões neste mundo ideal será inspirado por esta plataforma. Porque esta plataforma vai permitir-nos diretamente testar as nossas ideias sobre o processo de decisão coletivo e democrático mais adequado, para o repetir até encontrarmos aquele que todos concordam que conduzirá aos melhores resultados. Assim, não só dá a sua opinião sobre uma decisão, como também dá a sua opinião sobre o processo de tomada de decisão. O processo de decisão atual será revelado mais adiante.

> "Sozinhos vamos mais depressa, juntos vamos mais longe" Provérbio africano

## O processo de redação

Quanto às competências necessárias para julgar esta história ou para nela participar, não há nenhuma.

Se achas que o mundo é complicado, é complicado porque o tornámos complicado, mas na realidade, quando se parte de uma boa base, tudo acaba por ser mais simples e mais lógico do que parece.

Tudo o que nos parece complicado é, na verdade, uma série de elementos simples, algumas coisas apenas precisam de mais tempo do que outras para serem revistas e assimiladas.

No final, os grandes cientistas são aqueles que conseguiram vislumbrar esta simplicidade do mundo.

Não duvides de ti próprio: se não compreendes alguma coisa, é simplesmente porque alguém não se deu ao trabalho de te explicar a partir das bases que conheces.

De facto, é essencial começar sempre do zero. É um dos princípios fundamentais desta história, e é por isso que vamos dedicar algum tempo a criar uma base sólida na parte 2, antes de descrevermos Eutopia em pormenor na parte 3.

Este livro destina-se a ser lido e compreendido por toda a gente. Os intelectuais podem achar este livro demasiado simplista, com um vocabulário que não é suficientemente rico, mas é esse o objetivo.

Porque todos temos de ser agentes de mudança, como podemos unir-nos se o que nos deve unir é ininteligível e incompreensível para quem não tem um conhecimento profundo deste ou daquele domínio.

Não estamos aqui a procurar utilizar termos complicados ou frases longas e complexas, nem estamos a utilizar estes métodos para parecer mais científicos, mais inteligentes, e assim criar potencialmente um falso sentido de verdade em si.

Esta história tem de ser acessível, compreensível e direta. Pretende-se que seja honesta convosco e, sem qualquer recurso a efeitos psicológicos, deixamos-vos totalmente livres para acreditarem ou não no seu conteúdo.

Na história da humanidade, cada produção de um autor é apenas a produção indireta de uma inteligência colectiva.

Esta história decorre naturalmente da busca constante da "verdade" que tem sido efectuada ao longo da humanidade pelos inúmeros humanos que contribuíram para a evolução do pensamento global. Cada um deles nasceu numa época, num momento do tempo, em que recebeu conhecimentos colectivos do passado e beneficiou de interacções que os levaram, com a ajuda das competências, do estatuto e do tempo de que dispunham, a recolher e a publicar novos pensamentos e conhecimentos. As referências directas a estes indivíduos na nossa narrativa serão anotadas, mas evitaremos descrever longamente o processo a partir do qual os seus pensamentos surgiram, a fim de aligeirar esta narrativa, para a tornar acessível, mas também para evitar individualizar demasiado estas descobertas, e a fim de promover um novo paradigma: compreender que toda a criação de conhecimentos e pensamentos na história da humanidade foi, mesmo indiretamente, apenas colectiva. Queremos agora levar esta inteligência colectiva ainda mais longe, com a ajuda de ferramentas que facilitem estes mecanismos, como a utilizada nesta história. Ao longo desta história, notar-se-á também uma certa linguagem que tenta sair desta individualidade.

Para nós, Eutopia é o fruto de uma inteligência colectiva produzida ao longo de milhares de anos e não o fruto de indivíduos isolados ao longo dessas épocas, incluindo a nossa. Uma imagem mental é a de ver a humanidade como um único organismo que evolui ao longo do tempo, cheio de milhões e biliões de células, vivendo, morrendo e renascendo, em constante evolução e readaptação para encontrar um equilíbrio no seu ambiente.

No entanto, as referências aos autores originais serão sempre anotadas no final da página, para que possa saber mais sobre essas pessoas e o seu trabalho. Mas também para disponibilizar todas as nossas fontes, para que as provas que apresentamos possam ser verificadas.

Uma segunda versão, mais rica e científica, desta história será também desenvolvida e estará diretamente ligada a esta, a fim de apoiar a argumentação do seu conteúdo junto das instituições académicas. Esta versão estará facilmente acessível à distância de um clique, sempre que quiser saber mais sobre a história, as reflexões e os argumentos de um determinado assunto, e poderá aprofundar um determinado assunto à distância de um clique.

Muitos estudos serão citados ao longo da sua leitura, mas, regra geral, aconselhamo-lo a ter cuidado com as conclusões dos estudos que lê, uma vez que é fácil influenciar a ciência. Alguns estudos podem ser financiados por empresas ou grupos de interesse com motivações e interesses específicos que influenciarão o estudo.

Os estudos podem estar sujeitos a erros de conceção, tais como enviesamento de seleção, erros de medição ou erros estatísticos, que podem conduzir a resultados não representativos ou incorrectos. A interpretação dos resultados pode ser subjectiva e depender das crenças e preconceitos do autor do estudo. É importante considerar os resultados no seu contexto e olhar para as provas como um todo. Em geral, os números podem ser apresentados de diferentes formas para apoiar um argumento específico e, por conseguinte, levar a conclusões erróneas. [^19][^20][^21]

A ideia não é evitar a ciência, mas, pelo contrário, confiar nela, mas com um olhar vigilante, reconhecendo que o conhecimento produzido está em fluxo e em evolução, que novos estudos contradizem os anteriores e que, no estado atual, é largamente influenciado pelo sistema económico que financia a investigação. [^22]

Além disso, sem entrar em pormenores, a metafísica questiona os pressupostos básicos sobre o que existe e como as entidades podem ser categorizadas e compreendidas. Em particular, explora conceitos como a existência do livre arbítrio, a natureza da mente em relação ao corpo e os fundamentos da própria realidade. Embora não ponha diretamente em causa os estudos científicos, a reflexão metafísica questiona os quadros conceptuais e os pressupostos subjacentes à investigação científica. Isto pode incluir reflexões sobre o que significa "replicar" um estudo, como entendemos a causalidade e o que significa algo ser "real" ou "verdadeiro" num contexto científico, como o facto de o ato de observação não ser neutro, mas afetar o estado do objeto observado. Em suma, a metafísica questiona os fundamentos filosóficos daquilo que tomamos por garantido na nossa busca de conhecimento, incluindo nas ciências[^23].

É por isso que esta obra não está, nem nunca estará, gravada na pedra; a sua vocação é ser levada a cabo pela humanidade no seu conjunto e ser actualizada ao longo da sua evolução, por e para ela própria. Por conseguinte, estará sempre aberta a novas evidências que possam contradizer o seu conteúdo, incluindo os seus fundamentos mais profundos.

> A dúvida é o princípio da sabedoria. Aristóteles[^25]

## Conclusão da introdução

Como já devem ter percebido, estão prestes a ler Eutopia, uma história que é agora mais do que necessária para a humanidade. É tempo de nos afastarmos do nosso estado primitivo, de permitir que a nossa sociedade evolua para uma verdadeira civilização, de evitar a catástrofe ecológica e humana que se aproxima. O objetivo desta história é ser realizada pela humanidade no seu conjunto, por ela própria e para ela própria, para lhe devolver finalmente um sentido e uma direção, para assegurar um futuro garantido de liberdade e felicidade.

> As tensões e as contradições da alma humana só desaparecerão quando desaparecerem as tensões entre as pessoas, as contradições estruturais da rede humana. Então, o indivíduo já não será a exceção, mas a regra, para encontrar esse equilíbrio físico ótimo que as palavras sublimes "Felicidade" e "Liberdade" pretendem designar: nomeadamente o equilíbrio duradouro ou mesmo a harmonia perfeita entre as suas tarefas sociais, a totalidade das exigências da sua existência social, por um lado, e as suas inclinações e necessidades pessoais, por outro. Só quando a estrutura das inter-relações humanas se inspirar neste princípio, só quando a cooperação entre os homens, base da existência de cada indivíduo, se processar de tal forma que todos os que trabalham lado a lado na complexa cadeia de tarefas comuns tenham pelo menos a possibilidade de encontrar este equilíbrio, só então os homens poderão afirmar com um pouco mais de razão que são "civilizados". Até lá, estão, na melhor das hipóteses, empenhados no processo de civilização. Até lá, terão de continuar a repetir: "A civilização ainda não está completa. Ela está em construção! Nober Elias, A Dinâmica do Ocidente".

## Fontes (lista não exaustiva)

[^1]: Yuval Noah Harari, Sapiens : Uma Breve História da Humanidade. 2011
[^2]: Sébastien Bohler, Onde está o sentido?/ 2020
[^3]: [Etimologia da palavra utopia](https://fr.wiktionary.org/wiki/utopie)
[^4]: Serge-Christophe Kolm, A Boa Economia: Reciprocidade Geral. 1984
[^5]: [Concentração máxima de CO2 na atmosfera](https://www.ipcc.ch/report/ar6/wg1/)
[^6]: [Limite de perturbação dos ciclos bioquímicos do fósforo e do azoto](https://www.nature.com/articles/s41467-023-40569-3)
[^7]: [Erosão máxima da biodiversidade](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]: [Uso intensivo do solo](https://www.nature.com/articles/s41467-023-40569-3)
[^9]: [Uso abusivo de água doce](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]: [Acidificação dos oceanos](https://www.ipcc.ch/report/ar6/wg1/)
[^11]: Como tudo pode entrar em colapso, Pablo Servigne & Raphael Stevens
[^12]: [Etimologia da palavra eutopia](https://fr.wiktionary.org/wiki/eutopie)
[^13]: Martin J. S. Rudwick, A História da Terra
[^14]: O Futuro da Vida na Terra" de Peter D. Ward e Donald Brownlee
[^15]: Sébastien Bohler, O inseto humano. 2019
[^16]: Jared Diamond, Collapse. 2004
[^17]: Valérie Jousseaume, Plouc Pride: A new narrative for the countryside. 2021
[^18]: Serge Christhonne, As eleições são democracia? 1977
[^19]: Ben Goldacre, Bad Science. 2008
[^20]: [Why Most Published Research Findings Are False](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]: [A Crise da Replicação](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]: ARTE, Documentário : A Fábrica da Ignorância. 2020
[^23]: Jaegwon Kim, Foundations of metaphysics (Fundamentos da metafísica)
[^24]: Cyril Dion, Petit manuel de résistance contemporaine, 2018
[^25]: Aristóteles, Ética a Eudemo
