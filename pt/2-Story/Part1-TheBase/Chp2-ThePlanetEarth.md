---
title: Planet Earth
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Esta página ainda está a ser escrita, por isso algumas partes podem estar em falta.
> {.is-info}

## Descrição rápida da Terra

### O mapa do mundo certo

[![Projeção de Gall-Peters](https://www.partir.com/cartedumonde/carte-monde-taille-relle.jpg)]

A projeção de Peters (ou projeção Gall-Peters, em homenagem a James Gall (1808-1895) e Arno Peters (1916-2002)) é uma projeção cartográfica que, ao contrário da projeção de Mercator, permite ter em conta a superfície real dos continentes. Mas localmente, esta projeção não preserva os ângulos, o que resulta na deformação dos continentes, ao contrário do mapa de Mercator. [^1]

### Descrição rápida dos diferentes ecossistemas

## O ciclo da água

### Apresentação das diferentes formas de água na Terra

### Descrição do ciclo da água e das suas interacções com o ambiente

### Consequências das actividades humanas no ciclo da água

## Fotossíntese e produção de oxigénio

### Descrição da fotossíntese e do seu papel na produção de oxigénio.

Desde a sua origem, a Terra está constantemente a criar informação. Graças a esta informação, o movimento ordenado da matéria foi criado, dando origem a uma diversidade excecional de formas, cores e movimentos: a vida tal como a conhecemos.

De toda esta informação disponível, uma foi a força motriz mais do que qualquer outra: é a informação codificada nos genes das plantas que transportam os mecanismos da fotossíntese. Este processo, específico do reino vegetal, torna o sistema vivo particularmente eficiente e não tem equivalente. A fotossíntese tem poderes energéticos e químicos. Capta a energia bruta e imaterial - a energia luminosa - armazena-a e redistribui-a com extrema precisão, numa escala molecular inferior a um nano-joule. Permite também combinar elementos minerais dispersos em materiais e moléculas que, por sua vez, transportam informação nas suas interacções e energia na sua composição.

Trata-se de um fenómeno prodigioso em termos das leis físicas da energia. Enquanto as leis da física ditam que a energia emitida por uma fonte se deve dispersar e degradar, tornando-se cada vez menos utilizável, as plantas são capazes de a canalizar. Graças à informação contida na sua biblioteca genética, a planta contraria assim as leis físicas da energia, conhecidas como entropia. Em vez disso, forma um ciclo de entropia negativa no universo: a matéria é ordenada e organizada e a energia é canalizada. Antes de se desintegrar novamente na sua viagem pela vida. Comparada com a quantidade infinitamente maior de matéria no universo, a matéria orgânica é como uma renda: pode assumir qualquer forma. É este conjunto de moléculas em forma de ganchos, roldanas, rodas, cabos, portas, pontos e correntes que constitui a matéria viva[^2].

### Consequências das actividades humanas na fotossíntese e na produção de oxigénio

## O ciclo do carbono

### Apresentação do ciclo do carbono e da sua interação com o ambiente.

### Consequências das actividades humanas no ciclo do carbono e nos gases com efeito de estufa

Além disso, os materiais e as energias que nos permitem realizar tais proezas modificam o termóstato global, que é composto principalmente por carbono, e acrescentam gases com efeito de estufa à atmosfera quando são utilizados. E a atmosfera aquece. O calcário que constitui a base dos cimentos, dos rebocos e das cales é uma rocha carbonosa; o petróleo, o carvão e o gás, que constituem 80% das fontes de energia que utilizamos ou 100% do betume em que viajamos, são rochas carbonosas. Este carbono não estava presente na crosta inicial da Terra, mas concentrava-se na atmosfera. Foi transferido da atmosfera para a crosta terrestre pelos organismos vivos, que o absorveram e o depositaram no fundo dos oceanos ou nas camadas geológicas das terras e das lagoas. Em apenas algumas décadas, estamos a libertar carbono que os seres vivos levaram centenas de milhões de anos a enterrar. Estamos a desestabilizar um sistema cujo equilíbrio está na origem da maior parte das formas evoluídas de vida actuais, incluindo a nossa.[^3] [^4] Estamos a desestabilizar um sistema cujo equilíbrio está na origem da maior parte das formas evoluídas de vida actuais, incluindo a nossa.

## Biodiversidade

### Apresentação da importância da biodiversidade para a vida na Terra

### Consequências da atividade humana na biodiversidade

Mais de metade das florestas e zonas húmidas do mundo desapareceram num século [^2].

## Solo

### Apresentação da importância dos solos para a vida na Terra

### Consequências das actividades humanas nos solos

Um terço dos solos do mundo está degradado. [^4]

## Recursos naturais

### Apresentação dos diferentes recursos naturais (água, ar, solo, etc.).

### Gestão sustentável dos recursos naturais

### Alternativas sustentáveis para os recursos naturais

## Ecossistemas terrestres e marinhos

### Apresentação dos diferentes ecossistemas terrestres e marinhos e sua importância

### O impacto da atividade humana nos ecossistemas

## Fontes

[^1]: [Wikipedia : Projeção de Peters](https://fr.wikipedia.org/wiki/Projection_de_Peters)
[^2]: Isabelle Delannoy, A economia simbiótica, Actes sud | Colibri, 2017
[^3]: [Organização das Nações Unidas para a Alimentação e a Agricultura (FAO) ](https://www.zones-humides.org/milieux-en-danger/etat-des-lieux)
[^4]: [Organização das Nações Unidas para a Alimentação e a Agricultura (FAO)](https://www.fao.org/newsroom/detail/agriculture-soils-degradation-FAO-GFFA-2022/fr)
