---
title: Working together
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## O que é a economia?

A economia é a atividade humana que consiste na produção, distribuição, troca e consumo de bens e serviços[^2].

Mas um sistema económico não produz apenas bens e serviços, produz também seres humanos e as relações entre eles. O modo como a sociedade produz e consome tem uma grande influência na personalidade, no carácter, no conhecimento, nos desejos, na felicidade e nos tipos de relações interpessoais.

Por vezes, temos tendência para julgar a sociedade apenas com base nas quantidades dos diferentes bens e serviços de que os seus membros dispõem. Mas as pessoas valorizam não só esses bens e serviços, mas também os sentimentos, as atitudes e as relações interpessoais que os acompanham, o que estes transmitem sobre o que é ser humano. Estes aspectos são muitas vezes negligenciados pela economia[^3].

Um sistema económico forma e molda os seres humanos e as suas relações através das suas actividades de produção e consumo. A sociedade segrega o sistema económico, que por sua vez influencia profundamente a natureza e a qualidade das relações entre as pessoas e as suas psiques.

Numa troca, o doador e o recetor recebem sempre mais do que aquilo que realmente dão um ao outro, porque também dão um ao outro a oportunidade de entrar numa relação humana.

Habituámo-nos a seguir a ciência económica como se fosse um simples facto da natureza que dita o que o mundo deve ser, sem nos questionarmos profundamente. Estamos impregnados do mundo em que vivemos, mas quando compreendemos o que é um sistema económico, tornamo-nos capazes de imaginar o nosso mundo a funcionar com outras regras.

Algo de novo está a acontecer hoje em dia, porque para além da miséria do mundo e do domínio que temos uns sobre os outros, temos as alterações climáticas, que nos levarão ao fim do nosso crescimento económico. [^4]

Todos os anos, os meios de comunicação social e as organizações de investigação climática dão-nos uma data. É a data em que os cientistas estimam que a humanidade consumiu mais recursos do que a Terra pode produzir no mesmo período.

Este número é o resultado de um balanço feito por especialistas de vários domínios sobre os recursos que retiramos do planeta e o ritmo a que esses recursos são repostos.

Esta data é conhecida como o "dia da ultrapassagem". Para que uma economia seja sustentável, esta data deve ser 31 de dezembro e não antes. Neste caso, os recursos podem ser renovados ao mesmo ritmo a que são consumidos.

Em 2022, estima-se que esta data seja 22 de julho[^1].

Esta data deve ajudar-nos a olhar com mais atenção para o nosso sistema económico, a tomar consciência dos seus fundamentos humanos e dos seus vícios.

Em particular, continuar a promover um sistema económico que encoraja os nossos grandes reforçadores primários (= desejo de sexo, comida, informação) é provavelmente a pior coisa que podemos fazer[^5].

As teorias económicas não conseguem apresentar argumentos que realmente ganhem apoio intelectual, o que demonstra a sua fraqueza. Isto porque se baseiam frequentemente na nossa imaginação colectiva e não têm qualquer fundamento real.

Por isso, talvez a economia seja mais filosófica do que se quer deixar transparecer e menos científica do que se quer deixar transparecer.

Haverá outras possibilidades para um sistema económico?

## O plano e o mercado

Existem atualmente 2 sistemas económicos que dominam o mundo moderno, o mercado e o planeamento[^6].

Cada lado defende o seu sistema com uma ética social e ataca o outro pela realização de outra.

Para o mercado, esse valor é a liberdade e para o planeamento, a igualdade.

Mas no mercado estamos muito longe de ser verdadeiramente livres, embora sejamos mais livres do que no planeamento, mas estamos pior no que diz respeito à igualdade.

E no plano, estamos muito longe de sermos verdadeiramente iguais, mas somos mais do que no mercado, por outro lado somos os piores no que respeita à liberdade.

De facto, a realidade atual é que cada um destes sistemas tem um mau desempenho no domínio do valor que defende, mas é verdade, melhor do que o outro para o valor defendido e ainda pior segundo o outro critério ético.

Estes 2 sistemas económicos no seu extremo mais extremo tornaram-se o mercado capitalista e o comunismo totalitário, ambos engendrando e reforçando características e relações humanas nauseabundas e degradantes, como o egoísmo, o tratamento dos outros como coisas, a hostilidade, o conflito, a competição entre as pessoas, a dominação, a exploração, a alienação...

Nenhum destes dois sistemas económicos conseguiu exaltar as melhores qualidades do homem. Até reprimem fortemente algumas delas. Em ambos os sistemas existe uma certa forma de dominação.

## A 3ª economia: a reciprocidade

Haverá então outra alternativa?

Há uma literatura que começa com "Há três tipos de sistemas económicos". Mas esta literatura deve ser procurada nas margens históricas ou disciplinares do pensamento económico, seja entre economistas de várias gerações atrás, seja na antropologia económica.

É o caso do economista Karl Polanyi (1886-1964), cujos trabalhos classificam os sistemas económicos de todas as sociedades, incluindo as do passado, primitivas e tradicionais, em três categorias. E salienta que podem coexistir numa mesma sociedade, embora cada uma delas atribua um lugar privilegiado/dominante/principal a um deles. Em primeiro lugar, distingue um sistema económico, o mercado. Chama ao segundo sistema o sistema de redistribuição, que na sua forma elaborada e moderna é o planeamento, em que um centro político decide sobre a afetação económica. Ao terceiro sistema, Polanyi chama "Reciprocidade", de acordo com uma tradição antropológica já bem estabelecida, e que é até bastante clara e precisa. [^6]

Não temos consciência da existência deste tipo de economia, mas ele existe de facto nas nossas sociedades. Praticamente todas as sociedades contêm os três sistemas económicos, mas em todas elas um dos sistemas é mais importante do que os outros dois.

Aqui está um triângulo correspondente aos diferentes sistemas principais em 1984:
[![Diagrama dos 3 tipos de economia](./.../../../../GlobalProject/Image/Marché.png)]

Os economistas construíram a economia e o seu pensamento no pressuposto de que o homem era e sempre foi aquilo a que chamaríamos "homo oeconomicus", e criaram uma caricatura dele como um ser egoísta, calculista e antissocial. Mas o ser humano é, antes de mais, aquilo a que chamaríamos "homo sociabilis", praticando universalmente a reciprocidade e necessitando universalmente de um círculo de amigos íntimos ou de uma comunidade[^9].

A reciprocidade é o mecanismo da economia comunitária que alimentou os primórdios da humanidade, como caçadores-recolectores e depois como agricultores sedentários.

A reciprocidade ainda está entre nós e a vida económica no seio da família baseia-se essencialmente nesta economia. Oferecemos serviços uns aos outros sem esperar nada em troca, por exemplo, não pagamos a um membro da nossa família para nos ir buscar à escola, nem somos obrigados a fazê-lo por uma entidade externa. A reciprocidade nas nossas sociedades tem diminuído drasticamente nas últimas décadas, fruto do mercado, que procura rentabilizar cada vez mais as interacções sociais, precisamente para criar novos mercados. No entanto, para além da família, podemos ainda encontrar muita reciprocidade nas relações de amizade/emocionais/amorosas, através do voluntariado, das comunidades, mas também em vários domínios, como o mundo da informática com o opensource e através de inúmeras plataformas de partilha de informações, realizações, objectos, etc.

Uma longa série de dádivas recíprocas, regulares, contínuas e estabelecidas entre duas pessoas é o que chamamos reciprocidade. A reciprocidade geral é o equivalente, mas de todos. Todos dão à sociedade, e a sociedade recebe de todos.

O altruísmo e a dádiva, pela sua própria natureza, não podem ser obtidos nem pela coerção nem pela troca em sentido estrito.

É esta a diferença em relação ao plano: estas transferências não são coagidas e impostas ao dador por uma entidade social exterior a ele. E a diferença em relação ao mercado é que também não são trocas em sentido estrito, em que cada transator está exclusivamente interessado em ter mais ou não ter menos dos bens ou serviços que transfere.

Neste sistema social, ninguém dá a outro especificamente, pelo que é impossível dominar, coagir, humilhar ou forçar uma contra-dádiva, e explorar económica ou eticamente por essa via. Quem dá a todos não dá a ninguém.

Neste sistema, o trabalho de cada um beneficia o consumo de um grande número de outros, muitas vezes muito mais tarde. E todos beneficiam do trabalho de muitos outros, frequentemente muito mais cedo. Cada um recebe mesmo uma parte do trabalho de gerações que desapareceram e trabalha para gerações que ainda não nasceram.

O valor ético social da reciprocidade poderia ser chamado de "fraternidade". Aqui temos a famosa trilogia republicana: a fraternidade é definida como "Não faças aos outros o que não queres que te façam a ti; faz sempre aos outros o bem que gostarias de receber deles".

Mas a comparação com a trilogia termina aqui, porque a reciprocidade apaga o desejo e o próprio sentido dos outros dois valores. Porque neste sistema, em que cada um cuida do outro como se cuidasse de si próprio, as vontades e, portanto, as liberdades pessoais, já não entram em conflito e as exigências de igualdade já não têm qualquer fundamento.

## Vantagem da reciprocidade

A reciprocidade baseia-se na bondade.

A bondade significa dar ênfase ao altruísmo, ao amor, à solidariedade voluntária, à dádiva recíproca, à generosidade, à partilha, à comunidade livre, ao amor ao próximo e à caridade, à benevolência e à amizade, à simpatia e à compaixão. Todos nós temos falta delas e sentimos que precisamos de mais. Todas as nossas grandes famílias de espírito, de pensamento e de sentimento reclamam-nas. Mas elas são negadas na nossa vida económica. Elas dominam a existência das nossas sociedades, mas podemos constatar que a mais vasta das aspirações é a mais rara das realizações.

A reciprocidade geral é a reconciliação entre a nossa ética altruísta e a nossa técnica científica, entre as nossas aspirações a uma comunidade calorosa, à liberdade individual e ao progresso material; ela permite a duração dos significados culturais e das belezas do mundo na expansão dos nossos conhecimentos, capacidades e consciências.

Além disso, a reciprocidade traz consigo muitas vantagens para a produção económica. Porque oferece um sistema coerente e eficaz que reduzirá drasticamente o tempo de trabalho global.

## A reciprocidade é possível?

Embora tenhamos estudado extensivamente os mercados e o planeamento, não dissemos quase nada sobre o terceiro modo de transferência de bens e serviços, nomeadamente as ofertas, e nada sobre o grupo de ofertas conhecido como reciprocidade.

Ao longo da história, foram surgindo novos sistemas económicos e, de cada vez, houve pessoas, talvez mesmo a maioria, que pensaram e afirmaram que os sistemas possíveis são aqueles que podem ser observados no momento ou que existiram no passado. Mas estas crenças são constantemente desmentidas pela história, pela existência de novas crenças. E é certamente o que acontece ainda hoje, porque não há provas de que qualquer dos sistemas que existem ou existiram seja o melhor possível. De facto, sim, são possíveis sistemas económicos que nunca foram observados.

O defeito do nosso sistema é que impede os indivíduos de escolherem individualmente dar: uma empresa que ofereça preços demasiado favoráveis vai à falência; um indivíduo não pode dar o que produz numa sociedade em que ninguém lhe dá nada.

Os psicossociólogos estudaram durante muito tempo os "comportamentos de ajuda", com resultados muito interessantes: não só tendemos a ajudar aqueles que nos ajudaram, como também tendemos a ajudar mais se nós próprios tivermos sido ajudados, ou seja, tendemos a ajudar estranhos se nós próprios tivermos sido ajudados por estranhos. É esta a essência da reciprocidade. [^13]

E foi isto que vimos no capítulo 3 sobre o Homo Sapiens, que foi possível criar seres altruístas, porque o altruísmo também é motivado pelo prazer, e que é sobretudo o ambiente e a educação que contribuem para desenvolver valores e motivações como a generosidade, a compaixão e o desejo de servir os outros.

Mas será que é possível uma sociedade ter uma economia baseada na reciprocidade, e em particular na reciprocidade geral?

Há sociedades inteiras em que todas as relações económicas se baseiam essencialmente ou mesmo exclusivamente na reciprocidade. Mas essas subsociedades ou sociedades são aquilo a que chamamos "grupos face a face", cujos membros se conhecem, famílias, aldeias, bandos, etc., e a reciprocidade não é essencialmente geral. Ainda assim, era fácil aplicá-la aqui, mas uma pessoa não pode conhecer mais de cem outras, pelo que podemos legitimamente perguntar-nos se é realmente possível em grande escala[^14].

Porque, embora a economia resolva o problema da produção e da distribuição de bens e serviços, qualquer sistema económico (incluindo a reciprocidade) enfrenta dois problemas fundamentais para a produção: o problema da informação e o problema da motivação.

## O problema da informação

A informação é o conhecimento que o trabalhador ou o fornecedor de recursos ou serviços tem do que deve fazer para satisfazer as suas necessidades e desejos da melhor forma possível.

E a motivação é o incentivo para fazer o trabalho ou utilizar os recursos dessa forma.

O plano e o mercado resolvem mais ou menos bem estes dois problemas.

O mercado resolve o problema da informação através dos mecanismos descentralizados do sistema de preços, os "indicadores de escassez e de utilidade": a oferta e a procura.

O planeamento resolve o problema organizando esta informação, ou seja, recolhendo, calculando e emitindo instruções ou previsões, de forma relativamente centralizada.

O mercado resolve o problema da motivação, reduzindo o egoísmo através do incentivo da troca.

O plano resolve o problema através da coação, da ameaça ou do castigo, da recompensa ou da promoção, mas mais geralmente através da autoridade e dos seus diferentes meios de coação ou de recompensa, e da interiorização mais ou menos forte da necessidade de obedecer por hábito ou por sentido do dever.

Os mesmos problemas se aplicam à reciprocidade: como é que as pessoas saberão o que os outros precisam delas e quererão dar o suficiente aos outros?

Estes dois problemas reforçam-se mutuamente, na medida em que a resolução de um ajuda o outro e vice-versa.

Quanto melhor for resolvido o problema da motivação, melhor poderá ser resolvido o problema da informação, porque as pessoas podem então transmitir mais facilmente as informações necessárias umas às outras.

Quanto mais o problema da informação estiver resolvido, mais produtivo é o sistema económico e, portanto, menos desperdício existe, menos trabalho é necessário para um determinado serviço final e, portanto, menos necessidade existe de o sistema motivacional fornecer trabalho, ou menos grave é a sua imperfeição.

Reciprocamente, cada um considera as necessidades ou desejos dos outros tão importantes como os seus, e isso determina o que dá do seu trabalho (tempo, esforço, trabalho) e dos seus bens, qualquer que seja a sua origem.

Se o problema da motivação está resolvido, também está resolvido o problema da informação, porque as pessoas podem sempre tornar a economia pelo menos tão eficiente como no mercado ou no plano, transmitindo voluntariamente umas às outras a informação económica que circula nestes sistemas.

As tecnologias da informação revolucionaram verdadeiramente este domínio, uma vez que estamos todos ligados e, graças ao desenvolvimento de aplicações adequadas, a transferência de informação está a tornar-se mais fácil do que nunca.

A transmissão e o tratamento da informação fazem parte do trabalho da sociedade, a par de outras actividades. Em contrapartida, a motivação leva-nos a fazer o que é melhor para a comunidade. Este sistema pode, portanto, fazer tudo o que o mercado ou o plano podem fazer.

Além disso, a reciprocidade reduz consideravelmente a quantidade de informação necessária. Através da redução do consumo competitivo ou conspícuo, da redução das necessidades extrínsecas e da redução da produção dos bens ou serviços correspondentes, da redução da necessidade de renovar os produtos devido à sua conceção baseada na sustentabilidade e da redução de muitos serviços que deixarão de ser necessários para o funcionamento da economia.

## O problema da motivação

Quando falamos de "natureza humana", podemos ouvir "o homem nunca mudará, sempre foi egoísta, etc." e depois vêm as ideias sobre a "verdadeira" natureza humana que devem acabar por prevalecer. O conhecimento histórico, antropológico e psicológico, a análise, e mesmo a simples lógica, devem ser absolutamente contrapostos a estas convicções ingénuas, e a resposta a "o homem nunca mudará" pode muito bem ser que ele apenas mudou[^15][^16][^17].

A "o homem sempre foi e é egoísta em todo o lado" podemos facilmente responder perguntando-nos o que sabemos sobre o que ele "sempre foi".

A humanidade está a iniciar uma transformação sem precedentes na sua existência, em termos de profundidade e, claro, de escala.

Como vimos, a natureza humana não existe e o ser humano pode tornar-se naquilo que quiser.

Eis alguns pontos que respondem à questão da motivação:

- As pessoas têm preferência por relações altruístas e de dádiva[^18].
- Todas as grandes éticas religiosas e seculares defendem fortemente o altruísmo e a dádiva. [^20]
- A principal motivação humana é receber a aprovação de outros seres humanos: num sistema recíproco, a aprovação é naturalmente universal e recíproca[^19].
- As análises empíricas do "comportamento de ajuda" provam que as pessoas são muito mais susceptíveis de ajudar os outros se elas próprias tiverem sido ajudadas.
- Os sociólogos defenderam a ideia de que a principal caraterística do ser humano é a imitação[^22].
- O hábito, esta imitação de si próprio, é outro fenómeno humano de grande importância[^23].
- Estudos sobre o desenvolvimento das crianças mostram que somos inicialmente altruístas, depois a sociedade torna-nos egoístas[^21].
- Educação: como vimos, o ser humano é apenas uma página na qual a sociedade escreve[^24].
- Reforço social: quanto mais as pessoas têm um espírito de reciprocidade, mais demonstram pelos seus actos e exprimem pelas suas palavras que é normal e bom tê-lo, o que encoraja os outros nesse sentido. A educação e as relações sociais no seu conjunto desempenham assim um papel importante. Se o homem é, antes de mais, cultural e, portanto, maleável, a sociedade pode fazer dele tanto o homem da reciprocidade como o egoísta do mercado, ou o submisso ou o hierarca do plano[^25].
- Quanto mais reciprocidade existe, mais se vê que é viável e mais se percebe as suas vantagens e, portanto, mais a aceitamos, mais "acreditamos nela", mais a queremos, mais a promovemos. E assim, mais tende a espalhar-se.

- No caso de algumas pessoas capazes de o fazer não quererem participar, isso não é grave, porque se esses casos existissem, não seria desprezo por eles, porque esse sentimento significaria uma falta de bem-estar na sociedade: Mas mesmo que a pessoa não queira participar, isso não é grave, porque não só descobriremos que a carga de trabalho necessária por habitante será consideravelmente reduzida, mas também que isso pode ser contrabalançado pelo facto de outros ficarem felizes por darem "mais" do que recebem, por altruísmo, como vimos, mas também pelo seu próprio interesse no trabalho.

Afinal de contas, na sociedade atual, porquê trabalhar?

- Coação direta: trabalho forçado
- Troca: vender o seu trabalho a troco de um salário
- Por um desejo de ajudar os outros ou a comunidade, para "estar ao serviço".
- Por um sentimento de dever, possivelmente para retribuir aos outros o que deles recebemos
- Por interesse no trabalho em si.
- Para encontrar contactos sociais, relações, integração na sociedade, contribuindo para o ambiente de trabalho.
- Para mostrar aos outros que se trabalha para eles, ou que se fazem coisas bonitas ou que se é hábil.
- Pelo estatuto social associado ao trabalho (contribuição, responsabilidade, etc.).
- Para se manter ocupado, para ter "algo para fazer", para evitar o tédio ou a ociosidade.
- Para brincar
- Como um hábito

Poderíamos acrescentar outros, mas é evidente a importância da profissão em si e das relações sociais que o trabalho permite criar, como se pode ver nos sentimentos que se seguem à "reforma". Regra geral, as pessoas retiram mais sentido da sua atividade produtiva do que da sua atividade de consumo. E todas estas motivações, com exceção das duas primeiras, podem encorajar as pessoas a trabalhar num sistema de reciprocidade, pelo que tudo o que temos a fazer é jogar com elas, aumentando o significado e o interesse do trabalho.

O trabalho não está condenado a ser a tortura da sua origem etimológica contestada, da palavra travalhum: tripalium, instrumento de tortura com um tripé...[^26].

Todas as dimensões da penosidade da tarefa devem, portanto, ser apagadas ou reduzidas. O trabalho física ou psicologicamente desgastante, entorpecente ou entorpecedor, demasiado duro, mecânico, monótono, demasiado sujo ou desagradável, demasiado desprovido de sentido, desprezível, etc., deve ser abolido, substituído ou tornado menos cansativo. Devem ser eliminadas, substituídas, limitadas, automatizadas, melhoradas, enriquecidas, compensadas, recompensadas, valorizadas ou partilhadas de forma mais alargada.

A própria ideia é transformar a natureza do trabalho para que se torne uma atividade que é algo como a auto-realização, a manifestação da humanidade profunda de cada um através da transformação da natureza e da sua preservação. Desta forma, o trabalho tornar-se-ia a "primeira necessidade da vida". Assim, a expressão "trabalhar para viver" deixaria de significar sofrer a dor do trabalho para sobreviver e sustentar-se, mas ganhar a vida através de uma atividade rica em significado e experiência.

## O lugar do dinheiro na reciprocidade

Estamos tão habituados a ele que todos acreditamos que o dinheiro é uma condição necessária da vida em sociedade, mas o dinheiro não é um facto da natureza, mas uma invenção imaginária do ser humano que pode ser criticada[^30].

O funcionamento do dinheiro não explica a sua origem, não se explica a si próprio, e é por isso que os economistas não explicam nada. Nem se explicam a si próprios por razões existenciais.

O dinheiro foi supostamente inventado para resolver o problema da dupla coincidência de necessidades colocada pela troca direta, porque sem o dinheiro seria demasiado difícil efetuar as trocas. Como é que se comparam os bens, e se eu precisar dos teus sapatos e tu não precisares das minhas maçãs?

Apesar de muitas investigações antropológicas em contrário, este mito é um dos mais enraizados na perceção que as pessoas têm da economia.

Nas economias primitivas, as pessoas simplesmente pegavam no que precisavam e, de um ponto de vista social, partilhavam os seus recursos. Ignorando a obsessão da escassez que caracteriza as nossas economias de mercado, as economias primitivas podiam concentrar-se sistematicamente na abundância[^7][^8].

Algumas investigações antropológicas, mais baseadas em elementos históricos e em descobertas empíricas, levam-nos a pensar que o dinheiro não provém nem do trabalho nem da troca, mas que é, antes de mais, histórica e concetualmente, uma forma de criar um laço social entre os indivíduos, pois representa a medida das dívidas contraídas entre eles[^12].

Algumas civilizações, como os Incas, não conheciam nem o dinheiro, nem o mercado, nem o comércio. [^10]

O problema do dinheiro é que, desde que seja necessário tê-lo para aceder à troca de bens e serviços, o capital pode brincar connosco.

Não é, portanto, pelo facto de ter a mão no trabalho que o capitalismo assegura a sua dominação, mas pelo domínio das condições de socialidade que tornam o trabalho necessário e que incluem, entre outras coisas, a necessidade do dinheiro[^3].

O dinheiro é a marca da confiança entre os homens. Com efeito, é necessária uma confiança inabalável para aceitar um sinal monetário de outrem em troca de um serviço prestado ou de um bem fornecido: confiança no facto de não se tratar de uma nota falsa ou de dinheiro falso, confiança no futuro que esse sinal monetário representa, na medida em que torna possíveis projectos futuros. Em última análise, o dinheiro é o horizonte da reciprocidade no tempo, não necessariamente em relação à pessoa que me deu o dinheiro, mas a confiança no facto de que, no futuro, haverá alguém que poderá devolver-me, sob a forma de bens e serviços, o valor dos bens e serviços que vendi temporariamente por dinheiro.

Porque porquê usar um sinal monetário se confio realmente na outra pessoa? O próprio objeto, o próprio conceito de dinheiro, reflecte de facto uma falta de confiança no outro, ao passo que, se essa confiança for estabelecida, a sua utilidade desaparece.

Se a outra pessoa confia realmente em mim e na sociedade, pode dar-me algo sem me pedir dinheiro em troca, porque sabe que amanhã serei eu ou outra pessoa a dar-lhe algo.

A relação de troca, mediada pelo dinheiro, é uma relação baseada na ideia de que os participantes não podem ter confiança na duração da sua relação: utilizam o dinheiro para completar a troca, para finalizar a dívida, em suma, para não terem mais nada a ver um com o outro uma vez concluída a transação.

A discussão política atual nas nossas economias é sobre a forma de utilizar este dinheiro, para que funcione corretamente, mas talvez o dinheiro não possa simplesmente funcionar corretamente, talvez esteja destinado a perverter as relações sociais?

No entanto, se tivéssemos de lhe dar uma natureza, seria melhor dizer que o dinheiro está destinado a perverter as relações sociais na sua forma atual, como meio de troca, e não como "facilitador da circularização de bens e serviços".

O dinheiro ocupa todos os vazios deixados pela ausência de sentido, permite-nos manter o controlo para acalmar o nosso córtex cingulado, e o medo da morte torna-se o motor da avareza[^11].

Muitas análises registam, de facto, os efeitos negativos da influência do dinheiro na sociedade, mas não propõem uma resposta adequada ao problema da monetarização da realidade.

Numa sociedade monetarizada, o valor das coisas passa a ser o valor que lhes é atribuído pelo dinheiro.

Abolir o dinheiro parece impossível, de tão natural que a ideia de dinheiro se tornou para nós. A monetarização da sociedade parece cada vez mais um facto fora do qual nem sequer é possível pensar.

O problema é que já não é o indivíduo que determina o seu sistema de valores e, portanto, aquilo a que aspira, mas que esse sistema de valores é objetivado pelo dinheiro.

No entanto, ao distanciarmo-nos do dinheiro, podemos recuperar o sentido de liberdade em relação aos valores do mercado, recuperando os nossos próprios valores.

Mas se continuarmos a pensar que o dinheiro, na sua forma atual, é a única maneira de resolver a questão da troca de bens e, portanto, a questão dos encontros entre indivíduos, ou seja, se pensarmos que o dinheiro é um pré-requisito da socialidade, teremos muita dificuldade em distinguir o valor económico dos valores éticos e políticos.

Foram as nossas sociedades ocidentais que, muito recentemente, transformaram o ser humano num "animal económico". Mas nem todos somos ainda animais económicos. O Homo oeconomicus não está atrás de nós, está à nossa frente; como o homem da moral e do dever; como o homem da ciência e da razão. Durante muito tempo, o homem foi outra coisa; e não foi há muito tempo que se tornou uma máquina complicada equipada com uma máquina de calcular.

Procuramos aqui dar valor a outras facetas da humanidade para além da relação económica de troca de bens.

Um valor que permita a cada um desenvolver-se num encontro permanente com os outros, numa socialidade baseada nos princípios da partilha e da entreajuda.

> "Alguns homens vêem as coisas como elas são e perguntam: "Porquê? Eu sonho com coisas que nunca foram e pergunto 'Porque não? George Benard Shaw

## Comparação com as ideologias actuais

Esta apresentação conduz inevitavelmente à criação de uma nova ideologia. Pensamos que é essencial estabelecer paralelos com as ideologias existentes e explicar porque é que elas são diferentes e porque é que essas outras ideologias não nos parecem sustentáveis ou eficazes.

Mas a ideia não é rejeitar essas ideologias, porque os valores por detrás da esquerda, a igualdade, e da direita, a liberdade, são belos e necessários para uma Eutopia, mas agora é uma questão de reunir o que é bom. Para que possamos unir-nos.

Para não dizermos apenas "Estão todos errados", mas "Estão todos parcialmente certos".

A partir de agora, não procuraremos entrar em confronto com movimentos pré-existentes. Pelo contrário, preferimos manter relações amigáveis, criar abertura e apoiar outros em projectos que estejam de acordo com os valores que defendemos. A ideia é respeitar os outros por aquilo em que acreditam, para que nos possam respeitar também por aquilo em que acreditamos. Acreditamos que as verdades acabarão por se revelar por si próprias e que não devemos ser perturbados nas nossas reflexões, sob pena de nos fecharmos aos outros. Acreditamos que só assim poderemos sair do abismo em que nos encontramos, permanecendo juntos apesar das nossas opiniões divergentes, unidos na busca comum da realização.

### Comparação com a esquerda

Os sistemas ideológicos que defendem uma maior planificação, ou mesmo uma planificação total, são os que mais se poderiam assemelhar à nossa Eutopia, mas bastam algumas palavras para mudar completamente o sistema prático que surge.

Socialismo = "De cada um segundo as suas capacidades e a cada um segundo o seu trabalho".
Comunismo = "De cada um segundo as suas capacidades e a cada um segundo as suas necessidades"[^27].
Reciprocidade = "De cada um, voluntariamente, de acordo com as necessidades dos outros"[^3]

A inadequação e as insuficiências destas outras ideologias são, em parte, as causas essenciais do que é o mundo atual. Elas deram origem a desvios, a revoluções anti-capitalistas, mas através delas falharam mais ou menos em criar sociedades de um novo tipo numa grande parte do globo. E como o resultado não é obviamente melhor do que o capitalismo desenvolvido, protegeram-no. Atualmente, são utilizados por todas as partes para constituir o mais gigantesco logro e hipocrisia da história.

A igualdade entre as pessoas, seja qual for a sua definição, pode ser um limite à liberdade de algumas pessoas. E a liberdade, por si só, não garante geralmente a igualdade.

A melhor das sociedades é provavelmente muito diferente das que conhecemos, como a nossa sociedade de classes sociais dominantes e dominadas. Mas aqueles que tentaram reinventá-la falharam, em primeiro lugar, porque não especificam suficientemente as sociedades que propõem.

Se esta abordagem analítica era necessária, é preciso agora ultrapassá-la para continuar o trabalho de desnaturalização genética empreendido, nomeadamente, pelo célebre Karl Marx [^28].

Além disso, o sistema marxiano não pode pensar o dinheiro nem antes nem depois do capitalismo: para além de lançar na sombra dois mil anos de história humana, não pode propor nenhuma saída.

As dificuldades do pensamento de Marx residem na possibilidade de pensar uma outra forma de organização do trabalho e da socialidade económica que não a do modo de produção capitalista.

Assim, na era da noosfera, os movimentos marxistas que arrastam consigo teorias com 150 anos de idade estão a ter grandes dificuldades em reimaginar organizações de trabalho horizontais autogeridas, em reinventar os nossos processos democráticos, em imaginar um mundo diferente do produtivismo, em compreender o ser profundo que somos e os meios para o transformar. Com demasiada frequência, estão a lutar CONTRA o capitalismo e têm dificuldade em apresentar soluções alternativas genuínas pelas quais possamos lutar.

Não estamos aqui a criticar a obra magistral de Marx, que foi muito necessária para a reflexão, mas pensamos que se ele tivesse admitido sinceramente que também gostava de ética e a tivesse colocado muito mais nas suas obras, se tivesse falado mais sobre a sociedade final ou boa e os meios de a alcançar, se tivesse posto tanto altruísmo nas suas características como em si próprio, talvez menos do pior tivesse acontecido[^3].

### Comparação com o direito

As políticas de direita, ou seja, as que promovem o mercado, ou mesmo o mercado livre total, são o movimento ideológico dominante nos países ocidentais, pelo menos no poder, e é mesmo o movimento ideológico dominante no mundo, e o que tem maior impacto.

Todos podem ver os efeitos negativos, não só para o planeta e os nossos ecossistemas, mas também para o nosso bem-estar geral. Em suma, neste sistema económico, passamos o nosso tempo a desperdiçá-lo e desperdiçamos a nossa vida a "ganhá-lo". E aqueles que não perdem o seu tempo a desperdiçá-lo, quer o façam ou não, continuam a colher as consequências desastrosas, e se não for a curto prazo, será a longo prazo.

No mercado, as pessoas têm sempre correntes. Mas, na maior parte dos casos, já não são correntes rudes de aço enferrujado; são "finamente niqueladas", muito mais "resistentes" e, em última análise, ainda mais difíceis de quebrar, especialmente porque algumas pessoas colocam elos de ouro nelas. Sentimo-nos então como se tivéssemos algo de precioso a perder.

O nosso sistema aprisiona cada indivíduo por todos os outros, no sentido em que uma pessoa simplesmente não pode, sozinha e livremente, manifestar as suas melhores qualidades. Em economia, num sistema concorrencial, uma empresa não pode, sob pena de falência, deixar de procurar maximizar o seu lucro se os outros procurarem maximizar o deles. Do mesmo modo, uma pessoa que dá não pode sobreviver numa sociedade em que os outros tiram.

Este sistema tem alguns aspectos positivos, como a liberdade de empreendimento e a "liberdade" geral que prevalece, e não se pode negar a eficácia que teve em muitos avanços. Mas também não podemos negar as consequências que teve, porque onde alguns se encontraram, outros não... aqueles que foram negligenciados, explorados, esquecidos... E é mais do que tempo de seguir em frente, e finalmente incluir todos.

É aqui que termina esta primeira versão da nossa análise da comparação com a direita. Não é que não haja muito a dizer, pelo contrário, há uma enorme literatura sobre o capitalismo, que detalha longamente as suas origens, causas e consequências. Mas a forma como queremos olhar para estas ideologias, as relações que queremos manter com elas e, finalmente, todas as questões ligadas ao compromisso do nosso movimento com uma política concreta, ainda têm de ser definidas coletivamente. No entanto, se sentir que não sabe o suficiente sobre o assunto, sugerimos que consulte os muitos recursos disponíveis na literatura e na Internet:[^29][^31].

## Fontes (lista não exaustiva)

[^1]: O Earth Overshoot day 2017 caiu a 2 de agosto, o Earth Over shoot Day. https://overshootday.org
[^2]: [Wikipedia : Economia](<https://fr.wikipedia.org/wiki/%C3%89conomie_(activit%C3%A9_humaine)>)
[^3]: Serge-Christophe Kolm. La bonne économie. 1984
[^4]: O Fim do Crescimento. Richard Heinberg. 2013
[^5]: Sébastien Bohler. O Inseto Humano. 2020
[^6]: Karl Polanyi. "A grande transformação" e "A economia como processo instituído" em Trade and Market in the Early Empires: Economies in History and Theory.
[^7]: Marshall Sahlins - Stone-age Economics
[^8]: Marcel Mauss- Ensaio sobre a Dádiva
[^9]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova. L'homo sociabilis. 2019
[^10]: [Organização social dos Incas](https://info.artisanat-bolivie.com/Organisation-sociale-des-Incas-a309)
[^11]: Sébastien Bohler. Onde está o significado? 2020
[^12]: David Graeber. Dívida: 5000 anos de história. 2011
[^13]: Lee Alan Dugatkin. The Altruism Equation: Seven Scientists Search for the Origins of Goodness [A Equação do Altruísmo: Sete Cientistas Procuram as Origens da Bondade]. 2006
[^14]: [Número de Dunbar] (https://fr.wikipedia.org/wiki/Nombre_de_Dunbar)
[^15]: Yuval Noah Harari. Sapiens: Uma Breve História da Humanidade
[^16]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion [^17]: Jonathan Haidt.
[^17]: Daniel Kahneman. Thinking, Fast and Slow
[^18]: [Ernst Fehr, The nature of human altruism] (https://www.nature.com/articles/nature02043)
[^19]: [A necessidade de aprovação social](https://www.psychologytoday.com/gb/blog/emotional-nourishment/202006/the-need-social-approval)
[^20]: [Religião e Moralidade](https://plato.stanford.edu/entries/religion-morality/)
[^21]: Wright, B., Altruísmo nas crianças e a conduta percebida dos outros in Journal of Abnormal and social Psychology, 1942, 37, pp. 2018-233
[^22]: Gabriel Tarde. As leis da imitação, 1890
[^23]: Charles Duhigg. O Poder do Hábito. 2014
[^24]: Émile Durkheim. Educação e Sociologia; 1922
[^25]: Robert B. Cialdini, Carl A. Kallgren, Raymond R. Reno.TEORIA FOCADA DA CONDUTA NORMATIVA: UM REFINAMENTO TEÓRICO E REAVALIAÇÃO DO PAPEL DAS NORMAS NO COMPORTAMENTO HUMANO
[^26]: [A etimologia da palavra trabalho é tripalium?](https://jeretiens.net/letymologie-du-mot-travail-est-elle-tripalium/)
[^27]: Karl Marx, Crítica do Programa de Gotha (1875). 1891
[^28]: Karl Marx. O Capital. 1867
[^29]: [Conferência: Comunismo e Capitalismo: a história por detrás destas ideologias](https://www.youtube.com/watch?v=kAhpcHRbBYA)
[^30]: Nino Fournier. A ordem do dinheiro - Crítica da economia. 2019
[^31]: [Documentário Arte 2014 - Ilan Ziv - Capitalismo](https://www.youtube.com/watch?v=ZWkAeSZ3AdY&list=PL7Ex7rnPOFuYRZ---hVDUMD6COgPHYZLh)
