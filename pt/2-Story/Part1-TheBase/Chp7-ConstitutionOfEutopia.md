---
title: Constituição da Eutopia
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Conteúdo em fase experimental e em processo de escrita.
> {.is-danger}

A constituição de Eutopia divide-se em 3 afirmações:

- A Declaração Universal dos Direitos Humanos, que incorpora muitos dos princípios fundamentais da atual Declaração Universal dos Direitos Humanos[^1], mas que adaptámos para acrescentar importantes nuances e extensões para melhor refletir os nossos valores, tais como a sustentabilidade, a diversidade, a interdependência com a natureza, a liberdade, a solidariedade e a equidade.
- Declaração Universal dos Direitos da Natureza / Mãe Terra, é uma carta que foi estabelecida por iniciativa dos povos ameríndios e formulada na Conferência Mundial dos Povos contra as Alterações Climáticas em 2010[^2].
- Declaração Universal dos Direitos Humanos em Relação às Espécies Animais Não-Humanas, é uma carta provisória e experimental, que tem como objetivo estabelecer com mais precisão as nossas relações com outras espécies animais vivas.

Para não as tornar mais pesadas, as justificações dos diferentes artigos foram separadas e colocadas no fim desta mesma página. Estas declarações são livres de serem modificadas pelos cidadãos de Eutopia através do mesmo processo democrático descrito no final do capítulo anterior sobre como decidir em conjunto.

## Declaração Universal dos Direitos Humanos

### Preâmbulo

Guiados pelos valores da igualdade, da liberdade, da solidariedade e da sustentabilidade, reconhecemos a dignidade inerente a cada indivíduo, farol que ilumina o caminho para um mundo onde todos nascem iguais em direitos e liberdades, e para o nosso destino coletivo.

Celebramos a diversidade infinita que tece a riqueza da nossa tapeçaria humana e estamos unidos pela profunda convicção de que nesta diversidade reside a nossa força colectiva. É neste espírito de unidade e harmonia que enunciamos a presente Declaração, afirmando que todas as pessoas têm o direito de exprimir livremente o seu pensamento, de procurar a sua felicidade e de contribuir para a nossa prosperidade comum.

A nossa busca comum é moldada pelo respeito absoluto pela integridade de cada um, pela busca incessante da equidade e da justiça e por um compromisso sagrado com a preservação da nossa casa comum, a Mãe Terra.

Ao escrevermos estes direitos universais, estamos a erguer um poderoso escudo contra a opressão, a injustiça e o esquecimento. Declaramos que em cada canto de Eutopia, a luz da igualdade ilumina os cantos escuros da injustiça, estabelecendo um mundo onde a liberdade, o amor e a compaixão são direitos inalienáveis.

Que esta Declaração Universal dos Direitos do Homem inspire a ação, oriente as nossas decisões e una os nossos corações na construção perpétua de uma sociedade onde todos, de mãos dadas, marchem para a aurora de um mundo onde a dignidade humana seja a bússola que guia a nossa viagem.

### Propostas e resumos

Artigo 1. Igualdade :

- Os seres humanos nascem e permanecem livres e iguais em direitos.
- Cada cidadão de Eutopia tem o direito inalienável de participar no processo de decisão em pé de igualdade com todos os outros. Nenhuma forma de discriminação ou privilégio pode impedir a igualdade de poder de decisão de cada indivíduo.

Artigo 2. Liberdade :

- Os seres humanos têm total liberdade de se exprimir e de pensar; ninguém pode impedir a diversidade de opiniões e a livre circulação de ideias.
- A liberdade de crença é igualmente sagrada. Todos têm o direito de escolher, praticar e exprimir as suas convicções espirituais, religiosas ou filosóficas sem medo de perseguição ou de discriminação.
- Os seres humanos têm o direito de escolher livremente o seu vestuário, de se exprimirem pessoalmente através do vestuário, sem estarem sujeitos a julgamentos ou restrições impostas por terceiros.
- Os seres humanos têm o direito fundamental de circular livremente pelo mundo, exceto por razões de preservação da Natureza ou por razões de segurança da integridade humana ou material, nenhuma fronteira pode limitar esta liberdade de circulação.
- Cada indivíduo é livre de empreender, de se realizar, de criar e de levar ao mundo as suas criações e inovações. A criatividade, a inovação e a contribuição individual para o bem-estar comum da Eutopia devem ser incentivadas.

Artigo 3. Solidariedade (Fraternidade) :

- A solidariedade deve orientar as decisões tomadas pelos membros de Eutopia.

Artigo 3. Equidade :

- Cada decisão deve ser concebida de modo a que todos possam beneficiar plenamente dela, eliminando barreiras e desigualdades.

Artigo 4. Sustentabilidade:

- A preservação do ambiente é primordial e uma responsabilidade colectiva. As decisões devem ter em conta o impacto na natureza e assegurar um equilíbrio sustentável entre a humanidade e o planeta.

Artigo 5. Integridade :

- Em caso algum uma decisão pode comprometer a integridade de outra pessoa, quer física quer psíquica. No entanto, cada indivíduo permanece soberano e é o único detentor do poder de comprometer a sua própria integridade.

Artigo 7. Segurança :

- Cada indivíduo tem direito à segurança, à proteção contra qualquer violência física ou mental e a um ambiente seguro para se desenvolver.
- Nenhuma arma letal pode ser detida ou produzida fora dos centros de investigação e das pessoas autorizadas.

Artigo 8. Confiança :

- A sociabilidade baseia-se na confiança colectiva, na entreajuda altruísta e no reconhecimento das necessidades de cada um. Nenhum serviço prestado pode ser pedido ao destinatário em troca de dinheiro, nem pode ser medido e armazenado num valor imaginário com vista a uma futura troca pelo mesmo ou por outro serviço. Cada um dando de si a todos, não dá de si a ninguém em particular.

Artigo 9. Diversidade :

- A Eutopia celebra a diversidade em todas as suas formas e está empenhada em criar uma comunidade inclusiva, onde cada indivíduo é respeitado e valorizado pela sua singularidade.

## Declaração Universal dos Direitos da Natureza / Mãe Terra

### Preâmbulo

Nós, os Eutopianos da Terra :

- Considerando que somos todos parte da Mãe Terra, uma comunidade indivisível de vida composta por seres interdependentes intimamente ligados por um destino comum;
- Reconhecendo com gratidão que a Mãe Terra é a fonte da vida, do sustento e da educação, e que ela nos fornece tudo o que precisamos para viver bem;
- Convencidos de que, numa comunidade de vida que envolve relações de interdependência, é impossível conceder direitos apenas aos seres humanos sem causar desequilíbrios na Mãe Terra;
- Afirmando que, para garantir os direitos humanos, é necessário reconhecer e defender os direitos da Mãe Terra e de todos os seres vivos que nela vivem, e que existem culturas, práticas e leis que reconhecem e defendem esses direitos;
- Proclamar a presente Declaração Universal dos Direitos da Mãe Terra, para que cada pessoa e cada instituição assuma a responsabilidade de promover, através do ensino, da educação e do despertar das consciências, o respeito pelos direitos reconhecidos na Declaração, e de assegurar, através de medidas e disposições diligentes e progressivas à escala local e global, o seu reconhecimento e aplicação universal e efectiva por todos os eutopianos do mundo.

### Propostas e resumos

Artigo 1. A Mãe Terra :

1. A Mãe Terra é um ser vivo.
2. A Mãe Terra é uma comunidade única, indivisível e auto-regulada de seres inter-relacionados que nutre, contém e renova todos os seres.
3. Cada ser é definido pelas suas relações como um elemento constituinte da Mãe Terra.
4. Os direitos intrínsecos da Mãe Terra são inalienáveis, uma vez que derivam da mesma fonte que a própria existência.
5. A Mãe Terra e todos os seres possuem todos os direitos intrínsecos reconhecidos na presente Declaração, sem qualquer distinção entre seres biológicos e não biológicos ou qualquer distinção baseada na espécie, origem, utilidade para os seres humanos ou qualquer outra caraterística.
6. Tal como os seres humanos gozam de direitos humanos, todos os outros seres têm direitos que são específicos da sua espécie ou tipo e adequados ao seu papel e função nas comunidades em que existem.
7. Os direitos de cada ser são limitados pelos direitos dos outros seres, e qualquer conflito entre os seus respectivos direitos deve ser resolvido de forma a preservar a integridade, o equilíbrio e a saúde da Mãe Terra.

Artigo 2. Os Direitos Inerentes da Mãe Terra :

1. A Mãe Terra e todos os seres que a compõem possuem os seguintes direitos intrínsecos:
   - o direito de viver e existir ;
   - o direito ao respeito ;
   - o direito à regeneração da sua biocapacidade e à continuidade dos seus ciclos e processos vitais, livres de perturbações humanas;
   - o direito de manter a sua identidade e integridade como seres distintos, auto-regulados e interligados;
   - o direito à água como fonte de vida;
   - o direito ao ar puro;
   - o direito à saúde plena;
   - o direito de estar livre de contaminação, poluição e resíduos tóxicos ou radioactivos;
   - o direito de não ser geneticamente modificado ou transformado de forma a prejudicar a sua integridade ou o seu funcionamento vital e saudável;
   - o direito à reparação integral e imediata de qualquer violação dos direitos reconhecidos na presente Declaração resultante de actividades humanas.
2. Cada ser tem o direito de ocupar um lugar e de desempenhar o seu papel na Mãe Terra para que esta possa funcionar harmoniosamente.
3. Todos os seres têm direito ao bem-estar e a não serem submetidos a torturas ou tratamentos cruéis por parte dos seres humanos.

Artigo 3. Obrigações dos seres humanos para com a Mãe Terra :

1. Todo o ser humano tem o dever de respeitar a Mãe Terra e de viver em harmonia com ela.
2. Os seres humanos têm o dever de :
   - a) atuar em conformidade com os direitos e obrigações reconhecidos na presente Declaração;
   - (b) reconhecer e promover a plena realização dos direitos e obrigações enunciados na presente Declaração
   - (c) promover e participar na aprendizagem, análise, interpretação e comunicação de formas de vida em harmonia com a Mãe Terra, de acordo com a presente Declaração;
   - d) assegurar que a busca do bem-estar humano contribua para o bem-estar da Mãe Terra, agora e no futuro
   - e) estabelecer e fazer cumprir normas e leis eficazes para a defesa, proteção e preservação dos direitos da Mãe Terra
   - f) respeitar, proteger e preservar os ciclos, processos e equilíbrios ecológicos vitais da Mãe Terra e, sempre que necessário, restaurar a sua integridade
   - g) assegurar a reparação dos danos resultantes de violações humanas dos direitos intrínsecos reconhecidos na presente Declaração e que os responsáveis sejam obrigados a restaurar a integridade e a saúde da Mãe Terra
   - h) capacitar os seres humanos e as instituições para defenderem os direitos da Mãe Terra e de todos os seres; e
   - i) estabelecer medidas cautelares e restritivas para evitar que as actividades humanas provoquem a extinção de espécies, a destruição de ecossistemas ou a perturbação dos ciclos ecológicos
   - j) garantir a paz e eliminar as armas nucleares, químicas e biológicas
   - k) promover e incentivar práticas que respeitem a Mãe Terra e todos os seres, de acordo com as suas próprias culturas, tradições e costumes
   - l) promover sistemas económicos que estejam em harmonia com a Mãe Terra e sejam coerentes com os direitos reconhecidos na presente Declaração.

## Declaração Universal dos Direitos Humanos em Relação às Espécies Animais Não-Humanas

### Preâmbulo

Na presente Declaração Universal dos Direitos Humanos na Relação com as Espécies Animais Não-Humanas de Eutopia, lançamos as bases para uma coexistência harmoniosa baseada na reciprocidade e no mutualismo. É crucial enfatizar que os princípios aqui estabelecidos se aplicam a qualquer animal com o qual escolhemos cooperar e partilhar interacções simbióticas dentro de Eutopia.

Fazemos esta distinção deliberada para preservar a liberdade dos animais como parte integrante do reino animal e dos seus habitats naturais, onde podem evoluir sem interferência humana. Nestas áreas preservadas, reconhecemos o valor inestimável da natureza selvagem e estamos empenhados em deixar que estes ecossistemas prosperem em total autonomia.

No entanto, onde os seres humanos residem e optam por construir comunidades, aspiramos a viver em harmonia com as espécies animais seleccionadas, estabelecendo relações baseadas na compreensão mútua, no respeito e na cooperação benéfica. Assim, esta declaração aplica-se aos animais que integramos no nosso ecossistema, pelos quais nos tornamos automaticamente responsáveis, demonstrando o nosso empenho numa simbiose equilibrada e respeitosa.

### Direitos das espécies sob responsabilidade humana

Artigo 1. Respeito pela dignidade animal :

- Todos os animais sob a responsabilidade dos habitantes de Eutopia têm direito a uma vida digna e sem crueldade.
- A interação humana com os animais deve respeitar a sua natureza intrínseca e o seu comportamento natural.

Artigo 2. Condições de vida dignas :

- Os animais devem ser alojados em condições que respeitem o seu bem-estar físico e psicológico.
- As instalações devem oferecer um espaço adequado, acesso a alimentos apropriados, água limpa e condições sanitárias óptimas.

Artigo 3. Acesso aos cuidados :

- Qualquer animal sob a responsabilidade dos habitantes de Eutopia tem direito a cuidados veterinários regulares para assegurar a sua saúde e bem-estar.
- O tratamento médico deve ser efectuado de forma humana, respeitosa e ética.

Artigo 4. Fim da vida :

- Os animais destinados ao consumo humano devem ter um fim de vida respeitoso e sem sofrimentos desnecessários.

## Justificação

### Declaração Universal dos Direitos Humanos

Em curso...

### Declaração Universal dos Direitos da Natureza / Mãe Terra

Atualmente a ser redigida...

### Declaração Universal dos Direitos Humanos em Relação às Espécies Animais Não-Humanas

Atualmente a ser redigida...

## Fontes (lista não exaustiva)

[^1]: [Declaração Universal dos Direitos Humanos. 1948](https://www.un.org/fr/universal-declaration-human-rights/)
[^2]: [Declaração Universal dos Direitos da Mãe Terra. 2012](http://rio20.net/fr/propuestas/declaration-universelle-des-droits-de-la-terre-mere/)
