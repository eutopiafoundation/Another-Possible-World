---
title: Decidir em conjunto
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

> Esta página ainda está a ser escrita, por isso algumas partes podem estar em falta.
> {.is-info}

## Como é que podemos decidir todos juntos?

Precisamos de compreender como podemos tomar decisões e garantir que essas decisões são as mais correctas para atingir o nosso objetivo comum.

Existem muitas ferramentas de tomada de decisão e muitas mais estão ainda por inventar. Aqui deixaremos de lado as ferramentas de precisão de menor escala e concentrar-nos-emos na global.

Na nossa sociedade ocidental, o instrumento utilizado é a democracia. Ao analisar e reinventar a democracia, podemos desenvolver ferramentas de menor escala baseadas nela, que podem ser adaptadas a outras formas de organização, como empresas, escolas, etc.

## O que é a democracia?

Democracia é a combinação grega de demos "o povo" e kratos "poder". Atualmente, o termo é utilizado para descrever qualquer sistema político em que o povo é soberano[^1].

Muitos países conquistaram a igualdade do poder político através do sufrágio universal. Esta vitória está agora consagrada nas suas constituições e é considerada uma das jóias institucionais mais brilhantes das suas sociedades.

Mas "Democracia" continua a ser uma palavra muito difamada e mal utilizada.

Proclamamos a democracia, como se tivéssemos de facto o poder.

Mas a nossa democracia não é mais do que uma abdicação recorrente do nosso poder legítimo em favor dos políticos que dele se apoderam.

E uma vez que o têm, sentem-se legitimados a tê-lo, mas mesmo eles estão sob o controlo de um poder muito maior.

Pensamos que o sistema político domina e que pode mudar tudo o resto, incluindo a economia e a sua forma. Pensamos que o nosso sistema é perfeitamente democrático devido à concorrência entre os candidatos em que votamos.

Mas veremos que a economia desempenha um papel nesta escolha competitiva. Assim, por um lado, o sistema eleitoral já não tem a qualidade de oferecer uma boa concorrência entre os candidatos e, por outro, não domina a economia, porque a economia influencia-o[^2].

Portanto, é a economia que domina a política.

Em matéria de clima, por exemplo, os estudos mostram que não há correlação entre o que a maioria das pessoas quer e o que obtém, exceto quando têm as mesmas preferências que os 10% mais ricos da população[^3].

A nossa democracia precisa de ser completamente reformulada, e aqui estamos a falar de muitas falhas: abuso de poder, corrupção, influência dos lóbis, decisões descoordenadas e não partilhadas, falhas de representatividade, ilusões de escolha, desigualdade de oportunidades.

E é aqui que descobrimos que o resultado real do nosso sistema democrático é o oposto da sua aspiração ideológica.

Apesar de todos estes defeitos, a democracia é um risco que vale a pena correr[^4] se quisermos preservar a nossa liberdade.

Comecemos, portanto, por analisar a nossa democracia, perceber o que está errado e, depois, reinventar um sistema democrático completamente diferente.

## O problema das nossas democracias

### O empresário político

O objetivo de qualquer político é estar e permanecer no poder. Faz disso uma carreira, que lhe permite ganhar a vida e encontrar o seu lugar na sociedade.

Estão em concorrência uns com os outros, mas também em conluio: querem destruir o seu adversário político, mas não o cargo político.

Todos os políticos, independentemente da sua oposição, têm um interesse comum em reforçar, e não em diminuir, o poder dos políticos. E se um deles sugerir essa ideia, receberá a desaprovação de todos os outros.

A grande maioria dos empresários políticos provém da mesma classe social e, geralmente, também da mesma classe social que os dirigentes do sector privado e das mesmas famílias burguesas.

Os candidatos não procuram apresentar o programa que constitui o seu ideal de boa sociedade.

Independentemente dos seus objectivos pessoais, escolhem o programa que melhor convém ao maior número de pessoas. Acabam por sacrificar a defesa das suas opiniões, dos seus ideais e das suas preferências pessoais, se as tiverem, em detrimento do que a massa da população deseja.

No entanto, este conhecimento dos desejos dos eleitores por parte dos candidatos não é, na maior parte dos casos, utilizado para servir o maior número, mas sim, como veremos, para os explorar em benefício dos mais pequenos.

Os políticos limitam-se a reagir aos incentivos que lhes permitem adquirir e manter o poder.

Não vale a pena odiá-los, eles estão simplesmente a seguir as regras de um jogo. Portanto, é o jogo que deve ser odiado, não os jogadores[^8].

Mas o problema dos empresários políticos é que o seu objetivo não é ter razão, mas que as pessoas e as empresas lhes dêem razão.

Os empresários políticos não são estúpidos, são bons tácticos que reagem aos incentivos do jogo e são muito bem aconselhados pela experiência da sua equipa no terreno, pelo que utilizam vários estratagemas e alianças para atingir os seus objectivos.

Para darem uma boa imagem, os candidatos escondem a sua personalidade, disfarçam-se e tornam-se hipócritas.

Os eleitores pensam que sabem em quem estão a votar, porque a tónica é muitas vezes colocada na personalidade do candidato, realçando a sua sedução, o seu carácter tranquilizador ou o seu carisma, criando um efeito de auréola. E estas impressões são muito mais facilmente manipuladas pelos media do que o conteúdo de um programa.

### Programas políticos[^2]

Os candidatos constroem o seu "programa" não de uma só vez, mas através de adições ou modificações sucessivas de declarações e acções, sem no entanto poderem voltar atrás, apagar ou contradizer palavras ou acções anteriores. Este constrangimento explica-se pelo tempo necessário para dar a conhecer um programa, ao passo que clarificá-lo à medida que se avança, acrescentando-lhe elementos, reforçando-o, chega mais eficazmente aos eleitores.

Explica-se também pelo facto de que estas contradições ou desmentidos prejudicariam a fiabilidade e a credibilidade do candidato, criando uma estrutura evolutiva e iterativa de conhecimento e de construção de programas.

Isto conduz frequentemente a diferenças nos seus programas e a uma imperfeição que prejudica a qualidade democrática do resultado do processo.

Em segundo lugar, os programas são globais, abrangendo todas as matérias ao mesmo tempo, e é preciso escolher entre eles, embora se possa preferir um num ponto e outro noutro.

Neste caso, os programas dividem a população, e um deles pode ser eleito por maioria, enquanto essa mesma maioria prefere um ponto de todos os outros programas ao do vencedor.

É algo que os referendos específicos permitem, nomeadamente, exprimir a opinião sobre cada questão separadamente.

O efeito das contribuições financeiras egoístas é uniformizar os programas, enquanto o efeito do apoio idealista/ativista é diferenciá-los.

### Honestidade e transparência

Qualquer sistema que possa ser manipulado será manipulado no pior momento possível[^9].

Quando se concebe um sistema, é absolutamente necessário perguntar a si próprio como é que os intervenientes no sistema vão poder atacá-lo e tirar partido do sistema de forma desonesta.

A honestidade e a transparência são problemas persistentes nos sistemas democráticos em todo o mundo. Os políticos e os representantes eleitos são frequentemente acusados de falta de transparência, de serem corruptos e de estarem envolvidos em numerosos conflitos de interesses ao favorecerem interesses particulares em detrimento do interesse geral.

No nosso sistema, certas regras não promovem a honestidade e a ética.

Por conseguinte, é fundamental conceber sistemas que incentivem as pessoas a agir de forma ética e moral.

Temos de desenvolver o princípio da divulgação, em que os participantes têm interesse em ser honestos nas suas divulgações e em comportar-se de forma ética e moral, uma vez que isso seria do seu interesse e os beneficiaria a longo prazo. Devem ser incluídas medidas de transparência acrescida.

### A escolha de alternativas

Para além do facto de os programas serem globais, a escolha de programas ou candidatos oferecida ao eleitor é geralmente pequena.

O financiamento das campanhas eleitorais é um dos factores que limita a escolha de alternativas. Os candidatos são aqueles que têm acesso a fontes de financiamento e que podem divulgar informação e propaganda suficientes sobre si próprios para dar visibilidade ao seu partido e ter hipóteses de ganhar[^10].

Os partidos políticos podem ter estruturas internas que favorecem certos candidatos ou tendências políticas em detrimento de outros. Este facto pode limitar as escolhas dos eleitores, dando-lhes apenas algumas opções que foram aprovadas pelos partidos políticos.

### Delegação de poderes

Uma eleição é duas coisas: um voto e uma delegação de poder.

Como já foi referido, a nossa democracia é a abdicação recorrente do poder legítimo do povo a um grupo de pessoas.

A delegação de poder pode também levar a uma perda de controlo sobre as decisões. Se uma pessoa ou um grupo delegar o seu poder a outra pessoa ou grupo, pode perder o controlo sobre as decisões que são tomadas e a forma como são aplicadas.

Sob o pretexto da delegação, que confere plena legitimidade aos representantes eleitos, pode haver desvio, alienação, espoliação ou usurpação de poder.

Um dos principais problemas da delegação de poderes é o risco de perda de poder. Se uma pessoa ou um grupo delega o seu poder de decisão noutra pessoa ou grupo, pode eximir-se de responsabilidade em caso de falha ou erro por parte da pessoa ou do grupo em quem delegou o seu poder.

Outro problema da delegação de poderes é o risco de conflito de interesses.
Algumas operações exigem tempo, energia, conhecimentos prévios e capacidade intelectual. Nestes casos, pode ser rentável, ou mesmo necessário, recorrer à divisão do trabalho e à especialização, delegando uma parte do trabalho de decisão. Por exemplo, um governo pode delegar o seu poder a uma empresa para gerir um projeto. Mas como o delegatário não sabe exatamente o que está a delegar, uma vez que, por definição, não dispõe de toda a informação sobre as possibilidades. Isto deixa a porta aberta a todo o tipo de abusos[^2].

### Centralização do poder

O poder está centralizado quando poucos decidem muito e muitos decidem pouco.

A centralização aumenta a delegação de poder, o que, por sua vez, reduz a democracia.

Quando um pequeno grupo de pessoas detém o poder absoluto, isso pode levar a decisões unilaterais e a políticas injustas que já não têm em conta os interesses e as opiniões dos cidadãos.

A centralização do poder pode tornar os governos mais vulneráveis ao abuso de poder, à tirania e à ditadura, uma vez que há menos controlos e equilíbrios para limitar a autoridade de um pequeno grupo de pessoas.

### Sistemas de votação

É importante compreender que um candidato eleito é eleito através de um escrutínio e que a alteração do escrutínio irá alterar quem é eleito.

O processo eleitoral afecta grandemente o resultado de uma votação. Assim, a escolha do escrutínio afecta o futuro das nossas sociedades. Por conseguinte, é importante concentrarmo-nos na escolha do escrutínio que queremos pôr em prática.

As eleições a uma volta e a duas voltas têm um problema fundamental: não permitem que as preferências dos eleitores sejam expressas de forma clara e precisa[^11].

Num sistema de uma volta, o eleitor só pode votar num candidato, o que pode ser problemático se vários candidatos tiverem um apoio significativo. Por exemplo, numa eleição em que há três candidatos principais, o eleitor deve escolher entre três opções diferentes, mas tem apenas um voto para expressar a sua escolha. Isto pode levar a situações em que o candidato eleito não tem o apoio da maioria dos eleitores, mas simplesmente o maior número de votos.

O problema das eleições a duas voltas é que podem, por vezes, conduzir a tácticas de voto estratégicas e a alianças entre candidatos. Num sistema de duas voltas, os dois candidatos com mais votos na primeira volta enfrentam-se numa segunda volta. Isto pode levar a situações em que os eleitores votam de forma diferente na primeira volta, a fim de garantir que o seu candidato preferido se qualifica para a segunda volta, mesmo que não estejam realmente convencidos desse candidato. Além disso, pode haver casos em que os candidatos unem forças para afastar outro candidato da corrida, em vez de tentarem ganhar as eleições de forma independente.

De um modo geral, a votação por maioria tem o grave inconveniente de não ter em conta a intensidade das preferências das pessoas. Aqueles que são quase indiferentes entre duas alternativas, e aqueles que preferem uma muito mais do que a outra, têm o mesmo peso na escolha entre elas. Por exemplo, 1001 cidadãos que mal preferem uma alternativa têm mais peso do que os outros 1000 para quem essa é a pior catástrofe.

Estas sondagens incentivam o voto útil, o que encoraja os políticos a organizarem-se em partidos políticos e a colocarem os eleitores em caixas pré-definidas, reforçando o efeito de bipolarização.

A bipolarização do eleitorado na política refere-se à tendência dos eleitores para se agruparem em torno de dois partidos políticos principais, criando uma divisão política distinta entre estes dois partidos. Isto deixa pouco espaço para partidos ou candidatos independentes ou para partidos políticos mais pequenos. Este facto tem também a consequência negativa de conduzir a uma maior polarização da opinião, a uma redução da diversidade de pontos de vista

Com estes boletins de voto, acabamos por votar contra um candidato e não a favor dele.

Mas o voto útil não torna legítima a pessoa eleita.

A representação proporcional significa que a política não está demasiado polarizada, mas pode levar a uma fragmentação do poder e dificultar a formação de governos estáveis. Torna-se difícil para um partido obter uma maioria absoluta de assentos, o que pode levar a coligações governamentais instáveis ou a governos minoritários que têm dificuldade em fazer avançar a sua agenda.

Uma boa eleição deve ser independente de alternativas irrelevantes e fechar a porta ao dilema do voto útil.

Exemplo de um possível voto alternativo:

Voto de aprovação: cada eleitor pode aprovar quantos candidatos quiser, sem ordem de preferência. O candidato com o maior número de aprovações é eleito.

O voto por pontos (ou voto ponderado) é um sistema eleitoral alternativo em que cada eleitor tem um determinado número de pontos que pode distribuir entre os candidatos.

Votação por maioria: os eleitores atribuem uma pontuação a cada candidato, por exemplo, de "muito bom" a "rejeitado". O candidato com a pontuação média mais elevada é eleito.

A votação de Condorcet é um sistema de votação em que os eleitores votam comparando todos os candidatos dois a dois[^12].

Votação Condorcet aleatória: neste caso, cada eleitor compara pares de candidatos como na votação Condorcet, mas em vez de ter em conta todos os votos de todos os eleitores, um número de comparações é selecionado aleatoriamente para determinar o vencedor.

Votação Mehestan (Girassol): Este é um sistema de votação que combina a votação por pontos e a votação Condorcet. Implica comparar cada candidato um a um e dizer qual deles prefere para cada critério, mas também definir o nível de preferência para esse critério. Este método de votação tem como objetivo ser justo e representativo, permitindo que os eleitores votem no seu candidato preferido sem o risco de dividir o voto e tendo em conta as preferências de todos os eleitores na determinação do vencedor.

### Partidos políticos

Os sistemas de votação descritos acima encorajam os políticos a organizarem-se em partidos, devido à sua dependência de alternativas irrelevantes.

### Lentidão institucional

Em muitos casos, os problemas políticos exigem soluções rápidas e eficazes para minimizar danos e perdas. No entanto, processos políticos lentos, como negociações, debates, votações e burocracia, podem atrasar a tomada de decisões e impedir que os governos actuem rapidamente.

### Mandatos de curto prazo

Os representantes eleitos em questão são eleitos por um determinado período de tempo, normalmente alguns anos. Durante esse período, têm uma grande liberdade pessoal para escolher as suas acções. Os eleitores já não os condicionam de forma alguma. Muito pouco obriga os eleitos a cumprir as suas promessas eleitorais, para além da sua honra, mas eles podem muito bem dizer que as coisas mudaram.

Os eleitos têm frequentemente mais tendência para se concentrarem nos resultados a curto prazo do que nos desafios a longo prazo.

Isto deve-se ao facto de estes mandatos encorajarem os representantes eleitos a concentrarem-se em questões políticas mais susceptíveis de atrair rapidamente o apoio dos eleitores, em vez de se concentrarem em questões mais complexas que exigem um compromisso maior e a mais longo prazo.

Os políticos podem ser tentados a tomar decisões populares a curto prazo para maximizar as suas hipóteses de reeleição. Isto pode levar a políticas que não são viáveis ou sustentáveis a longo prazo e que podem causar problemas maiores no futuro. Isto conduz a uma abordagem política da governação em vez de uma abordagem mais pragmática e ponderada.

### Fazer bluff para obter conhecimentos

O "bluff da perícia" na política ocorre quando os políticos utilizam a sua posição com um diploma em ciências ou administração para dar a impressão de que têm perícia ou conhecimentos especiais sobre assuntos, quando na realidade não têm. Podem usar esta estratégia para manipular ou persuadir os eleitores, mas pode levar a más decisões políticas.

A ênfase nos cursos de ciências e administração leva a uma concentração de poder e influência nas mãos de algumas elites que tiveram a sorte de ter acesso a esses cursos. Isto leva a uma perda de diversidade e inclusão nos círculos de tomada de decisão, uma vez que as pessoas de meios menos privilegiados têm frequentemente menos oportunidades de obter estes diplomas.

No entanto, a posse de um diploma não garante necessariamente competências ou conhecimentos especializados em matéria de governação ou política. O bluff do conhecimento especializado, que discutimos anteriormente, pode ser utilizado por pessoas que possuem diplomas, mas que não possuem os conhecimentos ou competências reais para tomar decisões informadas[^2].

### A ciência desprezada

As decisões políticas têm muitas vezes de ser tomadas com base em dados científicos complexos e provas empíricas, particularmente em áreas como a saúde pública, as alterações climáticas, a segurança alimentar, etc. Quando os decisores políticos ignoram as provas científicas, tomam decisões que não se baseiam em factos e dados verificáveis, mas sim em crenças ou opiniões. Isto pode ter consequências graves para a sociedade, nomeadamente em termos de saúde, ambiente e segurança.

### Propaganda

Os eleitores não votam num candidato sobre o qual não sabem nada. Sem que a informação sobre o candidato seja transmitida aos eleitores, um candidato não tem qualquer hipótese. Só começa a ter hipóteses quando a informação que recebe ultrapassa um determinado valor: as suas hipóteses de ser eleito aumentam quando a informação que recebe aumenta.

O investimento político é um investimento como outro qualquer, o objetivo é sempre o mesmo: o lucro.

A história da instauração da "democracia eleitoral" é paralela às lutas e às vitórias formais da liberdade de expressão, de imprensa, de reunião e de associação. Mas estas liberdades, devido à sua necessária base material e económica, foram, na sua maioria, tomadas pelos detentores destes recursos económicos. E assim utilizá-las para a propaganda eleitoral[^24].

A propaganda é utilizada para manipular a opinião pública e influenciar os resultados de eleições ou referendos a favor de um determinado partido ou ideologia. Pode ser enganadora, partidária e baseada em mentiras, meias-verdades ou omissões de factos importantes. Pode também jogar com os medos, preconceitos e emoções das pessoas e não com a razão e a lógica.

### O hooligan político

Podemos distinguir 3 tipos caricaturais de cidadãos durante as eleições:[^13][^14]

- Os hobbits: aqueles que não têm qualquer interesse pela política e são muito ignorantes neste domínio, não têm demasiados preconceitos.
- Hooligans: são tendenciosos, mas muito tendenciosos, são frequentemente vítimas do viés de confirmação e defendem valores de forma irracional.
- Vulcanos: pessoas capazes de pensamento racional.

Os nossos modelos assumem que todos os cidadãos são ou podem tornar-se vulcanos, mas na prática a maioria das pessoas são hobbits ou hooligans.

A intuição obriga-nos a formar uma opinião, e a razão é depois utilizada para apoiar essas opiniões. Racionalizamos as nossas convicções.

Não usamos a nossa razão para descobrir a verdade, mas para justificar as posições ideológicas que já adoptámos.

Por exemplo, se dissermos a palavra "Democracia", a nossa intuição diz-nos que é uma coisa boa, e depois a nossa razão manipula a definição da palavra para justificar a nossa intuição.

Hoje em dia, é fácil confirmar a nossa intuição graças à Internet, que nos pode fornecer todos os argumentos de que necessitamos e, se esses argumentos ou números não nos agradarem, podemos sempre questionar os motivos do autor dos factos ou interpretá-los de uma forma diferente.

Além disso, ao rodearmo-nos de pessoas que pensam da mesma maneira, o nosso ambiente reforça constantemente a nossa intuição e tende a polarizá-la.

Os activistas políticos tornam-se demasiado confiantes e torna-se impossível mudar as suas ideias, mesmo com factos e números.

E quando a intuição predomina desta forma, quanto mais nos informamos, mais boas razões encontramos para acreditar naquilo em que acreditamos, independentemente da informação que lemos.

O empenhamento político só reforça o vandalismo político.

Quanto mais nos interessarmos por um partido político com uma etiqueta de preço, mais radical se tornará a nossa inclinação inicial.

Vídeos e artigos científicos enganadores mas bem explicados reforçam a convicção científica de quem os vê e lê.

Como a seleção de grupo é mais eficaz do que a seleção individual, as tribos que sobreviveram são aquelas em que os indivíduos cooperaram o suficiente para garantir a sobrevivência do grupo.

E, portanto, aqueles que estão preparados para sacrificar os seus interesses individuais pelo bem do grupo. A seleção natural seleccionou o hooliganismo político.

Um voto baseado no vandalismo político proporciona prazer, enquanto um voto racional requer esforço mental.

### A ignorância e a irracionalidade dos cidadãos

As pessoas são ignorantes, quase todos os eleitores não têm qualquer noção de ciência ou compreensão do sistema político, estão mal informados e não conhecem os números[^16].

Alguns deles não têm consciência de que estão a praticar vandalismo político.

A maioria de nós não tem consciência dos diferentes enviesamentos cognitivos e dos efeitos com que nos confrontamos. Analisámos alguns deles no capítulo 3 sobre o HomoSapiens. Eis alguns deles, para o recordar:

- Viés de confirmação[^17]
- O efeito halo[^18]
- Dissonância cognitiva
- Pressão dos pares
- Risco de disponibilidade [^20]

A escola e os licenciados podem fazer as pessoas acreditar que são eruditas, fazê-las pensar que são mais inteligentes do que os outros e criar um excesso de confiança em áreas que não são as suas.

Todos se tornam muito seguros de si próprios. E o problema é que a incerteza é vista como um sinal de fraqueza quando, de facto, é uma qualidade fundamental.

O efeito Dunning-Kruger mostra que as pessoas com poucos conhecimentos ou competências numa área tendem a sobrestimar o seu próprio nível de domínio, enquanto as pessoas com competências mais elevadas tendem a subestimar o seu nível de domínio[^16].

Mas a ignorância dos cidadãos não é o maior problema, porque mesmo informando os cidadãos não os impede de votar de forma irracional.

As pessoas comportam-se como hooligans, não como cientistas.

### Tempo e energia para se informarem

Estar informado, aprender, saber, compreender, depois escolher e decidir, e votar, requer tempo, energia e conhecimentos prévios por parte do eleitor, o que por sua vez requer tempo e esforço. Estes custos explicam uma certa delegação de poder.

As campanhas eleitorais são frequentemente curtas e intensas, deixando aos eleitores pouco tempo para se informarem e avaliarem as várias opções. Além disso, os meios de comunicação social tendem frequentemente a centrar-se nos aspectos mais espectaculares ou controversos dos acontecimentos políticos, em vez de fornecerem uma análise aprofundada e matizada.

Este facto pode levar a uma polarização da política e a uma redução da qualidade do debate público. Os eleitores podem ser tentados a recorrer a fontes de informação simplistas ou partidárias que não reflectem necessariamente a complexidade das questões.

### Comunitarismo

O comunitarismo pode ser definido como uma tendência para atribuir mais importância à pertença a um grupo ou comunidade do que ao indivíduo enquanto tal. O principal problema do comunitarismo é o facto de poder conduzir a uma fragmentação da sociedade, em que cada grupo se concentra nos seus próprios interesses e não no interesse comum da sociedade como um todo[^21].

O comunitarismo costumava ser geográfico, mas agora está a espalhar-se por toda a Internet.

Ninguém gosta de estar em minoria.

Quando os utilizadores da Internet deixam de participar numa discussão por se sentirem demasiado minoritários, isso conduz à segregação ideológica.

Mudar de vizinhança ideológica na Internet é muito mais fácil do que deslocar-se geograficamente.

As pessoas podem deixar comunidades onde se sentem em minoria para se juntarem a comunidades onde estão em maioria.

Os "filter bubbels" são aquilo a que chamamos isolamentos ideológicos na Internet.

Fechamo-nos num mundo onde tudo o que lemos e ouvimos confirma o que já pensamos, o que torna difícil questionar as nossas convicções e compreender porque é que os outros pensam o que pensam.

Estas bolhas de filtragem são amplificadas pelos conteúdos personalizados oferecidos pelos gigantes da Web: Facebook, YouTube, etc. Eles prendem-nos à comunidade com a qual partilhamos as nossas ideias, fornecendo conteúdos personalizados. Cada vez que clicamos numa hiperligação ou gostamos de uma publicação, ficamos ainda mais presos a uma comunidade com a qual partilhamos as nossas ideias[^22].

### Convicções

As convicções podem ser definidas como crenças profundamente enraizadas ou opiniões firmemente mantidas sobre um assunto. Embora as convicções possam ser benéficas, são geralmente mais problemáticas do que qualquer outra coisa, particularmente no domínio da política.

Um dos principais problemas das convicções é o facto de poderem conduzir à polarização e à divisão. Se as pessoas têm convicções muito fortes sobre um assunto, podem estar menos inclinadas a considerar outros pontos de vista ou a trabalhar com pessoas que têm convicções diferentes. Este facto pode levar à polarização e à divisão na sociedade[^23].

Podem conduzir a uma mentalidade fechada. Se as pessoas estiverem demasiado apegadas às suas crenças, podem estar menos inclinadas a ouvir opiniões ou factos que as contradigam. Isto pode levar a uma tomada de decisões baseada em preconceitos e não em factos,

As crenças podem levar à resistência à mudança. Se as pessoas estiverem firmemente apegadas às suas crenças, podem estar menos inclinadas a aceitar novas ideias ou mudanças na sociedade.

## Soluções para os problemas das nossas democracias

### O empreendedor político

A solução para o empresário político é abolir esta função ou, pelo menos, limitá-la, aumentando a participação dos cidadãos.

O empresário político não deve continuar a ser alguém a quem o poder é delegado e que, a partir do momento em que o adquire, pode fazer o que quiser com ele. Mas que tenha uma função representativa e que, logo que as pessoas que lhe delegam o poder sintam que ele já não as representa, possam demiti-lo.

Deve haver uma transparência total, para que as pessoas saibam exatamente a quem delegam o seu poder e o que fazem com ele.

E para uma boa diversidade, temos de garantir que os candidatos provêm de diferentes origens e têm as mesmas oportunidades que os outros para aceder a estes cargos.

### Programas políticos

Uma solução para o problema dos programas políticos poderia ser torná-los mais modulares e mais específicos para cada assunto, em vez de os apresentar como um pacote global. Isto permitiria aos eleitores escolher as propostas que preferem em cada questão separadamente, em vez de terem de aceitar ou rejeitar todo o programa.

### Honestidade e transparência

Numa democracia, é importante que a tomada de decisões seja transparente e responsável, e que os detentores do poder sejam diretamente responsabilizados pelas suas acções. A delegação de poderes deve ser utilizada com cuidado e os riscos associados devem ser tidos em conta para garantir que os interesses dos cidadãos são protegidos.

### A escolha de alternativas

É importante dar ao eleitor uma escolha completa e alargada de alternativas, e uma quantidade igual de visibilidade e informação sobre elas. Os programas abrangentes limitam mais uma vez esta possibilidade, e os temas podem ser separados para permitir decisões mais específicas.

### Delegação de poderes

Os eleitores devem poder escolher se querem ou não delegar o seu poder. Devem também poder retrair e retirar a sua delegação de poderes em qualquer altura.

### Centralização do poder

Numa democracia, a distribuição do poder é essencial para garantir que os interesses e opiniões dos cidadãos são representados e tidos em conta na tomada de decisões. Isto implica a participação ativa dos cidadãos, a descentralização do poder e a criação de mecanismos de responsabilidade e transparência para limitar o poder dos líderes eleitos.

### Sistemas de votação

É necessário favorecer sistemas de votação que tenham em conta a intensidade das preferências dos cidadãos e evitar os que incentivam o voto útil.

### Lentidão institucional

Os referendos rápidos devem ser introduzidos para situações de emergência. De uma forma mais geral, os cidadãos devem poder exprimir os seus pontos de vista com maior regularidade.

### Mandatos de curto prazo

Há uma série de possibilidades, incluindo a introdução de mandatos mais longos com a introdução de um procedimento que dê aos eleitores o direito permanente de revogar os seus representantes eleitos. Podemos também tornar os cidadãos mais conscientes do longo prazo e motivar os políticos a planear a longo prazo. Podemos também limitar os mandatos ou mesmo abolir o papel do empresário político.

### Bluffs para a perícia

É importante reconhecer que as competências e os conhecimentos especializados não são exclusivos das qualificações académicas e que a inclusão de pessoas de diversas origens pode trazer uma riqueza de perspectivas que pode ser benéfica para a governação e a política.

### A ciência desprezada

Envolver os cientistas nos processos de tomada de decisão: Os cientistas podem ajudar a informar as decisões políticas, fornecendo dados, análises e avaliações independentes e baseados em provas.

Aumentar a transparência e a responsabilidade dos dados e provas científicas que utilizam para tomar decisões políticas, e ser responsáveis pelas suas decisões e respectivas consequências. Incentivar uma comunicação eficaz entre cientistas e decisores políticos, comunicando as suas conclusões de forma clara e concisa, evitando o jargão técnico. Promover uma cultura de respeito pela ciência. Educar os decisores políticos sobre a ciência: os políticos devem receber formação sobre o método científico e os principais conceitos científicos para compreenderem melhor os dados e as provas científicas.

Estabelecer numa Constituição os princípios a ter em conta para que as decisões respeitem os princípios inalienáveis sobre os quais existe consenso, por exemplo, para garantir a felicidade humana e o equilíbrio dos ecossistemas na Terra.

### Propaganda

Para reduzir o impacto da propaganda na política, é importante encorajar uma informação livre e transparente, baseada em factos e dados verificáveis. Os meios de comunicação social devem desempenhar um papel fundamental neste processo, fornecendo uma cobertura equilibrada e imparcial dos acontecimentos políticos, verificando os factos políticos e promovendo a verificação independente dos factos. É igualmente importante sensibilizar o público para as técnicas de propaganda e de manipulação da opinião, ensinando-o a reconhecer e a resistir a essas tácticas.

### Ignorância cidadã

Uma educação de qualidade pode ajudar os cidadãos a tornarem-se eleitores mais informados. É importante ensinar aos alunos as competências de pensamento crítico, raciocínio lógico, procura de informação fiável e compreensão das questões políticas.

Transparência e acessibilidade: as políticas e decisões públicas devem ser transparentes e facilmente acessíveis para que os cidadãos possam compreender melhor como funciona o seu governo e que decisões são tomadas.

Comunicação: é importante que os líderes políticos comuniquem clara e regularmente com os cidadãos, utilizando uma linguagem simples e evitando o jargão técnico.

Participação: incentivar a participação dos cidadãos nos processos políticos, tais como eleições, petições, debates públicos, consultas, sondagens de opinião, etc., pode ajudar a aumentar o envolvimento dos cidadãos na vida política e a reforçar a sua compreensão das questões políticas.

### Falta de tempo

O problema do tempo pode ser resolvido dando mais tempo aos cidadãos, reduzindo o seu horário de trabalho. Outra possibilidade é dispor de um algoritmo que, de cada vez, seleccione com precisão uma amostra representativa da sociedade para evitar que todos tenham de votar.

### O hooligan político

É preciso ter o cuidado de ler os programas sem vandalismo político e de os cruzar com a investigação económica e sociológica. Pergunte a si próprio se estamos a seguir a nossa intuição, que a nossa razão teria depois apoiado com argumentos.

A tomada de consciência deste vandalismo conduzirá a melhores debates e proporcionará um modo de pensar que não está preso à intuição e que nos permite sair do nosso modo de pensar vândalo político. Temos de apreciar tudo o que é contra-intuitivo, de apreciar o facto de convencer a nossa intuição de que a nossa intuição está errada.

Temos de apreciar o facto de estarmos errados, de nos podermos questionar e de compreender que é desbloqueando a nossa intuição que podemos fazer os maiores progressos.

### Comunitarismo

Temos de lutar contra o desejo de cada um de fazer parte da maioria.
Depois, temos de lutar para que fazer parte da minoria não seja uma experiência embaraçosa.

Temos de ter cuidado para não dar à maioria a vantagem pelo simples facto de ser a maioria.

É preciso fazer com que a experiência de estar em minoria seja agradável.

Fazer com que as pessoas que estão em maioria queiram estar em minoria.

Deixar que todos tentem compreender-se e misturar-se.

### Convicções

Numa democracia, é importante que as pessoas tenham a mente aberta e estejam dispostas a ouvir as opiniões e os pontos de vista dos outros. Os cidadãos precisam de ser sensibilizados e educados para este facto.

### Conclusão das soluções

Há muitos problemas inerentes à democracia.

Para que funcione bem, precisa de condições ideais.

Bons meios de comunicação social para serem bem informados e para que os políticos no poder actuem realmente no interesse do povo.

Mas o problema é que a nossa democracia está a criar um contexto em que acontece exatamente o contrário.

As pessoas ou são hobbits, não se interessam por política, são muito ignorantes nesta área, não têm muitos preconceitos, ou são hooligans, têm um preconceito, mas muito assumido, e muitas vezes vítimas do viés de confirmação, chegam ao ponto de defender valores irracionalmente.

O que precisamos é de vulcanos, de pessoas capazes de pensar racionalmente.

E, atualmente, não é possível ter discussões racionais entre pessoas políticas numa democracia com representantes eleitos que querem ser eleitos.

Se queremos uma mudança radical na democracia, temos absolutamente de ter em conta todos estes elementos racionais, como o princípio dos vândalos políticos, a segregação comunitária, etc.

Mas a democracia é um risco que vale a pena correr, porque com ela todos nos sentimos iguais, oferece capital moral e federa-nos para cooperar.

## Ideia experimental para uma nova democracia profunda (V1)

Nome: E-Democracia participativa partilhada por referendo espontâneo apoiado por conhecimentos e sabedoria.

Para permitir uma participação mais ampla e direta dos cidadãos, estamos a utilizar as tecnologias da informação e da comunicação para criar uma ferramenta digital que facilitará e melhorará os processos democráticos.

Esta ferramenta é uma plataforma Web, também disponível como aplicação móvel. Pode pensar-se nela como uma wiki, na qual se pode aceder a informações relacionadas com os diferentes níveis da sociedade em que se está envolvido, tais como leis, modos de organização, decisões actuais, etc. Estes níveis estão divididos e estruturados de forma a poderem ser facilmente acedidos. Estes estratos estão divididos e estruturados em categorias e subcategorias, conforme necessário, em função da natureza do estrato.

Qualquer pessoa pode sugerir alterações a qualquer informação introduzida. Como é impossível para cada ser humano ler todas as propostas que poderiam ser feitas pelos 7 biliões de seres humanos. Utilizamos um algoritmo que recolhe uma amostra representativa de todas as pessoas ligadas à proposta. A proposta é então enviada a esta amostra representativa. As pessoas podem votar a favor, contra ou abster-se e, se houver mais de 50% de votos favoráveis, a proposta passa ao nível seguinte.

A proposta é então enviada a um consórcio de peritos e sábios que estudarão a proposta para dar o seu parecer aprofundado, verificar o aspeto técnico e científico, estimar os recursos e o tempo a afetar, mas também, e sobretudo, verificar se está em conformidade com as constituições estabelecidas.

A constituição da Eutopia lançará as bases da sociedade, que terá de ser estudada e justificada como permitindo-nos alcançar o nosso objetivo comum de viver uma vida plena e sustentável em harmonia com a Terra. Depois, será necessário ensiná-la para despertar as pessoas para ela, para que oriente as suas propostas e decisões.

Uma vez terminado o trabalho dos peritos e dos sábios, os seus pareceres serão anexados à proposta final, para que os cidadãos disponham de todas as chaves necessárias para fazer a sua escolha. As pessoas designadas como peritos ou sábios serão eleitas pelos cidadãos das respectivas zonas. A transparência e a veracidade da informação fornecida nestes pareceres deverão ser asseguradas, bem como a simplicidade da informação, tudo para permitir que os cidadãos tenham todas as cartas na mão para fazerem a sua escolha e exercerem o seu poder com a consciência tranquila.

A proposta será então enviada novamente a um grupo de amostragem com as informações e pareceres emitidos pelos peritos e sábios. Se a proposta for novamente aprovada, é então enviada a toda a população para adoção final. O resultado deverá então ser o mesmo que o da amostra. No entanto, quando os cidadãos receberem esta proposta para a votação final, saberão antecipadamente que mais do que a maioria tenciona aceitá-la, o que tem a vantagem de lhes dar uma última oportunidade para se retirarem.

Assim, o cidadão não recebe todas as propostas, mas apenas a que lhe diz respeito, consoante a decisão seja aplicada mais local ou globalmente.

Antes de enviar a sua proposta, criámos também um sistema de colaboração que aconselhamos vivamente a utilizar: a sua proposta é então, antes de mais, uma sugestão, e estas sugestões estão acessíveis a qualquer pessoa que as queira fazer, e qualquer pessoa pode participar nelas.

Pode também delegar o seu poder a alguém, que votará a proposta por si, mas terá acesso ao histórico das posições votadas para verificar se está de acordo com elas e pode retirar a sua delegação de poder a qualquer momento. Pode também escolher o nível de poder que deseja delegar em relação aos diferentes estratos da sociedade.

Finalmente, é este sistema democrático que também vamos utilizar para a edição colaborativa de histórias. Ou antes, à medida que testamos o nosso sistema, podemos iterá-lo e modificá-lo até estarmos totalmente satisfeitos com ele, e será esta versão final que será a democracia de Eutopia.

> "A democracia neste horizonte parece requerer tempo, coragem, perseverança... e talvez um toque de loucura..." Datageueule[^4]

## Fontes (Lista não exaustiva)

[^1]: [Wikipedia : Democracia ](https://fr.wikipedia.org/wiki/D%C3%A9mocratie)
[^2]: Serge-Christophe Kolm. As eleições são democracia? 1977
[^3]: David Shearman. The Climate Change Challenge and the Failure of Democracy (Política e Ambiente). 2007.
[^4]: [Datageule. Democracia(s) ?](https://www.youtube.com/watch?v=RAvW7LIML60)
[^5]: [Science4All : O Princípio Fundamental da Política](https://www.youtube.com/watch?v=4dxwQkrUXpY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^6]: Bruce Bueno de Mesquita, Alastair Smith, Randolph M. Siverson. The Logic of Political Survival (Mit Press). 2004
[^7]: Bruce Bueno de Mesquita. Prediction: How to See and Shape the Future with Game Theory [Previsão: Como ver e moldar o futuro com a teoria dos jogos]. 1656
[^8]: [Science4All. Hassiez le jeu, pas les joueurs. Démocratie 9](https://www.youtube.com/watch?v=jxsx4WdmoJg&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=9)
[^9]: [Science4All.Foster honestidade | Democracia 18](https://www.youtube.com/watch?v=zRMPT9ksAsA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=18)
[^10]: Raymond J. La Raja e Brian F. Schaffner. Campaign Finance and Political Polarization: When Purists Prevail [Financiamento de Campanhas e Polarização Política: Quando os Puristas Prevalecem]. 2015
[^11]: [Science4All. Our Democracies Divide | Democracy 2](https://www.youtube.com/watch?v=UIQki2ETZhY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=2)
[^12]: [Science4All. The Randomized Condorcet Ballot | Democracy 5](https://www.youtube.com/watch?v=wKimU8jy2a8&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=5)
[^13]: [Science4All. És um hooligan político? Democracia 10](https://www.youtube.com/watch?v=0WfcgfGTMlY&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=10)
[^14]: Jonathan Haidt. The Righteous Mind: Why Good People are Divided by Politics and Religion [A Mente Justa: Porque é que as pessoas boas estão divididas pela política e pela religião]. 2013
[^15]: [Science4All. Rationally Irrational | Democracy 11](https://www.youtube.com/watch?v=MSjbxYEe-yU&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=11)
[^16]: Bryan Caplan. The Myth of the Rational Voter: Why Democracies Choose Bad Policies [O Mito do Eleitor Racional: Porque as Democracias Escolhem Más Políticas]. 2008
[^17]: [The Bias Work #5: Confirmation Bias](https://www.youtube.com/watch?v=6cxEu-OP5mM)
[^18]: [AmazingScience - The Halo Effect - Stupid Brain #1](https://www.youtube.com/watch?v=xJO5GstqTSY)
[^19]: [O Bias Bracket #3- Dissonância Cognitiva](https://www.youtube.com/watch?v=Hf-KkI2U8b8)
[^20]: [Academia Franklin Templeton - Viés de disponibilidade](https://www.youtube.com/watch?v=2n3ITCIpd1Y)
[^21]: [Science4All. O pequeno comunitarismo tornar-se-á grande | Democracia 6](https://www.youtube.com/watch?v=VH5XoLEM_OA&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=6)
[^22]: [Conferência TED. Eli Pariser alerta para as "bolhas de filtragem" em linha](https://www.youtube.com/watch?v=B8ofWFx525s)
[^23]: [Science4All. Querida convicção, transforma-te numa infeção VIRAL !!! Democracia 7](https://www.youtube.com/watch?v=Re7fycp7vIk&list=PLtzmb84AoqRSmv5o-eFNb3i9z64IuOjdX&index=7)
[^24]: Michel Diard. Concentração dos media: os bilionários informam-vos. 2016
