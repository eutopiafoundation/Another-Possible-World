---
title: Imaginar um outro mundo
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

Como é que se imagina um mundo novo, um ideal, uma Eutopia?

## O sistema, os seus constrangimentos e incentivos

Se atualmente parecemos estar num impasse perante as alterações climáticas e os outros desafios do nosso tempo, é porque os fundamentos do nosso sistema geram constrangimentos e incentivos que acabam por impedir qualquer mudança efectiva sem perturbar a coerência estabelecida, o que arrasta a nossa sociedade para uma inércia destrutiva que ninguém parece ser capaz de parar.

Em cada eleição, os nossos políticos tentam convencer-nos das suas propostas e, à primeira vista, todos pensam que estão a fazer a coisa certa, que têm a solução que pode melhorar a vida quotidiana das pessoas que defendem. Mas estão a lutar entre si com estes constrangimentos e incentivos, e estes não podem conduzir a uma solução duradoura e desejável. Assim, impotentes perante o sistema, não têm outra opção senão deixarem-se governar por ele.

Na medida em que alguma vez o tivemos, nós, humanos, perdemos o nosso poder, mas, pior ainda, aqueles que elegemos para nos representar também o perderam.

Pensamos que o sistema político domina e que pode mudar tudo o resto, incluindo a economia e a sua forma. Que estamos numa democracia por causa da competição entre os candidatos em que votamos.

Mas um sistema económico não se limita a produzir bens e serviços, define as condições da nossa socialidade, produz seres humanos e as relações entre eles. [^1]

Veremos que a economia intervém mesmo nesta escolha democrática competitiva. Que ela a influencia e que, em consequência, o sistema eleitoral já não tem a qualidade de oferecer uma boa concorrência entre os candidatos. Assim, no final, não é a política que domina a economia, mas a economia que domina a política. [^2]

O que propomos aqui é uma limpeza do nosso sistema, uma saída completa de tudo o que constitui o nosso imaginário coletivo atual. Convidamo-vos a partir de uma nova base, a descobrir um Mundo Novo muito diferente daquele que conhecem, um mundo baseado no sentido e na lógica, e depois a elaborar em conjunto os caminhos e as soluções para uma transição para ele.

## Começar do zero

Então, o que é a base?

A base é a realidade, é o que realmente existia no início da existência da humanidade, quando ainda não podíamos contar histórias imaginárias a nós próprios. A base é tudo o que resta depois de termos apagado esta imaginação humana colectiva.

Assim, temos a Terra e tudo o que a compõe. Em particular, os seus diferentes ecossistemas: um ecossistema é um conjunto formado por uma comunidade de seres vivos que interagem com o seu ambiente, podendo estes seres vivos ser plantas ou animais. O planeta é um grande sistema no qual um número infinito de subsistemas interage e se equilibra dinamicamente. De entre todos os seres vivos do nosso planeta, existe-nos a nós, Homo Sapiens, mais vulgarmente conhecidos como Humanos.

Somos 7,9 mil milhões de seres humanos e prevê-se que seremos 11 mil milhões em 2100[^3]. Estamos agora em quase todo o planeta e, mesmo em zonas pouco povoadas, os seres humanos continuam a ter um impacto no ambiente e nos ecossistemas[^4]. No entanto, o problema não é a dimensão da população, mas as interacções que temos nos seus ecossistemas.

Por isso, começar pela base significa, antes de mais, que temos de compreender a base. Temos de compreender o mundo e o seu funcionamento. E como nós, seres humanos, vivemos nele, também temos de perceber como funcionamos. Uma parte será dedicada exclusivamente à Terra e outra parte será dedicada a uma compreensão profunda do ser humano.

Estas duas questões de investigação são muito vastas e exigem muitos conhecimentos, mas não precisamos de entrar em todos os pormenores. Vamos concentrar-nos no essencial, reuni-lo e resumi-lo para responder às questões que se seguem.

## Perguntas de investigação

Uma vez adquiridas as competências necessárias para compreender o mundo e os seres humanos, precisamos de um objetivo sob a forma de uma pergunta de investigação que estabeleça o nosso desejo quanto ao que procuramos num mundo ideal.

Esta questão de investigação é muito importante, porque orientará a investigação a partir da qual as nossas soluções surgirão. Perguntas de investigação diferentes conduzirão a resultados diferentes. As perguntas devem ser tão simples quanto possível, claras e precisas, mas devem captar a essência do que pretendemos.

Concentrar-nos-emos na nossa espécie, mas, como já foi referido, vivemos em ecossistemas e, ao estudarmos o mundo, descobriremos que estamos diretamente ligados a eles e que, portanto, dependemos deles para a nossa sobrevivência.

Procuramos o ideal. Naturalmente, o ideal depende dos valores, crenças, aspirações e experiências de cada indivíduo. Assim, podemos esperar que o ideal para cada indivíduo seja viver uma vida plena e equilibrada que satisfaça as suas necessidades e aspirações.

É esse o nosso objetivo, que cada ser humano tenha uma vida plena durante o seu tempo na Terra. Mas lembre-se que ainda faltam 80 milhões de gerações, pelo que esta investigação se aplica às gerações actuais, mas também tem em conta todas as gerações vindouras.

Com estes vários elementos em mente, eis a formulação da nossa pergunta de investigação:

> Como podemos permitir que todos os seres humanos actuais e as 80 milhões de gerações vindouras desfrutem de uma vida plena na Terra?
> {.is-success}

O nosso grande projeto coletivo é, portanto, garantir que todos os seres humanos tenham uma vida plena na Terra que satisfaça as suas necessidades e aspirações, tanto para as gerações actuais como para as 80 milhões de gerações vindouras.

Quando tivermos uma boa compreensão de como o mundo e os seres humanos funcionam, teremos descoberto como os seres humanos podem ser realizados, como podemos dar sentido à sua existência e como podem viver a longo prazo no seu ambiente na Terra.

Para completar esta investigação, percorreremos rapidamente as diferentes épocas da humanidade, porque conhecer o passado é a base e a inspiração para interpretar o presente [^5] e é preciso conhecer o presente para determinar uma possível transição futura. Porque a melhor sociedade possível é a melhor evolução possível das sociedades a partir de agora. [^1]

Para atingir este objetivo comum, os nossos 7,7 mil milhões de seres humanos terão de realizar 2 acções essenciais: colaborar e decidir.

## Subquestão: Colaboração

Como em qualquer sociedade, vamos ter de trabalhar em conjunto para fornecer os bens e serviços de que precisamos para prosperar. A nossa primeira grande sub-pergunta será, portanto, :

> Como podemos permitir que as pessoas trabalhem em conjunto para fornecerem umas às outras os bens e serviços de que precisamos para atingir este objetivo comum?
> {.is-success}

Este é o domínio da economia. É um dos grandes problemas do nosso sistema e de onde provém a maior parte dos constrangimentos e incentivos. Começar do zero e procurar a melhor economia possível permitir-nos-á definir as bases necessárias para uma sociedade possível ideal, evitando os constrangimentos e incentivos que entram em conflito com a sua realização. Como vamos descobrir, um sistema económico não produz apenas bens e serviços, produz também seres humanos e as relações entre eles, que são igualmente essenciais para a sua realização.

## Sub-questão: Decisões

Finalmente, os nossos 7,7 mil milhões de seres humanos, ao longo da sua vida e do desenvolvimento das suas sociedades, terão de fazer escolhas, chegar a acordos e tomar decisões em pequena e grande escala. Esta é a nossa segunda grande sub-pergunta:

> Como podemos tomar decisões em conjunto e garantir que essas decisões nos permitem alcançar o nosso objetivo comum?
> {.is-success}

Como se pode ver na pergunta, a ideia não é apenas chegar a um acordo, porque de que serve ter toda ou parte da população de acordo sobre uma decisão, e tê-la validada, se no final não nos permite atingir o nosso objetivo comum.

Este domínio é a ciência da tomada de decisão e aplica-se a muitas práticas, incluindo a política, onde atualmente utilizamos a democracia representativa.

Vamos analisar mais aprofundadamente as diferentes possibilidades, tendo em conta o que aprendemos sobre o funcionamento do ser humano e do mundo.

## Conclusão

Resumindo, vamos começar por compreender o nosso habitat, a Terra, como funciona, os seus ecossistemas e o nosso lugar dentro dela. De seguida, vamos compreender em profundidade o ser humano, o seu funcionamento e as suas necessidades para garantir o seu desenvolvimento durante o seu tempo na Terra.

Este conhecimento permitir-nos-á responder eficazmente à questão de saber como permitir que os seres humanos tomem decisões e cooperem para atingir o objetivo comum do desenvolvimento sustentável de todos e de cada um deles.

O funcionamento da Terra no capítulo 2, a compreensão dos seres humanos no capítulo 3 e a revisão das épocas da humanidade no capítulo 4 podem parecer aborrecidos de ler se já estiver familiarizado com estas áreas ou se não quiser aprofundar muito no início. Nesse caso, aconselhamos que passe diretamente aos capítulos 5 e 6, onde descobrirá uma nova forma de trabalhar em conjunto e de tomar decisões. Estas 2 partes são essenciais porque explicam como a sociedade que vamos imaginar é possível, criando as bases que a tornarão coerente. No entanto, os capítulos 2, 3 e 4 não são menos importantes e conduziram à realização e justificação das soluções dos capítulos 5 e 6. Se o desejar, foram elaborados resumos que [estão disponíveis aqui](/pt/4-Apêndices/1Resumos)

> Conhecimento é poder. Quanto mais compreendermos o mundo e a nós próprios, mais teremos a capacidade de o mudar para melhor. Malala Yousafzai

## Fontes (lista não exaustiva)

[^1]: Serge-Christophe Kolm, La bonne économie. 1984
[^2]: Serge-Christophe Kolm, As eleições são democracia?/ 1977
[^3]: [World Population Prospects 2022" publicado pelas Nações Unidas](https://desapublications.un.org/file/989/download)
[^4]: [Relatórios do IPCC 2013](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^5]: Valérie Jousseaume, "Plouc Pride": uma nova narrativa para o mundo rural. 2021
