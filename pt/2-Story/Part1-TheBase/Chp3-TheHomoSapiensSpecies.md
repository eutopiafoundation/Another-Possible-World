---
title: A espécie Homo Sapiens
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Homo Sapiens

A espécie humana pode ser dotada de uma inteligência formidável e de uma consciência e conhecimentos tecnológicos cada vez maiores, mas é evidente que, quando confrontados com as consequências presentes e futuras das alterações climáticas, com números que as sustentam, somos incapazes de mudar de rumo[^1].

Daí a necessidade de compreender o ser humano e o seu funcionamento, pois só assim podemos garantir que o ambiente criado na nossa Eutopia é possível e que podemos prever o nosso comportamento no mesmo.

A compreensão das "fraquezas" humanas permitir-nos-á adaptar e desenvolver ferramentas em conformidade, de modo a podermos explorá-las para criar uma força.

Vamos compreender os mecanismos a nível individual, os seus diferentes aspectos físicos e psicológicos... Mas esta abordagem seria insuficiente e é acompanhada de toda uma série de considerações globais e sociológicas.

<!-- ## Uma breve história do Homo sapiens -->

## As necessidades do Homo Sapiens

Voltemos à nossa questão de investigação:

"Como é que podemos permitir que todos os humanos tenham uma vida plena na Terra durante as próximas 80 milhões de gerações?"

Partimos do princípio de que uma vida plena para cada ser humano é uma vida plena que satisfaz as suas necessidades e aspirações. Uma necessidade é uma necessidade sentida, seja ela física, social ou mental.

### Classificação das necessidades

Para nos ajudar na nossa investigação, começaremos com a pirâmide de Abraham Maslow, formulada em 1943. Esta classifica as necessidades humanas em 5 fases e constitui uma base interessante para uma visão geral das principais necessidades dos seres humanos[^2].

Note-se que este modelo está a ser posto em causa atualmente, porque, segundo Maslow, só sentimos uma necessidade superior quando a anterior foi atingida. Mas estas necessidades não são de forma alguma piramidais e muitos avanços demonstraram que as necessidades sociais são tão importantes como as necessidades fisiológicas, o que não significa que devam estar na base da pirâmide, mas sim que são omnipresentes a todos os níveis e que todos as sentem ao mesmo tempo[^3].

Mas, em todo o caso, não precisamos de perceber exatamente que necessidade é mais importante do que outra e em que circunstâncias, porque o objetivo aqui é satisfazê-las a todas sem exceção.

A seguir, completaremos a pirâmide, actualizando a teoria à luz da evolução dos conhecimentos psicológicos e neurocientíficos.

Pirâmide de Maslow](https://definitionmarketing.fr/medias/2021/04/21/besoins-de-deficience-et-besoins-de-coissance.jpeg)

No primeiro nível temos as necessidades fisiológicas, que são as necessidades básicas necessárias para a manutenção da vida e o bom funcionamento do organismo, como comer, beber, sexualidade, dormir, respirar...

É de salientar a total incapacidade do nosso sistema atual para satisfazer as necessidades básicas de toda a população. Atualmente, 25.000 pessoas morreram de fome, incluindo 15.000 crianças com menos de 5 anos[^4]. Do mesmo modo, no que diz respeito à sexualidade, esta é muitas vezes tabu, estigmatizada, discriminada e deseducada, o que leva a numerosos distúrbios, transmissão de doenças, actos de não consentimento, traumas, problemas de saúde mental e física, falta ou dependência de prazeres, falta de sentido de liberdade e de confiança, o que cria uma falta de bem-estar global com a própria sexualidade para uma grande parte da população.

No segundo andar, temos a necessidade de segurança. A segurança de ter um abrigo, como um sítio para viver, a segurança de ter acesso aos recursos desejados, como equipamento, ferramentas, jogos... A segurança física contra a violência, a agressão, etc. Segurança moral e psicológica, segurança e estabilidade emocional, segurança médica/social e segurança sanitária.

Mais uma vez, constatamos as diversas carências da nossa sociedade também neste domínio, mesmo nos países ocidentais, onde a segurança do alojamento é um verdadeiro desafio para muitas pessoas, nomeadamente para os sem-abrigo, que se vêem muitas vezes confrontados com condições precárias e desumanas[^5]. O acesso aos recursos pode também ser limitado para algumas pessoas em função do seu estatuto socioeconómico, o que pode afetar o seu bem-estar e a sua qualidade de vida. Em termos de segurança médica e psicológica, há falta de profissionais de saúde mental, o que dificulta o acesso aos cuidados. Mesmo com a segurança social em países como a França, o custo dos cuidados pode ser proibitivo para algumas pessoas, impedindo-as de receber os cuidados de que necessitam[^6].

O terceiro nível são as necessidades de pertença, a necessidade de amar e ser amado, de ter relações íntimas, de ter amigos, de fazer parte de um grupo coeso, de se sentir aceite, de não se sentir sozinho ou rejeitado.

Vivemos numa sociedade em que o individualismo é generalizado e/ou as relações humanas são frequentemente negligenciadas em favor do sucesso pessoal. Uma grande parte da população sente-se cada vez mais só e sente uma perda de laços sociais. Alguns são rejeitados e isolados, o que conduz a problemas de saúde mental como a depressão e a ansiedade. [O nosso modelo social favorece a competição generalizada e as relações egoístas ou comerciais como único modelo relacional, promovendo uma verdadeira cultura do egoísmo.

O quarto nível representa as necessidades de estima: o ser humano tem necessidade de se sentir considerado, apreciado e respeitado pelos outros, de se sentir reconhecido pelas suas realizações e de se sentir capaz de ter êxito. Estas necessidades são satisfeitas fazendo coisas que são valorizadas pela sociedade, estabelecendo objectivos e trabalhando para os atingir, ou envolvendo-se em actividades que permitam o desenvolvimento pessoal.

O quinto nível é a necessidade de auto-realização. Trata-se da necessidade de se realizar, de explorar e de tirar o máximo partido do seu potencial pessoal. Manifesta-se no desejo de desenvolver os seus talentos e aptidões, de se envolver em actividades que tragam significado e satisfação e de se superar a si próprio.

Na nossa sociedade, é difícil satisfazer esta necessidade de realização pessoal, uma vez que as pressões sociais e as elevadas expectativas impostas à maioria dos indivíduos podem dificultar a prossecução das suas paixões e interesses pessoais. A nossa sociedade altamente competitiva e a sua alienação do indivíduo deixam pouco tempo para explorar e desenvolver talentos e competências. Muitas pessoas são também excluídas do sistema, nomeadamente as pessoas com deficiência que não conseguem integrar-se no sistema porque este não está adaptado às suas necessidades. [^9]

No entanto, esta necessidade de auto-realização foi um pouco posta em causa em 2010 pelo trabalho de 4 psicólogos, que propuseram uma atualização da teoria da evolução. [Esta suprime a necessidade de realização pessoal para dar lugar a três necessidades ligadas à pulsão reprodutiva: o encontro, a continuidade e a posteridade.

|     |     |
| --- | --- |

| Necessidade de posteridade: adiar a morte inevitável.
Necessidade de continuidade: conservar o parceiro, o estatuto, etc. | repelir a perda | Necessidade de encontro: conservar o parceiro, o estatuto, etc. | repelir a perda
| Necessidade de encontro: Repelir a concorrência e o isolamento social.
| Necessidade de estima e de estatuto: ser reconhecido como um ser único.
| Necessidade de pertença ou de filiação: ser amado e aceite pelo que é.
| Necessidade de proteção: abrigo, segurança +física, familiar, psicológica, financeira, etc. | Repelir a violência
| Necessidades fisiológicas: respirar, beber, comer, aliviar-se, dormir, manter-se quente.

\*Lista das sete necessidades humanas fundamentais inconscientes e concomitantes de D.Kenrick, V.Griskevicius, S. Neuberg, M. Schaller, Renovating the Pyramid of Needs,

Estas necessidades são inconscientes, necessidades evolutivas que se exprimem de mil formas diferentes na vida de cada pessoa. Por exemplo, a criatividade artística pode basear-se tanto na necessidade de adquirir estatuto e de ser reconhecido socialmente como um ser único, como na necessidade de seduzir parceiros ou de perdurar no tempo, deixando uma obra para a posteridade.

Acabámos de passar em revista os 5/7 tipos de necessidades, mas é de notar que, no final da sua vida, Abarham Maslow acrescentou uma 6ª necessidade, a "superação de si próprio". Esta situa-se no topo da pirâmide:

> "O ser humano plenamente desenvolvido que trabalha nas melhores condições tende a ser motivado por valores que transcendem o seu 'eu'"[^9].

É interessante perceber que um ser humano realizado, que vai além da auto-realização, estaria totalmente aberto a ir além da sua individualidade para se empenhar ao serviço dos outros.

Para concluir, estas necessidades podem também ser complementadas com as reflexões de Hannah Arendt.

Na nossa condição humana, somos levados a uma busca de eternidade: a vita contemplativa. Uma procura que é cada vez mais difícil de satisfazer nas nossas sociedades modernas, que já não acreditam em Deus. Somos também levados a uma busca de imortalidade, a vita ativa. [^10]

A busca da imortalidade é o que leva um ser humano mortal a dizer e a fazer coisas criativas que ultrapassam a sua própria necessidade vital, a sua própria existência, e o projectam para o futuro, para além da sua própria morte.

Inicialmente, a era da modernidade reconheceu socialmente a necessidade de imortalidade, por exemplo, através do heroísmo dos soldados, através de títulos e lemas, ou através da coragem e autenticidade de um discurso intransigente: "Só se pensa, só se fala com força do fundo do túmulo: é aí que se deve colocar, é aí que se deve dirigir aos seres humanos" (Diderot, 1782). Hoje, todos estes títulos, todas estas palavras, todas estas acções, já não têm eco na nossa sociedade. Hannah Arendt argumenta que a repressão social da busca da eternidade fora do domínio público, juntamente com a repressão social da busca da imortalidade, explica como "a era moderna - que começou com uma explosão de atividade humana tão nova, tão rica em promessas - terminou na mais inerte e estéril passividade que a história alguma vez conheceu. [^10][^11]

Emmanuel Todd defende um ponto de vista semelhante. Ele argumenta que a transição do "Homem que acredita no Homem" para o "Homem que acredita em Deus" gerou uma energia colectiva significativa. O afastamento da busca da eternidade reforçou as energias em direção à busca da imortalidade. Quando a Igreja como quadro social se desmoronou, desenvolveu-se um novo ideal humanista secular. Era a imensa esperança no Homem Novo que os movimentos revolucionários do século XIX traziam consigo. Por outro lado, o desmoronamento do ideal social comunista e do ideal revolucionário republicano marca o fim social da busca da imortalidade. "O Homem que acredita no Homem desaparece por sua vez e torna-se o Homem que já não acredita em nada, um ser humano niilista, apático, sem esperança nem objetivo, que preenche o seu vazio existencial através do consumo." [^12]

O que é que satisfaz a nossa necessidade de posteridade? Segundo Hannah Harendt, é a obra. Uma obra é aquilo que é capaz de deixar uma posteridade, ou seja, uma criança. Significa também plantar árvores sob as quais a passagem do tempo não nos permitirá vir fazer sombra. É transmitir algo às crianças. Significa escrever poesia ou esculpir pedaços de madeira. Significa sacrificar heroicamente a sua vida. Significa fazer um trabalho que tenha significado. Oferece segurança interior e auto-controlo. Através das suas acções, as pessoas mostram ao mundo o seu trabalho único e dão um vislumbre da sua imortalidade. O trabalho de cada um, criado em privado e oferecido em público, é o que faz do mundo humano um mundo. [^10]

O trabalho de subsistência é mera sobrevivência. Segundo o filósofo, o trabalho é uma necessidade vital que sustenta a nossa existência fisiológica. O trabalho é uma dimensão estreita da vida humana sobre a qual não temos qualquer poder real. Hannah Arendt lamenta o facto de o projeto de vida proposto coletivamente ter sido atrofiado pelo lugar incrível ocupado, nas sociedades modernas, pelo trabalho de subsistência. Para ela, o trabalho deve ser relegado para a esfera privada e ter uma importância secundária. O trabalho deve estar no centro da vida social pública.

A reflexão filosófica de Hannah Arendt sobre a importância excessiva do trabalho de necessidade vital e o desaparecimento do trabalho da posteridade fornece muitas ligações explicativas para a situação atual. Ajuda-nos a compreender o desaparecimento dos cidadãos, substituídos por consumidores. Permite-nos compreender a absoluta desordem existencial que o desemprego pode produzir numa sociedade que esqueceu o trabalho e que se organiza publicamente apenas em torno do trabalho de subsistência. Ajuda-nos a compreender a atitude das novas gerações face ao trabalho de subsistência e a sua procura de sentido numa profissão. Finalmente, permite-nos examinar a pirâmide das necessidades nas facetas do ser humano.

Do ponto de vista físico, sobreviver significa reproduzir-se e os diferentes têm filhos. Do ponto de vista espiritual, significa ser eterno. Do ponto de vista social, ser imortal significa deixar um rasto do seu tempo na terra. A busca da imortalidade, tal como Hannah Arendt a entende, é a expressão social da necessidade de posteridade que permeia todos os aspectos da nossa vida física, psicológica, social e espiritual. Neste sentido, a descendência encontra-se com a transcendência. Recorde-se que, tanto para Hannah Arendt como para Kenrick Douglas e os seus colegas, a necessidade de posteridade combina duas necessidades: a produção íntima de uma obra completamente única e pessoal, combinada com uma relação com o mundo que a possa manifestar.

Esta necessidade multidimensional de posteridade é completamente ofuscada pela nossa sociedade funcionalista e utilitarista. E, no entanto, é aqui, nestes actos que não têm qualquer significado económico ou racional aparente para a vida quotidiana ou para o desenvolvimento de benefícios imediatos, que encontramos significado, grandeza. A sociedade moderna atual, ao propor uma vida limitada à sobrevivência vital e ao desenvolvimento material, cortou progressivamente o acesso à satisfação da necessidade de posteridade. O resultado é um sentimento persistente de insatisfação e de insatisfação, numa sociedade onde, paradoxalmente, reinam a abundância e o prazer. É aqui que entra a procura de sentido. Esta procura de sentido parece não encontrar reconhecimento na sociedade hipermoderna. [^11]

Eis, portanto, as necessidades humanas fundamentais completadas pelas reflexões de Hannah Arendt. As setas representam as diferentes dimensões do corpo humano segundo a medicina oriental. Esta representação foi escolhida antes das distinções clássicas da medicina ocidental moderna, porque alarga o espetro e inclui a vita comtemplativa de Hannah Arendt. As setas estão pontilhadas para indicar que estão fora do âmbito da ciência atual.

[Necessidades humanas básicas complementadas com as reflexões de Hannah Arendt](./../../0-GlobalProject//Image/besoin-fondamentaux-hannah-arendt.png)]

### Necessidades ou desejos intrínsecos e extrínsecos

Existe uma diferença entre necessidades ou desejos intrínsecos e extrínsecos.

As necessidades intrínsecas são motivações internas e pessoais que vêm de dentro do indivíduo.

As necessidades extrínsecas são motivações externas que provêm do ambiente e do indivíduo. Estão relacionadas com a imagem que o indivíduo quer projetar para os outros e até para si próprio. O consumo pode satisfazer necessidades de ambos os tipos ao mesmo tempo, e a maioria dos consumos que satisfazem necessidades intrínsecas também têm uma dimensão extrínseca.

As necessidades extrínsecas estão intimamente ligadas a várias normas sociais, ao estatuto, ao comportamento ético e ao imaginário coletivo. Por vezes, a satisfação das necessidades extrínsecas tem uma desvantagem em relação às necessidades intrínsecas[^13].

A distinção social e a imitação criam desejos extrínsecos.

Todas as sociedades criam desejos e necessidades extrínsecos.

Na nossa sociedade, estes desejos são constante e poderosamente manipulados: a educação e o ambiente social inculcam modelos de estilo de vida, ou muito frequentemente o sentimento de que mais é melhor, e a publicidade cria desejo atrás de desejo[^14].

A insatisfação resulta do desfasamento entre os desejos e a sua realização ou, por outras palavras, entre as aspirações e as realizações.

Para limitar esta insatisfação, há duas abordagens possíveis: reduzir o desejo ou satisfazê-lo. A tónica é frequentemente colocada no segundo meio, a satisfação do desejo, mas a existência do primeiro é quase totalmente esquecida ou negligenciada.

Assim, o que temos de fazer primeiro é compreender como se formam os nossos desejos e como podemos controlá-los. E é isso que vamos fazer.

## Natureza Humana

Mas, antes de mais, é essencial fazer um balanço da natureza humana, porque é um ponto que surge regularmente no debate. As ciências sociais e as neurociências há muito que tentam distinguir entre o que é inato e o que é adquirido, mas não há consenso sobre a natureza humana.

Enquanto as ciências naturais pareciam apoiar o utilitarismo durante mais de meio século, é agora possível contradizer esta hipótese: o homem não é apenas egoísta, mas também não é perfeitamente racional. Este não utilitarismo do ser humano aplica-se às formas de vida mais primitivas: a cooperação está inscrita desde as origens e seria, com a sobrevivência dos mais aptos, um dos motores essenciais da evolução[^15].

Acreditar que a nossa evolução se baseia exclusivamente na competição inter-individual é ter uma visão parcial da evolução, quando o papel da cooperação já estava presente na teoria evolutiva darwiniana. [^16]

Mas qualquer que seja a natureza humana, os inúmeros exemplos da história provam que ela pode tornar-se em qualquer coisa: tanto no pior como no melhor.[^17][^18] A natureza humana não deve justificar nada sobre aquilo em que nos queremos tornar. Podemos certamente decidir o que queremos ser na nossa sociedade, porque simplesmente temos a oportunidade de o fazer[^19].

O que é certo, porém, é que os seres humanos não são eremitas; vivem em sociedade e são sociáveis. O ser humano é, antes de mais, um homo sociabilis, com uma propensão para se aproximar dos seus semelhantes e interagir com eles. Têm necessidade de falar, de trocar informações, de partilhar conhecimentos e emoções, mas também têm uma necessidade fundamental de existir aos olhos dos outros. Procuram a interação humana pela sociabilidade que esta lhes proporciona e pela troca de imagens positivas e gratificantes de que se alimentam. [^20][^21]

## Moldar os nossos desejos

Dissemos anteriormente que devemos primeiro compreender como se formam os nossos desejos e como podemos controlá-los.

Há mais de um bilião e meio de anos que temos um pequeno órgão no nosso crânio chamado striatum.

O striatum liberta dopamina, uma substância que nos dá uma sensação de prazer e que vai reforçar um comportamento identificado como um sucesso. [^22]

Faz parte de todo um mecanismo de cooperação e de diálogo entre diferentes estruturas e participa na decisão, recebe informações e compara as diferentes opções, os ganhos potenciais, o esforço necessário, o imediatismo da recompensa.

Este sistema é frequentemente designado por circuito de recompensa, mas a dopamina é apenas uma hormona que prevê o resultado de uma ação. [^23]

Estas descargas de dopamina dão origem a incentivos que nos levam a agir e que vão reforçar esse comportamento ou atividade no futuro.

Estes reforços, a que também podemos chamar motivações profundas do nosso cérebro, são : [^22]

- A comida
- sexo
- Posição social
- Necessidade de informação
- Menor esforço
- Estimulação intelectual

Estas motivações foram essenciais para a nossa sobrevivência e para o desenvolvimento da nossa espécie nos tempos primitivos e continuam a ser muito sensíveis atualmente. Os neurónios do estriado e de todo o circuito de recompensa atraem-nos para o que satisfaz os nossos reforçadores, ou seja, tudo o que proporciona alimento, sexo, estatuto, facilidade e informação.
Não são intrinsecamente nocivos, mas, como podemos ver atualmente, podem ser explorados para desenvolver economias produtivistas, consumistas, individualistas e destrutivas do ambiente.

O que há de especial neste mecanismo, que condiciona o nosso cérebro não só a procurar comida, dinheiro, sexo e estatuto social, é que ele procura obter cada vez mais dinheiro, cada vez mais comida, cada vez mais sexo ou cada vez mais estatuto.

Quando um rato é colocado perto de um corredor com um pedaço de queijo no fim, os seus neurónios libertam dopamina assim que ele entra no corredor. Da primeira vez que encontra um pedaço de queijo no fim do corredor, os seus neurónios libertam dopamina; da segunda vez, se encontrar o queijo, os seus neurónios dopaminérgicos não fazem nada. Mas se ele encontrar dois pedaços de queijo em vez de um, os seus neurónios dopaminérgicos voltam a descarregar. [^22]

O striatum só faz tudo isso na medida em que pode fazer mais e mais.

Assim, só conseguimos estimular os nossos circuitos de prazer aumentando as doses.

Outro dos nossos mecanismos é que o prazer e a facilidade que podemos ter agora têm cem vezes mais peso nas nossas decisões do que a consideração de um futuro distante. Quanto mais distante for o futuro de um benefício, menos valor ele tem para o nosso cérebro. Isto é conhecido como desvalorização temporal.

Privilegiamos o prazer imediato em detrimento do prazer futuro, ou fazemos uma escolha que nos beneficia agora mas que terá consequências nefastas a longo prazo.

Quanto maior for o atraso, mais fraca é a resposta antecipatória[^24].

Existem certamente diferenças genéticas entre as pessoas, e algumas são mais sensíveis ao seu striatum, mas são sobretudo os elementos ligados à educação e ao ambiente sociocultural que se revelam decisivos. [^25][^26]

Estes neurónios estão de facto na origem dos nossos processos de aprendizagem

Condicionam-nos: quando aprendemos que a comida segue um gesto, o próprio gesto torna-se atrativo. É o princípio da aprendizagem social: somos condicionados a gostar deste ou daquele comportamento. O princípio do condicionamento operante reina num local de aprendizagem por excelência, a escola.

O egoísmo e o altruísmo são ambos igualmente motivados pelo prazer.

Quando damos uma prenda a alguém de quem gostamos, não nos ocorre conscientemente fazê-lo para nos sentirmos bem, e no entanto sentimo-nos bem.

Por isso, podemos perguntar-nos: se o altruísmo nos faz sentir bem pessoalmente e, além disso, temos consciência disso, isso continua a ser altruísmo ou não é mais uma espécie de interesse próprio bem colocado?

Há uma linha ténue entre o altruísmo e o egoísmo, uma linha que nem sequer existe a nível biológico. Não há uma hormona do altruísmo ou uma hormona do egoísmo. Existe um ato para o qual o nosso cérebro calcula a probabilidade de saber se será benéfico para nós, e que nos dá prazer. As áreas envolvidas no cérebro são comuns, mas o que se nota é que há uma maior ativação do córtex pré-frontal dorsolateral no egoísmo, devido à avaliação mais acentuada dos cálculos de custo-benefício. [Mas, no geral, somos nós que classificamos os actos como altruístas ou egoístas, e esta definição pode, portanto, variar de acordo com os modos de pensamento.

Diremos que no ato egoísta existe apenas o objetivo de obter benefícios pessoais.

No ato altruísta, a intenção é a de dar benefícios aos outros, recebendo um benefício que é inconscientemente procurado. Mas também pode haver a intenção de dar um benefício a outrem, enquanto se recebe um benefício conscientemente procurado. [^28]

O ato altruísta é, portanto, uma troca recíproca que tem dois sentidos, enquanto o ato egoísta tem apenas um sentido.

Tanto o egoísmo como o altruísmo podem, portanto, conduzir ao prazer individual, e vamos descobrir qual destas duas motivações é a mais eficaz para atingir o nosso objetivo comum.

Assim, se algumas pessoas são mais generosas, isso deve-se provavelmente ao facto de os seus cérebros terem sido configurados dessa forma desde tenra idade. Tal como a Madre Teresa, conhecida pela sua dedicação infalível à ajuda aos outros, pela sua compaixão e pelo seu amor incondicional por todos os que encontrava, dedicou a sua vida a fazer o bem aos outros, sem se preocupar com a sua segurança ou conforto pessoal, e tornou-se um dos símbolos mais importantes da caridade e do altruísmo em todo o mundo. Quando tinha apenas 6 anos, a sua mãe levava-a a visitar os mais pobres dos pobres, os alcoólicos e os órfãos. E dava-lhe uma recomendação invariável e obstinada: "A minha filha nunca aceita um bocado de comida que não seja partilhada com os outros". A partilha, ensinada no berço de forma regular e com uma preocupação constante de aplicação e de exemplaridade, tornou-se uma forma tão poderosa de condicionamento que o estriado de Madre Teresa nunca vacilou nos oitenta anos da sua vocação. O seu ambiente e a sua educação ajudaram assim a desenvolver nela valores e motivações como a generosidade, a compaixão e o desejo de servir os outros[^29][^30] É claro que a religião, ao defender estes valores morais de generosidade e altruísmo, desempenha um papel importante na configuração do cérebro da Madre Teresa, e isto decorre da nossa procura de significado, que discutiremos mais adiante.

Estudos revelaram também a existência de um bónus de agilidade mental e de capacidade de resolução de problemas, que leva à ativação de zonas de memória e a uma melhor memorização da informação comunicada pelo professor. Assim, a dopamina recebida por um aluno quando obtém um bom resultado vai modular a plasticidade sináptica e consolidar as memórias. [^31]

É claro que o striatum não é a única parte do nosso cérebro que toma decisões. O córtex frontal é a parte mais anterior do cérebro humano e tem-se tornado cada vez mais espesso ao longo da evolução, reflectindo as nossas capacidades cognitivas crescentes. É a sede da força de vontade e do planeamento.

Por exemplo, quando foi pedido aos participantes que escolhessem entre 20 euros imediatamente ou 30 euros daqui a 2 semanas, os que aceitaram o dinheiro imediatamente viram o striatum acender-se, enquanto os que não aceitaram viram o córtex frontal acender-se[^32].

Assim, se quisermos formar uma comunidade humana capaz de enfrentar os desafios do futuro, são estas as ligações que temos de desenvolver através do trabalho árduo, da consciência e da perseverança.

## Controlar os nossos desejos

Para limitar os efeitos nefastos do nosso striatum, existem 3 soluções possíveis,

- Suprimir o nosso striatum: Historicamente, a atividade do striatum tem sido bloqueada por comandos morais e pelo esforço da vontade contra a tentação. Mas isto não é solução: se reprimirmos o nosso striatum, podemos afetar a nossa capacidade de sentir motivação ou prazer, o que pode levar a problemas como a depressão e várias perturbações. A repressão tem consequências negativas para o bem-estar e o funcionamento do indivíduo, pelo que não é uma solução eficaz nem sustentável. [^33]

- Se considerarmos o striatum no seu próprio jogo, podemos, de facto, jogar com os reforçadores primários para alcançar uma sociedade ideal, por exemplo, tornando a norma social ter um estilo de vida saudável e sustentável. Impulsionar o desenvolvimento dos reforçadores essenciais a uma boa sociedade.

- Mas uma solução mais eficaz e sustentável é recorrer à capacidade única do ser humano, a consciência. Porque a força do striatum vem do facto de os seus comandos não serem conscientes.

Quando os neurónios do striatum se habituam a um certo nível social, tornam-se baços e já não sentimos nada, pelo que é imperativo subir mais um degrau para os estimular. Este processo de incrementação não produz nenhuma satisfação duradoura, não pode trazer felicidade.

A maior parte das nossas acções são realizadas com um nível de consciência muito baixo. Na maior parte das vezes, actuamos mecanicamente. Mesmo quando nos entregamos a actividades intelectuais, seria errado dizer que o fazemos conscientemente. [^34]

Temos um córtex cerebral com um enorme poder de computação, que usamos principalmente para fins utilitários, de desempenho e técnicos. Durante vários milénios, o nosso poder de abstração, concetualização e planeamento foi utilizado sobretudo para conceber ferramentas que satisfazem o nosso striatum.

Somos seres com um elevado nível de inteligência mas com um baixo nível de consciência. [^22]

A inteligência desenvolve soluções, gera cálculos, implementa objectivos e programas. Mas pode fazer tudo isto sem consciência. O exemplo mais evidente é a inteligência artificial, que pode efetuar tarefas extremamente complexas sem qualquer consciência.

Os grandes sistemas de inteligência artificial que hoje conhecemos funcionam também com uma noção de probabilidade e com um sistema de reforço, que consiste em recompensar ou punir acções para favorecer a aprendizagem de comportamentos desejáveis. Por outras palavras, estes sistemas funcionam de forma semelhante à dopamina nos seres humanos.

Assim, se quisermos criar uma consciência na IA, só temos de compreender como é criada a nossa. Este mistério
ainda não é totalmente compreendido pelas neurociências, e voltaremos a esse assunto mais tarde. Mas se for e conseguirmos criar essa consciência, a diferença entre nós e uma máquina tornar-se-á muito ténue. Este possível avanço levanta um grande número de questões éticas e leva-nos a refletir sobre a perceção da nossa própria consciência.

Seja como for, parte da solução para os desafios que a nossa espécie enfrenta passa por acrescentar mais consciência às nossas acções quotidianas.

A consciência é também uma caixa de ressonância para as nossas percepções.

Ao desenvolvermos a nossa câmara de ressonância sensorial, podemos fazer com que o nosso striatum acredite que está a obter mais prazer, enquanto nós lhe estamos a dar menos em termos quantitativos.

Uma ferramenta muito prática para desenvolver a nossa consciência é a mediação. [^35]

Elevar o nosso nível de consciência a um nível comparável ao nosso nível de inteligência será, sem dúvida, um grande desafio para desenvolver uma boa sociedade e assegurar o futuro da nossa espécie. A evolução para uma sociedade de consciência e para uma economia de crescimento mental.

## A necessidade de sentido e de certeza

A partir do momento em que o indivíduo tem autonomia de escolha e de crença, ou seja, o direito de dar à sua vida a direção que deseja, definir essa direção tornou-se uma tarefa muito difícil. Uma vez que a direção já não é definida pela religião ou por um regime político totalitário, cada um de nós tem de criar o seu próprio sentido. No entanto, quando tomamos consciência de que a nossa existência é breve e está condenada ao nada, somos confrontados com a necessidade de encontrar uma justificação para a nossa existência e para as nossas acções, e esta constatação pode tornar-se insuportável. [^36]

Porque o sentido é mais importante para a vida do que qualquer outra coisa, ele tem um valor de sobrevivência para o ser humano. Assim, para compensar a nossa falta de sentido, vamos à procura de dinheiro, de estatuto, etc...

O sinal de prazer que é gerado a montante do que o animal procura é uma previsão feita pelo nosso cérebro sobre o que vai acontecer. Esta produção ocorre porque se trata de uma vantagem evolutiva, um mecanismo que aumenta as hipóteses de sobrevivência do animal.

Ao ser capaz de produzir o que vai acontecer com base no que observa à sua volta, o animal aumenta dez vezes o seu controlo e o seu poder de decisão. Pode procurar situações vantajosas e fugir de situações potencialmente perigosas. Estão um passo à frente da realidade.

O cérebro dos animais vertebrados inventou uma forma de fazer previsões e de se manter um passo à frente da realidade. Esta capacidade de estabelecer ligações entre o estado do ambiente num dado momento e o seu estado futuro é a base daquilo a que chamamos, numa espécie muito célebre como o Homo Sapiens, sentido.

Criámos um sentido da sociedade humana e sabemos que podemos ser aceites e encontrar o nosso caminho nela, desde que respeitemos as regras e os códigos estabelecidos. Esta sociedade não está num caos, tem uma ordem, e esta ordem inteligível é fundamentalmente tranquilizadora para nós.

A nossa tendência para detetar ligações de significado no nosso ambiente é tão desenvolvida e tão irreprimível que, por vezes, nos leva a discernir ligações onde elas não existem necessariamente.

Por exemplo, quando um guerreiro vai à caça de uma presa, coloca um colar e caça a presa com ele, da próxima vez que colocar o mesmo colar, o guerreiro receberá uma pequena sacudidela de dopamina sobre a possibilidade de capturar outra presa. O guerreiro sentir-se-á mais confiante quanto ao seu sucesso futuro.

Este sistema de antecipação ajuda a reduzir o sentimento de incerteza sobre o futuro. Observar, prever e antecipar acontecimentos futuros e reduzir a ansiedade fazem parte da noção de sentido. Esta vantagem é tão decisiva que há todas as razões para crer que foi selecionada pela evolução.

Detetar o sentido à nossa volta é tão crucial para a nossa sobrevivência que as situações em que esse sentido nos escapa desencadeiam uma ansiedade fisiológica aguda. Esta reação é provocada pelo nosso organismo como um instinto de sobrevivência[^37].

Utilizando ferramentas de imagiologia cerebral, podemos descobrir o que se está a passar no cérebro. Uma pequena faixa do córtex cerebral, situada alguns centímetros acima do striatum, entra em ação. Trata-se de uma dobra do córtex cerebral situada na interface entre os dois hemisférios cerebrais, eles próprios ligados ao striatum, e que se chama córtex cingulado anterior. Este córtex cingulado interno ilumina-se logo que as previsões feitas deixam de ser confirmadas pelo que realmente acontece.

Este sinal de alarme significa uma violação da previsão. Se demasiadas previsões forem invalidadas, torna-se difícil organizarmo-nos e temos a impressão de cair no caos. Sobrecarregado, este sinal de erro torna-se prejudicial para a saúde física e mental do indivíduo. Desencadeia uma poderosa reação de stress no corpo, com o córtex cingulado a ativar um circuito nervoso multi-ligado que conduz a um centro cerebral envolvido no medo e na ansiedade, a amígdala, depois às glândulas adrenocorticais situadas nos rins e a núcleos neuronais no tronco cerebral, que libertam hormonas como o cortisol e a noradrenalina, cujo efeito é colocar o corpo numa postura de fuga, até à paralisia, e provocar uma ansiedade que pode tornar-se existencial.

O nosso córtex cingulado desempenha assim o papel de um sinal de alarme que nos avisa quando o nosso mundo deixa de fazer sentido. As consequências desta reação vão desde os distúrbios do sono à depressão, à ansiedade, ao declínio da memória, às doenças cardiovasculares e à diabetes.

Se o nível de ordem e de organização do nosso ambiente começa a diminuir, esta parte central do nosso cérebro é activada e alerta-nos para a presença de um perigo potencial para a nossa sobrevivência. Se a sociedade é relativamente estável, onde as estruturas do trabalho, da família e das relações humanas não mudam de forma demasiado imprevisível e arbitrária, o córtex cingulado é um fator de adaptação e de ajustamento, mas se os pontos de referência mudam demasiado depressa e constantemente, sem dar ao indivíduo qualquer descanso, pode ser extremamente perigoso para si próprio e para os outros.

Mas quando confrontado com um vazio de sentido, como é que o espírito humano reage? Constrói sistemas de representação dotados de sentido, de ordem e de coerência. Desde que o homem existe, tudo o que tem feito é insuflar sentido na realidade.

As primeiras tentativas assumiram a forma de relatos míticos da criação e da natureza. A necessidade de sentido deriva da necessidade de controlo e é uma consequência do nosso desejo de sobrevivência. E imaginar o mundo como um lugar habitado por sentido acalma o sistema de alerta interno no caso de um erro de previsão pontual no mundo concreto.

Por conseguinte, a reação de alerta desencadeada pelo córtex cingulado é naturalmente atenuada nos crentes. Mas atualmente, para a maioria das pessoas no mundo ocidental, já não acreditamos e perdemos este sentido.

Os grandes sistemas de sentido religioso, mas também ideológico, democrático ou filosófico, não são, por assim dizer, mais do que referentes desvalorizados, enfraquecidos pelo conhecimento científico e pela coexistência de múltiplas mensagens espirituais ou ideológicas que tentamos tolerar, mas cuja multiplicidade é suficiente para reduzir a nada a esperança de que qualquer uma delas possa conter, por si só, uma verdade absoluta.

Ao longo da história, os rituais apareceram sistematicamente antes dos sistemas morais. A sincronização e o mimetismo dos rituais colectivos tornam o ser humano mais sensível aos sentimentos, aos desejos e às emoções dos seus semelhantes. Esta capacidade de nos colocarmos no lugar do outro chama-se empatia cognitiva e permite-nos "colocarmo-nos no lugar do outro", sentir o que ele sente e pensar o que ele pensa. E esta capacidade é multiplicada por dez pela imitação: os parceiros sincronizados sentem compaixão um pelo outro.

Os rituais acalmam o nosso córtex cingulado, permitindo-nos prever de forma mais fiável o que os nossos semelhantes poderão fazer ou não fazer, pelo simples facto de já podermos prever os seus movimentos durante o ritual.

Mas é mais difícil prever os pensamentos das pessoas do que os seus movimentos. Para isso, já não se trata apenas de partilhar gestos, mas de partilhar representações mentais. Por outras palavras, valores e visões do mundo.

Pode haver diferentes visões do mundo, umas ditas normativas, no sentido em que prescrevem certos comportamentos, e outras que são apenas positivas (ou factuais) e, pelo contrário, não prescrevem quaisquer comportamentos. Quando esta visão prescreve o que é bom ou não fazer, e as pessoas aderem a ela e vêem o mundo da mesma forma que nós, isso reduz consideravelmente a incerteza e, de facto, a atividade do córtex cingulado.

Mas a sua violação, por outro lado, ativa-o fortemente (bem como outras estruturas cerebrais). Para o nosso córtex cingulado anterior, o desrespeito pelas normas morais é um erro de previsão fundamental. Isto, por si só, é um poderoso fator calmante para o córtex cingulado, que procura orientar-se entre os seus semelhantes.

Assim que tivermos a certeza de que os outros acreditam nos mesmos valores morais sagrados que nós, podemos começar a prever com maior fiabilidade o que é provável que eles façam ou deixem de fazer.

No termo religião, o verbo latino religere significa unir, pelo que entendemos que a religião serve sobretudo para unir os indivíduos.

Com o passar do tempo, a humanidade perdeu o seu foco social e moral em favor de previsões sobre o mundo material. Como disse o famoso filósofo Friedrich Nietzsche, "Deus está morto, vale tudo".

Para o nosso córtex cingulado, este é o início da angústia. Porque o sentido fornecido pela ciência apenas oferece uma visão positiva (ou factual), não tem o mesmo poder tranquilizador que o fornecido pela religião e pela moral.

A ciência introduz um sentido factual, mas não, pelo menos inicialmente, um sentido moral.

É certo que o significado factual obtido pela ciência é colossal. Atualmente, os seres humanos sabem, em certa medida, como a Terra se formou e como chegaram a ela, e podem reconstituir com bastante precisão a sequência de acontecimentos que levaram à evolução das espécies, desde as bactérias até aos primeiros vertebrados, mamíferos, macacos e Homo Sapiens. A questão da causa da sua existência, da sua natureza e das suas características físicas e mentais foi respondida. Da mesma forma, o conhecimento das leis da física e dos organismos vivos permite-nos prever um número impressionante de factos concretos, como o tempo, os eclipses da lua e do sol, a potência de um motor de combustão em função do grau de refinamento da gasolina utilizada e da cilindrada do motor, etc.

Mas já não sabemos o que faz sentido em termos humanos e existenciais. Porque, se é verdade que matámos o sentido, não matámos a necessidade de sentido. A parte do nosso cérebro que levou centenas de milhares de anos a conformar-se para encontrar sentido no mundo e nas estruturas sociais continua a existir. [^36]

Na nossa sociedade, estamos a viver o mito de Sísifo, uma personagem da mitologia grega condenada a rolar eternamente um pedregulho até ao cimo de uma colina e a descer de novo, antes de chegar ao topo. [Na nossa sociedade, a forma como produzimos já não faz sentido. Estamos agora num mundo descartável. E quando nada dura, a vida torna-se um desperdício. Todos nós enfrentamos este problema, em maior ou menor grau.

Além disso, com as alterações climáticas, já nada é estável, nem o ciclo das estações, nem o nível do mar, nem a ocorrência de catástrofes naturais. O mundo está agora em movimento, e este movimento vai ultrapassar as nossas capacidades de endividamento.

A palavra é dura, mas a incerteza mata, mina os nossos cérebros, destrói a aspiração humana fundamental ao sentido.

## As nossas reacções à incerteza

O sentimento de indeterminação ativa o córtex cingulado, que constata dolorosamente a impossibilidade de fazer previsões sobre o futuro, de se definir e de ver um caminho de vida claro.

A eco-ansiedade, por exemplo, é uma nova forma de ansiedade resultante da consciência paralisante da destruição do nosso planeta. [^38]

Num mundo onde existe um nível máximo de incerteza, o córtex cingulado conseguirá restaurar a certeza em escalas mais pequenas, uma vez que não o pode fazer na escala mais global da sua existência. A isto chama-se microcerteza.

As drogas que afectam os nossos neurónios podem ser um substituto do sentido. A cocaína, por exemplo, destrói a incerteza.

A autoestima é a invenção egocêntrica de um mundo individualista e uma forma de compensar a perda de sentido.

O dinheiro é o passaporte definitivo para a libertação da angústia da incerteza.

Mas no jogo da competição, as desigualdades estão a aumentar. Os perdedores desta corrida, desumanizados pelo corte do cordão umbilical, transformar-se-ão, no pior dos casos, numa forma degradada de humanidade. Trata-se de uma humanidade que nega a humanidade dos outros para garantir a sua própria exclusividade. Estamos a falar de identidade.

Porque esta tensão dá automaticamente origem a uma necessidade urgente: saber, de uma vez por todas, quem somos. E a maneira mais simples de esclarecer a sua identidade é definir-se por referência a um grupo ao qual pertence. Definir-se através da sua relação com um grupo satisfaz uma necessidade fundamental: encontrar uma forma de se conformar com as regras em vigor, o que limitará as reacções intempestivas do córtex cingulado.

De facto, uma das principais fontes de sentido regularmente mencionadas nos inquéritos é o sentimento de pertença a um grupo social[^40].

As desigualdades empurram as sociedades para um recuo identitário. Onde a única coisa que conta é a capacidade de utilizar a máquina de produção para os seus próprios fins. O modelo económico neoliberal, baseado na concorrência, na mobilidade dos indivíduos, na aceleração do ritmo de trabalho e na redução das despesas públicas dedicadas à proteção e à distribuição equitativa da riqueza, provoca um impulso autoritário e identitário destinado a acalmar as reacções do indivíduo à incerteza e à falta de um sentimento de pertença social.

A anomia refere-se ao sentimento de perda de ordem e de lógica na sociedade.

As pessoas que mais frequentemente têm pensamentos nostálgicos são também aquelas que têm a impressão mais clara de que a existência tem sentido[^41].

O nosso cérebro é extremamente bom a negar.

Quando as nossas acções não estão de acordo com as nossas representações mentais, isso cria dissonância cognitiva. A contradição também ativa a dissonância cognitiva.

Isto deve-se ao facto de o cérebro humano procurar a coerência e a razão em todas as circunstâncias. Por conseguinte, tentará resolver a dissonância que representa a incerteza e, para isso, modificará as suas acções de modo a que estejam de acordo com o seu pensamento, ou modificará o seu pensamento de modo a que este esteja em sintonia com as suas acções.

A investigação sobre a dissonância cognitiva mostra que, na maior parte das vezes, são os pensamentos que se adaptam às acções e não o contrário. [^42][^43]

Perdemos o contacto com a realidade porque deixamos de saber interpretá-la. O sentido protege-nos porque nos sugere formas de interpretar o mundo e de agir mais eficazmente para o dominar e nos proteger das ameaças que ele contém.

À medida que o espetro das grandes catástrofes se aproxima, os recuos comunitários só se tornarão mais pronunciados, assim como os comportamentos compensatórios de hiperconsumo, inflação do ego e negação.

A ausência de sistemas de sentido leva-nos a consumir bens materiais, nomeadamente em situações de grande incerteza, aceleração e concorrência. O hipermaterialismo é o paliativo para a incerteza e a competição[^36].

## Soluções para a falta de sentido

O cérebro procura prever o mundo para o controlar melhor, e consegue-o quer através do sentido, quer através da tecnologia. Quando a tecnologia progride, deixa de precisar de sentido. E quando a tecnologia falha, precisa de significado.

Hoje temos tecnologia, mas ela falhou, porque está a assinar a nossa extinção. [^44]

Perante a catástrofe que se desenrola agora e que está para vir, temos de parar o mais rapidamente possível a máquina de consumo em que a humanidade se tornou, ao mesmo tempo que procuramos ativamente formas de reparar a Terra.

Se quanto mais o sentido diminui, mais o consumo aumenta, então, inversamente, quanto mais o sentido aumenta, mais o consumo diminui.

Temos 8 mil milhões de córtex cingulados ocupados a enganar o seu medo da morte e do imprevisível, agora precisamos de os manter ocupados a enganar esse medo, unindo-os em torno de um significado partilhado[^36].

Este significado deve reunir milhares de milhões de pessoas de diferentes origens culturais, com heranças civilizacionais dispersas e antecedentes intelectuais heterogéneos, para as fazer esquecer a sua obsessão de possuir e explorar.

Há dois sentidos: o cósmico e o social.

O primeiro baseia-se na nossa compreensão das leis da natureza, da matéria e do universo. O segundo envolve a nossa capacidade de agir, num contexto social, de acordo com o que acreditamos ser certo e errado.

Criam um sistema moral, definindo um bem e um mal que regulam a ação social e individual.

O sagrado é o pivô a partir do qual se constrói o sentido nas assembleias humanas. Hoje, a humanidade moderna perdeu o sagrado ao desconstruir a realidade e descobrir que tudo, desde o movimento dos átomos numa estrela fundida até ao fluxo de iões na membrana dos seus neurónios enquanto lê estas linhas, obedece a leis mecanicistas em que o bem e o mal não têm lugar.

A consequência desta desmoralização do mundo é que cada indivíduo na superfície da Terra pode decidir o que considera bom ou mau.

Mas nenhum sentido coletivo pode existir sem a noção do sagrado. A solução é recriar o sagrado, e esse sagrado pode ser a nossa Terra.

O sagrado só pode manifestar-se através do ritual, e a humanidade terá de inventar novos rituais para sair do impasse a que chegou. Como vimos, as práticas sincrónicas ajudam a acalmar o córtex cingulado. Tais rituais terão de tornar sagrada a preservação do planeta.

Procedendo desta forma, o ser humano pode dotar-se de meios eficazes para reduzir a carga mental de ansiedade ligada ao individualismo. Saber que os cidadãos do meu país, de todo o continente e, se possível, de todo o planeta, consideram sagrado o que eu próprio considero um valor insuperável cria a base para um novo pacto de pertença e confiança.

O homem é um cooperador condicional; é capaz de sacrifícios imensos desde que tenha a certeza de que os outros membros da sua comunidade farão o mesmo. [^45]

Ao tornar a pureza do carbono e a preservação do equilíbrio biológico e geológico valores sagrados, os biliões de seres humanos podem trazer a atividade dos seus córtices cingulados de volta a níveis aceitáveis e deixar de ser dependentes de bens materiais, drogas, dinheiro e substitutos do ego para sustentar a sua própria existência.

A ecologia pode oferecer uma visão do mundo que é comum a todos os seres humanos.

Com o conhecimento, a lista de actos compatíveis com o valor sagrado é uma questão de ciência, de medições e cálculos feitos pelo coletivo humano.

O desafio é criar um sentido de identidade global que dê a cada um o sentimento de ser aceite num grupo, sem a necessidade de provar o seu valor e elegibilidade através da acumulação de bens materiais ou de apoios do ego.

Atualmente, temos de aceitar que a ciência pode dizer-nos não só o que é verdadeiro e o que é falso, mas também o que é certo e o que é errado.

O terceiro significado que virá será, portanto, ecológico.

Para o desenvolver, precisamos de desenvolver o conhecimento através da educação, de transmitir conhecimentos sobre os sistemas vivos, a biodiversidade, a climatologia, a ecologia e a evolução das espécies.

Isto criará um sentimento de maravilhamento, porque o maravilhamento é um poderoso antídoto para a angústia existencial que nos aflige, através da descoberta da riqueza infinita dos seres vivos, da beleza das paisagens, da flora e da fauna, porque nelas encontramos uma fonte de significado.

Como todos habitamos a mesma Terra, agimos sobre ela e sofremos as consequências da sua degradação, somos todos levados a reconhecer o mesmo valor sagrado.

Desta forma, os cidadãos poderão empenhar-se em acções coerentes com os seus valores e convicções, e experimentarão um sentido.

Tornar-se ator desta mudança é, por si só, já uma fonte de sentido e de alinhamento entre as nossas opiniões e as nossas acções.

Temos de rever os fundamentos da nossa civilização. A lógica da produção e do consumo just-in-time que está na base das nossas economias é insustentável. No próximo capítulo, descobrirá as nossas propostas em termos de "economia".

No fundo, todos nós temos uma aspiração imensa, procuramos um sentido e não sabemos como o realizar. O objetivo deste trabalho é satisfazer essa aspiração sem sequer esperar que ela se torne realidade, porque ela já existe desde o início.

## Necessidade de ficção - O cérebro que conta histórias

O cérebro humano, essa extraordinária máquina de contar histórias, está no centro da nossa capacidade de criar histórias, para o melhor e para o pior. Ele forja as histórias que nos permitem justificar as nossas acções e dar-lhes sentido. Esta capacidade narrativa não é um acidente, mas o produto de uma evolução complexa, que revela a profundidade da nossa necessidade de sentido e de ordem num mundo caótico. [^46]

Assim, o que torna a nossa espécie especial não é, por exemplo, o facto de fazer guerra desde o início dos tempos - os chimpanzés e as formigas também o fazem - mas o facto de ter feito disso toda uma história... e milhões de histórias.

Os outros animais não fazem o mal pelo mal, nem fazem qualquer outra coisa pelo bem; actuam sobretudo sob o impulso de necessidades imediatas ou instintos. O ser humano, pelo contrário, procura um sentido para as suas acções e para as dos outros, muitas vezes através do prisma da moral, do bem e do mal. Esta procura de sentido é, portanto, uma manifestação das nossas funções cerebrais superiores, que nos levam a procurar explicações e a justificar o nosso comportamento num contexto mais vasto.

O que consideramos ser o "eu" não é uma entidade isolada ou autónoma, mas antes um mosaico de traços, crenças e valores emprestados, partilhados e, por vezes, contestados pelos que nos rodeiam. Cada interação, cada intercâmbio cultural ou social ajuda a moldar a nossa identidade, tornando-a dinâmica e em evolução. O nosso "eu" é tecido a partir dos outros. Definimo-nos no espelho das nossas relações, sendo cada pessoa um reflexo que nos ajuda a formar a nossa própria imagem[^49].

Este processo de construção narrativa está em sintonia com os ensinamentos budistas sobre a ilusão do eu. De acordo com o budismo, o "eu" que percepcionamos como uma entidade estável e contínua é uma ilusão, uma construção da mente[^47]. [Voltaremos a este ponto com mais profundidade na secção Religião da Parte 2. Da mesma forma, a neurociência revela que a nossa identidade é uma história que contamos a nós próprios, uma sequência coerente fabricada a partir do zero pelo nosso cérebro para manter uma ilusão de continuidade[^48].

A identificação com as nossas histórias e com o nosso "eu" construído pode prender-nos a padrões de pensamento limitadores. Reconhecer esta construção narrativa como uma ilusão pode ser libertador. Convida-nos a questionar as narrativas tradicionais e a abrirmo-nos a novas possibilidades de perceção e experiência do mundo.

A interação entre a "vida real" e a ficção assume uma nova dimensão. Em vez de vermos estas esferas como distintas, podemos compreender que estão intrinsecamente ligadas, alimentando-se mutuamente. As nossas narrativas pessoais e colectivas não são meras distracções ou fugas, mas sim poderosas ferramentas de transformação e compreensão.

Reconhecer a natureza narrativa dos nossos cérebros e reconhecer a ilusão do eu não significa rejeitar a nossa identidade ou as nossas histórias, mas vê-las pelo que são: construções flexíveis, modificáveis e profundamente humanas. Encoraja-nos a deixar de seguir as narrativas tradicionais e a dizer a nós próprios "O mundo sempre foi assim e sempre será", mas a compreender e a dizer a nós próprios "Só vemos o que vemos".

Oferece-nos um caminho para uma compreensão mais rica e matizada de nós próprios e do mundo, deixando de nos agarrarmos à nossa identidade como uma realidade inamovível e de nos identificarmos exclusivamente com ela, mas permitindo-nos vislumbrar nas nossas vidas a possibilidade de reimaginar o mundo e a nós próprios.

## Efeitos e preconceitos cognitivos

Neste ponto, vamos rever os diferentes efeitos e enviesamentos do nosso cérebro, uma vez que podem afetar a nossa perceção, memória, julgamentos, decisões e comportamento de várias formas, influenciando a maneira como processamos e interagimos com a informação. Esta compreensão de nós próprios permitir-nos-á desenvolver ferramentas eficazes para a nossa sociedade, em particular para moldar o nosso processo de tomada de decisões e evitar as consequências destes preconceitos que nos impediriam de atingir o nosso objetivo comum.

A leitura pormenorizada dos efeitos cognitivos e dos enviesamentos, embora aconselhável, não é de modo algum necessária e pode ser ignorada para maior conforto da leitura, tendo sido resumida na secção seguinte.

### Resumo dos efeitos cognitivos e dos preconceitos

Segue-se uma lista não exaustiva dos enviesamentos cognitivos:

- Viés de seleção: É mais provável que seleccionemos informação que se enquadre nas nossas crenças e opiniões pré-concebidas, em vez de considerarmos uma gama mais vasta de informação. [^50]

- Ilusão de memória: Temos tendência a sobrestimar a fiabilidade das nossas memórias e a colori-las com a nossa perceção atual[^51].

- Racionalização das crenças: Temos tendência a justificar as nossas decisões e comportamentos, mesmo que sejam errados ou prejudiciais, interpretando-os de uma forma que nos seja favorável[^53].

- Viés de sobrestimação: Tendemos a sobrestimar a nossa capacidade de prever acontecimentos futuros e de controlar situações[^52].

- Enviesamento do otimismo: Tendemos a sobrestimar a probabilidade de benefícios futuros e a subestimar os riscos potenciais. [^54]

- Viés de redução da complexidade: tendemos a simplificar a complexidade das situações para as tornar mais fáceis de gerir[^55].

- Enviesamento da inferência causal: tendemos a atribuir uma causa a um acontecimento, mesmo quando tal não é necessariamente justificado ou razoável[^56].

- Viés do efeito de redução: tendemos a sobrestimar os efeitos positivos de uma ação e a subestimar os efeitos negativos[^57].

- Enviesamento do efeito de primazia: Somos influenciados pela primeira informação que recebemos e tendemos a dar mais peso a essa informação do que a informações posteriores[^58].

- Viés do efeito de semelhança: É mais provável que gostemos e confiemos em pessoas que são semelhantes a nós[^59].
- Heurística da disponibilidade: Utilizamos frequentemente uma estimativa rápida e intuitiva para avaliar a probabilidade ou a frequência de um acontecimento, com base nas informações mais facilmente disponíveis na nossa memória[^60].

- Pressão dos pares: Somos influenciados pela perceção do que os outros pensam de nós e somos pressionados a conformarmo-nos com as suas opiniões e comportamentos[^61].

- Preconceito de halo: Tendemos a generalizar uma opinião positiva ou negativa de uma pessoa com base numa única caraterística.[^62]

### Explicação aprofundada dos efeitos e do enviesamento cognitivo

- Viés de confirmação

O enviesamento de confirmação é um enviesamento cognitivo, que ocorre quando favorecemos a informação que apoia as nossas opiniões e crenças. Como estamos à procura de significado, seleccionamos informações que vão na nossa direção. A solução para isto é sermos os nossos próprios contraditores, pôr à prova os nossos pensamentos e tentar provar que estão errados.

Assim, as contradições dos nossos juízos não devem magoar-nos, mas sim despertar-nos e incitar-nos a agir. Não gostamos da correção das nossas opiniões, mas, pelo contrário, devemos estar abertos a ela e oferecer-nos a ela, e que ela venha sob a forma de uma conversa e não de um sermão.

Não devemos, com razão ou sem ela, procurar formas de nos livrarmos de cada oposição, mas sim analisar em profundidade para ver se ela pode ser correcta.

- Preconceito de disponibilidade

O enviesamento da disponibilidade é outro enviesamento cognitivo que pode afetar a tomada de decisões. Ocorre quando estimamos a probabilidade de um acontecimento ou a frequência da sua ocorrência com base em exemplos que nos vêm rapidamente à cabeça. Este enviesamento pode levar-nos a subestimar a probabilidade de acontecimentos raros e a sobrestimar a probabilidade de acontecimentos comuns ou recentes. Sobrestimamos o nosso raciocínio ao privilegiarmos a informação que está diretamente acessível à nossa memória.

Para evitar este viés, é importante ter em conta uma amostra representativa da informação e tentar ser objetivo na avaliação das probabilidades. Também é importante não confiar apenas nas informações imediatamente disponíveis, mas procurar informações adicionais que nos possam dar uma visão mais completa e equilibrada.

- Efeito de halo

O efeito de auréola é o mecanismo que nos leva a atribuir qualidades a alguém com base na sua aparência física.

Por exemplo, um sítio de encontros chamado cupidexe realizou uma experiência em que os perfis das pessoas foram avaliados em termos de personalidade e aparência. Aqueles com uma pontuação física baixa têm uma pontuação de personalidade baixa e, inversamente, uma pontuação física alta significa uma boa personalidade. Seria de esperar que, por parecerem ter uma boa personalidade, tivessem um bom físico, mas no top 1% alguns perfis de personalidade estão vazios...
Para verificar este facto, foi realizada uma outra experiência, na qual os utilizadores se depararam com perfis com texto e, por vezes, sem texto, e, independentemente de ter texto ou não, foi-lhe atribuída a mesma pontuação.

Existem outros estudos e estatísticas que confirmam este efeito. Por exemplo, todos os presidentes americanos são altos e quanto mais alto se é, mais dinheiro se ganha.

- Efeito Barnum

O efeito Barnum refere-se à tendência das pessoas para acreditarem que comentários vagos ou gerais são descrições exactas da sua personalidade ou vida. Este fenómeno tem o nome do famoso showman P.T. Barnum, que utilizava leituras vagas da personalidade para atrair pessoas para os seus espectáculos.

Por exemplo, um teste de personalidade que afirma "você é uma pessoa carinhosa e amorosa com a sua família e amigos" é suficientemente vago para ser considerado verdadeiro para a maioria das pessoas. No entanto, não dá pormenores específicos sobre a pessoa, o que pode levar a um sentimento de identificação com a descrição geral, mesmo que não seja muito pessoal.

O efeito Barnum pode ser reforçado pela necessidade de reconhecimento e pela sede de auto-determinação das pessoas. As pessoas gostam de se sentir compreendidas e valorizadas, o que as pode levar a aceitar as descrições gerais como sendo verdadeiras para elas.

É importante ter em conta o efeito Barnum quando se analisam testes de personalidade ou comentários sobre a sua vida ou personalidade. É sempre melhor procurar informações mais específicas e pormenorizadas para obter uma compreensão mais exacta de si próprio.

- Efeito de ancoragem

O efeito de ancoragem refere-se à tendência das pessoas para basearem as suas estimativas ou decisões numa informação inicial, designada por "âncora". Esta primeira informação pode ter um grande impacto nas estimativas subsequentes, mesmo que seja irrelevante ou mesmo incorrecta.

Por exemplo, se lhe for pedido que adivinhe o número de pessoas que vivem na sua cidade e lhe for dado um valor elevado como primeira informação, terá tendência a fazer uma estimativa mais elevada do que se lhe tivesse sido dado um valor mais baixo. Do mesmo modo, se estiver a negociar o preço de um objeto, a primeira proposta será frequentemente considerada a âncora e terá influência nas propostas seguintes.

O efeito de ancoragem pode ser utilizado consciente ou inconscientemente em situações de negociação, venda ou persuasão, o que pode levar a decisões pouco racionais e tendenciosas.

- Pressão dos pares

A pressão dos pares é um fenómeno social que descreve a tendência dos indivíduos para se conformarem com as opiniões, atitudes e comportamentos de um grupo a que pertencem ou com o qual se sentem afiliados. Pode ocorrer quando os indivíduos procuram conformar-se às normas sociais e evitar a desaprovação ou o isolamento do grupo.

Por exemplo, quando uma pessoa se encontra num grupo de pessoas que têm uma determinada opinião ou atitude, pode ser influenciada por essas opiniões e atitudes, mesmo que não correspondam às suas próprias opiniões ou atitudes. Isto pode ser particularmente verdadeiro em situações em que a pessoa quer ser aceite pelo grupo e evitar a desaprovação ou a exclusão.

A pressão do grupo também pode surgir quando uma pessoa se sente obrigada a conformar-se com o comportamento de um grupo para evitar sentir-se diferente ou desconfortável. Por exemplo, uma pessoa num grupo que bebe álcool pode ser influenciada a beber álcool, mesmo que não o queira fazer.

É importante estar consciente da pressão do grupo e não ceder às opiniões ou comportamentos de um grupo simplesmente para evitar a desaprovação ou para se conformar com as normas sociais. É sempre melhor seguir as suas próprias opiniões e convicções do que ceder à pressão do grupo.

- Heurística da disponibilidade

A heurística da disponibilidade é um atalho mental que utilizamos frequentemente para avaliar a frequência ou a probabilidade de um acontecimento com base nos exemplos mais recentes e imediatamente disponíveis na nossa memória. Isto pode levar a erros de julgamento, uma vez que pode haver preconceitos nos exemplos que temos em mente.

Por exemplo, se uma pessoa acabou de ouvir falar de um acidente de avião na televisão, pode sentir-se mais preocupada com a sua segurança no seu próximo voo, uma vez que este acontecimento está fresco na sua memória e prontamente disponível. No entanto, esta pessoa pode subestimar a segurança da aviação em geral, uma vez que os acidentes aéreos são, de facto, muito raros.

É importante lembrar que a heurística da disponibilidade pode levar a erros de julgamento e procurar informações mais completas e fiáveis para avaliar a frequência ou a probabilidade de um acontecimento. É igualmente útil recordar que os meios de comunicação social podem influenciar a nossa perceção dos acontecimentos, destacando certas histórias em detrimento de outras. Isto pode levar a uma sobre-representação de acontecimentos raros ou dramáticos e dar uma falsa impressão da sua verdadeira frequência. Por isso, é importante lembrarmo-nos de não nos deixarmos influenciar apenas pelo que vemos ou ouvimos nos meios de comunicação social.

Em conclusão, a heurística da disponibilidade pode ser útil para tomar decisões rápidas em situações quotidianas, mas é importante considerá-la com cautela quando se trata de fazer julgamentos mais importantes ou de tomar decisões com consequências a longo prazo. É preferível procurar informações completas e fiáveis ao avaliar probabilidades e riscos, em vez de confiar apenas no que está imediatamente disponível na nossa memória.

## Conclusão

Esta secção sobre o Homo Sapiens já abriu uma série de caminhos. Vamos desenvolver algumas delas nesta conclusão, mas esta lista não é exaustiva. Acima de tudo, desenvolveremos muitas outras nos capítulos seguintes, partindo da base aqui desenvolvida.

Conhecemos agora as necessidades humanas que devem ser satisfeitas se quisermos alcançar a realização total. Vimos que é possível transformar as nossas necessidades extrínsecas e que será necessário fazê-lo para as tornar compatíveis com um projeto comum que nos permita a todos satisfazer a nossa necessidade de sentido.

Este sentido coletivo deve existir através da noção de sagrado. E não há nada mais sagrado do que aquilo que todos temos em comum e cuja sobrevivência está diretamente ligada a nós, ou seja, a nossa terra e os seus ecossistemas. Assim, a ciência e a ecologia já não são apenas capazes de nos dizer o que é verdadeiro ou falso, mas são também capazes de ditar os nossos valores, o que é certo ou errado.

Vimos que não existe uma natureza humana que nos impeça de ser bem sucedidos neste projeto, que não existe um eu inamovível, e nesta tarefa o desenvolvimento da nossa consciência será o nosso melhor aliado. Igualmente importante será a educação das novas gerações, encorajando-as a serem altruístas e a maravilharem-se com a natureza para a protegerem.

Através do estudo dos diferentes efeitos e preconceitos cognitivos, reunimos cada vez mais informações necessárias para nos compreendermos a nós próprios e, por conseguinte, para desenvolvermos ferramentas que tenham em conta estes parâmetros e que sejam eficazes para nos ajudarem no nosso projeto comum.

## Fontes (lista não exaustiva)

[^1]: [Relatório do IPCC](https://www.ipcc.ch/report/ar6/syr/downloads/report/IPCC_AR6_SYR_FullVolume.pdf)
[^2]: [Wikipedia : Pirâmide das necessidades](https://fr.wikipedia.org/wiki/Pyramide_des_besoins)
[^3]: Kenrick, D.T., Griskevicius, V., Neuberg, S.L et al. Renovating the pyramid of needs. Perspectivas da Ciência Psicológica
[^4]: [Organização das Nações Unidas para a Alimentação e a Agricultura (FAO)](https://www.fao.org/state-of-food-security-nutrition/2021/en/)
[^5]: [Números e análises sobre a habitação precária em França, incluindo o número de sem-abrigo e as condições de vida precárias](https://www.fondation-abbe-pierre.fr/actualites/28e-rapport-sur-letat-du-mal-logement-en-france-2023)
[^6]: [Renúncia aos cuidados de saúde por razões económicas na aglomeração de Paris:](https://drees.solidarites-sante.gouv.fr/sites/default/files/2020-10/dtee120.pdf)
[^7]: [A Fondation de France publica a 13ª edição do seu estudo sobre a solidão](https://www.carenews.com/fondation-de-france/news/la-fondation-de-france-publie-la-13eme-edition-de-son-etude-sur-les)
[^8]: [Handicap International: As pessoas com deficiência continuam a ser das mais excluídas do mundo](https://www.handicap-international.lu/fr/actualites/les-personnes-handicapees-restent-parmi-les-plus-exclues-au-monde)
[^9]: Abraham Maslow, Motivation and Personality, 1970 (segunda edição)
[^10]: Arendt Hannah, A Condição do Homem Moderno, 1961
[^11]: Valérie Jousseaume, Plouc Pride: Uma nova narrativa para o campo
[^12]: Emmanuel Todd, A origem dos sistemas familiares, 2011 - Em que ponto estamos? Um esboço da história da humanidade, 2017 - A luta de classes em França no século XXI, 2020.
[^13]: Robert J. Vallerand, Toward A Hierarchical Model of Intrinsic and Extrinsic Motivation, 1997
[^14]: Jean Baudrillard, A Sociedade de Consumo, 1970
[^15]: Robert Wright, The Moral Animal: Why We Are the Way We Are, 1994
[^16]: Charles Darwin, A Origem das Espécies: Capítulo IV / III, 1859 & A Descendência do Homem e a Seleção Sexual: Capítulo VII, 1871
[^17]: Hannah Arendt, As Origens do Totalitarismo, 1951
[^18]: Albert Camus, A Peste, 1947
[^19]: Jean-Paul Sartre, O existencialismo é um humanismo
[^20]: Ivan Samson, Myriam Donsimoni, Laure Frisa, Jean-Pierre Mouko, Anastassiya Zagainova, Homo Sociabilis: Reciprocidade
[^21]: [RIM Dunbar, The social brain hypothesis and its implications for social evolution](https://pubmed.ncbi.nlm.nih.gov/19575315/)
[^22]: Sébastien Bohler, O inseto humano
[^23]: [W Schultz 1 , P Dayan, P R Montague, A neural substrate of prediction and reward](https://pubmed.ncbi.nlm.nih.gov/9054347/)
[^24]: [Wolfram Schultz, The Role of Striatum in Reward and Decision-Making](https://www.jneurosci.org/content/27/31/8161)
[^25]: David Eagleman, The Primal Brain
[^26]: Joseph Henrich, The Role of Culture in Shaping Reward-Related Behavior
[^27]: [Jean Decety, The Neural Basis of Altruism, 2022](https://www.degruyter.com/document/doi/10.7312/pres20440-009/pdf)
[^28]: Matthieu Ricard, Altruísmo: um enigma?
[^29]: Matthieu Ricard, "Altruísmo: o gene ou a educação?"
[^30]: Kathryn Spink, "Madre Teresa: Uma vida de serviço aos outros"
[^31]: Wolfram Schultz, "The Role of Dopamine in Learning and Memory", 2007
[^32]: Read, D., & Northoff, G, Correlatos neurais de decisões impulsivas e reflexivas. Nature Neuroscience, 2018
[^33]: Jean-Didier Vincent, O Cérebro e o Prazer
[^34]: David Eagleman, O Cérebro Inconsciente
[^35]: Christina M. Luberto,1,2 Nina Shinday,3 Rhayun Song,4 Lisa L. Philpotts,5 Elyse R. Park,1,2 Gregory L. Fricchione,1,2 e Gloria Y. Yeh,3 Uma Revisão Sistemática e Meta-análise dos Efeitos da Meditação na Empatia, Compaixão e Comportamentos Prosociais, 2018
[^36]: Sébastien Bohler, Onde está o significado?, 2020
[^37]: [Roy F. Baumeister, The Need for Meaning: A Psychological Perspective](https://www.psychologytoday.com/us/blog/the-meaningful-life/201807/search-meaning-the-basic-human-motivation)
[^38]: Eddy Fougier, Eco-ansiedade: análise de uma angústia contemporânea
[^39]: Albert Camus, O Mito de Sísifo, 1942
[^40]: [Inquérito "Les Français et le sentiment d'appartenance" (Ipsos, 2022)](https://www.ipsos.com/en/broken-system-sentiment-2022)
[^41]: [Constantine Sedikides e Tim Wildschut, Nostalgia and the Search for Meaning: Exploring the Links Between Nostalgia and Life Meaning in Journal of Personality and Social Psychology, 2022](https://journals.sagepub.com/doi/abs/10.1037/gpr0000109)
[^42]: Jonathan Haidt, The Irrational Man 2001
[^43]: Elliot Aronson, A teoria da dissonância cognitiva
[^44]: Elizabeth Kolbert, The 6th Extinction, 2015
[^45]: Robert Axelrod, The prisoner's dilemma (O dilema do prisioneiro)
[^46]: Nancy Huston, The fabulist species
[^47]: Serge-Chritophe Kolm, Felicidade e liberdade
[^48]: David M. Eagleman, Incognito, 2015
[^49]: William James, The Principles of Psychology, 1980
[^50]: [Wikipedia: Selection bias](https://fr.wikipedia.org/wiki/Biais_de_s%C3%A9lection)
[^51]: Julia Shaw, The Illusion Of Memory
[^52]: David Dunning e Justin Kruger, Unskilled and unaware of it: How difficulties in recognising one's own incompetence lead to inflated self-assessments, 1997
[^53]: [Festinger e Leon Carlsmith, James M, Cognitive consequences of forced compliance, 1959](https://psycnet.apa.org/record/1960-01158-001)
[^54]: [Weinstein, Neil D, Unrealistic optimism about future life events, 19808](https://psycnet.apa.org/record/1981-28087-001)
[^55]: [Tversky, A., & Kahneman, D. Judgment under Uncertainty: Heuristics and Biases, 1974](https://www2.psych.ubc.ca/~schaller/Psyc590Readings/TverskyKahneman1974.pdf)
[^56]: [Edward E. Jones e Victor H. Harris, "The Attribution of Attitudes, 1967](https://www.sciencedirect.com/science/article/abs/pii/0022103167900340?via%3Dihub)
[^57]: Kahneman PhD, Daniel, "Thinking, Fast and Slow", 2011
[^58]: [Asch, S. E., Forming impressions of personality, 1946](https://psycnet.apa.org/record/1946-04654-001)
[^59]: [Byrne, D. The Attraction Paradigm, 1961](https://books.google.be/books/about/The_Attraction_Paradigm.html?id=FojZAAAAMAAJ&redir_esc=y)
[^60]: [Amos Tversky, Daniel Kanheman, Availability: A heuristic for judging frequency and probability, 1973](https://www.sciencedirect.com/science/article/abs/pii/0010028573900339)
[^61]: [Asch, Opinions and Social Pressure, 1955](https://www.jstor.org/stable/24943779)
[^62]: Thorndike, E.L. A constant error in psychological ratings, 1920
