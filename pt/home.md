---
title: Home
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z
---

> Clique no globo no canto superior esquerdo para mudar de idioma.
> {.is-info}

Bem-vindo à história colaborativa e democrática promovida pela fundação Eutopia.

Aconselhamo-lo a começar a ler pela página de apresentação e a seguir seguir a ordem das páginas.

Poderá sentir-se tentado a passar diretamente para a parte 2 para saber mais sobre o mundo que propomos, mas as bases da parte 1 são essenciais, porque são elas que justificam o que é possível, e sem elas arrisca-se a ver apenas o impossível...

É por isso que nos concentrámos principalmente na parte 1 e que grande parte da parte 2 ainda está inacabada.

Como é que tornamos possíveis todas as ideias da parte 2? Um dos elementos-chave é compreender o ser humano, o que fizemos no capítulo intitulado "Homo Sapiens", mas também escolher o sistema económico adequado, o que fizemos no capítulo intitulado "Trabalhar em conjunto", porque este sistema não produz apenas bens e serviços, mas sobretudo seres humanos e as relações entre eles.

Conscientes do tempo e da motivação que podemos ter nas nossas vidas, recomendamos-lhe primeiro estes capítulos e elaborámos também resumos de todos os capítulos da nossa história. [Estes estão disponíveis nos apêndices (/pt/4-Appendices/1Summaries)
