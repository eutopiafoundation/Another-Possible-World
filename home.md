---
title: Accueil
description:
published: true
date: 2024-03-12T14:24:47.978Z
tags:
editor: markdown
dateCreated: 2024-03-12T14:24:47.978Z
---

> Cliquer sur le globe en haut à gauche pour changer de langue.
> {.is-info}

Bienvenue sur le premier récit évolutif, collaboratif et démocratique d'un monde idéal réaliste : une Eutopie.

Ce projet est soutenu par la [Fondation Eutopia](https://www.fondationeutopia.org/fr) qui participe à la lutte des imaginaires et soutient la réalisation d'une véritable transition écologique et sociale. Elle aide les projets en accord avec ses valeurs, en développant et en mettant à disposition les outils nécessaires à leurs créations, leurs mises en relation et leurs autogestions.

Le récit hyper moderne est devenu complètement dissonant et la construction d'un récit de la transition est en panne. L'imaginaire derrière le mot écologie et décroissance ne convainc pas, il donne le sentiment de devoir faire des concessions sur son confort et son épanouissement. L'imaginaire du communisme c'est lui aussi complètement effondré avec les faits historiques, et il sert maintenant d'argument au système pour conserver le statu quo.

Ce contre récit est encore à construire.

Le premier récit collaboratif que vous allez lire aspire à une réconciliation des principes de liberté et d'égalité, cherche à assurer l’épanouissement et le bonheur de chacun, tout en assurant une symbiose avec nos écosystèmes En outre, il compte y parvenir, par la promotion d'une économie axée sur la réciprocité, un mode de production symbiotique, des modes d'organisations et de gouvernances basées sur l'intelligence collective et des processus collaboratifs et démocratiques, le tout soutenu par la mise en place d'une démocratie profonde.

Pour votre lecture nous vous conseillons de suivre l’ordre des pages. Vous serez tenté de directement passer à la partie 2 pour découvrir concrètement cet autre monde possible, néanmoins les bases de la partie 1 sont essentielles, car ce sont elles qui permettent de justifier le possible.

C'est pourquoi, nous nous sommes avant tout consacrés en premier lieu à la partie 1 et pourquoi une grande partie de la partie 2 est toujours en cours de rédaction.

Pour rendre toutes les idées de la partie 2 possible et cohérente, un des éléments clé est de comprendre l'être humain, ce que nous avons fait à travers le chapitre [Homo Sapiens](https://anotherpossible.world/fr/2-Story/Part1-TheBase/Chp3-TheHomoSapiensSpecies) mais aussi celui de choisir le bon le système économique, ce que nous avons fait à travers le chapitre [Collaborer tous ensemble](https://anotherpossible.world/fr/2-Story/Part1-TheBase/Chp5-CollaborateTogether).

Conscients du temps et de la motivation que nous pouvons avoir dans nos vies, nous vous conseillons ces chapitres en premier lieu, et nous vous avons également réalisé un résumé pour l'ensemble des chapitres de ce récit. [Celui-ci est disponible dans les Annexes.](/fr/4-Appendices/1Summaries)

<center>
  
---
<p style="font-size: larger; font-weight: bold; text-decoration: underline">Propositions directes sur cette page</p>

</center>

<div style="margin-top: 20px;" data-canny id="cannyBoardToken" data-token="3bbe06f5-ed77-fec1-fd2a-8a8676a74ef9" ></div>
</div>
