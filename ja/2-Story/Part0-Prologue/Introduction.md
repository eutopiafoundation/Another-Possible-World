---
title: はじめに
description:
published: true
date: 2024-03-07T21:14:03.200Z
tags:
editor: markdown
dateCreated: 2024-03-07T21:13:59.460Z
---

## Who are you?

あなたは偉大な社会学者でもなければ、偉大な哲学者でもなければ、偉大な経済学者でもなければ、他のどんな偉大な人でもないかもしれない。私たちの多くもそのようなケースです。しかし、私たちの教育的背景はまったく異なるにもかかわらず、私たちはここにもうひとつの可能性のある世界、理想を目指す世界を提案する自信があります。

だから、なぜ自分がそれを実現できる立場にいるのか、不思議に思っているのかもしれない。そして、この段落を読んだだけで、もうこれ以上時間を無駄にしないためにこの本を閉じたいという衝動に駆られた方もいらっしゃるかもしれません。しかし、あなたを納得させるために、あと数段落をお許しください。

## 想像力

あなたはこの世界の主要な分野のどれにも長けているわけではないが、ひとつだけ特別に才能があることがある。この想像する能力は、人間にとって最大の贈り物である。私たちはここに、夢を見るために、そして何よりも他の人々の夢を手助けするために、あなたを招待する。

人類が誕生した当初、私たちは取るに足らない種であり、行動する力も限られていた。私たちの種であるホモ・サピエンスと他の種との違いは、大勢で協力する能力にあった。[^1]

そして、私たちの想像力、物語を創作し、それを自分自身に語りかける能力のおかげである。

虚構」とも呼べる集合的な神話のおかげで、私たちは団結し、協力することができたのである。

私たちは存在しないものを信じる能力を持ち、多くの伝説、神話、物語を発明し、世界に架空の線を引き、国や習慣を作り、貨幣を発明し、想像の中で最大の価値を与えてきた。

この想像力という才能は、私たちが意味を求め、その中でよりよく生き残るために環境を予測し理解する必要性から生まれている[^2]。

私たちはこの集団的想像力に私たちの生活全体を支配させ、人類全体の運命さえも支配させている。 その力は、私たち自身の環境を破壊し、私たち自身の破滅をもたらすほどである。

しかし、これらすべては基本的には単なる集団的想像力であり、私たち全員がそれを信じることをやめると決めた瞬間に消滅する。

現実には、私たちが一般的に理性と呼んでいるものは、集団的想像力のルールを信じ続け、それに従うという事実であることが多い。

## 理性

想像の世界を捨て去ると決めたとき、私たちはようやく真の理性を見出すことができる。

あまりよくわからないと感じるかもしれないが、解決策を見つけ、充実した有意義な人生へと導く選択をするために、多くを知る必要はないことがわかるだろう。

なぜなら、疑問に対する答えはすでに私たち一人ひとりの中にあるからだ。それを見つけるために私たちがすべきことは、想像力を捨て、心を開き、耳を傾けることだ。馬鹿げて聞こえるかもしれないが、私たちはそこに本当の理由があると確信している。

そして、私たちはすでにこの脱構築のプロセスを始めているのだから、ここで皆さんに発見していただきたいのは、真に理性に基づいた世界なのである。

そのシンプルさ、意味、論理は、あなたにも明白に思えるだろう。

そして、あなたが誰であろうと、あなたの知識が何であろうと、何が良くて何が悪いかを判断するために多くを知る必要はないこと、この世界の形成に貢献するために多くを必要としないことを発見するでしょう。

> しかし、アリスは言う。「もし世界がまったく意味をなさないのであれば、私たちが世界を発明しない手はないでしょう」。ルイス・キャロル

<!-- > "あらゆる技術革新は、世論において3つの段階を経る：1）馬鹿げている 2）危険だ 3）明白だ"アーサー・ショーペンハウアーの迷言。

## ♪ユートピア

私たちがここで提案しているのはユートピアだと思うかもしれない。

ユートピアとは、古代ギリシャ語の "tópos"（「場所」）に接頭辞 "ou"（「ない」）をつけた造語で、文字通り「ない場所」[^3]である。

ユートピアはしばしば誤用され、乱用される言葉である。希望を押し殺し、想像力を否定し、進歩を抑圧するために、ユートピアはあらゆる場面で振りかざされる。それは諦めを呼びかける言葉であり、搾取、疎外、支配のシステムにおいて、現状を受け入れるためのキーワードである[^4]。

しかし、「非場所」とは存在しない社会のことであり、不可能な社会のことではない。そして、歴史を通して見ることができるように、明日の社会の未来は常に異なり、新しいものである。結局、明日の社会は常に今日のユートピアなのである。

ユートピアは時に「科学的」と対比される。しかし、これはギリシャ語だけでなく、何よりも科学の本質に対する無知を示している。

存在しないものの探求、実現可能なものの探求、現実的なユートピアの分析は、まさに科学的な活動である。観測されていないものの中で何が可能で何が不可能かを知るためには、世界の性質と構造について深い知識と認識を持つ必要がある。

私たちがここで採用しているのは科学的な態度であり、あらゆることに疑問を投げかけ、問いかけ、物事を深く説明しようとし、真の原因を探り、受け取った正当性を疑い、体系的に批判し、何事も与えられたもの、自明なもの、不変のものとして受け入れないという態度である。このような書き方をすることで、私たちは社会を根本的に疑うようになる。科学とは、私たちの目を開かせることなのだ。

だから、現在あるいは過去の実際の社会に関する研究に対して、可能性のある社会の研究はあまり広まっていない。ある学問分野や著者は、それが専門的な次元を持ちうるとは想像すらしていないし、その考えを熱烈に拒絶さえしている。おそらくこうした知識人は、想像力を制限することを正当化しようとしているのだろう。

私たちのシステムはひどく悪いかもしれないが、それが最悪のものではないことは誰もが知っている。急進的な変革は、さらに不愉快な社会をもたらす可能性がある。だから、このような正当化された恐怖を抱きながら、国民の多くは保守的か、慎重な改革派なのである。そうでないのは、人々が新しい、より良いシステムがどのようなものであるかについて、かなり正確で信頼できる考えを持つことができる場合だけである。

別の可能性のある社会の分析が、十分に真剣で、思慮深く、正確で、完全なものでなければならないのは、このためである。そして、その実現の段階、つまり既存のものから新しいものへの移行の段階についても同様である。

私たちが可能だと信じることは分析に依存するため、人々はそのプロジェクトに納得する必要がある。そうして初めて、より良い社会への歩みを自分たちの進化のために必要なものとし、それを終着点とするのである。[^4]

> もちろん、我々が構築した社会を分析することで、他の社会を構築するために必要な知識を見つけることができる。クリストフ・コルム

つまり、私たちの社会が認識している現在の定義では、ユートピアは理想であり、現実を考慮しない概念であり、それゆえ達成不可能なのだ。

そう、その場合、私たちはここでユートピアを創造することはない。

現実は私たちの集団的想像力ではない。現実は、私たちの生存に有利な条件を損なわないように、私たちが超えることのできない閾値で構成されている。大気中のCO2濃度の限界値、土壌や水域の良好な状態に不可欠なリンと窒素の生化学的循環を破壊する限界値、生物多様性の最大侵食、すなわち種の絶滅と生態系における種の重要な役割、土壌の集約的利用、淡水の乱用、海洋の酸性化などである。..[^5][^6][^7][^8][^9]

これは現実を構成するものの一部であり、例えば私たちの銀行口座のページにある小さな数字が発表する価値とは違う。

実際、私たちの社会はユートピアの真っただ中にあり、長期的にはこのような生活を無限に続けることができると信じている[^11]。

理想的な世界をデザインするためには、私たちは空想の世界を捨て去り、問題を根本から解決することから始めなければならない。そうすることで、意味のある別の世界、本当に可能な別の世界を築くことができるのだから。

私たちがここで作ろうとしているのは、ユートピアである。ユートピアとは、ユートピアに似た概念だが、現実に達成可能であると考えられているものだ。接頭辞の「u」は「eu」となり、ギリシャ語で「良い」を意味することから、文字通り「正しい場所」を意味する[^12]。

自由な人間を導くものは、未来、可能なもの、望ましいもの、ユートピア（理想郷）に対する想像力である。そして、人間は絶望に突き動かされるよりも、希望に惹かれて前進することの方がはるかに多いので、夢は結局、現実を創造することになる。私たちの希望は、現実がユートピアになることだ。

> ユートピアとは、すべてが以前と同じように続くと信じる人のことである。パブロ・セルヴィーニュとラファエル・スティーヴンス[^11]。

## 一歩下がる

私たちは、短期と長期の認識を問い直す必要がある。100年を短期と定義すべきか、長期と定義すべきか。全ては視点の問題だが、私たちは一歩引いて、短い人生のスケールを置き去りにする必要がある。

地球が誕生したのは40億年前、生命が誕生したのは5億年前、チンパンジーの系統からヒトの系統であるホミニアが分離したのは70億年前、その系統から私たちの種であるホモ・サピエンスが誕生したのはわずか30万年前である[^13]。

50億年から70億年後、太陽はヘリウムを使い果たして赤色巨星に変身し、半径を1,000倍にして、地球を徐々にその燃え盛る後光の中に引き込んでいくだろう。そのとき地球の温度は非常に高くなり、海は蒸発するだろう。しかし、酸素が不足するため、生命はそれよりもずっと前に消滅しているだろう[^14]。

現実には、人類が居住可能な惑星に住めるのはまだ20億年先である。

これらの数字を明確に理解するためには、スケールで視覚化することが重要である。

20億年は、視覚的には2,000,000,000年である。

25年ごとに1世代と考えれば、さらに80,000,000世代。そして私たちは現在、その12,000番目......。私たちの地球での生涯のわずか0.015％しか経過していないのだ。

しかし、地球が滅亡しても人類が滅亡するわけではなく、宇宙は広大である。現在はそうではない。

地球の資源を消費する速度や、地球やその生態系との関係を考えると、長くは生きられないだろう...。

現在の世界生産量では、石油は51年後、ガスは53年後、石炭は114年後、ウランは100年後にすべて使い尽くしてしまう。[^11]そして、私たちは2,000,000,000,000年持つことを望んでいるのだろうか？

それゆえ、一歩引いて、短い人間の一生を越えて、長期的に考えることが重要なのである。このことについては後で触れるが、もし我々にこの能力が欠けているとすれば、それはホモ・サピエンスの脳が長期的に考えるように設計されていないからだと気づくことが重要である。[^15]

私たちがここで救おうとしているのは、地球ではないことにもお気づきだろう。地球には、私たちがいてもいなくても、まだ明るい未来があるからだ。

## 夢が現実になるとき

おそらくあなたは、別の世界を想像することが、現存する世界を変える手助けになるのではないかと考えているのではないだろうか？

結局のところ、新しい世界を想像するよりも、すでに存在しているものを変えることに時間とエネルギーを割いたほうがいい、と言えるかもしれない。

しかし、何が起こるかは、私たちが試みることとは無関係ではない。何を試すかは、何が可能だと信じるかにかかっている。そして、私たちが可能だと考えることは、私たちの分析にかかっている。

私たちが皆さんに発見してもらおうと提案している世界の土台となる原理や価値観は、知識人たちによってほとんど開発・分析されてこなかった。この分析によって、あなた方はこのビジョンが可能であると信じることができ、そしてそれを試みることができる。そして最終的には、すでに存在しているものを変えることになる。この分析は、できるだけ多くの人々の支持を獲得し、集団的で賢明かつ遠大な行動につなげるためには、可能な限り首尾一貫したものでなければならない。

新しい世界を想像することによって、私たちは現在の世界の不条理さを比較することによって発見する。

新しい世界を想像することで、私たちは共通の理想を探求するための材料、内省と討論の場を創造する。

新しい世界を想像することで、私たちは共通の理想の周りに人々を集め、彼らが行動を起こし、すでに存在しているものを変えることを可能にする。

新しい世界を想像することで、私たちはそこからインスピレーションを得て、ツールや原則を生み出し、実生活に適用し、実験し、反復し、改善することができる。

最後に、異なる世界を想像することによって、私たちは夢と希望を抱くことができる。

私たちはこれまで、人間が持つこの才能、つまり想像し、自分自身に物語を語る能力についてお話ししてきた。

そして今、歴史を変えるために、私たちの物語をもう一度変える時が来た。私たちは共通の理想に突き動かされ、物語を変え、信念を修正する必要がある。

> 歴史を通して、物語は哲学的、倫理的、政治的変革のための最も強力な手段であった。現代の抵抗のハンドブック[^24］

これを読んでいるとき、「そんなことは不可能だ」と自分に言い聞かせてはいけない。おそらくその通りだろうし、現在の状況下ではそうなのだろう。しかし、自分自身に「それは可能になるかもしれない」と言わなければならない。

そのうえで、「この理想に近づくために、今何が可能か」と自問し、可能な限り移行の段階を積み上げていくのだ。しかし、そのためにはまず、その理想が何なのか、自分がどこへ向かっているのかを知る必要がある。

その上で、今何が可能か、他の実現に向けて努力する中でどんな新たな可能性が開けるかを考えればいいのだ。

しかし、ここでの目的は、何をすべきか、どのようにすべきかを指示することではなく、自分自身で見つけ、適切なツールを使ってそれを実行できるようにするためのビジョンを描くことである。

そして、このビジョンは、あなた自身が作り上げていくものであり、あなたが進歩し、新しいことを発見するにつれて、時間とともに進化していくように設計されているのだ。

だから、私たちが何をしようとも、このシステムは崩壊する運命にある可能性がある。惰性で環境を破壊し、制御不能に陥っているようだ。

崩壊論に関する本はたくさんあり、非常に真面目な研究に基づいているが、もしそのような崩壊が起こったとしても、人類は70億人から1人になることはなく、社会を再建するのに十分な数が残っているはずだ。崩壊は一夜にして起こるものではなく、起伏を伴いながら徐々に起こるものであり、その間に方向転換することは常に可能である。[^16]

いずれにせよ、ここで行われた研究は興味深いものである。この研究のおかげで、崩壊を回避するために社会を変えることができたか、あるいは、崩壊の間にも生き続けることができるように回復力を備える一方で、新しい社会を始めるための強固な基盤を準備することができたかのどちらかである。

なぜなら、崩壊によって真新しい社会が用意される余地はなく、その瞬間に個人個人が道を築き、共に分かち合うべき方向性を見出さなければならないからだ。

そのときから、私たちの準備不足は、饒舌で非力な集合体に場を明け渡すことになりかねない。同じように、自主管理システムをまず試さずに構築することはできない。社会のあらゆるレベルで協調的かつ民主的な意思決定を確立できなければ、緊急事態のために、望ましくない他の支配体制に道を譲ることになりかねない。だからこそ、今日、こうした新たな道を探る必要があるのだ。[^4]

何はともあれ、私たちは崩壊を回避できると確信している。崩壊は人道的・生態学的災害を引き起こし、その修復はさらに困難になるだろう。そのため、私たちは崩壊を回避するための解決策を見つけ、開発するために全力を尽くす。たとえシステムが強力であったとしても、私たちの集団的想像力がそれと同じくらい強力であることを決して忘れてはならない。

最後に、たとえそれが受け入れられず望まれていないとしても、このビジョンの構築は、世界のあり方を明らかにする。それは人々に、自分たちは単にインセンティブを生み出すルールで巨大なゲームをしているだけであり、ルールが変われば自分たちの生活や機会、特権は大きく変わることを気づかせることができる。

> ボートを作りたければ、男女を集めて命令したり、細部まで説明したり、どこに何があるか教えたりしてはいけない。船を作りたければ、男も女も海に憧れなさい」。アントワーヌ・ド・サン・テグジュペリ

## 移り変わりの物語の葛藤

私たちの社会を進化させてきたのは、太古の昔から物語であった。私たちの時代も例外ではなく、権力者たちは私たちの社会が終わるとは考えず、超近代的な物語と移行期の物語の構築を信じている。

彼らが想像する未来は、とりわけ気候変動の猛威に対抗することを可能にする技術が詰め込まれた人工都市に住むというものだ。食料はますます工業的、人工的、合成的になっていくだろう。私たちは情報経済、いや監視経済に支えられたインスタント消費に没頭するだろう。私たちはすべての借主となり、もはや何も求めず、しかし「幸せ」になるだろう。

しかし、超近代的な物語と移行期の物語の構築は崩壊した。万人のための社会的進歩と相関する、無限の物質的進歩という理想に基づいたこの近代は、多次元的な危機に陥り、完全に不協和音を発するようになった。ハイパーモダニティーの物語は、進歩の理想を利用すると同時に、その解放的な社会的本質を空にしてしまった。それは操作的であり、特に新語の出現を通してメディアに支えられている。言葉は本来の意味から乗っ取られ、その本質を空っぽにされ、隠された利益に従って方向づけられる。ハイパーモダニティとは、過度の蓄積であり、侵略的な広告によって飽くことのない欲望から生まれた過度の消費である。すべてを手に入れた現代人は、逆説的だが、人間の中で最も不満足な存在なのだ。[^17]

超近代的な物語は、3つの大きな失敗を経験している。第一は生態学的な失敗であり、グリーン資本主義、すなわち生命を尊重する生産主義の約束は、ますますあり得ないと思われる。第二の失敗は経済的なもので、物質的には並外れて豊かであるにもかかわらず、地域的なものから世界的なものまで、あらゆる規模での買い占めと不平等が指数関数的に増加している。第三の失敗は倫理的なものである。私たちの社会的関係は商業的関係に変容しつつあり、そこから誰もが勝者になることを望み、あらゆるものが売買されている。こうした不均衡は暴力を生み、もはや持続可能ではない。

超近代的な物語に代わるものとして、私たちを新たな時代、つまりヌースフィアに突入させる移行期の物語が生まれつつある。しかし、それはまだ建設にはほど遠く、望ましいものでもない。ユートピア・プロジェクトの目的は、この新しい物語を構築することである。

では、人々が本当は何を夢見ているのかを理解する必要があるのですね？想像力は力強く、実現への第一歩となる。例えば、私たちはいつでもどこでも休暇を過ごしているような気分を夢見ているのだろうか？では、私たちの集団的理想とは何だろうか？この運動に意味を持たせ、新たな政策を立案し、実行するために、私たちはこの問いに答えることが急務なのだ。

> 私たちが明るい明日を築くためには、友愛と創造的想像力の城壁が、私たちと崩壊との間に立ちはだからなければならない。Valérie Jousseaume[^17]より引用。

## 集合知

一人の人間が真理を握っているわけではないが、共にいれば少なくとも真理に近づくことができる。

ここに書かれていることを信じろとは言われない。 それどころか、何も信じないこと、自分が真実を握っているとは決して思わないこと、常に自分の中に疑いを持ち続けることが奨励されているのだ。

なぜなら、自分が真理を握っていると信じることは、自分が真理を求めることを妨げることであり、真理を見つけることを妨げることになるからだ。

だからこそ、私たちは自分たちだけではこの世界を想像することができない。それを達成するためには、集合知を利用し、そのツールを開発する必要がある。

だから、もしあなたがこれを読んでいるなら、私たちは現在、Gitというツールを使ってこの文章を編集しています。また、このストーリーの一部の編集に参加し、投稿することで、投稿者全員が意見を述べ、議論し、最終的に修正を取り入れるかどうかを投票することができます。変更が承認されると、今あなたが読んでいるところにオンラインで表示されます。

このアイデアは、このストーリーの共同的で民主的な編集に特化したプラットフォームをデザインすることである。このツールは、他の多くのプロジェクトでも使用できるよう、十分にモジュール化される。

このアプリケーションはEkklésiappと呼ばれ、古代ギリシャ語で「集会」を意味するEkklesiaとアプリケーションをミックスしたもので、民主主義の発祥地であるアテネを含む古代ギリシャの都市の市民集会にそのまま由来している。当時、民主主義は支配階級にしか浸透していなかったが、第5章「みんなで決める方法」で、民主主義がいかに不完全なものであるか、そしていかに多くの課題が残されているかを知ることができる。[^18]

Ecclesiappは、数人から全人口までの規模で使用することができる意思決定アプリケーションであり、無限の異なる意思決定方法を作成することができるように十分にモジュール化されており、行うべき仕事に簡単に適応させることができる。選択した方法は簡単に変更できるので、最良の結果をもたらす方法を継続的に探すことができる。

この方法は自由に利用できる。ユートピア、ディストピア、あるいはユートピアを創造することができる。そしてこれらすべては、この分野の研究を発展させるためである。

また、他の人々が他の種類の共同執筆作業を行うこともできる。組織やビジネス、その他あらゆるプロジェクトの管理にも利用でき、最終的には真の民主的なツールを提供し、パワーを草の根に取り戻し、支配やヒエラルキーのシステムをなくすのに十分な効果を発揮する。

しかし、この物語を発表するためのルールについて、私たちはどのように合意できるのだろうか？

エクレシアは、この理想的な世界に導入される民主的な意思決定プロセスに直接インスパイアされている。実際、この理想世界における民主的な意思決定プロセスは、このプラットフォームに触発されることになる。というのも、このプラットフォームは、正しい集団的かつ民主的な意思決定プロセスに関する私たちのアイデアをテストし、誰もが最良の結果につながると同意するものを見つけるまで、それを反復することを直接可能にするからです。つまり、意思決定について意見を述べるだけでなく、意思決定プロセスについても意見を述べるのだ。現在の意思決定プロセスは以下で明らかにされる。

> 一人ではより速く、共に進めばより遠くへ」 アフリカのことわざ

## 草案作成プロセス

この物語を審査するため、あるいはこの物語に参加するために必要なスキルについては、何もない。

世の中が複雑だと思うのなら、それは私たちが複雑にしてしまったから複雑なのであって、実際には、良いベースから出発すれば、すべては見かけよりも単純で論理的であることが判明する。

私たちには複雑に見えるものでも、本当は単純な要素の積み重ねにすぎない。

結局のところ、偉大な科学者とは、この世界の単純さを垣間見ることができた人たちなのである。

自分自身を疑わないでほしい。もしあなたが何かを理解できないのなら、それは単に、誰かが時間をかけて、あなたが知っている基礎から説明してくれていないだけなのだ。

実際、常にゼロから始めることが不可欠だ。それがこの物語の基本原則のひとつであり、だからこそパート2で時間をかけてしっかりとした土台を作り、パート3でユートピアの詳細を説明するのだ。

本書は誰もが読み、理解できるように意図されている。知的な人たちには、本書は単純すぎるし、語彙も豊かでないと思われるかもしれないが、それが狙いである。

私たちは皆、変革の主体でなければならないのだから、私たちを結びつけるべきものが、この分野やあの分野の深い知識を持たない人間には理解不能で理解不能なものであるならば、どうして私たちは団結できるのだろうか。

ここで私たちは、難しい用語や長く複雑な言い回しを使おうとしているわけではないし、より科学的に、より知的に見せるためにこのような方法を用いているわけでもない。

この話はわかりやすく、理解しやすく、要点がまとまっていなければならない。また、心理的な効果を利用することもなく、内容を信じるか信じないかはあなたの自由です。

人類の歴史において、一人の作家が生み出すものは、集合知の間接的な産物にすぎない。

この物語は、世界的な思想の進化に貢献してきた無数の人間たちが、人類全体を通じて行ってきた「真実」の絶え間ない探求から自然に生まれたものである。これらの人類はそれぞれ、過去から集合的な知識を受け取り、彼らが享受した技術、地位、時間の助けを借りて、新たな思想や知識を収集し、発表することにつながる相互作用の恩恵を受けた、ある時、ある瞬間に生まれた。それは、この物語を軽くし、親しみやすくするためであると同時に、これらの発見を個人化しすぎないようにするためでもあり、また、人類の歴史における知識や思考の創造はすべて、間接的にせよ、集合的なものであったということを理解するという新しいパラダイムを促進するためでもある。私たちは今、この物語で使われているようなメカニズムを促進するツールの助けを借りて、この集合的知性をさらに進化させたいと考えている。この物語を通して、この個別性から抜け出そうとするある種の言語にも気づくだろう。

私たちの見解では、ユートピアは何千年にもわたって生み出された集合知の結晶であり、私たち自身を含むこれらの時代の過程で孤立した個人が生み出したものではない。人類は、何百万、何千億もの細胞で満たされ、生き、死に、生まれ変わり、常に進化し、環境のバランスをとるために再適応している。

とはいえ、原著者への参照は常にページの下に注釈をつけ、これらの個人とその仕事についてより詳しく知ることができるようにしている。また、私たちが提示する証拠を検証できるよう、出典をすべて公開している。

このストーリーの、よりリッチで科学的な第2版も作成され、学術機関への内容説明をサポートするために、このストーリーに直接リンクされる予定である。このバージョンは、特定のテーマに関する歴史や考え、議論についてもっと知りたいと思ったときに、ボタンをクリックするだけで簡単にアクセスできるようになり、クリックすればするほど、どのテーマについてもより深く掘り下げることができるようになる。

多くの研究が引用されていますが、一般的なルールとして、科学に影響を与えやすいので、読んだ研究の結論には慎重になることをお勧めします。研究の中には、研究に影響を与えるような特定の動機や利害を持つ企業や利益団体から資金提供を受けているものもあります。

研究は、選択バイアス、測定エラー、統計エラーなどの設計エラーの影響を受ける可能性があり、その結果、代表的でない、または正しくない結果が出る可能性があります。結果の解釈は主観的で、研究著者の信念や偏見に左右されることがある。結果を文脈の中で考え、証拠を全体として見ることが重要である。一般的に、数値は特定の議論を支持するために様々な方法で提示されることがあり、そのため誤った結論を導くことがある。[^19][^20][^21]

その考えは、科学を敬遠することではなく、逆に科学に依存することである。しかし、警戒心をもって、生み出される知識は流動的で進化していること、新しい研究が以前の研究と矛盾していること、現状では研究に資金を提供する経済システムに大きく影響されていることを認識することである。[^22]

さらに、ここでは詳しく述べないが、形而上学は、何が存在し、実体がどのように分類され理解されうるかについての基本的な仮定に疑問を投げかける。特に、自由意志の存在、身体との関係における心の性質、現実そのものの基礎といった概念を探求する。形而上学的考察は、科学的研究に直接挑戦するものではないが、科学的研究の根底にある概念的枠組みや前提に疑問を投げかけるものである。これには、ある研究を「再現」するとはどういうことか、因果関係をどのように理解するか、科学的な文脈で何かが「現実」あるいは「真実」であるとはどういうことか、例えば、観察という行為は中立ではなく、観察される対象の状態に影響を与えるという事実などについての考察が含まれる。要するに、形而上学は、科学[^23]を含め、私たちが知識を探求する際に当然だと思っていることの哲学的基礎を問うものなのである。

その使命は、人類全体によって遂行されるものであり、人類自身のために、その進化を通じて更新されるものである。したがって、その最も深い基礎を含め、その内容に矛盾する可能性のある新たな証拠に対して、この作品は常に開かれているのである。

> 疑うことは知恵の始まりである。アリストテレス[^25］

## 序論の結論

お分かりのように、あなたはこれから『ユートピア』を読もうとしている。原始的な状態から脱却し、社会が真の文明に向かって進化し、迫り来る生態学的・人類学的災害を回避する時が来たのだ。この物語の目的は、人類が全体として、自らの手で、自らのために実現し、最終的に意味と方向性を取り戻し、自由と幸福の保証された未来を確保することである。

> 人間の魂の緊張と矛盾は、人と人との間の緊張、つまり人間のネットワークの構造的矛盾が消えて初めて消える。そのとき、「幸福」と「自由」という崇高な言葉が意図する最適な物理的均衡、すなわち、社会的課題、社会的存在に必要な要件の総体と、個人的傾向や欲求との間の永続的な均衡、あるいは完全な調和を、個人が見出すことは、もはや例外ではなく、原則となる。人間の相互関係の構造がこの原則に触発され、すべての個人の存在の根幹である人間同士の協力が、複雑な共通課題の連鎖に手を携えて取り組むすべての人々が、少なくともこの均衡を見出す可能性を持つような形で行われるようになって初めて、人々は自分たちが「文明人」であると、もう少し理性的に主張できるようになる。それまでは、せいぜい文明化の過程に携わっているにすぎない。それまでは、「文明はまだ完成していない。文明はまだ完成していない！ノーバー・エリアス『西洋の力学』。

## 出典（非網羅的リスト）

[^1]:ユヴァル・ノア・ハラリ『サピエンス：A Brief History of Humanity.2011
[^2]:Sébastien Bohler, Where is the meaning?
[^3]:[ユートピアの語源](https://fr.wiktionary.org/wiki/utopie)
[^4]:Serge-Christophe Kolm, The Good Economy: General Reciprocity.1984
[^5]:[大気中の二酸化炭素の最大濃度](https://www.ipcc.ch/report/ar6/wg1/)
[^6]:[リンと窒素の生化学的サイクルの撹乱限界](https://www.nature.com/articles/s41467-023-40569-3)
[^7]:[生物多様性の最大侵食](https://www.fondationbiodiversite.fr/wp-content/uploads/2019/11/IPBES-Depliant-Rapport-2019.pdf)
[^8]:[集約的土地利用](https://www.nature.com/articles/s41467-023-40569-3)
[^9]:[淡水の乱用](https://reliefweb.int/report/world/rapport-mondial-des-nations-unies-sur-la-mise-en-valeur-des-ressources-en-eau-2022-eaux)
[^10]:[海洋酸性化](https://www.ipcc.ch/report/ar6/wg1/)
[^11]:パブロ・セルヴィーニュ＆ラファエル・スティーヴンス『すべてが崩壊する方法
[^12]:[ユートピアの語源](https://fr.wiktionary.org/wiki/eutopie)
[^13]:マーティン・J・S・ラドウィック『地球の歴史
[^14]:ピーター・D・ワードとドナルド・ブラウンリー著『地球生命の未来
[^15]:Sébastien Bohler, The human bug.2019
[^16]:ジャレド・ダイアモンド『崩壊』.2004
[^17]:Valérie Jousseaume, Plouc Pride: A new narrative for the countryside.
[^18]:セルジュ・クリストンヌ『選挙は民主主義か？1977
[^19]:ベン・ゴールドエーカー『悪い科学2008
[^20]:[発表された研究結果のほとんどが誤りである理由](https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0020124)
[^21]:[再現性の危機](https://www.news-medical.net/life-sciences/What-is-the-Replication-Crisis.aspx)
[^22]:ARTE、ドキュメンタリー：無知の工場。2020
[^23]:キム・ジェグォン『形而上学の基礎
[^24]:シリル・ディオン『現代レジスタンスの手引き』2018年
[^25]:アリストテレス『エウデムスへの倫理学
